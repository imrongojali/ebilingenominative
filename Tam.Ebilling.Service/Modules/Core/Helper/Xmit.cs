﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;

namespace Tam.Ebilling.Service.Modules.Core.Helper
{
    public class Xmit
    {
        public static string Download(byte[] data, Page _page, string _FileName, string _ext, string _ScriptKey, string windowName, string tpath)
        {
            string oLink = "";

            string opath = _page.Server.MapPath(tpath);
            if (!Directory.Exists(opath))
                Directory.CreateDirectory(opath);
            string pdfFile = _FileName + _ext;
            tpath = tpath.Substring(1, tpath.Length - 1);

            oLink = _page.Request.Url.Scheme + Uri.SchemeDelimiter
                + _page.Request.Url.Authority
                + tpath + pdfFile;

            File.WriteAllBytes(Path.Combine(opath, pdfFile), data);

            _page.ClientScript.RegisterStartupScript(_page.GetType()
                , _ScriptKey,
                "openWin('" + oLink + "','" + windowName + "');",
                true);

            return oLink;
        }

        public static void Send(byte[] data, HttpContext c, string _FileName)
        {
            if (c == null || c.Response == null)
                return;
            c.Response.Clear();
            c.Response.AddHeader("content-disposition", String.Format("attachment;filename=\"{0}\"", _FileName));
            c.Response.Charset = "";
            c.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            c.Response.ContentType = ContentTypeOf(_FileName);
            c.Response.BinaryWrite(data);
        }

        public static void Transmit(HttpContext c, string filepath)
        {
            // Create New instance of FileInfo class to get the properties of the file being downloaded
            FileInfo file = new FileInfo(filepath);

            // Checking if file exists
            if (file.Exists)
            {
                // Clear the content of the response
                c.Response.Clear();
                c.Response.ClearContent();
                c.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                // LINE1: Add the file name and attachment, which will force the open/cance/save dialog to show, to the header
                c.Response.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", file.Name));

                // Add the file size into the response header
                c.Response.AddHeader("Content-Length", file.Length.ToString());

                // Set the ContentType
                c.Response.ContentType = ContentTypeOf(file.Extension.ToLower());

                // Write the file into the response (TransmitFile is for ASP.NET 2.0. In ASP.NET 1.1 you have to use WriteFile instead)
                c.Response.TransmitFile(filepath);

                // End the response
                //c.Response.End();
                //c.ApplicationInstance.CompleteRequest();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
                //send statistics to the class
            }
        }

        public static void ForceDownload(HttpContext c, string fullPathToFile)
        {
            //c.Response.Clear();
            //c.Response.AddHeader("content-disposition", "attachment; filename=" + outputFileName);
            //c.Response.WriteFile(fullPathToFile);
            //c.Response.ContentType = "";
            //c.Response.Flush();
            //HttpContext.Current.ApplicationInstance.CompleteRequest();

            FileInfo file = new FileInfo(fullPathToFile);
            if (file.Exists)
            {
                byte[] Content = File.ReadAllBytes(fullPathToFile);
                c.Response.ContentType = ContentTypeOf(file.Extension.ToLower());
                c.Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                c.Response.BufferOutput = true;
                c.Response.OutputStream.Write(Content, 0, Content.Length);
            }
        }

        public static string ContentTypeOf(string fn)
        {
            string ext = Path.GetExtension(fn).ToLower();
            if (ext.Equals(".doc") || ext.Equals(".docx"))
                return "application/msword";
            else if (ext.Equals(".pdf"))
                return "application/pdf";
            else if (ext.Equals(".xls") || ext.Equals(".xlsx"))
                return "application/vnd.ms-excel";
            else if (ext.Equals(".jpg") || ext.Equals(".jpeg"))
                return "image/jpeg";
            else if (ext.Equals(".png"))
                return "image/png";
            else if (ext.Equals(".tif") || ext.Equals(".tiff"))
                return "image/tiff";
            else if (ext.Equals(".zip"))
                return "application/zip";
            else if (ext.Equals(".txt"))
                return "text/plain";
            else
                return "application/octet-stream";
        }
    }

}
