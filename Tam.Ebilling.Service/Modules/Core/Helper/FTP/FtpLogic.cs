﻿using System;
using System.Net;
using System.IO;
using System.Collections;
using System.Text;
using Tam.Ebilling.Infrastructure.Cache;

namespace Tam.Ebilling.Service
{
    public class FtpLogic
    {
        public string Directory, DirectFP;
        public string server = ApplicationCacheManager.GetConfig<string>("FilePVMonitoringServer");
        public string serverFP = ApplicationCacheManager.GetConfig<string>("FileFPServer");
        public string FPfolder = ApplicationCacheManager.GetConfig<string>("FileFPFolder");

        public bool ftpUploadBytes(string docYear, string module,
                              string docNo, string filename,
                              byte[] fileBytes, ref bool result,
                              ref string errMessage, bool byInternal)
        {
            result = false;

            Directory = "ftp://" + server + "/" + docYear + "/" + module + "/" + docNo + "/";
            DirectFP = "ftp://" + serverFP + "/" + FPfolder + (!string.IsNullOrEmpty(FPfolder) ? "/" : "");

            int lastDelim = filename.LastIndexOf(Path.DirectorySeparatorChar);
            string filepath = (lastDelim > 0) ? filename.Substring(0, lastDelim) : "";
            string justname = (lastDelim > 0) ? filename.Substring(lastDelim + 1, filename.Length - lastDelim - 1) : filename;
            string ftpfullpath = Directory + "/" + filename;

            FtpWorker fw = new FtpWorker();

            result = fw.Upload(fileBytes, filename, Directory, ref result, ref errMessage);

            if (result)
            {
                if (filename.Substring(0, 3) == "FP_" && !byInternal)
                {
                    result = fw.UploadFP(fileBytes, filename, DirectFP, ref result, ref errMessage);

                    if (!result)
                    {
                        fw.Delete(ftpfullpath, ref errMessage);
                    }
                }
            }

            return result;
        }

        public bool ftpUploadAttachBankBytes(string directory, string filename,
                              byte[] fileBytes, ref bool result, ref string guidFilename, 
                              ref string errMessage)
        {
            result = false;
            
            string fullserver = "ftp://" + server + "/" + directory + "/";

            int lastDelim = filename.LastIndexOf(Path.DirectorySeparatorChar);
            string filepath = (lastDelim > 0) ? filename.Substring(0, lastDelim) : "";
            string justname = (lastDelim > 0) ? filename.Substring(lastDelim + 1, filename.Length - lastDelim - 1) : filename;
                   guidFilename = Guid.NewGuid().ToString() + "-" + justname;

            FtpWorker fw = new FtpWorker();

            result = fw.Upload(fileBytes, guidFilename, fullserver, ref result, ref errMessage);            

            return result;
        }

        public byte[] ftpDownloadFile(string docYear, string module, string docNo, string filename, ref string errMessage)
        {
            byte[] result;

            string url = "ftp://" + server + "/" + docYear + "/" + module + "/" + docNo + "/" + filename;
            
            FtpWorker fw = new FtpWorker();
            result = fw.ftpDownloadByte(url, ref errMessage);

            return result;
        }

        public byte[] ftpDownloadBankFile(string guidFilename, string directory, ref string errMessage)
        {
            byte[] result;

            string url = "ftp://" + server + "/" + directory + "/" + guidFilename;

            FtpWorker fw = new FtpWorker();
            result = fw.ftpDownloadByte(url, ref errMessage);

            return result;
        }

    }
}