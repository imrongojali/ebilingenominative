﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using System.Diagnostics;
using Tam.Ebilling.Infrastructure.Cache;

namespace Tam.Ebilling.Service
{
    public class FtpWorker : IDisposable
    {
        private FtpWebRequest f = null;
        private List<string> reply = new List<string>();
        private List<string> errs = new List<string>();

        public string username = ApplicationCacheManager.GetConfig<string>("FilePVMonitoringUserName");
        public string password = ApplicationCacheManager.GetConfig<string>("FilePVMonitoringPassword");

        public string usernameFP = ApplicationCacheManager.GetConfig<string>("FileFPUserName");
        public string passwordFP = ApplicationCacheManager.GetConfig<string>("FileFPPassword");

        public bool Delete(string url, ref string errMessage)
        {
            try
            {
                Uri serverUri = new Uri(url);
                if (serverUri.Scheme != Uri.UriSchemeFtp)
                {
                    return false;
                }
                FtpWebRequest reqFTP = (FtpWebRequest)WebRequest.Create(serverUri);

                reqFTP.Credentials = new NetworkCredential(username, password);
                reqFTP.KeepAlive = false;
                reqFTP.Proxy = new WebProxy();
                reqFTP.Method = WebRequestMethods.Ftp.DeleteFile;

                string result = string.Empty;
                FtpWebResponse response = (FtpWebResponse)reqFTP.GetResponse();
                long size = response.ContentLength;
                Stream datastream = response.GetResponseStream();
                StreamReader sr = new StreamReader(datastream);
                result = sr.ReadToEnd();
                sr.Close();
                datastream.Close();
                response.Close();
                return true;
            }
            catch (Exception ex)
            {
                errMessage = ex.Message;
            }
            return false;
        }

        public byte[] ftpDownloadByte(string url, ref string errMessage)
        {
            try
            {
                errMessage = "";

                Uri serverUri = new Uri(url);
                if (serverUri.Scheme != Uri.UriSchemeFtp)
                {
                    return null;
                }

                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(serverUri);
                request.Method = WebRequestMethods.Ftp.DownloadFile;

                request.Credentials = new NetworkCredential(username, password);
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                Stream responseStream = response.GetResponseStream();

                byte[] fileBytes = null;

                using (MemoryStream ms = new MemoryStream())
                {
                    responseStream.CopyTo(ms);
                    fileBytes = ms.ToArray();
                }

                responseStream.Close();
                response.Close();

                return fileBytes;
            }
            catch (Exception ex)
            {
                errMessage = ex.Message;
            }

            return null;
        }

        public bool Upload(byte[] fileBytes, string filename, string uploadPath, ref bool result, ref string errMessage)
        {
            result = false;

            string uri = uploadPath;
            int lastDelim = filename.LastIndexOf(Path.DirectorySeparatorChar);
            string filepath = (lastDelim > 0) ? filename.Substring(0, lastDelim) : "";
            string justname = (lastDelim > 0) ? filename.Substring(lastDelim + 1, filename.Length - lastDelim - 1) : filename;

            bool? x = Has(uri, justname);

            result = !(x ?? false);

            if (x == null)
            {
                //err("Connect failed");
                errMessage = "Connecting to ftp failed";
                result = false;
            }
            else
            {
                if (result)
                {
                    MakeDirs(uri);

                    string ftpfullpath = uri + "/" + justname;

                    Connect(ftpfullpath);
                    f.Method = WebRequestMethods.Ftp.UploadFile;

                    Stream ftpstream = f.GetRequestStream();
                    ftpstream.Write(fileBytes, 0, fileBytes.Length);
                    ftpstream.Close();

                    result = Fetch();
                }
                else
                {
                    err("File {0} already exists", filename);
                }

            }

            return result;
        }

        public bool UploadFP(byte[] fileBytes, string filename, string uploadPath, ref bool result, ref string errMessage)
        {
            result = false;

            string uri = uploadPath;

            int lastDelim = filename.LastIndexOf(Path.DirectorySeparatorChar);
            string filepath = (lastDelim > 0) ? filename.Substring(0, lastDelim) : "";
            string justname = (lastDelim > 0) ? filename.Substring(lastDelim + 1, filename.Length - lastDelim - 1) : filename;

            bool? x = HasFP(uri, justname);

            result = !(x ?? false);

            if (x == null)
            {
                //err("Connect failed");
                errMessage = "Connecting to ftp Faktur failed";
                result = false;
            }
            else
            {
                if (result)
                {
                    MakeDirsFP(uri);

                    string ftpfullpath = uri + "/" + justname;

                    ConnectFP(ftpfullpath);
                    f.Method = WebRequestMethods.Ftp.UploadFile;

                    Stream ftpstream = f.GetRequestStream();
                    ftpstream.Write(fileBytes, 0, fileBytes.Length);
                    ftpstream.Close();

                    result = Fetch();
                }
                else
                {
                    err("File {0} already exists", filename);
                }

            }

            return result;
        }

        public bool MakeDirs(string uri)
        {
            bool r = false;
            int j = uri.Length;


            if (Has(uri) ?? false)
                return true;

            int firstSlash = uri.IndexOf("/", 7);
            int nextSlash = uri.IndexOf("/", firstSlash + 1);
            int lastSlash = uri.LastIndexOf("/", j - 1);
            bool justMadeIt = false;
            while ((nextSlash < j) && (nextSlash > 0))
            {
                string url = uri.Substring(0, firstSlash);
                string fn = uri.Substring(firstSlash + 1, nextSlash - firstSlash - 1);
                if (justMadeIt || (!Has(url, fn) ?? false))
                {
                    r = MD(uri.Substring(0, nextSlash));
                    justMadeIt = true;
                }
                firstSlash = nextSlash;
                nextSlash = uri.IndexOf("/", nextSlash + 1);
            }
            if (justMadeIt || (!Has(uri) ?? false))
                MD(uri);

            return r;
        }

        public bool MakeDirsFP(string uri)
        {
            bool r = false;
            int j = uri.Length;


            if (HasFP(uri) ?? false)
                return true;

            int firstSlash = uri.IndexOf("/", 7);
            int nextSlash = uri.IndexOf("/", firstSlash + 1);
            int lastSlash = uri.LastIndexOf("/", j - 1);
            bool justMadeIt = false;
            while ((nextSlash < j) && (nextSlash > 0))
            {
                string url = uri.Substring(0, firstSlash);
                string fn = uri.Substring(firstSlash + 1, nextSlash - firstSlash - 1);
                if (justMadeIt || (!HasFP(url, fn) ?? false))
                {
                    r = MDFP(uri.Substring(0, nextSlash));
                    justMadeIt = true;
                }
                firstSlash = nextSlash;
                nextSlash = uri.IndexOf("/", nextSlash + 1);
            }
            if (justMadeIt || (!HasFP(uri) ?? false))
                MDFP(uri);

            return r;
        }

        private bool MDFP(string uri)
        {
            bool r = false;
            try
            {
                ConnectFP(uri);
                f.Method = WebRequestMethods.Ftp.MakeDirectory;

                r = Fetch();
            }
            catch (Exception ex)
            {
                err(ex);
            }
            return r;
        }

        private bool MD(string uri)
        {
            bool r = false;
            try
            {
                Connect(uri);
                f.Method = WebRequestMethods.Ftp.MakeDirectory;

                r = Fetch();
            }
            catch (Exception ex)
            {
                err(ex);
            }
            return r;
        }

        private bool? Has(string uri, string filename = "")
        {
            bool? r = null;
            string go = uri;
            if (!uri.Substring(uri.Length - 1, 1).Equals("/")) go = uri + "/";
            try
            {
                Connect(go);
                f.Method = WebRequestMethods.Ftp.ListDirectory;

                if (Fetch())
                {
                    if (string.IsNullOrEmpty(filename) && reply.Count > 0) r = true;
                    else
                    {
                        r = false;
                        foreach (string s in reply)
                        {
                            if (string.Compare(s, filename, true) == 0)
                            {
                                r = true;
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                err(ex);
            }
            go = go + filename;

            return r;
        }

        private bool? HasFP(string uri, string filename = "")
        {
            bool? r = null;
            string go = uri;
            if (!uri.Substring(uri.Length - 1, 1).Equals("/")) go = uri + "/";
            try
            {
                ConnectFP(go);
                f.Method = WebRequestMethods.Ftp.ListDirectory;

                if (Fetch())
                {
                    if (string.IsNullOrEmpty(filename) && reply.Count > 0) r = true;
                    else
                    {
                        r = false;
                        foreach (string s in reply)
                        {
                            if (string.Compare(s, filename, true) == 0)
                            {
                                r = true;
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                err(ex);
            }
            go = go + filename;

            return r;
        }

        private bool Fetch()
        {
            bool r = false;
            string line;
            reply.Clear();
            try
            {
                using (WebResponse response = f.GetResponse())
                {
                    using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                    {
                        while ((line = reader.ReadLine()) != null)
                        {
                            reply.Add(line);
                        }
                    }
                }
                r = true;
            }
            catch (WebException ex)
            {
                WebExceptionStatus[] l = new WebExceptionStatus[] {
                    WebExceptionStatus.ConnectFailure,
                    WebExceptionStatus.ConnectionClosed,
                    WebExceptionStatus.Timeout,
                    WebExceptionStatus.TrustFailure,
                    WebExceptionStatus.ProxyNameResolutionFailure
                };
                if (!l.Contains(ex.Status))
                    r = true;
                err(ex);
            }
            catch (Exception ex)
            {
                err(ex);
            }
            return r;
        }

        private void err(Exception ex)
        {
            errs.Add(ex.Message);
        }

        private void err(string s, params object[] x)
        {
            if (x != null)
                errs.Add(string.Format(s, x));
            else
                errs.Add(s);
        }

        private void Connect(string uri, bool KeepAlive = true)
        {
            f = (FtpWebRequest)FtpWebRequest.Create(uri);

            f.Credentials = new NetworkCredential(username, password);
            f.KeepAlive = KeepAlive;
            f.UsePassive = false;
            f.UseBinary = true;
            f.Proxy = new WebProxy();
        }

        private void ConnectFP(string uri, bool KeepAlive = true)
        {
            f = (FtpWebRequest)FtpWebRequest.Create(uri);

            f.Credentials = new NetworkCredential(usernameFP, passwordFP);
            f.KeepAlive = KeepAlive;
            f.UsePassive = false;
            f.UseBinary = true;
            f.Proxy = new WebProxy();
        }

        public void Dispose()
        {

        }
    }
}