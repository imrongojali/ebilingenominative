﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tam.Ebilling.Service
{
    public class EmailSettingRequest
    {
        public string FromAddress { get; set; }
        public string FromDisplay { get; set; }
        public string SmtpHost { get; set; }
        public int SmtpPort { get; set; }
        public bool EnableSSL { get; set; }
        public bool UseDefaultCredential { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
