﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tam.Ebilling.Service
{
    public class RoleRequest
    {
        public IEnumerable<string> Permissions { get; set; }
        public Guid ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
