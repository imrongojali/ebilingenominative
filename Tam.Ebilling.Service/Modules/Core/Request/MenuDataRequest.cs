﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tam.Ebilling.Service
{
    public class MenuDataRequest
    {
        public int ID { get; set; }
        public int MenuGroupID { get; set; }
        public int ParentID { get; set; }
        public int OrderIndex { get; set; }
        public string Title { get; set; }
    }
}
