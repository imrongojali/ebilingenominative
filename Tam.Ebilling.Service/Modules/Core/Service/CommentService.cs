﻿using Dapper;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tam.Ebilling.Domain;

namespace Tam.Ebilling.Service
{
    public class CommentService : DbServiceBase
    {
        public CommentService(IDbHelper db) : base(db)
        {

        }
        public Kendo.Mvc.UI.DataSourceResult GetCommentDataSourceResult(DataSourceRequest request, string invoice_number, int business_unit_id)
        {
            var result = Db.CommentRepository.GetCommentDataSourceResult(request, invoice_number, business_unit_id);
            return result;
        }

        public IEnumerable<udf_GetComment> GetComment(string invoice_number, int business_unit_id)
        {
            var d = Db.Connection.Query<udf_GetComment>("SELECT * FROM [dbo].[udf_GetComment](@invoice_number, @business_unit_id)", new { invoice_number = invoice_number, business_unit_id = business_unit_id }).ToList();
            return d;
        }

        public int Add(Comment comment, string[] columns)
        {
            var d = Db.CommentRepository.Add(comment, columns);
            return d;
        }
    }
}
