﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tam.Ebilling.Domain;

namespace Tam.Ebilling.Service
{
    public class ARStatusService : DbServiceBase
    {
        public ARStatusService(IDbHelper db) : base(db)
        {

        }
        public List<ARStatus> GetARStatus()
        {
            var strsql = @"SELECT * FROM vw_AR_Status";
            var d = Db.Connection.Query<ARStatus>(strsql).ToList();
            return d;
        }
    }
}
