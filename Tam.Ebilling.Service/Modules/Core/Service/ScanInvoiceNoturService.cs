﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Tam.Ebilling.Domain.Modules.Core.Model;
using Tam.Ebilling.Domain;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using Tam.Ebilling.Infrastructure.Session;
using System;
using Tam.Ebilling.Infrastructure.Cache;
using System.IO;
using Tam.Ebilling.Infrastructure.Helper;
using System.Net;
using Tam.Ebilling.Infrastructure.Adapter;
namespace Tam.Ebilling.Service
{
    public class ScanInvoiceNoturService : DbServiceBase
    {
        public ScanInvoiceNoturService(IDbHelper db) : base(db)
        {
        }
        public DataSourceResult GetDataSourceResult(DataSourceRequest request)
        {
            var result = Db.ScanInvoiceNoturRepository.GetDataSourceResult(request);

            return result;
        }
        public IEnumerable<ScanInvoiceNotur> GetDocumentFileByInvoice(string invoice)
        {
            var result = Db.ScanInvoiceNoturRepository.Find(new { invoice_number = invoice });
            result = result.Where(x => x.row_status != 1);

            return result;
        }
        
        public IEnumerable<ScanInvoiceNotur> GetDocumentByID(string id)
        {
            var result = Db.ScanInvoiceNoturRepository.Find(new { attachment_invoice_notur_id = id });

            return result;
        }
        public bool SaveUploadScanNotur(string invoice, IEnumerable<ScanInvoiceNotur> docs, out string msgerror)
        {
            msgerror = "";
            var result = true;

            //long? id = null;
            //var reference = "";

            var username = ApplicationCacheManager.GetConfig<string>("FileUploadUserName");
            var password = ApplicationCacheManager.GetConfig<string>("FileUploadPassword");
            var config = ApplicationCacheManager.GetConfig<string>("FileUploadScanNoturTemp");
            var domain = ApplicationCacheManager.GetConfig<string>("FileUploadDomain");
            var userdomain = domain + "\\" + username;
            var pers = new Impersonation();
            pers.Impersonate(userdomain, password);

            if (docs != null)
            {
                if (docs.Count() > 0)
                {
                    //save document file
                    foreach (ScanInvoiceNotur doc in docs)
                    {
                        if (doc.Status == "New")
                        {

                            // Update Property
                            doc.invoice_number = invoice;
                            doc.created_date = DateTime.Now;
                            doc.created_by = SessionManager.Username;
                            doc.modified_by = SessionManager.Username;
                            doc.modified_date = DateTime.Now;
                            // Rest status
                            doc.Status = null;
                            Db.ScanInvoiceNoturRepository.UploadScan(doc.filename, doc.invoice_number);

                            var tempFile = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileUploadScanNoturTemp"), doc.filename);
                            var newFile = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileUploadScanNotur"), doc.filename);
                            //var Update = Service<InvoiceService>().GetUploadNotur(daNumber);
                            //var reference = reff;
                            var credential = new NetworkCredential(userdomain, password);
                            using (new NetworkConnection(tempFile, credential))
                            using (new NetworkConnection(newFile, credential))
                            {
                                try
                                {
                                    File.Move(tempFile, newFile);
                                }
                                catch (Exception e)
                                {

                                }
                                //Db.InvoiceRepository.GetUploadNotur(doc.reference);
                                //Db.NotaReturRepository.GetUploadNoturMV(doc.reference);
                            }
                            //}

                        }
                        else if (doc.Status == "Delete")
                        {
                            if (doc.invoice_number == default(string))
                            {
                                // Delete physical file
                                var tempFile = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileUploadScanNoturTemp"), doc.filename);
                                var credential = new NetworkCredential(userdomain, password);
                                using (new NetworkConnection(tempFile, credential))
                                {
                                    try
                                    {
                                        File.Delete(tempFile);
                                    }
                                    catch (Exception e)
                                    {

                                    }
                                }
                            }
                            else
                            {
                                //Delete data
                                Db.ScanInvoiceNoturRepository.RemoveScanNotur(doc.attachment_invoice_notur_id);
                                //if (doc.reference.HasValue)
                                //{
                                //    var dattodel = dbhelper.AttachmentNotaReturRepository.Find(new { reference = reference.Value, filepath = doc.filepath }).FirstOrDefault();
                                //    if (dattodel != null)
                                //    {
                                //        dbhelper.AttachmentNotaReturRepository.Remove(dattodel);
                                //    }
                                //}

                                // Delete physical file
                                var file = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileUploadScanNotur"), doc.filename);
                                var credential = new NetworkCredential(userdomain, password);
                                using (new NetworkConnection(file, credential))
                                {
                                    try
                                    {
                                        File.Delete(file);
                                    }
                                    catch (Exception e)
                                    {

                                    }
                                }
                            }
                        }
                    }
                }
            }

            return result;
        }
        public List<ScanInvoiceNotur> GetAttachment(string invoice_number)
        {
            return Db.ScanInvoiceNoturRepository.Find(new { invoice_number = invoice_number }).ToList();
        }
    }
}
