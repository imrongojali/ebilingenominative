﻿using Dapper;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tam.Ebilling.Domain;

namespace Tam.Ebilling.Service
{
    public class Transaction_Code : DbServiceBase
    {
        public Transaction_Code(IDbHelper db) : base(db)
        {

        }
        public List<TransactionTypeCode> GetTransactionCode()
        {
            var strsql = @" SELECT distinct id, TransaksiTypeCode from [TAM_EFAKTUR].[dbo].[tb_m_jenispajak] a
                            order by a.TransaksiTypeCode asc";
            var d = Db.Connection.Query<TransactionTypeCode>(strsql).ToList();
            return d;
        }
    }
}