﻿using Kendo.Mvc.UI;
using Tam.Ebilling.Domain;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Tam.Ebilling.Infrastructure.Session;
using System.Web;

namespace Tam.Ebilling.Service
{
    public class ReceiptService : DbServiceBase
    {
        public ReceiptService(IDbHelper db) : base(db)
        {
        }
        // private SampleEntities entities;

        //public ProductService(SampleEntities entities)
        //{
        //    this.entities = entities;
        //}
        public DataSourceResult GetDataSourceResult(DataSourceRequest request, string customer_group, string userType)
        {
            var result = Db.ReceiptRepository.GetViewDataSourceResult(request, customer_group, userType);
            return result;
        }

        public void RecordHistory(string dateFrom, string dateTo, string businessType, string customerId, string user)
        {
            Db.ReceiptRepository.RecordHistory(dateFrom, dateTo, businessType, customerId, user);
        }
    }
}
