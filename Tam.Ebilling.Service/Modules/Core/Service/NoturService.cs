﻿using Kendo.Mvc.UI;
using System.Collections.Generic;
using Tam.Ebilling.Domain;
using System.Linq;
using Dapper;
using Tam.Ebilling.Infrastructure.Session;

namespace Tam.Ebilling.Service
{
    public class NoturService : DbServiceBase
    {
        public NoturService(IDbHelper db) : base(db)
        {
        }

        public DataSourceResult GetViewDataSourceResult(DataSourceRequest request, int invoice_type, string customer_group, string checkedRecords , bool checkedAll, string notifFilter=null)
        {
            var result = Db.InvoiceRepository.GetViewDataSourceResult(request, invoice_type, customer_group, checkedRecords, checkedAll, null, notifFilter);
            return result;
        }

        #region By AD ADD 23042021
        public DataSourceResult GetViewDataSourceResultDownloadCA(DataSourceRequest request, int invoice_type, string customer_group, string checkedRecords, bool checkedAll, string notifFilter = null)
        {
            var resultCA = Db.InvoiceRepository.GetViewDataSourceResultDownloadCA(request, invoice_type, customer_group, checkedRecords, checkedAll, null, notifFilter);
            return resultCA;
        }
        #endregion

        public List<Invoice> GetDataPDF(string inv_number)
        {
            string inv_no = inv_number.Replace(" ", "");
                   inv_no = inv_no.Replace("'", "");

            //var strsql = @"select distinct tax_attc_url, customer_name
            //         from BES.dbo.udf_Invoice(2, '{0}')
            //         WHERE tax_attc_url is not null and NoFakturPajakYangDiRetur='{1}'";


            var strsql = @"select distinct PdfUrl tax_attc_url, ef.NamaCustomer customer_name
	                    from [TAM_EFAKTUR].[dbo].[TB_R_VATOut] ef 
						WHERE   ef.NomorFakturGabungan='{1}' and ApprovalStatus = 'Approval Sukses' ";

            strsql = string.Format(strsql, SessionManager.RoleStr, inv_no);
            var d = Db.Connection.Query<Invoice>(strsql).ToList();

            return d;
        }

        public List<MvInvoice> GetMvInvoice(DataSourceRequest request, string daNumber, string user)
        {
            var result = Db.NotaReturRepository.GetMvInvoice(request, daNumber, user);

            return result;
        }
        public List<MvInvoice> GetMvInvoice(string daNumber, string user)
        {
            var result = Db.NotaReturRepository.GetMvInvoice(daNumber, user);

            return result;
        }
        public List<PrintNotur> GetPrintNotaRetur(DataSourceRequest request, string da_number,string user)
        {
            var result = Db.NotaReturRepository.GetPrintNotaRetur(request, da_number, user);

            return result;
        }

        public List<PrintNotur> GetPrintNotaRetur(string da_number, string user)
        {
            var result = Db.NotaReturRepository.GetPrintNotaRetur(da_number, user);

            return result;
        }

        public List<TransactionType> GetDataPDFx(string inv_numbe)
        {
            var strsql = @"SELECT * FROM vw_Transaction_Type";
            var d = Db.Connection.Query<TransactionType>(strsql).ToList();
            return d;
        }

        public List<DebitAdvice> GetDebitAdvice(DataSourceRequest request, string daNumber, string user)
        {
            var result = Db.NotaReturRepository.GetDebitAdvice(request, daNumber, user);
            return result;
        }

        public List<DebitAdvice> GetDebitAdvice(string da_number, string user)
        {
            var result = Db.NotaReturRepository.GetDebitAdvice(new DataSourceRequest(), da_number, user);
            return result;
        }

        public List<SpDebitCreditAdvice> GetSpDebitAdvice(DataSourceRequest request, string daNumber, string user)
        {
            var result = Db.NotaReturRepository.GetSpDebitAdvice(request, daNumber, user);
            return result;
        }

        public List<SpDebitCreditAdvice> GetSpDebitAdvice(string da_number, string user)
        {
            var result = Db.NotaReturRepository.GetSpDebitAdvice(new DataSourceRequest(), da_number, user);
            return result;
        }

        public List<SpDebitCreditAdvice> GetSpCreditAdvice(DataSourceRequest request, string caNumber, string user)
        {
            var result = Db.NotaReturRepository.GetSpCreditAdvice(request, caNumber, user);
            return result;
        }

        public List<SpNotaRetur> GetNotaReturSp(DataSourceRequest request, string caNumber, string user)
        {
            var result = Db.NotaReturRepository.GetNotaReturSp(request, caNumber, user);
            return result;
        }

        public List<SpNotaRetur> GetNotaReturMv(DataSourceRequest request, string NomorNotaRetur, string user)
        {
            var result = Db.NotaReturRepository.GetNotaReturMv(request,NomorNotaRetur, user);
            return result;
        }

        public List<Invoice> GetNotaReturs(string customer_group,
            string invoice_number = null,
            int? business_unit_id = null,
            int? transaction_type_id = null,
            string date_from = null,
            string date_to = null,
            string ar_status = null,
            int? document_status = null)
        {
            var output = Db.NotaReturRepository.GetNotaRetur(customer_group,
                    invoice_number,
                    business_unit_id,
                    transaction_type_id,
                    date_from,
                    date_to,
                    ar_status,
                    document_status);

            return output;
        }
    }
}
