﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tam.Ebilling.Domain;

namespace Tam.Ebilling.Service
{
    public class WHTTaxArticleService : DbServiceBase
    {
        public WHTTaxArticleService(IDbHelper db) : base(db)
        {

        }
        public List<WHTTaxArticle> GetWHTTaxArticle(string customer_group)
        {
            var result = Db.WHTRepository.GetWHTTaxArticle(customer_group);
            return result;
        }
    }
}
