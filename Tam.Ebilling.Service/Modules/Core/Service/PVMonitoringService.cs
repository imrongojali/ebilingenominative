﻿using Kendo.Mvc.UI;
using Tam.Ebilling.Domain;
using Dapper;
using System;
using System.Web;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Tam.Ebilling.Infrastructure.Session;
using Tam.Ebilling.Infrastructure.Cache;
using Tam.Ebilling.Infrastructure.Adapter;
using Tam.Ebilling.Infrastructure.Helper;
using System.IO;
using System.Net;

using System.Text.RegularExpressions;
using Kendo.Mvc.Extensions;
using System.Net.Http.Headers;
using System.Net.Http;

using Tam.Ebilling.Service.Modules.Core.Helper;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System.Text;
using System.Web.UI;

namespace Tam.Ebilling.Service
{
    public class PVMonitoringService : DbServiceBase
    {
        public PVMonitoringService(IDbHelper db) : base(db)
        {
        }

        public DataSourceResult GetDataSourceResult(DataSourceRequest request, string customerGroup, string userType, string levelCode, string statusFlag)
        {
            DataSourceResult result;

            List<PVHeader> x = new List<PVHeader>();
                    x = Db.PVMonitoringRepository.GetViewDataSourceResult(SessionManager.Current, customerGroup, userType, levelCode, statusFlag);

            result = x.ToDataSourceResult(request,
                        a => new PVHeader
                        {
                            PROCESS_ID = a.PROCESS_ID,
                            PV_TYPE = a.PV_TYPE,
                            PV_DATE = a.PV_DATE,
                            PV_NO = a.PV_NO,
                            TRANSACTION_DESC = a.TRANSACTION_DESC,
                            STATUS_BUDGET = a.STATUS_BUDGET,
                            BUDGET = a.BUDGET,
                            VENDOR_CD = a.VENDOR_CD,
                            VENDOR_NAME = a.VENDOR_NAME,
                            VENDOR_GRP = a.VENDOR_GRP,
                            STATUS_FLAG = a.STATUS_FLAG,
                            STATUS_DESC = a.STATUS_DESC,
                            IS_USERTAM = a.IS_USERTAM,
                            ACCOUNT_INFO = a.ACCOUNT_INFO,
                            INVOICE_NO = a.INVOICE_NO,
                            PV_DATE_DT = a.PV_DATE_DT,
                            PLANNING_PAYMENT_DT = a.PLANNING_PAYMENT_DT,
                            ACTUAL_PAYMENT_DT = a.ACTUAL_PAYMENT_DT,
                            AMOUNT = a.AMOUNT,
                            BUDGET_DESCRIPTION = a.BUDGET_DESCRIPTION
                        }
                    );

            return result;
        }
        
        public PVHeader GetPVHeader(int pid)
        {
            PVHeader result = Db.PVMonitoringRepository.GetPVHeader(pid);
            return result;
        }

        public PVHeader GetPVTHeader(int pid)
        {
            PVHeader result = Db.PVMonitoringRepository.GetPVTHeader(pid);
            return result;
        }

        public List<PVMonitoring> GetPVMonitoring(int pid)
        {
            List<PVMonitoring> result = new List<PVMonitoring>();

            result = Db.PVMonitoringRepository.GetPVMonitoring(pid);

            return result;
        }

        public List<TransactionType> GetTransactionType(string divId)
        {
            List<TransactionType> result = Db.PVMonitoringRepository.GetTransactionType(divId);

            return result;
        }

        public List<CodeConstant> GetCostCenterExcelTemp(string divId, int prodFlag)
        {
            List<CodeConstant> result = Db.PVMonitoringRepository.GetCostCenter(divId, prodFlag);

            if (result.Count() > 1)
            {
                foreach (CodeConstant x in result)
                {
                    x.Description = x.Code + ": " + x.Description;
                }
            }

            return result;
        }

        public List<ComboEx> GetVendorByInvoice(string processId, bool isAll)
        {
            List<ComboEx> result = Db.PVMonitoringRepository.GetVendorByInvoice(processId, isAll);

            return result;
        }

        public List<WBSStructure> GetBugdetNo(int div, string year, string tc)
        {            
            List<WBSStructure> result = Db.PVMonitoringRepository.GetBudgetNo(div, year, tc);

            return result;
        }
        /*
        public string GetBankAccount(string processId, string userType)
        {
            var result = Db.PVMonitoringRepository.GetBankAccount(processId, userType);

            return result;
        }

        public bool GetBankAccount(string bank_key, string bank_account, string vendor)
        {
            bool result = Db.PVMonitoringRepository.GetBankAccount(bank_key, bank_account, vendor);

            return result;
        }
        */
        public IList<Table> GetTableProperty(string tableName)
        {
            IList<Table> result = Db.PVMonitoringRepository.GetTableProperty(tableName);

            return result;
        }

        public IEnumerable<PVMonitoringAttachment> GetDocumentFileByReference(string processId, string userType)
        {           
            var result = Db.PVMonitoringRepository.GetDocumentFileByReference(processId, userType);
            return result;
        }

        public IEnumerable<PVHDistribusionStatus> GetDistributionStatusByReference(string pvno)
        {
            var result = Db.PVMonitoringRepository.GetDistributionStatusByReference(pvno);
            return result;
        }

        public IEnumerable<PVMonitoringAttachment> GetDocumentFileByReference(string[] processId, string userType)
        {
            List<PVMonitoringAttachment> result = new List<PVMonitoringAttachment>();

            foreach (string z in processId)
            {
                var x = Db.PVMonitoringRepository.GetDocumentFileByReference(z, userType);

                foreach (PVMonitoringAttachment v in x)
                {
                    result.Add(v);
                }
            }

            return result;
        }

        public IEnumerable<PVMonitoringAttachment> GetDocumentFileByPIDReff(string[] processId, string userType)
        {
            List<PVMonitoringAttachment> result = new List<PVMonitoringAttachment>();

            foreach (string z in processId)
            {
                string zo = "PID" + z + DateTime.Now.Year.ToString();
                var x = Db.PVMonitoringRepository.GetDocumentFileByPIDReff(zo, userType);

                foreach (PVMonitoringAttachment v in x)
                {
                    result.Add(v);
                }
            }

            return result;
        }

        /*
        public IEnumerable<PVMonitoringBankAccount> GetBankProcessByInvoice(string processId)
        {
            var result = Db.PVMonitoringRepository.GetBankProcessByInvoice(processId);
            return result;
        }
        */

        public List<ComboEx> GetStatusCmb(string val, string keyParam, string levelCode)
        {
            var result = Db.PVMonitoringRepository.GetStatusCmb(val, keyParam, levelCode);
            return result;
        }

        public string GetStatusCmb(string val, string keyParam, bool singleString)
        {
            var result = Db.PVMonitoringRepository.GetStatusCmb(val, keyParam, singleString);
            return result;
        }

        public string GetDivId(string user)
        {
            var result = Db.PVMonitoringRepository.GetDivId(user);
            return result;
        }

        public HashSet<string> GetUploadValidation(string[] pid)
        {
            HashSet<string> result = new HashSet<string>();            

            foreach (string x in pid)
            {
                int xl = int.Parse(x);
                var output = Db.PVMonitoringRepository.GetUploadValidation(xl);

                if (output.Count > 0)
                {
                    foreach (string xx in output)
                    {
                        result.Add(xx);

                        Db.PVMonitoringRepository.Log("ERR", xx, "_GetUploadValidation", SessionManager.Current, "UPLOAD-PV", xl, "", "ERR");
                    }
                }
            }            

            return result;
        }

        public int GetPid(PVHeader pvHs, string user)
        {
            int pid=0;

            try
            {
                pid = Db.PVMonitoringRepository.GetPid(pvHs.pVMonitorings);

                if (pid == 0)
                {
                    pid = Db.PVMonitoringRepository.GetPid(0, "UPDATE-PV", user);
                }
            }
            catch (Exception)
            {
                throw;
            }

            return pid;
        }

        public void UpdateBudgetPVMonitoring(int pid, string status, string budgetNo)
        {
            Db.Connection.Open();
            var transact = Db.Connection.BeginTransaction();

            try
            {
                Db.PVMonitoringRepository.UpdateBudgetPVMonitoring(transact, pid, status, budgetNo);

                transact.Commit();
            }
            catch (Exception)
            {
                transact.Rollback();
                throw;
            }
            finally
            {
                Db.Connection.Close();
            }
        }

        public string ErrTxt(HashSet<string> data, string pathErrTax, string errName, string keyParam)
        {
            string result = "";
            string name = errName + "-" + Guid.NewGuid().ToString();
            string filename = name + ".txt";
            string fullpath = pathErrTax + filename;

            string[] files = Directory.GetFiles(pathErrTax);

            foreach (string file in files)
            {
                FileInfo fi = new FileInfo(file);
                if (fi.LastAccessTime < DateTime.Now.AddDays(-5))
                    fi.Delete();
            }

            File.WriteAllLines(fullpath, data);

            string msg = Db.PVMonitoringRepository.GetStatusCmb("1", keyParam, true);
            result = string.Format(msg, name, errName + ".txt");

            return result;
        }

        public bool SaveUploadListPVMonitoring(PVHeader pvHs, int pid, string pathErrTax, string user)
        {
            var result = false;
            string err_mesg = "";

            Db.Connection.Open();
            var transact = Db.Connection.BeginTransaction();

            try
            {
                foreach (PVMonitoring pvM in pvHs.pVMonitorings)
                {
                    err_mesg = Db.PVMonitoringRepository.InsertPVMonitoring(transact, pvM, user, pid);

                    if (err_mesg != null && err_mesg != "")
                    {
                        throw new Exception(err_mesg);
                    }
                }
                
                if (pvHs.pVMonitorings.Count > 0 && err_mesg == "")
                {
                    err_mesg = Db.PVMonitoringRepository.InsertPVH(transact, pvHs, user, pid);

                    if (err_mesg != null && err_mesg != "")
                    {
                        throw new Exception(err_mesg);
                    }

                    //Db.PVMonitoringRepository.InsertPVT(transact, user, pid);
                }
                
                transact.Commit();
                result = true;
            }
            catch (Exception)
            {
                result = false;
                transact.Rollback();
                throw;
            }
            finally
            {
                Db.Connection.Close();
            }

            return result;
        }

        public bool InsertPVHD(int pid, string user)
        {
            var result = false;

            Db.Connection.Open();
            var transact = Db.Connection.BeginTransaction();

            try
            {
                Db.PVMonitoringRepository.InsertPVHD(transact, user, pid);

                transact.Commit();
                result = true;
            }
            catch (Exception)
            {
                result = false;
                transact.Rollback();
                throw;
            }
            finally
            {
                Db.Connection.Close();
            }

            return result;
        }

        public List<PVMonitoring> GetPVMonitoring(string[] pids)
        {
            List<PVMonitoring> result = new List<PVMonitoring>();

            if (pids.Count() > 0)
            {
                List<PVMonitoring> xz;
                foreach (string x in pids)
                {
                    xz = new List<PVMonitoring>();
                    xz = Db.PVMonitoringRepository.GetPVMonitoring(int.Parse(x));
                    foreach (PVMonitoring xx in xz)
                    {
                        result.Add(xx);
                    }
                }
            }

            return result;
        }

        public bool NotProper(string[] pvNos, string reason, string user)
        {
            var result = false;

            Db.Connection.Open();
            var transact = Db.Connection.BeginTransaction();

            try
            {
                foreach (string pv in pvNos)
                {
                    Db.PVMonitoringRepository.NotProper(transact, pv, reason, user);
                }
                transact.Commit();
                result = true;
            }
            catch (Exception)
            {
                result = false;
                transact.Rollback();
                throw;
            }

            return result;
        }

        public bool DeleteUnSubmittedPVMonitoring(string[] pv_no, int clearDelete)
        {
            var result = false;

            Db.Connection.Open();
            var transact = Db.Connection.BeginTransaction();

            try
            {
                foreach (string pvno in pv_no)
                {
                    Db.PVMonitoringRepository.DeleteUnSubmittedPVMonitoring(transact, pvno, clearDelete, SessionManager.Current);
                }
                transact.Commit();
                result = true;
            }
            catch (Exception)
            {
                result = false;
                transact.Rollback();
                throw;
            }

            return result;
        }

        readonly static string DirtyFileNamePattern = "[\\+/\\\\\\#%&*{}/:<>?|\"-]";

        public static string SanitizeFilename(string insane)
        {
            string replacement = "_";

            Regex regEx = new Regex(DirtyFileNamePattern);
            return Regex.Replace(regEx.Replace(insane, replacement), @"\s+", " ");
        }

        public static string CleanFilename(string s)
        {
            string t = SanitizeFilename(s);

            if (t.Length > 75)
            {
                string ext = Path.GetExtension(t);
                t = t.Substring(0, 75 - ext.Length - 1) + ext;
            }
            return t;
        }

        protected FtpLogic ftp = new FtpLogic();

        public bool UploadAttachBank(HttpPostedFileBase file, string vendorCode, out string msgerror)
        {
            bool result = false;
            msgerror = "";

            int maxFileSize = int.Parse(ApplicationCacheManager.GetConfig<string>("MaxUploadSize")) * 1024 * 1024;

            var length = file.InputStream.Length;

            byte[] fileData = null;
            using (var binaryReader = new BinaryReader(file.InputStream))
            {
                fileData = binaryReader.ReadBytes(file.ContentLength);
            }

            if (length < maxFileSize)
            {
                var fileName = CleanFilename(Path.GetFileName(file.FileName));

                bool uploadSucceded = true;
                string errorMessage = null;
                string guidFilename = null;

                try
                {
                    string fileFolder = "Bank Attachment";

                    ftp.ftpUploadAttachBankBytes(fileFolder,
                                    fileName,
                                    fileData,
                                    ref uploadSucceded,
                                    ref guidFilename,
                                    ref errorMessage);

                    if (!uploadSucceded)
                    {
                        if (errorMessage != null)
                        {
                            msgerror = errorMessage;
                        }
                        else
                        {
                            msgerror = "File already exists in FTP";
                        }
                    }
                    else
                    {
                        bool succ = saveBankAttachmentInfo(vendorCode, fileFolder, fileName, guidFilename, SessionManager.Current);

                        if (succ)
                        {
                            result = true;
                        }
                    }
                }
                catch (Exception e)
                {
                    msgerror = e.Message;
                }
            }
            else
            {
                msgerror = "File size cannot exceed 10 MB";
            }

            return result;
        }

        public PVMonitoringAttachment Upload(HttpPostedFileBase file, string[] pid, string attachmentType, out string msgerror, bool byInternal)
        {
            msgerror = "";

            int maxFileSize = int.Parse(ApplicationCacheManager.GetConfig<string>("MaxUploadSize")) * 1024 * 1024;

            var length = file.InputStream.Length;

            byte[] fileData = null;
            using (var binaryReader = new BinaryReader(file.InputStream))
            {
                fileData = binaryReader.ReadBytes(file.ContentLength);
            }

            var doc = new PVMonitoringAttachment();

            if (length < maxFileSize)
            {
                try
                {
                    foreach (string proid in pid)
                    {
                        var fileName = CleanFilename(Path.GetFileName(file.FileName));
                        var year = DateTime.Now.Year.ToString();
                        var no = 'T' + proid;
                        var refNo = no + year;

                        var pv = Db.PVMonitoringRepository.GetPVByProcessId(proid);

                        if (pv == null)
                        {
                            pv = "PID" + proid;
                        }

                        var attachDesc = Db.PVMonitoringRepository.GetStatusCmb(attachmentType, "pv_attachment_type", true);

                        bool uploadSucceded = true;
                        string errorMessage = null;

                        ftp.ftpUploadBytes(year, "PV",
                                        pv,
                                        fileName,
                                        fileData,
                                        ref uploadSucceded,
                                        ref errorMessage,
                                        byInternal);

                        doc = new PVMonitoringAttachment
                        {
                            REFERENCE_NO = pv + year,
                            PV_NO = pv,
                            FILE_NAME = fileName,
                            ATTACH_CD = attachmentType,
                            ATTACH_DESC = attachDesc,
                            DIRECTORY = year + "/PV/" + pv,
                            CREATED_BY = SessionManager.Current,
                            CREATED_DT = DateTime.Now
                        };

                        if (!uploadSucceded)
                        {
                            if (errorMessage != null)
                            {
                                msgerror = errorMessage;
                            }
                            else
                            {
                                msgerror = "File already exists in FTP";
                            }
                        }
                        else
                        {
                            bool succ = saveAttachmentInfo(doc, SessionManager.UserType);
                        }
                    }
                }
                catch (Exception e)
                {
                    msgerror = e.Message;
                }
            }
            else
            {
                msgerror = "File size cannot exceed 10 MB";
            }

            return doc;
        }

        public PVMonitoringAttachment Upload(HttpPostedFileBase file, string pid, string attachmentType, out string msgerror, bool byInternal)
        {
            msgerror = "";

            int maxFileSize = int.Parse(ApplicationCacheManager.GetConfig<string>("MaxUploadSize")) * 1024 * 1024;

            var length = file.InputStream.Length;

            byte[] fileData = null;
            using (var binaryReader = new BinaryReader(file.InputStream))
            {
                fileData = binaryReader.ReadBytes(file.ContentLength);
            }

            var doc = new PVMonitoringAttachment();

            if (length < maxFileSize)
            {
                var fileName = CleanFilename(Path.GetFileName(file.FileName));
                var year = DateTime.Now.Year.ToString();
                var no = 'T' + pid;
                var refNo = no + year;
                var pv = Db.PVMonitoringRepository.GetPVByProcessId(pid);
                var attachDesc = Db.PVMonitoringRepository.GetStatusCmb(attachmentType, "pv_attachment_type", true);

                bool uploadSucceded = true;
                string errorMessage = null;

                try
                {
                    ftp.ftpUploadBytes(year, "PV",
                                    pv,
                                    fileName,
                                    fileData,
                                    ref uploadSucceded,
                                    ref errorMessage,
                                    byInternal);

                    doc = new PVMonitoringAttachment
                    {
                        REFERENCE_NO = pv + year,
                        PV_NO = pv,
                        FILE_NAME = fileName,
                        ATTACH_CD = attachmentType,
                        ATTACH_DESC = attachDesc,
                        DIRECTORY = year + "/PV/" + pv,
                        CREATED_BY = SessionManager.Current,
                        CREATED_DT = DateTime.Now
                    };

                    if (!uploadSucceded)
                    {
                        if (errorMessage != null)
                        {
                            msgerror = errorMessage;
                        }
                        else
                        {
                            msgerror = "File already exists in FTP";
                        }
                    }
                    else
                    { 
                        bool succ = saveAttachmentInfo(doc, SessionManager.UserType);
                    }
                }
                catch (Exception e)
                {
                    msgerror = e.Message;
                }
            }
            else
            {
                msgerror = "File size cannot exceed 10 MB";
            }

            return doc;
        }

        public byte[] Download(string reffNo, string fileName, string pv_no, out string msgerror)
        {
            msgerror = "";
            string errorMessage = "";

            byte[] result = null;

            var year = reffNo.ToString().Substring(reffNo.ToString().Length - 4, 4);                       

            try
            {
                result = ftp.ftpDownloadFile(year, "PV",
                                pv_no,
                                fileName,
                                ref errorMessage);

                if (result != null)
                {
                    if (errorMessage != null)
                    {
                        msgerror = errorMessage;
                    }
                }
            }
            catch (Exception e)
            {
                msgerror = e.Message;
            }

            return result;
        }

        public byte[] DownloadBankAttach(string guidFilename, string directory, out string msgerror)
        {
            msgerror = "";
            string errorMessage = "";

            byte[] result = null;

            try
            {
                result = ftp.ftpDownloadBankFile(
                                guidFilename,
                                directory,
                                ref errorMessage);

                if (result != null)
                {
                    if (errorMessage != null)
                    {
                        msgerror = errorMessage;
                    }
                }
            }
            catch (Exception e)
            {
                msgerror = e.Message;
            }

            return result;
        }

        public bool saveBankAttachmentInfo(string vendorCode, string folderFile, string filename, string guidFilename, string userType)
        {
            var result = false;

            Db.Connection.Open();
            var transact = Db.Connection.BeginTransaction();
            try
            {
                Db.PVMonitoringRepository.SaveBankAttachmentInfo(transact, vendorCode, folderFile, filename, guidFilename, userType);

                transact.Commit();
                result = true;
            }
            catch (Exception)
            {
                result = false;
                transact.Rollback();
                throw;
            }
            finally
            {
                Db.Connection.Close();
            }

            return result;
        }

        public bool saveAttachmentInfo(PVMonitoringAttachment attachment, string userType)
        {
            var result = false;

            Db.Connection.Open();
            var transact = Db.Connection.BeginTransaction();
            try
            {
                Db.PVMonitoringRepository.SaveAttachmentInfo(transact, attachment, userType);

                transact.Commit();
                result = true;
            }
            catch (Exception)
            {
                result = false;
                transact.Rollback();
                throw;
            }
            finally
            {
                Db.Connection.Close();
            }

            return result;
        }

        public bool VerifiedPIC(string[] pv_no, string pathErrTax, string user)
        {
            var result = false;            

            Db.Connection.Open();
            //try
            //{
                //HashSet <string> errTax = Db.PVMonitoringRepository.CompareViewGeneratorByPV(pv_no);
                //                 
                //if (errTax.Count() > 0)
                //{
                //    string err = ErrTxt(errTax, pathErrTax, "Error-Tax", "pv_errmegs_tax");
                //
                //    throw new Exception(err);
                //}
                //else
                //{
                    var transact = Db.Connection.BeginTransaction();
                    try
                    {
                        foreach (string inv in pv_no)
                        {
                            Db.PVMonitoringRepository.VerifiedPIC(transact, inv, user);
                        }
                        transact.Commit();
                        result = true;
                    }
                    catch (Exception)
                    {
                        result = false;
                        transact.Rollback();
                        throw;
                    }
                //}
            //}
            //catch (Exception)
            //{
            //    result = false;
            //    throw;
            //}

            return result;
        }

        public List<PVMonitoringAttachment> GetPVAttachment(int xx)
        {
            List<PVMonitoringAttachment> output = new List<PVMonitoringAttachment>();
                output = Db.PVMonitoringAttachmentRepository.GetPVAttachment(xx).ToList();
            return output;
        }

        public bool Submit(string[] pv_no, string[] pid, string pathErrTax, string user, string checkEFBFlag)
        {
            string err_mesg;
            bool result = false;

            Db.Connection.Open();
            try
            {
                List<string> pvno = new List<string>(pv_no);                
                var x = Db.PVMonitoringRepository.CheckBeforeSubmit(pv_no);
                //var z = Db.PVMonitoringRepository.CompareViewGeneratorByPV(pv_no);

                HashSet<string> errTax = x;//new HashSet<string>(x.Union(z));

                if (errTax.Count() > 0)
                {
                    err_mesg = ErrTxt(errTax, pathErrTax, "Error-Submit", "pv_errmegs_submit");

                    throw new Exception(err_mesg);
                }
                else
                {
                    var resultApi = new PVOutput();
                    string BaseUri = GetStatusCmb("1", "pv_url_elvis", true);

                    List<PVHeader> pvH = new List<PVHeader>();

                    foreach (string xx in pid)
                    {
                        PVHeader au = Db.PVMonitoringRepository.GetPVHeader(int.Parse(xx));
                                 au.IS_EBILLING = "Y";
                                 au.pVMonitorings = new List<PVMonitoring>();
                                 au.pvAmounts = new List<PVAmount>();
                                 au.pvAttachments = new List<PVMonitoringAttachment>();

                        if (au != null)
                        {
                            au.pVMonitorings = Db.PVMonitoringRepository.GetPVD(int.Parse(xx)).ToList();

                            List<string> pvnoo = au.pVMonitorings.Where(c => c.TAX_NO == null).Select(z => z.PV_NO).ToList();

                            if (pvnoo != null)
                            {
                                foreach (string aa in pvnoo)
                                {
                                    pvno.Remove(aa);
                                }
                            }

                            au.pvAmounts = Db.PVMonitoringRepository.GetPVAmount(int.Parse(xx)).ToList();
                            au.pvAttachments = GetPVAttachment(int.Parse(xx)).ToList();
                        }

                        pvH.Add(au);
                    }

                    string[] xxx = pvno.ToArray();
                    if (xxx.Count() > 0 && checkEFBFlag == "1")
                    {
                        HashSet<string> z = Db.PVMonitoringRepository.CompareViewGeneratorByPV(pv_no);

                        if (z.Count() > 0)
                        {
                            err_mesg = ErrTxt(z, pathErrTax, "Error-Submit", "pv_errmegs_submit");
                            throw new Exception(err_mesg);
                        }
                    }

                    var transact = Db.Connection.BeginTransaction();
                    try
                    {
                        foreach (PVHeader xx in pvH)
                        {
                            if (xx.pVMonitorings.Count() > 0
                                && xx.pvAmounts.Count() > 0
                                && xx.pvAttachments.Count() > 0
                            )
                            { 
                                using (var client = new HttpClient())
                                {
                                    client.BaseAddress = new Uri(BaseUri);
                                    client.DefaultRequestHeaders.Accept.Clear();
                                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                                    var uri = string.Format(client.BaseAddress + "api/PVEbilling/PostPVH");
                                    var response = client.PostAsJsonAsync(uri, xx).Result;

                                    if (response.IsSuccessStatusCode)
                                    {
                                        resultApi = response.Content.ReadAsAsync<PVOutput>().Result;
                                    }
                                }

                                if (resultApi.STATUS_PV == "S")
                                {
                                    Db.PVMonitoringRepository.Submit(transact, xx.PV_NO.ToString(), user);
                                }
                                else
                                {
                                    throw new Exception(resultApi.ERR_MESSAGE);
                                }
                            }
                        }

                        transact.Commit();
                        result = true;
                    }
                    catch (Exception ex)
                    {
                        result = false;
                        transact.Rollback();
                        throw;
                    }
                }
            }
            catch (Exception ex)
            {
                result = false;
                throw;
            }

            return result;
        }

        public bool RegisterBankAccount(string user, string pid, string vendor, string bank, string bankAccount, string beneficiaries, out string msgerror)
        {
            msgerror = "";
            var result = true;

            if (pid != null && bank !=null && bankAccount != null)
            {
                Db.Connection.Open();

                var transact = Db.Connection.BeginTransaction();

                try
                {
                    Db.PVMonitoringRepository.RequestBankAccount(transact, user, pid, vendor, bank, bankAccount, beneficiaries);

                    transact.Commit();
                }
                catch (Exception)
                {
                    result = false;
                    transact.Rollback();
                    throw;
                }
                finally {
                    Db.Connection.Close();
                }
            }

            return result;
        }

        public bool ReAttachment(IEnumerable<PVMonitoringAttachment> docs, string reffNoOld)
        {
            var result = true;

            Db.Connection.Open();
            var transact = Db.Connection.BeginTransaction();

            try
            {
                if (docs.Count() > 0)
                {
                    bool reuploaded = false;
                    string reffNoNew = "", filename ="", year = "", newFdlr = "", oldFdlr ="";

                    foreach (PVMonitoringAttachment doc in docs)
                    {
                        if (doc.STATUS == "Delete")
                        {
                            DeleteFile(reffNoOld, doc.FILE_NAME);

                            Db.PVMonitoringAttachmentRepository.Remove(transact, reffNoOld, doc.REF_SEQ_NO);
                        }
                        else
                        {
                            filename = doc.FILE_NAME;
                            reffNoNew = doc.REFERENCE_NO;

                            if (reffNoNew != "")
                            {
                                year = reffNoNew.ToString().Substring(reffNoNew.ToString().Length - 4, 4);
                                int x = reffNoNew.ToString().Length - 4;
                                newFdlr = reffNoNew.ToString().Substring(0, x);

                                int z = reffNoOld.ToString().Length - 4;
                                oldFdlr = reffNoOld.ToString().Substring(0, z);

                                string pathNew = year + "/PV/" + newFdlr + "/" + filename;
                                string pathOld = year + "/PV/" + oldFdlr + "/" + filename;

                                reuploaded = RenameFolderAttach(pathOld, pathNew);//, filename);
                                //reuploaded = RenameFolderAttach(reffNoNew, reffNoOld, filename);
                            }
                        }
                    }

                    if(reuploaded)
                    {
                        string direct = year + "/PV/" + oldFdlr;
                        string directNew = year + "/PV/" + newFdlr;

                        RemoveDir(direct);
                        Db.PVMonitoringAttachmentRepository.UpdatePVAttachment(transact, reffNoOld, reffNoNew, directNew);
                    }

                    transact.Commit();
                }
            }
            catch (Exception ex)
            {
                result = false;
                transact.Rollback();
                throw;
            }
            finally
            {
                Db.Connection.Close();
            }

            return result;
        }

        public bool SaveUpload(string pid, string bank_key, IEnumerable<PVMonitoringAttachment> docs, IEnumerable<PVMonitoring> pvDtl, out string err_mesg, string user, string pathErrTax)
        {
            err_mesg = "";
            var result = true;

            try
            { 
                HashSet<string> x = Db.PVMonitoringRepository.ValDuplicateTaxNo(pvDtl.ToList());

                if (x.First() != "")
                {
                    err_mesg = ErrTxt(x, pathErrTax, "Error-Tax", "pv_errmegs_tax");
                    throw new Exception(err_mesg);
                }
                else
                {
                    if (docs != null)
                    {
                        if (docs.Count() > 0)
                        {
                            Db.Connection.Open();
                            var transact = Db.Connection.BeginTransaction();

                            try
                            {
                                HashSet<string> xo = Db.PVMonitoringRepository.UpdatePVMonitoring(transact, pvDtl, pid, bank_key, user);
                                
                                if (xo.Count() > 0)
                                {
                                    err_mesg = xo.First();
                                    throw new Exception(err_mesg);
                                }
                                else
                                {
                                    foreach (PVMonitoringAttachment doc in docs)
                                    {
                                        if (doc.STATUS == "Delete")
                                        {
                                            DeleteFile(doc.REFERENCE_NO, doc.FILE_NAME);

                                            Db.PVMonitoringAttachmentRepository.Remove(transact, doc.REFERENCE_NO, doc.REF_SEQ_NO);
                                        }
                                    }
                                }

                                transact.Commit();
                            }
                            catch (Exception e)
                            {
                                result = false;
                                transact.Rollback();
                                throw;
                            }
                            finally
                            {
                                Db.Connection.Close();
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                result = false;
                throw;
            }

            return result;
        }

        public bool DeleteFile(string id, string fileName)
        {
            string year = id.ToString().Substring(id.ToString().Length - 4, 4);
            int x = id.ToString().Length - 4;
            string folder = id.ToString().Substring(0, x);
            string server = ApplicationCacheManager.GetConfig<string>("FilePVMonitoringServer");
            string ftpUserName = ApplicationCacheManager.GetConfig<string>("FilePVMonitoringUserName");
            string ftpPassWord = ApplicationCacheManager.GetConfig<string>("FilePVMonitoringPassword");

            string url = "ftp://" + server + "/" + year + "/PV/" + folder + "/" + fileName.ToString();

            Uri serverUri = new Uri(url);
            if (serverUri.Scheme != Uri.UriSchemeFtp)
            {
                return false;
            }
            FtpWebRequest reqFTP = (FtpWebRequest)WebRequest.Create(serverUri);

            reqFTP.Credentials = new NetworkCredential(ftpUserName, ftpPassWord);
            reqFTP.KeepAlive = false;
            reqFTP.Proxy = new WebProxy();
            reqFTP.Method = WebRequestMethods.Ftp.DeleteFile;

            string result = String.Empty;
            FtpWebResponse response = (FtpWebResponse)reqFTP.GetResponse();
            long size = response.ContentLength;
            Stream datastream = response.GetResponseStream();
            StreamReader sr = new StreamReader(datastream);
            result = sr.ReadToEnd();
            sr.Close();
            datastream.Close();
            response.Close();

            return true;
        }

        public bool RemoveDir(string url)
        {
            try
            {
                string server = ApplicationCacheManager.GetConfig<string>("FilePVMonitoringServer");
                string ftpUserName = ApplicationCacheManager.GetConfig<string>("FilePVMonitoringUserName");
                string ftpPassWord = ApplicationCacheManager.GetConfig<string>("FilePVMonitoringPassword");

                url = "ftp://" + server + "/" + url;
                Uri serverUri = new Uri(url);
                if (serverUri.Scheme != Uri.UriSchemeFtp)
                {
                    return false;
                }
                FtpWebRequest reqFTP = (FtpWebRequest)WebRequest.Create(serverUri);

                reqFTP.Credentials = new NetworkCredential(ftpUserName, ftpPassWord);
                reqFTP.KeepAlive = false;
                reqFTP.Proxy = new WebProxy();
                reqFTP.Method = WebRequestMethods.Ftp.RemoveDirectory;

                string result = String.Empty;
                FtpWebResponse response = (FtpWebResponse)reqFTP.GetResponse();
                long size = response.ContentLength;
                Stream datastream = response.GetResponseStream();
                StreamReader sr = new StreamReader(datastream);
                result = sr.ReadToEnd();
                sr.Close();
                datastream.Close();
                response.Close();
                return true;
            }
            catch (Exception ex)
            {
                //
            }

            return false;
        }

        /*
        public bool RenameFolderAttach(string newFolder, string oldFolder, string filename)
        {
            bool result = false;

            string server = ApplicationCacheManager.GetConfig<string>("FilePVMonitoringServer");
            string ftpUserName = ApplicationCacheManager.GetConfig<string>("FilePVMonitoringUserName");
            string ftpPassWord = ApplicationCacheManager.GetConfig<string>("FilePVMonitoringPassword");

            string year = newFolder.ToString().Substring(newFolder.ToString().Length - 4, 4);
            int x = newFolder.ToString().Length - 4;
            string newFdlr = newFolder.ToString().Substring(0, x);

            int z = oldFolder.ToString().Length - 4;
            string oldFdlr = oldFolder.ToString().Substring(0, x);

            bool uploaded = false;
            string errMsg=null;

            try
            {
                byte[] resbyte = ftp.ftpDownloadFile(year, "PV", oldFdlr, filename, ref errMsg);

                if (string.IsNullOrEmpty(errMsg))
                {
                    ftp.ftpUploadBytes(year, "PV", newFdlr, filename, resbyte, ref uploaded, ref errMsg, true);
                  
                    if (uploaded)
                    {
                        DeleteFile(oldFolder, filename);
                    }
                    
                }

                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                throw;
            }

            return result;
        }
        */
        
        public bool RenameFolderAttach(string src, string des)
        {
            string result = "";
            string server = ApplicationCacheManager.GetConfig<string>("FilePVMonitoringServer");
            string ftpUserName = ApplicationCacheManager.GetConfig<string>("FilePVMonitoringUserName");
            string ftpPassWord = ApplicationCacheManager.GetConfig<string>("FilePVMonitoringPassword");

            FtpWebRequest reqFTP = null;

            FtpWorker fw = new FtpWorker();
            string reqHead = "ftp://" + server + "/";
            string md = "";
            int lastSlash = des.LastIndexOf("/");
            if (lastSlash > 0)
            {
                md = reqHead + des.Substring(0, lastSlash);
                fw.MakeDirs(md);
            }

            reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + server + "/" + src));
            
            reqFTP.UseBinary = true;
            reqFTP.Credentials = new NetworkCredential(ftpUserName, ftpPassWord);
            reqFTP.Method = WebRequestMethods.Ftp.Rename;
            reqFTP.KeepAlive = false;
            reqFTP.UsePassive = false;
            reqFTP.Proxy = new WebProxy();

            string target = "";
            string[] a = src.Split('/');

            for (int i = 0; i < a.Length - 1; i++)
            {
                target += "../";
            }
            target = target + des;
            
            reqFTP.RenameTo = target;
            FtpWebResponse response = (FtpWebResponse)reqFTP.GetResponse();
            Stream datastream = response.GetResponseStream();
            StreamReader sr = new StreamReader(datastream);
            result = sr.ReadToEnd();
            sr.Close();
            response.Close();

            return true;
        }

        public byte[] printCover(
                       string imagePath,
                       string image,
                       string templatePath,
                       string companyName,
                       string tpath,
                       string user,
                       string bankAccount,
                       List<PVHeader> listData,
                       string userType)
        {
            byte[] xa=null;

            try
            {
                Ini pvcover = new Ini(templatePath + "\\PVCoverTemplate.ini");

                string tempelCover = "";
                if (userType == "Internal")
                {
                    tempelCover = File.ReadAllText(Path.Combine(templatePath, "PVCoverTemplateNew[Internal].html"));
                }
                else
                {
                    tempelCover = File.ReadAllText(Path.Combine(templatePath, "PVCoverTemplateNew.html"));
                }

                string tempelInvoice = File.ReadAllText(Path.Combine(templatePath, "PVCoverTemplateInvoice.html"));
                string tempelTTD = File.ReadAllText(Path.Combine(templatePath, "PVCoverTemplateTTD.html"));
                string tempelUnsettled = File.ReadAllText(Path.Combine(templatePath, "PVCoverTemplateUnsettled.html"));
                Templet tCover = new Templet(tempelCover);
                Templet tInvoice = new Templet(tempelInvoice);
                Templet tUnsettled = new Templet(tempelUnsettled);

                Document doc = new Document(PageSize.A4, 50, 50, 50, 50);
                string _FileName = "PV_Cover_" + DateTime.Now.ToString("ddMMyyyy_HHmmss");

                string contentTemp;
                HTMLWorker hw = new HTMLWorker(doc);

                MemoryStream ms = new MemoryStream();
                PdfWriter PDFWriter = PdfWriter.GetInstance(doc, ms);

                PDFWriter.ViewerPreferences = PdfWriter.PageModeUseOutlines;
                doc.Open();

                StringBuilder amounts = new StringBuilder("");

                for (int i = 0; i < listData.Count; i++)
                {
                    PVHeader data = listData[i];
                    tCover.Init(tempelCover);
                    tInvoice.Init(tempelInvoice);
                    tUnsettled.Init(tempelUnsettled);

                    Filler fCover = tCover.Mark("COMPANY_NAME,ISSUING_DIVISION,PV_DATE,PV_NO,"
                        + "VENDOR_NAME,TRANSACTION_TYPE,PAYMENT_METHOD,"
                        + "BUDGET_NO_DESC,DESC,PV_TYPE_NAME,AMOUNT,INVOICE,UNSETTLED,TTD,USERNAME,PRINT_DATE,BANK_ACCOUNT");

                    Filler fInvoice = tInvoice.Mark("rows", "INVOICE_NO,CURRENCY,"
                        + "val,TAX_NO,INVOICE_DATE,valTax");

                    Image img = Image.GetInstance(Path.Combine(imagePath, Path.GetFileName(image)));
                    img.ScaleToFit(pvcover["logo.width"].Int(), pvcover["logo.height"].Int());
                    System.Globalization.TextInfo myTI = new System.Globalization.CultureInfo("en-US", false).TextInfo;

                    string username = Db.PVMonitoringRepository.GetFullName(user);
                    username = myTI.ToTitleCase(username.ToLower());

                    amounts.Clear();

                    List<PVAmount> la = Db.PVMonitoringRepository.GetPVAmount(data.PV_NO.Int(), data.PV_YEAR.Int());

                    if (la != null)
                        foreach (PVAmount amt in la)
                        {
                            if (amt.TOTAL_AMOUNT != 0 && (amt.TOTAL_AMOUNT > 0))
                                amounts.AppendFormat(
                                    "<tr><td>{0}</td><td style='font-size: 16pt;' align='right'>{1}</td></tr>"
                                    , amt.CURRENCY_CD
                                    , Eval_Curr(amt.CURRENCY_CD, amt.TOTAL_AMOUNT));
                        }

                    List<PVCoverData> listInvoiceData = null;

                    listInvoiceData = Db.PVMonitoringRepository.getInvoiceValue(data.PV_NO, data.PV_YEAR);
                    StringBuilder amtStr = new StringBuilder("");
                    
                    if (listInvoiceData != null)
                        foreach (var invoice in listInvoiceData)
                        {
                            string val = "";
                            string valTax = "";

                            if (invoice.AMOUNT != null)
                            {
                                if (!invoice.CURRENCY.Equals("IDR") && !invoice.CURRENCY.Equals("JPY") && !invoice.CURRENCY.Equals("USD"))
                                {
                                    val = string.Format("{0:N2}", invoice.AMOUNT.Value);
                                    valTax = string.Format("{0:N2}", invoice.AMOUNT * invoice.TAX_AMOUNT);
                                }
                                else
                                {
                                    val = string.Format("{0:N0}", invoice.AMOUNT.Value);
                                    valTax = string.Format("{0:N0}", invoice.AMOUNT * invoice.TAX_AMOUNT);
                                }
                            }
                            else
                            {
                                val = " ";
                                valTax = " ";
                            }
                            fInvoice.Add(
                                (invoice.INVOICE_NO == "" ? " " : invoice.INVOICE_NO),
                                (invoice.CURRENCY == "" ? " " : invoice.CURRENCY),
                                val,
                                (invoice.TAX_NO == "" ? " " : invoice.TAX_NO),
                                (invoice.INVOICE_DATE == null ? " " : invoice.INVOICE_DATE.Value.ToString("dd/MM/yyyy")),
                                valTax);
                        }

                    if (listInvoiceData != null && listInvoiceData.Count > 0)
                    {
                        amtStr.Append(tInvoice.Get());
                    }

                    string barcodeFormat = "{0}\t{1}\r";
                    if (!pvcover["barcode.format"].isEmpty())
                    {
                        barcodeFormat = pvcover["barcode.format"];
                        barcodeFormat = barcodeFormat.Trim().Replace("\\t", "\t").Replace("\\r", "\r").Replace("\\n", "\n");
                    }

                    Barcode128 bar = new Barcode128();

                    bar.CodeType = iTextSharp.text.pdf.Barcode.CODE128;
                    bar.ChecksumText = false;
                    bar.GenerateChecksum = false;
                    bar.StartStopText = false;
                    bar.Code = string.Format(barcodeFormat, data.PV_NO, data.PV_YEAR);

                    Image imgBarCode = null;

                    imgBarCode = bar.CreateImageWithBarcode(PDFWriter.DirectContent, null, null);

                    StyleSheet sh = new StyleSheet();
                    string budgetNoDesc = "";

                    if (!data.BUDGET.isEmpty()
                        && data.TRANSACTION_CD.Int() == Db.PVMonitoringRepository.GetNum("TRANS_TYPE/ACCR_YE", 219))
                    {
                        List<WBSStructure> wbs = Db.PVMonitoringRepository.WbsNumbers(data.BUDGET);
                        string desc = "", wbsno = "";
                        if (wbs != null && wbs.Count > 0)
                        {
                            desc = wbs[0].Description;
                            wbsno = wbs[0].WbsNumber;
                        }
                        
                        data.BUDGET_NO_FORMAT = (data.BUDGET != null) ? wbsno + " / " + desc + " / " : "";

                        budgetNoDesc = data.BUDGET_NO_FORMAT + "  <br>" + data.BUDGET;
                    }

                    else if (!data.BUDGET.isEmpty())
                    {
                        if (data.BUDGET_DESCRIPTION.isEmpty())
                        {
                            List<WBSStructure> wbs = Db.PVMonitoringRepository.GetBudgetNo(int.Parse(data.DIVISION_ID), data.PV_YEAR, data.TRANSACTION_CD.ToString());
                            string desc = "", wbsno = "";

                            if (wbs != null && wbs.Count > 0)
                            {
                                var xb = wbs.Where(o => o.WbsNumber == data.BUDGET).SingleOrDefault();

                                if (xb != null)
                                {
                                    desc = xb.Description;
                                    wbsno = xb.WbsNumber;
                                }

                                if (desc != "" && desc != null)
                                {
                                    budgetNoDesc = desc.Replace(": ", "-");
                                }
                                else
                                {
                                    budgetNoDesc = data.BUDGET;
                                }
                            }
                        }
                        else
                        {
                            budgetNoDesc = data.BUDGET + "-" + data.BUDGET_DESCRIPTION;
                        }
                    }

                    string ttdStr = tempelTTD;

                    List<CodeConstant> tranSkipApproval = Db.PVMonitoringRepository.GetTransactionTypesSkipApproval();

                    CodeConstant codeSkipApp = (tranSkipApproval != null && tranSkipApproval.Count > 0)
                        ? tranSkipApproval[0]
                        : new CodeConstant() { Code = "", Description = "" };

                    string ttd = "";
                    if (tranSkipApproval.Count() > 0)
                    {
                        string[] strSplit = codeSkipApp.Description.Split(',');
                        ttd = (strSplit.Where(a => a.Equals(data.TRANSACTION_CD)).Any()) ? "" : ttdStr;
                    }

                    string pvTypeName = "";
                    if (data.PV_TYPE_NAME != null && !data.PV_TYPE_NAME.Equals(""))
                    {
                        pvTypeName = data.PV_TYPE_NAME;
                    }
                    else
                    {
                        List<CodeConstant> pvTypeList = Db.PVMonitoringRepository.GetPVTypes(0);
                        for (i = 0; i < pvTypeList.Count; i++)
                        {
                            CodeConstant codeCs = pvTypeList[i];
                            if (codeCs.Code.Equals(data.PV_TYPE_CD.ToString()))
                            {
                                pvTypeName = codeCs.Description;
                                break;
                            }
                        }
                    }

                    List<OutstandingSuspense> los = new List<OutstandingSuspense>();

                    string loses = "";
                    if (los.Any())
                    {
                        Filler fSUSPENSE_COUNT = tUnsettled.Mark("TITLE", "SUS");
                        fSUSPENSE_COUNT.Add(los.Count.str());

                        Filler fLINE = tUnsettled.Mark("LINE", "ROW");
                        if (los.Count <= 5)
                        {
                            foreach (var osu in los)
                            {
                                fLINE.Add(string.Format("{0} Rp. {1} - {2} - {3}{4}"
                                        , osu.DOC_NO
                                        , osu.TOTAL_AMOUNT.fmt()
                                        , osu.VENDOR_NAME
                                        , ((osu.DAYS_OUTSTANDING > 0) ? osu.DAYS_OUTSTANDING.str() + " day" : "")
                                        , (osu.DAYS_OUTSTANDING > 1) ? "s" : "")
                                        );
                            }

                        }
                        else
                        {
                            fLINE.Add(string.Format("TOTAL Rp. {0}", los.Sum(a => a.TOTAL_AMOUNT).fmt()));

                            /// mapped out to array 5x5 
                            string[][] lono = new string[][]
                            {
                                new string[] { "", "", "", "", "" },
                                new string[] { "", "", "", "", "" },
                                new string[] { "", "", "", "", "" },
                                new string[] { "", "", "", "", "" },
                                new string[] { "", "", "", "", "" }
                            };
                            int losj = (los.Count > 25) ? 25 : los.Count;
                            for (int losi = 0; losi < losj; losi++)
                            {
                                lono[losi / 5][losi % 5] = los[losi].DOC_NO.str();
                            }
                            if (los.Count > 24)
                            {
                                lono[4][4] = "...NOT SHOWN";
                            }
                            for (int mx = 0; mx < 5; mx++)
                            {
                                fLINE.Add(string.Format("{0} &nbsp; {1} &nbsp; {2} &nbsp; {3} &nbsp; {4} &nbsp;"
                                    , lono[mx][0], lono[mx][1], lono[mx][2], lono[mx][3], lono[mx][4]));
                            }

                        }

                        loses = tUnsettled.Get();
                    }
                    else
                    {
                        loses = "";
                    }
                    
                    fCover.Set(
                            companyName,
                            data.DIVISION_NAME,
                            data.PV_DATE.Value.ToString("d/M/yyyy"),
                            data.PV_NO,
                            string.Format("{0}: {1}", data.VENDOR_CD, data.VENDOR_NAME),
                            data.TRANSACTION_DESC,
                            data.PAYMENT_METHOD,
                            budgetNoDesc,
                            data.DESCRIPTION,
                            pvTypeName, 
                            amounts.ToString(), amtStr.ToString(),
                            loses,
                            ttd,
                            username,
                            DateTime.Now.ToString("d MMMM yyyy"),
                            bankAccount);

                    contentTemp = tCover.Get();
                    List<IElement> elements = HTMLWorker.ParseToList(new StringReader(contentTemp), sh);

                    string padding = pvcover["MF.Padding"];
                    PrintHelper.padMF = padding.Int(PrintHelper.padMF);
                    string mfCols = pvcover["MF.Columns"];
                    string mfRows = pvcover["MF.Rows"];
                    string mfTable = pvcover["MF.Table"];

                    PdfPTable digits = new PdfPTable(10);
                    for (int ix = 0; ix < 10; ix++)
                    {
                        PdfPCell x10 = new PdfPCell(new Paragraph("  "));
                        x10.BorderWidthTop = 1;
                        x10.BorderWidthRight = 1;
                        x10.BorderWidthBottom = 1;
                        x10.BorderWidthLeft = (ix > 0) ? 0 : 1;
                        x10.PaddingTop = PrintHelper.padMF;
                        x10.PaddingBottom = PrintHelper.padMF;
                        x10.PaddingLeft = PrintHelper.padMF;
                        x10.PaddingRight = PrintHelper.padMF;
                        digits.AddCell(x10);
                    }
                    digits.WidthPercentage = 100;

                    float[] wtDigits = new float[] { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
                    PrintHelper.WidthsParse(pvcover, "MF.Digits", ref wtDigits);
                    digits.SetWidths(wtDigits);

                    float[] wtRow = new float[] { 17, 20 };
                    PrintHelper.WidthsParse(pvcover, "MF.Rows", ref wtRow);

                    float[] wtTable = new float[] { 5, 6 };
                    PrintHelper.WidthsParse(pvcover, "MF.Table", ref wtTable);

                    PdfPTable pRow = new PdfPTable(2);

                    PrintHelper.coverMFRow(pRow, new PdfPCell(digits), "SAP Invoice Document No");
                    PrintHelper.coverMFRow(pRow, new PdfPCell(digits), "SAP Payment Document No");
                    PrintHelper.coverMFRow(pRow, new PdfPCell(new Paragraph("")), "Payment Date");

                    pRow.SetWidths(wtRow);

                    pRow.WidthPercentage = 100;

                    PdfPTable tMF = new PdfPTable(2);
                    PdfPCell mf0 = new PdfPCell(imgBarCode);
                    mf0.BorderWidth = 0;
                    tMF.AddCell(mf0);
                    PdfPCell mf1 = new PdfPCell(pRow);
                    tMF.AddCell(mf1);

                    tMF.SetWidths(wtTable);
                    tMF.WidthPercentage = 100;
                    tMF.SpacingBefore = 0f;
                    tMF.SpacingAfter = 10f;
                    doc.Add(tMF);

                    int logoTop = pvcover["logo.top"].Int(680);
                    int logoLeft = pvcover["logo.left"].Int(40);
                    
                    int elmnt = 1;

                    PdfPTable tableHeader = elements[elmnt++] as PdfPTable;

                    PdfPCell[] hCells = tableHeader.Rows[0].GetCells();
                    hCells[0].AddElement(img);

                    tableHeader.SetWidths(new float[] { 1f, 7f });
                    doc.Add(tableHeader);

                    Paragraph par = elements[elmnt++] as Paragraph;
                    par.SpacingBefore = 10f;
                    par.SpacingAfter = 5f;

                    doc.Add(par);

                    PdfPTable tableDetail = elements[elmnt++] as PdfPTable;

                    tableDetail.SetWidths(new float[] { 4f, 7f, 2f, 4f });

                    for (int j = 0; j < tableDetail.Rows.Count; j++)
                    {
                        PdfPCell[] cells = tableDetail.Rows[j].GetCells();

                        for (int k = 0; k < cells.Count(); k++)
                        {
                            PdfPCell cell = cells[k];
                            if (cell != null)
                            {
                                cell.NoWrap = true;

                                if (k == 1 && j == 1)
                                {
                                    // cell.PaddingTop = -17f;
                                }
                                else if (j != 1 && k % 2 != 0)
                                {
                                    cell.PaddingTop = -4f;
                                    cell.PaddingBottom = 3f;
                                }
                                else if (j != 1 && k % 2 == 0)
                                {
                                    cell.PaddingTop = -2f;
                                    cell.PaddingBottom = 4f;
                                }
                            }
                        }
                    }

                    PdfPTable wraper = new PdfPTable(1);
                    PdfPCell cellWrap = new PdfPCell(tableDetail);
                    cellWrap.BorderWidth = 1f;
                    wraper.AddCell(cellWrap);
                    wraper.WidthPercentage = 100f;

                    doc.Add(wraper);

                    doc.Add(new Paragraph("\n"));

                    PdfPTable tableAmount = elements[elmnt++] as PdfPTable;
                    tableAmount.SetWidths(new float[] { 1f, 4f });
                    tableAmount.WidthPercentage = 50f;

                    for (int j = 0; j < tableAmount.Rows.Count; j++)
                    {
                        PdfPCell[] cells = tableAmount.Rows[j].GetCells();

                        for (int k = 0; k < cells.Count(); k++)
                        {
                            PdfPCell cell = cells[k];
                            if (cell != null)
                            {
                                if (k % 2 != 0)
                                {
                                    cell.PaddingTop = -7f;
                                }
                                else
                                {
                                    cell.PaddingTop = -4f;
                                }
                            }
                        }
                    }
                    doc.Add(tableAmount);

                    if (listInvoiceData != null && listInvoiceData.Count > 0)
                    {
                        doc.Add(elements[elmnt++] as PdfPTable);
                        PdfPTable tableInvoice = elements[elmnt++] as PdfPTable;
                        
                        float[] aic = new float[] { 4f, 2f, 3f, 4f, 3f, 3f };
                        PrintHelper.WidthsParse(pvcover, "Invoice.Columns", ref aic);

                        tableInvoice.SetWidths(aic);

                        for (int j = 0; j < tableInvoice.Rows.Count; j++)
                        {
                            PdfPCell[] cells = tableInvoice.Rows[j].GetCells();

                            for (int k = 0; k < cells.Count(); k++)
                            {
                                PdfPCell cell = cells[k];
                                if (cell != null)
                                {
                                    if (j == 0)
                                        cell.PaddingTop = -3f;
                                    else
                                        cell.PaddingTop = -2f;
                                }
                            }
                        }

                        doc.Add(tableInvoice);
                        elmnt++;
                    }
                    if (los.Any())
                    {
                        PdfPTable tableLUS = elements[elmnt++] as PdfPTable;
                        doc.Add(tableLUS);
                    }

                    if (!ttd.isEmpty() && userType=="Internal")
                    {
                        PdfPTable tableTtd = elements[elmnt++] as PdfPTable;
                        tableTtd.TotalWidth = 540f;
                        tableTtd.SetWidths(new float[] { 8f, 98f });
                        tableTtd.CalculateHeights();
                    
                        tableTtd.WriteSelectedRows(pvcover["ttd.rowStart"].Int(0), pvcover["ttd.rowEnd"].Int(-1),
                            (float)pvcover["ttd.xPos"].Num(4d), (float)pvcover["ttd.yPos"].Num(210d), PDFWriter.DirectContent);
                    }

                    PdfPTable tableFooter = elements[elmnt++] as PdfPTable;
                    tableFooter.TotalWidth = 300f;
                    float[] tfcols = new float[] { 2f, 7f };
                    PrintHelper.WidthsParse(pvcover, "tableFooter.Columns", ref tfcols);
                    tableFooter.SetWidths(tfcols);
                    tableFooter.WriteSelectedRows(pvcover["tableFooter.rowStart"].Int(0), pvcover["tableFooter.rowEnd"].Int(-1),
                        (float)pvcover["tableFooter.xPos"].Num(390), (float)pvcover["tableFooter.yPos"].Num(80), PDFWriter.DirectContent);

                    if (listData.Count - 1 > i)
                    {
                        doc.NewPage();
                    }
                }

                doc.Close();

                xa = ms.GetBuffer();

                //outLink = Xmit.Download(ms.GetBuffer(), Page _page, _FileName, ".pdf", "printCover", "printCoverPV", tpath);

                /*
                // fid.Hadid 20181203. Integrate PV Cover to TOWASS
                if (listData.Count == 1)
                {
                    try
                    {
                        bool succesUpload = false;
                        var ftpLo = new FtpLogic();

                        ftpLo.ftpUploadWOS(ms.ToArray(), _FileName + ".pdf", ref succesUpload);

                        int pvNo = listData[0].PV_NO.Int();
                        int pvYr = listData[0].PV_YEAR.Int();

                        fp.Exec("sp_wos_send_result"
                                , new
                                {
                                    sync_id = 0
                                    ,
                                    sync_action = 4
                                    ,
                                    pv_no_towass = ""
                                    ,
                                    pv_no_elvis = pvNo
                                    ,
                                    pv_year = pvYr
                                    ,
                                    pv_amount = 0
                                    ,
                                    vendor_cd = ""
                                    ,
                                    vendor_nm = ""
                                    ,
                                    pv_cover_file = _FileName + ".pdf"
                                    ,
                                    submit_date = DateTime.Now
                                    ,
                                    cancel_date = DateTime.Now
                                    ,
                                    result_sts = 1
                                    ,
                                    message = ""
                                }
                               );
                    }
                    catch (Exception)
                    {

                    }
                }
                */
            }
            catch (Exception ex)
            {
                //_Err.ErrID = 2;
                //_Err.ErrMsg = ex.Message;
            }

            return xa;
        }

        public static string Eval_Curr(Object currencyCd, Object decimalValue, int displayCurrency = 0)
        {
            decimal decimalResult = 0;
            string preCurr = (displayCurrency == 1) ? currencyCd.str() + " " : "";
            string postCurr = (displayCurrency == 2) ? " " + currencyCd.str() : "";
            bool parseOk = Decimal.TryParse(decimalValue.str(), out decimalResult);

            if (currencyCd != null
                && !string.IsNullOrWhiteSpace(currencyCd.ToString())
                && decimalValue != null && parseOk)
            {
                if (currencyCd.Equals("IDR") || currencyCd.Equals("JPY"))
                {
                    return preCurr + decimalResult.ToString("###,###,###,###,###,##0") + postCurr;
                }
                else
                {
                    return preCurr + decimalResult.ToString("###,###,###,###,###,##0.00") + postCurr;
                }
            }

            if (decimalValue != null && parseOk)
            {
                return preCurr + decimalResult.ToString() + postCurr;
            }


            return null;
        }

    }
}
