﻿using Kendo.Mvc.UI;
using Tam.Ebilling.Domain;

namespace Tam.Ebilling.Service
{
    public class EmailTemplateService : DbServiceBase
    {
        public EmailTemplateService(IDbHelper db) : base(db)
        {
        }
        public DataSourceResult GetDataSourceResult(DataSourceRequest request)
        {
            var result = Db.EmailTemplateRepository.GetDataSourceResult(request);

            return result;
        }
    }
}
