﻿using Dapper;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tam.Ebilling.Domain;

namespace Tam.Ebilling.Service
{
    public class CommentDetailService : DbServiceBase
    {
        public CommentDetailService(IDbHelper db) : base(db)
        {

        }

        public IEnumerable<CommentDetail> GetCommentDetail(string comment_id)
        {
            var d = Db.Connection.Query<CommentDetail>("SELECT * FROM tb_r_comment_id where comment_id = '{0}'", new { comment_id = comment_id }).ToList();
            return d;
        }

        public int Add(Comment comment, string[] columns)
        {
            var d = Db.CommentRepository.Add(comment, columns);
            return d;
        }
    }
}
