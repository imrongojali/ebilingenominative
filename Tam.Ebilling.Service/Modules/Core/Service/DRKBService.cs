﻿using Kendo.Mvc.UI;
using Tam.Ebilling.Domain;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Tam.Ebilling.Infrastructure.Session;
using System.Web;

namespace Tam.Ebilling.Service
{
    public class DRKBService : DbServiceBase
    {
        public DRKBService(IDbHelper db) : base(db)
        {
        }

        public DataSourceResult GetViewDataSourceResult(DataSourceRequest request, string customer_group)
        {
            var result = Db.DRKBRepository.GetViewDataSourceResult(request, customer_group);
            return result;
        }

        public DataSourceResult GetDRKBList(DataSourceRequest request, string customer_group)
        {
            var result = Db.DRKBRepository.GetDRKBList(request, customer_group);
            return result;
        }

        public DataSourceResult GetDataPDF(DataSourceRequest request, string customer_group)
        {
            var result = Db.DRKBRepository.GetDataPDFResult(request, customer_group);
            return result;
        }

        public DataSourceResult getchkDRKB(DataSourceRequest request, string customer_group, String[] id)
        {
            var result = Db.DRKBRepository.getchkDRKB(request, customer_group, id);
            return result;
        }
        public DataSourceResult GetDRKBch(DataSourceRequest request, string customer_group, String[] id, string rowid, bool rowall)
        {
            var result = Db.DRKBRepository.GetDRKBch(request, customer_group, id, rowid, rowall);
            return result;
        }
       

        public DataSourceResult GetChekDRKBList(DataSourceRequest request, string customer_group, string id)
        {
            var result = Db.DRKBRepository.GetChekDRKBList(request, customer_group, id);
            return result;
        }

        public DataSourceResult GetViewDataSourceResult(DataSourceRequest request, string customer_group, String[] id)
        {
            var result = Db.DRKBRepository.GetDataPDFResult(request, customer_group);
            return result;
        }

        public DataSourceResult GetCompanyDataSourceResult(DataSourceRequest request)
        {
            var result = Db.DRKBRepository.GetCompany(request);
            return result;
        }
    }
}
