﻿using Agit.Domain;
using Agit.Mvc;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tam.Ebilling.Domain;
using Tam.Ebilling.Infrastructure.Session;
using Dapper;
using Tam.Ebilling.Infrastructure.Cache;
using Agit.Helper;

namespace Tam.Ebilling.Service
{
    public class MenuRoleService : DbServiceBase
    {
        public MenuRoleService(IDbHelper db) : base(db)
        {
        }

        public bool IsRoleAllowedAccessMenu(int menuId, string levelCode)
        {
            try
            {
                var listMenu = Db.MenuRoleRepository.GetAllowedRoleByMenu(menuId);

                return listMenu.Exists(m => m.level_code == levelCode);
            }
            catch (Exception)
            {
                return false;
            }
        }

        //wo.apiyudin permission PV Monitoring
        public bool IsRoleAllowedAccessMenu(int menuId, string userType, string user)
        {
            bool isValid = false;

            try
            {
                isValid = Db.MenuRoleRepository.GetAllowedByUserTypeUsername(menuId, userType, user);
            }
            catch (Exception)
            {

            }

            return isValid;
        }
        //end

    }
}
