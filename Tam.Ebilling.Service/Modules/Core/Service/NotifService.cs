﻿using Kendo.Mvc.UI;
using Tam.Ebilling.Domain;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Tam.Ebilling.Infrastructure.Session;
using System.Web;

namespace Tam.Ebilling.Service
{
    public class NotifService : DbServiceBase
    {
        public NotifService(IDbHelper db) : base(db)
        {
        }
 
        public string GetNotif(string customerCode)
        {
            var result = Db.NotifRepository.GetNotif(customerCode);
            return result;
        }
    }
}
