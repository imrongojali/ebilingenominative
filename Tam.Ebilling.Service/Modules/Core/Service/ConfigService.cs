﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Tam.Ebilling.Domain;

namespace Tam.Ebilling.Service
{
    public class ConfigService : DbServiceBase
    {
        public ConfigService(IDbHelper db) : base(db)
        {
        }
        public DataSourceResult GetDataSourceResult(DataSourceRequest request)
        {
            var result = Db.ConfigRepository.FindAll().ToDataSourceResult(request);

            return result;
        }

    }
}
