﻿using Dapper;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tam.Ebilling.Domain;

namespace Tam.Ebilling.Service
{
    public class TransactionTypeService : DbServiceBase
    {
        public TransactionTypeService(IDbHelper db) : base(db)
        {

        }
        public List<TransactionType> GetTransactionType()
        {
            //var strsql = @"SELECT * FROM vw_Transaction_Type";
            var strsql = @"SELECT distinct concat(transaction_name, ' (',transaction_type_id,')') as transaction_name,transaction_type_id FROM vw_Transaction_Type";
            var d = Db.Connection.Query<TransactionType>(strsql).ToList();
            return d;
        }
    }
}
