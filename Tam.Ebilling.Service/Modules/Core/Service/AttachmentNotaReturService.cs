﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Tam.Ebilling.Domain.Modules.Core.Model;
using Tam.Ebilling.Domain;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using Tam.Ebilling.Infrastructure.Session;
using System;
using Tam.Ebilling.Infrastructure.Cache;
using System.IO;
using Tam.Ebilling.Infrastructure.Helper;
using System.Net;
using Tam.Ebilling.Infrastructure.Adapter;


namespace Tam.Ebilling.Service
{
    public class AttachmentNotaReturService : DbServiceBase
    {
        public AttachmentNotaReturService(IDbHelper db) : base(db)
        {
        }
        public DataSourceResult GetDataSourceResult(DataSourceRequest request)
        {
            var result = Db.AttachmentNotaReturRepository.GetDataSourceResult(request);

            return result;
        }
        public List<Invoice> GetUpdateUpload(string reff)
        {
            var result = Db.InvoiceRepository.GetUploadNotur(reff);
            return result;
        }
        public IEnumerable<AttachmentNotaRetur> GetDocumentFileByReference(string reff)
        {
            var result = Db.AttachmentNotaReturRepository.Find(new { reference = reff });
            result = result.Where(x => x.row_status != 1);

            return result;
        }

        public IEnumerable<AttachmentNotaRetur> GetDocumentByID(string id)
        {
            var result = Db.AttachmentNotaReturRepository.Find(new { attachment_notaretur_id = id });

            return result;
        }


        public IEnumerable<AttachmentSpNotaRetur> GetNotaReturDocumentByID(string id)
        {
            var d = Db.Connection.Query<AttachmentSpNotaRetur>("SELECT * FROM [dbo].[tb_r_attachment_spca_retur]" +
                " where attachment_spcaretur_id = @id", new { id = id }).ToList();
            return d;
        }

        public List<AttachmentSpNotaRetur> GetSpNotaReturAttachment(string reference)
        {
            var d = Db.Connection.Query<AttachmentSpNotaRetur>("SELECT * FROM [dbo].[tb_r_attachment_spca_retur]" +
                " where attachment_spcaretur_id = @reference", new { reference = reference }).ToList();
            return d;
        }

        public List<udf_GetAttachmentNotaRetur> GetNotaReturAttachment(string reference)
        {
            var d = Db.Connection.Query<udf_GetAttachmentNotaRetur>("SELECT * FROM [dbo].[udf_GetAttachmentNotaRetur](@reference)", new { reference = reference }).ToList();
            return d;
        }

        //Add by AD
        public List<udf_GetAttachmentNotaRetur> GetNotaReturAttachmentTtdDgt(string reff)
        {
            var d = Db.Connection.Query<udf_GetAttachmentNotaRetur>("SELECT * FROM [dbo].[udf_GetAttachmentNotaRetur](@reff)", new { reference = reff }).ToList();
            return d;
        }
        //END

        public AttachmentNotaRetur Upload(IPostedFile file, string reff, string TypeDoc, out string msgerror)
        {
            msgerror = "";
            int maxFileSize = int.Parse(ApplicationCacheManager.GetConfig<string>("MaxUploadSize")) * 1024 * 1024;
            string fileTypeAllowed = ApplicationCacheManager.GetConfig<string>("FileTypeUpload");
            List<string> fileType = fileTypeAllowed.Split(',').ToList();
            bool isFileTypeAllowed = fileType.Exists(x => x.Contains(file.ContentType));

            var doc = new AttachmentNotaRetur();
            if(TypeDoc == "Notur")
            {
                if (isFileTypeAllowed)
                {
                    if (file.ContentLength < maxFileSize)
                    {

                        var username = ApplicationCacheManager.GetConfig<string>("FileUploadUserName");
                        var password = ApplicationCacheManager.GetConfig<string>("FileUploadPassword");
                        var config = ApplicationCacheManager.GetConfig<string>("FileUploadTemp");
                        var domain = ApplicationCacheManager.GetConfig<string>("FileUploadDomain");


                        var userdomain = domain + "\\" + username;
                        var fileName = Path.GetFileName(file.FileName);
                        var newFileName = config + "\\" + fileName;
                        var url = Path.Combine(newFileName);
                        var dir = Path.GetDirectoryName(reff);

                        var pers = new Impersonation();
                        pers.Impersonate(userdomain, password);
                        var credential = new NetworkCredential(userdomain, password);

                        using (new NetworkConnection(config, credential))
                        {
                            try
                            {
                                if (!Directory.Exists(dir))
                                    Directory.CreateDirectory(dir);
                            }
                            catch (Exception e)
                            {

                            }
                            file.SaveAs(newFileName);
                        }

                        doc = new AttachmentNotaRetur
                        {
                            attachment_notaretur_id = -1,
                            reference = reff,
                            filename = fileName,
                            document_type = TypeDoc,
                            //filepath = newFileName,
                            created_by = SessionManager.Current,
                            created_date = DateTime.Now
                        };
                    }
                    else
                    {
                        msgerror = "File size cannot exceed 10 MB";
                    }
                }
                else
                {
                    msgerror = "File type allowed are " + fileTypeAllowed;
                }

            }
            else if(TypeDoc == "NoturSignOff")
            {
                if (isFileTypeAllowed)
                {
                    if (file.ContentLength < maxFileSize)
                    {

                        var username = ApplicationCacheManager.GetConfig<string>("FileUploadUserName");
                        var password = ApplicationCacheManager.GetConfig<string>("FileUploadPassword");
                        var config = ApplicationCacheManager.GetConfig<string>("FileUploadTempTTD");
                        var domain = ApplicationCacheManager.GetConfig<string>("FileUploadDomain");


                        var userdomain = domain + "\\" + username;
                        var fileName = Path.GetFileName(file.FileName);
                        var newFileName = config + "\\" + fileName;
                        var url = Path.Combine(newFileName);
                        var dir = Path.GetDirectoryName(reff);

                        var pers = new Impersonation();
                        pers.Impersonate(userdomain, password);
                        var credential = new NetworkCredential(userdomain, password);

                        using (new NetworkConnection(config, credential))
                        {
                            try
                            {
                                if (!Directory.Exists(dir))
                                    Directory.CreateDirectory(dir);
                            }
                            catch (Exception e)
                            {

                            }
                            file.SaveAs(newFileName);
                        }

                        doc = new AttachmentNotaRetur
                        {
                            attachment_notaretur_id = -1,
                            reference = reff,
                            filename = fileName,
                            document_type = TypeDoc,
                            //filepath = newFileName,
                            created_by = SessionManager.Current,
                            created_date = DateTime.Now
                        };
                    }
                    else
                    {
                        msgerror = "File size cannot exceed 10 MB";
                    }
                }
                else
                {
                    msgerror = "File type allowed are " + fileTypeAllowed;
                }
            }
            else if(TypeDoc == "NoturDGT")
            {
                if (isFileTypeAllowed)
                {
                    if (file.ContentLength < maxFileSize)
                    {

                        var username = ApplicationCacheManager.GetConfig<string>("FileUploadUserName");
                        var password = ApplicationCacheManager.GetConfig<string>("FileUploadPassword");
                        var config = ApplicationCacheManager.GetConfig<string>("FileUploadTempDGT");
                        var domain = ApplicationCacheManager.GetConfig<string>("FileUploadDomain");


                        var userdomain = domain + "\\" + username;
                        var fileName = Path.GetFileName(file.FileName);
                        var newFileName = config + "\\" + fileName;
                        var url = Path.Combine(newFileName);
                        var dir = Path.GetDirectoryName(reff);

                        var pers = new Impersonation();
                        pers.Impersonate(userdomain, password);
                        var credential = new NetworkCredential(userdomain, password);

                        using (new NetworkConnection(config, credential))
                        {
                            try
                            {
                                if (!Directory.Exists(dir))
                                    Directory.CreateDirectory(dir);
                            }
                            catch (Exception e)
                            {

                            }
                            file.SaveAs(newFileName);
                        }

                        doc = new AttachmentNotaRetur
                        {
                            attachment_notaretur_id = -1,
                            reference = reff,
                            filename = fileName,
                            document_type = TypeDoc,
                            //filepath = newFileName,
                            created_by = SessionManager.Current,
                            created_date = DateTime.Now
                        };
                    }
                    else
                    {
                        msgerror = "File size cannot exceed 10 MB";
                    }
                }
                else
                {
                    msgerror = "File type allowed are " + fileTypeAllowed;
                }
            }

            return doc;
        }

        public List<AttachmentNotaRetur> UploadNotur(string filename, string reference)
        {
            var result = Db.AttachmentNotaReturRepository.UploadNotur(filename, reference, "Notur");
            return result;
        }
        public List<AttachmentNotaRetur> Remove(long attachment_notaretur_id)
        {
            var result = Db.AttachmentNotaReturRepository.Remove(attachment_notaretur_id);
            return result;
        }

        public ResultUpload SaveUpload(string reff, IEnumerable<AttachmentNotaRetur> docs, string documenttype, out string msgerror)
        {
            msgerror = "";
            //var result = true;

            ResultUpload resultupload = new ResultUpload
            {
                Success = 0,
                Error = 0,
                NamaFile = ""
            };
            //long? id = null;
            //var reference = "";


            var username = ApplicationCacheManager.GetConfig<string>("FileUploadUserName");
            var password = ApplicationCacheManager.GetConfig<string>("FileUploadPassword");
            var config = ApplicationCacheManager.GetConfig<string>("FileUploadTemp");
            var configTTD = ApplicationCacheManager.GetConfig<string>("FileUploadTempTTD");
            var configDGT = ApplicationCacheManager.GetConfig<string>("FileUploadTempDGT");
            var domain = ApplicationCacheManager.GetConfig<string>("FileUploadDomain");

            var userdomain = domain + "\\" + username;
            var pers = new Impersonation();
            pers.Impersonate(userdomain, password);

            //var docfilenameNotur = "Nota Retur - ";
            //var docfilenameNoturSignOff = "Nota Retur Sign Off - ";
            //var docfilenameNoturDGT = "Nota Retur DGT - ";

            if (docs != null)
            {
                if (docs.Count() > 0)
                {
                    //save document file
                    foreach (AttachmentNotaRetur doc in docs)
                    {
                        if (doc.Status == "New")
                        {

                            // Update Property
                            doc.reference = reff;
                            doc.created_date = DateTime.Now;
                            doc.created_by = SessionManager.Username;
                            doc.modified_by = SessionManager.Username;
                            doc.modified_date = DateTime.Now;
                            // Rest status
                            doc.Status = null;
                            //add AD
                            //var docfilenameNotur = "Nota Retur - " + doc.filename;
                            //var docfilenameNoturSignOff = "Nota Retur Sign Off - " + doc.filename;
                            //var docfilenameNoturDGT = "Nota Retur DGT - " + doc.filename;
                            //END
                            //if (documenttype == "Notur")
                            //{

                            //    Db.AttachmentNotaReturRepository.UploadNotur(doc.filename, doc.reference);
                            //}
                            //else if(documenttype == "NoturSignOff")
                            //{

                            //    Db.AttachmentNotaReturRepository.UploadNotur(docfilenameNoturSignOff+doc.filename, doc.reference);
                            //}
                            //else if(documenttype == "NoturDGT")
                            //{

                            //    Db.AttachmentNotaReturRepository.UploadNotur(docfilenameNoturDGT+doc.filename, doc.reference);
                            //}
                            //Db.AttachmentNotaReturRepository.UploadNotur(doc.filename, doc.reference, doc.document_type);

                            //Enhancment 14102020
                            if (doc.document_type == "Notur")
                            {
                                var tempFile = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileUploadTemp"), doc.filename);
                                var newFile = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileUpload"), doc.filename);

                                //var Update = Service<InvoiceService>().GetUploadNotur(daNumber);
                                //var reference = reff;
                                var credential = new NetworkCredential(userdomain, password);
                                using (new NetworkConnection(tempFile, credential))
                                using (new NetworkConnection(newFile, credential))
                                {
                                    try
                                    {
                                        var path = Path.GetDirectoryName(newFile);
                                        if (!Directory.Exists(path))
                                            Directory.CreateDirectory(path);
                                        File.Move(tempFile, newFile);
                                        Db.AttachmentNotaReturRepository.UploadNotur(doc.filename, doc.reference, doc.document_type);
                                    }
                                    catch (Exception e)
                                    {
                                        var message = e.Message;
                                    }
                                    Db.InvoiceRepository.GetUploadNotur(doc.reference);
                                    //Db.NotaReturRepository.GetUploadNoturMV(doc.reference);
                                }
                            }
                            else if (doc.document_type == "NoturSignOff")
                            {
                                var tempFileTTD = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileUploadTempTTD"), doc.filename);
                                var newFile = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileUploadCreditnoteTTD"), doc.filename);

                                var credential = new NetworkCredential(userdomain, password);
                                using (new NetworkConnection(tempFileTTD, credential))
                                using (new NetworkConnection(newFile, credential))
                                {
                                    try
                                    {
                                        var path = Path.GetDirectoryName(newFile);
                                        if (!Directory.Exists(path))
                                            Directory.CreateDirectory(path);
                                        File.Move(tempFileTTD, newFile);
                                        Db.AttachmentNotaReturRepository.UploadNotur(doc.filename, doc.reference, doc.document_type);
                                    }
                                    catch (Exception e)
                                    {
                                        if (e != null)
                                        {
                                            resultupload.Error = resultupload.Error + 1;
                                            resultupload.NamaFile += $"{e.Message.Replace("\n", "").Replace("\r", "")} {doc.filename}{Environment.NewLine}";
                                        }
                                        else
                                        {
                                            throw;
                                        }

                                    }
                                    Db.InvoiceRepository.GetUploadNoturTTD(doc.reference);
                                    //Db.NotaReturRepository.GetUploadNoturMV(doc.reference);
                                }
                            }
                            else if (doc.document_type == "NoturDGT")
                            {
                                var tempFileDGT = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileUploadTempDGT"), doc.filename);
                                var newFile = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileUploadDGT"), doc.filename);

                                //var Update = Service<InvoiceService>().GetUploadNotur(daNumber);
                                //var reference = reff;
                                var credential = new NetworkCredential(userdomain, password);
                                using (new NetworkConnection(tempFileDGT, credential))
                                using (new NetworkConnection(newFile, credential))
                                {
                                    try
                                    {
                                        var path = Path.GetDirectoryName(newFile);
                                        if (!Directory.Exists(path))
                                            Directory.CreateDirectory(path);
                                        File.Move(tempFileDGT, newFile);
                                        Db.AttachmentNotaReturRepository.UploadNotur(doc.filename, doc.reference, doc.document_type);
                                    }
                                    catch (Exception e)
                                    {
                                        if (e != null)
                                        {
                                            resultupload.Error = resultupload.Error + 1;
                                            resultupload.NamaFile += $"{e.Message.Replace("\n", "").Replace("\r", "")} {doc.filename}{Environment.NewLine}";
                                        }
                                        else
                                        {
                                            throw;
                                        }
                                    }
                                    Db.InvoiceRepository.GetUploadNoturDGT(doc.reference);
                                    //Db.NotaReturRepository.GetUploadNoturMV(doc.reference);
                                }
                            }
                            //END

                        }
                        else if (doc.Status == "Delete")
                        {
                            if (doc.reference == default(string))
                            {
                                // Delete physical file
                                var existingFile = "";
                                var newFile = "";
                                switch (doc.document_type)
                                {
                                    case "NoturDGT":
                                        existingFile = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileUploadDGT"), doc.filename);
                                        newFile = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileUploadDGT"), $"removed_{System.DateTime.Now.ToString("YYYYMMddHHmmsss")}_{doc.filename}");
                                        break;
                                    case "NoturSignOff":
                                        existingFile = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileUploadCreditnoteTTD"), doc.filename);
                                        newFile = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileUploadCreditnoteTTD"), $"removed_{System.DateTime.Now.ToString("YYYYMMddHHmmsss")}_{doc.filename}");
                                        break;
                                    case "Notur":
                                        existingFile = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileUpload"), doc.filename);
                                        newFile = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileUpload"), $"removed_{System.DateTime.Now.ToString("YYYYMMddHHmmsss")}_{doc.filename}");
                                        break;
                                }
                                switch (doc.document_type)
                                {
                                    case "NoturDGT":
                                        var tempFileDGT = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileUploadTempDGT"), doc.filename);
                                        var credentialDGT = new NetworkCredential(userdomain, password);
                                        using (new NetworkConnection(tempFileDGT, credentialDGT))
                                        {
                                            try
                                            {
                                                File.Delete(tempFileDGT);
                                                File.Move(existingFile, newFile);
                                            }
                                            catch (Exception e)
                                            {

                                            }
                                        }
                                        break;
                                    case "NoturSignOff":
                                        var tempFileTTD = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileUploadTempTTD"), doc.filename);
                                        var credentialTTD = new NetworkCredential(userdomain, password);
                                        using (new NetworkConnection(tempFileTTD, credentialTTD))
                                        {
                                            try
                                            {
                                                File.Delete(tempFileTTD);
                                                File.Move(existingFile, newFile);
                                            }
                                            catch (Exception e)
                                            {

                                            }
                                        }
                                        break;
                                    case "Notur":
                                        var tempFile = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileUploadTemp"), doc.filename);
                                        var credential = new NetworkCredential(userdomain, password);
                                        using (new NetworkConnection(tempFile, credential))
                                        {
                                            try
                                            {
                                                File.Delete(tempFile);
                                                File.Move(existingFile, newFile);
                                            }
                                            catch (Exception e)
                                            {

                                            }
                                        }
                                        break;
                                }
                                //var tempFile = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileUploadTemp"), doc.filename);
                                //var credential = new NetworkCredential(userdomain, password);
                                //using (new NetworkConnection(tempFile, credential))
                                //{
                                //    try
                                //    {
                                //        File.Delete(tempFile);
                                //        File.Move(existingFile, newFile);
                                //    }
                                //    catch (Exception e)
                                //    {

                                //    }
                                //}
                            }
                            else
                            {
                                //Delete data
                                Db.AttachmentNotaReturRepository.Remove(doc.attachment_notaretur_id);
                                var existingFile = "";
                                var newFile = "";
                                switch (doc.document_type)
                                {
                                    case "NoturDGT":
                                        existingFile = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileUploadDGT"), doc.filename);
                                        newFile = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileUploadDGT"), $"removed_{System.DateTime.Now.ToString("YYYYMMddHHmmsss")}_{doc.filename}");
                                        break;
                                    case "NoturSignOff":
                                        existingFile = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileUploadCreditnoteTTD"), doc.filename);
                                        newFile = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileUploadCreditnoteTTD"), $"removed_{System.DateTime.Now.ToString("YYYYMMddHHmmsss")}_{doc.filename}");
                                        break;
                                    case "Notur":
                                        existingFile = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileUpload"), doc.filename);
                                        newFile = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileUpload"), $"removed_{System.DateTime.Now.ToString("YYYYMMddHHmmsss")}_{doc.filename}");
                                        break;
                                }
                                //if (doc.reference.HasValue)
                                //{
                                //    var dattodel = dbhelper.AttachmentNotaReturRepository.Find(new { reference = reference.Value, filepath = doc.filepath }).FirstOrDefault();
                                //    if (dattodel != null)
                                //    {
                                //        dbhelper.AttachmentNotaReturRepository.Remove(dattodel);
                                //    }
                                //}

                                // Delete physical files
                                switch (doc.document_type)
                                {
                                    case "NoturDGT":
                                        var tempFileDGT = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileUploadTempDGT"), doc.filename);
                                        var credentialDGT = new NetworkCredential(userdomain, password);
                                        using (new NetworkConnection(tempFileDGT, credentialDGT))
                                        {
                                            try
                                            {
                                                File.Delete(tempFileDGT);
                                                File.Move(existingFile, newFile);
                                            }
                                            catch (Exception e)
                                            {

                                            }
                                            Db.InvoiceRepository.DeleteUploadNoturDGT(doc.reference);
                                        }
                                        break;
                                    case "NoturSignOff":
                                        var tempFileTTD = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileUploadTempTTD"), doc.filename);
                                        var credentialTTD = new NetworkCredential(userdomain, password);
                                        using (new NetworkConnection(tempFileTTD, credentialTTD))
                                        {
                                            try
                                            {
                                                File.Delete(tempFileTTD);
                                                File.Move(existingFile, newFile);
                                            }
                                            catch (Exception e)
                                            {

                                            }
                                            Db.InvoiceRepository.DeleteUploadNoturTTD(doc.reference);
                                        }
                                        break;
                                    case "Notur":
                                        var tempFile = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileUploadTemp"), doc.filename);
                                        var credential = new NetworkCredential(userdomain, password);
                                        using (new NetworkConnection(tempFile, credential))
                                        {
                                            try
                                            {
                                                File.Delete(tempFile);
                                                File.Move(existingFile, newFile);
                                            }
                                            catch (Exception e)
                                            {

                                            }
                                            Db.InvoiceRepository.DeleteUploadNoturOri(doc.reference);
                                        }
                                        break;
                                }
                                //var file = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileUploadTemp"), doc.filename);
                            }
                        }
                    }
                }
            }

            return resultupload;
        }

        #region TTD N DGT Upload ALL
        //add by AD TTD DGT
        public ResultUpload SaveUploadTtdDgtAll(string reff, IEnumerable<AttachmentNotaRetur> docs, string documenttype, out string msgerror)
        {
            msgerror = "";
            var result = true;
            //long? id = null;
            ResultUpload resultupload = new ResultUpload
            {
                Success = 0,
                Error = 0,
                NamaFile = ""
            };
            //var reference = "";


            var username = ApplicationCacheManager.GetConfig<string>("FileUploadUserName");
            var password = ApplicationCacheManager.GetConfig<string>("FileUploadPassword");
            var config = ApplicationCacheManager.GetConfig<string>("FileUploadTemp");
            var domain = ApplicationCacheManager.GetConfig<string>("FileUploadDomain");

            var userdomain = domain + "\\" + username;
            var pers = new Impersonation();
            pers.Impersonate(userdomain, password);

            if (docs != null)
            {
                if (docs.Count() > 0)
                {
                    //save document file
                    foreach (AttachmentNotaRetur doc in docs)
                    {
                        if (doc.Status == "New")
                        {

                            // Update Property
                            var docnumber = doc.filename;
                            docnumber = docnumber.Replace(".pdf", "");
                            doc.reference = docnumber;
                            doc.created_date = DateTime.Now;
                            doc.created_by = SessionManager.Username;
                            doc.modified_by = SessionManager.Username;
                            doc.modified_date = DateTime.Now;

                            // Rest status
                            doc.Status = null;

                            //Enhancment 14102020
                            if (doc.document_type == "NoturSignOff")
                            {
                                var tempFile = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileUploadTempTTD"), doc.filename);
                                var newFile = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileUploadCreditnoteTTD"), doc.filename);

                                var credential = new NetworkCredential(userdomain, password);
                                using (new NetworkConnection(tempFile, credential))
                                using (new NetworkConnection(newFile, credential))
                                {
                                    try
                                    {
                                        var duplicat = Db.AttachmentNotaReturRepository.Cekdata(doc.reference);
                                        if (duplicat.Count != 0)
                                        {
                                            var path = Path.GetDirectoryName(newFile);
                                            if (!Directory.Exists(path))
                                                Directory.CreateDirectory(path);
                                            File.Move(tempFile, newFile);

                                            Db.AttachmentNotaReturRepository.UploadNotur(doc.filename, doc.reference, doc.document_type);
                                        }
                                        else
                                        {
                                            resultupload.Error = resultupload.Error + 1;
                                            resultupload.NamaFile += $"{msgerror = "Credit Note Number doesn't match"} {doc.filename}{Environment.NewLine}";
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        if (e != null)
                                        {
                                            resultupload.Error = resultupload.Error + 1;
                                            //resultupload.NamaFile += $"{e.Message.Replace("\n", "").Replace("\r", "")} {doc.filename}{Environment.NewLine}";
                                            resultupload.NamaFile += $"{msgerror = "Credit Note Sign Off Already Exist"} {doc.filename}{Environment.NewLine}";
                                        }
                                        else
                                        {
                                            throw;
                                        }
                                    }
                                    Db.InvoiceRepository.GetUploadNoturTTD(doc.reference);
                                    //Db.NotaReturRepository.GetUploadNoturMV(doc.reference);
                                }
                            }
                            else if (doc.document_type == "NoturDGT")
                            {
                                var tempFile = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileUploadTempDGT"), doc.filename);
                                var newFile = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileUploadDGT"), doc.filename);

                                //var Update = Service<InvoiceService>().GetUploadNotur(daNumber);
                                //var reference = reff;
                                var credential = new NetworkCredential(userdomain, password);
                                using (new NetworkConnection(tempFile, credential))
                                using (new NetworkConnection(newFile, credential))
                                {
                                    try
                                    {
                                        var duplicat = Db.AttachmentNotaReturRepository.Cekdata(doc.reference);
                                        if (duplicat.Count != 0)
                                        {
                                            var path = Path.GetDirectoryName(newFile);
                                            if (!Directory.Exists(path))
                                                Directory.CreateDirectory(path);
                                            File.Move(tempFile, newFile);

                                            Db.AttachmentNotaReturRepository.UploadNotur(doc.filename, doc.reference, doc.document_type);
                                            //resultupload.NamaFile += $"{doc.filename}{Environment.NewLine}";
                                        }
                                        else
                                        {
                                            resultupload.Error = resultupload.Error + 1;
                                            resultupload.NamaFile += $"{msgerror = "Credit Note Number doesn't match"} {doc.filename}{Environment.NewLine}";
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        if (e != null)
                                        {
                                            resultupload.Error = resultupload.Error + 1;
                                            //resultupload.NamaFile += $"{e.Message.Replace("\n", "").Replace("\r", "")} {doc.filename}{Environment.NewLine}";
                                            resultupload.NamaFile += $"{msgerror = "Credit Note DGT Already Exist"} {doc.filename}{Environment.NewLine}";
                                        }
                                        else
                                        {
                                            throw;
                                        }
                                    }
                                    Db.InvoiceRepository.GetUploadNoturDGT(doc.reference);
                                    //Db.NotaReturRepository.GetUploadNoturMV(doc.reference);
                                }
                            }
                            //END

                        }
                        else if (doc.Status == "Delete")
                        {
                            if (doc.reference == default(string))
                            {
                                // Delete physical file
                                var tempFile = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileUploadTemp"), doc.filename);
                                var credential = new NetworkCredential(userdomain, password);
                                using (new NetworkConnection(tempFile, credential))
                                {
                                    try
                                    {
                                        File.Delete(tempFile);
                                    }
                                    catch (Exception e)
                                    {

                                    }
                                }
                            }
                            else
                            {
                                //Delete data
                                Db.AttachmentNotaReturRepository.Remove(doc.attachment_notaretur_id);
                                //if (doc.reference.HasValue)
                                //{
                                //    var dattodel = dbhelper.AttachmentNotaReturRepository.Find(new { reference = reference.Value, filepath = doc.filepath }).FirstOrDefault();
                                //    if (dattodel != null)
                                //    {
                                //        dbhelper.AttachmentNotaReturRepository.Remove(dattodel);
                                //    }
                                //}

                                // Delete physical file
                                var file = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileUpload"), doc.filename);
                                var credential = new NetworkCredential(userdomain, password);
                                using (new NetworkConnection(file, credential))
                                {
                                    try
                                    {
                                        File.Delete(file);
                                    }
                                    catch (Exception e)
                                    {

                                    }
                                }
                            }
                        }
                    }
                }
            }
            return resultupload;
        }
        //END
        #endregion

        public bool SaveUploadSpCaRetur(string reff, IEnumerable<AttachmentNotaRetur> docs, out string msgerror)
        {
            msgerror = "";
            var result = true;

            //long? id = null;
            //var reference = "";

            var username = ApplicationCacheManager.GetConfig<string>("FileSparePartUserName");
            var password = ApplicationCacheManager.GetConfig<string>("FileSparePartPassword");
            var config = ApplicationCacheManager.GetConfig<string>("FileSparePartFolderTemp");
            var domain = ApplicationCacheManager.GetConfig<string>("FileSparePartDomain");
            var userdomain = domain + "\\" + username;
            var pers = new Impersonation();
            pers.Impersonate(userdomain, password);

            if (docs != null)
            {
                if (docs.Count() > 0)
                {
                    //save document file
                    foreach (AttachmentNotaRetur doc in docs)
                    {
                        if (doc.Status == "New")
                        {

                            // Update Property
                            doc.reference = reff;
                            doc.created_date = DateTime.Now;
                            doc.created_by = SessionManager.Username;
                            doc.modified_by = SessionManager.Username;
                            doc.modified_date = DateTime.Now;
                            // Rest status
                            doc.Status = null;
                            Db.AttachmentNotaReturRepository.UploadSpCaRetur(doc.filename, doc.reference);

                            var tempFile = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileSparePartFolderTemp"), doc.filename);
                            var newFile = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileSparePart"), doc.filename);
                            //var Update = Service<InvoiceService>().GetUploadNotur(daNumber);
                            //var reference = reff;
                            var credential = new NetworkCredential(userdomain, password);
                            using (new NetworkConnection(tempFile, credential))
                            using (new NetworkConnection(newFile, credential))
                            {
                                try
                                {
                                    var path = Path.GetDirectoryName(newFile);
                                    if (!Directory.Exists(path))
                                        Directory.CreateDirectory(path);

                                    File.Move(tempFile, newFile);
                                }
                                catch (Exception e)
                                {

                                }
                                Db.InvoiceRepository.GetUploadSpCaRetur(doc.reference);
                                //Db.NotaReturRepository.GetUploadNoturMV(doc.reference);
                            }
                            //}

                        }
                        else if (doc.Status == "Delete")
                        {
                            if (doc.reference == default(string))
                            {
                                // Delete physical file
                                var tempFile = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileSparePartFolderTemp"), doc.filename);
                                var credential = new NetworkCredential(userdomain, password);
                                using (new NetworkConnection(tempFile, credential))
                                {
                                    try
                                    {
                                        File.Delete(tempFile);
                                    }
                                    catch (Exception e)
                                    {

                                    }
                                }
                            }
                            else
                            {
                                //Delete data
                                Db.AttachmentNotaReturRepository.RemoveSpCaNotaRetur(doc.attachment_notaretur_id);
                                //if (doc.reference.HasValue)
                                //{
                                //    var dattodel = dbhelper.AttachmentNotaReturRepository.Find(new { reference = reference.Value, filepath = doc.filepath }).FirstOrDefault();
                                //    if (dattodel != null)
                                //    {
                                //        dbhelper.AttachmentNotaReturRepository.Remove(dattodel);
                                //    }
                                //}

                                // Delete physical file
                                var file = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileSparePart"), doc.filename);
                                var credential = new NetworkCredential(userdomain, password);
                                using (new NetworkConnection(file, credential))
                                {
                                    try
                                    {
                                        File.Delete(file);
                                    }
                                    catch (Exception e)
                                    {

                                    }
                                }
                            }
                        }
                    }
                }
            }

            return result;
        }
    }
}
