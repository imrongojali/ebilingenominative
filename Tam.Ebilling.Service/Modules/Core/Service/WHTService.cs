﻿using Kendo.Mvc.UI;
using Tam.Ebilling.Domain;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Tam.Ebilling.Infrastructure.Session;
using System.Web;

namespace Tam.Ebilling.Service
{
    public class WHTService : DbServiceBase
    {
        public WHTService(IDbHelper db) : base(db)
        {
        }

        public DataSourceResult GetViewDataSourceResult(DataSourceRequest request, string customer_group)
        {
            var result = Db.WHTRepository.GetViewDataSourceResult(request, customer_group);
            return result;
        }
        
        public DataSourceResult GetDataPDF(DataSourceRequest request, string customer_group)
        {
            var result = Db.WHTRepository.GetDataPDFResult(request, customer_group);
            return result;
        }

        public DataSourceResult getchkwht(DataSourceRequest request, string customer_group, Guid[] id)
        {
            var result = Db.WHTRepository.getchkwht(request, customer_group, id);
            return result;
        }

        public DataSourceResult GetChekWHTList(DataSourceRequest request, string customer_group, string id)
        {
            var result = Db.WHTRepository.GetChekWHTList(request, customer_group, id);
            return result;
        }

        public DataSourceResult GetPDFFileExists(DataSourceRequest request, string customer_group, string id)
        {
            var result = Db.WHTRepository.GetChekWHTList(request, customer_group, id);
            return result;
        }

        public DataSourceResult GetViewDataSourceResult(DataSourceRequest request, string customer_group, Guid[] id)
        {
            var result = Db.WHTRepository.GetDataPDFResult(request, customer_group);
            return result;
        }
    }
}
