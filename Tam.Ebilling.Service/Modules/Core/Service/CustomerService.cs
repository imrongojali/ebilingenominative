﻿using Dapper;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tam.Ebilling.Domain;
using Tam.Ebilling.Infrastructure.Session;

namespace Tam.Ebilling.Service
{
    public class CustomerService : DbServiceBase
    {
        public CustomerService(IDbHelper db) : base(db)
        {

        }
        public Kendo.Mvc.UI.DataSourceResult GetDataSourceResult(DataSourceRequest request)
        {
            var result = Db.CustomerRepository.GetDataSourceResult(request);
            return result;
        }

        public Customer GetCustomer(string customer_code, string business_unit_id = null)
        {
            object param;

            if (business_unit_id != null)
            {
                param = new { customer_code = customer_code, business_unit_id = business_unit_id };
            }
            else
            {
                param = new { customer_code = customer_code };
            }

            var d = Db.CustomerRepository.Find(param).FirstOrDefault();
            return d;
        }

        public Customer GetCustomerName(string customer_name)
        {
            object param;

            param = new { customer_name = customer_name };

            var d = Db.CustomerRepository.Find(param).FirstOrDefault();
            return d;
        }
        public List<User> GetCust()
        {
            var strsql = @"SELECT * FROM vw_Customer_All order by customer_name";
            var d = Db.Connection.Query<User>(strsql).ToList();
            return d;
        }

        public List<CustomerGroup> GetCustomer()
        {
            var strsql = @"SELECT * From tb_m_customer_group order by customer_group_name ";
            var d = Db.Connection.Query<CustomerGroup>(strsql).ToList();
            return d;
        }

        #region Add By AD
        public List<BuktiPotong> GetCustomerAll()
        {
            var strsql = @"SP_SelectAllCustomer";
            var d = Db.Connection.Query<BuktiPotong>(strsql).ToList();
            return d;
        }
        #endregion

        public bool Save(Customer cust)
        {
            var result = false;

            try
            {
                var data = Db.CustomerRepository.Find(new { customer_code = cust.customer_code, business_unit_id = cust.business_unit_id });

                if (data.Count() == 0)
                {
                    cust.created_by = SessionManager.Current;
                    cust.created_date = DateTime.Now;

                    Db.CustomerRepository.Add(cust, new string[] {
                        "business_unit_id",
                        "customer_code",
                        "customer_name",
                        "customer_group",
                        "created_date",
                        "created_by"
                    });
                }
                else
                {
                    cust.modified_by = SessionManager.Current;
                    cust.modified_date = DateTime.Now;

                    Db.CustomerRepository.Update(cust, new string[] {
                        "business_unit_id",
                        "customer_code",
                        "customer_name",
                        "customer_group",
                        "modified_date",
                        "modified_by",
                        "row_status"
                    });
                }

                result = true;
            }
            catch (Exception e)
            {
                result = false;
            }
            
            return result;
        }
    }
}
