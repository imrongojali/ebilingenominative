﻿using Kendo.Mvc.UI;
using Tam.Ebilling.Domain;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Tam.Ebilling.Infrastructure.Session;
using System.Web;
using Tam.Ebilling.Infrastructure.Adapter;
using Tam.Ebilling.Infrastructure.Cache;
using System.IO;
using Tam.Ebilling.Infrastructure.Helper;
using System.Net;
using Tam.Ebilling.Infrastructure.Enum;
using Agit.Helper.Helper;
using System.Net.Mail;
using System.Diagnostics;
using System.Text;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using Tam.Ebilling.Service.Modules.Core.Helper;

namespace Tam.Ebilling.Service
{
    public class BuktiPotongService : DbServiceBase
    {
        #region excel row
        public const int DATA_ROW_INDEX_START = 1;
        public const int COL_IDX_BUSINESS_UNIT = 0;
        public const int COL_IDX_NO_NPWP = 1;
        public const int COL_IDX_INVOICE_NUMBER = 2;
        public const int COL_IDX_INVOICE_DATE = 3;
        public const int COL_IDX_FLAG_POTONG_PAJAK = 4;
        public const int COL_IDX_MATERAI_AMOUNT = 5;
        public const int COL_IDX_TGL_PAYMENT = 6;
        public const int COL_IDX_NOMOR_BUKTI_POTONG = 7;
        public const int COL_IDX_TANGGAL_BUKTI_POTONG = 8;
        public const int COL_IDX_JUMLAH_DPP = 9;
        public const int COL_IDX_TARIF = 10;
        public const int COL_IDX_JUMLAH_PPH = 11;
        public const int COL_IDX_JENIS_PAJAK = 12;
        public const int COL_IDX_DESCRIPTION = 13;
        #endregion

        public BuktiPotongService(IDbHelper db) : base(db)
        {
        }

        public DataSourceResult GetViewDataSourceResult(DataSourceRequest request, string invoiceType, string customerGroup,string notifFilter = null)
        {
            var result = Db.BuktiPotongRepository.GetViewDataSourceResult(request, invoiceType, customerGroup, notifFilter);
            return result;
        }

        public List<BuktiPotong> GetBuktiPotong(string invoiceNumbers, string customerGroup)
        {
            string invoiceNoSql = string.Empty;
            if (!string.IsNullOrEmpty(invoiceNumbers))
            {
                string[] invoiceNoArr = invoiceNumbers.Split(',');
                for (int i = 0; i < invoiceNoArr.Length; i++)
                {
                    invoiceNoSql += "'" + invoiceNoArr[i] + "'";

                    if (i != (invoiceNoArr.Length - 1))
                        invoiceNoSql += ",";
                }
            }
            var result = Db.BuktiPotongRepository.GetBuktiPotong(invoiceNoSql, customerGroup);
            return result;
        }

        public List<ViewTaxTransactionCode> ViewTaxTransactionCode()
        {
            var result = Db.BuktiPotongRepository.ViewTaxTransactionCode();
            return result;
        }

        public string GetTransactionCode(string invoiceNo)
        {
            string result = Db.BuktiPotongRepository.GetTransactionCode(invoiceNo);

            return result;
        }

        public List<BuktiPotong> CheckTaxArticle(string invoiceNo)
        {
            string invoiceNoSql = string.Empty;
            if (!string.IsNullOrEmpty(invoiceNo))
            {
                string[] invoiceNoArr = invoiceNo.Split(',');
                for (int i = 0; i < invoiceNoArr.Length; i++)
                {
                    invoiceNoSql += "'" + invoiceNoArr[i] + "'";

                    if (i != (invoiceNoArr.Length - 1))
                        invoiceNoSql += ",";
                }
            }

            var result = Db.BuktiPotongRepository.CheckTaxArticle(invoiceNoSql);

            return result;
        }
        //public string GetTaxArticle(string transactionCode)
        //{
        //    string result = Db.BuktiPotongRepository.GetTaxArticle(transactionCode);

        //    return result;
        //}

        public decimal GetTarifNPWP(string transactionTypeCode)
        {
            decimal result = Db.BuktiPotongRepository.GetTarifNPWP(transactionTypeCode);

            return result;
        }

        //public int CheckNomorBuktiPotongWHT(string nomorBuktiPotong)
        //{
        //    int result = Db.BuktiPotongRepository.CheckNomorBuktiPotongWHT(nomorBuktiPotong);

        //    return result;
        //}

        public IEnumerable<BuktiPotongAttachment> GetDocumentFileByReference(string invoiceNo)
        {
            var result = Db.BuktiPotongAttachmentRepository.Find(new { invoice_number = invoiceNo });
            result = result.Where(x => x.row_status != 1);

            return result;
        }

        public IEnumerable<BuktiPotongAttachment> GetDocumentFileByUploadId(string uploadId)
        {
            var result = Db.BuktiPotongAttachmentRepository.Find(new { upload_id = uploadId });
            result = result.Where(x => x.row_status != 1);

            return result;
        }

        public BuktiPotongAttachment Upload(IPostedFile file, string invoices, out string msgerror, string documentType)
        {
            msgerror = "";
            int maxFileSize = int.Parse(ApplicationCacheManager.GetConfig<string>("MaxUploadSize")) * 1024 * 1024;
            //string fileTypeAllowed = ApplicationCacheManager.GetConfig<string>("FileTypeUpload");
            //List<string> fileType = fileTypeAllowed.Split(',').ToList();
            //bool isFileTypeAllowed = fileType.Exists(x => x.Contains(file.ContentType));

            var doc = new BuktiPotongAttachment();
            //if (isFileTypeAllowed)
            //{
            if (file.ContentLength < maxFileSize)
            {
                //var username = ApplicationCacheManager.GetConfig<string>("FileUploadUserName");
                //var password = ApplicationCacheManager.GetConfig<string>("FileUploadPassword");
                //var config = ApplicationCacheManager.GetConfig<string>("FileUploadTemp");
                //var domain = ApplicationCacheManager.GetConfig<string>("FileUploadDomain");

                var username = ApplicationCacheManager.GetConfig<string>("FileBuktiPotongUserName");
                var password = ApplicationCacheManager.GetConfig<string>("FileBuktiPotongPassword");
                var config = ApplicationCacheManager.GetConfig<string>("FileBuktiPotongFolderTemp");
                //var config = @"D:\BES\attachment\BuktiPotongTemp";
                var domain = ApplicationCacheManager.GetConfig<string>("FileBuktiPotongDomain");

                var userdomain = domain + "\\" + username;
                var fileName = Path.GetFileName(file.FileName);

                var serverFileName = fileName;
                if (documentType == "BuktiPotong")
                {
                    serverFileName = Guid.NewGuid().ToString() + "_" + fileName;
                }

                var newFileName = config + "\\" + serverFileName;
                //var newFileName = @"D:\BES\attachment\BuktiPotongTemp" + "\\" + serverFileName;
                var url = Path.Combine(newFileName);
                var dir = Path.GetDirectoryName(newFileName);

                var pers = new Impersonation();
                pers.Impersonate(userdomain, password);
                var credential = new NetworkCredential(userdomain, password);

                using (new NetworkConnection(config, credential))
                {
                    try
                    {
                        if (!Directory.Exists(dir))
                            Directory.CreateDirectory(dir);
                    }
                    catch (Exception e)
                    {

                    }
                    file.SaveAs(newFileName);
                }

                doc = new BuktiPotongAttachment
                {
                    attachment_bukti_potong_id = -1,
                    invoice_number = invoices,
                    filename = fileName,
                    server_filename = serverFileName,
                    created_by = SessionManager.Current,
                    created_date = DateTime.Now
                };
            }
            else
            {
                msgerror = "File size cannot exceed 10 MB";
            }
            //}
            //else
            //{
            //    msgerror = "File type allowed are " + fileTypeAllowed;
            //}

            return doc;
        }

        public IEnumerable<BuktiPotongAttachment> GetDocumentByID(string id)
        {
            var result = Db.BuktiPotongAttachmentRepository.Find(new { attachment_bukti_potong_id = id });

            return result;
        }

        public bool SaveUpload(string invoiceNumber, string documentType, IEnumerable<BuktiPotongAttachment> docs, out string msgerror, IList<string> errMesgs)
        {
            msgerror = "";
            //var result = true;
            var result = false;

            //long? id = null;
            //var reference = "";

            //var username = ApplicationCacheManager.GetConfig<string>("FileUploadUserName");
            //var password = ApplicationCacheManager.GetConfig<string>("FileUploadPassword");
            //var config = ApplicationCacheManager.GetConfig<string>("FileUploadTemp");
            //var domain = ApplicationCacheManager.GetConfig<string>("FileUploadDomain");

            var username = ApplicationCacheManager.GetConfig<string>("FileBuktiPotongUserName");
            var password = ApplicationCacheManager.GetConfig<string>("FileBuktiPotongPassword");
            var config = ApplicationCacheManager.GetConfig<string>("FileBuktiPotongFolderTemp");
            var domain = ApplicationCacheManager.GetConfig<string>("FileBuktiPotongDomain");

            var userdomain = domain + "\\" + username;
            var pers = new Impersonation();
            pers.Impersonate(userdomain, password);

            if (docs != null)
            {
                if (docs.Count() > 0)
                {
                    //save document file
                    string[] invoiceNumberArr = invoiceNumber.Split(',');
                    Db.Connection.Open();

                    int deleted = 0;

                    foreach (BuktiPotongAttachment doc in docs)
                    {
                        if (doc.status == "Delete")
                        {
                            deleted++;
                        }
                    }

                    foreach (BuktiPotongAttachment doc in docs)
                    {
                        if (doc.status == "New")
                        {
                            var guidStr = Guid.NewGuid().ToString();

                            var tempFile = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileBuktiPotongFolderTemp"), doc.server_filename);
                            var newFile = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileBuktiPotong"), doc.server_filename);

                            string fileName = Path.GetFileName(doc.filename);
                            string nomorBuktiPotong = Path.GetFileNameWithoutExtension(doc.filename);

                            #region comment documentType = buktipotong
                            //if (documentType == "BuktiPotong" && (Path.GetExtension(doc.filename) == ".xls" || Path.GetExtension(doc.filename) == ".xlsx"))
                            //{
                            //    //excel read    
                            //    IList<BuktiPotong> listData = getDataExcel(tempFile, errMesgs); 

                            //    foreach (var item in listData)
                            //    {
                            //        var datas = this.GetBuktiPotong(item.invoice_number, SessionManager.RoleStr);

                            //        if (datas.Exists(b => b.flag_potong_pajak != "Y") || datas.Count() == 0)
                            //        {
                            //            var transact = Db.Connection.BeginTransaction();
                            //            try
                            //            {
                            //                //var tempFile = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileBuktiPotongFolderTemp"), doc.server_filename);
                            //                //var newFile = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileBuktiPotong"), doc.server_filename);

                            //                //IList<string> errMesgs = new List<string>();

                            //                //bool isUpdate = false; 

                            //                //comment by rg
                            //                foreach (string invoiceNo in invoiceNumberArr)
                            //                {
                            //                    // Update Property
                            //                    doc.invoice_number = invoiceNo;
                            //                    doc.created_date = DateTime.Now;
                            //                    doc.created_by = SessionManager.Username;
                            //                    doc.modified_by = SessionManager.Username;
                            //                    doc.modified_date = DateTime.Now;
                            //                    doc.upload_id = guidStr;

                            //                    // Rest status
                            //                    doc.status = null;

                            //                    //IList<BuktiPotong> listData = getDataExcel(tempFile, errMesgs);

                            //                    //foreach (var item in errMesgs)
                            //                    //{
                            //                    //    CustomLog.WriteAppLog("Err_Notif_BuktiPotong", item, "Bukti Potong Service");
                            //                    //}

                            //                    if (item.invoice_number == invoiceNo)
                            //                    {
                            //                        result = true;
                            //                        //isUpdate = true;
                            //                        Db.BuktiPotongAttachmentRepository.UploadBuktiPotong(transact, doc.filename, doc.server_filename, doc.invoice_number, doc.upload_id);
                            //                        Db.BuktiPotongRepository.UpdateBuktiPotong(transact, item, SessionManager.Current);
                            //                    }
                            //                }
                            //                if (!result)
                            //                {
                            //                    errMesgs.Add(String.Format("Invoice Number: {0} doesn't match", item.invoice_number));
                            //                }
                            //                //end coooment

                            //                //var tempFile = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileUploadTemp"), doc.filename);
                            //                //var newFile = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileUpload"), doc.filename);



                            //                //comment by rg
                            //                //var tempFile = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileBuktiPotongFolderTemp"), doc.server_filename);
                            //                //var newFile = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileBuktiPotong"), doc.server_filename); 
                            //                //end coooment

                            //                //var Update = Service<InvoiceService>().GetUploadNotur(daNumber);
                            //                //var reference = reff;
                            //                var credential = new NetworkCredential(userdomain, password);
                            //                using (new NetworkConnection(tempFile, credential))
                            //                using (new NetworkConnection(newFile, credential))
                            //                {
                            //                    try
                            //                    {
                            //                        var path = Path.GetDirectoryName(newFile);
                            //                        if (!Directory.Exists(path))
                            //                            Directory.CreateDirectory(path);

                            //                        if (result)
                            //                        {
                            //                            if (File.Exists(newFile))
                            //                            {
                            //                                File.Delete(newFile);
                            //                            }

                            //                            File.Move(tempFile, newFile);
                            //                        }
                            //                        //else
                            //                        //{
                            //                        //    errMesgs.Add("Invoice number and Invoice Number Excel not match");

                            //                        //    CustomLog.WriteAppLog("Err_Notif_BuktiPotong", "Save Upload : Invoice number and Invoice Number Excel not match", "Bukti Potong Service");
                            //                        //}
                            //                    }
                            //                    catch (Exception e)
                            //                    {

                            //                    }

                            //                    //Db.NotaReturRepository.GetUploadNoturMV(doc.reference);
                            //                }
                            //                //}

                            //                transact.Commit();
                            //            }
                            //            catch (Exception)
                            //            {
                            //                result = false;
                            //                transact.Rollback();
                            //                throw;
                            //            }
                            //        }
                            //        else
                            //        {
                            //            errMesgs.Add(String.Format("Invoice No: {0} + NPWP: {1} + Tax Transaction Code: {2}' already submitted", item.invoice_number, item.no_npwp, item.jenis_pajak));
                            //        }
                            //    }

                            //    foreach (var item in errMesgs)
                            //    {
                            //        CustomLog.WriteAppLog("Err_Notif_BuktiPotong", item, "Bukti Potong Service");
                            //    }
                            //}
                            #endregion

                            if (Path.GetExtension(doc.filename) == ".pdf")
                            {
                                string configSuccess = "";
                                string configFailed = "";

                                var serverFileName = fileName;

                                var processFileName = tempFile; //tempFile == progress //Path.Combine(configProcess, serverFileName); 

                                var datas = this.GetBuktiPotong(invoiceNumber, SessionManager.RoleStr);

                                foreach (var item in datas)
                                {
                                    if (item.nomor_bukti_potong == nomorBuktiPotong)
                                    {
                                        documentType = item.tax_article;
                                        break;
                                    }
                                }

                                if (documentType == "22")
                                {
                                    configSuccess = ApplicationCacheManager.GetConfig<string>("UploadPdfWht22Success");
                                    configFailed = ApplicationCacheManager.GetConfig<string>("UploadPdfWht22Failed");
                                }
                                else if (documentType == "23")
                                {
                                    configSuccess = ApplicationCacheManager.GetConfig<string>("UploadPdfWht23Success");
                                    configFailed = ApplicationCacheManager.GetConfig<string>("UploadPdfWht23Failed");
                                }
                                else if (documentType == "4 ayat 2")
                                {
                                    configSuccess = ApplicationCacheManager.GetConfig<string>("UploadPdfWht42Success");
                                    configFailed = ApplicationCacheManager.GetConfig<string>("UploadPdfWht42Failed");
                                }
                                var newFileName = Path.Combine(configSuccess, serverFileName);
                                var failedFileName = Path.Combine(configFailed, serverFileName);
                                var dir = Path.GetDirectoryName(newFileName);

                                //cek nomor bukti potong yang di substring
                                //if (datas.Exists(b => (b.nomor_bukti_potong == nomorBuktiPotong) || (b.nomor_bukti_potong.Substring(0, 10) == nomorBuktiPotong.Substring(0, 10))))
                                if (datas.Exists(b => (b.nomor_bukti_potong == nomorBuktiPotong && (documentType == "4 ayat 2" || documentType == "22"))
                                    || (b.nomor_bukti_potong.Substring(0, 10) == nomorBuktiPotong.Substring(0, 10) && documentType == "23")
                                    ))
                                {

                                    var transact = Db.Connection.BeginTransaction();

                                    try
                                    {
                                        if (documentType != "23")
                                        {
                                            foreach (var item in datas)
                                            {
                                                if (item.tax_article != "23" && item.nomor_bukti_potong == nomorBuktiPotong)
                                                {
                                                    Db.BuktiPotongRepository.UpdatePdfWhtTMS(transact, item.invoice_number, newFileName);
                                                }
                                            }
                                        }
                                        Db.BuktiPotongRepository.UpdatePdfWht(transact, nomorBuktiPotong, documentType, newFileName);

                                        var credential = new NetworkCredential(userdomain, password);

                                        using (new NetworkConnection(configSuccess, credential))
                                        {
                                            try
                                            {
                                                if (!Directory.Exists(dir))
                                                    Directory.CreateDirectory(dir);



                                                //file.SaveAs(newFileName);
                                                if (File.Exists(processFileName))
                                                {
                                                    if (File.Exists(newFileName))
                                                        File.Delete(newFileName);
                                                    File.Move(processFileName, newFileName);
                                                }
                                            }
                                            catch (Exception e)
                                            {
                                                transact.Rollback();
                                                result = false;


                                                if (File.Exists(processFileName))
                                                {
                                                    if (File.Exists(failedFileName))
                                                        File.Delete(failedFileName);
                                                    File.Move(processFileName, failedFileName);
                                                }

                                                throw;
                                            }
                                        }

                                        result = true;
                                        transact.Commit();
                                    }
                                    catch (Exception)
                                    {
                                        transact.Rollback();
                                        result = false;

                                        if (File.Exists(processFileName))
                                        {
                                            if (File.Exists(failedFileName))
                                                File.Delete(failedFileName);
                                            File.Move(processFileName, failedFileName);
                                        }
                                        throw;
                                    }
                                }
                                else if (datas.Exists(b => b.nomor_bukti_potong != "NULL" || b.nomor_bukti_potong != null))
                                {
                                    if (File.Exists(failedFileName))
                                        File.Delete(failedFileName);

                                    //File.Copy(processFileName, failedFileName);

                                    errMesgs.Add(String.Format("{0} Withholding Number is doesn't match", nomorBuktiPotong));
                                    CustomLog.WriteAppLog("Err_Notif_BuktiPotong", string.Format("Save Upload Pdf Wht {0} : FileName : {1}, Witholding Tax Number : {2} is Invalid", documentType, fileName, nomorBuktiPotong), "Bukti Potong Service");
                                }
                                else
                                {
                                    if (File.Exists(failedFileName))
                                        File.Delete(failedFileName);

                                    //File.Copy(processFileName, failedFileName);

                                    errMesgs.Add("WHT Number doesn't exist or Uncomplete Worklist Data");
                                    CustomLog.WriteAppLog("Err_Notif_BuktiPotong", string.Format("Save Upload Pdf Wht {0} : FileName : {1}, WHT Number doesn't exist or Uncomplete Worklist Data", documentType, fileName), "Bukti Potong Service");
                                }

                                #region comment
                                //var transact = Db.Connection.BeginTransaction();
                                //try
                                //{ 
                                //    //comment by rg
                                //    foreach (string invoiceNo in invoiceNumberArr)
                                //    {
                                //        // Update Property
                                //        doc.invoice_number = invoiceNo;
                                //        doc.created_date = DateTime.Now;
                                //        doc.created_by = SessionManager.Username;
                                //        doc.modified_by = SessionManager.Username;
                                //        doc.modified_date = DateTime.Now;
                                //        doc.upload_id = guidStr;

                                //        // Rest status
                                //        doc.status = null;

                                //        result = true;
                                //        //isUpdate = true;
                                //        Db.BuktiPotongAttachmentRepository.UploadBuktiPotong(transact, doc.filename, doc.server_filename, doc.invoice_number, doc.upload_id);
                                //        // Db.BuktiPotongRepository.UpdateBuktiPotong(transact, item, SessionManager.Current);
                                //        if (documentType == "Pdf22")
                                //        {

                                //        }
                                //        else if (documentType == "Pdf23")
                                //        {

                                //        }
                                //        else if (documentType == "Pdf42")
                                //        {

                                //        }  
                                //    } 
                                //    var credential = new NetworkCredential(userdomain, password);
                                //    using (new NetworkConnection(tempFile, credential))
                                //    using (new NetworkConnection(newFile, credential))
                                //    {
                                //        try
                                //        {
                                //            var path = Path.GetDirectoryName(newFile);
                                //            if (!Directory.Exists(path))
                                //                Directory.CreateDirectory(path);

                                //            if (result)
                                //            {
                                //                if (File.Exists(newFile))
                                //                {
                                //                    File.Delete(newFile);
                                //                }

                                //                File.Move(tempFile, newFile);
                                //            } 
                                //        }
                                //        catch (Exception e)
                                //        {

                                //        } 
                                //    } 
                                //    transact.Commit();
                                //}
                                //catch (Exception)
                                //{
                                //    result = false;
                                //    transact.Rollback();
                                //    throw;
                                //}
                                #endregion
                            }
                            else
                            {
                                errMesgs.Add("Document Type incorrect");

                                CustomLog.WriteAppLog("Err_Notif_BuktiPotong", string.Format("Save Upload Pdf Wht : FileName : {0}, Nomor Bukti Potong : {1} not found...", fileName, nomorBuktiPotong), "Bukti Potong Service");
                            }
                        }
                        #region comment
                        //else if (doc.status == "Delete" && documentType == "BuktiPotong")
                        //{
                        //    if (doc.invoice_number == default(string))
                        //    {
                        //        // Delete physical file
                        //        //var tempFile = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileUploadTemp"), doc.filename);
                        //        var tempFile = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileBuktiPotongFolderTemp"), doc.server_filename);
                        //        var credential = new NetworkCredential(userdomain, password);
                        //        using (new NetworkConnection(tempFile, credential))
                        //        {
                        //            try
                        //            {
                        //                result = true;
                        //                File.Delete(tempFile);
                        //            }
                        //            catch (Exception e)
                        //            {

                        //            }
                        //        }
                        //    }
                        //    else
                        //    {

                        //        var transact = Db.Connection.BeginTransaction();
                        //        try
                        //        {
                        //            //Delete data
                        //            Db.BuktiPotongAttachmentRepository.Remove(transact, doc.attachment_bukti_potong_id);
                        //            //if (doc.reference.HasValue)
                        //            //{
                        //            //    var dattodel = dbhelper.AttachmentNotaReturRepository.Find(new { reference = reference.Value, filepath = doc.filepath }).FirstOrDefault();
                        //            //    if (dattodel != null)
                        //            //    {
                        //            //        dbhelper.AttachmentNotaReturRepository.Remove(dattodel);
                        //            //    }
                        //            //}

                        //            // Delete physical file

                        //            //var file = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileUpload"), doc.filename);
                        //            var file = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileBuktiPotong"), doc.server_filename);
                        //            var credential = new NetworkCredential(userdomain, password);
                        //            using (new NetworkConnection(file, credential))
                        //            {
                        //                File.Delete(file);
                        //            }

                        //            result = true;
                        //            transact.Commit();
                        //        }
                        //        catch (Exception)
                        //        {
                        //            result = false;
                        //            transact.Rollback();
                        //            throw;
                        //        }
                        //    }
                        //}
                        #endregion
                        else if (doc.status == "Delete" && docs.Count() == deleted)
                        {
                            result = false;
                            errMesgs.Add("WHT pdf cannot be empty");
                            //break;
                        }
                    }
                    Db.Connection.Close();
                }
            }

            return result;
        }

        private IList<BuktiPotong> getDataExcel(string tempFile, IList<string> errMesgs)
        {
            XSSFWorkbook workbook = new XSSFWorkbook(tempFile);

            if (workbook == null)
            {
                throw new ArgumentNullException("Cannot create Workbook object from excel file ");
            }

            IRow row = null;
            ICell cell = null;
            BuktiPotong data = null;
            IList<BuktiPotong> listData = new List<BuktiPotong>();

            int indexRow = DATA_ROW_INDEX_START;
            bool isAllCellEmpty = true;
            bool isBreak = false;

            ISheet sheet = workbook.GetSheetAt(0);

            if (sheet.LastRowNum < 1)
            {
                errMesgs.Add(string.Format("File {0} cannot be empty", tempFile));
                return listData;
            }

            for (indexRow = DATA_ROW_INDEX_START; indexRow <= sheet.LastRowNum; indexRow++)
            {
                decimal tarifNpwp = 0;

                data = new BuktiPotong();
                isAllCellEmpty = true;
                isBreak = false;
                row = sheet.GetRow(indexRow);

                if (row != null) //null is when the row only contains empty cells 
                {
                    bool mandatory = true;

                    try
                    {
                        cell = row.GetCell(COL_IDX_FLAG_POTONG_PAJAK);
                        if (cell != null)
                        {
                            if (cell.CellType == CellType.Blank)
                            {
                                errMesgs.Add(string.Format("Flag Potong Pajak row {0} is required", indexRow + 1));
                                return listData;
                            }
                            else if (cell.CellType == CellType.String)
                            {
                                if (cell.StringCellValue == "N")
                                {
                                    DateTime dateDefault = DateTime.ParseExact("01-01-1900", "dd-mm-yyyy", null);

                                    data.tanggal_bukti_potong = dateDefault;
                                    data.invoice_date = dateDefault;
                                    data.tgl_payment = dateDefault;

                                    mandatory = false;
                                }
                                data.flag_potong_pajak = cell.StringCellValue;

                                isAllCellEmpty = false;
                            }
                            else if (mandatory)
                            {
                                errMesgs.Add(string.Format("Flag Potong Pajak format is incorrect"));
                                return listData;
                            }
                        }
                        else if (mandatory)
                        {
                            errMesgs.Add(string.Format("Flag Potong Pajak row {0} is required", indexRow + 1));
                            return listData;
                        }
                    }
                    catch (Exception ex)
                    {
                        errMesgs.Add(string.Format("Unable to get value of Flag Potong Pajak at row {0}, Error Mesg : {1}",
                            indexRow + 1, ex.Message));
                        return listData;
                    }

                    //Business Unit
                    try
                    {
                        cell = row.GetCell(COL_IDX_BUSINESS_UNIT);
                        if (cell != null)
                        {
                            if (cell.CellType == CellType.Blank)
                            {
                                errMesgs.Add(string.Format("Business Unit row {0} is required", indexRow + 1));
                                return listData;
                            }
                            else if (cell.CellType == CellType.String)
                            {
                                data.business_unit = cell.StringCellValue;

                                isAllCellEmpty = false;
                            }
                            else if (cell.CellType == CellType.Numeric)
                            {
                                data.business_unit = cell.DateCellValue.ToString();

                                isAllCellEmpty = false;
                            }
                            else if (mandatory)
                            {
                                errMesgs.Add(string.Format("Business Unit format is incorrect"));
                                return listData;
                            }

                            string[] bisinisUnit = { "Lexus-Motor Vehicle", "Lexus-Workshop", "Motor Vehicle", "Others", "Service Part", "Workshop" };

                            if (!bisinisUnit.Contains(data.business_unit))
                            {
                                errMesgs.Add(string.Format("Business Unit row {0} invalid value", indexRow + 1));
                                return listData;
                            }
                        }
                        else if (mandatory)
                        {
                            errMesgs.Add(string.Format("Business Unit row {0} is required", indexRow + 1));
                            return listData;
                        }
                    }
                    catch (Exception ex)
                    {
                        errMesgs.Add(string.Format("Unable to get value of Business Unit at row {0}, Error Mesg : {1}",
                            indexRow + 1, ex.Message));
                        return listData;
                    }

                    //Npwp
                    try
                    {
                        cell = row.GetCell(COL_IDX_NO_NPWP);
                        if (cell != null)
                        {
                            if (cell.CellType == CellType.Blank)
                            {
                                errMesgs.Add(string.Format("No NPWP row {0} is required", indexRow + 1));
                                return listData;
                            }
                            else if (cell.CellType == CellType.String)
                            {
                                data.no_npwp = cell.StringCellValue;

                                isAllCellEmpty = false;
                            }
                            else if (cell.CellType == CellType.Numeric)
                            {
                                data.no_npwp = cell.DateCellValue.ToString();

                                isAllCellEmpty = false;
                            }
                            else if (mandatory)
                            {
                                errMesgs.Add(string.Format("No NPWP format is incorrect"));
                                return listData;
                            }
                        }
                        else if (mandatory)
                        {
                            errMesgs.Add(string.Format("No NPWP row {0} is required", indexRow + 1));
                            return listData;
                        }
                    }
                    catch (Exception ex)
                    {
                        errMesgs.Add(string.Format("Unable to get value of No NPWP at row {0}, Error Mesg : {1}",
                            indexRow + 1, ex.Message));
                        return listData;
                    }

                    //InvoiceNumber
                    try
                    {
                        cell = row.GetCell(COL_IDX_INVOICE_NUMBER);
                        if (cell != null)
                        {
                            if (cell.CellType == CellType.Blank)
                            {
                                errMesgs.Add(string.Format("Invoice Number row {0} is required", indexRow + 1));
                                return listData;
                            }
                            else if (cell.CellType == CellType.String)
                            {
                                data.invoice_number = cell.StringCellValue;
                                isAllCellEmpty = false;
                            }
                            else if (cell.CellType == CellType.Numeric)
                            {
                                data.invoice_number = cell.NumericCellValue.ToString();
                                isAllCellEmpty = false;
                            }
                            else if (mandatory)
                            {
                                errMesgs.Add(string.Format("Invoice Number row {0} is Incorrect Format", indexRow + 1));
                                return listData;
                            }
                        }
                        else if (mandatory)
                        {
                            errMesgs.Add(string.Format("Invoice Number row {0} is required", indexRow + 1));
                            return listData;
                        }
                    }
                    catch (Exception ex)
                    {
                        errMesgs.Add(string.Format("Unable to get value of Invoice Number at row {0}, Error Mesg : {1}",
                            indexRow + 1, ex.Message));
                        return listData;
                    }

                    //InvoiceDate
                    try
                    {
                        cell = row.GetCell(COL_IDX_INVOICE_DATE);
                        if (cell != null)
                        {
                            if (cell.CellType == CellType.Blank)
                            {
                                errMesgs.Add(string.Format("Invoice Date row {0} is required", indexRow + 1));
                                return listData;
                            }
                            else if (cell.CellType == CellType.String)
                            {
                                string dt = cell.StringCellValue;
                                try
                                {
                                    data.invoice_date = DateTime.ParseExact(dt, "dd/MM/yyyy", null);
                                }
                                catch (FormatException fe)
                                {
                                    throw new Exception(String.Format("Invoice Date at Row {0} must dd/MM/yyyy", indexRow + 1));
                                }

                                isAllCellEmpty = false;
                            }
                            else if (cell.CellType == CellType.Numeric)
                            {
                                data.invoice_date = cell.DateCellValue;

                                isAllCellEmpty = false;
                            }
                            else if (mandatory)
                            {
                                errMesgs.Add(string.Format("Invoice Date format is incorrect"));
                                return listData;
                            }
                        }
                        else if (mandatory)
                        {
                            errMesgs.Add(string.Format("Invoice Date row {0} is required", indexRow + 1));
                            return listData;
                        }
                    }
                    catch (Exception ex)
                    {
                        errMesgs.Add(string.Format("Invoice Date at row {0}, Error Mesg : {1}",
                            indexRow + 1, ex.Message));
                        return listData;
                    }

                    //MateraiAmount
                    try
                    {
                        cell = row.GetCell(COL_IDX_MATERAI_AMOUNT);
                        if (cell != null)
                        {
                            if (cell.CellType == CellType.Blank && mandatory)
                            {
                                errMesgs.Add(string.Format("Materai Amount row {0} is required", indexRow + 1));
                                return listData;
                            }
                            else if (cell.CellType == CellType.String && mandatory)
                            {
                                data.materai_amount = decimal.Parse(cell.StringCellValue);

                                isAllCellEmpty = false;
                            }
                            else if (cell.CellType == CellType.Numeric && mandatory)
                            {
                                data.materai_amount = (decimal)cell.NumericCellValue;

                                isAllCellEmpty = false;
                            }
                            else if ((cell.CellType == CellType.String || cell.CellType == CellType.Numeric) && !mandatory)
                            {
                                errMesgs.Add(string.Format("Materai Amount row {0} if Tax Object is N value must be blank", indexRow + 1));
                                return listData;
                            }
                            else if (mandatory)
                            {
                                errMesgs.Add(string.Format("Materai Amount format is incorrect"));
                                return listData;
                            }
                        }
                        else if (mandatory)
                        {
                            errMesgs.Add(string.Format("Materai Amount row {0} is required", indexRow + 1));
                            return listData;
                        }
                    }
                    catch (Exception ex)
                    {
                        errMesgs.Add(string.Format("Unable to get value of Materai Amount at row {0}, Error Mesg : {1}",
                            indexRow + 1, ex.Message));
                        return listData;
                    }

                    //TanggalPayment
                    try
                    {
                        cell = row.GetCell(COL_IDX_TGL_PAYMENT);
                        if (cell != null)
                        {
                            if (cell.CellType == CellType.Blank && mandatory)
                            {
                                errMesgs.Add(string.Format("Payment Date row {0} is required", indexRow + 1));
                                return listData;
                            }
                            else if (cell.CellType == CellType.String && mandatory)
                            {
                                string dt = cell.StringCellValue;
                                try
                                {
                                    data.tgl_payment = DateTime.ParseExact(dt, "dd/MM/yyyy", null);
                                }
                                catch (FormatException fe)
                                {
                                    throw new Exception(String.Format("Payment Date at Row {0} must dd/MM/yyyy", indexRow + 1));
                                }

                                isAllCellEmpty = false;
                            }
                            else if (cell.CellType == CellType.Numeric && mandatory)
                            {
                                data.tgl_payment = cell.DateCellValue;

                                isAllCellEmpty = false;
                            }
                            else if ((cell.CellType == CellType.String || cell.CellType == CellType.Numeric) && !mandatory)
                            {
                                errMesgs.Add(string.Format("Payment Date {0} if Tax Object is N value must be blank", indexRow + 1));
                                return listData;
                            }
                            else if (mandatory)
                            {
                                errMesgs.Add(string.Format("Payment Date format is incorrect"));
                                return listData;
                            }
                        }
                        else if (mandatory)
                        {
                            errMesgs.Add(string.Format("Payment Date row {0} is required", indexRow + 1));
                            return listData;
                        }
                    }
                    catch (Exception ex)
                    {
                        errMesgs.Add(string.Format("Payment Date at row {0}, Error Mesg : {1}",
                            indexRow + 1, ex.Message));
                        return listData;
                    }

                    //NomorBuktiPotong
                    try
                    {
                        cell = row.GetCell(COL_IDX_NOMOR_BUKTI_POTONG);
                        if (cell != null)
                        {
                            if (cell.CellType == CellType.Blank && mandatory)
                            {
                                errMesgs.Add(string.Format("Witholding Tax No row {0} is required", indexRow + 1));
                                return listData;
                            }
                            else if (cell.CellType == CellType.String && mandatory)
                            {
                                data.nomor_bukti_potong = cell.StringCellValue;
                                isAllCellEmpty = false;
                            }
                            else if (cell.CellType == CellType.Numeric && mandatory)
                            {
                                data.nomor_bukti_potong = cell.NumericCellValue.ToString();

                                isAllCellEmpty = false;
                            }
                            else if (cell.CellType == CellType.String && !mandatory)
                            {
                                errMesgs.Add(string.Format("Witholding Tax No row {0} if Tax Object is N value must be blank", indexRow + 1));
                                return listData;
                            }
                            else if (mandatory)
                            {
                                errMesgs.Add(string.Format("Witholding Tax No row {0} is Incorrect Format", indexRow + 1));
                                return listData;
                            }
                        }
                        else if (mandatory)
                        {
                            errMesgs.Add(string.Format("Witholding Tax No row {0} is required", indexRow + 1));
                            return listData;
                        }
                    }
                    catch (Exception ex)
                    {
                        errMesgs.Add(string.Format("Unable to get value of Witholding Tax No at row {0}, Error Mesg : {1}",
                            indexRow + 1, ex.Message));
                        return listData;
                    }

                    //TanggalBuktiPotong
                    try
                    {
                        cell = row.GetCell(COL_IDX_TANGGAL_BUKTI_POTONG);
                        if (cell != null)
                        {
                            if (cell.CellType == CellType.Blank && mandatory)
                            {
                                errMesgs.Add(string.Format("Witholding Tax Date row {0} is required", indexRow + 1));
                                return listData;
                            }
                            else if (cell.CellType == CellType.String && mandatory)
                            {
                                string dt = cell.StringCellValue;
                                try
                                {
                                    data.tanggal_bukti_potong = DateTime.ParseExact(dt, "dd/MM/yyyy", null);
                                }
                                catch (FormatException fe)
                                {
                                    throw new Exception(String.Format("Withholding Tax Date at Row {0} must dd/MM/yyyy", indexRow + 1));
                                }

                                isAllCellEmpty = false;
                            }
                            else if (cell.CellType == CellType.Numeric && mandatory)
                            {
                                data.tanggal_bukti_potong = cell.DateCellValue;

                                isAllCellEmpty = false;
                            }
                            else if ((cell.CellType == CellType.String || cell.CellType == CellType.Numeric) && !mandatory)
                            {
                                errMesgs.Add(string.Format("Witholding Tax Date row {0} if Tax Object is N value must be blank", indexRow + 1));
                                return listData;
                            }
                            else if (mandatory)
                            {
                                errMesgs.Add(string.Format("Witholding Tax Date format is incorrect"));
                                return listData;
                            }
                        }
                        else if (mandatory)
                        {
                            errMesgs.Add(string.Format("Witholding Tax Date row {0} is required", indexRow + 1));
                            return listData;
                        }
                    }
                    catch (Exception ex)
                    {
                        errMesgs.Add(string.Format("Witholding Tax Date at row {0}, Error Mesg : {1}",
                            indexRow + 1, ex.Message));
                        return listData;
                        //break;
                    }

                    //JumlahDPP
                    try
                    {
                        cell = row.GetCell(COL_IDX_JUMLAH_DPP);
                        if (cell != null)
                        {
                            if (cell.CellType == CellType.Blank && mandatory)
                            {
                                errMesgs.Add(string.Format("WHT Based Amount row {0} is required", indexRow + 1));
                                return listData;
                            }
                            else if (cell.CellType == CellType.String && mandatory)
                            {
                                data.jumlah_dpp = decimal.Parse(cell.StringCellValue);

                                isAllCellEmpty = false;
                            }
                            else if (cell.CellType == CellType.Numeric && mandatory)
                            {
                                data.jumlah_dpp = (decimal)cell.NumericCellValue;

                                isAllCellEmpty = false;
                            }
                            else if ((cell.CellType == CellType.String || cell.CellType == CellType.Numeric) && !mandatory)
                            {
                                errMesgs.Add(string.Format("WHT Based Amount row {0} if Tax Object is N value must be blank", indexRow + 1));
                                return listData;
                            }
                            else if (mandatory)
                            {
                                errMesgs.Add(string.Format("WHT Based Amount format is incorrect"));
                                return listData;
                            }
                        }
                        else if (mandatory)
                        {
                            errMesgs.Add(string.Format("WHT Based Amount row {0} is required", indexRow + 1));
                            return listData;
                        }
                    }
                    catch (Exception ex)
                    {
                        errMesgs.Add(string.Format("Unable to get value of WHT Based Amount at row {0}, Error Mesg : {1}",
                            indexRow + 1, ex.Message));
                        return listData;
                    }

                    //JenisPajak = Tax Transaction Code
                    try
                    {
                        cell = row.GetCell(COL_IDX_JENIS_PAJAK);
                        if (cell != null)
                        {
                            if (cell.CellType == CellType.Blank && mandatory)
                            {
                                errMesgs.Add(string.Format("Tax Transaction Code row {0} is required", indexRow + 1));
                                return listData;
                            }
                            else if (cell.CellType == CellType.String && mandatory)
                            {
                                data.jenis_pajak = cell.StringCellValue;
                                isAllCellEmpty = false;

                                decimal cekTransactionTypeCode = Db.BuktiPotongRepository.GetTarifNPWP(data.jenis_pajak);

                                if (cekTransactionTypeCode == 0)
                                {
                                    errMesgs.Add(string.Format("Tax Transaction Code row {0} not found", indexRow + 1));
                                    return listData;
                                }

                                tarifNpwp = cekTransactionTypeCode;
                            }
                            else if (cell.CellType == CellType.Numeric && mandatory)
                            {
                                data.jenis_pajak = cell.NumericCellValue.ToString();

                                isAllCellEmpty = false;

                                decimal cekTransactionTypeCode = Db.BuktiPotongRepository.GetTarifNPWP(data.jenis_pajak);

                                if (cekTransactionTypeCode == 0)
                                {
                                    errMesgs.Add(string.Format("Tax Transaction Code row {0} not found", indexRow + 1));
                                    return listData;
                                }

                                tarifNpwp = cekTransactionTypeCode;
                            }
                            else if ((cell.CellType == CellType.String || cell.CellType == CellType.Numeric) && !mandatory)
                            {
                                errMesgs.Add(string.Format("Tax Transaction Code row {0} if Tax Object is N value must be blank", indexRow + 1));
                                return listData;
                            }
                            else if (mandatory)
                            {
                                errMesgs.Add(string.Format("Tax Transaction Code row {0} is Incorrect Format", indexRow + 1));
                                return listData;
                            }
                        }
                        else if (mandatory)
                        {
                            errMesgs.Add(string.Format("Tax Transaction Code row {0} is required", indexRow + 1));
                            return listData;
                        }
                    }
                    catch (Exception ex)
                    {
                        errMesgs.Add(string.Format("Unable to get value of Tax Transaction Code at row {0}, Error Mesg : {1}",
                            indexRow + 1, ex.Message));
                        return listData;
                    }

                    //Tarif
                    try
                    {
                        cell = row.GetCell(COL_IDX_TARIF);
                        if (cell != null)
                        {
                            if (cell.CellType == CellType.Blank && mandatory)
                            {
                                errMesgs.Add(string.Format("WHT Tarif row {0} is required", indexRow + 1));
                                return listData;
                            }
                            else if (cell.CellType == CellType.String && mandatory)
                            {
                                data.tarif = decimal.Parse(cell.StringCellValue);

                                isAllCellEmpty = false;

                                if (data.tarif != tarifNpwp)
                                {
                                    errMesgs.Add(string.Format("WHT Tarif row {0} is incorrect", indexRow + 1));
                                    return listData;
                                }
                            }
                            else if (cell.CellType == CellType.Numeric && mandatory)
                            {
                                data.tarif = (decimal)cell.NumericCellValue;

                                isAllCellEmpty = false;

                                if (data.tarif != tarifNpwp)
                                {
                                    errMesgs.Add(string.Format("WHT Tarif row {0} is incorrect", indexRow + 1));
                                    return listData;
                                }
                            }
                            else if ((cell.CellType == CellType.String || cell.CellType == CellType.Numeric) && !mandatory)
                            {
                                errMesgs.Add(string.Format("WHT Tarif row {0} if Tax Object is N value must be blank", indexRow + 1));
                                return listData;
                            }
                            else if (mandatory)
                            {
                                errMesgs.Add(string.Format("WHT Tarif format is incorrect"));
                                return listData;
                            }
                        }
                        else if (mandatory)
                        {
                            errMesgs.Add(string.Format("WHT Tarif row {0} is required", indexRow + 1));
                            return listData;
                        }
                    }
                    catch (Exception ex)
                    {
                        errMesgs.Add(string.Format("Unable to get value of WHT Tarif at row {0}, Error Mesg : {1}",
                            indexRow + 1, ex.Message));
                        return listData;
                    }

                    //JumlahPPh
                    try
                    {
                        cell = row.GetCell(COL_IDX_JUMLAH_PPH);
                        if (cell != null)
                        {
                            if (cell.CellType == CellType.Blank && mandatory)
                            {
                                errMesgs.Add(string.Format("WHT Amount row {0} is required", indexRow + 1));
                                return listData;
                            }
                            else if (cell.CellType == CellType.String && mandatory)
                            {
                                data.jumlah_pph = decimal.Parse(cell.StringCellValue);

                                isAllCellEmpty = false;
                            }
                            else if (cell.CellType == CellType.Numeric && mandatory)
                            {
                                data.jumlah_pph = (decimal)cell.NumericCellValue;

                                isAllCellEmpty = false;
                            }
                            else if ((cell.CellType == CellType.String || cell.CellType == CellType.Numeric) && !mandatory)
                            {
                                errMesgs.Add(string.Format("WHT Amount row {0} if Tax Object is N value must be blank", indexRow + 1));
                                return listData;
                            }
                            else if (mandatory)
                            {
                                errMesgs.Add(string.Format("WHT Amount format is incorrect"));
                                return listData;
                            }
                        }
                        else if (mandatory)
                        {
                            errMesgs.Add(string.Format("WHT Amount row {0} is required", indexRow + 1));
                            return listData;
                        }
                    }
                    catch (Exception ex)
                    {
                        errMesgs.Add(string.Format("Unable to get value of WHT Amount at row {0}, Error Mesg : {1}",
                            indexRow + 1, ex.Message));
                        return listData;
                    }

                    //Description
                    try
                    {
                        cell = row.GetCell(COL_IDX_DESCRIPTION);
                        if (cell != null)
                        {
                            if (cell.CellType == CellType.Blank && mandatory)
                            {
                                errMesgs.Add(string.Format("Description row {0} is required", indexRow + 1));
                                return listData;
                            }
                            else if (cell.CellType == CellType.String && mandatory)
                            {
                                data.description = cell.StringCellValue;

                                isAllCellEmpty = false;
                            }
                            else if (cell.CellType == CellType.String && !mandatory)
                            {
                                errMesgs.Add(string.Format("Description row {0} if Tax Object is N value must be blank", indexRow + 1));
                                return listData;
                            }
                            else if (mandatory)
                            {
                                errMesgs.Add(string.Format("Description format is incorrect"));
                                return listData;
                            }
                        }
                        else if (mandatory)
                        {
                            errMesgs.Add(string.Format("Description row {0} is required", indexRow + 1));
                            return listData;
                        }
                    }
                    catch (Exception ex)
                    {
                        errMesgs.Add(string.Format("Unable to get value of Description at row {0}, Error Mesg : {1}",
                            indexRow + 1, ex.Message));
                        return listData;
                    }

                    listData.Add(data);
                }
            }

            return listData;
        }

        public bool SaveUploadListBuktiPotong(IList<BuktiPotong> buktiPotongs, string user, IList<string> errMesgs)
        {
            var result = false;

            List<BuktiPotong> getDataBuktiPotong = new List<BuktiPotong>();

            foreach (BuktiPotong bp in buktiPotongs)
            {
                var datas = this.GetBuktiPotong(bp.invoice_number, SessionManager.RoleStr);

                foreach (var item in datas)
                {
                    getDataBuktiPotong.Add(item);
                }
            }

            Db.Connection.Open();
            var transact = Db.Connection.BeginTransaction();
            try
            {
                foreach (BuktiPotong bp in buktiPotongs)
                {
                    Db.BuktiPotongRepository.insertBuktiPotong(transact, bp, user);
                }
                transact.Commit();
                result = true;
            }
            catch (Exception)
            {
                result = false;
                transact.Rollback();
                throw;
            }

            Db.Connection.Close();

            return result;
        }

        public void SendEmailNotification(IList<BuktiPotong> buktiPotongs)
        {
            string username, password, domain, host, port, subject, body, from, url;
            //List<string> to = new List<string>();

            try
            {
                username = Db.ParameterRepository.Find(new { key_param = "email_username_chat_notification" }).FirstOrDefault().value1;
                password = Db.ParameterRepository.Find(new { key_param = "email_password_chat_notification" }).FirstOrDefault().value1;
                domain = Db.ParameterRepository.Find(new { key_param = "email_domain_chat_notification" }).FirstOrDefault().value1;
                host = Db.ParameterRepository.Find(new { key_param = "email_host_chat_notification" }).FirstOrDefault().value1;
                port = Db.ParameterRepository.Find(new { key_param = "email_port_chat_notification" }).FirstOrDefault().value1;
                subject = Db.EmailTemplateRepository.Find(new { module = "Upload Bukti Potong Email Reminder", mail_key = "email_key" }).FirstOrDefault().subject;
                body = Db.EmailTemplateRepository.Find(new { module = "Upload Bukti Potong Email Reminder", mail_key = "email_key" }).FirstOrDefault().mail_content;
                from = Db.ParameterRepository.Find(new { key_param = "email_username" }).FirstOrDefault().value1;
                url = Db.ParameterRepository.Find(new { key_param = "bukti_potong_url" }).FirstOrDefault().value1;

                body = body.Replace("{url}", url);

                foreach (BuktiPotong bp in buktiPotongs)
                {
                    HashSet<string> to = new HashSet<string>();
                    Invoice inv = Db.InvoiceRepository.GetSingleInvoiceByInvoiceNumber(EnumHelper.GetDescription(InvoiceType.Invoice), bp.invoice_number);
                    var users = Db.UserRepository.Find(new { customer_group = inv.customer_group });

                    foreach (var usr in users)
                    {
                        //if (!Debugger.IsAttached)
                        //{
                        if (!string.IsNullOrEmpty(usr.email))
                        {
                            to.Add(usr.email);
                        }
                        //}
                        //else
                        //{
                        //    to.Add(Db.ParameterRepository.Find(new { key_param = "email_to_chat_notification" }).FirstOrDefault().value1);
                        //}
                    }

                    //EmailService emailSvc = new EmailService();
                    SmtpClient emailSvc = new SmtpClient();
                    emailSvc.Host = host;
                    emailSvc.Port = int.Parse(port);
                    //emailSvc.User = username;
                    //emailSvc.Password = password;
                    emailSvc.UseDefaultCredentials = false;
                    emailSvc.Credentials = new NetworkCredential(username, password);
                    emailSvc.EnableSsl = false;
                    if (from.Contains("gmail")) { emailSvc.EnableSsl = true; }

                    //EmailMessage emailMessage = new EmailMessage();
                    MailMessage emailMessage = new MailMessage(from, from);
                    emailMessage.From = new MailAddress(from);
                    emailMessage.To.RemoveAt(0);
                    emailMessage.Subject = subject;
                    emailMessage.Body = body;
                    emailMessage.IsBodyHtml = true;

                    if (to.Count > 0)
                    {
                        foreach (var mail in to)
                        {
                            //emailMessage.Recipients.Add(mail);
                            emailMessage.To.Add(new MailAddress(mail));
                        }

                        emailSvc.Send(emailMessage);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool SendEmailNotification(string invoiceNumbers)
        {
            bool result = false;
            string username, password, domain, host, port, subject, from, url;


            try
            {
                username = Db.ParameterRepository.Find(new { key_param = "email_username_chat_notification" }).FirstOrDefault().value1;
                password = Db.ParameterRepository.Find(new { key_param = "email_password_chat_notification" }).FirstOrDefault().value1;
                domain = Db.ParameterRepository.Find(new { key_param = "email_domain_chat_notification" }).FirstOrDefault().value1;
                host = Db.ParameterRepository.Find(new { key_param = "email_host_chat_notification" }).FirstOrDefault().value1;
                port = Db.ParameterRepository.Find(new { key_param = "email_port_chat_notification" }).FirstOrDefault().value1;
                subject = Db.EmailTemplateRepository.Find(new { module = "Notification Worklist Available", mail_key = "email_key" }).FirstOrDefault().subject;

                from = Db.ParameterRepository.Find(new { key_param = "email_username" }).FirstOrDefault().value1;
                url = Db.ParameterRepository.Find(new { key_param = "bukti_potong_url" }).FirstOrDefault().value1;

                List<BuktiPotong> buktiPotong = this.GetBuktiPotong(invoiceNumbers, SessionManager.RoleStr);

                List<BuktiPotong> groupedCustomerBuktiPotong = buktiPotong
                        .GroupBy(b => b.customer_group)
                        .Select(s => s.First())
                        .ToList();

                foreach (BuktiPotong bp in groupedCustomerBuktiPotong)
                {
                    HashSet<string> to = new HashSet<string>();
                    string body = BuildBody(url, buktiPotong, bp);

                    var users = Db.UserRepository.Find(new { customer_group = bp.customer_group });

                    foreach (var usr in users)
                    {
                        //if (!System.Diagnostics.Debugger.IsAttached)
                        //{
                        //    if (!string.IsNullOrEmpty(usr.email))
                        //    {
                        to.Add(usr.email);
                        //    }
                        //}
                        //else
                        //{
                        //    to.Add(Db.ParameterRepository.Find(new { key_param = "email_to_chat_notification" }).FirstOrDefault().value1);
                        //}
                    }

                    //EmailService emailSvc = new EmailService();
                    SmtpClient emailSvc = new SmtpClient();
                    emailSvc.Host = host;
                    emailSvc.Port = int.Parse(port);
                    //emailSvc.User = username;
                    //emailSvc.Password = password;
                    emailSvc.UseDefaultCredentials = false;
                    emailSvc.Credentials = new NetworkCredential(username, password);
                    emailSvc.EnableSsl = false;
                    if (from.Contains("gmail")) { emailSvc.EnableSsl = true; }

                    //EmailMessage emailMessage = new EmailMessage();
                    MailMessage emailMessage = new MailMessage(from, from);
                    emailMessage.From = new MailAddress(from);
                    emailMessage.To.RemoveAt(0);
                    emailMessage.Subject = subject;
                    emailMessage.Body = body;
                    emailMessage.IsBodyHtml = true;

                    if (to.Count > 0)
                    {
                        foreach (var mail in to)
                        {
                            //emailMessage.Recipients.Add(mail);
                            emailMessage.To.Add(new MailAddress(mail));
                        }

                        emailSvc.Send(emailMessage);
                    }
                }
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
            }

            return result;
        }

        private string BuildBody(string url, List<BuktiPotong> dueDateBuktiPotong, BuktiPotong distinctBuktiPotonog)
        {
            StringBuilder stringBuilder = new StringBuilder();
            string body = Db.EmailTemplateRepository.Find(new { module = "Notification Worklist Available", mail_key = "email_key" }).FirstOrDefault().mail_content;
            body = body.Replace("{url}", url);
            body = body.Replace("{NamaDealer}", distinctBuktiPotonog.customer_name);
            foreach (BuktiPotong bpDetail in dueDateBuktiPotong.Where(b => b.customer_group == distinctBuktiPotonog.customer_group))
            {
                stringBuilder.AppendFormat("<tr><td>{0}</td></tr>", bpDetail.invoice_number);
            }

            body = body.Replace("{Detail}", stringBuilder.ToString());
            return body;
        }

        public bool SubmitWHTSubmission(string[] invoice_number, string user)
        {
            var result = false;

            Db.Connection.Open();
            var transact = Db.Connection.BeginTransaction();
            try
            {
                foreach (string inv in invoice_number)
                {
                    Db.BuktiPotongRepository.SubmitWHTSubmission(transact, inv, user);
                }
                transact.Commit();
                result = true;
            }
            catch (Exception)
            {
                result = false;
                transact.Rollback();
                throw;
            }

            return result;
        }

        public bool DeleteUnSubmittedWHTSubmission(string[] invoice_number, List<BuktiPotong> buktiPotongs)
        {
            var result = false;

            var username = ApplicationCacheManager.GetConfig<string>("FileBuktiPotongUserName");
            var password = ApplicationCacheManager.GetConfig<string>("FileBuktiPotongPassword");
            var domain = ApplicationCacheManager.GetConfig<string>("FileBuktiPotongDomain");

            var userdomain = domain + "\\" + username;

            var pers = new Impersonation();
            pers.Impersonate(userdomain, password);

            Db.Connection.Open();

            var transact = Db.Connection.BeginTransaction();

            try
            {
                foreach (string inv in invoice_number)
                {
                    Db.BuktiPotongRepository.DeleteUnSubmittedWHTSubmission(transact, inv);
                }
                transact.Commit();
                result = true;

                foreach (var item in buktiPotongs)
                {
                    var credential = new NetworkCredential(userdomain, password);

                    //var datas = this.GetBuktiPotong(item.nomor_bukti_potong, SessionManager.RoleStr);

                    var datas = Db.BuktiPotongRepository.GetBuktiPotongByNomorBuktiPotong(item.nomor_bukti_potong, SessionManager.RoleStr);

                    if (datas.Count <= 1)
                    {
                        using (new NetworkConnection(item.wht_pdf_22, credential))
                        using (new NetworkConnection(item.wht_pdf_23, credential))
                        using (new NetworkConnection(item.wht_pdf_42, credential))
                        {
                            try
                            {
                                if (File.Exists(item.wht_pdf_22))
                                    File.Delete(item.wht_pdf_22);

                                if (File.Exists(item.wht_pdf_23))
                                    File.Delete(item.wht_pdf_23);

                                if (File.Exists(item.wht_pdf_42))
                                    File.Delete(item.wht_pdf_42);
                            }
                            catch (Exception e)
                            {
                                transact.Rollback();
                                result = false;
                                throw;
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                result = false;
                transact.Rollback();
                throw;
            }

            return result;
        }

        public bool SendEmailDealerHasBeenUploadNotification(string invoiceNumbers)
        {
            bool result = false;
            string username, password, domain, host, port, subject, from, url, to, toName;


            try
            {
                username = Db.ParameterRepository.Find(new { key_param = "email_username_chat_notification" }).FirstOrDefault().value1;
                password = Db.ParameterRepository.Find(new { key_param = "email_password_chat_notification" }).FirstOrDefault().value1;
                domain = Db.ParameterRepository.Find(new { key_param = "email_domain_chat_notification" }).FirstOrDefault().value1;
                host = Db.ParameterRepository.Find(new { key_param = "email_host_chat_notification" }).FirstOrDefault().value1;
                port = Db.ParameterRepository.Find(new { key_param = "email_port_chat_notification" }).FirstOrDefault().value1;
                subject = Db.EmailTemplateRepository.Find(new { module = "Notification Dealer Upload WHT", mail_key = "email_key" }).FirstOrDefault().subject;

                from = Db.ParameterRepository.Find(new { key_param = "email_username" }).FirstOrDefault().value1;
                url = Db.ParameterRepository.Find(new { key_param = "bukti_potong_url" }).FirstOrDefault().value1;

                to = Db.ParameterRepository.Find(new { key_param = "email_internal_upload_wht" }).FirstOrDefault().value1;
                toName = Db.ParameterRepository.Find(new { key_param = "email_internal_upload_wht" }).FirstOrDefault().value2;

                List<BuktiPotong> buktiPotong = this.GetBuktiPotong(invoiceNumbers, SessionManager.RoleStr);

                List<BuktiPotong> groupedCustomerBuktiPotong = buktiPotong
                        .GroupBy(b => b.customer_group)
                        .Select(s => s.First())
                        .ToList();

                foreach (BuktiPotong bp in groupedCustomerBuktiPotong)
                {
                    string body = BuildBodyDealerHasBeenUpload(url, buktiPotong, bp, toName);

                    //EmailService emailSvc = new EmailService();
                    SmtpClient emailSvc = new SmtpClient();
                    emailSvc.Host = host;
                    emailSvc.Port = int.Parse(port);
                    //emailSvc.User = username;
                    //emailSvc.Password = password;
                    emailSvc.UseDefaultCredentials = false;
                    emailSvc.Credentials = new NetworkCredential(username, password);
                    emailSvc.EnableSsl = false;
                    if (from.Contains("gmail")) { emailSvc.EnableSsl = true; }

                    //EmailMessage emailMessage = new EmailMessage();
                    MailMessage emailMessage = new MailMessage(from, from);
                    emailMessage.From = new MailAddress(from);
                    emailMessage.To.RemoveAt(0);
                    emailMessage.Subject = subject;
                    emailMessage.Body = body;
                    emailMessage.IsBodyHtml = true;

                    emailMessage.To.Add(new MailAddress(to));
                    emailSvc.Send(emailMessage);

                }
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
            }

            return result;
        }

        public DataSourceResult GetPDFFileExists(DataSourceRequest request, string invoiceType, string customer_group, string id)
        {
            var result = Db.BuktiPotongRepository.GetChekBuktiPotongList(request, invoiceType, customer_group, id);
            return result;
        }

        public DataSourceResult GetChekBuktiPotongList(DataSourceRequest request, string invoiceType, string customer_group, string id)
        {
            var result = Db.BuktiPotongRepository.GetChekBuktiPotongList(request, invoiceType, customer_group, id);
            return result;
        }

        public DataSourceResult getchkbuktipotong(DataSourceRequest request, string invoiceType, string customer_group, String[] id)
        {
            var result = Db.BuktiPotongRepository.getchkbuktipotong(request, invoiceType, customer_group, id);
            return result;
        }


        private string BuildBodyDealerHasBeenUpload(string url, List<BuktiPotong> dueDateBuktiPotong
            , BuktiPotong distinctBuktiPotonog, string toName)
        {
            StringBuilder stringBuilder = new StringBuilder();
            string body = Db.EmailTemplateRepository.Find(new { module = "Notification Dealer Upload WHT", mail_key = "email_key" }).FirstOrDefault().mail_content;
            body = body.Replace("{User}", toName);
            body = body.Replace("{url}", url);
            body = body.Replace("{NamaDealer}", distinctBuktiPotonog.customer_name);
            foreach (BuktiPotong bpDetail in dueDateBuktiPotong.Where(b => b.customer_group == distinctBuktiPotonog.customer_group))
            {
                stringBuilder.AppendFormat("<tr><td>{0}</td></tr>", bpDetail.invoice_number);
            }

            body = body.Replace("{Detail}", stringBuilder.ToString());
            return body;
        }
    }
}
