﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Tam.Ebilling.Domain.Modules.Core.Model;
using Tam.Ebilling.Domain;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using Tam.Ebilling.Infrastructure.Session;
using System;
using Tam.Ebilling.Infrastructure.Cache;
using System.IO;
using Tam.Ebilling.Infrastructure.Helper;
using System.Net;
using Tam.Ebilling.Infrastructure.Adapter;
namespace Tam.Ebilling.Service
{
    public class ScanInvoiceService : DbServiceBase
    {
        public ScanInvoiceService(IDbHelper db) : base(db)
        {
        }
        public DataSourceResult GetDataSourceResult(DataSourceRequest request)
        {
            var result = Db.ScanInvoiceRepository.GetDataSourceResult(request);

            return result;
        }
        public List<udf_GetInvoiceNumber> GetInvoiceNumber(string invoice_number)
        {
            var d = Db.Connection.Query<udf_GetInvoiceNumber>("SELECT * FROM [dbo].[udf_GetInvoiceNumber](@invoice_number)", new { invoice_number = invoice_number }).ToList();
            return d;
        }
        public List<udf_GetInvoiceNumberNotur> GetInvoiceNumberNotur(string invoice_number)
        {
            var d = Db.Connection.Query<udf_GetInvoiceNumberNotur>("SELECT * FROM [dbo].[udf_GetInvoiceNumberNotur](@invoice_number)", new { invoice_number = invoice_number }).ToList();
            return d;
        }
        public IEnumerable<ScanInvoice> GetDocumentFileByReference(string invoice)
        {
            var result = Db.ScanInvoiceRepository.Find(new { invoice_number = invoice });
            result = result.Where(x => x.row_status != 1);

            return result;
        }
        public List<ScanInvoice> GetAttachment(string invoice_number)
        {
            return Db.ScanInvoiceRepository.Find(new { invoice_number = invoice_number }).ToList();
        }

        public IEnumerable<ScanInvoice> GetDocumentByID(string id)
        {
            var result = Db.ScanInvoiceRepository.Find(new { attachment_invoice_id = id });

            return result;
        }
        public List<ScanInvoice> UploadScan(string filename, string invoice_number)
        {
            var result = Db.ScanInvoiceRepository.UploadScan(filename, invoice_number);
            return result;
        }
        public List<ScanInvoiceNotur> UploadScanInvoiceNotur(string filename, string invoice_number)
        {
            var result = Db.ScanInvoiceNoturRepository.UploadScan(filename, invoice_number);
            return result;
        }
        public List<ScanInvoice> Remove(long attachment_invoice_id)
        {
            var result = Db.ScanInvoiceRepository.Remove(attachment_invoice_id);
            return result;
        }
        public ScanInvoice Upload(IPostedFile file, string invoice, out string msgerror)
        {
            msgerror = "";
            int maxFileSize = int.Parse(ApplicationCacheManager.GetConfig<string>("MaxUploadSize")) * 1024 * 1024;
            string fileTypeAllowed = ApplicationCacheManager.GetConfig<string>("FileTypeUpload");
            List<string> fileType = fileTypeAllowed.Split(',').ToList();
            bool isFileTypeAllowed = fileType.Exists(x => x.Contains(file.ContentType));

            var doc = new ScanInvoice();
            if (isFileTypeAllowed)
            {
                if (file.ContentLength < maxFileSize)
                {
                    var username = ApplicationCacheManager.GetConfig<string>("FileUploadUserName");
                    var password = ApplicationCacheManager.GetConfig<string>("FileUploadPassword");
                    var config = ApplicationCacheManager.GetConfig<string>("FileUploadScanTemp");
                    var domain = ApplicationCacheManager.GetConfig<string>("FileUploadDomain");
                    var userdomain = domain + "\\" + username;
                    var fileName = Path.GetFileName(file.FileName);
                    var newFileName = config + "\\" + fileName;
                    var url = Path.Combine(newFileName);
                    var dir = Path.GetDirectoryName(invoice);

                    var pers = new Impersonation();
                    pers.Impersonate(userdomain, password);
                    var credential = new NetworkCredential(userdomain, password);

                    using (new NetworkConnection(config, credential))
                    {
                        try
                        {
                            if (!Directory.Exists(dir))
                                Directory.CreateDirectory(dir);
                        }
                        catch (Exception e)
                        {

                        }
                        file.SaveAs(newFileName);
                    }

                    doc = new ScanInvoice
                    {
                        attachment_invoice_id = -1,
                        invoice_number = invoice,
                        filename = fileName,
                        //filepath = newFileName,
                        created_by = SessionManager.Current,
                        created_date = DateTime.Now
                    };
                }
                else
                {
                    msgerror = "File size cannot exceed 10 MB";
                }
            }
            else
            {
                msgerror = "File type allowed are " + fileTypeAllowed;
            }

            return doc;
        }
        public ScanInvoiceNotur UploadScanNotur(IPostedFile file, string invoice, out string msgerror)
        {
            msgerror = "";
            int maxFileSize = int.Parse(ApplicationCacheManager.GetConfig<string>("MaxUploadSize")) * 1024 * 1024;
            string fileTypeAllowed = ApplicationCacheManager.GetConfig<string>("FileTypeUpload");
            List<string> fileType = fileTypeAllowed.Split(',').ToList();
            bool isFileTypeAllowed = fileType.Exists(x => x.Contains(file.ContentType));

            var doc = new ScanInvoiceNotur();
            if (isFileTypeAllowed)
            {
                if (file.ContentLength < maxFileSize)
                {
                    var username = ApplicationCacheManager.GetConfig<string>("FileUploadUserName");
                    var password = ApplicationCacheManager.GetConfig<string>("FileUploadPassword");
                    var config = ApplicationCacheManager.GetConfig<string>("FileUploadScanNoturTemp");
                    var domain = ApplicationCacheManager.GetConfig<string>("FileUploadDomain");
                    var userdomain = domain + "\\" + username;
                    var fileName = Path.GetFileName(file.FileName);
                    var newFileName = config + "\\" + fileName;
                    var url = Path.Combine(newFileName);
                    var dir = Path.GetDirectoryName(invoice);

                    var pers = new Impersonation();
                    pers.Impersonate(userdomain, password);
                    var credential = new NetworkCredential(userdomain, password);

                    using (new NetworkConnection(config, credential))
                    {
                        try
                        {
                            if (!Directory.Exists(dir))
                                Directory.CreateDirectory(dir);
                        }
                        catch (Exception e)
                        {

                        }
                        file.SaveAs(newFileName);
                    }

                    doc = new ScanInvoiceNotur
                    {
                        attachment_invoice_notur_id = -1,
                        invoice_number = invoice,
                        filename = fileName,
                        //filepath = newFileName,
                        created_by = SessionManager.Current,
                        created_date = DateTime.Now
                    };
                }
                else
                {
                    msgerror = "File size cannot exceed 10 MB";
                }
            }
            else
            {
                msgerror = "File type allowed are " + fileTypeAllowed;
            }

            return doc;
        }
        public bool SaveUpload(string invoice, IEnumerable<ScanInvoice> docs, out string msgerror)
        {
            msgerror = "";
            var result = true;

            //long? id = null;
            //var reference = "";

            var username = ApplicationCacheManager.GetConfig<string>("FileUploadUserName");
            var password = ApplicationCacheManager.GetConfig<string>("FileUploadPassword");
            var config = ApplicationCacheManager.GetConfig<string>("FileUploadScanTemp");
            var domain = ApplicationCacheManager.GetConfig<string>("FileUploadDomain");
            var userdomain = domain + "\\" + username;
            var pers = new Impersonation();
            pers.Impersonate(userdomain, password);

            if (docs != null)
            {
                if (docs.Count() > 0)
                {
                    //save document file
                    foreach (ScanInvoice doc in docs)
                    {
                        if (doc.Status == "New")
                        {

                            // Update Property
                            doc.invoice_number = invoice;
                            doc.created_date = DateTime.Now;
                            doc.created_by = SessionManager.Username;
                            doc.modified_by = SessionManager.Username;
                            doc.modified_date = DateTime.Now;
                            // Rest status
                            doc.Status = null;
                            Db.ScanInvoiceRepository.UploadScan(doc.filename, doc.invoice_number);

                            var tempFile = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileUploadScanTemp"), doc.filename);
                            var newFile = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileUploadScan"), doc.filename);
                            //var Update = Service<InvoiceService>().GetUploadNotur(daNumber);
                            //var reference = reff;
                            var credential = new NetworkCredential(userdomain, password);
                            using (new NetworkConnection(tempFile, credential))
                            using (new NetworkConnection(newFile, credential))
                            {
                                try
                                {
                                    File.Move(tempFile, newFile);
                                }
                                catch (Exception e)
                                {

                                }
                                //Db.InvoiceRepository.GetUploadNotur(doc.reference);
                                //Db.NotaReturRepository.GetUploadNoturMV(doc.reference);
                            }
                            //}

                        }
                        else if (doc.Status == "Delete")
                        {
                            if (doc.invoice_number == default(string))
                            {
                                // Delete physical file
                                var tempFile = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileUploadScanTemp"), doc.filename);
                                var credential = new NetworkCredential(userdomain, password);
                                using (new NetworkConnection(tempFile, credential))
                                {
                                    try
                                    {
                                        File.Delete(tempFile);
                                    }
                                    catch (Exception e)
                                    {

                                    }
                                }
                            }
                            else
                            {
                                //Delete data
                                Db.ScanInvoiceRepository.Remove(doc.attachment_invoice_id);
                                //if (doc.reference.HasValue)
                                //{
                                //    var dattodel = dbhelper.AttachmentNotaReturRepository.Find(new { reference = reference.Value, filepath = doc.filepath }).FirstOrDefault();
                                //    if (dattodel != null)
                                //    {
                                //        dbhelper.AttachmentNotaReturRepository.Remove(dattodel);
                                //    }
                                //}

                                // Delete physical file
                                var file = Path.Combine(ApplicationCacheManager.GetConfig<string>("FileUploadScan"), doc.filename);
                                var credential = new NetworkCredential(userdomain, password);
                                using (new NetworkConnection(file, credential))
                                {
                                    try
                                    {
                                        File.Delete(file);
                                    }
                                    catch (Exception e)
                                    {

                                    }
                                }
                            }
                        }
                    }
                }
            }

            return result;
        }
      

    }
}
