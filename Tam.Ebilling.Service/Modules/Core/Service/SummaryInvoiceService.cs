﻿using Kendo.Mvc.UI;
using Tam.Ebilling.Domain;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Tam.Ebilling.Infrastructure.Session;
using System.Web;

namespace Tam.Ebilling.Service
{
    public class SummaryInvoiceService : DbServiceBase
    {
        public SummaryInvoiceService(IDbHelper db) : base(db)
        {
        }
        // private SampleEntities entities;

        //public ProductService(SampleEntities entities)
        //{
        //    this.entities = entities;
        //}
        public DataSourceResult GetDataSourceResult(DataSourceRequest request, string customerGroup, string userType)
        {
            var result = Db.SummaryInvoiceRepository.GetViewDataSourceResult(request, customerGroup, userType);
            return result;
        }

        public void RecordHistory(string dateFrom, string dateTo, string businessType, string customerId, string user)
        {
            Db.SummaryInvoiceRepository.RecordHistory(dateFrom, dateTo, businessType, customerId, user);
        }
    }
}
