﻿using System.Collections.Generic;
using Tam.Ebilling.Domain;

namespace Tam.Ebilling.Service
{
    public class MenuGroupService : DbServiceBase
    {
        public MenuGroupService(IDbHelper db) : base(db)
        {
        }
        public IEnumerable<MenuGroup> GetMenuGroups()
        {
            return Db.MenuGroupRepository.FindAll();
        }

    }
}
