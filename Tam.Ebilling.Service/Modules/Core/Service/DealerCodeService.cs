﻿using Kendo.Mvc.UI;
using Tam.Ebilling.Domain;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Tam.Ebilling.Infrastructure.Session;
using System.Web;


namespace Tam.Ebilling.Service
{
    public class DealerCodeService : DbServiceBase
    {
        public DealerCodeService(IDbHelper db) : base(db)
        {

        }
        public List<DealerCode> GetDealerCode()
        {
            var strsql = @"SELECT * From vw_DealerCode2";
            var d = Db.Connection.Query<DealerCode>(strsql).ToList();
            return d;
        }

        public List<DealerCode> GetDealerCode2(string customer_group)
        {
            var result = Db.DealerCodeRepository.GetDealerCode(customer_group);
            return result;
        }
    }
}
