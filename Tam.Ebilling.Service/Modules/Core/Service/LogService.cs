﻿using Kendo.Mvc.UI;
using Tam.Ebilling.Domain;

namespace Tam.Ebilling.Service
{
    public class LogService : DbServiceBase
    {
        public LogService(IDbHelper db) : base(db)
        {
        }
        public DataSourceResult GetDataSourceResult(DataSourceRequest request)
        {
            var result = Db.LogRepository.GetDataSourceResult(request);

            return result;
        }
    }
}
