﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tam.Ebilling.Domain;

namespace Tam.Ebilling.Service
{
    public class BankAccountService : DbServiceBase
    {
        public BankAccountService(IDbHelper db) : base(db)
        {

        }

        public List<BankAccount> GetBankAccount(string roleStr, string userType, string isForm)
        {
            var strsql="";

            if (isForm != "1")
            {
                strsql = @"select distinct (vb.bank_type +':'+ vb.bank_key)[bank_key], (vb.bank_account +': '+ upper(vb.name_of_bank)) [bank_name]
                            from sap_db.dbo.tb_m_vendor_bank vb
                            left join tb_t_mapp_vendor mapven on vb.vendor_cd = mapven.vendor_cd                    
                            where 1=1";
            }
            else
            {
                strsql = @"select distinct a.*, upper(isnull(b.NAME_OF_BANK, a.BANK_NAME)) [BANK_NAME]
                        from sap_db.dbo.tb_m_bank a
                        left join 
                        (
	                        select distinct bank_key, name_of_bank from sap_db.dbo.tb_m_vendor_bank 
                        ) b
                        on a.BANK_KEY = b.BANK_KEY
                        where 1=1
	                        and bank_country = 'ID'";
            }

            if (!string.IsNullOrEmpty(roleStr) && userType != "Internal" && isForm == "")
            {
                string query = " and mapven.customer_group_id = '{0}'";
                       query = query.Replace("{0}", roleStr);
                strsql = strsql + query;
            }

            var d = Db.Connection.Query<BankAccount>(strsql).ToList();
            return d;
        }

        /*
        public List<BankAccount> GetDataBankRequested()
        {
            var strsql = @"select a.bank_account, a.name_of_bank, 'On Request' [status_desc]
                            from tb_m_vendor_bank a
                            left join SAP_DB.dbo.tb_m_vendor_bank b
	                            on a.bank_account = b.bank_account
		                            and a.vendor_cd = b.vendor_cd
                            where 1=1
	                            and status_req = '1'
	                            and b.bank_account is null";

            var d = Db.Connection.Query<BankAccount>(strsql).ToList();

            return d;
        }
        */

        public List<BankAccount> GetDataBankRequested()
        {
            List<BankAccount> data = new List<BankAccount>();
                           data = Db.Connection.Query<BankAccount>("usp_GetEmailBankRequested",null, null, true, null, System.Data.CommandType.StoredProcedure).ToList();

            return data;
        }

        public string GetBankAccount(string processId, string userType)
        {
            StringBuilder fetchquery = null;

            fetchquery = new StringBuilder(@"select case when x.bank_type is not null
				                                            and x.bank_key is not null
			                                            then x.bank_type + ':' + x.bank_key
			                                            else ''
	                                                end [bank_key]
                                            from 
                                            (
	                                            select '' [x]
                                            ) z
                                            outer apply
                                            (
	                                            select top 1 cast(a.bank_type as varchar)[bank_type], c.bank_key [bank_key]
	                                            from tb_r_pv_h a
	                                            join tb_t_upload_h b on a.pv_no = b.pv_no
	                                            join sap_db.dbo.tb_m_vendor_bank c on c.bank_key= b.bank_key 
		                                            and c.bank_account = b.bank_account
	                                            where b.process_id='{0}'
		                                            and case when '{1}' = 'Internal' then 
					                                            case when len(a.status_cd) = 1 then
						                                                case when a.status_cd >= 4 then 1
							                                                else 0
						                                                end
				                                                    else 1
					                                            end
				                                                else 1
			                                            end = 1
                                            ) x");

            string query = string.Format(fetchquery.ToString(), processId, userType);
            var result = Db.Connection.QuerySingleOrDefault<string>(query);

            return result;
        }

        public bool GetBankAccount(string vendor)
        {
            bool output = false;
            StringBuilder fetchquery = null;

            fetchquery = new StringBuilder(@"select top 1 BANK_ACCOUNT 
                                            from tb_m_vendor_bank
                                            where vendor_cd = '{0}'
	                                            and STATUS_REQ = '1'");

            string query = string.Format(fetchquery.ToString(), vendor);
            string result = Db.Connection.QuerySingleOrDefault<string>(query);

            if (!string.IsNullOrEmpty(result))
            {
                output = true;
            }

            return output;
        }

        public List<PVMonitoringBankAccount> GetBankProcessByInvoice(string processId)
        {
            StringBuilder fetchquery = null;

            fetchquery = new StringBuilder(@"select distinct 
	                                            d.bank_account,
	                                            upper(d.name_of_bank)[name_of_bank],
	                                            'Available' [status_req],
	                                            x.beneficiaries [beneficiaries],
                                                x.directory [directory],
                                                x.file_name [file_name],	
                                                x.guid_filename [guid_filename]
                                            from tb_t_upload_h a
                                            join tb_t_upload_d c
                                                on c.process_id = a.process_id
                                            join sap_db.dbo.tb_m_vendor_bank d on d.vendor_cd = a.vendor_cd
                                            left join tb_m_vendor_bank x
                                                on d.vendor_cd = x.vendor_cd
		                                            and x.bank_account = d.bank_account
                                            where 1=1
	                                            and c.process_id = '{0}'
                                            union all
                                            select distinct 
	                                            b.bank_account,
	                                            upper(b.name_of_bank)[name_of_bank],
	                                            cast(e.[description] as varchar)[status_req],
	                                            b.beneficiaries,
                                                b.directory,
                                                b.file_name,	
                                                b.guid_filename
                                            from tb_t_upload_h a
                                            left join tb_m_vendor_bank b
                                                on a.vendor_cd = b.vendor_cd
                                            left join tb_m_parameter e
	                                            on b.status_req = e.value1
		                                            and e.key_param = 'PV_BANK_STATUS'
                                            join tb_t_upload_d c
                                                on c.process_id = a.process_id
                                            join sap_db.dbo.tb_m_vendor_bank d on d.vendor_cd = a.vendor_cd
	                                            and d.bank_account = b.bank_account
	                                            and d.bank_key = b.bank_key
                                            where 1=1
                                                and b.bank_account is not null
	                                            and d.bank_account is null
	                                            and c.process_id = '{1}'
                                            ");

            string query = string.Format(fetchquery.ToString(), processId, processId);
            var output = Db.Connection.Query<PVMonitoringBankAccount>(query).ToList();

            return output;
        }
    }
}
