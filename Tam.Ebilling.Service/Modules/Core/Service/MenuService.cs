﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using Tam.Ebilling.Domain;
using System.Linq;
using Tam.Ebilling.Infrastructure.Session;

namespace Tam.Ebilling.Service
{
    public class MenuService : DbServiceBase
    {
        public MenuService(IDbHelper db) : base(db)
        {
        }
        public IEnumerable<Tam.Ebilling.Domain.Menu> GetByGroupID(int GroupID)
        {
            return Db.MenuRepository.Find(new { MenuGroupID = GroupID }).OrderBy(x => x.parent_id).ThenBy(x => x.order_index);
        }

        public bool Save(Tam.Ebilling.Domain.Menu menu)
        {
            var result = false;

            if (menu.menu_id == 0)
            {
                menu.created_date = DateTime.Now;
                menu.created_by = SessionManager.Current;

                Db.MenuRepository.Add(menu, new string[] {
                    "ParentID",
                    "PermissionID",
                    "MenuGroupID",
                    "Title",
                    "Url",
                    "Description",
                    "IconClass",
                    "OrderIndex",
                    "Visible",
                    "CreatedOn",
                    "CreatedBy"
                });
            }
            else
            {
                Db.MenuRepository.Update(menu, new string[]
                {
                    "ParentID",
                    "PermissionID",
                    "MenuGroupID",
                    "Title",
                    "Url",
                    "Description",
                    "IconClass",
                    "Visible",
                    "CreatedOn",
                    "CreatedBy"
                });
            }

            return result;
        }
        public Tam.Ebilling.Domain.Menu GetByID(int id)
        {
            return Db.MenuRepository.Find(new { menu_id = id }).FirstOrDefault();
        }

        public bool ToggleVisible(int id)
        {
            var menu = Db.MenuRepository.Find(new { menu_id = id }).FirstOrDefault();
            menu.visible = menu.visible == 1 ? (short)0 : (short)1;

            return Db.MenuRepository.Update(menu, new string[] { "Visible" }) > 0;
        }

        public bool Rename(int id, string title)
        {
            var menu = Db.MenuRepository.Find(new { menu_id = id } ).FirstOrDefault();
            menu.title = title;

            return Db.MenuRepository.Update(menu, new string[] { "Title" }) > 0;
        }

        public void UpdateParent(int id, int parentID, int menuGroupID, int orderIndex)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@id", id);
            parameters.Add("@orderIndex", orderIndex);
            parameters.Add("@menuGroupID", menuGroupID);

            if (parentID == null) parameters.Add("@parentID", null);
            else parameters.Add("@parentID", parentID);

            Db.Connection.Execute("usp_Core_UpdateMenuOrder", parameters, commandType: CommandType.StoredProcedure);
        }

        public bool Delete(int id)
        {
            var result = false;
            Db.MenuRepository.Delete(id);

            return result;
        }
    }
}
