﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tam.Ebilling.Domain;

namespace Tam.Ebilling.Service
{
    public class WHTMAPCodeService : DbServiceBase
    {
        public WHTMAPCodeService(IDbHelper db) : base(db)
        {

        }
        public List<WHTMAPCode> GetWHTMAPCode(string customer_group)
        {
            var result = Db.WHTRepository.GetWHTMAPCode(customer_group);
            return result;
        }
    }
}
