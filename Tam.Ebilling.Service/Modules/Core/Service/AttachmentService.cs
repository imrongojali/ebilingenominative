﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Tam.Ebilling.Domain.Modules.Core.Model;
using Tam.Ebilling.Domain;
using System.Collections.Generic;
using System.Linq;
using Dapper;

namespace Tam.Ebilling.Service
{
    public class AttachmentService : DbServiceBase
    {
        public AttachmentService(IDbHelper db) : base(db)
        {
        }
        public DataSourceResult GetDataSourceResult(DataSourceRequest request)
        {
            var result = Db.AttachmentRepository.GetDataSourceResult(request);

            return result;
        }
        public List<Attachment> GetAttachment (string reference)
        {
            return Db.AttachmentRepository.Find(new { reference = reference }).ToList();
        }
        public string GetInvoiceRawDataByInvoice(string inv)
        {
            var d = Db.Connection.Query("Select b.da_code as da_code FROM [BES].[dbo].[tb_r_da] a " +
                                                    "INNER JOIN[dbo].[tb_r_da_detil] b ON a.da_code = b.da_code "+
                                                      "where b.invoice_number = '"+ inv + "'").Select(x=>x.da_code).FirstOrDefault();
            return d;
        }
        public List<AttachmentSpDebitAdvice> GetSpAttachment(string daNumber, string user, string deliveryDate)
        {
            return Db.AttachmentRepository.GetSpDebitAdvice(new DataSourceRequest(), daNumber, user, deliveryDate).ToList();
        }
        public List<AttachmentSpDebitAdvice> GetSpCreditAttachment(string caNumber, string user)
        {
            return Db.AttachmentRepository.GetSpCreditAdvice(new DataSourceRequest(), caNumber, user).ToList();
        }
        
        public List<SpDeliveryNote> GetSpDeliveryNote(string daNumber, string user)
        {
            return Db.AttachmentRepository.GetSpDeliveryNote(new DataSourceRequest(), daNumber, user).ToList();
        }

        public List<SpDeliveryNote> GetSpCreditDeliveryNote(string daNumber, string user)
        {
            return Db.AttachmentRepository.GetSpCreditDeliveryNote(new DataSourceRequest(), daNumber, user).ToList();
        }
    }
}
