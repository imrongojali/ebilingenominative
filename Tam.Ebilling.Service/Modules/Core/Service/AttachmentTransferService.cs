﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Tam.Ebilling.Domain.Modules.Core.Model;
using Tam.Ebilling.Domain;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using Tam.Ebilling.Infrastructure.Session;
using System;
using Tam.Ebilling.Infrastructure.Cache;
using System.IO;
using Tam.Ebilling.Infrastructure.Helper;
using System.Net;
using Tam.Ebilling.Infrastructure.Adapter;


namespace Tam.Ebilling.Service
{
    public class AttachmentTransferService : DbServiceBase
    {
        public AttachmentTransferService(IDbHelper db) : base(db)
        {
        }
        public List<udf_GetAttachmentTransfer> GetAttachmentTransfer(string invoiceNumber)
        {
            var d = Db.Connection.Query<udf_GetAttachmentTransfer>("SELECT * FROM [dbo].[udf_GetAttachmentTransfer](@invoiceNumber)", new { invoiceNumber = invoiceNumber }).ToList();
            return d;
        }
        public AttachmentTransfer Upload(IPostedFile file, string invoice, out string msgerror)
        {
            msgerror = "";
            int maxFileSize = int.Parse(ApplicationCacheManager.GetConfig<string>("MaxUploadSize")) * 1024 * 1024;
            string fileTypeAllowed = ApplicationCacheManager.GetConfig<string>("FileTypeUpload");
            List<string> fileType = fileTypeAllowed.Split(',').ToList();
            bool isFileTypeAllowed = fileType.Exists(x => x.Contains(file.ContentType));

            var doc = new AttachmentTransfer();
            if (isFileTypeAllowed)
            {
                if (file.ContentLength < maxFileSize)
                {
                    var username = ApplicationCacheManager.GetConfig<string>("FileUploadUserName");
                    var password = ApplicationCacheManager.GetConfig<string>("FileUploadPassword");
                    var config = ApplicationCacheManager.GetConfig<string>("FileUploadTransferTemp");
                    var domain = ApplicationCacheManager.GetConfig<string>("FileUploadDomain");
                    var userdomain = domain + "\\" + username;
                    var fileName = Path.GetFileName(file.FileName);
                    var newFileName = config + "\\" + fileName;
                    var url = Path.Combine(newFileName);
                    var dir = Path.GetDirectoryName(invoice);

                    var pers = new Impersonation();
                    pers.Impersonate(userdomain, password);
                    var credential = new NetworkCredential(userdomain, password);

                    using (new NetworkConnection(config, credential))
                    {
                        try
                        {
                            if (!Directory.Exists(dir))
                                Directory.CreateDirectory(dir);
                        }
                        catch (Exception e)
                        {

                        }
                        file.SaveAs(newFileName);
                    }

                    doc = new AttachmentTransfer
                    {
                        ID= -1,
                        invoiceNumber = invoice,
                        //attachment = fileName,
                        //filepath = newFileName,
                        created_by = SessionManager.Current,
                        created_date = DateTime.Now
                    };
                }
                else
                {
                    msgerror = "File size cannot exceed 10 MB";
                }
            }
            else
            {
                msgerror = "File type allowed are " + fileTypeAllowed;
            }

            return doc;
        }
    }
}
