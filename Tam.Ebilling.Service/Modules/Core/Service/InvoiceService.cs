﻿using Kendo.Mvc.UI;
using Tam.Ebilling.Domain;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Tam.Ebilling.Infrastructure.Session;
using System.Web;

namespace Tam.Ebilling.Service
{
    public class InvoiceService : DbServiceBase
    {
        public InvoiceService(IDbHelper db) : base(db)
        {
        }
        // private SampleEntities entities;

        //public ProductService(SampleEntities entities)
        //{
        //    this.entities = entities;
        //}
        public DataSourceResult GetDataSourceResult(DataSourceRequest request)
        {
            var result = Db.InvoiceRepository.GetDataSourceResult(request);
            return result;
        }

        public DataSourceResult GetViewDataSourceResult(DataSourceRequest request, int invoice_type,  string customer_group)
        {
            var result = Db.InvoiceRepository.GetViewDataSourceResult(request, invoice_type, customer_group, "", true);
            return result;
        }

        //tes AD
        public List<Invoice> GetViewDataSourceResultVAT(int invoice_type, string customer_group)
        {
            var result = Db.InvoiceRepository.GetViewDataSourceResultVAT(invoice_type, customer_group);
            return result;
        }
        //end

        #region Download ALL WHT PDF
        public DataSourceResult GetViewDataSourceResultWHTPDF(DataSourceRequest request, int invoice_type, string customer_group)
        {
            var result = Db.InvoiceRepository.GetGridInitDataSourceResult(request, invoice_type, customer_group);
            return result;
        }
        #endregion

        public DataSourceResult GetGridInitDataSourceResult(DataSourceRequest request, int invoice_type, string customer_group)
        {
            var result = Db.InvoiceRepository.GetGridInitDataSourceResult(request, invoice_type, customer_group);
            return result;
        }

        public DataSourceResult GetInvoiceData(DataSourceRequest request,string inv_number)
        {
             var result = Db.InvoiceRepository.GetDataSourceResult(request,inv_number);
            return result;

        }

        public Invoice GetInvoiceByDaNumber(string da_no)
        {
            var result = Db.InvoiceRepository.GetInvoiceByDaNumber(da_no);
            return result;
        }

        public Invoice GetInvoiceByCaNumberDaNumber(string invoiceType, string caNumber, string daNumber)
        {
            var result = Db.InvoiceRepository.GetInvoiceByCaNumberDaNumber(invoiceType, caNumber, daNumber);
            return result;
        }

        public List<Invoice> GetInvoiceByInvoiceNumber(string invoiceType, string invoiceNumber)
        {
            var result = Db.InvoiceRepository.GetInvoiceByInvoiceNumber(invoiceType, invoiceNumber);
            return result;
        }

        public Invoice GetSingleInvoiceByInvoiceNumber(string invoiceType, string invoiceNo)
        {
            var result = Db.InvoiceRepository.GetSingleInvoiceByInvoiceNumber(invoiceType, invoiceNo);
            return result;
        }

        public List<Invoice> GetSummaryInvoice(string dateFrom, string dateTo, string businessType, string customerGroup, string totalAmount)
        {
            string businessTypeSql = string.Empty;
            if (!string.IsNullOrEmpty(businessType))
            {
                string[] businessTypeArr = businessType.Split('#');
                for (int i=0; i < businessTypeArr.Length; i++)
                {
                    businessTypeSql += "'" + businessTypeArr[i] + "'";

                    if (i != (businessTypeArr.Length - 1))
                        businessTypeSql += ",";
                }
            }

            string customerGroupSql = string.Empty;
            if (!string.IsNullOrEmpty(customerGroup))
            {
                string[] customerGroupArr = customerGroup.Split('#');
                for (int i = 0; i < customerGroupArr.Length; i++)
                {
                    customerGroupSql += "'" + customerGroupArr[i] + "'";
                    if (i != (customerGroupArr.Length - 1))
                        customerGroupSql += ",";
                }
            }

            var result = Db.InvoiceRepository.GetSummaryInvoice(dateFrom, dateTo, businessTypeSql, customerGroupSql, totalAmount);
            return result;
        }

        public List<Invoice> GetReceipt(string dateFrom, string dateTo, string businessType, string customerGroup, string totalAmount)
        {
            string businessTypeSql = string.Empty;
            if (!string.IsNullOrEmpty(businessType))
            {
                string[] businessTypeArr = businessType.Split('#');
                for (int i = 0; i < businessTypeArr.Length; i++)
                {
                    businessTypeSql += "'" + businessTypeArr[i] + "'";

                    if (i != (businessTypeArr.Length - 1))
                        businessTypeSql += ",";
                }
            }

            string customerGroupSql = string.Empty;
            if (!string.IsNullOrEmpty(customerGroup))
            {
                string[] customerGroupArr = customerGroup.Split('#');
                for (int i = 0; i < customerGroupArr.Length; i++)
                {
                    customerGroupSql += "'" + customerGroupArr[i] + "'";
                    if (i != (customerGroupArr.Length - 1))
                        customerGroupSql += ",";
                }
            }

            var result = Db.InvoiceRepository.GetReceipt(dateFrom, dateTo, businessTypeSql, customerGroupSql, totalAmount);
            return result;
        }

        //public void UpdateDownload(string daNumber, string da_detil_code)
        //{
        //    var parameters = new DynamicParameters();
        //    parameters.Add("@daNumber", daNumber);
        //    parameters.Add("@da_detil_code", da_detil_code);

        //    //if (parentID == null) parameters.Add("@parentID", null);
        //    //else parameters.Add("@parentID", parentID);

        //    Db.Connection.Execute("usp_UpdateDownloaded", parameters, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);
        //}
        public List<Invoice> GetUpdate(string daNumber, string user)
        {
            var result = Db.InvoiceRepository.GetUpdate(daNumber, user);
            return result;
        }
        public List<Invoice> UpdateSparePartStatusDownload(string daNumber, string user, string downloadType, string invoiceType)
        {
            var result = Db.InvoiceRepository.UpdateSparePartStatusDownload(daNumber, user, downloadType, invoiceType);
            return result;
        }
        public List<Invoice> GetUpdateInvoice(string invoice_number, string user)
        {
            var result = Db.InvoiceRepository.GetUpdateInvoice(invoice_number, user);
            return result;
        }
        public List<Invoice> GetDataMV(string customer_group)
        {
            var result = Db.InvoiceRepository.GetDataMV(customer_group);
            return result;
        }
        public List<DebitAdvice> GetUpdateOther(string daNumber)
        {
            var result = Db.InvoiceRepository.GetUpdateOther(daNumber);
            return result;
        }
        public List<Invoice> GetUploadNotur(string daNumber)
        {
            var result = Db.InvoiceRepository.GetUploadNotur(daNumber);
            return result;
        }
        public List<AttachmentNotaRetur> UpdateRowStatus(string daNumber, string filename)
        {
            var result = Db.InvoiceRepository.GetRowStatus(daNumber, filename);
            return result;
        }

        public List<Invoice> GetDataPDF(string inv_number)
        {
            string inv_no = inv_number.Replace(" ", "");
                   inv_no = inv_no.Replace("'", "");
            //var strsql= @"select distinct tax_attc_url, customer_name
            //         from BES.dbo.udf_Invoice(1, '{0}')
            //         WHERE tax_attc_url is not null and NoFakturPajakYangDiRetur='{1}'";

            var strsql = @"select distinct PdfUrl tax_attc_url, ef.NamaCustomer customer_name
	                    from [TAM_EFAKTUR].[dbo].[TB_R_VATOut] ef 
						WHERE   ef.NomorFakturGabungan='{1}' and ApprovalStatus = 'Approval Sukses' ";
            strsql = string.Format(strsql, SessionManager.RoleStr, inv_no);
            var d = Db.Connection.Query<Invoice>(strsql).ToList();
            
            return d;
        }

        #region Get WHT PDF Row
        public List<Invoice> GetDataWHTPDF(string inv_number)
        {
            string inv_no = inv_number.Replace(" ", "");
            inv_no = inv_no.Replace("'", "");
            var strsql = @"select distinct PDFWithholding,customer_name 
                            from BES.dbo.udf_Invoice_wht('{0}')
                            where PDFWithholding is not null and (da_no like '{1}%' or invoice_number like '{1}%') and Dari = 'eFAk'";
            strsql = string.Format(strsql, SessionManager.RoleStr, inv_no);
            var d = Db.Connection.Query<Invoice>(strsql).ToList();

            return d;
        }
        #endregion

        public List<TransactionType> GetDataPDFx(string inv_numbe)
        {
            var strsql = @"SELECT * FROM vw_Transaction_Type";
            var d = Db.Connection.Query<TransactionType>(strsql).ToList();
            return d;
        }

        public List<Invoice> GetInvoices(string customer_group,
            string invoice_number = null,
            int? business_unit_id = null,
            int? transaction_type_id = null,
            string date_from = null,
            string date_to = null,
            string ar_status = null,
            int? document_status = null)
        {
            var output = Db.InvoiceRepository.GetInvoice(customer_group,
                    invoice_number,
                    business_unit_id,
                    transaction_type_id,
                    date_from,
                    date_to,
                    ar_status,
                    document_status);
            return output;
        }
        public List<Invoice> GetDACode(string invoice_number, int business_unit_id, int transaction_type_id)
        {
            return Db.InvoiceRepository.Find(new { invoice_number = invoice_number, business_unit_id = business_unit_id, transaction_type_id = transaction_type_id }).ToList();
        }
       
        //public IList<Invoice> GetAll()
        //{
        //    var result = HttpContext.Current.Session["Invoice"] as IList<Invoice>;
        //    //ExcelPackage pck = new ExcelPackage();
        //    //if (result == null || UpdateDatabase)
        //    //{
        //    result = entities.Invoice.Select(product => new Invoice
        //        {
        //            ProductID = product.ProductID,
        //            ProductName = product.ProductName,
        //            UnitPrice = product.UnitPrice.HasValue ? product.UnitPrice.Value : default(decimal),
        //            UnitsInStock = product.UnitsInStock.HasValue ? product.UnitsInStock.Value : default(short),
        //            QuantityPerUnit = product.QuantityPerUnit,
        //            Discontinued = product.Discontinued,
        //            UnitsOnOrder = product.UnitsOnOrder.HasValue ? (int)product.UnitsOnOrder.Value : default(int),
        //            CategoryID = product.CategoryID,
        //            Category = new CategoryViewModel()
        //            {
        //                CategoryID = product.Category.CategoryID,
        //                CategoryName = product.Category.CategoryName
        //            },
        //            LastSupply = DateTime.Today
        //        }).ToList();

        //        HttpContext.Current.Session["Products"] = result;
        //    //}

        //    return result;
        //}
    }
}
