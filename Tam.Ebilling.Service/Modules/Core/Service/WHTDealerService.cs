﻿using Kendo.Mvc.UI;
using Tam.Ebilling.Domain;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Tam.Ebilling.Infrastructure.Session;
using System.Web;

namespace Tam.Ebilling.Service
{
    public class WHTDealerService : DbServiceBase
    {
        public WHTDealerService(IDbHelper db) : base(db)
        {

        }

        public List<WHTDealer> GetWHTDealerName(string customer_group)
        {
            var result = Db.WHTDealerRepository.GetWHTDealerName(customer_group);
            return result;
        }

        public List<WHTDealer> GetWHTOriDealerName(string customer_group)
        {
            var result = Db.WHTDealerRepository.GetWHTOriDealerName(customer_group);
            return result;
        }

        public List<WHTDealer> GetWHTDealerNPWP(string customer_group)
        {
            var result = Db.WHTDealerRepository.GetWHTDealerNPWP(customer_group);
            return result;
        }
    }
}
