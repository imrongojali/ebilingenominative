﻿using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using Tam.Ebilling.Domain;

namespace Tam.Ebilling.Service
{
    public class NotaReturService : DbServiceBase
    {
        public NotaReturService(IDbHelper db) : base(db)
        {
        }

        //public DataSourceResult GetDataSourceResult(DataSourceRequest request)
        //{
        //    var result = Db.NotaReturRepository.GetDataSourceResult(request);

        //    return result;
        //}

        public List<Invoice> GetUpdate(string da_number)
        {
            var result = Db.NotaReturRepository.GetUpdateDownloadNotur(da_number);
            return result;
        }

        public DataSourceResult GetReportNotaReturDataSourceResult(DataSourceRequest request, string customer_group)
        {
            var result = Db.NotaReturRepository.GetReportNotaReturDataSourceResult(request, customer_group);
            return result;
        }

        public AttachmentNotaRetur GetNoturAttachByRef(string da_no, string status)
        {
            var result = Db.NotaReturRepository.GetNoturAttachByRef(da_no, status);
            return result;
        }

        /* public List<PrintNotur> GetNotaRetur(DataSourceRequest request, string da_number, string user)
         {
             var result = Db.NotaReturRepository.GetPrintNotaRetur(request, da_number, user);
             ///var result = Db.NotaReturRepository.GetDataSourceResult(request);
             return result;
         }*/

    }
}
