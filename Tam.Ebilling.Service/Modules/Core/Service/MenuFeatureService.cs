﻿using Agit.Domain;
using Agit.Mvc;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tam.Ebilling.Domain;
using Tam.Ebilling.Infrastructure.Session;
using Dapper;
using Tam.Ebilling.Infrastructure.Cache;
using Agit.Helper;

namespace Tam.Ebilling.Service
{
    public class MenuFeatureService : DbServiceBase
    {
        public MenuFeatureService(IDbHelper db) : base(db)
        {
        }

        public User GetUserByUsername(string userId)
        {
            return Db.UserRepository.Find(new { user_id = userId }).FirstOrDefault();
        }

        public List<MenuFeature> GetAllowedLevelByMenu(int menuId)
        {
            List<MenuFeature> items = Db.MenuFeatureRepository.GetMenuFeature(menuId);

            foreach(MenuFeature mf in items)
            {
                mf.level_codes = Db.MenuLevelFeatureRepository.GetMenuLevelFeatureByFeatureId(mf.internal_id);
            }
            
            return items;
        }

        public bool IsFeatureAllowed(int menuId, string featureCode, string levelCode)
        {
            try
            {
                MenuFeature mf = Db.MenuFeatureRepository.GetMenuFeatureByMenuAndFeature(menuId, featureCode);
                List<MenuLevelFeature> menuLevelFeature = Db.MenuLevelFeatureRepository.GetMenuLevelFeatureByFeatureId(mf.internal_id);

                if (menuLevelFeature.Count > 0)
                {
                    return menuLevelFeature.Exists(i => i.level_code == levelCode);
                }

                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

    }
}
