﻿using Agit.Domain;
using Agit.Mvc;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tam.Ebilling.Domain;
using Tam.Ebilling.Infrastructure.Session;
using Dapper;
using Tam.Ebilling.Infrastructure.Cache;
using Agit.Helper;
using System.Text.RegularExpressions;

namespace Tam.Ebilling.Service
{
    public class UserService : DbServiceBase
    {
        public UserService(IDbHelper db) : base(db)
        {
        }

        public User GetUserInternalByUsername(string userId)
        {
            return Db.UserRepository.FindUserInternal(userId).FirstOrDefault();
        }

        public User GetUserByUsername(string userId)
        {
            return Db.UserRepository.Find(new { user_id = userId }).FirstOrDefault();
        }
        //public User GetUserByID(Guid id)
        //{
        //    return Db.UserRepository.Find(new { ID = id }).FirstOrDefault();
        //}
        //public IEnumerable<string> GetRolesByUsername(string username)
        //{
        //    var userRole = Db.UserRoleRepository.FindByUsername(username);
        //    var userRoleString = userRole.Select(x => x.RoleName).ToList();

        //    return userRoleString.Count() > 0 ? userRoleString : new[] { Infrastructure.AppConstants.CoreRoles.DefaultUser }.ToList();
        //}

        public bool isSQLInjectString(string strWords)
        {
            strWords = Regex.Replace(strWords, @"[^\u0000-\u007F]+", string.Empty);

            bool isSQLInjection = false;
            List<string> badChars = new List<string>() { "select", "drop", ";", "--", "insert", "delete", "xp_", "¿", "waitfor delay" };

            for (int i = 0; i < badChars.Count(); i++)
            {
                if (strWords.Contains(badChars[i]))
                    isSQLInjection = true;
                break;
            }

            return isSQLInjection;
        }

        public bool Authenticate(LoginViewModel loginViewModel)
        {
            var username = loginViewModel.Username;
            var password = loginViewModel.Password;
            var defaultPassword = ApplicationCacheManager.GetConfig<string>("core-default-password");

            if (!isSQLInjectString(loginViewModel.Username))
            {
                var user = GetUserByUsername(username);

                if (user == null) throw new ModelException("Username", "User not found");
                if (password != defaultPassword) throw new ModelException("Password", "Password not match");
            }

            return true;
        }
    }
}
