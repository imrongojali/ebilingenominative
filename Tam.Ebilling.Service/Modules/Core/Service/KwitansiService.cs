﻿using Kendo.Mvc.UI;
using Tam.Ebilling.Domain;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Tam.Ebilling.Infrastructure.Session;
using System.Web;

namespace Tam.Ebilling.Service.Modules.Core.Service
{
    public class KwitansiService : DbServiceBase
    {
        public KwitansiService(IDbHelper db) : base(db)
        {

        }

        public DataSourceResult GetDataSourceResult(DataSourceRequest request)
        {
            var result = Db.InvoiceRepository.GetDataSourceResult(request);
            return result;
        }

        public DataSourceResult GetViewDataSourceResult(DataSourceRequest request, int invoice_type, string customer_group)
        {
            var result = Db.InvoiceRepository.GetViewDataSourceResult(request, invoice_type, customer_group,"",true);
            return result;
        }
    }
}
