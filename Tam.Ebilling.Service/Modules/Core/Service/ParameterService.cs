﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Tam.Ebilling.Domain.Modules.Core.Model;
using Tam.Ebilling.Domain;
using System.Collections.Generic;
using System.Linq;
using Dapper;

namespace Tam.Ebilling.Service
{
    public class ParameterService : DbServiceBase
    {
        public ParameterService(IDbHelper db) : base(db)
        {

        }

        public udf_GetParameter GetParameter(string key_param)
        {
            var d = Db.Connection.Query<udf_GetParameter>("SELECT * FROM [dbo].[udf_GetParameter](@key_param)", new { key_param = key_param}).FirstOrDefault();
            return d;
        }

        public IList<udf_GetParameter> GetParameterList(string key_param)
        {
            var d = Db.Connection.Query<udf_GetParameter>("SELECT * FROM [dbo].[udf_GetParameter](@key_param)", new { key_param = key_param }).ToList();
            return d;
        }

        public udf_GetParameterNotif GetParameterNotif(string parameter_code)
        {
            var d = Db.Connection.Query<udf_GetParameterNotif>("SELECT * FROM [dbo].[udf_GetParameterNotif](@parameter_code)", new { parameter_code = parameter_code  }).FirstOrDefault();
            return d;
        }

        public Parameter GetParameterById(int parameterId)
        {
            return Db.ParameterRepository.Find(new { parameter_id = parameterId }).FirstOrDefault();
        }
    }
}
