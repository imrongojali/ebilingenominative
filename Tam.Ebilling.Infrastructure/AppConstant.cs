﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tam.Ebilling.Infrastructure
{
    public static class AppConstants
    {
        public const string SessionKey = "AppUserSession";
        public const string TokenSessionKey = "AppTokenSession";
        public const string NoRecordsText = "No records yet";

        public class CoreRoles
        {
            public const string DefaultUser = "DEFAULT_USER";
            public const string Administrator = "ADMINISTRATOR";
        }
    }
}
