﻿using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Tam.Ebilling.Domain;
using System.Web;

namespace Tam.Ebilling.Infrastructure.Mvc
{
    public class WebControllerBase : Controller
    {
        protected T Service<T>() where T : DbServiceBase
        {
            return ObjectFactory.GetInstance<T>();
        }

        public object GetForm<T>(string key, bool validated = true)
        {
            var values = validated ? System.Web.HttpContext.Current.Request.Form.GetValues(key) : System.Web.HttpContext.Current.Request.Unvalidated.Form.GetValues(key);
            if (values != null && values.Length > 0)
            {
                return (T)Convert.ChangeType(values[0], typeof(T));
            }

            return null;
        }

        public object GetQueryString<T>(string key, bool validated = true)
        {
            var values = validated ? System.Web.HttpContext.Current.Request.QueryString.GetValues(key) : System.Web.HttpContext.Current.Request.Unvalidated.QueryString.GetValues(key);
            if (values != null && values.Length > 0)
            {
                return (T)Convert.ChangeType(values[0], typeof(T));
            }

            return null;
        }

        public Guid ParseID(string key, bool encrypt = true)
        {
            Guid id = default(Guid);
            var values = System.Web.HttpContext.Current.Request.QueryString.GetValues(key);

            if (values != null && values.Length > 0)
            {
                Guid.TryParse(values[0], out id);
            }

            return id;
        }

        protected virtual void DownloadFinish(bool _status = true)
        {
            try
            {
                System.Web.HttpCookie myCookie = new System.Web.HttpCookie("fileDownload");
                myCookie.Value = "true";


                System.Web.HttpCookie myCookieStatus = new System.Web.HttpCookie("fileDownloadStatus");
                myCookieStatus.Value = _status.ToString();

                System.Web.HttpContext.Current.Response.Cookies.Add(myCookie);
                System.Web.HttpContext.Current.Response.Cookies.Add(myCookieStatus);
            }
            catch (Exception)
            {
                 
            }
            
        }
    }

}
