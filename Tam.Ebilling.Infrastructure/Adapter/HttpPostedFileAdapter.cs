﻿using System;
using System.Web;

namespace Tam.Ebilling.Infrastructure.Adapter
{
    public class HttpPostedFileAdapter : IPostedFile
    {
        public HttpPostedFile FileBase { get; private set; }
        public HttpPostedFileAdapter(HttpPostedFile fileBase)
        {
            FileBase = fileBase;
        }

        public string FileName { get { return FileBase.FileName; } }
        public string ContentType { get { return FileBase.ContentType; } }
        public int ContentLength { get { return FileBase.ContentLength; } }

        public void SaveAs(string filename)
        {
            FileBase.SaveAs(filename);
        }
    }
}
