﻿using Agit.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Tam.Ebilling.Infrastructure.Session
{
    public static class SessionManager
    {
        public static UserSession UserSession
        {
            get
            {
                return HttpContext.Current.Session == null ? null : (UserSession)HttpContext.Current.Session[AppConstants.SessionKey];
            }
        }

        public static string Current { get { return UserSession.Current; } }
        public static string Name { get { return UserSession.Name; } }
        public static string Username { get { return UserSession.Username; } }
        public static string RoleStr { get { return UserSession.RoleStr; } }
        public static IEnumerable<string> Roles { get { return UserSession.Roles; } }
        public static string UserType { get { return UserSession.UserType; } }
        public static string LevelCode { get { return UserSession.LevelCode; } }
    }
}
