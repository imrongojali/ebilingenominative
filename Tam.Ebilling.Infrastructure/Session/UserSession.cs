﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tam.Ebilling.Infrastructure.Session
{
    [Serializable]
    public class UserSession
    {
        public string Current { get; set; }
        public string Name { get; set; }
        public string Username { get; set; }
        public string CustomerGroup { get; set; }
        public string UserType { get; set; }
        public string RoleStr { get; set; }
        public string[] Roles { get { return RoleStr.Split(','); } }
        public string LevelCode { get; set; }
    }
}
