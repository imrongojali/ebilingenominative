﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Tam.Ebilling.Domain;

namespace Tam.Ebilling.Infrastructure.Navigation
{
    public class MenuBuilder
    {
        //private IEnumerable<RolePermission> _acl;
        private IEnumerable<string> _actors;
        private IEnumerable<Menu> _menus;
        private readonly Dictionary<int, Menu> _selectedMenus = new Dictionary<int, Menu>();

        //public MenuBuilder(IEnumerable<RolePermission> acl, IEnumerable<string> actors, IEnumerable<Menu> menus)
        //{
        //    _acl = acl;
        //    _actors = actors;
        //    _menus = menus;
        //}

        public MenuBuilder(IEnumerable<Menu> menus)
        {
            _menus = menus;
        }

        public MvcHtmlString Render()
        {
            DeepSelect(HttpContext.Current.Request.Url.AbsoluteUri);

            var sb = new StringBuilder();

            var roots = _menus.Where(x => x.parent_id >= 0).OrderBy(x => x.order_index);

            sb.Append(@"<ul class=""page-sidebar-menu page-sidebar-menu-closed"" data-keep-expanded=""false"" data-auto-scroll=""true"" data-slide-speed=""200"">");
            foreach (var root in roots)
            {
                sb.Append(Render(root));
            }
            sb.Append("</ul>");

            return new MvcHtmlString(sb.ToString());
        }

        private string Render(Menu menu)
        {
            var selected = _selectedMenus.ContainsKey(menu.menu_id);
            var sb = new StringBuilder();
            var childs = _menus.Where(x => x.parent_id == menu.menu_id).OrderBy(x => x.order_index);
            var hasChild = childs.Count(x => x.visible == 1) > 0;

            //if (!_actors.Any(x => _acl.Any(y => y.RoleName == x && y.PermissionName == menu.PermissionName)) || menu.Visible != 1) return sb.ToString();

            sb.Append(string.Format(@"<li class=""nav-item {0}"">", selected ? " active open" : string.Empty));

            sb.Append(string.Format(@"<a href=""{0}"" class=""nav-link {1}"">", !string.IsNullOrEmpty(menu.url) && !menu.url.Contains("#") ? VirtualPathUtility.ToAbsolute(menu.url) : "#", hasChild ? "nav-toggle" : string.Empty));
            sb.Append(string.Format(@"<i class=""{0}""></i>", menu.icon_class));
            sb.Append(string.Format(@"<span class=""title"">&nbsp;&nbsp;{0}</span>", menu.title));

            if (hasChild)
                sb.Append(string.Format(@"<span class=""arrow {0}""></span>", selected ? "open" : string.Empty));

            if (selected)
                sb.Append(@"<span class=""selected""></span>");

            sb.Append("</a>");

            if (hasChild)
            {
                sb.Append(@"<ul class=""sub-menu"">");
                foreach (var child in childs)
                {
                    sb.Append(Render(child));
                }
                sb.Append("</ul>");
            }

            sb.Append(@"</li>");

            return sb.ToString();
        }

        private string GetAbsoluteUrl(string url)
        {
            if (!string.IsNullOrEmpty(url))
                return new System.Uri(HttpContext.Current.Request.Url,
                    System.Web.VirtualPathUtility.ToAbsolute(url)).AbsoluteUri;

            return url;
        }
        private void DeepSelect(string uri)
        {
            var menu = _menus.FirstOrDefault(x => GetAbsoluteUrl(x.url) == uri);

            if (menu == null) return;

            while (menu != null)
            {
                _selectedMenus.Add(menu.menu_id, menu);

                menu = _menus.FirstOrDefault(x => x.menu_id == menu.parent_id);
            }
        }
    }
}
