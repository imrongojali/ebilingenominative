﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Tam.Ebilling.Domain;
using Agit.Domain;

namespace Tam.Ebilling.Infrastructure.Cache
{
    public static class ApplicationCacheManager
    {
        public static readonly string AppConfigCacheKey = "app_config";
        public static readonly string GeneralCategoryCacheKey = "generalcategory_config";
        public static readonly string PermissionCacheKey = "permission_config";
        public static readonly string PermissionMenuCacheKey = "permissionmenu_config";
        private static object _locker = new object();

        public static void GenerateConfigs()
        {
            lock (_locker)
            {
                using (var Db = new DbHelper())
                {
                    var config = Db.ConfigRepository.FindAll().ToList();

                    Set(AppConfigCacheKey, config);
                }
            }
        }

        //public static void RefreshRolePermissions()
        //{
        //    lock (_locker)
        //    {
        //        using (var Db = new DbHelper())
        //        {
        //            var rolePermission = Db.RolePermissionRepository.FindAll().ToList();
        //            var menu = Db.MenuRepository.FindAll().ToList();

        //            Set(PermissionCacheKey, rolePermission);
        //            Set(PermissionMenuCacheKey, menu);
        //        }
        //    }
        //}

        public static void RefreshMenu()
        {
            lock (_locker)
            {
                using (var Db = new DbHelper())
                {
                    var menu = Db.MenuRepository.FindAll().ToList();

                    Set(PermissionMenuCacheKey, menu);
                }
            }
        }

        //wo.apiyudin permission PV Monitoring
        public static bool IsRoleAllowedAccessMenu(int menuId, string userType, string user)
        {
            bool isValid;

            using (var Db = new DbHelper())
            {
                isValid = Db.MenuRoleRepository.GetAllowedByUserTypeUsername(menuId, userType, user);
            }

            return isValid;
        }
        //end

        public static T Get<T>(string key)
        {
            var cache = HttpContext.Current.Cache;

            return (T)cache[key];
        }

        public static void Set(string key, object obj)
        {
            var cache = HttpContext.Current.Cache;
            var objCache = cache.Get(key);

            if (objCache != null)
            {
                cache.Remove(key);
            }

            cache.Insert(key, obj);
        }

        //public static IEnumerable<RolePermission> GetPermissions(IEnumerable<string> roles)
        //{
        //    if (roles == null) return new List<RolePermission>();

        //    var cache = HttpContext.Current.Cache;
        //    var permissions = cache.Get(PermissionCacheKey) as IEnumerable<RolePermission>;

        //    return permissions.Where(x => roles.Contains(x.RoleName));
        //}

        public static T GetConfig<T>(string key, string module = null)
        {
            var value = string.Empty;

            if (HttpContext.Current != null)
            {
                var cache = HttpContext.Current.Cache;
                var configurations = cache.Get(AppConfigCacheKey) as IEnumerable<Config>;
                Func<Config, bool> expr = null;

                if (!string.IsNullOrEmpty(module))
                    expr = x => x.config_key == key && x.module == module;
                else
                    expr = x => x.config_key == key;

                value = configurations.FirstOrDefault(expr).config_value;
            }
            else
            {
                value = ConfigurationManager.AppSettings[key];
            }

            return (T)System.Convert.ChangeType(value, typeof(T));
        }

    }
}
