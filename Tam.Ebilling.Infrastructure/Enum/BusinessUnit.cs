﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tam.Ebilling.Infrastructure.Enum
{
    public enum BusinessUnit
    {
        [Description("1")]
        MotorVehicle,
        [Description("2")]
        Others
    }
}
