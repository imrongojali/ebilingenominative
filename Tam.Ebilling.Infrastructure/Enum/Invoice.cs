﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tam.Ebilling.Infrastructure.Enum
{
    public enum InvoiceType
    {
        [Description("1")]
        Invoice,
        [Description("2")]
        NotaRetur,
        [Description("3")]
        Kwitansi
    }
}
