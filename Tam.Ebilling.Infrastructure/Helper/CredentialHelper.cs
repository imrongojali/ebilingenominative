﻿using System.Net;
using Tam.Ebilling.Infrastructure.Cache;

namespace Tam.Ebilling.Infrastructure.Helper
{
    public class CredentialHelper
    {

        public static NetworkCredential Credential()
        {
            var usernameCredential = ApplicationCacheManager.GetConfig<string>("FileUploadUserName");
            var passwordCredential = ApplicationCacheManager.GetConfig<string>("FileUploadPassword");

            return new NetworkCredential(usernameCredential, passwordCredential);
        }

    }
}
