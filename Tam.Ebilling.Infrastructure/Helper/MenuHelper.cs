﻿using Tam.Ebilling.Domain;
using Tam.Ebilling.Infrastructure.Cache;
using Tam.Ebilling.Infrastructure.Navigation;
using Tam.Ebilling.Infrastructure.Session;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Tam.Ebilling.Infrastructure.Helper
{
    public static class MenuHelpers
    {
        public static MvcHtmlString Sidebar()
        {
            var menus = ApplicationCacheManager.Get<IEnumerable<Menu>>(ApplicationCacheManager.PermissionMenuCacheKey);
            //var rolePermissions = ApplicationCacheManager.Get<IEnumerable<RolePermission>>(ApplicationCacheManager.PermissionCacheKey);
            //var extendedRoles = new List<string>();

            //var roles = SessionManager.Roles.Union(extendedRoles).ToList();

            var sidebarMenu = menus.Where(x => x.group_name == "Sidebar").ToList();
            
            if(SessionManager.UserType == "Customer")
            {
                sidebarMenu.RemoveAll(m => m.url.Contains("~/Core/SummaryInvoice") || m.url.Contains("~/Core/Receipt"));
            }

            //wo.apiyudin permission PV Monitoring
            HashSet<Menu> menu = new HashSet<Menu>();
            string[] listRemove = new string[] { "PVMonitoring", "SummaryInvoice", "Receipt" };

            foreach (string lr in listRemove)
            {
                menu.Add(menus.FirstOrDefault(m => m.url.Contains(lr)));
            }

            if (menu.Count() > 0)
            {
                foreach (Menu mn in menu)
                {
                    if (!ApplicationCacheManager.IsRoleAllowedAccessMenu(mn.menu_id, SessionManager.UserType, SessionManager.Current))
                    {
                        sidebarMenu.RemoveAll(m => m.url.Contains(mn.url));
                    }
                }
            }
            //end

            var menuBuilder = new MenuBuilder(sidebarMenu);

            return menuBuilder.Render();
        }

    }
}
