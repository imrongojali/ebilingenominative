﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.IO;
using System.Net.Mail;
using Tam.Ebilling.Domain;
using System.Net;
using System.Globalization;
using System.Text;

namespace Tam.Ebilling.NotificationWo
{
    public class SendMailService
    {
        public static void WriteErrorLog(Exception ex)
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", true);
                sw.WriteLine(DateTime.Now.ToString() + ": " + ex.Source.ToString().Trim() + "; " + ex.Message.ToString().Trim());
                sw.Flush();
                sw.Close();
            }
            catch
            {
            }
        }
        // This function write Message to log file.
        public static void WriteErrorLog(string Message)
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", true);
                sw.WriteLine(DateTime.Now.ToString() + ": " + Message);
                sw.Flush();
                sw.Close();
            }
            catch
            {
            }
        }

        public static void WriteAppLog(string logType, string logDesc)
        {
            using (var db = new DbHelper())
            {
                var log = new ApplicationLog
                {
                    created_by = "Service",
                    created_date = DateTime.Now,
                    username = "Service",
                    message_type = logType,
                    message_location = string.Format("<b>Module:</b> {0}", "Invoice Notification WO Service"),
                    message_description = logDesc,
                    iP = Dns.GetHostAddresses(Dns.GetHostName())[0].ToString(),
                    browser = string.Format("<b>Browser:</b> {0}<br/><b>Version:</b> {1}", "None", "None")
                };

                db.LogRepository.Add(log, null);
            }
        }

        public static void SendEmailBuktiPotong()
        {
            string username, password, domain, host, port, subject, from, url, deadline;
            
            try
            {
                using (var db = new DbHelper())
                {
                    WriteAppLog("Info", "Bukti Potong Due Date Notification Service is starting.");

                    username = db.ParameterRepository.Find(new { key_param = "email_username_chat_notification" }).FirstOrDefault().value1;
                    password = db.ParameterRepository.Find(new { key_param = "email_password_chat_notification" }).FirstOrDefault().value1;
                    domain = db.ParameterRepository.Find(new { key_param = "email_domain_chat_notification" }).FirstOrDefault().value1;
                    host = db.ParameterRepository.Find(new { key_param = "email_host_chat_notification" }).FirstOrDefault().value1;
                    port = db.ParameterRepository.Find(new { key_param = "email_port_chat_notification" }).FirstOrDefault().value1;
                    subject = db.EmailTemplateRepository.Find(new { module = "Upload Bukti Potong Email Reminder", mail_key = "email_key" }).FirstOrDefault().subject;
                    
                    from = db.ParameterRepository.Find(new { key_param = "email_username" }).FirstOrDefault().value1;
                    url = db.ParameterRepository.Find(new { key_param = "bukti_potong_url" }).FirstOrDefault().value1;

                    deadline = db.ParameterRepository.Find(new { key_param = "bukit_potong_deadline" }).FirstOrDefault().value1;

                    

                    List<BuktiPotong> dueDateBuktiPotong = db.BuktiPotongRepository.GetDueDatedBuktiPotong(deadline);

                    List<BuktiPotong> groupedCustomerBuktiPotong = dueDateBuktiPotong
                        .GroupBy(b => b.customer_group)
                        .Select(s => s.First())
                        .ToList();

                    

                    foreach (BuktiPotong bp in groupedCustomerBuktiPotong)
                    {
                        HashSet<string> to = new HashSet<string>();
                        string body = BuildBuktiPotongBody(url, db, dueDateBuktiPotong, bp);

                        var users = db.UserRepository.Find(new { customer_group = bp.customer_group });

                        foreach (var usr in users)
                        {
                            if (!System.Diagnostics.Debugger.IsAttached)
                            {
                                if (!string.IsNullOrEmpty(usr.email))
                                {
                                    to.Add(usr.email);
                                }
                            }
                            else
                            {
                                to.Add(db.ParameterRepository.Find(new { key_param = "email_to_chat_notification" }).FirstOrDefault().value1);
                            }
                        }

                        //EmailService emailSvc = new EmailService();
                        SmtpClient emailSvc = new SmtpClient();
                        emailSvc.Host = host;
                        emailSvc.Port = int.Parse(port);
                        //emailSvc.User = username;
                        //emailSvc.Password = password;
                        emailSvc.UseDefaultCredentials = false;
                        emailSvc.Credentials = new NetworkCredential(username, password);
                        emailSvc.EnableSsl = false;
                        if (from.Contains("gmail")) { emailSvc.EnableSsl = true; }

                        //EmailMessage emailMessage = new EmailMessage();
                        MailMessage emailMessage = new MailMessage(from, from);
                        emailMessage.From = new MailAddress(from);
                        emailMessage.To.RemoveAt(0);
                        emailMessage.Subject = subject;
                        emailMessage.Body = body;
                        emailMessage.IsBodyHtml = true;

                        if (to.Count > 0)
                        {
                            foreach (var mail in to)
                            {
                                //emailMessage.Recipients.Add(mail);
                                emailMessage.To.Add(new MailAddress(mail));
                            }

                            emailSvc.Send(emailMessage);
                        }
                    }
                }

                WriteAppLog("Info", "Bukti Potong Due Date Notification Service successfully ended.");
            }
            catch (Exception ex)
            {
                WriteAppLog("Err", "Bukti Potong Due Date Notification error: " + ex.Message + ". Stack Trace: " + ex.StackTrace + ".");
                throw;
            }
        }

        private static string BuildBuktiPotongBody(string url, DbHelper db, List<BuktiPotong> dueDateBuktiPotong, BuktiPotong distinctBuktiPotonog)
        {
            StringBuilder stringBuilder = new StringBuilder();
            string body = db.EmailTemplateRepository.Find(new { module = "Upload Bukti Potong Email Reminder", mail_key = "email_key" }).FirstOrDefault().mail_content;
            body = body.Replace("{url}", url);
            body = body.Replace("{NamaDealer}", distinctBuktiPotonog.customer_name);
            foreach (BuktiPotong bpDetail in dueDateBuktiPotong.Where(b=> b.customer_group == distinctBuktiPotonog.customer_group))
            {
                stringBuilder.AppendFormat("<tr><td>{0}</td></tr>", bpDetail.invoice_number);
            }

            body = body.Replace("{Detail}", stringBuilder.ToString());
            return body;
        }

        public static void SendEmailVATCreditNoteAvailable()
        {
            string username, password, domain, host, port, subject, from, url, deadline;
            

            try
            {
                using (var db = new DbHelper())
                {
                    WriteAppLog("Info", "Nota Retur Available Notification Service is starting.");

                    username = db.ParameterRepository.Find(new { key_param = "email_username_chat_notification" }).FirstOrDefault().value1;
                    password = db.ParameterRepository.Find(new { key_param = "email_password_chat_notification" }).FirstOrDefault().value1;
                    domain = db.ParameterRepository.Find(new { key_param = "email_domain_chat_notification" }).FirstOrDefault().value1;
                    host = db.ParameterRepository.Find(new { key_param = "email_host_chat_notification" }).FirstOrDefault().value1;
                    port = db.ParameterRepository.Find(new { key_param = "email_port_chat_notification" }).FirstOrDefault().value1;
                    subject = db.EmailTemplateRepository.Find(new { module = "Notification Nota Retur Available", mail_key = "email_key" }).FirstOrDefault().subject;

                    from = db.ParameterRepository.Find(new { key_param = "email_username" }).FirstOrDefault().value1;
                    url = db.ParameterRepository.Find(new { key_param = "vat_credit_note_url" }).FirstOrDefault().value1;

                    deadline = db.ParameterRepository.Find(new { key_param = "vat_credit_note_deadline" }).FirstOrDefault().value1;

                    HashSet<string> uniqueCC = new HashSet<string>();
                    GetEmailCC(db, uniqueCC);

                    var availableInvoices = db.InvoiceRepository.GetInvoicesAvailableYesterday("2");

                    var groupedCustomerInvoice = availableInvoices
                        .GroupBy(b => b.customer_group)
                        .Select(s => s.First())
                        .ToList();
                    
                    foreach (Invoice i in groupedCustomerInvoice)
                    {
                        HashSet<string> to = new HashSet<string>();

                        string body = BuildNotaReturBody(url, db, availableInvoices, i);

                        var users = db.UserRepository.Find(new { customer_group = i.customer_group });

                        foreach (var usr in users)
                        {
                            //if (!System.Diagnostics.Debugger.IsAttached)
                            //{
                                if (!string.IsNullOrEmpty(usr.email))
                                {
                                    to.Add(usr.email);
                                }
                            //}
                            //else
                            //{
                            //    to.Add(db.ParameterRepository.Find(new { key_param = "email_to_chat_notification" }).FirstOrDefault().value1);
                            //}
                        }

                        //EmailService emailSvc = new EmailService();
                        SmtpClient emailSvc = new SmtpClient();
                        emailSvc.Host = host;
                        emailSvc.Port = int.Parse(port);
                        //emailSvc.User = username;
                        //emailSvc.Password = password;
                        emailSvc.UseDefaultCredentials = false;
                        emailSvc.Credentials = new NetworkCredential(username, password);
                        emailSvc.EnableSsl = false;
                        if (from.Contains("gmail")) { emailSvc.EnableSsl = true; }

                        //EmailMessage emailMessage = new EmailMessage();
                        MailMessage emailMessage = new MailMessage(from, from);
                        emailMessage.From = new MailAddress(from);
                        emailMessage.To.RemoveAt(0);
                        emailMessage.Subject = subject;
                        emailMessage.Body = body;
                        emailMessage.IsBodyHtml = true;

                        if (to.Count > 0)
                        {
                            foreach (var mail in to)
                            {
                                //emailMessage.Recipients.Add(mail);
                                emailMessage.To.Add(new MailAddress(mail));
                            }

                            foreach (var mailCC in uniqueCC)
                            {
                                //emailMessage.Recipients.Add(mail);
                                emailMessage.CC.Add(new MailAddress(mailCC));
                            }

                            emailSvc.Send(emailMessage);
                        }
                    }
                }

                WriteAppLog("Info", "Nota Retur Available Notification Service successfully ended.");
            }
            catch (Exception ex)
            {
                WriteAppLog("Err", "Nota Retur Available Notification error: " + ex.Message + ". Stack Trace: " + ex.StackTrace + ".");
                throw;
            }
        }

        private static string BuildNotaReturBody(string url, DbHelper db, List<Invoice> availableInvoices, Invoice distinctInvoice)
        {
            StringBuilder stringBuilder = new StringBuilder();
            string body = db.EmailTemplateRepository.Find(new { module = "Notification Nota Retur Available", mail_key = "email_key" }).FirstOrDefault().mail_content;
            body = body.Replace("{url}", url);
            body = body.Replace("{NamaDealer}", distinctInvoice.customer_name);
            foreach (Invoice bpDetail in availableInvoices.Where(b => b.customer_group == distinctInvoice.customer_group))
            {
                stringBuilder.AppendFormat("<tr><td>{0}</td></tr>", bpDetail.invoice_number);
            }

            body = body.Replace("{Detail}", stringBuilder.ToString());
            return body;
        }

        public static void SendEmailVATCreditNoteDueDate()
        {
            string username, password, domain, host, port, subject, from, url, deadline, cc;
            

            try
            {
                using (var db = new DbHelper())
                {
                    WriteAppLog("Info", "Nota Retur Reminder Notification Service is starting.");

                    username = db.ParameterRepository.Find(new { key_param = "email_username_chat_notification" }).FirstOrDefault().value1;
                    password = db.ParameterRepository.Find(new { key_param = "email_password_chat_notification" }).FirstOrDefault().value1;
                    domain = db.ParameterRepository.Find(new { key_param = "email_domain_chat_notification" }).FirstOrDefault().value1;
                    host = db.ParameterRepository.Find(new { key_param = "email_host_chat_notification" }).FirstOrDefault().value1;
                    port = db.ParameterRepository.Find(new { key_param = "email_port_chat_notification" }).FirstOrDefault().value1;
                    subject = db.EmailTemplateRepository.Find(new { module = "Notification Nota Retur Reminder", mail_key = "email_key" }).FirstOrDefault().subject;

                    from = db.ParameterRepository.Find(new { key_param = "email_username" }).FirstOrDefault().value1;
                    url = db.ParameterRepository.Find(new { key_param = "vat_credit_note_url" }).FirstOrDefault().value1;

                    deadline = db.ParameterRepository.Find(new { key_param = "vat_credit_note_deadline" }).FirstOrDefault().value1;

                    HashSet<string> uniqueCC = new HashSet<string>();
                    GetEmailCC(db, uniqueCC);

                    var availableInvoices = db.InvoiceRepository.GetInvoicesDueDateVAT("2", deadline);

                    var groupedCustomerInvoice = availableInvoices
                        .GroupBy(b => b.customer_group)
                        .Select(s => s.First())
                        .ToList();



                    foreach (Invoice i in groupedCustomerInvoice)
                    {
                        HashSet<string> to = new HashSet<string>();

                        string body = BuildVatCreditNoteDueDateBody(url, db, availableInvoices, i);

                        var users = db.UserRepository.Find(new { customer_group = i.customer_group });

                        foreach (var usr in users)
                        {
                            if (!string.IsNullOrEmpty(usr.email))
                            {
                                to.Add(usr.email);
                            }

                            //if (!System.Diagnostics.Debugger.IsAttached)
                            //{
                            //    if (!string.IsNullOrEmpty(usr.email))
                            //    {
                            //        to.Add(usr.email);
                            //    }
                            //}
                            //else
                            //{
                            //    to.Add(db.ParameterRepository.Find(new { key_param = "email_to_chat_notification" }).FirstOrDefault().value1);
                            //}
                        }

                        //EmailService emailSvc = new EmailService();
                        SmtpClient emailSvc = new SmtpClient();
                        emailSvc.Host = host;
                        emailSvc.Port = int.Parse(port);
                        //emailSvc.User = username;
                        //emailSvc.Password = password;
                        emailSvc.UseDefaultCredentials = false;
                        emailSvc.Credentials = new NetworkCredential(username, password);
                        emailSvc.EnableSsl = false;
                        if (from.Contains("gmail")) { emailSvc.EnableSsl = true; }

                        //EmailMessage emailMessage = new EmailMessage();
                        MailMessage emailMessage = new MailMessage(from, from);
                        emailMessage.From = new MailAddress(from);
                        emailMessage.To.RemoveAt(0);
                        emailMessage.Subject = subject;
                        emailMessage.Body = body;
                        emailMessage.IsBodyHtml = true;

                        if (to.Count > 0)
                        {
                            foreach (var mail in to)
                            {
                                //emailMessage.Recipients.Add(mail);
                                emailMessage.To.Add(new MailAddress(mail));
                            }

                            foreach (var mailCC in uniqueCC)
                            {
                                //emailMessage.Recipients.Add(mail);
                                emailMessage.CC.Add(new MailAddress(mailCC));
                            }

                            emailSvc.Send(emailMessage);
                        }
                    }
                }

                WriteAppLog("Info", "Nota Retur Reminder Notification Service successfully ended.");
            }
            catch (Exception ex)
            {
                WriteAppLog("Err", "Nota Retur Reminder Notification error: " + ex.Message + ". Stack Trace: " + ex.StackTrace + ".");
                throw;
            }
        }

        private static void GetEmailCC(DbHelper db, HashSet<string> uniqueCC)
        {
            var ccList = db.ParameterRepository.Find(new { key_param = "email_vat_credit_note_cc" });
            foreach (Parameter p in ccList)
            {
                uniqueCC.Add(p.value1);
            }
        }

        private static string BuildVatCreditNoteDueDateBody(string url, DbHelper db, List<Invoice> availableInvoices, Invoice distinctInvoice)
        {
            StringBuilder stringBuilder = new StringBuilder();
            string body = db.EmailTemplateRepository.Find(new { module = "Notification Nota Retur Reminder", mail_key = "email_key" }).FirstOrDefault().mail_content;
            body = body.Replace("{url}", url);
            body = body.Replace("{NamaDealer}", distinctInvoice.customer_name);
            foreach (Invoice bpDetail in availableInvoices.Where(b => b.customer_group == distinctInvoice.customer_group))
            {
                stringBuilder.AppendFormat("<tr><td>{0}</td></tr>", bpDetail.invoice_number);
            }

            body = body.Replace("{Detail}", stringBuilder.ToString());
            return body;
        }

        //public static void SendEmail(String ToEmail)
        public static void SendEmail()
        {
            // these method belong to Tam.Ebilling.Notification
            //SendEmailInvoice();
            //SendEmailNotur();
            //SendEmailMV();
            //SendEmailDueDate(6);
            //SendEmailDueDate(4);
            //SendEmailDueDate(2);

            // added by wo\dwi
            SendEmailBuktiPotong();
            SendEmailVATCreditNoteAvailable();
            SendEmailVATCreditNoteDueDate();
        }
    }
}
