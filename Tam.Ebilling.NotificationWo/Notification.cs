﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace Tam.Ebilling.NotificationWo
{
    public partial class Notification : ServiceBase
    {
        private System.Timers.Timer timer = new System.Timers.Timer();

        private bool isInProgress;

        public Notification()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                SendMailService.WriteAppLog("Info", "Starting Service Notification WO...");

                isInProgress = false;
                timer.Enabled = true;
                timer.Interval = Convert.ToInt32(ConfigurationManager.AppSettings["Interval"].ToString()) * 1000;
                timer.Elapsed += new ElapsedEventHandler(ServiceTimer_Tick);
            }
            catch (Exception ex)
            {
                SendMailService.WriteAppLog("Err", "Error while starting Notification WO Service. Message: " + ex.Message + ". Stacktrace: + " + ex.StackTrace);
            }
        }

        protected override void OnStop()
        {
            isInProgress = false;
            SendMailService.WriteAppLog("Info", "Stopping Service Notification WO...");
        }

        private void ServiceTimer_Tick(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (!isInProgress)
            {
                DateTime dateschedule = Convert.ToDateTime(ConfigurationManager.AppSettings["ScheduleTime"]);
                if (DateTime.Now.Hour == dateschedule.Hour && DateTime.Now.Minute == dateschedule.Minute)
                {
                    try
                    {
                        isInProgress = true;
                        SendMailService.SendEmail();
                        Thread.Sleep(60 * 1000);   // hold 1 menit biar tidak masuk lagi di next interval
                        isInProgress = false;
                    }
                    catch (Exception ex)
                    {
                        SendMailService.WriteAppLog("Err", "Error while looping service timer. Message: " + ex.Message + ". Stacktrace: + " + ex.StackTrace);
                        isInProgress = false;

                    }
                }
            }
        }
    }
}
