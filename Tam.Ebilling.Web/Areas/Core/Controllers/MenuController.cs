﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Tam.Ebilling.Infrastructure.Mvc;
using Tam.Ebilling.Service;
using Tam.Ebilling.Infrastructure.Cache;

namespace Tam.Ebilling.Web.Areas.Core.Controllers
{
    public class MenuController : WebControllerBase
    {
        public ActionResult Index()
        {
            var menuGroups = Service<MenuGroupService>().GetMenuGroups();
            //var menuPermissions = Service<PermissionService>().GetMenuPermissions();

            ViewBag.MenuGroups = menuGroups;
            //ViewBag.MenuPermissions = menuPermissions;

            return View();
        }

        [HttpPost]
        public ActionResult Save(Domain.Menu menu)
        {
            var output = Service<MenuService>().Save(menu);

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpPost]
        public ActionResult Get()
        {
            var id = int.Parse(GetForm<string>("id").ToString());
            var result = Service<MenuService>().GetByID(id);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetsByGroupID()
        {
            var groupID = GetForm<string>("GroupID").ToString();
            var result = Service<MenuService>().GetByGroupID(int.Parse(groupID));

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ToggleVisible()
        {
            var id = int.Parse(GetForm<string>("id").ToString());
            Service<MenuService>().ToggleVisible(id);

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpPost]
        public ActionResult Rename(MenuDataRequest request)
        {
            Service<MenuService>().Rename(request.ID, request.Title);

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpPost]
        public ActionResult UpdateParent(MenuDataRequest request)
        {
            Service<MenuService>().UpdateParent(request.ID, request.ParentID, request.MenuGroupID, request.OrderIndex);

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpPost]
        public ActionResult DeleteMenu(int id)
        {
            var result = Service<MenuService>().Delete(id);

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        public ActionResult RefreshMenu()
        {
            ApplicationCacheManager.RefreshMenu();

            return RedirectToAction("Index");
        }
    }
}