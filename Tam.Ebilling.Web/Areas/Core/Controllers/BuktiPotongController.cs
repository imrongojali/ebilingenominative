﻿using Kendo.Mvc.UI;
using Microsoft.Reporting.WebForms;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tam.Ebilling.Domain;
using Tam.Ebilling.Infrastructure.Mvc;
using Tam.Ebilling.Infrastructure.Session;
using Tam.Ebilling.Infrastructure.Enum;
using Tam.Ebilling.Service;
using Agit.Helper.Helper;
using Tam.Ebilling.Infrastructure.Helper;
using System.Net;
using Tam.Ebilling.Infrastructure.Cache;
using Tam.Ebilling.Reporting;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System.Text;
using System.Globalization;
using System.Data;
using Tam.Ebilling.Web.Areas.Core.Models;
using Tam.Ebilling.Infrastructure.Adapter;
using Agit.Common.Email;
using Tam.Ebilling.Web.Helper;

namespace Tam.Ebilling.Web.Areas.Core.Controllers
{
    public class BuktiPotongController : CommonFeatureController
    {
        #region excel row
        public const int DATA_ROW_INDEX_START = 1;

        public const int COL_IDX_BUSINESS_UNIT = 0;
        public const int COL_IDX_NO_NPWP = 1;
        public const int COL_IDX_INVOICE_NUMBER = 2;
        public const int COL_IDX_INVOICE_DATE = 3;
        public const int COL_IDX_FLAG_POTONG_PAJAK = 4;
        public const int COL_IDX_MATERAI_AMOUNT = 5;
        public const int COL_IDX_TGL_PAYMENT = 6;
        public const int COL_IDX_NOMOR_BUKTI_POTONG = 7;
        public const int COL_IDX_TANGGAL_BUKTI_POTONG = 8;
        public const int COL_IDX_JUMLAH_DPP = 9;
        public const int COL_IDX_TARIF = 10;
        public const int COL_IDX_JUMLAH_PPH = 11;
        public const int COL_IDX_JENIS_PAJAK = 12;
        public const int COL_IDX_DESCRIPTION = 13;
        #endregion
        public BuktiPotongController()
        {
            try
            {
                ViewBag.Notif_Days = Service<ParameterService>().GetParameterNotif("NOTIF_DAYS_FOR_DWHT").parameter_value;

            }
            catch (Exception)
            {

                ViewBag.Notif_Days = "0";
            }
        }
        // GET: Core/Log
        public ActionResult Index()
        {
            var notifFilter = Request.QueryString["notifFilter"];
            var id = Request.QueryString["id"];
            if (notifFilter != null)
            {
                ViewBag.NotifFilter = notifFilter;
            }
            else
            {
                ViewBag.NotifFilter = "";
            }
            if (id != null)
            {
                ViewBag.ID = id;
            }
            else
            {
                ViewBag.ID = null;
            }

            var transactionCode = Service<Transaction_Code>().GetTransactionCode();
            ViewBag.TransactionTypeCode = transactionCode;

            var transactiontype = Service<TransactionTypeService>().GetTransactionType();
            ViewBag.TransactionType = transactiontype;

            var arstatus = Service<ARStatusService>().GetARStatus();
            ViewBag.ARStatus = arstatus;

            var customer = Service<CustomerService>().GetCustomerAll();
            //var customer = Service<CustomerService>().GetCustomer();
            ViewBag.CustomerName = customer;

            //var WHT_dealer_npwp = Service<WHTDealerService>().GetWHTDealerNPWP(SessionManager.RoleStr);
            //ViewBag.WHTDealerNPWP = WHT_dealer_npwp;

            //string[] NamesArray = WHT_dealer_npwp.Select(c => c.dealer_npwp).ToArray();


            var cust = SessionManager.RoleStr;
            ViewBag.Customer = cust;

            ViewBag.UserType = SessionManager.UserType;
            ViewBag.IsAllowedUploadWorklist = IsAllowed("upload_worklist");
            ViewBag.IsAllowedDownloadTemplate = IsAllowed("download_template") || ViewBag.UserType == "Customer";
            ViewBag.IsAllowedUpload = IsAllowed("upload") || ViewBag.UserType == "Customer";
            ViewBag.IsAllowedUploadGabungan = IsAllowed("upload_gabungan") || ViewBag.UserType == "Customer";

            ViewBag.IsAllowedDownloadAll = IsAllowed("download_all") || ViewBag.UserType == "Customer";
            ViewBag.IsAllowedDownloadWhtPdf = IsAllowed("download_wht_pdf") || ViewBag.UserType == "Customer";
            ViewBag.IsAllowedExportExcel = IsAllowed("export_data_to_excel") || ViewBag.UserType == "Customer";

            return View();
        }

        [HttpPost]
        public ActionResult Read([DataSourceRequest] Kendo.Mvc.UI.DataSourceRequest request, string notifFilter = null)
        {
            foreach (var item in request.Filters)
            {
                if (item.GetType().Name == "CompositeFilterDescriptor")
                {
                    var lists = (Kendo.Mvc.CompositeFilterDescriptor)item;

                    foreach (var i in lists.FilterDescriptors.ToList())
                    {
                        if (i.GetType().Name == "FilterDescriptor")
                        {
                            Kendo.Mvc.FilterDescriptor ii = (Kendo.Mvc.FilterDescriptor)i;

                            if (ii.Member == "invoice_date" && ii.Operator == Kendo.Mvc.FilterOperator.IsLessThanOrEqualTo)
                            {
                                ii.Value = DateTime.Parse(ii.Value.ToString().Replace("00:00:00", "23:59:59"));
                            }
                        }
                        else
                        {
                            Kendo.Mvc.CompositeFilterDescriptor iii = (Kendo.Mvc.CompositeFilterDescriptor)i;

                            foreach (var iiii in iii.FilterDescriptors.ToList())
                            {
                                if (iiii.GetType().Name == "FilterDescriptor")
                                {
                                    Kendo.Mvc.FilterDescriptor iiiii = (Kendo.Mvc.FilterDescriptor)iiii;

                                    if (iiiii.Member == "invoice_date" && iiiii.Operator == Kendo.Mvc.FilterOperator.IsLessThanOrEqualTo)
                                    {
                                        iiiii.Value = DateTime.Parse(iiiii.Value.ToString().Replace("00:00:00", "23:59:59"));
                                    }
                                }
                            }
                        }
                    }
                }
            }

            var result = Service<BuktiPotongService>().GetViewDataSourceResult(request, EnumHelper.GetDescription(InvoiceType.Invoice), SessionManager.RoleStr, notifFilter);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UploadFileInitial(string invoiceNo)
        {
            //var Model = Service<AttachmentNotaReturService>().GetAttachment(reference);
            List<BuktiPotongAttachment> listDocument = Service<BuktiPotongService>().GetDocumentFileByReference(invoiceNo).ToList();
            //bool alreadyExist = listDocument.Exists(e => e.bukti_potong_uploaded);
            //if (alreadyExist)
            //    throw new Exception("Invoice(s) already have Bukti Potong");

            ViewBag.ListDocument = listDocument;
            ViewBag.InvoiceNo = invoiceNo;

            var url = "~/Areas/Core/Views/BuktiPotong/Form/_BuktiPotongUpload.cshtml";
            return PartialView(url);
        }

        [HttpPost]
        public ActionResult ViewTaxTransactionCode()
        {
            //var Model = Service<AttachmentNotaReturService>().GetAttachment(reference);
            List<ViewTaxTransactionCode> listDocument = Service<BuktiPotongService>().ViewTaxTransactionCode().ToList();

            //bool alreadyExist = listDocument.Exists(e => e.bukti_potong_uploaded);
            //if (alreadyExist)
            //    throw new Exception("Invoice(s) already have Bukti Potong");

            ViewBag.ListDocument = listDocument;

            var url = "~/Areas/Core/Views/BuktiPotong/Form/_ViewTaxTransactionCode.cshtml";
            return PartialView(url);
        }

        public FileResult DownloadTemplate()
        {
            string fileName = "TemplateBuktiPotong.xlsx";
            string filesTMp = HttpContext.Request.MapPath("~/Content/template/" + fileName);

            byte[] fileBytes = System.IO.File.ReadAllBytes(filesTMp);

            return File(fileBytes, "application/vnd.ms-excel", fileName);

            //var strclmn = new string[] { "Invoice Number", "Materai Amount", "Amount WHT", "Tgl Payment"
            //    , "Tax Transaction Code", "Witholding Tax No", "Witholding Tax Date", "Tax Period Month", "Tax Period Year"
            //    , "WHT Based Amount", "WHT Tarif", "WHT Amount"
            //    , "Nomor Invoice Dasar Pemotongan", "Invoice Date" 
            //    , "Flag Witholidng Tax Object", "Description", "NPWP", "Address", "Customer Name" };

            //var strclmn = new string[] { "Invoice Number", "Materai Amount", "Tgl Payment"
            //    , "Flag Witholidng Tax Object", "Witholding Tax No", "Witholding Tax Date"
            //    , "NPWP", "Address", "Customer Name", "Tax Period Month", "Tax Period Year"
            //    , "WHT Based Amount", "WHT Tarif", "WHT Amount", "Invoice Date", "Tax Transaction Code",  "Description"};

            //using (var exportData = new MemoryStream())
            //{
            //    var workbook = new XSSFWorkbook();
            //    var sheet = workbook.CreateSheet();

            //    var sheetSample = workbook.CreateSheet("Sample");

            //    var red = workbook.CreateCellStyle();
            //    red.FillForegroundColor = IndexedColors.Red.Index;
            //    red.FillPattern = FillPattern.SolidForeground;

            //    var noColor = workbook.CreateCellStyle();
            //    noColor.FillForegroundColor = IndexedColors.White.Index;
            //    noColor.FillPattern = FillPattern.SolidForeground;

            //    var yellow = workbook.CreateCellStyle();
            //    yellow.FillForegroundColor = IndexedColors.Yellow.Index;
            //    yellow.FillPattern = FillPattern.SolidForeground;

            //    var mandatory = sheetSample.CreateRow(0);
            //    mandatory.CreateCell(0).CellStyle = red;
            //    mandatory.CreateCell(1).SetCellValue("Mandatory"); 

            //    var optional = sheetSample.CreateRow(1);
            //    optional.CreateCell(0).CellStyle = yellow;
            //    optional.CreateCell(1).SetCellValue("Optional");

            //    var headerRow = sheet.CreateRow(0);
            //    var headerRowSample = sheetSample.CreateRow(3);

            //    var x = sheetSample.CreateRow(4);
            //    var y = sheetSample.CreateRow(5);

            //    for (int i = 0; i < strclmn.Length; i++)
            //    {
            //        var headerCell = headerRow.CreateCell(i);
            //        headerCell.SetCellValue(strclmn[i]);

            //        var headerCellSample = headerRowSample.CreateCell(i);
            //        headerCellSample.SetCellValue(strclmn[i]);

            //        //header 
            //        x.CreateCell(i).CellStyle = red;
            //        y.CreateCell(i).CellStyle = red;
            //        if (i == 3)
            //        {
            //            x.CreateCell(i).SetCellValue("Y");
            //            y.CreateCell(i).SetCellValue("N");
            //        }
            //        else if (i > 3)
            //        {
            //            y.CreateCell(i).CellStyle = yellow;
            //        } 
            //        //endheader

            //        var headerCellStyle = workbook.CreateCellStyle();
            //        headerCellStyle.FillForegroundColor = IndexedColors.Aqua.Index;
            //        headerCellStyle.FillPattern = FillPattern.SolidForeground;

            //        // Add Style to Cell
            //        headerCell.CellStyle = headerCellStyle;
            //        headerCellSample.CellStyle = headerCellStyle;

            //        sheet.AutoSizeColumn(i);
            //        sheetSample.AutoSizeColumn(i);
            //    }

            //    workbook.Write(exportData);
            //    string saveAsFileName = "TemplateBuktiPotong.xlsx";
            //    byte[] bytes = exportData.ToArray();
            //    return File(bytes, "application/vnd.ms-excel", saveAsFileName); 
        }

        public JsonResult UploadListBuktiPotong(HttpPostedFileBase uploadFileWorklist)
        {
            bool result = false;
            IList<string> errMesgs = new List<string>();

            try
            {
                IList<BuktiPotong> listData = this.getDataFromExcel(uploadFileWorklist, errMesgs);

                if (listData.Count() > 0 && errMesgs.Count() == 0)
                {
                    result = Service<BuktiPotongService>().SaveUploadListBuktiPotong(listData, SessionManager.Current, errMesgs);
                }

                foreach (var item in errMesgs)
                {
                    CustomLog.WriteAppLog("Err_Notif_BuktiPotong", item, "Bukti Potong");
                }
            }
            catch (Exception ex)
            {
                errMesgs.Add(ex.Message);
            }

            return Json(new { IsSuccess = result, MessageError = errMesgs }, JsonRequestBehavior.AllowGet);
        }
        private IList<BuktiPotong> getDataFromExcel(HttpPostedFileBase file, IList<string> errMesgs)
        {
            XSSFWorkbook xssfwb = null;
            using (Stream file2 = file.InputStream)
            {
                xssfwb = new XSSFWorkbook(file2);
            }

            if (xssfwb == null)
            {
                throw new ArgumentNullException("Cannot create Workbook object from excel file " + file.FileName);
            }

            IRow row = null;
            ICell cell = null;
            BuktiPotong data = null;
            IList<BuktiPotong> listData = new List<BuktiPotong>();

            int indexRow = DATA_ROW_INDEX_START;
            bool isAllCellEmpty = true;
            bool isBreak = false;

            ISheet sheet = xssfwb.GetSheetAt(0);

            if (sheet.LastRowNum < 1)
            {
                errMesgs.Add(string.Format("File {0} cannot be empty", file.FileName));
                return listData;
            }

            for (indexRow = DATA_ROW_INDEX_START; indexRow <= sheet.LastRowNum; indexRow++)
            {
                decimal tarifNpwp = 0;

                data = new BuktiPotong();
                isAllCellEmpty = true;
                isBreak = false;
                row = sheet.GetRow(indexRow);

                if (row != null) //null is when the row only contains empty cells 
                {
                    bool mandatory = true;

                    try
                    {
                        cell = row.GetCell(COL_IDX_FLAG_POTONG_PAJAK);
                        if (cell != null)
                        {
                            if (cell.CellType == CellType.Blank)
                            {
                                errMesgs.Add(string.Format("Witholding Tax Object row {0} is required", indexRow + 1));
                                return listData;
                            }
                            else if (cell.CellType == CellType.String)
                            {
                                if (cell.StringCellValue == "N" || cell.StringCellValue == "Y")
                                {
                                    if (cell.StringCellValue == "N")
                                    {
                                        DateTime dateDefault = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", null);

                                        data.tanggal_bukti_potong = dateDefault;
                                        data.invoice_date = dateDefault;
                                        data.tgl_payment = dateDefault;

                                        mandatory = false;
                                    }
                                    data.flag_potong_pajak = cell.StringCellValue;

                                    isAllCellEmpty = false;
                                }
                                else
                                {
                                    errMesgs.Add(string.Format("Valid input field Witholding Tax Object row {0} is Y or N", indexRow + 1));
                                    return listData;
                                }
                            }
                            else if (mandatory)
                            {
                                errMesgs.Add(string.Format("Witholding Tax Object row {0} is incorrect", indexRow + 1));
                                return listData;
                            }
                        }
                        else if (mandatory)
                        {
                            errMesgs.Add(string.Format("Witholding Tax Object row {0} is required", indexRow + 1));
                            return listData;
                        }
                    }
                    catch (Exception ex)
                    {
                        errMesgs.Add(string.Format("Unable to get value of Witholding Tax Object at row {0}, Error Mesg : {1}",
                            indexRow + 1, ex.Message));
                        return listData;
                    }

                    //Business Unit
                    try
                    {
                        cell = row.GetCell(COL_IDX_BUSINESS_UNIT);
                        if (cell != null)
                        {
                            if (cell.CellType == CellType.Blank)
                            {
                                errMesgs.Add(string.Format("Business Unit row {0} is required", indexRow + 1));
                                return listData;
                            }
                            else if (cell.CellType == CellType.String)
                            {
                                data.business_unit = cell.StringCellValue;

                                isAllCellEmpty = false;
                            }
                            else if (cell.CellType == CellType.Numeric)
                            {
                                data.business_unit = cell.DateCellValue.ToString();

                                isAllCellEmpty = false;
                            }
                            else if (mandatory)
                            {
                                errMesgs.Add(string.Format("Business Unit format is incorrect"));
                                return listData;
                            }

                            string[] bisinisUnit = { "Lexus-Workshop", "Others" };

                            if (!bisinisUnit.Contains(data.business_unit))
                            {
                                errMesgs.Add(string.Format("Business Unit row {0} invalid value", indexRow + 1));
                                return listData;
                            }
                        }
                        else if (mandatory)
                        {
                            errMesgs.Add(string.Format("Business Unit row {0} is required", indexRow + 1));
                            return listData;
                        }
                    }
                    catch (Exception ex)
                    {
                        errMesgs.Add(string.Format("Unable to get value of Business Unit at row {0}, Error Mesg : {1}",
                            indexRow + 1, ex.Message));
                        return listData;
                    }

                    //Npwp
                    try
                    {
                        cell = row.GetCell(COL_IDX_NO_NPWP);
                        if (cell != null)
                        {
                            if (cell.CellType == CellType.Blank)
                            {
                                errMesgs.Add(string.Format("No NPWP row {0} is required", indexRow + 1));
                                return listData;
                            }
                            else if (cell.CellType == CellType.String || cell.CellType == CellType.Numeric)
                            {
                                string checkNPWP = cell.CellType == CellType.String ? cell.StringCellValue : cell.DateCellValue.ToString();
                                //checkNPWP = checkNPWP.Replace(".", "").Replace("-", ""); //strip-off symbols
                                double x = 0;

                                if (!double.TryParse(checkNPWP, out x))
                                {
                                    errMesgs.Add(string.Format("NPWP Format must be numeric characters"));
                                    return listData;
                                }

                                if (checkNPWP.Length != 15)
                                {
                                    errMesgs.Add(string.Format("NPWP Format must be 15 characters (plain) without symbol"));
                                    return listData;
                                }

                                string noNPWP = FormatNPWP(checkNPWP);

                                data.no_npwp = noNPWP;

                                isAllCellEmpty = false;
                            }
                            else if (mandatory)
                            {
                                errMesgs.Add(string.Format("No NPWP format is incorrect"));
                                return listData;
                            }
                        }
                        else if (mandatory)
                        {
                            errMesgs.Add(string.Format("No NPWP row {0} is required", indexRow + 1));
                            return listData;
                        }
                    }
                    catch (Exception ex)
                    {
                        errMesgs.Add(string.Format("Unable to get value of No NPWP at row {0}, Error Mesg : {1}",
                            indexRow + 1, ex.Message));
                        return listData;
                    }

                    //InvoiceNumber
                    try
                    {
                        cell = row.GetCell(COL_IDX_INVOICE_NUMBER);
                        if (cell != null)
                        {
                            if (cell.CellType == CellType.Blank)
                            {
                                errMesgs.Add(string.Format("Invoice Number row {0} is required", indexRow + 1));
                                return listData;
                            }
                            else if (cell.CellType == CellType.String || cell.CellType == CellType.Numeric)
                            {
                                data.invoice_number = cell.CellType == CellType.String ? cell.StringCellValue : cell.NumericCellValue.ToString();
                                isAllCellEmpty = false;

                                var buktiPotongs = Service<BuktiPotongService>().GetBuktiPotong(data.invoice_number, SessionManager.RoleStr);

                                if (buktiPotongs.Exists(b => b.flag_potong_pajak == "Y"))
                                {
                                    errMesgs.Add(string.Format("Invoice Number {0} row {1} already submitted", data.invoice_number, indexRow + 1));
                                    return listData;
                                }
                                else if (buktiPotongs.Count() == 0)
                                {
                                    errMesgs.Add(string.Format("Invoice Number {0} row {1} not exist", data.invoice_number, indexRow + 1));
                                    return listData;
                                }
                            }
                            else if (mandatory)
                            {
                                errMesgs.Add(string.Format("Invoice Number row {0} is Incorrect Format", indexRow + 1));
                                return listData;
                            }
                        }
                        else if (mandatory)
                        {
                            errMesgs.Add(string.Format("Invoice Number row {0} is required", indexRow + 1));
                            return listData;
                        }
                    }
                    catch (Exception ex)
                    {
                        errMesgs.Add(string.Format("Unable to get value of Invoice Number at row {0}, Error Mesg : {1}",
                            indexRow + 1, ex.Message));
                        return listData;
                    }

                    //InvoiceDate
                    try
                    {
                        cell = row.GetCell(COL_IDX_INVOICE_DATE);
                        if (cell != null)
                        {
                            if (cell.CellType == CellType.Blank)
                            {
                                errMesgs.Add(string.Format("Invoice Date row {0} is required", indexRow + 1));
                                return listData;
                            }
                            else if (cell.CellType == CellType.String)
                            {
                                string dt = cell.StringCellValue;
                                try
                                {
                                    data.invoice_date = DateTime.ParseExact(dt, "dd/MM/yyyy", null);
                                }
                                catch (FormatException fe)
                                {
                                    throw new Exception(String.Format("Invoice Date at Row {0} must dd/MM/yyyy", indexRow + 1));
                                }

                                isAllCellEmpty = false;
                            }
                            else if (cell.CellType == CellType.Numeric)
                            {
                                data.invoice_date = cell.DateCellValue;

                                isAllCellEmpty = false;
                            }
                            else if (mandatory)
                            {
                                errMesgs.Add(string.Format("Invoice Date format is incorrect"));
                                return listData;
                            }
                        }
                        else if (mandatory)
                        {
                            errMesgs.Add(string.Format("Invoice Date row {0} is required", indexRow + 1));
                            return listData;
                        }
                    }
                    catch (Exception ex)
                    {
                        errMesgs.Add(string.Format("Invoice Date at row {0}, Error Mesg : {1}",
                            indexRow + 1, ex.Message));
                        return listData;
                    }

                    //MateraiAmount
                    try
                    {
                        cell = row.GetCell(COL_IDX_MATERAI_AMOUNT);
                        if (cell != null)
                        {
                            if (cell.CellType == CellType.Blank && mandatory)
                            {
                                errMesgs.Add(string.Format("Materai Amount row {0} is required", indexRow + 1));
                                return listData;
                            }
                            else if (cell.CellType == CellType.String && mandatory)
                            {
                                data.materai_amount = decimal.Parse(cell.StringCellValue);

                                isAllCellEmpty = false;
                            }
                            else if (cell.CellType == CellType.Numeric && mandatory)
                            {
                                data.materai_amount = (decimal)cell.NumericCellValue;

                                isAllCellEmpty = false;
                            }
                            else if ((cell.CellType == CellType.String || cell.CellType == CellType.Numeric) && !mandatory)
                            {
                                errMesgs.Add(string.Format("Materai Amount row {0} if Tax Object is N value must be blank", indexRow + 1));
                                return listData;
                            }
                            else if (mandatory)
                            {
                                errMesgs.Add(string.Format("Materai Amount format is incorrect"));
                                return listData;
                            }
                        }
                        else if (mandatory)
                        {
                            errMesgs.Add(string.Format("Materai Amount row {0} is required", indexRow + 1));
                            return listData;
                        }
                    }
                    catch (Exception ex)
                    {
                        errMesgs.Add(string.Format("Unable to get value of Materai Amount at row {0}, Error Mesg : {1}",
                            indexRow + 1, ex.Message));
                        return listData;
                    }

                    //TanggalPayment
                    try
                    {
                        cell = row.GetCell(COL_IDX_TGL_PAYMENT);
                        if (cell != null)
                        {
                            if (cell.CellType == CellType.Blank && mandatory)
                            {
                                errMesgs.Add(string.Format("Payment Date row {0} is required", indexRow + 1));
                                return listData;
                            }
                            else if (cell.CellType == CellType.String && mandatory)
                            {
                                string dt = cell.StringCellValue;
                                try
                                {
                                    data.tgl_payment = DateTime.ParseExact(dt, "dd/MM/yyyy", null);
                                }
                                catch (FormatException fe)
                                {
                                    throw new Exception(String.Format("Payment Date at Row {0} must dd/MM/yyyy", indexRow + 1));
                                }

                                isAllCellEmpty = false;
                            }
                            else if (cell.CellType == CellType.Numeric && mandatory)
                            {
                                data.tgl_payment = cell.DateCellValue;

                                isAllCellEmpty = false;
                            }
                            else if ((cell.CellType == CellType.String || cell.CellType == CellType.Numeric) && !mandatory)
                            {
                                errMesgs.Add(string.Format("Payment Date row {0} if Tax Object is N value must be blank", indexRow + 1));
                                return listData;
                            }
                            else if (mandatory)
                            {
                                errMesgs.Add(string.Format("Payment Date format is incorrect"));
                                return listData;
                            }
                        }
                        else if (mandatory)
                        {
                            errMesgs.Add(string.Format("Payment Date row {0} is required", indexRow + 1));
                            return listData;
                        }
                    }
                    catch (Exception ex)
                    {
                        errMesgs.Add(string.Format("Payment Date at row {0}, Error Mesg : {1}",
                            indexRow + 1, ex.Message));
                        return listData;
                    }

                    //NomorBuktiPotong
                    try
                    {
                        cell = row.GetCell(COL_IDX_NOMOR_BUKTI_POTONG);
                        if (cell != null)
                        {
                            if (cell.CellType == CellType.Blank && mandatory)
                            {
                                errMesgs.Add(string.Format("Withholding Tax No  row {0} is required", indexRow + 1));
                                return listData;
                            }
                            else if ((cell.CellType == CellType.String || cell.CellType == CellType.Numeric) && mandatory)
                            {
                                data.nomor_bukti_potong = cell.CellType == CellType.String ? cell.StringCellValue : cell.NumericCellValue.ToString();
                                isAllCellEmpty = false;

                                //int countNomorBuktiPotong = Service<BuktiPotongService>().CheckNomorBuktiPotongWHT(data.nomor_bukti_potong);

                                //if (countNomorBuktiPotong == 0)
                                //{ 
                                //    errMesgs.Add(string.Format("Withholding Tax No row {0} doesn't exist or Waiting for Approval Sukses", indexRow + 1));
                                //    return listData;
                                //}
                            }
                            else if (cell.CellType == CellType.String && !mandatory)
                            {
                                errMesgs.Add(string.Format("Withholding Tax No row {0} if Tax Object is N value must be blank", indexRow + 1));
                                return listData;
                            }
                            else if (mandatory)
                            {
                                errMesgs.Add(string.Format("Withholding Tax No row {0} is Incorrect Format", indexRow + 1));
                                return listData;
                            }
                        }
                        else if (mandatory)
                        {
                            errMesgs.Add(string.Format("Withholding Tax No row {0} is required", indexRow + 1));
                            return listData;
                        }
                    }
                    catch (Exception ex)
                    {
                        errMesgs.Add(string.Format("Unable to get value of Withholding Tax No at row {0}, Error Mesg : {1}",
                            indexRow + 1, ex.Message));
                        return listData;
                    }

                    //TanggalBuktiPotong
                    try
                    {
                        cell = row.GetCell(COL_IDX_TANGGAL_BUKTI_POTONG);
                        if (cell != null)
                        {
                            if (cell.CellType == CellType.Blank && mandatory)
                            {
                                errMesgs.Add(string.Format("Witholding Tax Date row {0} is required", indexRow + 1));
                                return listData;
                            }
                            else if (cell.CellType == CellType.String && mandatory)
                            {
                                string dt = cell.StringCellValue;
                                try
                                {
                                    data.tanggal_bukti_potong = DateTime.ParseExact(dt, "dd/MM/yyyy", null);
                                }
                                catch (FormatException fe)
                                {
                                    throw new Exception(String.Format("Withholding Tax Date at Row {0} must dd/MM/yyyy", indexRow + 1));
                                }

                                isAllCellEmpty = false;
                            }
                            else if (cell.CellType == CellType.Numeric && mandatory)
                            {
                                data.tanggal_bukti_potong = cell.DateCellValue;

                                isAllCellEmpty = false;
                            }
                            else if ((cell.CellType == CellType.String || cell.CellType == CellType.Numeric) && !mandatory)
                            {
                                errMesgs.Add(string.Format("Witholding Tax Date row {0} if Tax Object is N value must be blank", indexRow + 1));
                                return listData;
                            }
                            else if (mandatory)
                            {
                                errMesgs.Add(string.Format("Witholding Tax Date format is incorrect"));
                                return listData;
                            }
                        }
                        else if (mandatory)
                        {
                            errMesgs.Add(string.Format("Witholding Tax Date row {0} is required", indexRow + 1));
                            return listData;
                        }
                    }
                    catch (Exception ex)
                    {
                        errMesgs.Add(string.Format("Witholding Tax Date at row {0}, Error Mesg : {1}",
                            indexRow + 1, ex.Message));
                        return listData;
                        //break;
                    }

                    //JumlahDPP
                    try
                    {
                        cell = row.GetCell(COL_IDX_JUMLAH_DPP);
                        if (cell != null)
                        {
                            if (cell.CellType == CellType.Blank && mandatory)
                            {
                                errMesgs.Add(string.Format("WHT Based Amount row {0} is required", indexRow + 1));
                                return listData;
                            }
                            else if (cell.CellType == CellType.String && mandatory)
                            {
                                data.jumlah_dpp = decimal.Parse(cell.StringCellValue);

                                isAllCellEmpty = false;
                            }
                            else if (cell.CellType == CellType.Numeric && mandatory)
                            {
                                data.jumlah_dpp = (decimal)cell.NumericCellValue;

                                isAllCellEmpty = false;
                            }
                            else if ((cell.CellType == CellType.String || cell.CellType == CellType.Numeric) && !mandatory)
                            {
                                errMesgs.Add(string.Format("WHT Based Amount row {0} if Tax Object is N value must be blank", indexRow + 1));
                                return listData;
                            }
                            else if (mandatory)
                            {
                                errMesgs.Add(string.Format("WHT Based Amount format is incorrect"));
                                return listData;
                            }
                        }
                        else if (mandatory)
                        {
                            errMesgs.Add(string.Format("WHT Based Amount row {0} is required", indexRow + 1));
                            return listData;
                        }
                    }
                    catch (Exception ex)
                    {
                        errMesgs.Add(string.Format("Unable to get value of WHT Based Amount at row {0}, Error Mesg : {1}",
                            indexRow + 1, ex.Message));
                        return listData;
                    }

                    //JenisPajak = Tax Transaction Code
                    try
                    {
                        cell = row.GetCell(COL_IDX_JENIS_PAJAK);
                        if (cell != null)
                        {
                            if (cell.CellType == CellType.Blank && mandatory)
                            {
                                errMesgs.Add(string.Format("Tax Transaction Code row {0} is required", indexRow + 1));
                                return listData;
                            }
                            else if (cell.CellType == CellType.String && mandatory)
                            {
                                data.jenis_pajak = cell.StringCellValue;
                                isAllCellEmpty = false;

                                decimal cekTransactionTypeCode = Service<BuktiPotongService>().GetTarifNPWP(data.jenis_pajak);

                                if (cekTransactionTypeCode == 0)
                                {
                                    errMesgs.Add(string.Format("Tax Transaction Code row {0} not found", indexRow + 1));
                                    return listData;
                                }

                                tarifNpwp = cekTransactionTypeCode;
                            }
                            else if (cell.CellType == CellType.Numeric && mandatory)
                            {
                                data.jenis_pajak = cell.NumericCellValue.ToString();

                                isAllCellEmpty = false;

                                decimal cekTransactionTypeCode = Service<BuktiPotongService>().GetTarifNPWP(data.jenis_pajak);

                                if (cekTransactionTypeCode == 0)
                                {
                                    errMesgs.Add(string.Format("Tax Transaction Code row {0} not found", indexRow + 1));
                                    return listData;
                                }

                                tarifNpwp = cekTransactionTypeCode;
                            }
                            else if ((cell.CellType == CellType.String || cell.CellType == CellType.Numeric) && !mandatory)
                            {
                                errMesgs.Add(string.Format("Tax Transaction Code row {0} if Tax Object is N value must be blank", indexRow + 1));
                                return listData;
                            }
                            else if (mandatory)
                            {
                                errMesgs.Add(string.Format("Tax Transaction Code row {0} is Incorrect Format", indexRow + 1));
                                return listData;
                            }
                        }
                        else if (mandatory)
                        {
                            errMesgs.Add(string.Format("Tax Transaction Code row {0} is required", indexRow + 1));
                            return listData;
                        }
                    }
                    catch (Exception ex)
                    {
                        errMesgs.Add(string.Format("Unable to get value of Tax Transaction Code at row {0}, Error Mesg : {1}",
                            indexRow + 1, ex.Message));
                        return listData;
                    }

                    //Tarif
                    try
                    {
                        cell = row.GetCell(COL_IDX_TARIF);
                        if (cell != null)
                        {
                            if (cell.CellType == CellType.Blank && mandatory)
                            {
                                errMesgs.Add(string.Format("WHT Tarif row {0} is required", indexRow + 1));
                                return listData;
                            }
                            else if (cell.CellType == CellType.String && mandatory)
                            {
                                data.tarif = decimal.Parse(cell.StringCellValue);

                                isAllCellEmpty = false;

                                if (data.tarif != tarifNpwp)
                                {
                                    errMesgs.Add(string.Format("WHT Tarif row {0} is incorrect", indexRow + 1));
                                    return listData;
                                }
                            }
                            else if (cell.CellType == CellType.Numeric && mandatory)
                            {
                                data.tarif = (decimal)cell.NumericCellValue;

                                isAllCellEmpty = false;

                                if (data.tarif != tarifNpwp)
                                {
                                    errMesgs.Add(string.Format("WHT Tarif row {0} is incorrect", indexRow + 1));
                                    return listData;
                                }
                            }
                            else if ((cell.CellType == CellType.String || cell.CellType == CellType.Numeric) && !mandatory)
                            {
                                errMesgs.Add(string.Format("WHT Tarif row {0} if Tax Object is N value must be blank", indexRow + 1));
                                return listData;
                            }
                            else if (mandatory)
                            {
                                errMesgs.Add(string.Format("WHT Tarif format is incorrect"));
                                return listData;
                            }
                        }
                        else if (mandatory)
                        {
                            errMesgs.Add(string.Format("WHT Tarif row {0} is required", indexRow + 1));
                            return listData;
                        }
                    }
                    catch (Exception ex)
                    {
                        errMesgs.Add(string.Format("Unable to get value of WHT Tarif at row {0}, Error Mesg : {1}",
                            indexRow + 1, ex.Message));
                        return listData;
                    }

                    //JumlahPPh
                    try
                    {
                        cell = row.GetCell(COL_IDX_JUMLAH_PPH);
                        if (cell != null)
                        {
                            if (cell.CellType == CellType.Blank && mandatory)
                            {
                                errMesgs.Add(string.Format("WHT Amount row {0} is required", indexRow + 1));
                                return listData;
                            }
                            else if (cell.CellType == CellType.String && mandatory)
                            {
                                data.jumlah_pph = decimal.Parse(cell.StringCellValue);

                                isAllCellEmpty = false;
                            }
                            else if (cell.CellType == CellType.Numeric && mandatory)
                            {
                                data.jumlah_pph = (decimal)cell.NumericCellValue;

                                isAllCellEmpty = false;
                            }
                            else if ((cell.CellType == CellType.String || cell.CellType == CellType.Numeric) && !mandatory)
                            {
                                errMesgs.Add(string.Format("WHT Amount row {0} if Tax Object is N value must be blank", indexRow + 1));
                                return listData;
                            }
                            else if (mandatory)
                            {
                                errMesgs.Add(string.Format("WHT Amount format is incorrect"));
                                return listData;
                            }
                        }
                        else if (mandatory)
                        {
                            errMesgs.Add(string.Format("WHT Amount row {0} is required", indexRow + 1));
                            return listData;
                        }
                    }
                    catch (Exception ex)
                    {
                        errMesgs.Add(string.Format("Unable to get value of WHT Amount at row {0}, Error Mesg : {1}",
                            indexRow + 1, ex.Message));
                        return listData;
                    }

                    //Description
                    try
                    {
                        cell = row.GetCell(COL_IDX_DESCRIPTION);
                        if (cell != null)
                        {
                            if (cell.CellType == CellType.Blank && mandatory)
                            {
                                errMesgs.Add(string.Format("Description row {0} is required", indexRow + 1));
                                return listData;
                            }
                            else if (cell.CellType == CellType.String && mandatory)
                            {
                                data.description = cell.StringCellValue;

                                isAllCellEmpty = false;
                            }
                            else if (cell.CellType == CellType.String && !mandatory)
                            {
                                errMesgs.Add(string.Format("Description row {0} if Tax Object is N value must be blank", indexRow + 1));
                                return listData;
                            }
                            else if (mandatory)
                            {
                                errMesgs.Add(string.Format("Description format is incorrect"));
                                return listData;
                            }
                        }
                        else if (mandatory)
                        {
                            errMesgs.Add(string.Format("Description row {0} is required", indexRow + 1));
                            return listData;
                        }
                    }
                    catch (Exception ex)
                    {
                        errMesgs.Add(string.Format("Unable to get value of Description at row {0}, Error Mesg : {1}",
                            indexRow + 1, ex.Message));
                        return listData;
                    }

                    listData.Add(data);
                }
            }

            return listData;
        }

        public static string FormatNPWP(string value)
        {
            //11.222.333.4-555.666
            string a = value.Substring(0, 2);
            string b = value.Substring(2, 3);
            string c = value.Substring(5, 3);
            string d = value.Substring(8, 1);
            string e = value.Substring(9, 3);
            string f = value.Substring(12, 3);

            return a + "." + b + "." + c + "." + d + "-" + e + "." + f;
        }

        public JsonResult Upload(string invoices, string documentType)
        {
            if (documentType == "")
                documentType = "22";

            string msgerror = "";

            var files = Request.Files;
            var arrayObjects = new List<BuktiPotongAttachment>();
            var uploadService = Service<BuktiPotongService>();

            if (files != null && files.Count > 0)
            {
                string[] invoiceNumberArr = invoices.Split(',');

                var checkTaxArticle = Service<BuktiPotongService>().CheckTaxArticle(invoices); 

                if (checkTaxArticle.Exists(b => b.flag_potong_pajak == null || b.flag_potong_pajak == "N"))
                { 
                    msgerror = "Withholding Tax Object N or Blank cannot upload pdf";
                    CustomLog.WriteAppLog("Err_Notif_BuktiPotong", "Upload : Withholding Tax Object N or Blank cannot upload pdf", "Bukti Potong");
                } 
                else if (!checkTaxArticle.Exists(b => b.tax_article == documentType))
                {
                    msgerror = "Tax Article is doesn't match";
                    CustomLog.WriteAppLog("Err_Notif_BuktiPotong", "Upload : Tax Article is doesn't match", "Bukti Potong");
                } 
                else
                {
                    foreach (string invoiceNo in invoiceNumberArr)
                    {
                        #region
                        //cari transactioncode dari invoiceno
                        //cari ke jenis pajak untuk transaction code tersebut tax article nya berapa
                        //compare antara taxarticle dan documenttype
                        //kalo beda gagal

                        //List<BuktiPotongAttachment> listDocument = Service<BuktiPotongService>().GetDocumentFileByReference(invoiceNo).ToList();

                        //for (var i = 0; i < files.Count; i++)
                        //{
                        //    var file = files[i];

                        //    if (!listDocument.Exists(e => e.filename == file.FileName))
                        //    {
                        //        var doc = uploadService.Upload(new HttpPostedFileBaseAdapter(file), invoiceNo, out msgerror, documentType);

                        //        arrayObjects.Add(doc);
                        //    }
                        //    else
                        //    {
                        //        msgerror = string.Format("File {0} already exist in invoice {1} ", file.FileName, invoiceNo);
                        //    } 
                        //}
                        #endregion

                        #region
                        //19012021 validasi dibuka lagi
                        //string taxTransactionCode = Service<BuktiPotongService>().GetTransactionCode(invoiceNo);

                        //string taxArticle = Service<BuktiPotongService>().GetTaxArticle(taxTransactionCode);

                        //if (documentType == "BuktiPotong" || documentType == taxArticle)
                        //{
                        //    List<BuktiPotongAttachment> listDocument = Service<BuktiPotongService>().GetDocumentFileByReference(invoiceNo).ToList();

                        //    for (var i = 0; i < files.Count; i++)
                        //    {
                        //        var file = files[i];

                        //        if (!listDocument.Exists(e => e.filename == file.FileName))
                        //        {
                        //            var doc = uploadService.Upload(new HttpPostedFileBaseAdapter(file), invoiceNo, out msgerror, documentType);

                        //            arrayObjects.Add(doc);
                        //        }
                        //        else
                        //        {
                        //            msgerror = string.Format("File {0} already exist in invoice {1} ", file.FileName, invoiceNo);
                        //        }

                        //    }
                        //}
                        //else
                        //{
                        //    msgerror = "Tax Article is doesn't match";
                        //    CustomLog.WriteAppLog("Err_Notif_BuktiPotong", "Upload : Tax Article is doesn't match", "Bukti Potong");
                        //}
                        //end
                        #endregion

                        #region 25022021
                        List<BuktiPotongAttachment> listDocument = Service<BuktiPotongService>().GetDocumentFileByReference(invoiceNo).ToList();

                        for (var i = 0; i < files.Count; i++)
                        {
                            var file = files[i];

                            if (!listDocument.Exists(e => e.filename == file.FileName))
                            {
                                var doc = uploadService.Upload(new HttpPostedFileBaseAdapter(file), invoiceNo, out msgerror, documentType);

                                arrayObjects.Add(doc);
                            }
                            else
                            {
                                msgerror = string.Format("File {0} already exist in invoice {1} ", file.FileName, invoiceNo);
                            }

                        }
                        #endregion

                    }
                }
            }
            return Json(new { Result = arrayObjects, MessageError = msgerror }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetListDocuments(string invoice)
        {
            ViewBag.InvoiceNo = invoice;
            var result = Service<BuktiPotongService>().GetDocumentFileByReference(ViewBag.InvoiceNo);

            var ds = new DataSourceResult
            {
                Data = result
            };
            return Json(ds, JsonRequestBehavior.AllowGet);
        }

        public void Temp()
        {
            //var username = ApplicationCacheManager.GetConfig<string>("FileUploadUserName");
            //var password = ApplicationCacheManager.GetConfig<string>("FileUploadPassword");
            //var domain = ApplicationCacheManager.GetConfig<string>("FileUploadDomain");
            var username = ApplicationCacheManager.GetConfig<string>("FileBuktiPotongUserName");
            var password = ApplicationCacheManager.GetConfig<string>("FileBuktiPotongPassword");
            var domain = ApplicationCacheManager.GetConfig<string>("FileBuktiPotongDomain");

            var userdomain = domain + "\\" + username;
            var pers = new Impersonation();
            pers.Impersonate(userdomain, password);
            var credential = new NetworkCredential(userdomain, password);
            //var uploadFolder = "E:\\NotaRetur";

            //var uploadFolder = ApplicationCacheManager.GetConfig<string>("FileUploadTemp");
            var uploadFolder = ApplicationCacheManager.GetConfig<string>("FileBuktiPotongFolderTemp");
            using (new NetworkConnection(uploadFolder, credential))
            {
                //var filepath = GetQueryString<string>("filepath").ToString();
                var filename = GetQueryString<string>("filename").ToString();

                var url = Path.Combine(uploadFolder, filename);

                Response.Clear();
                Response.AddHeader("Access-Control-Allow-Origin", "*");
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("content-disposition", "attachment;filename=" + Path.GetFileName(filename));
                Response.WriteFile(url);
                Response.Flush();
                Response.End();
            }
        }

        public void Document()
        {
            var username = ApplicationCacheManager.GetConfig<string>("FileBuktiPotongUserName");
            var password = ApplicationCacheManager.GetConfig<string>("FileBuktiPotongPassword");
            var domain = ApplicationCacheManager.GetConfig<string>("FileBuktiPotongDomain");
            var userdomain = domain + "\\" + username;
            var pers = new Impersonation();
            pers.Impersonate(userdomain, password);
            var credential = new NetworkCredential(userdomain, password);

            var uploadFolder = ApplicationCacheManager.GetConfig<string>("FileBuktiPotong");
            using (new NetworkConnection(uploadFolder, credential))
            {
                var id = GetQueryString<string>("id");
                var doc = Service<BuktiPotongService>().GetDocumentByID(id.ToString()).FirstOrDefault();

                var url = Path.Combine(uploadFolder, doc.server_filename);

                Response.Clear();
                Response.AddHeader("Access-Control-Allow-Origin", "*");
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("content-disposition", "attachment;filename=" + Path.GetFileName(doc.filename));
                Response.WriteFile(url);
                Response.Flush();
                Response.End();
            }
        }

        public JsonResult SaveUploadInitial(string invoiceNumber, string documentType, IEnumerable<BuktiPotongAttachment> Docs)
        {
            if (documentType == "")
                documentType = "22";

            IList<string> errMesgs = new List<string>();

            string msgerror = "";
            bool result = false, emailResult = false;

            if (!isContain1PdfAnd1Xls(Docs))
            {
                msgerror = "Upload must contain pdf file and excel";
                errMesgs.Add("Upload must contain pdf file and excel");


                CustomLog.WriteAppLog("Err_Notif_BuktiPotong", "Save Upload Initial : Upload must contain pdf file or excel", "Bukti Potong");
            }
            else
            {
                try
                {
                    result = Service<BuktiPotongService>().SaveUpload(invoiceNumber, documentType, Docs, out msgerror, errMesgs);

                    //send email
                    if (result)
                    {
                        emailResult = Service<BuktiPotongService>().SendEmailDealerHasBeenUploadNotification(invoiceNumber);
                    }
                }
                catch (Exception ex)
                {
                    result = false;
                    emailResult = false;

                    //msgerror = ex.Message;
                    errMesgs.Add(ex.Message);
                }
            }

            return Json(new { IsSuccess = result, IsSuccessSendEmail = emailResult, MessageError = errMesgs.Distinct() }, JsonRequestBehavior.AllowGet);
        }

        private bool isContain1PdfAnd1Xls(IEnumerable<BuktiPotongAttachment> Docs)
        {
            bool pdfExist = false, excelExist = false;
            excelExist = Docs.Any(
                    d => (Path.GetExtension(d.filename) == ".xls" || Path.GetExtension(d.filename) == ".xlsx")
                    );
            pdfExist = Docs.Any(
                d => Path.GetExtension(d.filename) == ".pdf"
                );

            if (pdfExist || excelExist)
                return true;
            else
                return false;
        }

        public JsonResult Submit(string invoices)
        {
            string msgerror = "";
            string[] invoiceNumberArr = invoices.Split(',');
            bool result = false;
            bool emailResult = false;

            var buktiPotongs = Service<BuktiPotongService>().GetBuktiPotong(invoices, SessionManager.RoleStr);
            if (buktiPotongs.Exists(b => b.bukti_potong_uploaded == true || b.is_active == true))
            {
                msgerror = "One or More Invoices already submitted or uploaded";

                CustomLog.WriteAppLog("Err_Notif_BuktiPotong", "Submit : One or More Invoices already submitted or uploaded", "Bukti Potong");
            }
            else
            {
                try
                {
                    result = Service<BuktiPotongService>().SubmitWHTSubmission(invoiceNumberArr, SessionManager.Current);

                    if (result)
                    {
                        emailResult = Service<BuktiPotongService>().SendEmailNotification(invoices);
                    }
                }
                catch (Exception ex)
                {
                    result = false;
                    emailResult = false;
                    msgerror = ex.Message;
                }
            }

            return Json(new { IsSuccess = result, IsSuccessSendEmail = emailResult, MessageError = msgerror }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Delete(string invoices)
        {
            string msgerror = "";
            string[] invoiceNumberArr = invoices.Split(',');
            bool result = false;

            var buktiPotongs = Service<BuktiPotongService>().GetBuktiPotong(invoices, SessionManager.RoleStr);
            result = Service<BuktiPotongService>().DeleteUnSubmittedWHTSubmission(invoiceNumberArr, buktiPotongs);

            //10122020 validasi di lepas
            //if (buktiPotongs.Exists(b => b.bukti_potong_uploaded == true || b.is_active == true))
            //{
            //    msgerror = "One or More Invoices have been uploaded withholding tax or Submited";

            //    CustomLog.WriteAppLog("Err_Notif_BuktiPotong", "Delete : One or More Invoices have been uploaded withholding tax or Submited", "Bukti Potong");
            //} 
            //else
            //{ 
            //    result = Service<BuktiPotongService>().DeleteUnSubmittedWHTSubmission(invoiceNumberArr, buktiPotongs);
            //}

            return Json(new { IsSuccess = result, MessageError = msgerror }, JsonRequestBehavior.AllowGet);
        }

        public FileResult DownloadTemplateWHT()
        {
            string fileName = "TemplateBuktiPotong.xlsx";
            string filesTMp = HttpContext.Request.MapPath("~/Content/template/" + fileName);

            byte[] fileBytes = System.IO.File.ReadAllBytes(filesTMp);

            return File(fileBytes, "application/vnd.ms-excel", fileName);

            //var strclmn = new string[] { "Invoice Number", "Materai Amount", "Tgl Payment"
            //    , "Flag Witholidng Tax Object", "Witholding Tax No", "Witholding Tax Date"
            //    , "NPWP", "Address", "Customer Name", "Tax Period Month", "Tax Period Year"
            //    , "WHT Based Amount", "WHT Tarif", "WHT Amount", "Invoice Date", "Tax Transaction Code",  "Description"};

            //using (var exportData = new MemoryStream())
            //{
            //    var workbook = new XSSFWorkbook();
            //    var sheet = workbook.CreateSheet();

            //    var sheetSample = workbook.CreateSheet("Sample");

            //    var red = workbook.CreateCellStyle();
            //    red.FillForegroundColor = IndexedColors.Red.Index;
            //    red.FillPattern = FillPattern.SolidForeground;

            //    var noColor = workbook.CreateCellStyle();
            //    noColor.FillForegroundColor = IndexedColors.White.Index;
            //    noColor.FillPattern = FillPattern.SolidForeground;

            //    var yellow = workbook.CreateCellStyle();
            //    yellow.FillForegroundColor = IndexedColors.Yellow.Index;
            //    yellow.FillPattern = FillPattern.SolidForeground;

            //    var mandatory = sheetSample.CreateRow(0);
            //    mandatory.CreateCell(0).CellStyle = red;
            //    mandatory.CreateCell(1).SetCellValue("Mandatory");

            //    var optional = sheetSample.CreateRow(1);
            //    optional.CreateCell(0).CellStyle = yellow;
            //    optional.CreateCell(1).SetCellValue("Optional");

            //    var headerRow = sheet.CreateRow(0);
            //    var headerRowSample = sheetSample.CreateRow(3);

            //    var x = sheetSample.CreateRow(4);
            //    var y = sheetSample.CreateRow(5);

            //    for (int i = 0; i < strclmn.Length; i++)
            //    {
            //        var headerCell = headerRow.CreateCell(i);
            //        headerCell.SetCellValue(strclmn[i]);

            //        var headerCellSample = headerRowSample.CreateCell(i);
            //        headerCellSample.SetCellValue(strclmn[i]);

            //        //header 
            //        x.CreateCell(i).CellStyle = red;
            //        y.CreateCell(i).CellStyle = red;
            //        if (i == 3)
            //        {
            //            x.CreateCell(i).SetCellValue("Y");
            //            y.CreateCell(i).SetCellValue("N");
            //        }
            //        else if (i > 3)
            //        {
            //            y.CreateCell(i).CellStyle = yellow;
            //        }
            //        //endheader

            //        var headerCellStyle = workbook.CreateCellStyle();
            //        headerCellStyle.FillForegroundColor = IndexedColors.Aqua.Index;
            //        headerCellStyle.FillPattern = FillPattern.SolidForeground;

            //        // Add Style to Cell
            //        headerCell.CellStyle = headerCellStyle;
            //        headerCellSample.CellStyle = headerCellStyle;

            //        sheet.AutoSizeColumn(i);
            //        sheetSample.AutoSizeColumn(i);
            //    }

            //    workbook.Write(exportData);
            //    string saveAsFileName = "Template Upload WHT.xlsx";
            //    byte[] bytes = exportData.ToArray();
            //    return File(bytes, "application/vnd.ms-excel", saveAsFileName);

            //var strclmn = new string[] { "No", "Jenis Pajak", "Nomor Bukti Potong", "Tanggal Bukti Potong", "Masa Pajak"
            //    , "Tahun Pajak", "DPP", "Tarif Pajak", "PPh", "Nomor Invoice Dasar Pemotongan", "Tanggal Invoice"};
            //using (var exportData = new MemoryStream())
            //{
            //    var workbook = new XSSFWorkbook();
            //    var sheet = workbook.CreateSheet();
            //    var headerRow = sheet.CreateRow(0);

            //    for (int i = 0; i < strclmn.Length; i++)
            //    {
            //        var headerCell = headerRow.CreateCell(i);
            //        headerCell.SetCellValue(strclmn[i]);

            //        var headerCellStyle = workbook.CreateCellStyle();
            //        headerCellStyle.FillForegroundColor = IndexedColors.Aqua.Index;
            //        headerCellStyle.FillPattern = FillPattern.SolidForeground;

            //        // Add Style to Cell
            //        headerCell.CellStyle = headerCellStyle;

            //        sheet.AutoSizeColumn(i);
            //    }

            //    workbook.Write(exportData);
            //    string saveAsFileName = "Template Upload WHT.xlsx";
            //    byte[] bytes = exportData.ToArray();
            //    return File(bytes, "application/vnd.ms-excel", saveAsFileName);

        }

        public ActionResult GetListInvoiceByUploadId(string uploadId)
        {
            ViewBag.Invoices = Service<BuktiPotongService>().GetDocumentFileByUploadId(uploadId).ToList();

            var url = "~/Areas/Core/Views/BuktiPotong/Form/_ListInvoiceGabungan.cshtml";
            return PartialView(url);
        }

        public JsonResult GetPDFFileExists([DataSourceRequest] DataSourceRequest request, string checks)
        {
            DataSourceResult result = Service<BuktiPotongService>().GetPDFFileExists(request, EnumHelper.GetDescription(InvoiceType.Invoice), SessionManager.RoleStr, checks);
            Boolean pdfExists = false;


            foreach (BuktiPotong data in result.Data)
            {
                if ((data.wht_pdf_22 != "" && data.wht_pdf_22 != null && System.IO.File.Exists(data.wht_pdf_22) == true && new FileInfo(data.wht_pdf_22).Length > 0)
                    || (data.wht_pdf_23 != "" && data.wht_pdf_23 != null && System.IO.File.Exists(data.wht_pdf_23) == true && new FileInfo(data.wht_pdf_23).Length > 0)
                    || (data.wht_pdf_42 != "" && data.wht_pdf_42 != null && System.IO.File.Exists(data.wht_pdf_42) == true && new FileInfo(data.wht_pdf_42).Length > 0))
                {
                    pdfExists = true;
                    break;
                }
            }

            if (pdfExists)
                return Json("Yes", JsonRequestBehavior.AllowGet);
            else
                return Json("No", JsonRequestBehavior.AllowGet);
        }

        public void DownloadBuktiPotongPdf([DataSourceRequest] DataSourceRequest request, string checks)
        {
            string[] cekcekall = checks.Split(',');

            DataSourceResult result;

            //if (checks == null || checks == "" || cekcekall[0] == "on")
            if (cekcekall[0] == "on")
            {
                var result2 = Service<BuktiPotongService>().GetViewDataSourceResult(request, EnumHelper.GetDescription(InvoiceType.Invoice), SessionManager.RoleStr);
                List<String> list = new List<String>();
                list = result2.Data.Cast<BuktiPotong>().Select(_wht => _wht.invoice_number).ToList();

                //list = result.Data
                String[] intList = list.ToArray();
                result = Service<BuktiPotongService>().getchkbuktipotong(request, EnumHelper.GetDescription(InvoiceType.Invoice), SessionManager.RoleStr, intList);
            }
            else
            {
                result = Service<BuktiPotongService>().GetChekBuktiPotongList(request, EnumHelper.GetDescription(InvoiceType.Invoice), SessionManager.RoleStr, checks);
            }

            String files = string.Empty;
            byte[] fileBytes;

            request.Page = 0;
            request.PageSize = 0;
            string filename = string.Empty;
            var outputfile = "WHTSubmission_" + DateTime.Now.ToString("yyyyMMdd") + "_" + DateTime.Now.ToString("Hmmss") + ".zip";

            string namafile = string.Empty;
            var username = Service<ParameterService>().GetParameter("file_impersonate_user").value1;
            var domain = Service<ParameterService>().GetParameter("file_impersonate_domain").value1;
            var userdomain = domain + "\\" + username;
            var password = Service<ParameterService>().GetParameter("file_impersonate_password").value1;

            var credential = new NetworkCredential(userdomain, password);
            string fileserver = Service<ParameterService>().GetParameter("file_server").value1;
            string filepath = Service<ParameterService>().GetParameter("file_repo_path_tax").value1;
            string file = "\\\\" + fileserver + "\\" + filepath;

            try
            {
                using (new NetworkConnection(file, credential))
                {
                    //var result = Service<WHTService>().GetDataPDF(request, SessionManager.RoleStr);

                    using (var memoryStream = new MemoryStream())
                    {
                        using (var ziparchive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
                        {

                            foreach (BuktiPotong data in result.Data)
                            {
                                string linkdata = string.Empty;

                                if (System.IO.File.Exists(data.wht_pdf_22) == true && new FileInfo(data.wht_pdf_22).Length > 0)
                                {
                                    linkdata = data.wht_pdf_22;
                                }
                                else if (System.IO.File.Exists(data.wht_pdf_23) == true && new FileInfo(data.wht_pdf_23).Length > 0)
                                {
                                    linkdata = data.wht_pdf_23;
                                }
                                else if (System.IO.File.Exists(data.wht_pdf_42) == true && new FileInfo(data.wht_pdf_42).Length > 0)
                                {
                                    linkdata = data.wht_pdf_42;
                                }

                                if (!String.IsNullOrEmpty(linkdata))
                                {
                                    //filename = Path.GetFileNameWithoutExtension(outputfile) + "\\" + data.pdf_wht_fname;
                                    filename = Path.GetFileName(linkdata);
                                    var fileInArchive = ziparchive.CreateEntry(filename);

                                    Byte[] bytes = System.IO.File.ReadAllBytes(linkdata);
                                    fileBytes = bytes;

                                    using (var zipStream = fileInArchive.Open())
                                    using (var fileToCompressStream = new MemoryStream(fileBytes))
                                    {
                                        fileToCompressStream.CopyTo(zipStream);
                                    }
                                }
                            }
                            ziparchive.Dispose();
                        }
                        Response.Clear();
                        Response.AddHeader("Access-Control-Allow-Origin", "*");
                        Response.ContentType = "application/octet-stream";
                        Response.AddHeader("content-disposition", "attachment;filename=" + outputfile);
                        Response.BinaryWrite(memoryStream.ToArray());
                        Response.Flush();
                        Response.End();
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public FileResult ExportToExcel([DataSourceRequest] DataSourceRequest request, string checks)
        {
            string[] cekcekall = checks.Split(',');

            DataSourceResult result;

            //if (checks == null || checks == "" || cekcekall[0] == "on")
            if (cekcekall[0] == "on")
            {
                var result2 = Service<BuktiPotongService>().GetViewDataSourceResult(request, EnumHelper.GetDescription(InvoiceType.Invoice), SessionManager.RoleStr);
                List<String> list = new List<String>();
                list = result2.Data.Cast<BuktiPotong>().Select(_wht => _wht.invoice_number).ToList();

                //list = result.Data
                String[] intList = list.ToArray();
                result = Service<BuktiPotongService>().getchkbuktipotong(request, EnumHelper.GetDescription(InvoiceType.Invoice), SessionManager.RoleStr, intList);
            }
            else
            {
                result = Service<BuktiPotongService>().GetChekBuktiPotongList(request, EnumHelper.GetDescription(InvoiceType.Invoice), SessionManager.RoleStr, checks);
            }

            var strclmn = new string[] { "No", "Business Unit", "Customer Name", "NPWP", "Address", "Invoice No", "Invoice Date", "Witholding Tax Object Status (Y/N)", "WHT Dealer Submission Status", "Material Amount", "Payment Date", "Witholding Tax No.", "Witholding Tax Date", "Tax Period Month", "Tax Period Year", "WHT Based Amount", "WHT Tariff ", "WHT Amount", "Tax Transaction Code", "Tax Article", "Description Transaction", "PDF WHT art. Status" };

            //var result = Service<WHTService>().GetViewDataSourceResult(request, SessionManager.RoleStr);

            List<BuktiPotong> total = result.Data.Cast<BuktiPotong>().ToList();

            // Initial variable
            // var IsPrint = true;
            int totalData = total.Count;

            // code to create workbook 
            if (totalData < 3000)
            {
                using (var exportData = new MemoryStream())
                {
                    var workbook = new XSSFWorkbook();
                    var sheet = workbook.CreateSheet("WHT Submission");
                    var headerRow = sheet.CreateRow(0);

                    int i = 0;

                    foreach (string c in strclmn)
                    {

                        // Create New Cell
                        var headerCell = headerRow.CreateCell(i);

                        // Set Cell Value
                        headerCell.SetCellValue(c);

                        // Create Style
                        var headerCellStyle = workbook.CreateCellStyle();
                        headerCellStyle.FillForegroundColor = IndexedColors.Aqua.Index;
                        headerCellStyle.FillPattern = FillPattern.SolidForeground;

                        // Add Style to Cell
                        headerCell.CellStyle = headerCellStyle;

                        sheet.AutoSizeColumn(i);
                        i++;
                    }

                    i = 0;
                    foreach (BuktiPotong wht in result.Data)
                    {
                        var content = sheet.CreateRow(i + 1);
                        content.CreateCell(0).SetCellValue(i + 1);
                        content.CreateCell(1).SetCellValue(wht.business_unit);
                        content.CreateCell(2).SetCellValue(wht.nama_npwp);
                        //content.CreateCell(2).SetCellValue(wht.wht_date.ToString("dd/MM/yyyy"));
                        content.CreateCell(3).SetCellValue(wht.no_npwp);
                        content.CreateCell(4).SetCellValue(wht.alamat_npwp);
                        content.CreateCell(5).SetCellValue(wht.invoice_number);
                        content.CreateCell(6).SetCellValue(wht.invoice_date.ToString("dd/MM/yyyy"));
                        //content.CreateCell(7).SetCellValue(wht.dealer_address); //takeout 20190911
                        content.CreateCell(7).SetCellValue(wht.flag_potong_pajak);
                        content.CreateCell(8).SetCellValue(wht.wht_prepaid_status != null ? wht.wht_prepaid_status.Split('|')[1] : "");

                        string materaiAmount = wht.materai_amount != null ? wht.materai_amount.ToString() : "0";
                        string jumlahDPP = wht.jumlah_dpp != null ? wht.jumlah_dpp.ToString() : "0";
                        string jumlahPPh = wht.amount_wht != null ? wht.amount_wht.ToString() : "0";

                        if (wht.flag_potong_pajak == "Y")
                        {
                            content.CreateCell(9).SetCellValue(decimal.Parse(materaiAmount).ToString("###0", new CultureInfo("en-US")));
                            //content.CreateCell(9).SetCellValue(String.Format("{0:0.##}", wht.tarif) + "%");
                            content.CreateCell(10).SetCellValue(wht.tgl_payment.ToString("dd/MM/yyyy"));
                            content.CreateCell(11).SetCellValue(wht.nomor_bukti_potong);
                            content.CreateCell(12).SetCellValue(wht.tanggal_bukti_potong.ToString("dd/MM/yyyy"));
                            content.CreateCell(13).SetCellValue(wht.masa_pajak);
                            content.CreateCell(14).SetCellValue(wht.tahun_pajak);
                            content.CreateCell(15).SetCellValue(decimal.Parse(jumlahDPP).ToString("###0", new CultureInfo("en-US")));
                            content.CreateCell(16).SetCellValue(String.Format("{0:0.##}", wht.tarif) + "%");
                            content.CreateCell(17).SetCellValue(decimal.Parse(jumlahPPh).ToString("###0", new CultureInfo("en-US")));

                        }
                        else
                        {
                            content.CreateCell(9).SetCellValue("");
                            content.CreateCell(10).SetCellValue("");
                            content.CreateCell(11).SetCellValue("");
                            content.CreateCell(12).SetCellValue("");
                            content.CreateCell(13).SetCellValue("");
                            content.CreateCell(14).SetCellValue("");
                            content.CreateCell(15).SetCellValue("");
                            content.CreateCell(16).SetCellValue("");
                            content.CreateCell(17).SetCellValue("");
                        }
                        content.CreateCell(18).SetCellValue(wht.jenis_pajak);
                        content.CreateCell(19).SetCellValue(wht.tax_article);
                        content.CreateCell(20).SetCellValue(wht.description);
                        content.CreateCell(21).SetCellValue(wht.wht_pdf_status == "1" ? "Exist" : "Not Exist");
                        //content.CreateCell(21).SetCellValue(wht.wht_pdf_22 != null ? "Exist" : "Not Exist");
                        //content.CreateCell(22).SetCellValue(wht.wht_pdf_23 != null ? "Exist" : "Not Exist");
                        //content.CreateCell(23).SetCellValue(wht.wht_pdf_42 != null ? "Exist" : "Not Exist");

                        i++;
                    }


                    workbook.Write(exportData);
                    string saveAsFileName = string.Format("WHTSubmission_{0:MM-dd-yyyy_HHmmss}.xlsx", DateTime.Now).Replace("/", "-");

                    byte[] bytes = exportData.ToArray();
                    return File(bytes, "application/vnd.ms-excel", saveAsFileName);

                }
            }

            else
            {
                throw new Exception("Export Excel Failed, Total row can not more than 3000");
            }
        }

        public FileResult ExportToExcelTransactionCode()
        {
            List<ViewTaxTransactionCode> listDocument = Service<BuktiPotongService>().ViewTaxTransactionCode().ToList();

            var strclmn = new string[] { "No", "Tax Transaction Code", "Description" };

            int totalData = listDocument.Count;

            if (totalData < 3000)
            {
                using (var exportData = new MemoryStream())
                {
                    var workbook = new XSSFWorkbook();
                    var sheet = workbook.CreateSheet("Tax Transaction Code");
                    var headerRow = sheet.CreateRow(0);

                    int i = 0;

                    foreach (string c in strclmn)
                    {
                        // Create New Cell
                        var headerCell = headerRow.CreateCell(i);

                        // Set Cell Value
                        headerCell.SetCellValue(c);

                        // Create Style
                        var headerCellStyle = workbook.CreateCellStyle();
                        headerCellStyle.FillForegroundColor = IndexedColors.Aqua.Index;
                        headerCellStyle.FillPattern = FillPattern.SolidForeground;

                        // Add Style to Cell
                        headerCell.CellStyle = headerCellStyle;

                        sheet.AutoSizeColumn(i);
                        i++;
                    }

                    i = 0;
                    foreach (ViewTaxTransactionCode data in listDocument)
                    {
                        var content = sheet.CreateRow(i + 1);
                        content.CreateCell(0).SetCellValue(i + 1);
                        content.CreateCell(1).SetCellValue(data.tax_transaction_code);
                        content.CreateCell(2).SetCellValue(data.description);

                        i++;
                    }

                    workbook.Write(exportData);
                    //string saveAsFileName = string.Format("TaxTransactionCode_{0:MM-dd-yyyy_HHmmss}.xlsx", DateTime.Now).Replace("/", "-");
                    string saveAsFileName = "TaxTransactionCode.xlsx";

                    byte[] bytes = exportData.ToArray();
                    return File(bytes, "application/vnd.ms-excel", saveAsFileName);
                }
            }
            else
            {
                throw new Exception("Export Excel Failed, Total row can not more than 3000");
            }
        }
    }
}