﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Tam.Ebilling.Infrastructure.Mvc;
using Tam.Ebilling.Service;
using Agit.Common.Email;

namespace Tam.Ebilling.Web.Areas.Core.Controllers
{
    public class SettingController : WebControllerBase
    {
        // GET: Core/Setting
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SendTestEmail(EmailSettingRequest emailSetting, string toEmail)
        {
            string emailContent = string.Format("Email testing");

            EmailMessage mail = new EmailMessage();
            mail.From = emailSetting.FromAddress;
            mail.DisplayName = emailSetting.FromDisplay;
            mail.Subject = "Email Tester";
            mail.Recipients.Add(toEmail);
            mail.Body = emailContent;

            EmailService mailService = new EmailService();

            mailService.Host = emailSetting.SmtpHost;
            mailService.Port = emailSetting.SmtpPort;
            mailService.UseDefaultCredentials = emailSetting.UseDefaultCredential;
            mailService.EnableSsl = emailSetting.EnableSSL;
            mailService.User = emailSetting.Username;
            mailService.Password = emailSetting.Password;

            mailService.Send(mail);

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
    }
}