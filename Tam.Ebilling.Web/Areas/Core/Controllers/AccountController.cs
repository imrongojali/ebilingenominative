﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Tam.Ebilling.Domain;
using Tam.Ebilling.Infrastructure;
using Tam.Ebilling.Infrastructure.Cache;
using Tam.Ebilling.Infrastructure.Session;
using Tam.Ebilling.Service;
using Tam.Ebilling.Infrastructure.Mvc;
using System.Text;
using Tam.Ebilling.Domain.Modules.Core.ViewModel;
using Newtonsoft.Json;
using System.Diagnostics;

namespace Tam.Ebilling.Web.Areas.Core.Controllers
{
    public class AccountController : WebControllerBase
    {
        /// <summary>
        /// Display login page
        /// </summary>
        public ActionResult Login()
        {
            if (Request.IsAuthenticated) return Redirect("~");

            var loginType = "default";

            try
            {
                loginType = ApplicationCacheManager.GetConfig<string>("LoginType");

            }
            catch (Exception)
            {

            }
             
            if (loginType == "default")
            {
                return View("Login");
            }
            else
            {
                return View("TamLogin");
            }

            //return View(loginType == "default" ? "Login" : "TamLogin");

            //return View("Login");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Login(string TAMSignOnToken, LoginViewModel loginViewModel)
        {
            var loginType = "default";

            try
            {
                loginType = ApplicationCacheManager.GetConfig<string>("LoginType");

            }
            catch (Exception)
            {
 
            }
            var currentDate = DateTime.Now;
            var returnUrl = Request.Form["ReturnUrl"] ?? string.Empty;
            returnUrl = returnUrl.EndsWith("/Logout") ? string.Empty : (returnUrl.EndsWith("/undefined") ? string.Empty : returnUrl);
            var userService = Service<UserService>();
            var errorMessage = string.Empty;


            if (loginType == "sso")
            {
                //var output = WebApiHelper.PostFromWebApi(new Uri("https://passport.toyota.astra.co.id"), "api/v1/verify", new System.Net.Http.StringContent("token=" + TAMSignOnToken), "application/json");
                var splitter = TAMSignOnToken.Split('.');
                var encodedToken = splitter[1];
                int mod4 = encodedToken.Length % 4;
                if (mod4 > 0)
                {
                    encodedToken += new string('=', 4 - mod4);
                }
                var base64EncodedBytes = Convert.FromBase64String(encodedToken);
                var decodedToken = Encoding.UTF8.GetString(base64EncodedBytes);

                Response.Write(decodedToken);

                var obj = JsonConvert.DeserializeObject<TamToken>(decodedToken);


                 ////user.LastLogin = DateTime.Now;
                 ////userService.Save(user);

                 ////var roles = userService.GetRolesByUsername(user.Username);


                var user = userService.GetUserInternalByUsername(obj.Sub);
                if (user == null)
                {
                    user = userService.GetUserByUsername(obj.Sub);

                    if (user == null)
                    {
                        returnUrl = "err1";
                        //return View("InvalidLogin");
                    }
                }
                var userSession = new UserSession
                {
                    Current = user.user_id,
                    Name = user.display_name,
                    Username = user.user_id,
                    //RoleStr = string.Join(",", roles)
                    RoleStr = user.customer_group,
                    UserType = user.user_type,
                    LevelCode = user.level_code
                };


                Session[AppConstants.SessionKey] = userSession;

                FormsAuthentication.SetAuthCookie(user.user_id, true);
                
                //var user = userService.GetUserByUsername(obj.Sub);

                return Redirect(!string.IsNullOrEmpty(returnUrl) ? returnUrl : "~");

            }
            else
            {
                try
                {
                    if (string.IsNullOrEmpty(loginViewModel.Username)) throw new Exception("Please fill the Username");
                    if (string.IsNullOrEmpty(loginViewModel.Password)) throw new Exception("Please fill the Password");

                    //loginViewModel.Username = domain + @"\" + loginViewModel.Username.ToLower();

                    if (userService.Authenticate(loginViewModel))
                    {
                        //var user = userService.GetUserInternalByUsername(loginViewModel.Username);
                        //if (user == null)
                        //{
                        //    user = userService.GetUserByUsername(loginViewModel.Username);
                        //}
                        var user = userService.GetUserByUsername(loginViewModel.Username);
                        //var roles = userService.GetRolesByUsername(loginViewModel.Username);

                        var userSession = new UserSession
                        {
                            Current = user.user_id,
                            Name = user.display_name,
                            Username = user.user_id,
                            //RoleStr = string.Join(",", roles)
                            RoleStr = user.customer_group,
                            UserType = user.user_type,
                            LevelCode = user.level_code
                        };

                        Session[AppConstants.SessionKey] = userSession;

                        FormsAuthentication.SetAuthCookie(user.user_id, true);
                    }
                }
                catch (Exception ex)
                {
                    errorMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                }

                return Redirect(!string.IsNullOrEmpty(returnUrl) ? returnUrl : "~");
            }
        }

        /// <summary>
        /// Logout from application asynchronously
        /// </summary>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LogoutAsync()
        {
            ClearSession();

            return Json(new { Success = true });
        }

        /// <summary>
        /// Logout from application
        /// </summary>
        public ActionResult Logout()
        {
            ClearSession();

            return RedirectToAction("Login");
        }

        /// <summary>
        /// Clear user form cookies and session
        /// </summary>
        private void ClearSession()
        {
            Session.RemoveAll();
            FormsAuthentication.SignOut();
            HttpCookie rFormsCookie = new HttpCookie(FormsAuthentication.FormsCookieName, string.Empty);
            rFormsCookie.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(rFormsCookie);
        }
    }
}
