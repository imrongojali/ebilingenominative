﻿using Kendo.Mvc.UI;
using Microsoft.Reporting.WebForms;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tam.Ebilling.Domain;
using Tam.Ebilling.Infrastructure.Mvc;
using Tam.Ebilling.Infrastructure.Session;
using Tam.Ebilling.Infrastructure.Enum;
using Tam.Ebilling.Service;
using Agit.Helper.Helper;
using Tam.Ebilling.Infrastructure.Helper;
using System.Net;
using Tam.Ebilling.Infrastructure.Cache;
using Tam.Ebilling.Reporting;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System.Text;
using System.Globalization;
using System.Data;
using Tam.Ebilling.Web.Areas.Core.Models;

namespace Tam.Ebilling.Web.Areas.Core.Controllers
{
    public class ReceiptController : CommonFeatureController
    {
        // GET: Core/Log
        public ActionResult Index()
        {
            if (!IsRoleAllowedAccessMenu())
            {
                return RedirectToAction("AccessDenied", "Error", new { area = "" });
            }

            var transactiontype = Service<TransactionTypeService>().GetTransactionType();
            ViewBag.TransactionType = transactiontype;

            var arstatus = Service<ARStatusService>().GetARStatus();
            ViewBag.ARStatus = arstatus;

            List<CustomerGroup> customer = Service<CustomerService>().GetCustomer();

            CustomerGroup x = new CustomerGroup();
                          x.customer_group_id = "ALL-CUST";
                          x.customer_group_name = "ALL";

            customer.Insert(0, x);

            ViewBag.CustomerName = customer;

            var totAmount = Service<ParameterService>().GetParameterList("summary_tot_amount");
            ViewBag.TotAmount = totAmount;

            var signs = Service<ParameterService>().GetParameterList("summary_inv_sign");
            ViewBag.Signs = signs;

            var cust = SessionManager.RoleStr;
            ViewBag.Customer = cust;

            var userType = SessionManager.UserType;
            ViewBag.UserType = userType;

            ViewBag.IsAllowedDownload = IsAllowed("download");

            return View();
        }

        [HttpPost]
        public ActionResult Read([DataSourceRequest] Kendo.Mvc.UI.DataSourceRequest request)
        {
            var result = Service<ReceiptService>().GetDataSourceResult(request, SessionManager.RoleStr, SessionManager.UserType);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public void ExportZip(string dateFrom, string dateTo, string businessType,
                    string customerId, int signBy, string repPath, Parameter paramSignBy , string totalAmount)
        {
            byte[] fileBytes, zipBytes;
            string mimeType, extension;

            string filename = string.Empty;

            var outputfile = "Receipt - " + dateFrom + "-" + dateTo + ".zip";

            var dataSourceX = Service<InvoiceService>().GetReceipt(dateFrom, dateTo, businessType, customerId,totalAmount);

            if (dataSourceX.Count <= 0)
            {
                throw new Exception("Data not exist");
            }

            var custId = dataSourceX.GroupBy(x => x.customer_group).Select(x => x.FirstOrDefault());

            using (var memoryStream = new MemoryStream())
            {
                using (var ziparchive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
                {
                    foreach (Invoice x in custId)
                    {
                        List<Invoice> dataSource = dataSourceX.Where(z => z.customer_group == x.customer_group).ToList();

                        Reporting.Report report = new Reporting.Report();
                        filename = Path.GetFileNameWithoutExtension(outputfile) + "\\" + "Receipt - " + x.customer_name.Replace('/', '-') + ".pdf";
                        var fileInArchive = ziparchive.CreateEntry(filename);

                        fileBytes = report.GetSumReceiptInvoiceAllReport(
                            dateFrom,
                            dateTo,
                            dataSource,
                            repPath,
                            paramSignBy,
                            out extension,
                            out mimeType);

                        using (var zipStream = fileInArchive.Open())
                        using (var fileToCompressStream = new MemoryStream(fileBytes))
                        {
                            fileToCompressStream.CopyTo(zipStream);
                        }
                    }
                    ziparchive.Dispose();
                }

                Service<ReceiptService>().RecordHistory(dateFrom, dateTo, businessType, customerId, SessionManager.Username);

                Response.Clear();
                Response.AddHeader("Access-Control-Allow-Origin", "*");
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("content-disposition", "attachment;filename=\"" + outputfile + "\"");
                Response.BinaryWrite(memoryStream.ToArray());
                Response.Flush();
                Response.End();
            }
        }

        public void Download(string dateFrom, string dateTo,  string businessType, string customerId, int signBy, string totalAmount)
        {
            try
            {
                Invoice inv = new Invoice();
                var paramSignBy = Service<ParameterService>().GetParameterById(signBy);

                string repPath = string.Format("{0}/{1}", ConfigurationManager.AppSettings["RDLPath"], ConfigurationManager.AppSettings["ReceiptPath"]);

                if (SessionManager.UserType == "Customer")
                {
                    if (string.IsNullOrEmpty(SessionManager.RoleStr))
                    {
                        throw new Exception("Current user does not have group");
                    }

                    customerId = SessionManager.RoleStr;
                }

                if (customerId == "ALL-CUST")
                {
                    ExportZip(dateFrom, dateTo, businessType, customerId, signBy, repPath, paramSignBy, totalAmount);
                }
                else
                {
                    var dataSource = Service<InvoiceService>().GetReceipt(dateFrom, dateTo, businessType, customerId, totalAmount);

                    if (dataSource.Count <= 0)
                    {
                        throw new Exception("Data not exist");
                    }

                    LocalReport report = new LocalReport();
                    report.ReportPath = repPath;
                    report.SetParameters(new ReportParameter("pDateFrom", dateFrom));
                    report.SetParameters(new ReportParameter("pDateTo", dateTo));
                    report.SetParameters(new ReportParameter("pDateTo", dateTo));
                    report.SetParameters(new ReportParameter("pCustomerName", dataSource.FirstOrDefault().customer_name));
                    report.SetParameters(new ReportParameter("pCustomerAddress", dataSource.FirstOrDefault().customer_address));
                    report.SetParameters(new ReportParameter("pPosition", paramSignBy.description));
                    report.SetParameters(new ReportParameter("pSignBy", paramSignBy.value1));
                    report.DataSources.Clear();
                    report.DataSources.Add(new ReportDataSource("DataSetSummaryInvoice", dataSource));
                    report.Refresh();

                    Stream stream = new MemoryStream();
                    Warning[] warnings;
                    string mimeType, encoding, extension;
                    string[] streamIds;

                    byte[] bytes = report.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

                    Service<ReceiptService>().RecordHistory(dateFrom, dateTo, businessType, customerId, SessionManager.Username);

                    Response.Buffer = true;
                    Response.Clear();
                    Response.ContentType = mimeType;
                    Response.SetCookie(new HttpCookie("fileDownload", "true") { Path = "/" });
                    Response.AddHeader("content-disposition", "attachment; filename= " + "Receipt" + "-" + dateFrom + "-" + dateTo + "." + extension);
                    Response.OutputStream.Write(bytes, 0, bytes.Length); // create the file  
                    Response.Flush(); // send it to the client to download  
                    Response.End();

                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}