﻿using Kendo.Mvc.UI;
using Microsoft.Reporting.WebForms;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tam.Ebilling.Domain;
using Tam.Ebilling.Infrastructure.Mvc;
using Tam.Ebilling.Infrastructure.Session;
using Tam.Ebilling.Infrastructure.Enum;
using Tam.Ebilling.Service;
using Agit.Helper.Helper;
using Tam.Ebilling.Infrastructure.Helper;
using System.Net;
using Tam.Ebilling.Infrastructure.Cache;
using Tam.Ebilling.Reporting;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System.Text;
using System.Globalization;
using System.Data;
using Tam.Ebilling.Web.Areas.Core.Models;
using System.Text.RegularExpressions;
using NPOI.SS.Formula.Functions;
using Agit.Helper.Extension;

namespace Tam.Ebilling.Web.Areas.Core.Controllers
{
    public class InvoiceController : CommonFeatureController
    {
        // GET: Core/Log
        public ActionResult Index()
        {
            var transactiontype = Service<TransactionTypeService>().GetTransactionType();
            ViewBag.TransactionType = transactiontype;

            var arstatus = Service<ARStatusService>().GetARStatus();
            ViewBag.ARStatus = arstatus;

            var customer = Service<CustomerService>().GetCustomer();
            ViewBag.CustomerName = customer;

            var cust = SessionManager.RoleStr;
            ViewBag.Customer = cust;

            ViewBag.UserType = SessionManager.UserType;
            ViewBag.IsAllowedExportToExcel = IsAllowed("export_to_excel") || ViewBag.UserType == "Customer";
            ViewBag.IsAllowedDownloadAll = IsAllowed("download_all") || ViewBag.UserType == "Customer";
            ViewBag.IsAllowedDownloadInvoice = IsAllowed("download_invoice") || ViewBag.UserType == "Customer";
            ViewBag.IsAllowedDownloadInvoiceQR = IsAllowed("download_invoice_qr") || ViewBag.UserType == "Customer";
            ViewBag.IsAllowedDownloadAttachment = IsAllowed("download_attachment") || ViewBag.UserType == "Customer";
            ViewBag.IsAllowedDownloadPI = IsAllowed("download_pi") || ViewBag.UserType == "Customer";
            ViewBag.IsAllowedUploadInvoice = IsAllowed("upload_invoice") || ViewBag.UserType == "Customer";
            ViewBag.IsAllowedDownloadPPH2 = true; //IsAllowed("download_pph22") || ViewBag.UserType == "Customer";
            ViewBag.IsAllowedDownloadWhtPDF = true;
            ViewBag.IsAllowedDownloadTaxInvoice = true;
            return View();
        }
        
        [HttpPost]
        public ActionResult Read([DataSourceRequest] Kendo.Mvc.UI.DataSourceRequest request)
        {
            
            var result = Service<InvoiceService>().GetGridInitDataSourceResult(request, Convert.ToInt16(EnumHelper.GetDescription(InvoiceType.Invoice)), SessionManager.RoleStr);

            return Json(result, JsonRequestBehavior.AllowGet);
 
        }

        public void ExportZip([DataSourceRequest] DataSourceRequest request)
        {
            request.Page = 0;
            request.PageSize = 0;

            string linkdata = string.Empty;
            string namapath = string.Empty;
            string filenamepdf = string.Empty;
            var username = Service<ParameterService>().GetParameter("file_impersonate_user").value1;
            var domain = Service<ParameterService>().GetParameter("file_impersonate_domain").value1;
            var userdomain = domain + "\\" + username;
            var password = Service<ParameterService>().GetParameter("file_impersonate_password").value1;

            var credential = new NetworkCredential(userdomain, password);
            string fileserver = Service<ParameterService>().GetParameter("file_server").value1;
            string filepath = Service<ParameterService>().GetParameter("file_repo_path_tax").value1;
            string file = "\\\\" + fileserver + "\\" + filepath;

            try
            {
                using (new NetworkConnection(file, credential))
                {
                    var result = Service<InvoiceService>().GetViewDataSourceResult(request, Convert.ToInt16(EnumHelper.GetDescription(InvoiceType.Invoice)), SessionManager.RoleStr);
                    //var result = Service<InvoiceService>().GetGridInitDataSourceResult(request, Convert.ToInt16(EnumHelper.GetDescription(InvoiceType.Invoice)), SessionManager.RoleStr);
                    var outputfile = "TAXINVOICE - " + DateTime.Now.ToString("yyyyMMdd") + ".zip";

                    using (var memoryStream = new MemoryStream())
                    {
                        using (var ziparchive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
                        {
                            foreach (Invoice inv in result.Data)
                            {       
                                if (inv.tax_attc_url != null)
                                {
                                    var value = inv.tax_attc_url;
                                    string Result = Path.GetFileNameWithoutExtension(value) + ".pdf";
                                    var url = Path.Combine(file, Result);
                                    if (System.IO.File.Exists(url) == true)
                                    {
                                        namapath = Path.GetFileNameWithoutExtension(outputfile) + "\\" + Path.GetFileNameWithoutExtension(inv.tax_attc_url) + "\\" + Path.GetFileNameWithoutExtension(inv.tax_attc_url) + ".pdf";
                                        var fileInArchive = ziparchive.CreateEntry(namapath);

                                        using (FileStream _file = new FileStream(url, FileMode.Open, FileAccess.Read))
                                        {
                                            byte[] bytes = new byte[_file.Length];

                                            _file.Read(bytes, 0, (int)_file.Length);

                                            using (var zipStream = fileInArchive.Open())
                                            using (var fileToCompressStream = new MemoryStream(bytes))
                                            {
                                                fileToCompressStream.CopyTo(zipStream);
                                            }
                                        }
                                    }
                                    else if (System.IO.File.Exists(url) == false)
                                    {
                                        ziparchive.CreateEntry(Path.GetFileNameWithoutExtension(outputfile) + "\\" + Path.GetFileNameWithoutExtension(inv.tax_attc_url) + "\\" + Path.GetFileNameWithoutExtension(inv.tax_attc_url) + ".NOTFOUND");
                                    }
                                }
                                else
                                {
                                    //ziparchive.CreateEntry(Path.GetFileNameWithoutExtension(outputfile) + "\\" + Path.GetFileNameWithoutExtension(inv.tax_attc_url) + "\\" + Path.GetFileNameWithoutExtension(inv.tax_attc_url) + ".NOTFOUND");
                                    //throw new Exception("File Not Found !");
                                }
                            }
                            ziparchive.Dispose();

                        }
                        DownloadFinish();
                        Response.Clear();
                        Response.AddHeader("Access-Control-Allow-Origin", "*");
                        Response.ContentType = "application/octet-stream";
                        Response.AddHeader("content-disposition", "attachment;filename=" + outputfile);
                        Response.BinaryWrite(memoryStream.ToArray());
                        Response.Flush();
                        Response.End();
                    }
                }
            }
            //catch (FileNotFoundException e)
            //{
            //    throw new Exception(e.Message);
            //}
            catch (Exception e)
            {
                DownloadFinish();
                throw new Exception(e.Message);
            }
        }

        public void ExportZipInvoice([DataSourceRequest] DataSourceRequest request) 
        {
            byte[] fileBytes, zipBytes;
            string mimeType, extension;

            request.Page = 0;
            request.PageSize = 0;
            string filename = string.Empty;
            
            //var result = Service<InvoiceService>().GetViewDataSourceResult(request, Convert.ToInt16(EnumHelper.GetDescription(InvoiceType.Invoice)), SessionManager.RoleStr);
            var result = Service<InvoiceService>().GetGridInitDataSourceResult(request, Convert.ToInt16(EnumHelper.GetDescription(InvoiceType.Invoice)), SessionManager.RoleStr);
            var outputfile = "INVOICE - " + DateTime.Now.ToString("yyyyMMdd") + ".zip";
            string user = SessionManager.Current;
            string userid = "";
            if (user != "TAM\\Mayumi" || user != "TAM\\frans" || user != "TAM\\ronnyk102181" || user != "TAM\\indah.gustiana" || user != "TAM\\aliandy.syahriel")
            {
                user = userid;
            }
            else
            {
                user = SessionManager.Current;
            }
            string name = SessionManager.Name;
            string nameid = "";
            if (name != "Mayumi Eguchi" || name != "Frans lhutan B" || name != "Ronny Kusgianto" || name != "Indah Sari Gustiana" || name != "Aliandy Syahriel")
            {
                name = nameid;
            }
            else
            {
                name = SessionManager.Name;
            }
            using (var memoryStream = new MemoryStream())
            {
                using (var ziparchive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
                {
                    foreach (Invoice data in result.Data)
                    {
                        Reporting.Report report = new Reporting.Report();

                            //filename = Path.GetFileNameWithoutExtension(outputfile) + "\\" + data.doc_date.ToString("yyyyMMdd") + "\\" + data.invoice_number.Replace('/', '-') + ".pdf";
                            filename = Path.GetFileNameWithoutExtension(outputfile) + "\\" + "INVOICE - " + data.invoice_number.Replace('/', '-') + "\\" + "Invoice - " + data.invoice_number.Replace('/', '-') + ".pdf";
                            var fileInArchive = ziparchive.CreateEntry(filename);

                            if (data.business_unit_id == 2)
                            {
                                fileBytes = report.GetDebitAdviceReport(
                                    data.da_no,
                                    data.business_unit_id.ToString(),
                                    name,
                                    out extension,
                                    out mimeType);
                            }
                            else if (data.business_unit_id == 1)
                            {
                                fileBytes = report.GetMvInvoiceReport(
                                    data.da_no,
                                    data.business_unit_id.ToString(),
                                    name,
                                    out extension,
                                    out mimeType);
                            }
                            else if (data.business_unit_id == 3) // spare part invoice
                            {
                                if (data.invoice_number.Contains("FDA")) //debit
                                {
                                    fileBytes = report.GetSpDebitAdviceReport(
                                    data.invoice_number,
                                    name,
                                    out extension,
                                    out mimeType);
                                }
                                else if (data.invoice_number.Contains("FCA")) //credit
                                {
                                    fileBytes = report.GetSpCreditAdviceReport(
                                    data.invoice_number,
                                    name,
                                    out extension,
                                    out mimeType);
                                }
                                else
                                {
                                    fileBytes = null;
                                }
                            }
                            else
                            {
                                fileBytes = null;
                            }

                            using (var zipStream = fileInArchive.Open())
                            using (var fileToCompressStream = new MemoryStream(fileBytes))
                            {
                                fileToCompressStream.CopyTo(zipStream);
                            }

                            if (data.business_unit_id != 3) // not spare part invoice
                            {
                                var Update = Service<InvoiceService>().GetUpdate(data.da_no, user);
                            }
                            else
                            {
                                var Update = Service<InvoiceService>().UpdateSparePartStatusDownload(
                                    data.da_no,
                                    user,
                                    EnumHelper.GetDescription(DownloadType.InvoiceQR),
                                    EnumHelper.GetDescription(InvoiceType.Invoice)
                                    );
                            }                        
                    }
                    ziparchive.Dispose();
                }
                DownloadFinish();
                Response.Clear();
                Response.AddHeader("Access-Control-Allow-Origin", "*");
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("content-disposition", "attachment;filename=\"" + outputfile + "\"");
                Response.BinaryWrite(memoryStream.ToArray());
                Response.Flush();
                Response.End();
            }
        }

        public ActionResult Excel_Export_Save(string contentType, string base64, string fileName)
        {
            var fileContents = Convert.FromBase64String(base64);

            string test = "2018-01-02";
            var splitted = test.Split('-');



            return File(fileContents, contentType, fileName);
        }

        public ActionResult Search_Invoice(string trans_code)
        {
            string filename = Service<AttachmentService>().GetAttachment(trans_code).ToString();
            return View();
        }

        public ActionResult InvoiceDetail([DataSourceRequest] Kendo.Mvc.UI.DataSourceRequest request, string invoice_number,
            int business_unit_id, string transaction_type, string pph_no, decimal total_amount, decimal dpp, decimal tax, decimal rate, string amount_pph, string currency, int transaction_type_id,
            string dormitory, string vat_dormitory)
        {
            //var result = Service<CommentService>().GetComment(invoice_number, business_unit_id);
            Invoice empObj = new Invoice();
            empObj.invoice_number = invoice_number;
            empObj.business_unit_id = business_unit_id;
            empObj.business_unit = business_unit_id.ToString();
            empObj.transaction_type = transaction_type;
            empObj.dpp = dpp;
            empObj.tax = tax;
            empObj.total_amount = total_amount;
            empObj.pph_no = pph_no.Replace("|","#");
            empObj.comments = Service<CommentService>().GetComment(invoice_number, business_unit_id).ToList();
            empObj.rate = rate;
            empObj.amount_pph = amount_pph == null ? 0 : Convert.ToDecimal(amount_pph);
            empObj.currency = currency;
            empObj.transaction_type_id = transaction_type_id;
            empObj.dormitory = dormitory == null ? 0 : Convert.ToDecimal(dormitory);
            empObj.vat_dormitory = vat_dormitory == null ? 0 : Convert.ToDecimal(vat_dormitory);

            return Json(new { empObj }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult TaxInvoice([DataSourceRequest] Kendo.Mvc.UI.DataSourceRequest request, string invoice_number,
            int business_unit_id, string tax_invoice)
        {
            Invoice taxObj = new Invoice();
            taxObj.invoice_number = invoice_number;
            taxObj.business_unit = business_unit_id.ToString();
            taxObj.tax_invoice = tax_invoice;
            return PartialView("~/Areas/Core/Views/Invoice/Form/TaxInvoice.cshtml", taxObj);
        }
        public ActionResult EmailSchedule([DataSourceRequest] Kendo.Mvc.UI.DataSourceRequest request, string invoice_number,
           string customer_group, string email)
        {
            //var result = Service<CommentService>().GetComment(invoice_number, business_unit_id);
            Invoice mailSch = new Invoice();
            mailSch.invoice_number = invoice_number;
            mailSch.customer_group = customer_group;
            mailSch.email = email;

            return PartialView("Tam.Ebilling.Notification/SendMailService.cs", mailSch);
        }

        public ActionResult GetPDF([DataSourceRequest] Kendo.Mvc.UI.DataSourceRequest request, string id)
        {
            var pdfid = Service<InvoiceService>().GetDataPDF(id);
            string linkdata = string.Empty;
            string namafile = string.Empty;
            string url = string.Empty;
            var username = Service<ParameterService>().GetParameter("file_impersonate_user").value1;
            var domain = Service<ParameterService>().GetParameter("file_impersonate_domain").value1;
            var userdomain = domain + "\\" + username;
            var password = Service<ParameterService>().GetParameter("file_impersonate_password").value1;

            var credential = new NetworkCredential(userdomain, password);
            string fileserver = Service<ParameterService>().GetParameter("file_server").value1;
            string filepath = Service<ParameterService>().GetParameter("file_repo_path_tax").value1;
            string file = "\\\\" + fileserver + "\\" + filepath;


            using (new NetworkConnection(file, credential))
            {
                foreach (var datatax in pdfid)
                {
                    linkdata = datatax.tax_attc_url;
                    namafile = Path.GetFileNameWithoutExtension(linkdata) + ".pdf";
                    url = Path.Combine(file, namafile);
                }

                if (System.IO.File.Exists(url) == true)
                {
                    FileStream fs = new FileStream(url, FileMode.Open, FileAccess.Read);
                    return File(fs, "application/pdf");
                }
                else
                {
                    namafile = Server.MapPath("~/Content/img/ico-taxnotfoundd.png");
                    FileStream fs = new FileStream(namafile, FileMode.Open, FileAccess.Read);
                    return File(fs, "image/jpeg");
                }
            }
        }

        [HttpPost]
        public ActionResult ExportPDF(string id)
        {
            dynamic result;

            var yy = Service<InvoiceService>().GetDataPDF(id);
            string url = string.Empty;
            string linkdata = string.Empty;
            
            foreach (var data in yy)
            {
                linkdata = data.tax_attc_url;
            }
            
            if (string.IsNullOrEmpty(linkdata))
            {
                result = new { Url = Server.MapPath("~/Content/img/ico-taxnotfoundd.png"), Result = false, Message = "Tax invoice document is waiting for approval." };
            }
            else
            {
                var username = Service<ParameterService>().GetParameter("file_impersonate_user").value1;
                var domain = Service<ParameterService>().GetParameter("file_impersonate_domain").value1;
                //var userdomain = domain + "\\" + username;
                //var password = Service<ParameterService>().GetParameter("file_impersonate_password").value1;

                //var credential = new NetworkCredential(userdomain, password);
                string fileserver = Service<ParameterService>().GetParameter("file_server").value1;
                string filedir = Service<ParameterService>().GetParameter("file_repo_path_tax").value1;
                string filepath = "\\\\" + fileserver + "\\" + filedir;

                //using (new NetworkConnection(filepath, credential))
                //{
                    var temp = Path.GetPathRoot(linkdata);

                    if (temp == "\\\\VSV-C003-016018\\Interface")
                    {
                        linkdata = Path.Combine(filepath, Path.GetFileName(linkdata));

                        if (!System.IO.File.Exists(linkdata))
                        {
                            Byte[] bytes = System.IO.File.ReadAllBytes(Server.MapPath("~/Content/img/ico-taxnotfoundd.png"));
                            String file = Convert.ToBase64String(bytes);

                            result = new { Url = file, Result = false, Message = "Error! " + Path.GetFileName(linkdata) + " is not exists on server." };
                        }
                        else
                        {
                            Byte[] bytes = System.IO.File.ReadAllBytes(linkdata);
                            String file = Convert.ToBase64String(bytes);

                            result = new { Url = file, Result = true, Message = "" };
                        }
                    }
                    else if (temp == "\\\\10.85.11.83\\Interface")
                    {
                        linkdata = Path.Combine(filepath, Path.GetFileName(linkdata));

                        if (!System.IO.File.Exists(linkdata))
                        {
                            Byte[] bytes = System.IO.File.ReadAllBytes(Server.MapPath("~/Content/img/ico-taxnotfoundd.png"));
                            String file = Convert.ToBase64String(bytes);

                            result = new { Url = file, Result = false, Message = "Error! " + Path.GetFileName(linkdata) + " is not exists on server." };
                        }
                        else
                        {
                            Byte[] bytes = System.IO.File.ReadAllBytes(linkdata);
                            String file = Convert.ToBase64String(bytes);

                            result = new { Url = file, Result = true, Message = "" };

                        }
                    }
                    else
                    {
                        if (linkdata == "")
                        {
                            Byte[] bytes = System.IO.File.ReadAllBytes(Server.MapPath("~/Content/img/ico-taxnotfoundd.png"));
                            String file = Convert.ToBase64String(bytes);

                            result = new { Url = file, Result = false, Message = "Error! " + Path.GetFileName(linkdata) + " is not exists on server." };
                        }
                        else
                        {
                            Byte[] bytes = System.IO.File.ReadAllBytes(linkdata);
                            String file = Convert.ToBase64String(bytes);

                            result = new { Url = file, Result = true, Message = "" };

                        }
                    }
                }
            //}

            return Json(new { result }, JsonRequestBehavior.AllowGet);
        }

        #region WHT PDF ROW
        [HttpPost]
        public ActionResult ExportPDFWHTROW(string id)
        {
            dynamic result;

            var yy = Service<InvoiceService>().GetDataWHTPDF(id);
            string url = string.Empty;
            string linkdata = string.Empty;
            foreach (var data in yy)
            {
                linkdata = data.PDFWithholding;
            }
            if (string.IsNullOrEmpty(linkdata))
            {
                result = new { Url = Server.MapPath("~/Content/img/ico-taxnotfoundd.png"), Result = false, Message = "Withholding Tax Art 22 document is waiting for approval." };
            }
            else
            {
                //foreach (var data in yy)
                //{
                //    linkdata = data.PDFWithholding;
                //}
                var username = Service<ParameterService>().GetParameter("file_impersonate_user").value1;
                var domain = Service<ParameterService>().GetParameter("file_impersonate_domain").value1;
                var userdomain = domain + "\\" + username;
                var password = Service<ParameterService>().GetParameter("file_impersonate_password").value1;

                var credential = new NetworkCredential(userdomain, password);
                string fileserver = Service<ParameterService>().GetParameter("file_server").value1;
                string filedir = Service<ParameterService>().GetParameter("file_repo_path_tax").value1;
                string filepath = "\\\\" + fileserver + "\\" + filedir;

                using (new NetworkConnection(filepath, credential))
                {

                    var temp = Path.GetPathRoot(linkdata);

                    Byte[] bytes = System.IO.File.ReadAllBytes(linkdata);
                    String file = Convert.ToBase64String(bytes);

                    result = new { Url = file, Result = true, Message = "" };
                }

            }

            return Json(new { result }, JsonRequestBehavior.AllowGet);

        }

        //public FileResult DownloadPDFWHTROW([DataSourceRequest] Kendo.Mvc.UI.DataSourceRequest request, string id)
        public void DownloadPDFWHTROW([DataSourceRequest] Kendo.Mvc.UI.DataSourceRequest request, string id)
        {
            //dynamic result;
            var invoicedata = Service<InvoiceService>().GetDataWHTPDF(id);
            string linkdata = string.Empty;
            string filenamepdf = string.Empty;
            var username = Service<ParameterService>().GetParameter("file_impersonate_user").value1;
            var domain = Service<ParameterService>().GetParameter("file_impersonate_domain").value1;
            var userdomain = domain + "\\" + username;
            var password = Service<ParameterService>().GetParameter("file_impersonate_password").value1;

            var credential = new NetworkCredential(userdomain, password);
            string fileserver = Service<ParameterService>().GetParameter("file_server").value1;
            string filepath = Service<ParameterService>().GetParameter("file_repo_path_tax").value1;
            string file = "\\\\" + fileserver + "\\" + filepath;
            var outputfile = id.Replace("/", "-") + ".zip";
            byte[] fileBytes;
            try
            {
                //var doc2 = new List<string>();
                //foreach (var data in invoicedata)
                //{
                //    linkdata = data.PDFWithholding;
                //    filenamepdf = Path.GetFileNameWithoutExtension(linkdata);
                //    //doc2.Add(filenamepdf);

                //}
                using (new NetworkConnection(filepath, credential))
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        using (var ziparchive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
                        {
                            foreach (Invoice inv in invoicedata)
                            {
                                var docdownload = inv.PDFWithholding;
                                var temp = Path.GetPathRoot(docdownload);
                                filenamepdf = Path.GetFileName(docdownload);
                                if (filenamepdf != null)
                                {
                                    //byte[] fileBytes = System.IO.File.ReadAllBytes(linkdata);

                                    // string fileName = filenamepdf;
                                    var fileInArchive = ziparchive.CreateEntry(filenamepdf);
                                    Byte[] bytes = System.IO.File.ReadAllBytes(docdownload);
                                    fileBytes = bytes;
                                    //return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
                                    using (var zipStream = fileInArchive.Open())
                                    using (var fileToCompressStream = new MemoryStream(fileBytes))
                                    {
                                        fileToCompressStream.CopyTo(zipStream);
                                    }
                                }
                                else
                                {
                                    DownloadFinish();
                                    throw new Exception("Withholding Tax Art 22 document is waiting for approval.");
                                }
                            }

                            ziparchive.Dispose();
                        }
                        DownloadFinish();
                        Response.Clear();
                        Response.AddHeader("Access-Control-Allow-Origin", "*");
                        Response.ContentType = "application/octet-stream";
                        Response.AddHeader("content-disposition", "attachment;filename=" + outputfile);
                        Response.BinaryWrite(memoryStream.ToArray());
                        Response.Flush();
                        Response.End();
                    }
                    //return Json(new { result }, JsonRequestBehavior.AllowGet);
                    //result = new { Url = file, Result = true, Message = "" };
                }
            }
            catch (Exception e)
            {
                DownloadFinish();
                throw new Exception(e.Message);
            }


            //return Json(new { result }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public FileResult DownloadPDF([DataSourceRequest] Kendo.Mvc.UI.DataSourceRequest request, string id)
        {
            var invoicedata = Service<InvoiceService>().GetDataPDF(id);
            string linkdata = string.Empty;
            string filenamepdf = string.Empty;
            var username = Service<ParameterService>().GetParameter("file_impersonate_user").value1;
            var domain = Service<ParameterService>().GetParameter("file_impersonate_domain").value1;
            var userdomain = domain + "\\" + username;
            var password = Service<ParameterService>().GetParameter("file_impersonate_password").value1;

            var credential = new NetworkCredential(userdomain, password);
            string fileserver = Service<ParameterService>().GetParameter("file_server").value1;
            string filepath = Service<ParameterService>().GetParameter("file_repo_path_tax").value1;
            string file = "\\\\" + fileserver + "\\" + filepath;

            using (new NetworkConnection(file, credential))
            {
                foreach (var data in invoicedata)
                {
                    linkdata = data.tax_attc_url;
                    filenamepdf = Path.GetFileNameWithoutExtension(linkdata) + ".pdf";
                }

                var temp = Path.GetPathRoot(linkdata);

                if (temp == "\\\\VSV-C003-016018\\Interface")
                {
                    var url = Path.Combine(file, filenamepdf);
                    byte[] fileBytes = System.IO.File.ReadAllBytes(url);
                    string fileName = filenamepdf;
                    return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, filenamepdf);
                }
                else if (temp == "\\\\10.85.11.83\\Interface")
                {
                    var url = Path.Combine(file, filenamepdf);
                    byte[] fileBytes = System.IO.File.ReadAllBytes(url);
                    string fileName = filenamepdf;
                    return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, filenamepdf);
                }
                else
                {
                    filenamepdf = Path.GetFileName(linkdata);

                    byte[] fileBytes = System.IO.File.ReadAllBytes(linkdata);
                    string fileName = filenamepdf;
                    return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
                }
            }

            //using (new NetworkConnection(file, credential))
            //{
            //    foreach (var data in invoicedata)
            //    {                    
            //        linkdata = data.tax_attc_url;
            //        filenamepdf = Path.GetFileNameWithoutExtension(linkdata) + ".pdf";

                //    }
                //    var url = Path.Combine(file, filenamepdf);
                //    byte[] fileBytes = System.IO.File.ReadAllBytes(url);
                //    string fileName = filenamepdf;
                //    return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, filenamepdf);
                //}

        }
        public void DownloadAllAttachment([DataSourceRequest] DataSourceRequest request)
        {
            request.Page = 0;
            request.PageSize = 0;

            var username = Service<ParameterService>().GetParameter("file_impersonate_user").value1;
            var domain = Service<ParameterService>().GetParameter("file_impersonate_domain").value1;
            var userdomain = domain + "\\" + username;
            var password = Service<ParameterService>().GetParameter("file_impersonate_password").value1;

            var credential = new NetworkCredential(userdomain, password);
            string fileserver = Service<ParameterService>().GetParameter("file_server").value1;
            string filepath = Service<ParameterService>().GetParameter("file_repo_path").value1;
            string file = "\\\\" + fileserver + "\\" + filepath;

            string mimeType, encoding, extension, repPath = string.Empty;
            byte[] fileBytes;
            Warning[] warnings;
            string[] streamIds;
            LocalReport report = new LocalReport();
            ReportViewer reportViewer = new ReportViewer(); 

            try
            {
                
                    //var result = Service<InvoiceService>().GetViewDataSourceResult(request, Convert.ToInt16(EnumHelper.GetDescription(InvoiceType.Invoice)), SessionManager.RoleStr);
                    var result = Service<InvoiceService>().GetGridInitDataSourceResult(request, Convert.ToInt16(EnumHelper.GetDescription(InvoiceType.Invoice)), SessionManager.RoleStr);
                    
                    var outputfile = "AttachmentInvoice - " + DateTime.Now.ToString("yyyyMMdd") + ".zip";
                    var zipStream = new MemoryStream();
                    var zip = new ZipArchive(zipStream, ZipArchiveMode.Create);

                    // Convert IEnumerable ke List
                    List<Invoice> data = result.Data.Cast<Invoice>().ToList();

                    // Initial variable
                    var IsPrint = true;
                    int totalData = data.Count;
                    int mvData = data.Where(x => x.business_unit_id == 1).Count();
                    // 
                    IsPrint = totalData == mvData ? false : true;

                    if (IsPrint)
                    {
                        foreach (Invoice inv in result.Data)
                        {
                            var id = inv.da_code;
                            if (inv.business_unit_id == 2) // others
                            {
                                using (new NetworkConnection(file, credential))
                                {
                                    var unit = inv.business_unit;
                                    var doc = Service<AttachmentService>().GetAttachment(id);
                                    if (doc.Count > 0)
                                    {
                                        var item = doc.FirstOrDefault();
                                        var filename = item.created_date.ToString("yyyyM") + "\\" + item.reference + "\\" + item.filename;
                                        var url = Path.Combine(file, filename);

                                        if (System.IO.File.Exists(url) == true)
                                        {
                                            zip.CreateEntryFromFile(url, "AttachmentInvoice - " + DateTime.Now.ToString("yyyyMMdd") + "\\" + "AttachmentInvoice - " + inv.invoice_number.Replace('/', '-') + "\\" + item.filename);
                                        }
                                        else
                                        {
                                            zip.CreateEntry("AttachmentInvoice - " + DateTime.Now.ToString("yyyyMMdd") + "\\" + "AttachmentInvoice - " + inv.invoice_number.Replace('/', '-') + "\\" + Path.GetFileNameWithoutExtension(item.filename) + ".NOTFOUND");
                                        }
                                    }
                                    else
                                    {

                                    }
                                }
                            }
                            else if (inv.business_unit_id == 3) // spare part
                            { 
                                //attchmentSparePart(username, domain, zip, inv);
                                string filename = Path.GetFileNameWithoutExtension(outputfile) + "\\" + "Attachment - " + inv.invoice_number.Replace("/", "-") + ".pdf";
                                List<AttachmentSpDebitAdvice> dataSource = new List<AttachmentSpDebitAdvice>();
                                if (inv.invoice_number.Contains("FDA")) //debit
                                {
                                    repPath = string.Format("{0}/{1}", ConfigurationManager.AppSettings["RDLPath"], ConfigurationManager.AppSettings["SpAttDebitAdvicePath"]);

                                    dataSource = Service<AttachmentService>().GetSpAttachment(inv.invoice_number, "", inv.doc_date.ToString("yyyy-MM-dd"));
                                }
                                else
                                {
                                    repPath = string.Format("{0}/{1}", ConfigurationManager.AppSettings["RDLPath"], ConfigurationManager.AppSettings["SpAttCreditAdvicePath"]);

                                    dataSource = Service<AttachmentService>().GetSpCreditAttachment(inv.invoice_number, "");
                                }   

                                var fileInArchive = zip.CreateEntry(filename);

                                fileBytes = SaveReportPdf(inv.invoice_number, out warnings, out mimeType, out encoding, out extension, repPath, out streamIds, report, dataSource);

                                using (var zipStreamOther = fileInArchive.Open())
                                if (fileBytes != null)
                                {
                                    using (var fileToCompressStream = new MemoryStream(fileBytes))
                                    {
                                        fileToCompressStream.CopyTo(zipStreamOther);
                                    }
                                }
                                
                            }
                        }

                        DownloadFinish();
                        zip.Dispose();
                        Response.Clear();
                        Response.AddHeader("Access-Control-Allow-Origin", "*");
                        Response.ContentType = "application/octet-stream";
                        Response.AddHeader("content-disposition", "attachment;filename=\"" + outputfile + "\"");
                        Response.BinaryWrite(zipStream.ToArray());
                        Response.Flush();
                        Response.End();
                    }
                    else
                    {
                        DownloadFinish();
                        throw new Exception("Invoice doesn't have attachment");

                    }
                
            }


            catch (Exception e)
            {
                DownloadFinish(); 
                throw new Exception(e.Message);
            }

        }

        //Not used!!
        //private void attchmentSparePart(string username, string domain, ZipArchive zip, Invoice inv)
        //{
        //    List<KeyValuePair<string, string>> urlPaths = new List<KeyValuePair<string, string>>();
        //    var username1 = ApplicationCacheManager.GetConfig<string>("DeliveryNoteUserName");
        //    var password1 = ApplicationCacheManager.GetConfig<string>("DeliveryNotePassword");
        //    var config1 = ApplicationCacheManager.GetConfig<string>("DeliveryNoteFolderTemp");
        //    var domain1 = ApplicationCacheManager.GetConfig<string>("DeliveryNoteDomain");
        //    var userdomain1 = domain + "\\" + username;

        //    var dir = Path.GetDirectoryName(config1);

        //    var pers = new Impersonation();
        //    pers.Impersonate(userdomain1, password1);
        //    var credential1 = new NetworkCredential(userdomain1, password1);

        //    using (new NetworkConnection(config1, credential1))
        //    {
        //        try
        //        {
        //            if (!Directory.Exists(dir))
        //                Directory.CreateDirectory(dir);
        //        }
        //        catch (Exception e)
        //        {
        //            throw e;
        //        }
        //        //file.SaveAs(newFileName);
        //    }

        //    var daNo = inv.da_no;
        //    string mimeType, encoding, extension, repPath = string.Empty;
        //    string[] streamIds;
        //    Warning[] warnings;
        //    LocalReport report = new LocalReport();
        //    repPath = string.Format("{0}/{1}", ConfigurationManager.AppSettings["RDLPath"], ConfigurationManager.AppSettings["SpAttDebitAdvicePath"]);
        //    //repPath = string.Format("{0}/{1}", ConfigurationManager.AppSettings["RDLPath"], "Report1.rdlc");

        //    //var inv = Service<InvoiceService>().GetInvoiceByDaNumber(daNumber);

        //    List<AttachmentSpDebitAdvice> dataSource = Service<AttachmentService>().GetSpAttachment(daNo, "", inv.doc_date.ToString("yyyy-MM-dd"));

        //    report.ReportPath = repPath;
        //    report.SetParameters(new ReportParameter("pDaNo", dataSource.First().DaNo));
        //    report.SetParameters(new ReportParameter("pDeliveryDate", dataSource.First().InvoiceDate.ToString("dd/MM/yyyy")));
        //    report.SetParameters(new ReportParameter("pCustomerName", dataSource.First().CustomerName));
        //    report.SetParameters(new ReportParameter("pCustomerAddress", dataSource.First().CustomerAddress));

        //    DataTable table = new DataTable();
        //    table.Columns.Add("InvoiceNo", type: typeof(string));
        //    table.Columns.Add("InvoiceDate", type: typeof(DateTime));
        //    table.Columns.Add("OT", type: typeof(int));
        //    table.Columns.Add("OrderNo", type: typeof(string));
        //    table.Columns.Add("Items", type: typeof(int));
        //    table.Columns.Add("GrossAmount", type: typeof(decimal));
        //    table.Columns.Add("NetAmount", type: typeof(decimal));

        //    foreach (AttachmentSpDebitAdvice a in dataSource)
        //    {
        //        DataRow row = table.NewRow();

        //        row["InvoiceNo"] = a.InvoiceNo;
        //        row["InvoiceDate"] = a.InvoiceDate;
        //        row["OT"] = a.OT;
        //        row["OrderNo"] = a.OrderNo;
        //        row["Items"] = a.Items;
        //        row["GrossAmount"] = a.GrossAmount;
        //        row["NetAmount"] = a.NetAmount;

        //        table.Rows.Add(row);
        //    }

        //    report.DataSources.Clear();
        //    report.DataSources.Add(new ReportDataSource("DataSetSpAttachment", table));

        //    report.Refresh();

        //    byte[] bytes = report.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

        //    var daReplaced = daNo.Replace("/", "-");

        //    var newFileNameTemp = daReplaced + "-" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + "." + extension;
        //    var newFileName = daReplaced + "." + extension;
        //    var daFileNameConcat = daReplaced + "\\" + newFileNameTemp;
        //    var combineFileName = config1 + "\\" + daFileNameConcat;
        //    var url = Path.Combine(combineFileName);

        //    var dir1 = Path.GetDirectoryName(url);
        //    if (!Directory.Exists(dir1))
        //        Directory.CreateDirectory(dir1);

        //    urlPaths.Add(new KeyValuePair<string, string>(url, daFileNameConcat));

        //    System.IO.File.WriteAllBytes(url, bytes);

        //    foreach (KeyValuePair<string, string> url1 in urlPaths)
        //    {
        //        zip.CreateEntryFromFile(url1.Key, url1.Value);
        //        try
        //        {
        //            System.IO.File.Delete(url1.Key);
        //        }
        //        catch (Exception e)
        //        {
        //            throw e;
        //        }
        //    }
        //}

        private byte[] SaveReportPdf(string daNumber, out Warning[] warnings, out string mimeType, out string encoding, out string extension, string repPath, out string[] streamIds, LocalReport report, List<AttachmentSpDebitAdvice> dataSource)
        {
            report.ReportPath = repPath;
            try
            {

                report.SetParameters(new ReportParameter("pDaNo", dataSource.First().DaNo));
                report.SetParameters(new ReportParameter("pDeliveryDate", dataSource.First().InvoiceDate.ToString("dd/MM/yyyy")));
                report.SetParameters(new ReportParameter("pCustomerName", dataSource.First().CustomerName));
                report.SetParameters(new ReportParameter("pCustomerAddress", dataSource.First().CustomerAddress));
            }
            catch (Exception)
            {
            }

            DataTable table = new DataTable();
            table.Columns.Add("InvoiceNo", type: typeof(string));
            table.Columns.Add("InvoiceDate", type: typeof(DateTime));
            table.Columns.Add("OT", type: typeof(string));
            table.Columns.Add("OrderNo", type: typeof(string));
            table.Columns.Add("Items", type: typeof(int));
            table.Columns.Add("GrossAmount", type: typeof(decimal));
            table.Columns.Add("NetAmount", type: typeof(decimal));

            foreach (AttachmentSpDebitAdvice a in dataSource)
            {
                DataRow row = table.NewRow();

                row["InvoiceNo"] = a.InvoiceNo;
                row["InvoiceDate"] = a.InvoiceDate;
                row["OT"] = a.OT;
                row["OrderNo"] = a.OrderNo;
                row["Items"] = a.Items;
                row["GrossAmount"] = a.GrossAmount;
                row["NetAmount"] = a.NetAmount;

                table.Rows.Add(row);
            }

            report.DataSources.Clear();
            report.DataSources.Add(new ReportDataSource("DataSetSpAttachment", table));

            report.Refresh();
            byte[] bytes=null;

            warnings = null;
            mimeType = null;
            encoding = null;
            extension = null;
            streamIds = null;

            try
            {
                bytes = report.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            }
            catch (Exception)
            { 
            }

            return bytes;
        }

        public void DownloadNotaRetur(string da_number, string business_unit, string invoicenumber)
        {
            try
            {
                string user = "";
                Invoice inv = new Invoice();
                if (!string.IsNullOrEmpty(da_number)) //&& !string.IsNullOrEmpty(user))
                {
                    if (business_unit.Equals("Others"))
                    {
                        
                        string repPath = string.Format("{0}/{1}", ConfigurationManager.AppSettings["RDLPath"], ConfigurationManager.AppSettings["NotaReturPath"]);

                        var dataSource = Service<NoturService>().GetPrintNotaRetur(new DataSourceRequest(), da_number, user);

                        string ca_number = dataSource.Select(s => s.ReturNumber).FirstOrDefault();

                        LocalReport report = new LocalReport();
                        report.ReportPath = repPath;
                        report.DataSources.Clear();
                        report.DataSources.Add(new ReportDataSource("DataSet1", dataSource));
                        report.Refresh();

                        Stream stream = new MemoryStream();
                        Warning[] warnings;
                        string mimeType, encoding, extension;
                        string[] streamIds;

                        byte[] bytes = report.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

                        Response.Buffer = true;
                        Response.Clear();
                        Response.ContentType = mimeType;
                        Response.AddHeader("content-disposition", "attachment; filename= " + "NotaRetur" + "-" + ca_number.Replace("/", "-") + "." + extension);
                        Response.OutputStream.Write(bytes, 0, bytes.Length); // create the file  
                        Response.Flush(); // send it to the client to download  
                        Response.End();
                        var Update = Service<NotaReturService>().GetUpdate(da_number);
                    }
                    else
                    {
                        throw new Exception("MV is Not Available");
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        
        //public FileResult CsvExport([DataSourceRequest] DataSourceRequest request)
        //{
        //    var columns = new string[] { "Invoice Number", "Transaction Type", "Doc date", "Due Date", "Currency", "DPP", "Tax Amount", "Total Amount", "Tax Invoice", "PPH 22 Amount", "PPH No", "AR Status" };
        //    var invoices = Service<InvoiceService>().GetViewDataSourceResult(request, Convert.ToInt16(EnumHelper.GetDescription(InvoiceType.Invoice)), SessionManager.RoleStr);

        //    MemoryStream ms = new MemoryStream();
        //    TextWriter csv = new StreamWriter(ms);

        //    var headerLine = "Invoice Number, Transaction Type, Doc Date, Due Date, Currency, DPP, Tax Amount, Total Amount, Tax Invoice, PPH 22 Amount, PPH No, AR Status";
        //    csv.WriteLine(headerLine);

        //    foreach(var inv in invoices.Data.Cast<Invoice>().ToList())
        //    {
        //        var newLine = inv.invoice_number + "," +
        //            inv.transaction_type + "," +
        //            inv.doc_date.ToString("dd-MM-yyyy") + "," +
        //            inv.due_date.ToString("dd-MM-yyyy") + "," +
        //            inv.currency + "," +
        //            inv.dpp.ToString("N").Replace(",",".") + "," +
        //            inv.tax.ToString("N").Replace(",", ".") + "," +
        //            inv.total_amount.ToString("N").Replace(",", ".") + "," +
        //            inv.tax_invoice + "," +
        //            inv.amount_pph.ToString("N").Replace(",", ".") + "," +
        //            inv.pph_no + "," +
        //            inv.ar_status_name;

        //        csv.WriteLine(newLine);
        //    }

        //    string saveAsFileName = string.Format("Invoice-{0:d}.csv", DateTime.Now).Replace("/", "-");

        //    byte[] bytes = ms.ToArray();
        //    return File(bytes, "text/csv", saveAsFileName);
        //}

        public FileResult Excel_Export([DataSourceRequest] DataSourceRequest request)
        {
            try
            {
                var strclmn = new string[] { "Customer Code", "Customer Name", "Invoice Number", "Transaction Type", "Doc date", "Due Date", "Currency", "DPP", "VAT Amount", "Dormitory", "VAT Dormitory", "Total Amount", "Tax Invoice", "PPH 22 Amount", "PPH No", "AR Status", "Keyword" };
                //var result = Service<InvoiceService>().GetViewDataSourceResult(request, Convert.ToInt16(EnumHelper.GetDescription(InvoiceType.Invoice)), SessionManager.RoleStr);
                //Fixing Enhan 20210407
                var result = Service<InvoiceService>().GetGridInitDataSourceResult(request, Convert.ToInt16(EnumHelper.GetDescription(InvoiceType.Invoice)), SessionManager.RoleStr);
                //END
                var config = Service<ParameterService>().GetParameter("limit_row").value1;

                List<Invoice> total = result.Data.Cast<Invoice>().ToList();

                // Initial variable
                // var IsPrint = true;
                int totalData = total.Count;

                // code to create workbook 
                if (totalData <= Convert.ToInt32(config))
                {
                    using (var exportData = new MemoryStream())
                    {
                        var workbook = new XSSFWorkbook();
                        var sheet = workbook.CreateSheet("Invoice");
                        var headerRow = sheet.CreateRow(0);

                        int i = 0;

                        foreach (string c in strclmn)
                        {

                            // Create New Cell
                            var headerCell = headerRow.CreateCell(i);

                            // Set Cell Value
                            headerCell.SetCellValue(c);

                            // Create Style
                            var headerCellStyle = workbook.CreateCellStyle();
                            headerCellStyle.FillForegroundColor = IndexedColors.Aqua.Index;
                            headerCellStyle.FillPattern = FillPattern.SolidForeground;

                            // Add Style to Cell
                            headerCell.CellStyle = headerCellStyle;

                            sheet.AutoSizeColumn(i);
                            i++;
                        }
                        //var data = Service<InvoiceService>().GetViewDataSourceResult(new DataSourceRequest(), 1, SessionManager.RoleStr);

                        i = 0;
						foreach (Invoice inv in result.Data)
						{
                            var content = sheet.CreateRow(i + 1);
                            content.CreateCell(0).SetCellValue(inv.customer_code);
                            content.CreateCell(1).SetCellValue(inv.customer_name);
                            content.CreateCell(2).SetCellValue(inv.invoice_number);
                            content.CreateCell(3).SetCellValue(inv.transaction_type);
                            content.CreateCell(4).SetCellValue(inv.doc_date.ToString("dd/MM/yyyy"));
                            content.CreateCell(5).SetCellValue(inv.due_date.ToString("dd/MM/yyyy"));
                            content.CreateCell(6).SetCellValue(inv.currency);
                            if (inv.business_unit != "Spare Part" && inv.is_retur)
                            {
                                content.CreateCell(7).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0}", inv.dpp));
                                content.CreateCell(8).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0}", inv.tax));
                                content.CreateCell(9).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0}", Math.Floor((inv.dormitory * -1))));
                                content.CreateCell(10).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0}", Math.Floor((inv.vat_dormitory * -1))));
                                content.CreateCell(11).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0}", inv.total_amount));

                                //content.CreateCell(7).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0:N0}", inv.dpp));
                                //content.CreateCell(8).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0:N0}", inv.tax));
                                //content.CreateCell(9).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0:N0}", inv.dormitory * -1));
                                //content.CreateCell(10).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0:N0}", inv.vat_dormitory * -1));
                                //content.CreateCell(11).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0:N0}", inv.total_amount));

                                //content.CreateCell(7).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0:N0}", inv.dpp * -1));
                                //content.CreateCell(8).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0:N0}", inv.tax * -1));
                                //content.CreateCell(9).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0:N0}", inv.dormitory * -1));
                                //content.CreateCell(10).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0:N0}", inv.vat_dormitory * -1));
                                //content.CreateCell(11).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0:N0}", inv.total_amount * -1));
                            }
                            else
                            {
                                //content.CreateCell(7).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0:N0}", inv.dpp));
                                //content.CreateCell(8).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0:N0}", inv.tax));
                                //content.CreateCell(9).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0:N0}", inv.dormitory));
                                //content.CreateCell(10).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0:N0}", inv.vat_dormitory));
                                //content.CreateCell(11).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0:N0}", inv.total_amount));

                                content.CreateCell(7).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0}", inv.dpp));
                                content.CreateCell(8).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0}", inv.tax));
                                content.CreateCell(9).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0}", Math.Floor(inv.dormitory)));
                                content.CreateCell(10).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0}", Math.Floor(inv.vat_dormitory))); 
                                content.CreateCell(11).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0}", inv.total_amount));
                            }
                            content.CreateCell(12).SetCellValue(inv.tax_invoice);
                            if (inv.business_unit != "Spare Part")
                            {
                                //content.CreateCell(13).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0:N0}", inv.amount_pph));
                                content.CreateCell(13).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0}", inv.amount_pph));
                            }
                            else
                            {
                                content.CreateCell(13).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0}", inv.amount_pph * -1));
                                //content.CreateCell(13).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0:N0}", inv.amount_pph * -1));
                            }
                            content.CreateCell(14).SetCellValue(inv.pph_no);
                            content.CreateCell(15).SetCellValue(inv.ar_status_name);
                            content.CreateCell(16).SetCellValue(inv.keyword);

                            i++;
                        }
                        workbook.Write(exportData);
                        string saveAsFileName = string.Format("Invoice-{0:d}.xlsx", DateTime.Now).Replace("/", "-");

                        byte[] bytes = exportData.ToArray();
                        DownloadFinish();
                        return File(bytes, "application/vnd.ms-excel", saveAsFileName);
                    }
                }

                else
                {
                    DownloadFinish();
                    throw new Exception("Export Excel Failed, Total row can not more than " + Convert.ToInt32(config));
                }
            }
            catch(Exception ex)
            {
                DownloadFinish();
                throw new Exception(ex.Message);
            }
        }
        
        public ActionResult GetInvoice(string invoice_no)
        {
            var request = new DataSourceRequest();
            request.Page = 0;
            request.PageSize = 0;
            var data = Service<InvoiceService>().GetInvoices(SessionManager.RoleStr, invoice_no).FirstOrDefault();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public void RemoveSQLInjectString(string strWords)
        {
            strWords = Regex.Replace(strWords, @"[^\u0000-\u007F]+", string.Empty);

            bool isError = false;
            List<string> badChars = new List<string>() { "select", "drop", ";", "--", "insert", "delete", "xp_", "¿" };

            for (int i = 0; i < badChars.Count(); i++)
            {
                if (strWords.Contains(badChars[i]))
                    isError = true;
                    break;
            }

            if (isError)
                throw new Exception("Invalid character detected :  " + strWords + "");
        }

        public void DownloadInvoiceSparepart(string daNumber)
        {
            Stream stream = new MemoryStream();
            Warning[] warnings;
            string mimeType, encoding, extension, repPath = string.Empty;
            string[] streamIds;
            LocalReport report = new LocalReport();
            ReportViewer reportViewer = new ReportViewer();

            //QRCodeGenerator qrGenerator = new QRCodeGenerator();
            //QRCodeData qrCodeData = qrGenerator.CreateQrCode("The text which should be encoded.", QRCodeGenerator.ECCLevel.Q);
            //QRCoder.QRCode qrCode = new QRCoder.QRCode(qrCodeData);
            //Bitmap qrCodeImage = qrCode.GetGraphic(20);

            try
            {
                RemoveSQLInjectString(daNumber);

                string user = SessionManager.Current;
                string userid = "";
                if (user != "TAM\\Mayumi" || user != "TAM\\frans" || user != "TAM\\ronnyk102181" || user != "TAM\\indah.gustiana" || user != "TAM\\aliandy.syahriel")
                {
                    user = userid;
                }
                else
                {
                    user = SessionManager.Current;
                }
                string name = SessionManager.Name;
                string nameid = "";
                if (name != "Mayumi Eguchi" || name != "Frans lhutan B" || name != "Ronny Kusgianto" || name != "Indah Sari Gustiana" || name != "Aliandy Syahriel")
                {
                    name = nameid;
                }
                else
                {
                    name = SessionManager.Name;
                }
                Infrastructure.Barcode.Barcode barcode = new Infrastructure.Barcode.Barcode();

                if (!string.IsNullOrEmpty(daNumber))// && !string.IsNullOrEmpty(user))
                {


                    List<SpDebitCreditAdvice> dataSource = new List<SpDebitCreditAdvice>();
                    if (daNumber.Contains("FCA")) //credit
                    {
                        Tam.Ebilling.Reporting.Report getReportByte = new Tam.Ebilling.Reporting.Report();
                        byte[] bytes = getReportByte.GetSpCreditAdviceReport(daNumber, user, out extension, out mimeType);

                        Response.Buffer = true;
                        Response.Clear();
                        Response.ContentType = mimeType;
                        Response.AddHeader("content-disposition", "attachment; filename= " + daNumber.Replace("/", "-") + "." + extension);
                        Response.OutputStream.Write(bytes, 0, bytes.Length); // create the file  
                        Response.Flush(); // send it to the client to download  
                        Response.End();
                        var Update = Service<InvoiceService>().UpdateSparePartStatusDownload(
                                    daNumber,
                                    user,
                                    EnumHelper.GetDescription(DownloadType.InvoiceQR),
                                    EnumHelper.GetDescription(InvoiceType.NotaRetur)
                                );
                        //dataSource = Service<NoturService>().GetSpDebitAdvice(new DataSourceRequest(), daNumber, user);
                        //printCreditAdvice(daNumber, out warnings, out mimeType, out encoding, out extension, out repPath, out streamIds, report, user, barcode, dataSource);
                    }
                    else //debit
                    {
                        //dataSource = Service<NoturService>().GetSpDebitAdvice(new DataSourceRequest(), daNumber, user);
                        //printDebitAdvice(daNumber, out warnings, out mimeType, out encoding, out extension, out repPath, out streamIds, report, user, barcode, dataSource);
                        Tam.Ebilling.Reporting.Report getReportByte = new Tam.Ebilling.Reporting.Report();
                        byte[] bytes = getReportByte.GetSpDebitAdviceReport(daNumber, user, out extension, out mimeType);

                        Response.Buffer = true;
                        Response.Clear();
                        Response.ContentType = mimeType;
                        Response.AddHeader("content-disposition", "attachment; filename= " + daNumber.Replace("/", "-") + "." + extension);
                        Response.OutputStream.Write(bytes, 0, bytes.Length); // create the file  
                        Response.Flush(); // send it to the client to download  
                        Response.End();
                        var Update = Service<InvoiceService>().UpdateSparePartStatusDownload(
                            daNumber,
                            user,
                            EnumHelper.GetDescription(DownloadType.InvoiceQR),
                            EnumHelper.GetDescription(InvoiceType.Invoice)
                            );
                    }

                    

                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void printDebitAdvice(string daNumber, out Warning[] warnings, out string mimeType, out string encoding, out string extension, out string repPath, out string[] streamIds, LocalReport report, string user, Infrastructure.Barcode.Barcode barcode, List<SpDebitCreditAdvice> dataSource)
        {
            repPath = string.Format("{0}/{1}", ConfigurationManager.AppSettings["RDLPath"], ConfigurationManager.AppSettings["SpDebitAdvicePath"]);

            var inv = Service<InvoiceService>().GetSingleInvoiceByInvoiceNumber(EnumHelper.GetDescription(InvoiceType.Invoice), daNumber);

            var queryString = string.Format("invoice/getsingleinvoice?invoice_number={0}&customer_group={1}", inv.invoice_number, inv.customer_group);
            var apiUrl = string.Format("{0}/{1}", ConfigurationManager.AppSettings["APIUrl"], queryString);


            report.ReportPath = repPath;
            report.SetParameters(new ReportParameter("pDaNo", dataSource.First().DaNo));
            report.SetParameters(new ReportParameter("pDeliveryDate", dataSource.First().DeliveryDate.ToString("dd/MM/yyyy")));
            report.SetParameters(new ReportParameter("pCustomerName", dataSource.First().CustomerName));
            report.SetParameters(new ReportParameter("pCustomerAddress", dataSource.First().CustomerAddress));
            report.SetParameters(new ReportParameter("pFakturPajakNo", dataSource.First().FakturPajakNo));
            report.SetParameters(new ReportParameter("pTurnOverAmount", dataSource.First().TurnOverAmount == 0 ? string.Empty : String.Format("{0:N}", dataSource.First().TurnOverAmount)));
            report.SetParameters(new ReportParameter("p10Vat", dataSource.First().Vat10 == 0 ? string.Empty : String.Format("{0:N}", dataSource.First().Vat10)));
            report.SetParameters(new ReportParameter("pTotal", dataSource.First().Total == 0 ? string.Empty : String.Format("{0:N}", dataSource.First().Total)));
            report.SetParameters(new ReportParameter("pDueDateTo", dataSource.First().DueDateTo.ToString("dd/MM/yyyy")));
            report.SetParameters(new ReportParameter("pDueDateVat", dataSource.First().DueDateVat.ToString("dd/MM/yyyy")));
            report.SetParameters(new ReportParameter("pCollectionDate", dataSource.First().CollectionDate.ToString("dd/MM/yyyy")));
            report.SetParameters(new ReportParameter("pQrCode", barcode.GenerateQrCode(apiUrl)));
            report.DataSources.Clear();
            report.DataSources.Add(new ReportDataSource("DataSet2", dataSource));

            report.Refresh();

            byte[] bytes = report.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            Response.Buffer = true;
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("content-disposition", "attachment; filename= " + daNumber.Replace("/", "-") + "." + extension);
            Response.OutputStream.Write(bytes, 0, bytes.Length); // create the file  
            Response.Flush(); // send it to the client to download  
            Response.End();
            var Update = Service<InvoiceService>().UpdateSparePartStatusDownload(
                daNumber,
                user,
                EnumHelper.GetDescription(DownloadType.InvoiceQR),
                EnumHelper.GetDescription(InvoiceType.Invoice)
                );
        }

        private void printCreditAdvice(string daNumber, out Warning[] warnings, out string mimeType, out string encoding, out string extension, out string repPath, out string[] streamIds, LocalReport report, string user, Infrastructure.Barcode.Barcode barcode, List<SpDebitCreditAdvice> dataSource)
        {
            repPath = string.Format("{0}/{1}", ConfigurationManager.AppSettings["RDLPath"], ConfigurationManager.AppSettings["SpCreditAdvicePath"]);

            report.ReportPath = repPath;
            report.SetParameters(new ReportParameter("pDaNo", dataSource.First().DaNo));
            report.SetParameters(new ReportParameter("pDeliveryDate", dataSource.First().DeliveryDate.ToString("dd/MM/yyyy")));
            report.SetParameters(new ReportParameter("pCustomerName", dataSource.First().CustomerName));
            report.SetParameters(new ReportParameter("pCustomerAddress", dataSource.First().CustomerAddress));
            report.SetParameters(new ReportParameter("pFakturPajakNo", dataSource.First().FakturPajakNo));
            report.SetParameters(new ReportParameter("pTurnOverAmount", dataSource.First().TurnOverAmount == 0 ? string.Empty : String.Format("{0:N}", dataSource.First().TurnOverAmount)));
            report.SetParameters(new ReportParameter("p10Vat", dataSource.First().Vat10 == 0 ? string.Empty : String.Format("{0:N}", dataSource.First().Vat10)));
            report.SetParameters(new ReportParameter("pTotal", dataSource.First().Total == 0 ? string.Empty : String.Format("{0:N}", dataSource.First().Total)));
            report.SetParameters(new ReportParameter("pDueDateTo", dataSource.First().DueDateTo.ToString("dd/MM/yyyy")));
            report.SetParameters(new ReportParameter("pDueDateVat", dataSource.First().DueDateVat.ToString("dd/MM/yyyy")));
            report.SetParameters(new ReportParameter("pCollectionDate", dataSource.First().CollectionDate.ToString("dd/MM/yyyy")));
            
            report.DataSources.Clear();
            report.DataSources.Add(new ReportDataSource("DataSet2", dataSource));

            report.Refresh();

            byte[] bytes = report.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            Response.Buffer = true;
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("content-disposition", "attachment; filename= " + daNumber.Replace("/", "-") + "." + extension);
            Response.OutputStream.Write(bytes, 0, bytes.Length); // create the file  
            Response.Flush(); // send it to the client to download  
            Response.End();
            var Update = Service<InvoiceService>().UpdateSparePartStatusDownload(
                        daNumber,
                        user,
                        EnumHelper.GetDescription(DownloadType.InvoiceQR),
                        EnumHelper.GetDescription(InvoiceType.NotaRetur)
                    );
        }
    }
}
