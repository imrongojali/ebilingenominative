﻿using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tam.Ebilling.Domain;
using Tam.Ebilling.Infrastructure.Mvc;
using Tam.Ebilling.Service;

namespace Tam.Ebilling.Web.Areas.Core.Controllers
{
    public class CustomerController : WebControllerBase
    {
        // GET: Core/Log
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Read([DataSourceRequest] DataSourceRequest request)
        {
            var result = Service<CustomerService>().GetDataSourceResult(request);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CustomerDetail([DataSourceRequest] DataSourceRequest request, string customer_code, int business_unit_id)
        {
            var cust = Service<CustomerService>().GetCustomer(customer_code, business_unit_id.ToString());

            Customer empObj = new Customer();
            empObj.customer_code = cust.customer_code;
            empObj.business_unit_id = cust.business_unit_id;
            empObj.customer_name = cust.customer_name;
            empObj.customer_group = cust.customer_group;

            return PartialView("~/Areas/Core/Views/Customer/Form/CustomerModal.cshtml", empObj);
        }

        public ActionResult Save(Customer customer)
        {
            var result = Service<CustomerService>().Save(customer);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}