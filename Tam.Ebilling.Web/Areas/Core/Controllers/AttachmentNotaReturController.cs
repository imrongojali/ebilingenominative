﻿using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Tam.Ebilling.Domain;
using Tam.Ebilling.Infrastructure.Adapter;
using Tam.Ebilling.Infrastructure.Cache;
using Tam.Ebilling.Infrastructure.Helper;
using Tam.Ebilling.Infrastructure.Mvc;
using Tam.Ebilling.Service;

namespace Tam.Ebilling.Web.Areas.Core.Controllers
{
    public class AttachmentNotaReturController : WebControllerBase
    {
        public ActionResult NotaReturUpload(string da_no)
        {
            return PartialView("~/Areas/Core/Views/NotaRetur/Form/NotaReturUpload.cshtml");
        }
        public ActionResult Submit(IEnumerable<HttpPostedFileBase> files)
        {
            if (files != null)
            {
                TempData["UploadedFiles"] = Get_File_Info(files);
            }

            return RedirectToAction("Result");
        }
        public ActionResult Result()
        {
            return View();
        }

        private IEnumerable<string> Get_File_Info(IEnumerable<HttpPostedFileBase> files)
        {
            return
                from a in files
                where a != null
                select string.Format("{0} ({1} bytes)", Path.GetFileName(a.FileName), a.ContentLength);
        }

        [HttpPost]
        public ActionResult Read([DataSourceRequest] Kendo.Mvc.UI.DataSourceRequest request)
        {
            var result = Service<AttachmentNotaReturService>().GetDataSourceResult(request);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public void Temp()
        {
            var username = ApplicationCacheManager.GetConfig<string>("FileUploadUserName");
            var password = ApplicationCacheManager.GetConfig<string>("FileUploadPassword");
            var domain = ApplicationCacheManager.GetConfig<string>("FileUploadDomain");
            var userdomain = domain + "\\" + username;
            var pers = new Impersonation();
            pers.Impersonate(userdomain, password);
            var credential = new NetworkCredential(userdomain, password);

            //var uploadFolder = "E:\\NotaRetur";
            
            var uploadFolder = ApplicationCacheManager.GetConfig<string>("FileUploadTemp");
            //var uploadFolder1 = ApplicationCacheManager.GetConfig<string>("FileUploadTempTTD");
            using (new NetworkConnection(uploadFolder, credential))
                {
                    //var filepath = GetQueryString<string>("filepath").ToString();
                    var filename = GetQueryString<string>("filename").ToString();

                    var url = Path.Combine(uploadFolder, filename);

                    Response.Clear();
                    Response.AddHeader("Access-Control-Allow-Origin", "*");
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("content-disposition", "attachment;filename=" + Path.GetFileName(filename));
                    Response.WriteFile(url);
                    Response.Flush();
                    Response.End();
                }
            //else if(doctype == "NoturSignOff")
            //{
            //    var uploadFolder = ApplicationCacheManager.GetConfig<string>("FileUploadTempTTD");
            //    using (new NetworkConnection(uploadFolder, credential))
            //    {
            //        //var filepath = GetQueryString<string>("filepath").ToString();
            //        var filename = GetQueryString<string>("filename").ToString();

            //        var url = Path.Combine(uploadFolder, filename);

            //        Response.Clear();
            //        Response.AddHeader("Access-Control-Allow-Origin", "*");
            //        Response.ContentType = "application/octet-stream";
            //        Response.AddHeader("content-disposition", "attachment;filename=" + Path.GetFileName(filename));
            //        Response.WriteFile(url);
            //        Response.Flush();
            //        Response.End();
            //    }
            //}
           
        }

        #region TEMP TTD & DGT AD 2021
        public void TempTTD()
        {
            var username = ApplicationCacheManager.GetConfig<string>("FileUploadUserName");
            var password = ApplicationCacheManager.GetConfig<string>("FileUploadPassword");
            var domain = ApplicationCacheManager.GetConfig<string>("FileUploadDomain");
            var userdomain = domain + "\\" + username;
            var pers = new Impersonation();
            pers.Impersonate(userdomain, password);
            var credential = new NetworkCredential(userdomain, password);
            
            //var uploadFolder = "E:\\NotaRetur";
            var uploadFolder = ApplicationCacheManager.GetConfig<string>("FileUploadTempTTD");
            using (new NetworkConnection(uploadFolder, credential))
            {
                //var filepath = GetQueryString<string>("filepath").ToString();
                var filename = GetQueryString<string>("filename").ToString();

                var url = Path.Combine(uploadFolder, filename);

                Response.Clear();
                Response.AddHeader("Access-Control-Allow-Origin", "*");
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("content-disposition", "attachment;filename=" + Path.GetFileName(filename));
                Response.WriteFile(url);
                Response.Flush();
                Response.End();
            }
        }

        public void TempDGT()
        {
            var username = ApplicationCacheManager.GetConfig<string>("FileUploadUserName");
            var password = ApplicationCacheManager.GetConfig<string>("FileUploadPassword");
            var domain = ApplicationCacheManager.GetConfig<string>("FileUploadDomain");
            var userdomain = domain + "\\" + username;
            var pers = new Impersonation();
            pers.Impersonate(userdomain, password);
            var credential = new NetworkCredential(userdomain, password);
            //var uploadFolder = "E:\\NotaRetur";
            var uploadFolder = ApplicationCacheManager.GetConfig<string>("FileUploadTempDGT");
            using (new NetworkConnection(uploadFolder, credential))
            {
                //var filepath = GetQueryString<string>("filepath").ToString();
                var filename = GetQueryString<string>("filename").ToString();

                var url = Path.Combine(uploadFolder, filename);

                Response.Clear();
                Response.AddHeader("Access-Control-Allow-Origin", "*");
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("content-disposition", "attachment;filename=" + Path.GetFileName(filename));
                Response.WriteFile(url);
                Response.Flush();
                Response.End();
            }
        }

        #endregion

        public ActionResult UploadFileInitial(string reference, string businessType)
        {
            //var Model = Service<AttachmentNotaReturService>().GetAttachment(reference);
            var listDocument = Service<AttachmentNotaReturService>().GetNotaReturAttachment(reference);

            ViewBag.ListDocument = listDocument;
            ViewBag.Reference = reference;
            ViewBag.BusinessType = businessType;
            //ViewBag.Reference = reference;

            var url = "~/Areas/Core/Views/NotaRetur/Form/NotaReturUpload.cshtml";
            return PartialView(url);
        }

        #region Upload TTD and DGT 2020 AD
        public ActionResult UploadFileInitialTTDDGT(string reference, string businessType)
        {
            //var Model = Service<AttachmentNotaReturService>().GetAttachment(reference);
            var listDocument = Service<AttachmentNotaReturService>().GetNotaReturAttachment(reference);
            //var caNumber = Service<InvoiceService>().GetViewDataSourceResultVAT(2, "");
            //ViewBag.CaNumber = caNumber;
            ViewBag.ListDocument = listDocument;
            ViewBag.Reference = reference;
            ViewBag.BusinessType = businessType;
            //ViewBag.Reference = reference;

            var url = "~/Areas/Core/Views/NotaRetur/Form/NotaReturUploadTTDDGT.cshtml";
            return PartialView(url);
        }
        #endregion
        public JsonResult AllocationRead(string da_no)
        {
            var result = Service<AttachmentNotaReturService>().GetNotaReturAttachment(da_no);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public void Document()
        {
            var username = ApplicationCacheManager.GetConfig<string>("FileUploadUserName");
            var password = ApplicationCacheManager.GetConfig<string>("FileUploadPassword");
            var domain = ApplicationCacheManager.GetConfig<string>("FileUploadDomain");
            var userdomain = domain + "\\" + username;
            var pers = new Impersonation();
            pers.Impersonate(userdomain, password);
            var credential = new NetworkCredential(userdomain, password);
            //var uploadFolder = "E:\\BES\\Attachment\\NotaRetur";
            //TES AD
            var idtype = GetQueryString<string>("id");
            var doctype = Service<AttachmentNotaReturService>().GetDocumentByID(idtype.ToString()).FirstOrDefault();
            if(doctype.document_type == "Notur")
            {
                var uploadFolder = ApplicationCacheManager.GetConfig<string>("FileUpload");
                using (new NetworkConnection(uploadFolder, credential))
                {
                    var id = GetQueryString<string>("id");
                    var doc = Service<AttachmentNotaReturService>().GetDocumentByID(id.ToString()).FirstOrDefault();

                    var url = Path.Combine(uploadFolder, doc.filename);

                    Response.Clear();
                    Response.AddHeader("Access-Control-Allow-Origin", "*");
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("content-disposition", "attachment;filename=" + Path.GetFileName(doc.filename));
                    Response.WriteFile(url);
                    Response.Flush();
                    Response.End();
                }
            }
            else if(doctype.document_type == "NoturSignOff")
            {
                var uploadFolder = ApplicationCacheManager.GetConfig<string>("FileUploadCreditnoteTTD");
                using (new NetworkConnection(uploadFolder, credential))
                {
                    var id = GetQueryString<string>("id");
                    var doc = Service<AttachmentNotaReturService>().GetDocumentByID(id.ToString()).FirstOrDefault();

                    var url = Path.Combine(uploadFolder, doc.filename);

                    Response.Clear();
                    Response.AddHeader("Access-Control-Allow-Origin", "*");
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("content-disposition", "attachment;filename=" + Path.GetFileName(doc.filename));
                    Response.WriteFile(url);
                    Response.Flush();
                    Response.End();
                }
            }
            else if (doctype.document_type == "NoturDGT")
            {
                var uploadFolder = ApplicationCacheManager.GetConfig<string>("FileUploadDGT");
                using (new NetworkConnection(uploadFolder, credential))
                {
                    var id = GetQueryString<string>("id");
                    var doc = Service<AttachmentNotaReturService>().GetDocumentByID(id.ToString()).FirstOrDefault();

                    var url = Path.Combine(uploadFolder, doc.filename);

                    Response.Clear();
                    Response.AddHeader("Access-Control-Allow-Origin", "*");
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("content-disposition", "attachment;filename=" + Path.GetFileName(doc.filename));
                    Response.WriteFile(url);
                    Response.Flush();
                    Response.End();
                }
            }
            //END


        }

        public void DocumentSpNotaRetur(string caNumber)
        {
            var username = ApplicationCacheManager.GetConfig<string>("FileSparePartUserName");
            var password = ApplicationCacheManager.GetConfig<string>("FileSparePartPassword");
            var domain = ApplicationCacheManager.GetConfig<string>("FileSparePartDomain");
            var userdomain = domain + "\\" + username;
            var pers = new Impersonation();
            pers.Impersonate(userdomain, password);
            var credential = new NetworkCredential(userdomain, password);
            //var uploadFolder = "E:\\BES\\Attachment\\NotaRetur";

            var uploadFolder = ApplicationCacheManager.GetConfig<string>("FileSparePart");
            using (new NetworkConnection(uploadFolder, credential))
            {
                //var doc = Service<AttachmentNotaReturService>().GetNotaReturDocumentByID(id.ToString()).FirstOrDefault();
                var docs = Service<AttachmentNotaReturService>().GetSpNotaReturAttachment(caNumber);
                var outputfile = "NotaReturUploaded - " + DateTime.Now.ToString("yyyyMMdd") + ".zip";

                using (var memoryStream = new MemoryStream())
                {
                    using (var ziparchive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
                    {
                        foreach (AttachmentSpNotaRetur notur in docs)
                        {
                            var url = Path.Combine(uploadFolder, notur.filename);
                            if (System.IO.File.Exists(url) == true)
                            {
                                //var fileInArchive = ziparchive.CreateEntry(namapath);
                            }
                        }
                    }
                }

                
                
                    //Response.Clear();
                    //Response.AddHeader("Access-Control-Allow-Origin", "*");
                    //Response.ContentType = "application/octet-stream";
                    //Response.AddHeader("content-disposition", "attachment;filename=" + Path.GetFileName(doc.filename));
                    //Response.WriteFile(url);
                    //Response.Flush();
                    //Response.End();
            }
        }
        [HttpPost]
        public ActionResult GetListDocuments(string reff)
        {
            ViewBag.Reference = reff;
            var result = Service<AttachmentNotaReturService>().GetDocumentFileByReference(ViewBag.Reference);

            var ds = new DataSourceResult
            {
                Data = result
            };
            return Json(ds, JsonRequestBehavior.AllowGet);
        }
        //[HttpPost]
        //[AppAuthorize("App.NotaReturManagement.AttachmentNotaRetur.Save")]
        public JsonResult SaveUploadInitial(string reff,string businessType,string documenttype ,IEnumerable<AttachmentNotaRetur> Docs)
        {
            string msgerror = "";
            //bool result = false;
            //if (businessType != "Spare Part")
            //{
             ResultUpload  result = Service<AttachmentNotaReturService>().SaveUpload(reff, Docs, documenttype,out msgerror);
            //}
            //else
            //{
            //    result = Service<AttachmentNotaReturService>().SaveUploadSpCaRetur(reff, Docs, out msgerror);
            //}
            return Json(new { results = result, MessageError = msgerror }, JsonRequestBehavior.AllowGet);
        }

        #region DGT & TTD ALL
        public JsonResult SaveUploadInitialTtdDgt(string reff, string businessType, string documenttype, IEnumerable<AttachmentNotaRetur> Docs)
        {
            string msgerror = "";
            //bool result = false;
           // ResultUpload result = 
            //if (businessType != "Spare Part")
            //{
            ResultUpload result = Service<AttachmentNotaReturService>().SaveUploadTtdDgtAll(reff, Docs, documenttype, out msgerror);
            //}
            //else
            //{
            //    result = Service<AttachmentNotaReturService>().SaveUploadSpCaRetur(reff, Docs, out msgerror);
            //}
            return Json(new { results = result, MessageError = msgerror }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public JsonResult Upload(string reff, string TypeDoc)
        {
            string msgerror = "";

            var files = Request.Files;
            var arrayObjects = new List<Tam.Ebilling.Domain.AttachmentNotaRetur>();
            var uploadService = Service<AttachmentNotaReturService>();

            if (files != null && files.Count > 0)
            {
                for (var i = 0; i < files.Count; i++)
                {
                    var file = files[i];

                    var doc = uploadService.Upload(new HttpPostedFileBaseAdapter(file), reff, TypeDoc, out msgerror);
                    arrayObjects.Add(doc);
                }
            }
            return Json(new { Result = arrayObjects, MessageError = msgerror }, JsonRequestBehavior.AllowGet);
        }
    }
}

