﻿using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tam.Ebilling.Domain;
using Tam.Ebilling.Infrastructure.Session;
using Tam.Ebilling.Service;
using System.Net;
using Tam.Ebilling.Infrastructure.Cache;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;

using System.Globalization;
using System.Data;

using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Script.Serialization;
using NPOI.SS.Util;
using System.Net.Mail;
using System.Text;
using System.Web.UI;
using System.Configuration;

namespace Tam.Ebilling.Web.Areas.Core.Controllers
{
    public class PVMonitoringController : CommonFeatureController
    {
        public const int DATA_ROW_INDEX_START = 6;

        #region Index
        public ActionResult Index()
        {
            if (!IsRoleAllowedAccessMenu(SessionManager.UserType, SessionManager.Current))
            {
                return RedirectToAction("AccessDenied", "Error", new { area = "" });
            }

            var cust = SessionManager.RoleStr;
            ViewBag.Customer = cust;

            var userType = SessionManager.UserType;
            ViewBag.UserType = userType;

            var status = Service<PVMonitoringService>().GetStatusCmb(userType, "pv_status", SessionManager.LevelCode);

            if (SessionManager.LevelCode == "BG")
            {
                ViewBag.StatusSelected = "1";                
            }
            else if (SessionManager.LevelCode == "MD")
            {
                ViewBag.StatusSelected = "2";
            }
            else
            {
                ViewBag.StatusSelected = "0";
            }

            ViewBag.Status = status;

            var divId = Service<PVMonitoringService>().GetDivId(SessionManager.Current);
            ViewBag.DivId = divId;

            ViewBag.IsAllowedUploadLetterDisburs = IsAllowed("upload_lod");
            ViewBag.IsAllowedDownloadTemplate = IsAllowed("download_template");
            ViewBag.IsAllowedVerified = IsAllowed("verified_pic");
            ViewBag.IsAllowedNotProper = IsAllowed("notproper_bg");
            ViewBag.IsAllowedDelete = IsAllowed("delete_lod");

            return View();
        }
        #endregion

        #region Read
        [HttpPost]
        public ActionResult Read([DataSourceRequest] Kendo.Mvc.UI.DataSourceRequest request, string statusFlag)
        {
            string x = statusFlag.ToString();
            var result = Service<PVMonitoringService>().GetDataSourceResult(request, SessionManager.RoleStr, SessionManager.UserType, SessionManager.LevelCode, x);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Modal Initial
        [HttpPost]
        public ActionResult UploadFileInitial(string pvno, string processId, string isCheck)
        {
            var url = "";
            if (isCheck != "Y")
            {
                var userType = SessionManager.UserType;
                ViewBag.UserType = userType;

                ViewBag.BankAccountSelected = Service<BankAccountService>().GetBankAccount(processId, userType);
                ViewBag.BankAccount = Service<BankAccountService>().GetBankAccount(SessionManager.RoleStr, SessionManager.UserType, string.Empty);

                List<PVMonitoringAttachment> listDocument = Service<PVMonitoringService>().GetDocumentFileByReference(processId, userType).ToList();
                ViewBag.ListDocument = listDocument;

                List<PVMonitoring> listDtl = Service<PVMonitoringService>().GetPVMonitoring(int.Parse(processId)).ToList();
                ViewBag.ListDetail = listDtl;

                List<ComboEx> x = Service<PVMonitoringService>().GetVendorByInvoice(processId, false);
                ViewBag.VenCode = x.Select(p => p.CODE).ToArray().First();
                ViewBag.VenName = x.Select(p => p.NAME).ToArray().First();

                ViewBag.Invoice = pvno;
                ViewBag.ProcessID = processId;

                var transactionType = Service<PVMonitoringService>().GetStatusCmb(string.Empty, "pv_attachment_type", string.Empty);
                ViewBag.TransactionType = transactionType;

                url = "~/Areas/Core/Views/PVMonitoring/Form/_PVMonitoringUploadByDealer.cshtml";
            }
            else {

                ViewBag.PvNo = pvno;

                List<PVHDistribusionStatus> listDocument = Service<PVMonitoringService>().GetDistributionStatusByReference(pvno).ToList();
                ViewBag.ListDocument = listDocument;

                url = "~/Areas/Core/Views/PVMonitoring/Form/_PVMonitoringCheck.cshtml";
            }

            return PartialView(url);
        }

        [HttpPost]
        public ActionResult PVHeaderInitial(int pid, int pvno, string stsbugdet, string pvbudget)
        {
            List<PVMonitoring> x = Service<PVMonitoringService>().GetPVMonitoring(pid).ToList();
            List<PVMonitoring> listDtl = new List<PVMonitoring>();
            if (x.Count > 0)
            {
                foreach (PVMonitoring xz in x)
                {
                    xz.IS_USERTAM = SessionManager.UserType == "Internal" ? "Y" : "N";
                    listDtl.Add(xz);
                }
            }
            ViewBag.ListDetail = listDtl;

            var xx = listDtl
                    .Select(cc => new { cc.STATUS_FLAG, cc.STATUS_DESC })
                    .First();

            if (xx.STATUS_FLAG == "6" || xx.STATUS_FLAG == "7"){
                ViewBag.IsRejected = "Y";
                ViewBag.StatusDesc = xx.STATUS_DESC;
                ViewBag.Pvstatus = xx.STATUS_FLAG;
                PVHeader pvHeader = Service<PVMonitoringService>().GetPVHeader(pid);
                ViewBag.RejectedReason = pvHeader.REJECTED_REASON;
                ViewBag.RejectedBy = pvHeader.REJECTED_BY;
                ViewBag.RejectedDt = pvHeader.REJECTED_DT;
            }
            else {
                ViewBag.IsRejected = "N";
                ViewBag.StatusDesc = "";
                ViewBag.Pvstatus = xx.STATUS_FLAG;
                ViewBag.RejectedReason = "";
                ViewBag.RejectedBy = "";
                ViewBag.RejectedDt = "";
            }

            List<PVMonitoringAttachment> listDocument = Service<PVMonitoringService>().GetDocumentFileByReference(pid.ToString(), SessionManager.UserType).ToList();
            ViewBag.ListDocument = listDocument;

            ViewBag.BankAccountSelected = Service<BankAccountService>().GetBankAccount(pid.ToString(), SessionManager.UserType);
            ViewBag.BankAccount = Service<BankAccountService>().GetBankAccount(SessionManager.RoleStr, SessionManager.UserType, string.Empty);

            ViewBag.IsAllowedVerified = IsAllowed("verified_pic");
            ViewBag.IsAllowedNotProper = IsAllowed("notproper_bg");
            ViewBag.ProccesId = pid;
            ViewBag.Pvno = pvno;
            ViewBag.Stsbudget = stsbugdet;
            ViewBag.Pvbudget = pvbudget;
            ViewBag.UserType = SessionManager.UserType;
            ViewBag.LevelCode = SessionManager.LevelCode;

            var url = "~/Areas/Core/Views/PVMonitoring/Form/_PVHeaderView.cshtml";

            return PartialView(url);
        }

        [HttpPost]
        public ActionResult UploadLetterInitial(int divId)
        {
            string pid = "0";
            ViewBag.ProccesId = pid;

            IList<TransactionType> listTt = Service<PVMonitoringService>().GetTransactionType(divId.ToString());
            List<WBSStructure> xfin = new List<WBSStructure>();
            
            foreach (TransactionType x in listTt)
            {
                List<WBSStructure> xres = Service<PVMonitoringService>().GetBugdetNo(divId, DateTime.Now.Year.ToString(), x.transaction_cd.ToString());
                foreach (WBSStructure xlg in xres)
                {
                    bool has = xfin.Any(o => o.WbsNumber == xlg.WbsNumber);
                    if (!has)
                    {
                        xlg.Description = xlg.WbsNumber + ": " + xlg.Description;

                        xfin.Add(xlg);
                    }
                }
            }

            ViewBag.BudgetNo = xfin;
            ViewBag.UserType = SessionManager.UserType;

            List<PVMonitoringAttachment> listDocument = new List<PVMonitoringAttachment>();//Service<PVMonitoringService>().GetDocumentFileByReference(pid, SessionManager.UserType).ToList();
            ViewBag.ListDocument = listDocument;

            var url = "~/Areas/Core/Views/PVMonitoring/Form/_PVMonitoringUploadLoD.cshtml";
            return PartialView(url);
        }

        [HttpPost]
        public ActionResult RegisterBankInitial(string invoice, string processId, string vendor, string venName)
        {
            var userType = SessionManager.UserType;
            ViewBag.UserType = userType;

            List<PVMonitoringBankAccount> listDocProcess = Service<BankAccountService>().GetBankProcessByInvoice(processId).ToList();
            ViewBag.ListDocProcess = listDocProcess;

            ViewBag.Invoice = invoice;
            ViewBag.ProcessID = processId;
            ViewBag.Vendor = vendor;
            ViewBag.VenName = venName;

            var bankAccountIn = Service<BankAccountService>().GetBankAccount(SessionManager.RoleStr, SessionManager.UserType, "1");
            ViewBag.BankAccountIn = bankAccountIn;

            var url = "~/Areas/Core/Views/PVMonitoring/Form/_PVMonitoringBankRegister.cshtml";
            return PartialView(url);
        }

        public ActionResult GetListDocuments(string processID)
        {
            var result = Service<PVMonitoringService>().GetDocumentFileByReference(processID, SessionManager.UserType);

            var ds = new DataSourceResult
            {
                Data = result
            };
            return Json(ds, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetListDistribution(string pvno)
        {
            var result = Service<PVMonitoringService>().GetDistributionStatusByReference(pvno);

            var ds = new DataSourceResult
            {
                Data = result
            };
            return Json(ds, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetListBankAccount(string processID, string invoice, string vendorCode, string vendorName)
        {
            ViewBag.Invoice = invoice;
            ViewBag.ProcessID = processID;
            ViewBag.Vendor = vendorCode;
            ViewBag.VenName = vendorName;

            ViewBag.UserType = SessionManager.UserType;

            var bankAccountIn = Service<BankAccountService>().GetBankAccount(SessionManager.RoleStr, SessionManager.UserType, "1");
            ViewBag.BankAccountIn = bankAccountIn;

            List<PVMonitoringBankAccount> list = base.Service<BankAccountService>().GetBankProcessByInvoice(invoice).ToList<PVMonitoringBankAccount>();
            DataSourceResult result1 = new DataSourceResult();
            result1.Data = list;
            DataSourceResult data = result1;
            return base.Json(data, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Download
        public void DownloadTemplate(string divId)
        {
            string fileName = "ExternalWithTax-Ebilling.xls";
            string filesTMp = HttpContext.Request.MapPath("~/Content/template/" + fileName);
            FileStream ftmp = new FileStream(filesTMp, FileMode.Open, FileAccess.ReadWrite);
            byte[] result = null;

            HSSFWorkbook workbook = new HSSFWorkbook(ftmp);
            HSSFSheet sheet = (HSSFSheet)workbook.GetSheetAt(0);

            int row = DATA_ROW_INDEX_START;
            IRow Hrow;

            IList<TransactionType> listTt = Service<PVMonitoringService>().GetTransactionType(divId);
            IList<ComboEx> listVendor = Service<PVMonitoringService>().GetVendorByInvoice(string.Empty, true);
            IList<CodeConstant> listCc = Service<PVMonitoringService>().GetCostCenterExcelTemp(string.Empty, 0);

            for (int i = 1; i <= 1; i++)
            {
                Hrow = sheet.GetRow(row);

                for (int x = 1; x <= 2; x++)
                {
                    Hrow.CreateCell(x);
                }

                row++;
            }

            int rowMin = row - 1;

            sheet.AddValidationData(createVendorListSheet(workbook, "Dealer List", listVendor.ToList(), rowMin));
            sheet.AddValidationData(createTtListSheet(workbook, "Transaction List", listTt.ToList(), rowMin));
            sheet.AddValidationData(createCcListSheet(workbook, "Cost Center List", listCc.ToList(), rowMin));

            ftmp.Close();
            using (MemoryStream ms = new MemoryStream())
            {
                workbook.Write(ms);
                result = ms.GetBuffer();
            }

            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.ContentType = "application/octet-stream";

            Response.AddHeader("Content-Length", Convert.ToString(result.Length));

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", fileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(result);
            Response.End();
        }

        public HSSFDataValidation createVendorListSheet(HSSFWorkbook wb, string sheetName, List<ComboEx> data, int row)
        {
            HSSFSheet sheet = (HSSFSheet)wb.CreateSheet(sheetName);
            HSSFName namedRange = (HSSFName)wb.CreateName();

            namedRange.NameName = "listVendorxls";
            namedRange.RefersToFormula = "'" + sheetName + "'!$B$1:$B$" + data.Count + "";

            for (int i = 0; i < data.Count; i++)
            {
                IRow Hrow = sheet.CreateRow(i);
                Hrow.CreateCell(0).SetCellValue(data[i].CODE);
                Hrow.CreateCell(1).SetCellValue(data[i].NAME);
            }

            CellRangeAddressList vendorList = new CellRangeAddressList(
                row, row, 1, 1);
            DVConstraint dvConstraintVendor = DVConstraint.CreateFormulaListConstraint("listVendorxls");

            HSSFDataValidation dataValidationVendor = new HSSFDataValidation
              (vendorList, dvConstraintVendor);
            dataValidationVendor.SuppressDropDownArrow = false;

            return dataValidationVendor;
        }

        public HSSFDataValidation createTtListSheet(HSSFWorkbook wb, string sheetName, List<TransactionType> data, int row)
        {
            HSSFSheet sheet = (HSSFSheet)wb.CreateSheet(sheetName);
            HSSFName namedRange = (HSSFName)wb.CreateName();

            namedRange.NameName = "listTtxls";
            namedRange.RefersToFormula = "'" + sheetName + "'!$B$1:$B$" + data.Count + "";

            for (int i = 0; i < data.Count; i++)
            {
                IRow Hrow = sheet.CreateRow(i);
                Hrow.CreateCell(0).SetCellValue(data[i].transaction_cd);
                Hrow.CreateCell(1).SetCellValue(data[i].std_wording);
            }

            CellRangeAddressList ttList = new CellRangeAddressList(
                row, row, 2, 2);
            DVConstraint dvConstraintTt = DVConstraint.CreateFormulaListConstraint("listTtxls");

            HSSFDataValidation dataValidationTt = new HSSFDataValidation
              (ttList, dvConstraintTt);
            dataValidationTt.SuppressDropDownArrow = false;

            return dataValidationTt;
        }

        public HSSFDataValidation createCcListSheet(HSSFWorkbook wb, string sheetName, List<CodeConstant> data, int row)
        {
            HSSFSheet sheet = (HSSFSheet)wb.CreateSheet(sheetName);
            HSSFName namedRange = (HSSFName)wb.CreateName();

            namedRange.NameName = "listCcxls";
            namedRange.RefersToFormula = "'" + sheetName + "'!$B$1:$B$" + data.Count + "";

            for (int i = 0; i < data.Count; i++)
            {
                IRow Hrow = sheet.CreateRow(i);
                Hrow.CreateCell(0).SetCellValue(data[i].Code);
                Hrow.CreateCell(1).SetCellValue(data[i].Description);
            }

            CellRangeAddressList ccList = new CellRangeAddressList(
                row, row, 15, 15);
            DVConstraint dvConstraintCc = DVConstraint.CreateFormulaListConstraint("listCcxls");

            HSSFDataValidation dataValidationCc = new HSSFDataValidation
              (ccList, dvConstraintCc);
            dataValidationCc.SuppressDropDownArrow = false;

            return dataValidationCc;
        }

        public void DownloadErrTxt(string filename)
        {
            string fileDirectory = Server.MapPath("~/Content/template/err/");
            byte[] hasil = System.IO.File.ReadAllBytes(fileDirectory + filename);

            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;
            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(hasil.Length));
            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", filename));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(hasil);
            Response.End();
        }

        public void Document()
        {
            string msgerror = "";
            var uploadService = Service<PVMonitoringService>();

            try
            {
                var id = GetQueryString<string>("id");
                var folder = GetQueryString<string>("pv_no");
                var filename = GetQueryString<string>("filename");              
                string year = id.ToString().Substring(id.ToString().Length - 4, 4);

                byte[] x = uploadService.Download(id.ToString(), filename.ToString(), folder.ToString(), out msgerror);

                if (x != null)
                {
                    this.Response.Clear();
                    this.Response.AddHeader("Access-Control-Allow-Origin", "*");
                    this.Response.ContentType = "application/octet-stream";
                    this.Response.AddHeader("content-disposition", "attachment;filename=" + filename);
                    this.Response.BinaryWrite(x);
                    this.Response.Flush();
                }
            }
            catch (Exception exc)
            {
                //
            }
            this.Response.End();
        }

        public void PrintCover(int pid)
        {
            List<PVHeader> listData = new List<PVHeader>();

            PVHeader x = Service<PVMonitoringService>().GetPVHeader(pid);
            string bankAccountSelected = Service<BankAccountService>().GetBankAccount(pid.ToString(), SessionManager.UserType);
            List<BankAccount> bankAccounts = Service<BankAccountService>().GetBankAccount(SessionManager.RoleStr, SessionManager.UserType, string.Empty);

            string bankAccount = bankAccounts.Where(o => o.BANK_KEY == bankAccountSelected).Select(o => o.BANK_NAME).FirstOrDefault();

            if (x != null)
            {
                listData.Add(x);
            }

            string _FileName = "PV_Cover_" + DateTime.Now.ToString("ddMMyyyy_HHmmss") + ".pdf";

            byte[] xx = Service<PVMonitoringService>().printCover(
                            System.IO.Path.GetDirectoryName(Server.MapPath(ConfigurationManager.AppSettings["ImgPath"])),
                            ConfigurationManager.AppSettings["CompanyLogo"],
                            Server.MapPath(ConfigurationManager.AppSettings["TemplatePvCover"]),
                            ConfigurationManager.AppSettings["CompanyName"],
                            ConfigurationManager.AppSettings["TempFileUpload"],
                            SessionManager.Current,
                            bankAccount,
                            listData,
                            SessionManager.UserType);

            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.ContentType = "application/octet-stream";

            Response.AddHeader("Content-Length", Convert.ToString(xx.Length));

            Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", _FileName));
            Response.AddHeader("Set-Cookie", "fileDownload=true; path=/");

            Response.BinaryWrite(xx);
            Response.End();
        }

        public void DocumentBankAttach()
        {
            string msgerror = "";
            var uploadService = Service<PVMonitoringService>();

            try
            {
                var guid = GetQueryString<string>("guid");
                var filename = GetQueryString<string>("filename");
                var directory = GetQueryString<string>("directory");

                byte[] x = uploadService.DownloadBankAttach(guid.ToString(), directory.ToString(), out msgerror);

                if (x != null)
                {
                    this.Response.Clear();
                    this.Response.AddHeader("Access-Control-Allow-Origin", "*");
                    this.Response.ContentType = "application/octet-stream";
                    this.Response.AddHeader("content-disposition", "attachment;filename=" + filename);
                    this.Response.BinaryWrite(x);
                    this.Response.Flush();
                }
            }
            catch (Exception exc)
            {
                //
            }
            this.Response.End();
        }
        #endregion

        #region Budget
        [HttpPost]
        public ActionResult GetComboBoxBudgetNo(string transactType, int divId)
        {
            int b = transactType == "" ? 0 : int.Parse(transactType);
            var x = Service<PVMonitoringService>().GetTransactionType(divId.ToString());
            var isBudgeted = x
                    .Where(i => i.transaction_cd == b)
                    .Select(i => i.budget_flag).SingleOrDefault();

            var dataList = Service<PVMonitoringService>().GetBugdetNo(divId, DateTime.Now.Year.ToString(), transactType);
            return Json(new { budgeted = isBudgeted, data = dataList }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetBudget(string wbs, string pvno)
        {
            string BaseUri = Service<PVMonitoringService>().GetStatusCmb("1", "pv_url_elvis", true);

            Budget budget = new Budget();
                   budget.no = pvno;
                   budget.year = DateTime.Now.Year.ToString();
                   budget.wbs = wbs;

            var resultApi = new MessageModel();
            var result = new object();

            try
            {
                TryUpdateModel<Budget>(budget);

                if (ModelState.IsValid)
                {
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(BaseUri);
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                        var uri = string.Format(client.BaseAddress + "api/Budget/GetBudget");
                        var response = client.PostAsJsonAsync(uri, budget).Result;
                        if (response.IsSuccessStatusCode)
                        {
                            resultApi = response.Content.ReadAsAsync<MessageModel>().Result;
                        }
                    }
                    result = new
                    {
                        resultApi.success,
                        resultApi.data,
                        resultApi.message
                    };
                }
                else
                {
                    result = new
                    {
                        success = false,
                        message = "Get budget failed, please check your form"
                    };
                }
            }
            catch (Exception e)
            {
                result = new
                {
                    success = false,
                    message = e.Message
                };
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public MessageModel CheckBudget(string wbs, int pid, int status, bool withModel, PVHeader pvh)
        {
            var resultApi = new MessageModel();

            string BaseUri = "";
            Budget budget = new Budget();
            PVHeader data;

            try
            {
                if (!withModel)
                {
                    data = Service<PVMonitoringService>().GetPVHeader(pid);
                }
                else
                {
                    data = pvh;
                }

                if (data != null)
                {
                    BaseUri = Service<PVMonitoringService>().GetStatusCmb("1", "pv_url_elvis", true);

                    budget.doc_no = int.Parse(data.PV_NO);
                    budget.date = data.PV_DATE;
                    budget.status = status;
                    budget.amount = data.TOTAL_TURNOVER;
                    
                    if (!withModel)
                    {
                        budget.wbs_no = wbs;
                    }
                    else
                    {
                        budget.wbs_no = data.BUDGET;
                    }

                    TryUpdateModel<Budget>(budget);

                    if (ModelState.IsValid)
                    {
                        using (var client = new HttpClient())
                        {
                            client.BaseAddress = new Uri(BaseUri);
                            client.DefaultRequestHeaders.Accept.Clear();
                            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                            var uri = string.Format(client.BaseAddress + "api/Budget/CheckBudget");
                            var response = client.PostAsJsonAsync(uri, budget).Result;
                            if (response.IsSuccessStatusCode)
                            {
                                resultApi = response.Content.ReadAsAsync<MessageModel>().Result;
                            }
                        }
                    }
                    else
                    {
                        resultApi.success = false;
                        resultApi.message = "Get budget failed, please check your form";
                    }
                }
                else
                {
                    resultApi.success = false;
                    resultApi.message = "No found PV";
                }
            }
            catch (Exception e)
            {
                resultApi.success = false;
                resultApi.message = e.Message;
            }

            return resultApi;
        }
        #endregion

        #region Upload
        private int getColumnLength(IList<Table> Table, string columnName) =>
            Convert.ToInt16(Table.SingleOrDefault<Table>(x => (x.COLUMN_NAME == columnName)).CHARACTER_MAXIMUM_LENGTH);

        public JsonResult UploadListPVMonitoring(HttpPostedFileBase uploadFilePVMonitoring, string divId)
        {
            bool flag = false;
            //bool budgetChecked = false;

            bool success = false;

            IList<string> errMesgs = new List<string>();
            HashSet<string> source = new HashSet<string>();

            string pathErrTax = base.Server.MapPath("~/Content/template/err/");

            string pid = "";
            List<PVHeader> list3 = new List<PVHeader>();

            try
            {
                List<PVHeader> list2 = this.GetDataFromExcel(uploadFilePVMonitoring, divId, errMesgs);

                HashSet<string> set2 = new HashSet<string>(errMesgs);
                if (set2.Count<string>() > 0)
                {
                    errMesgs = new List<string>();
                    throw new Exception(base.Service<PVMonitoringService>().ErrTxt(set2, pathErrTax, "Error-Uploadxls", "pv_errmegs_uploadxls"));
                }
                else
                {
                    foreach (PVHeader xu in list2)
                    {
                        xu.ACCOUNT_INFO = xu.pVMonitorings.First().ACCOUNT_INFO.ToString();
                    }
                }

                var list1 = list2.GroupBy(x => new { x.VENDOR_CD, x.TRANSACTION_CD, x.BUDGET, x.IS_BUDGET, x.UPLOAD_FILENAME, x.ACCOUNT_INFO })
                                 .Select(b => new PVHeader
                                 {
                                     VENDOR_CD = b.Key.VENDOR_CD,
                                     TRANSACTION_CD = b.Key.TRANSACTION_CD,
                                     BUDGET = b.Key.BUDGET,
                                     IS_BUDGET = b.Key.IS_BUDGET,
                                     UPLOAD_FILENAME = b.Key.UPLOAD_FILENAME,
                                     ACCOUNT_INFO = b.Key.ACCOUNT_INFO
                                 });

                foreach (PVHeader xy in list1)
                {
                    PVHeader item = new PVHeader();
                             item = xy;
                             item.pVMonitorings = new List<PVMonitoring>();

                    List<PVHeader> list4 = list2.Where<PVHeader>(delegate (PVHeader c) {
                        if (c.VENDOR_CD != xy.VENDOR_CD)
                        {
                            return false;
                        }
                        int? nullable = c.TRANSACTION_CD;
                        int? nullable2 = xy.TRANSACTION_CD;
                        return (((nullable.GetValueOrDefault() == nullable2.GetValueOrDefault()) & ((nullable != null) == (nullable2 != null))) && ((c.IS_BUDGET == xy.IS_BUDGET) && (c.BUDGET == xy.BUDGET) && (c.ACCOUNT_INFO == xy.ACCOUNT_INFO)));
                    }).ToList<PVHeader>();

                    if (list4 != null)
                    {
                        int i = 1;
                        foreach (PVHeader header2 in list4)
                        {
                            header2.pVMonitorings[0].SEQ_NO = i++;
                            item.pVMonitorings.Add(header2.pVMonitorings.First<PVMonitoring>());
                        }
                    }
                    list3.Add(item);
                }
                
                foreach (PVHeader header3 in list3)
                {
                    int num = base.Service<PVMonitoringService>().GetPid(header3, SessionManager.Current);

                    if (num != 0)
                    {
                        success = base.Service<PVMonitoringService>().SaveUploadListPVMonitoring(header3, num, pathErrTax, SessionManager.Current);

                        if (success)
                        {
                            pid = pid + Convert.ToString(num) + ",";
                            header3.PROCESS_ID = num;
                            /*
                            if (header3.IS_BUDGET)
                            {
                                MessageModel model = this.CheckBudget(header3.BUDGET, num, 1, false, new PVHeader());

                                if (model.success)
                                {
                                    string budgetMesg = "";
                                    string stsBudget = "";

                                    List<string> budgetMesgArr = new List<string>();

                                    if (model.message == "AVAILABLE")
                                    {
                                        char[] separator = new char[] { ';' };
                                        budgetMesgArr = model.message.Split(separator).ToList<string>();
                                        budgetMesg = budgetMesgArr[0];
                                        stsBudget = budgetMesg;
                                        budgetChecked = true;
                                    }
                                    else
                                    {
                                        char[] separator = new char[] { ';' };
                                        budgetMesgArr = model.message.Split(separator).ToList<string>();
                                        budgetMesg = "NOT AVAILABLE";
                                        stsBudget = budgetMesgArr[0];
                                        budgetChecked = true;
                                    }

                                    if (budgetChecked)
                                    {
                                        base.Service<PVMonitoringService>().UpdateBudgetPVMonitoring(num, budgetMesg, header3.BUDGET);

                                        if (budgetMesgArr.Count<string>() > 1)
                                        {
                                            source.Add("BUDGET: " + header3.BUDGET + " -> " + budgetMesgArr[1]);
                                        }
                                        else
                                        {
                                            source.Add("BUDGET: " + header3.BUDGET + " " + stsBudget);
                                        }

                                        success = model.success;
                                    }
                                }
                            }
                            */
                        }
                    }
                }

                List<string> pidv = pid.Split(',').ToList();
                             pidv.Remove("");
                string[] pidvArr = pidv.ToArray();

                
                if (pidvArr.Count() > 0)
                {
                    HashSet<string> xo = new HashSet<string>();
                                    xo = base.Service<PVMonitoringService>().GetUploadValidation(pidvArr);
                    
                    if (xo.Count<string>() > 0)
                    {
                        errMesgs = new List<string>();
                        throw new Exception(base.Service<PVMonitoringService>().ErrTxt(xo, pathErrTax, "Error-Uploadxls", "pv_errmegs_uploadxls"));
                    }
                    
                    /*
                    else
                    { 
                        if (budgetChecked && (source.Count<string>() > 0))
                        {
                            errMesgs = new List<string> {
                                base.Service<PVMonitoringService>().ErrTxt(source, pathErrTax, "Check-budget", "pv_success_budget")
                            };
                        }
                    }
                    */
                }                
            }
            catch (Exception exception1)
            {
                this.Delete(pid, 1, list3);
                success = false;
                flag = true;
                errMesgs.Add(exception1.Message);
            }
            return base.Json(new
            {
                IsSuccess = success,
                MessageError = errMesgs,
                IsError = flag,
                Pid = pid
            }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Upload(string pid, string attachmentType, bool by_internal)
        {
            string msgerror = "";
            HttpFileCollectionBase files = base.Request.Files;
            List<PVMonitoringAttachment> list = new List<PVMonitoringAttachment>();
            PVMonitoringService service = base.Service<PVMonitoringService>();
            string userType = SessionManager.UserType;
            if ((files != null) && ((files.Count > 0) && ((attachmentType != "") && (attachmentType != null))))
            {
                List<string> list1 = pid.Split(',').ToList<string>();
                list1.Remove("");
                string[] processId = list1.ToArray();

                if (by_internal)
                {
                    int seqLatest = 0;
                    List<PVMonitoringAttachment> list2 = base.Service<PVMonitoringService>().GetDocumentFileByPIDReff(processId, userType).ToList<PVMonitoringAttachment>();
                    if (list2.Count > 0)
                    {
                        seqLatest = list2
                                    .OrderByDescending(o => o.REF_SEQ_NO)
                                    .Select(o => o.REF_SEQ_NO).First();
                    }

                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];
                        if (!list2.Exists(e => e.FILE_NAME == file.FileName))
                        {
                            PVMonitoringAttachment xi = service.Upload(file, processId, attachmentType, out msgerror, by_internal);
                            if (xi != null)
                            {
                                xi.REF_SEQ_NO = seqLatest + (i + 1);
                                list.Add(xi);
                            }
                        }
                        else
                        {
                            msgerror = $"File {file.FileName} already exists";
                        }
                    }
                }
                else
                {
                    string[] strArray2 = processId;
                    int index = 0;
                    while (index < strArray2.Length)
                    {
                        string str3 = strArray2[index];
                        List<PVMonitoringAttachment> list3 = base.Service<PVMonitoringService>().GetDocumentFileByReference(str3, userType).ToList<PVMonitoringAttachment>();
                        int seqLatest = list3
                                        .OrderByDescending(o => o.REF_SEQ_NO)
                                        .Select(o => o.REF_SEQ_NO).First();
                        int num3 = 0;
                        while (true)
                        {
                            if (num3 >= files.Count)
                            {
                                index++;
                                break;
                            }
                            HttpPostedFileBase base1 = files[num3];
                            if (!list3.Exists(e => e.FILE_NAME == base1.FileName))
                            {
                                PVMonitoringAttachment xi = service.Upload(base1, str3, attachmentType, out msgerror, by_internal);
                                if (xi != null)
                                {
                                    xi.REF_SEQ_NO = seqLatest + (num3+1);
                                    list.Add(xi);
                                }
                            }
                            else
                            {
                                msgerror = $"File {base1.FileName} already exist in process id {str3} ";
                            }
                            num3++;
                        }
                    }
                }
            }
            return base.Json(new
            {
                Result = list,
                MessageError = msgerror
            }, JsonRequestBehavior.AllowGet);
        }        

        private List<PVHeader> GetDataFromExcel(HttpPostedFileBase file, string divId, IList<string> errMesgs)
        {
            HSSFWorkbook workbook = null;
            using (Stream stream = file.InputStream)
            {
                workbook = new HSSFWorkbook(stream);
            }
            if (workbook == null)
            {
                throw new ArgumentNullException("Cannot create Workbook object from excel file " + file.FileName);
            }
            HSSFFormulaEvaluator evaluator1 = new HSSFFormulaEvaluator(workbook);
            DataFormatter formatter = new DataFormatter();
            IRow row = null;
            ICell cell = null;

            List<PVHeader> list = new List<PVHeader>();
            PVHeader item = null;
            PVMonitoring monitoring = null;
            IList<TransactionType> transactionType = base.Service<PVMonitoringService>().GetTransactionType(divId);
            IList<CodeConstant> costCenterCd = base.Service<PVMonitoringService>().GetCostCenterExcelTemp(string.Empty, 0);
            IList<ComboEx> vendorByInvoice = base.Service<PVMonitoringService>().GetVendorByInvoice(string.Empty, true);

            List<WBSStructure> budgetNo = new List<WBSStructure>();
            foreach (TransactionType x in transactionType)
            {
                List<WBSStructure> xres = Service<PVMonitoringService>().GetBugdetNo(int.Parse(divId), DateTime.Now.Year.ToString(), x.transaction_cd.ToString());

                foreach (WBSStructure xlg in xres)
                {
                    bool has = budgetNo.Any(o => o.WbsNumber == xlg.WbsNumber);
                    if (!has)
                    {
                        budgetNo.Add(xlg);
                    }
                }
            }

            IList<Table> tableProperty = base.Service<PVMonitoringService>().GetTableProperty("TB_T_UPLOAD_D");

            bool inputted_tax = false;
            int num = 0;
            int num2 = 1;
            int cellnum = 0;
            ISheet sheetAt = workbook.GetSheetAt(0);
            int rownum = 6;
            goto TR_01D0;
            TR_0007:
            rownum++;
            TR_01D0:
            while (true)
            {
                if (rownum > sheetAt.LastRowNum)
                {
                    break;
                }
                item = new PVHeader
                {
                    pVMonitorings = new List<PVMonitoring>()
                };
                monitoring = new PVMonitoring();
                cellnum = 1;
                row = sheetAt.GetRow(rownum);
                if (row != null)
                {
                    item.UPLOAD_FILENAME = file.FileName;
                    try
                    {
                        cell = row.GetCell(cellnum);
                        if (cell != null)
                        {
                            if (cell.CellType == CellType.Blank)
                            {
                                errMesgs.Add($"Vendor row {rownum + 1} is required");
                            }
                            else if (cell.CellType != CellType.String)
                            {
                                errMesgs.Add($"Vendor row {rownum + 1} is Incorrect Format");
                            }
                            else
                            {
                                var predicate = vendorByInvoice.Where(x => x.NAME.Contains(cell.StringCellValue));
                                item.VENDOR_CD = predicate.Select(z => z.CODE).SingleOrDefault(); //vendorByInvoice.Where<ComboEx>(predicate).SingleOrDefault<ComboEx>().CODE;
                                if (item.VENDOR_CD == null)
                                {
                                    errMesgs.Add($"Vendor at row {rownum + 1} is not registered");
                                }
                            }
                        }
                    }
                    catch (Exception exception)
                    {
                        errMesgs.Add($"Unable to get value of Vendor at row {rownum + 1}, Error Mesg : {exception.Message}");
                    }
                    try
                    {
                        cell = row.GetCell(++cellnum);
                        if (cell != null)
                        {
                            if (cell.CellType == CellType.Blank)
                            {
                                errMesgs.Add($"Transaction row {rownum + 1} is required");
                            }
                            else if (cell.CellType != CellType.String)
                            {
                                errMesgs.Add($"Transaction row {rownum + 1} is Incorrect Format");
                            }
                            else
                            {
                                TransactionType type = transactionType.Where(x => x.std_wording == cell.StringCellValue).SingleOrDefault();

                                if (type != null)
                                {
                                    item.TRANSACTION_CD = new int?(type.transaction_cd);
                                    item.IS_BUDGET = type.budget_flag == 1;

                                    if (item.TRANSACTION_CD == null || item.TRANSACTION_CD == 0)
                                    {
                                        errMesgs.Add($"Transaction row {rownum + 1} is required");
                                    }
                                }
                                else
                                {
                                    errMesgs.Add($"Transaction at row {rownum + 1} is not authorized");
                                }
                            }
                        }
                    }
                    catch (Exception exception2)
                    {
                        errMesgs.Add($"Unable to get value of Transaction at row {rownum + 1}, Error Mesg : {exception2.Message}");
                    }
                    try
                    {
                        cell = row.GetCell(++cellnum);
                        if (cell != null)
                        {
                            if (cell.CellType == CellType.Blank)
                            {
                                if (item.IS_BUDGET)
                                {
                                    errMesgs.Add($"Budget row {rownum + 1} is required");
                                }
                            }
                            else if (cell.CellType != CellType.String)
                            {
                                errMesgs.Add($"Budget row {rownum + 1} is Incorrect Format");
                            }
                            else
                            {
                                if (item.IS_BUDGET)
                                {
                                    bool has = budgetNo.Any(o => o.WbsNumber == cell.StringCellValue);
                                    if (has)
                                    {
                                        item.BUDGET = cell.StringCellValue;
                                    }
                                    else
                                    {
                                        errMesgs.Add($"Budget at row {rownum + 1} is not available");
                                    }
                                }
                                else
                                {
                                    errMesgs.Add($"Transaction type not use a budget, please empty budget no ! at row {rownum + 1}");
                                }
                            }
                        }
                    }
                    catch (Exception exception3)
                    {
                        errMesgs.Add($"Unable to get value of Budget at row {rownum + 1}, Error Mesg : {exception3.Message}");
                    }
                    cellnum = 0;
                    try
                    {
                        cell = row.GetCell(cellnum);
                        if (cell != null)
                        {
                            if (cell.CellType == CellType.Blank)
                            {
                                monitoring.SEQ_XCL = num2;
                            }
                            else if (cell.CellType == CellType.String)
                            {
                                monitoring.SEQ_XCL = int.Parse(cell.StringCellValue);
                            }
                            else if (cell.CellType == CellType.Numeric)
                            {
                                monitoring.SEQ_XCL = Convert.ToInt32(cell.NumericCellValue);
                            }
                            else
                            {
                                errMesgs.Add($"No row {rownum + 1} is Incorrect Format");
                            }
                        }
                    }
                    catch (Exception exception4)
                    {
                        errMesgs.Add($"Unable to get value of No at row {rownum + 1}, Error Mesg : {exception4.Message}");
                    }
                    cellnum += 7;

                    #region comment invoice_no, invoice_dt, tax_no, tax_dt
                    /*
                    try
                    {
                        cell = row.GetCell(++cellnum);
                        if (cell != null)
                        {
                            if (cell.CellType == CellType.Blank)
                            {
                                //errMesgs.Add($"Invoice No row {rownum + 1} is required");
                                monitoring.INVOICE_NO = null;
                            }
                            else if (cell.CellType == CellType.String)
                            {
                                monitoring.INVOICE_NO = cell.StringCellValue;
                                num = this.getColumnLength(tableProperty, "INVOICE_NO");
                                if (!string.IsNullOrEmpty(monitoring.INVOICE_NO) && (monitoring.INVOICE_NO.Length > num))
                                {
                                    errMesgs.Add($"Invoice No at row {rownum + 1} character length should not be more than {num} characters");
                                }
                            }
                            else if (cell.CellType != CellType.Numeric)
                            {
                                errMesgs.Add($"Invoice No row {rownum + 1} is Incorrect Format");
                            }
                            else
                            {
                                monitoring.INVOICE_NO = formatter.FormatCellValue(cell);
                                num = this.getColumnLength(tableProperty, "INVOICE_NO");
                                if (!string.IsNullOrEmpty(monitoring.INVOICE_NO) && (monitoring.INVOICE_NO.Length > num))
                                {
                                    errMesgs.Add($"Invoice No at row {rownum + 1} character length should not be more than {num} characters");
                                }
                            }
                        }
                    }
                    catch (Exception exception5)
                    {
                        errMesgs.Add($"Unable to get value of Invoice No at row {rownum + 1}, Error Mesg : {exception5.Message}");
                    }
                    try
                    {
                        cell = row.GetCell(++cellnum);
                        if (cell != null)
                        {
                            if (cell.CellType == CellType.Blank)
                            {
                                //errMesgs.Add($"Invoice Date row {rownum + 1} is required");
                                monitoring.INVOICE_DATE = null;
                            }
                            else if (cell.CellType != CellType.String)
                            {
                                errMesgs.Add($"Invoice Date row {rownum + 1} format is incorrect");
                            }
                            else
                            {
                                string stringCellValue = cell.StringCellValue;
                                try
                                {
                                    DateTime.ParseExact(stringCellValue, "dd.MM.yyyy", null);
                                    monitoring.INVOICE_DATE = stringCellValue;
                                    num = this.getColumnLength(tableProperty, "INVOICE_DATE");
                                    if (!string.IsNullOrEmpty(monitoring.INVOICE_DATE) && (monitoring.INVOICE_DATE.Length > num))
                                    {
                                        errMesgs.Add($"Invoice Date at row {rownum + 1} character length should not be more than {num} characters");
                                    }
                                }
                                catch (FormatException)
                                {
                                    throw new Exception("Invoice Date must dd.MM.yyyy");
                                }
                            }
                        }
                    }
                    catch (Exception exception6)
                    {
                        errMesgs.Add($"Unable to get value of Invoice Date at row {rownum + 1}, Error Mesg : {exception6.Message}");
                    }
                   
                    try
                    {
                        cell = row.GetCell(++cellnum);
                        if (cell != null)
                        {
                            if (cell.CellType == CellType.Blank)
                            {
                                cell = row.GetCell(cellnum + 1);
                                if (cell != null)
                                {
                                    cell2 = row.GetCell(cellnum + 4);
                                    if ((cell2 != null) && ((cell.CellType != CellType.Blank) || (cell2.CellType != CellType.Blank)))
                                    {
                                        //errMesgs.Add($"Tax No row {rownum + 1} is required");
                                        monitoring.TAX_NO = null;
                                    }
                                }
                            }
                            else if (cell.CellType == CellType.String)
                            {
                                monitoring.TAX_NO = cell.StringCellValue;
                                num = this.getColumnLength(tableProperty, "TAX_NO");
                                if (!string.IsNullOrEmpty(monitoring.TAX_NO) && (monitoring.TAX_NO.Length > num))
                                {
                                    errMesgs.Add($"Tax No at row {rownum + 1} character length should not be more than {num} characters");
                                }
                            }
                            else if (cell.CellType != CellType.Numeric)
                            {
                                errMesgs.Add($"Tax No row {rownum + 1} format is incorrect");
                            }
                            else
                            {
                                monitoring.TAX_NO = formatter.FormatCellValue(cell);
                                num = this.getColumnLength(tableProperty, "TAX_NO");
                                if (!string.IsNullOrEmpty(monitoring.TAX_NO) && (monitoring.TAX_NO.Length > num))
                                {
                                    errMesgs.Add($"Tax No at row {rownum + 1} character length should not be more than {num} characters");
                                }
                            }
                        }
                    }
                    catch (Exception exception7)
                    {
                        errMesgs.Add($"Unable to get value of Tax No at row {rownum + 1}, Error Mesg : {exception7.Message}");
                    }
                    try
                    {
                        cell = row.GetCell(++cellnum);
                        if (cell != null)
                        {
                            if (cell.CellType == CellType.Blank)
                            {
                                cell = row.GetCell(cellnum - 1);
                                if (cell != null)
                                {
                                    cell2 = row.GetCell(cellnum + 3);
                                    if ((cell2 != null) && ((cell.CellType != CellType.Blank) || (cell2.CellType != CellType.Blank)))
                                    {
                                        //errMesgs.Add($"Tax Date row {rownum + 1} is required");
                                        monitoring.TAX_DT = null;
                                    }
                                }
                            }
                            else if (cell.CellType != CellType.String)
                            {
                                errMesgs.Add($"Tax Date row {rownum + 1} format is incorrect");
                            }
                            else
                            {
                                string stringCellValue = cell.StringCellValue;
                                try
                                {
                                    DateTime.ParseExact(stringCellValue, "dd.MM.yyyy", null);
                                    monitoring.TAX_DT = stringCellValue;
                                    num = this.getColumnLength(tableProperty, "TAX_DT");
                                    if (!string.IsNullOrEmpty(monitoring.TAX_DT) && (monitoring.TAX_DT.Length > num))
                                    {
                                        errMesgs.Add($"Tax Date at row {rownum + 1} character length should not be more than {num} characters");
                                    }
                                }
                                catch (FormatException)
                                {
                                    throw new Exception("Tax Date must dd.MM.yyyy");
                                }
                            }
                        }
                    }
                    catch (Exception exception8)
                    {
                        errMesgs.Add($"Unable to get value of Tax Date at row {rownum + 1}, Error Mesg : {exception8.Message}");
                    }
                    */
                    #endregion

                    try
                    {
                        cell = row.GetCell(++cellnum);
                        if (cell != null)
                        {
                            if (cell.CellType == CellType.Blank)
                            {
                                errMesgs.Add($"Curr row {rownum + 1} is required");
                            }
                            else if (cell.CellType != CellType.String)
                            {
                                errMesgs.Add($"Curr row {rownum + 1} is Incorrect Format");
                            }
                            else
                            {
                                monitoring.CURRENCY_CD = cell.StringCellValue;
                                num = this.getColumnLength(tableProperty, "CURRENCY_CD");
                                if (!string.IsNullOrEmpty(monitoring.CURRENCY_CD) && (monitoring.CURRENCY_CD.Length > num))
                                {
                                    errMesgs.Add($"Curr at row {rownum + 1} character length should not be more than {num} characters");
                                }
                            }
                        }
                    }
                    catch (Exception exception9)
                    {
                        errMesgs.Add($"Unable to get value of Curr at row {rownum + 1}, Error Mesg : {exception9.Message}");
                    }
                    try
                    {
                        cell = row.GetCell(++cellnum);
                        if (cell != null)
                        {
                            if (cell.CellType == CellType.Blank)
                            {
                                errMesgs.Add($"Turnover row {rownum + 1} is required");
                            }
                            else if (cell.CellType == CellType.Numeric)
                            {
                                monitoring.AMOUNT_TURN_OVER = new double?(this.ConvertDouble(cell.NumericCellValue.ToString()));

                                if (monitoring.AMOUNT_TURN_OVER == 0)
                                {
                                    errMesgs.Add($"Turnover row {rownum + 1} is required");
                                }
                            }
                            else
                            {
                                errMesgs.Add($"Turnover {rownum + 1} format is incorrect");
                            }
                        }
                    }
                    catch (Exception exception10)
                    {
                        errMesgs.Add($"Unable to get value of Turnover at row {rownum + 1}, Error Mesg : {exception10.Message}");
                    }
                    try
                    {
                        cell = row.GetCell(++cellnum);
                        if (cell != null)
                        {
                            if (cell.CellType != CellType.Blank)
                            {
                                if (cell.CellType == CellType.Numeric)
                                {
                                    monitoring.AMOUNT_PPN = new double?(this.ConvertDouble(cell.NumericCellValue.ToString()));
                                }
                                else if (cell.CellType == CellType.Formula)
                                {
                                    monitoring.AMOUNT_PPN = new double?(this.ConvertDouble(cell.NumericCellValue.ToString()));
                                }
                                else
                                {
                                    errMesgs.Add($"PPN {rownum + 1} format is incorrect");
                                }
                            }
                        }
                    }
                    catch (Exception exception11)
                    {
                        errMesgs.Add($"Unable to get value of PPN at row {rownum + 1}, Error Mesg : {exception11.Message}");
                    }
                    
                    #region comment materai
                    /*
                    try
                    {
                        cell = row.GetCell(++cellnum);
                        if ((cell != null) && (cell.CellType != CellType.Blank))
                        {
                            if (cell.CellType == CellType.Numeric)
                            {
                                monitoring.MATERAI = new double?(cell.NumericCellValue);
                            }
                            else
                            {
                                errMesgs.Add($"Materai {rownum + 1} format is incorrect");
                            }
                        }
                    }
                    catch (Exception exception12)
                    {
                        errMesgs.Add($"Unable to get value of Materai at row {rownum + 1}, Error Mesg : {exception12.Message}");
                    }
                    */
                    #endregion

                    cellnum = (++cellnum) + 1;
                    try
                    {
                        cell = row.GetCell(cellnum);
                        if ((cell != null) && (cell.CellType != CellType.Blank))
                        {
                            if (cell.CellType == CellType.Numeric)
                            {
                                monitoring.AMOUNT = new double?(this.ConvertDouble(cell.NumericCellValue.ToString()));
                            }
                            else if (cell.CellType == CellType.Formula)
                            {
                                monitoring.AMOUNT = new double?(this.ConvertDouble(cell.NumericCellValue.ToString()));
                            }
                            else
                            {
                                errMesgs.Add($"Total {rownum + 1} format is incorrect");
                            }
                        }
                    }
                    catch (Exception exception13)
                    {
                        errMesgs.Add($"Unable to get value of Total at row {rownum + 1}, Error Mesg : {exception13.Message}");
                    }
                    try
                    {
                        cell = row.GetCell(++cellnum);
                        if (cell != null)
                        {
                            if (cell.CellType == CellType.Blank)
                            {
                                errMesgs.Add($"Text row {rownum + 1} is required");
                            }
                            else if (cell.CellType != CellType.String)
                            {
                                errMesgs.Add($"Text {rownum + 1} format is incorrect");
                            }
                            else
                            {
                                monitoring.DESCRIPTION = cell.StringCellValue;
                                num = 32;//this.getColumnLength(tableProperty, "DESCRIPTION");
                                if (!string.IsNullOrEmpty(monitoring.DESCRIPTION) && (monitoring.DESCRIPTION.Length > num))
                                {
                                    errMesgs.Add($"Text at row {rownum + 1} character length should not be more than {num} characters");
                                }
                            }
                        }
                    }
                    catch (Exception exception14)
                    {
                        errMesgs.Add($"Unable to get value of Text at row {rownum + 1}, Error Mesg : {exception14.Message}");
                    }
                    try
                    {
                        cell = row.GetCell(++cellnum);
                        if (cell != null)
                        {
                            if (cell.CellType == CellType.Blank)
                            {
                                errMesgs.Add($"Letter No at row {rownum + 1} is required");
                            }
                            else if (cell.CellType == CellType.String)
                            {
                                monitoring.ACCOUNT_INFO = cell.StringCellValue;

                                num = this.getColumnLength(tableProperty, "account_info");

                                if (!string.IsNullOrEmpty(monitoring.ACCOUNT_INFO) && (monitoring.ACCOUNT_INFO.Length > num))
                                {
                                    errMesgs.Add($"Letter No at row {rownum + 1} character length should not be more than {num} characters");
                                }
                            }
                            else if (cell.CellType == CellType.Numeric)
                            {
                                monitoring.ACCOUNT_INFO = cell.NumericCellValue.ToString();

                                num = this.getColumnLength(tableProperty, "account_info");

                                if (!string.IsNullOrEmpty(monitoring.ACCOUNT_INFO) && (monitoring.ACCOUNT_INFO.Length > num))
                                {
                                    errMesgs.Add($"Letter No at row {rownum + 1} character length should not be more than {num} characters");
                                }
                            }
                            else
                            {
                                errMesgs.Add($"Letter No at row {rownum + 1} format is incorrect");
                            }
                        }
                        try
                        {
                            cell = row.GetCell(++cellnum);
                            if ((cell != null))
                            {
                                if (cell.CellType == CellType.Blank)
                                {
                                    if (item.BUDGET != null)
                                    {
                                        errMesgs.Add($"Cost Center at row {rownum + 1} is required");
                                    }
                                }
                                else if (cell.CellType != CellType.String)
                                {
                                    errMesgs.Add($"Cost Center row {rownum + 1} format is incorrect");
                                }
                                else
                                {
                                    CodeConstant Cc = costCenterCd.Where(x => x.Description == cell.StringCellValue).SingleOrDefault();

                                    if (Cc != null)
                                    {
                                        monitoring.COST_CENTER_CD = Cc.Code;
                                        num = this.getColumnLength(tableProperty, "COST_CENTER_CD");
                                        if (!string.IsNullOrEmpty(monitoring.COST_CENTER_CD) && (monitoring.COST_CENTER_CD.Length > num))
                                        {
                                            errMesgs.Add($"Cost Center at row {rownum + 1} character length should not be more than {num} characters");
                                        }
                                    }
                                    else
                                    {
                                        errMesgs.Add($"Cost Center at row {rownum + 1} is not authorized");
                                    }
                                }
                            }
                        }
                        catch (Exception exception16)
                        {
                            errMesgs.Add($"Unable to get value of Cost Center at row {rownum + 1}, Error Mesg : {exception16.Message}");
                        }
                        try
                        {
                            cell = row.GetCell(++cellnum);
                            if (cell != null)
                            {
                                if (cell.CellType == CellType.Blank)
                                {
                                    //errMesgs.Add($"DPP PPH row {rownum + 1} is required");
                                    monitoring.DPP_PPh_AMOUNT = null;
                                }
                                else if (cell.CellType == CellType.Numeric)
                                {
                                    monitoring.DPP_PPh_AMOUNT = new double?(this.ConvertDouble(cell.NumericCellValue.ToString()));
                                }
                                else if (cell.CellType == CellType.Formula)
                                {
                                    monitoring.DPP_PPh_AMOUNT = new double?(this.ConvertDouble(cell.NumericCellValue.ToString()));
                                }
                                else
                                {
                                    errMesgs.Add($"DPP PPH row {rownum + 1} format is incorrect");
                                }
                            }
                        }
                        catch (Exception exception17)
                        {
                            errMesgs.Add($"Unable to get value of DPP PPH at row {rownum + 1}, Error Mesg : {exception17.Message}");
                        }
                        try
                        {
                            cell = row.GetCell(++cellnum);
                            if (cell != null)
                            {
                                if (cell.CellType == CellType.Blank)
                                {
                                    if (monitoring.DPP_PPh_AMOUNT!=null)
                                    {
                                        errMesgs.Add($"NPWP row {rownum + 1} is required");
                                    }
                                }
                                else if (cell.CellType != CellType.String)
                                {
                                    errMesgs.Add($"NPWP row {rownum + 1} format is incorrect");
                                }
                                else
                                {
                                    monitoring.NPWP_Available = cell.StringCellValue;
                                    num = this.getColumnLength(tableProperty, "NPWP_Available");
                                    if (!string.IsNullOrEmpty(monitoring.NPWP_Available) && (monitoring.NPWP_Available.Length > num))
                                    {
                                        errMesgs.Add($"NPWP at row {rownum + 1} character length should not be more than {num} characters");
                                    }
                                }
                            }
                        }
                        catch (Exception exception18)
                        {
                            errMesgs.Add($"Unable to get value of NPWP at row {rownum + 1}, Error Mesg : {exception18.Message}");
                        }
                        try
                        {
                            cell = row.GetCell(++cellnum);
                            if (cell != null)
                            {
                                if (cell.CellType == CellType.Blank)
                                {
                                    //errMesgs.Add($"Tax Code PPH 23 row {rownum + 1} is required");
                                    monitoring.TAX_CODE_PPh23 = null;
                                }
                                else if (cell.CellType == CellType.String)
                                {
                                    monitoring.TAX_CODE_PPh23 = cell.StringCellValue;
                                    num = this.getColumnLength(tableProperty, "TAX_CODE_PPh23");

                                    if (!string.IsNullOrEmpty(monitoring.TAX_CODE_PPh23) && (monitoring.TAX_CODE_PPh23.Length > num))
                                    {
                                        errMesgs.Add($"Tax Code PPH 23 at row {rownum + 1} character length should not be more than {num} characters");
                                    }
                                    else {
                                        inputted_tax = true;
                                    }
                                }
                                else if (cell.CellType != CellType.Numeric)
                                {
                                    errMesgs.Add($"Tax Code PPH 23 row {rownum + 1} format is incorrect");
                                }
                                else
                                {
                                    monitoring.TAX_CODE_PPh23 = formatter.FormatCellValue(cell);
                                    num = this.getColumnLength(tableProperty, "TAX_CODE_PPh23");
                                    if (!string.IsNullOrEmpty(monitoring.TAX_CODE_PPh23) && (monitoring.TAX_CODE_PPh23.Length > num))
                                    {
                                        errMesgs.Add($"Tax Code PPH 23 at row {rownum + 1} character length should not be more than {num} characters");
                                    }
                                    else
                                    {
                                        inputted_tax = true;
                                    }
                                }
                            }
                            try
                            {
                                cell = row.GetCell(++cellnum);
                                if ((cell != null) && (cell.CellType != CellType.Blank))
                                {
                                    if (cell.CellType == CellType.String)
                                    {
                                        string stringCellValue = cell.StringCellValue;
                                        if (!stringCellValue.Contains("%"))
                                        {
                                            monitoring.TAX_TARIFF_PPh23 = new double?(this.ConvertDouble(cell.StringCellValue));
                                        }
                                        else
                                        {
                                            string input = stringCellValue.Replace("%", "");
                                            monitoring.TAX_TARIFF_PPh23 = new double?(this.ConvertDouble(input) / 100.0);
                                        }
                                    }
                                    else if (cell.CellType == CellType.Numeric)
                                    {
                                        monitoring.TAX_TARIFF_PPh23 = new double?(this.ConvertDouble(cell.NumericCellValue.ToString()));
                                    }
                                    else if (cell.CellType == CellType.Formula)
                                    {
                                        monitoring.TAX_TARIFF_PPh23 = new double?(this.ConvertDouble(cell.NumericCellValue.ToString()));
                                    }
                                    else
                                    {
                                        errMesgs.Add($"Tarif PPH 23 row {rownum + 1} format is incorrect");
                                    }
                                }
                            }
                            catch (Exception exception20)
                            {
                                errMesgs.Add($"Unable to get value of Tarif PPH 23 at row {rownum + 1}, Error Mesg : {exception20.Message}");
                            }
                            try
                            {
                                cell = row.GetCell(++cellnum);
                                if ((cell != null) && (cell.CellType != CellType.Blank))
                                {
                                    if (cell.CellType == CellType.String)
                                    {
                                        monitoring.AMOUNT_PPh23 = new double?(this.ConvertDouble(cell.StringCellValue));
                                    }
                                    else if (cell.CellType == CellType.Numeric)
                                    {
                                        monitoring.AMOUNT_PPh23 = new double?(this.ConvertDouble(cell.NumericCellValue.ToString()));
                                    }
                                    else if (cell.CellType == CellType.Formula)
                                    {
                                        monitoring.AMOUNT_PPh23 = new double?(this.ConvertDouble(cell.NumericCellValue.ToString()));
                                    }
                                    else
                                    {
                                        errMesgs.Add($"Total PPH 23 row {rownum + 1} format is incorrect");
                                    }
                                }
                            }
                            catch (Exception exception21)
                            {
                                errMesgs.Add($"Unable to get value of Total PPH 23 at row {rownum + 1}, Error Mesg : {exception21.Message}");
                            }
                            try
                            {
                                cell = row.GetCell(++cellnum);
                                if ((cell != null) && (cell.CellType != CellType.Blank))
                                {
                                    if (cell.CellType == CellType.Numeric)
                                    {
                                        monitoring.AMOUNT_PPh21_INTERN = new double?(this.ConvertDouble(cell.NumericCellValue.ToString()));
                                    }
                                    else if (cell.CellType == CellType.Formula)
                                    {
                                        monitoring.AMOUNT_PPh21_INTERN = new double?(this.ConvertDouble(cell.NumericCellValue.ToString()));
                                    }
                                    else
                                    {
                                        errMesgs.Add($"Total PPH 21 row {rownum + 1} format is incorrect");
                                    }
                                }
                            }
                            catch (Exception exception22)
                            {
                                errMesgs.Add($"Unable to get value of Total PPH 21 at row {rownum + 1}, Error Mesg : {exception22.Message}");
                            }
                            try
                            {
                                cell = row.GetCell(++cellnum);
                                if (cell != null)
                                {
                                    if (cell.CellType == CellType.Blank)
                                    {
                                        //errMesgs.Add($"Tax Code PPH 21 Ext row {rownum + 1} is required");
                                        monitoring.TAX_CODE_PPh21_EXTERN = null;
                                    }
                                    else if (cell.CellType == CellType.String)
                                    {
                                        monitoring.TAX_CODE_PPh21_EXTERN = cell.StringCellValue;
                                        num = this.getColumnLength(tableProperty, "TAX_CODE_PPh21_EXTERN");
                                        if (!string.IsNullOrEmpty(monitoring.TAX_CODE_PPh21_EXTERN) && (monitoring.TAX_CODE_PPh21_EXTERN.Length > num))
                                        {
                                            errMesgs.Add($"Tax Code PPH 21 Ext at row {rownum + 1} character length should not be more than {num} characters");
                                        }
                                        else
                                        {
                                            inputted_tax = true;
                                        }
                                    }
                                    else if (cell.CellType != CellType.Numeric)
                                    {
                                        errMesgs.Add($"Tax Code PPH 21 Ext row {rownum + 1} format is incorrect");
                                    }
                                    else
                                    {
                                        monitoring.TAX_CODE_PPh21_EXTERN = formatter.FormatCellValue(cell);
                                        num = this.getColumnLength(tableProperty, "TAX_CODE_PPh21_EXTERN");
                                        if (!string.IsNullOrEmpty(monitoring.TAX_CODE_PPh21_EXTERN) && (monitoring.TAX_CODE_PPh21_EXTERN.Length > num))
                                        {
                                            errMesgs.Add($"Tax Code PPH 21 Ext at row {rownum + 1} character length should not be more than {num} characters");
                                        }
                                        else
                                        {
                                            inputted_tax = true;
                                        }
                                    }
                                }
                            }
                            catch (Exception exception23)
                            {
                                errMesgs.Add($"Unable to get value of Tax Code PPH 21 Ext at row {rownum + 1}, Error Mesg : {exception23.Message}");
                            }
                            try
                            {
                                cell = row.GetCell(++cellnum);
                                if (cell != null)
                                {
                                    if (cell.CellType == CellType.Blank)
                                    {
                                        if (monitoring.TAX_CODE_PPh21_EXTERN != null)
                                        {
                                            errMesgs.Add($"Akumulasi DPP PPH 21 row {rownum + 1} is required");
                                        }
                                    }
                                    else if (cell.CellType == CellType.String)
                                    {
                                        monitoring.TAX_DPP_ACCUMULATION_PPH21_EXTERN = new double?((double)int.Parse(cell.StringCellValue));
                                    }
                                    else if (cell.CellType == CellType.Numeric)
                                    {
                                        monitoring.TAX_DPP_ACCUMULATION_PPH21_EXTERN = new double?(this.ConvertDouble(cell.NumericCellValue.ToString()));
                                    }
                                    else if (cell.CellType == CellType.Formula)
                                    {
                                        monitoring.TAX_DPP_ACCUMULATION_PPH21_EXTERN = new double?(this.ConvertDouble(cell.NumericCellValue.ToString()));
                                    }
                                    else
                                    {
                                        errMesgs.Add($"Akumulasi DPP PPH 21 row {rownum + 1} format is incorrect");
                                    }
                                }
                            }
                            catch (Exception exception24)
                            {
                                errMesgs.Add($"Unable to get value of Akumulasi DPP PPH 21 at row {rownum + 1}, Error Mesg : {exception24.Message}");
                            }
                            try
                            {
                                cell = row.GetCell(++cellnum);
                                if ((cell != null) && (cell.CellType != CellType.Blank))
                                {
                                    if (cell.CellType == CellType.String)
                                    {
                                        string stringCellValue = cell.StringCellValue;
                                        if (stringCellValue.Contains("%"))
                                        {
                                            string input = stringCellValue.Replace("%", "");
                                            monitoring.TAX_TARIFF_PPh21_EXTERN = new double?(this.ConvertDouble(input) / 100.0);
                                        }
                                    }
                                    else if (cell.CellType == CellType.Numeric)
                                    {
                                        monitoring.TAX_TARIFF_PPh21_EXTERN = new double?(this.ConvertDouble(cell.NumericCellValue.ToString()));
                                    }
                                    else if (cell.CellType == CellType.Formula)
                                    {
                                        monitoring.TAX_TARIFF_PPh21_EXTERN = new double?(this.ConvertDouble(cell.NumericCellValue.ToString()));
                                    }
                                    else
                                    {
                                        errMesgs.Add($"Tarif PPH 21 Ext row {rownum + 1} format is incorrect");
                                    }
                                }
                            }
                            catch (Exception exception25)
                            {
                                errMesgs.Add($"Unable to get value of Tarif PPH 21 Ext at row {rownum + 1}, Error Mesg : {exception25.Message}");
                            }
                            try
                            {
                                cell = row.GetCell(++cellnum);
                                if ((cell != null) && (cell.CellType != CellType.Blank))
                                {
                                    if (cell.CellType == CellType.String)
                                    {
                                        monitoring.AMOUNT_PPh21_EXTERN = new double?(this.ConvertDouble(cell.StringCellValue));
                                    }
                                    else if (cell.CellType == CellType.Numeric)
                                    {
                                        monitoring.AMOUNT_PPh21_EXTERN = new double?(this.ConvertDouble(cell.NumericCellValue.ToString()));
                                    }
                                    else if (cell.CellType == CellType.Formula)
                                    {
                                        monitoring.AMOUNT_PPh21_EXTERN = new double?(this.ConvertDouble(cell.NumericCellValue.ToString()));
                                    }
                                    else
                                    {
                                        errMesgs.Add($"Total PPH 21 Ext row {rownum + 1} format is incorrect");
                                    }
                                }
                            }
                            catch (Exception exception26)
                            {
                                errMesgs.Add($"Unable to get value of Total PPH 21 Ext at row {rownum + 1}, Error Mesg : {exception26.Message}");
                            }
                            try
                            {
                                cell = row.GetCell(++cellnum);
                                if (cell != null)
                                {
                                    if (cell.CellType == CellType.Blank)
                                    {
                                        if (monitoring.TAX_CODE_PPh21_EXTERN != null)
                                        {
                                            errMesgs.Add($"NIK PPH 21 Ext row {rownum + 1} is required");
                                        }
                                    }
                                    else if (cell.CellType == CellType.String)
                                    {
                                        monitoring.NIK_PPh21_EXTERN = cell.StringCellValue;
                                        num = this.getColumnLength(tableProperty, "NIK_PPh21_EXTERN");
                                        if (!string.IsNullOrEmpty(monitoring.NIK_PPh21_EXTERN) && (monitoring.NIK_PPh21_EXTERN.Length > num))
                                        {
                                            errMesgs.Add($"NIK PPH 21 Ext at row {rownum + 1} character length should not be more than {num} characters");
                                        }
                                    }
                                    else if (cell.CellType != CellType.Numeric)
                                    {
                                        errMesgs.Add($"NIK PPH 21 Ext row {rownum + 1} format is incorrect");
                                    }
                                    else
                                    {
                                        monitoring.NIK_PPh21_EXTERN = formatter.FormatCellValue(cell);
                                        num = this.getColumnLength(tableProperty, "NIK_PPh21_EXTERN");
                                        if (!string.IsNullOrEmpty(monitoring.NIK_PPh21_EXTERN) && (monitoring.NIK_PPh21_EXTERN.Length > num))
                                        {
                                            errMesgs.Add($"NIK PPH 21 Ext at row {rownum + 1} character length should not be more than {num} characters");
                                        }
                                    }
                                }
                            }
                            catch (Exception exception27)
                            {
                                errMesgs.Add($"Unable to get value of NIK PPH 21 Ext at row {rownum + 1}, Error Mesg : {exception27.Message}");
                            }
                            try
                            {
                                cell = row.GetCell(++cellnum);
                                if (cell != null)
                                {
                                    if (cell.CellType == CellType.Blank)
                                    {
                                        //errMesgs.Add($"Tax Code PPH 26 row {rownum + 1} is required");
                                        monitoring.TAX_CODE_PPh26 = null;
                                    }
                                    else if (cell.CellType == CellType.String)
                                    {
                                        monitoring.TAX_CODE_PPh26 = cell.StringCellValue;
                                        num = this.getColumnLength(tableProperty, "TAX_CODE_PPh26");
                                        if (!string.IsNullOrEmpty(monitoring.TAX_CODE_PPh26) && (monitoring.TAX_CODE_PPh26.Length > num))
                                        {
                                            errMesgs.Add($"Tax Code PPH 26 at row {rownum + 1} character length should not be more than {num} characters");
                                        }
                                        else
                                        {
                                            inputted_tax = true;
                                        }
                                    }
                                    else if (cell.CellType != CellType.Numeric)
                                    {
                                        errMesgs.Add($"Tax Code PPH 26 row {rownum + 1} format is incorrect");
                                    }
                                    else
                                    {
                                        monitoring.TAX_CODE_PPh26 = formatter.FormatCellValue(cell);
                                        num = this.getColumnLength(tableProperty, "TAX_CODE_PPh26");
                                        if (!string.IsNullOrEmpty(monitoring.TAX_CODE_PPh26) && (monitoring.TAX_CODE_PPh26.Length > num))
                                        {
                                            errMesgs.Add($"Tax Code PPH 26 at row {rownum + 1} character length should not be more than {num} characters");
                                        }
                                        else
                                        {
                                            inputted_tax = true;
                                        }
                                    }
                                }
                            }
                            catch (Exception exception28)
                            {
                                errMesgs.Add($"Unable to get value of Tax Code PPH 26 at row {rownum + 1}, Error Mesg : {exception28.Message}");
                            }
                            try
                            {
                                cell = row.GetCell(++cellnum);
                                if ((cell != null) && (cell.CellType != CellType.Blank))
                                {
                                    if (cell.CellType == CellType.String)
                                    {
                                        string stringCellValue = cell.StringCellValue;
                                        if (stringCellValue.Contains("%"))
                                        {
                                            string input = stringCellValue.Replace("%", "");
                                            monitoring.TAX_TARIFF_PPh26 = new double?(this.ConvertDouble(input) / 100.0);
                                        }
                                    }
                                    else if (cell.CellType == CellType.Numeric)
                                    {
                                        monitoring.TAX_TARIFF_PPh26 = new double?(this.ConvertDouble(cell.NumericCellValue.ToString()));
                                    }
                                    else if (cell.CellType == CellType.Formula)
                                    {
                                        monitoring.TAX_TARIFF_PPh26 = new double?(this.ConvertDouble(cell.NumericCellValue.ToString()));
                                    }
                                    else
                                    {
                                        errMesgs.Add($"Tarif PPH 26 row {rownum + 1} format is incorrect");
                                    }
                                }
                            }
                            catch (Exception exception29)
                            {
                                errMesgs.Add($"Unable to get value of Tarif PPH 26 at row {rownum + 1}, Error Mesg : {exception29.Message}");
                            }
                            try
                            {
                                cell = row.GetCell(++cellnum);
                                if ((cell != null) && (cell.CellType != CellType.Blank))
                                {
                                    if (cell.CellType == CellType.String)
                                    {
                                        monitoring.AMOUNT_PPh26 = new double?(this.ConvertDouble(cell.StringCellValue));
                                    }
                                    else if (cell.CellType == CellType.Numeric)
                                    {
                                        monitoring.AMOUNT_PPh26 = new double?(this.ConvertDouble(cell.NumericCellValue.ToString()));
                                    }
                                    else if (cell.CellType == CellType.Formula)
                                    {
                                        monitoring.AMOUNT_PPh26 = new double?(this.ConvertDouble(cell.NumericCellValue.ToString()));
                                    }
                                    else
                                    {
                                        errMesgs.Add($"Total PPH 26 row {rownum + 1} format is incorrect");
                                    }
                                }
                            }
                            catch (Exception exception30)
                            {
                                errMesgs.Add($"Unable to get value of Total PPH 26 at row {rownum + 1}, Error Mesg : {exception30.Message}");
                            }
                            try
                            {
                                cell = row.GetCell(++cellnum);
                                if (cell != null)
                                {
                                    if (cell.CellType == CellType.Blank)
                                    {
                                        //errMesgs.Add($"Tax Code PPH Final row {rownum + 1} is required");
                                        monitoring.TAX_CODE_PPh_FINAL = null;
                                    }
                                    else if (cell.CellType == CellType.String)
                                    {
                                        monitoring.TAX_CODE_PPh_FINAL = cell.StringCellValue;
                                        num = this.getColumnLength(tableProperty, "TAX_CODE_PPh_FINAL");
                                        if (!string.IsNullOrEmpty(monitoring.TAX_CODE_PPh_FINAL) && (monitoring.TAX_CODE_PPh_FINAL.Length > num))
                                        {
                                            errMesgs.Add($"Tax Code PPH Final at row {rownum + 1} character length should not be more than {num} characters");
                                        }
                                        else
                                        {
                                            inputted_tax = true;
                                        }
                                    }
                                    else if (cell.CellType != CellType.Numeric)
                                    {
                                        errMesgs.Add($"Tax Code PPH Final row {rownum + 1} format is incorrect");
                                    }
                                    else
                                    {
                                        monitoring.TAX_CODE_PPh_FINAL = formatter.FormatCellValue(cell);
                                        num = this.getColumnLength(tableProperty, "TAX_CODE_PPh_FINAL");
                                        if (!string.IsNullOrEmpty(monitoring.TAX_CODE_PPh_FINAL) && (monitoring.TAX_CODE_PPh_FINAL.Length > num))
                                        {
                                            errMesgs.Add($"Tax Code PPH Final at row {rownum + 1} character length should not be more than {num} characters");
                                        }
                                        else
                                        {
                                            inputted_tax = true;
                                        }
                                    }
                                }
                            }
                            catch (Exception exception31)
                            {
                                errMesgs.Add($"Unable to get value of Tax Code PPH Final at row {rownum + 1}, Error Mesg : {exception31.Message}");
                            }
                            try
                            {
                                cell = row.GetCell(++cellnum);
                                if ((cell != null) && (cell.CellType != CellType.Blank))
                                {
                                    if (cell.CellType == CellType.String)
                                    {
                                        string stringCellValue = cell.StringCellValue;
                                        if (stringCellValue.Contains("%"))
                                        {
                                            string input = stringCellValue.Replace("%", "");
                                            monitoring.TAX_TARIFF_PPh_FINAL = new double?(this.ConvertDouble(input) / 100.0);
                                        }
                                    }
                                    else if (cell.CellType == CellType.Numeric)
                                    {
                                        monitoring.TAX_TARIFF_PPh_FINAL = new double?(this.ConvertDouble(cell.NumericCellValue.ToString()));
                                    }
                                    else if (cell.CellType == CellType.Formula)
                                    {
                                        monitoring.TAX_TARIFF_PPh_FINAL = new double?(this.ConvertDouble(cell.NumericCellValue.ToString()));
                                    }
                                    else
                                    {
                                        errMesgs.Add($"Tarif PPH Final row {rownum + 1} format is incorrect");
                                    }
                                }
                            }
                            catch (Exception exception32)
                            {
                                errMesgs.Add($"Unable to get value of Tarif PPH Final at row {rownum + 1}, Error Mesg : {exception32.Message}");
                            }
                            try
                            {
                                cell = row.GetCell(++cellnum);
                                if ((cell != null) && (cell.CellType != CellType.Blank))
                                {
                                    if (cell.CellType == CellType.String)
                                    {
                                        monitoring.AMOUNT_PPh_FINAL = new double?(this.ConvertDouble(cell.StringCellValue));
                                    }
                                    else if (cell.CellType == CellType.Numeric)
                                    {
                                        monitoring.AMOUNT_PPh_FINAL = new double?(this.ConvertDouble(cell.NumericCellValue.ToString()));
                                    }
                                    else if (cell.CellType == CellType.Formula)
                                    {
                                        monitoring.AMOUNT_PPh_FINAL = new double?(this.ConvertDouble(cell.NumericCellValue.ToString()));
                                    }
                                    else
                                    {
                                        errMesgs.Add($"Total PPH Final row {rownum + 1} format is incorrect");
                                    }
                                }
                            }
                            catch (Exception exception33)
                            {
                                errMesgs.Add($"Unable to get value of Total PPH Final at row {rownum + 1}, Error Mesg : {exception33.Message}");
                            }
                            try
                            {
                                cell = row.GetCell(++cellnum);
                                if (cell != null)
                                {
                                    if (cell.CellType == CellType.Blank)
                                    {
                                        if (monitoring.TAX_CODE_PPh_FINAL != null)
                                        {
                                            errMesgs.Add($"Info PPH Final row {rownum + 1} is required");
                                        }
                                    }
                                    else if (cell.CellType == CellType.String)
                                    {
                                        monitoring.INFO_PPh_FINAL = cell.StringCellValue;
                                        num = this.getColumnLength(tableProperty, "INFO_PPh_FINAL");
                                        if (!string.IsNullOrEmpty(monitoring.INFO_PPh_FINAL) && (monitoring.INFO_PPh_FINAL.Length > num))
                                        {
                                            errMesgs.Add($"Info PPH Final at row {rownum + 1} character length should not be more than {num} characters");
                                        }
                                    }
                                    else if (cell.CellType != CellType.Numeric)
                                    {
                                        errMesgs.Add($"Info PPH Final row {rownum + 1} format is incorrect");
                                    }
                                    else
                                    {
                                        monitoring.INFO_PPh_FINAL = formatter.FormatCellValue(cell);
                                        num = this.getColumnLength(tableProperty, "INFO_PPh_FINAL");
                                        if (!string.IsNullOrEmpty(monitoring.INFO_PPh_FINAL) && (monitoring.INFO_PPh_FINAL.Length > num))
                                        {
                                            errMesgs.Add($"Info PPH Final at row {rownum + 1} character length should not be more than {num} characters");
                                        }
                                    }
                                }
                            }
                            catch (Exception exception34)
                            {
                                errMesgs.Add($"Unable to get value of Info PPH Final at row {rownum + 1}, Error Mesg : {exception34.Message}");
                            }

                            if (inputted_tax == false && monitoring.DPP_PPh_AMOUNT != null)
                            {
                                errMesgs.Add($"Tax at row {rownum + 1} is required");
                            }

                            double? jumlahTax = (monitoring.AMOUNT_PPh23 == null ? 0 : monitoring.AMOUNT_PPh23) + (monitoring.AMOUNT_PPh21_INTERN == null ? 0 : monitoring.AMOUNT_PPh21_INTERN)
                                                + (monitoring.AMOUNT_PPh21_EXTERN == null ? 0 : monitoring.AMOUNT_PPh21_EXTERN) + (monitoring.AMOUNT_PPh26 == null ? 0 : monitoring.AMOUNT_PPh26)
                                                +(monitoring.AMOUNT_PPh_FINAL == null ? 0 : monitoring.AMOUNT_PPh_FINAL);

                            double? jumlahTurnover = (monitoring.AMOUNT_TURN_OVER == null ? 0 : monitoring.AMOUNT_TURN_OVER) + (monitoring.AMOUNT_PPN == null ? 0 : monitoring.AMOUNT_PPN)
                                                + (monitoring.MATERAI == null ? 0 : monitoring.MATERAI);

                            if (jumlahTax >= jumlahTurnover)
                            {
                                errMesgs.Add($"Turnover at row {rownum + 1} is minus");
                            }

                            item.pVMonitorings.Add(monitoring);
                            list.Add(item);
                            goto TR_0007;
                        }
                        catch (Exception exception19)
                        {
                            errMesgs.Add($"Unable to get value of Tax Code PPH 23 at row {rownum + 1}, Error Mesg : {exception19.Message}");
                        }
                        return list;
                    }
                    catch (Exception exception15)
                    {
                        errMesgs.Add($"Unable to get value of Letter No at row {rownum + 1}, Error Mesg : {exception15.Message}");
                    }
                    return list;
                }
                goto TR_0007;
            }
            return list;
        }

        public static void SendEmailWorklistPV(string pid, bool toBudget)
        {
            string username, password, domain, host, port, subject, from, url, expired;

            try
            {
                using (var db = new DbHelper())
                {

                    username = db.ParameterRepository.Find(new { key_param = "email_username_chat_notification" }).FirstOrDefault().value1;
                    password = db.ParameterRepository.Find(new { key_param = "email_password_chat_notification" }).FirstOrDefault().value1;
                    domain = db.ParameterRepository.Find(new { key_param = "email_domain_chat_notification" }).FirstOrDefault().value1;
                    host = db.ParameterRepository.Find(new { key_param = "email_host_chat_notification" }).FirstOrDefault().value1;
                    port = db.ParameterRepository.Find(new { key_param = "email_port_chat_notification" }).FirstOrDefault().value1;

                    from = db.ParameterRepository.Find(new { key_param = "email_username" }).FirstOrDefault().value1;
                    url = db.ParameterRepository.Find(new { key_param = "pv_monitoring_url" }).FirstOrDefault().value1;
                    expired = db.ParameterRepository.Find(new { key_param = "pv_expired" }).FirstOrDefault().value1;

                    if (toBudget)
                    {
                        subject = db.EmailTemplateRepository.Find(new { module = "Notification work list for PIC Budget", mail_key = "email_key" }).FirstOrDefault().subject;
                    }
                    else {
                        subject = db.EmailTemplateRepository.Find(new { module = "Notification work list for PIC Dealer", mail_key = "email_key" }).FirstOrDefault().subject;
                    }

                    List<PVHeader> dataPVEmail = db.PVMonitoringRepository.GetDataPVEmail(pid, expired, toBudget);

                    List<PVHeader> groupedPVEmail = dataPVEmail
                        .GroupBy(b => b.GROUP)
                        .Select(s => s.First())
                        .ToList();
                    
                    foreach (PVHeader bp in groupedPVEmail)
                    {
                        HashSet<string> to = new HashSet<string>();
                        string body="";

                        if (toBudget)
                        {
                            body = emailBudgetBody(url, db, bp);

                            var users = db.PVMonitoringRepository.FindEmailByLevelCode(bp.GROUP);
                            HashSet<string> xxo = new HashSet<string>(users);

                            foreach (var usr in xxo)
                            {
                                if (!string.IsNullOrEmpty(usr))
                                {
                                    to.Add(usr);
                                }
                            }
                        }
                        else
                        {
                            body = emailDealerBody(url, db, bp);
                            var users = db.UserRepository.Find(new { customer_group = bp.GROUP });

                            foreach (var usr in users)
                            {
                                if (!string.IsNullOrEmpty(usr.email))
                                {
                                    to.Add(usr.email);
                                }
                            }
                        }

                        //EmailService emailSvc = new EmailService();
                        SmtpClient emailSvc = new SmtpClient();
                        emailSvc.Host = host;
                        emailSvc.Port = int.Parse(port);
                        //emailSvc.User = username;
                        //emailSvc.Password = password;
                        emailSvc.UseDefaultCredentials = false;
                        emailSvc.Credentials = new NetworkCredential(username, password);
                        emailSvc.EnableSsl = false;
                        if (from.Contains("gmail")) { emailSvc.EnableSsl = true; }

                        //EmailMessage emailMessage = new EmailMessage();
                        MailMessage emailMessage = new MailMessage(from, from);
                        emailMessage.From = new MailAddress(from);
                        emailMessage.To.RemoveAt(0);
                        emailMessage.Subject = subject;
                        emailMessage.Body = body;
                        emailMessage.IsBodyHtml = true;

                        if (to.Count > 0)
                        {
                            foreach (var mail in to)
                            {
                                emailMessage.To.Add(new MailAddress(mail));
                            }

                            emailSvc.Send(emailMessage);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private static string emailBudgetBody(string url, DbHelper db, PVHeader groupedPVEmail)
        {
            StringBuilder stringBuilder = new StringBuilder();
            string body = db.EmailTemplateRepository.Find(new { module = "Notification work list for PIC Budget", mail_key = "email_key" }).FirstOrDefault().mail_content;

            body = body.Replace("{0}", groupedPVEmail.TOTAL_ALL.ToString());
            body = body.Replace("{1}", groupedPVEmail.TOTAL_NEW.ToString());
            body = body.Replace("{2}", url);
            
            return body;
        }

        private static string emailDealerBody(string url, DbHelper db, PVHeader groupedPVEmail)
        {
            StringBuilder stringBuilder = new StringBuilder();
            string body = db.EmailTemplateRepository.Find(new { module = "Notification work list for PIC Dealer", mail_key = "email_key" }).FirstOrDefault().mail_content;

            body = body.Replace("{0}", groupedPVEmail.TOTAL_ALL.ToString());
            body = body.Replace("{1}", groupedPVEmail.TOTAL_NEW.ToString());
            body = body.Replace("{3}", url);
            body = body.Replace("{2}", groupedPVEmail.DAY_REMAINING);

            return body;
        }

        private static string BuildNotaReturBody(string url, DbHelper db, List<Invoice> availableInvoices, Invoice distinctInvoice)
        {
            StringBuilder stringBuilder = new StringBuilder();
            string body = db.EmailTemplateRepository.Find(new { module = "Notification Nota Retur Available", mail_key = "email_key" }).FirstOrDefault().mail_content;

            body = body.Replace("{url}", url);
            body = body.Replace("{NamaDealer}", distinctInvoice.customer_name);

            foreach (Invoice bpDetail in availableInvoices.Where(b => b.customer_group == distinctInvoice.customer_group))
            {
                stringBuilder.AppendFormat("<tr><td>{0}</td></tr>", bpDetail.invoice_number);
            }

            body = body.Replace("{Detail}", stringBuilder.ToString());
            return body;
        }

        public double ConvertDouble(string input)
        {
            string str = input.Trim().Replace(" ", "").Replace(",", ".");
            char[] separator = new char[] { '.' };
            string[] source = str.Split(separator);
            if (source.Count<string>() > 1)
            {
                str = string.Join("", source.Take<string>((source.Count<string>() - 1)).ToArray<string>());
                str = $"{str}.{source.Last<string>()}";
            }
            return double.Parse(str, CultureInfo.InvariantCulture);
        }
        #endregion

        #region Delete
        public JsonResult Delete(string pid, int clearDelete, List<PVHeader> xi)
        {
            bool result = false;
            List<string> msgerror = new List<string>();

            try
            {
                string pathErrTax = Server.MapPath("~/Content/template/err/");

                List<string> xpid = pid.Split(',').ToList();
                xpid.Remove("");
                string[] pidArr = xpid.ToArray();

                var pvMonitorings = Service<PVMonitoringService>().GetPVMonitoring(pidArr);
                if (pvMonitorings.Exists(b => b.STATUS_FLAG != "8" && b.STATUS_FLAG != "6"
                        && clearDelete != 1))
                {
                    List<string> au = new List<string>();

                    au = pvMonitorings
                            .Where(i => i.STATUS_FLAG != "8" && i.STATUS_FLAG != "6")
                            .Select(i => "Cannot delete PV No: " + i.PV_NO + " with Status: " + i.STATUS_DESC).ToList();

                    HashSet<string> errFilexls = new HashSet<string>(au);

                    if (errFilexls.Count() > 0)
                    {
                        string err = Service<PVMonitoringService>().ErrTxt(errFilexls, pathErrTax, "Error-Delete", "pv_errmegs_regular");
                        msgerror = new List<string>();
                        msgerror.Add(err);
                    }
                }
                else
                {
                    bool releaseBudget = true;
                    foreach (string x in pidArr)
                    {
                        int xz = int.Parse(x);

                        PVHeader pvHeader = Service<PVMonitoringService>().GetPVTHeader(xz);
                        if (pvHeader != null)
                        {
                            pvHeader.pvAttachments = new List<PVMonitoringAttachment>();
                        }
                        else
                        {
                            pvHeader = xi.Where(o => o.PROCESS_ID == Convert.ToInt16(x)).First();
                        }

                        if (pvHeader.BUDGET != null && pvHeader.BUDGET != "" && pvHeader.PV_NO != null)
                        {
                            MessageModel check = CheckBudget(pvHeader.BUDGET, xz, 3, true, pvHeader);
                            if (!check.success)
                            {
                                releaseBudget = false;
                                msgerror.Add(check.message);
                            }
                        }

                        pvHeader.pvAttachments = Service<PVMonitoringService>().GetPVAttachment(int.Parse(x));

                        if (pvHeader.pvAttachments.Count() > 0)
                        {
                            foreach (PVMonitoringAttachment pa in pvHeader.pvAttachments)
                            {
                                Service<PVMonitoringService>().DeleteFile(pa.REFERENCE_NO, pa.FILE_NAME);
                            }
                        }
                    }

                    if (releaseBudget)
                    {
                        string[] pvnoArr = pvMonitorings.Select(x => x.PV_NO).ToArray();
                        if (pvnoArr.Count() > 0)
                        {
                            result = Service<PVMonitoringService>().DeleteUnSubmittedPVMonitoring(pvnoArr, clearDelete);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result = false;
                msgerror.Add(ex.Message);
            }

            return Json(new { IsSuccess = result, MessageError = msgerror }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Save
        public JsonResult SaveUploadInitial(string invoiceNo, string pid, string bank_key,
                                IEnumerable<PVMonitoringAttachment> docsUpload, IEnumerable<PVMonitoring> pvDtl)
        {
            bool result = false;
            string msgerror = "";

            try
            {
                string pathErrTax = Server.MapPath("~/Content/template/err/");
                result = Service<PVMonitoringService>().SaveUpload(pid, bank_key, docsUpload, pvDtl, out msgerror, SessionManager.Current, pathErrTax);
            }
            catch (Exception ex)
            {
                result = false;
                msgerror = ex.Message;
            }            

            return Json(new { IsSuccess = result, MessageError = msgerror }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UploadAttachBank(string vendor)
        {
            bool result = false;
            string msgerror = "";

            HttpFileCollectionBase files = base.Request.Files;
            PVMonitoringService service = base.Service<PVMonitoringService>();

            if (files != null)
            {
                for (int i = 0; i < files.Count; i++)
                {
                    HttpPostedFileBase file = files[i];

                    result = service.UploadAttachBank(file, vendor, out msgerror);
                }
            }
            return base.Json(new
            {
                Result = result,
                MessageError = msgerror
            }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveBankAccountInitial(string pid, string vendor, string bank, string bankAccount, string beneficiaries)
        {
            bool result = false;
            string msgerror = "";

            try
            {
                bool onRequest = Service<BankAccountService>().GetBankAccount(vendor);

                if (onRequest)
                {
                    msgerror = "One or more bank account have been on request";
                }
                else
                {
                    result = Service<PVMonitoringService>().RegisterBankAccount(SessionManager.Current, pid, vendor, bank, bankAccount, beneficiaries, out msgerror);

                    if (result)
                    {
                        SendEmailRegisterBank();
                    }
                }
            }
            catch (Exception ex)
            {
                result = false;
                msgerror = ex.Message;
            }

            return Json(new { IsSuccess = result, MessageError = msgerror }, JsonRequestBehavior.AllowGet);
        }
       
        public void SendEmailRegisterBank()
        {
            string username, password, domain, host, port, subject, from;

            try
            {
                using (var db = new DbHelper())
                {
                    username = db.ParameterRepository.Find(new { key_param = "email_username_chat_notification" }).FirstOrDefault().value1;
                    password = db.ParameterRepository.Find(new { key_param = "email_password_chat_notification" }).FirstOrDefault().value1;
                    domain = db.ParameterRepository.Find(new { key_param = "email_domain_chat_notification" }).FirstOrDefault().value1;
                    host = db.ParameterRepository.Find(new { key_param = "email_host_chat_notification" }).FirstOrDefault().value1;
                    port = db.ParameterRepository.Find(new { key_param = "email_port_chat_notification" }).FirstOrDefault().value1;

                    from = db.ParameterRepository.Find(new { key_param = "email_username" }).FirstOrDefault().value1;

                    subject = db.EmailTemplateRepository.Find(new { module = "Register Bank Account Notification", mail_key = "email_key" }).FirstOrDefault().subject;

                    List<BankAccount> dataBankRequested = Service<BankAccountService>().GetDataBankRequested();

                    List<BankAccount> groupedEmail = dataBankRequested
                        .GroupBy(b => b.EMAIL_TO)
                        .Select(s => s.First())
                        .ToList();
                    
                    foreach (BankAccount bp in groupedEmail)
                    {
                        HashSet<string> to = new HashSet<string>();
                        string body = "";

                        body = emailBankReqBody(db, bp);
                        
                        if (!string.IsNullOrEmpty(bp.EMAIL_TO))
                        {
                            to.Add(bp.EMAIL_TO);
                        }

                        //EmailService emailSvc = new EmailService();
                        SmtpClient emailSvc = new SmtpClient();
                        emailSvc.Host = host;
                        emailSvc.Port = int.Parse(port);
                        //emailSvc.User = username;
                        //emailSvc.Password = password;
                        emailSvc.UseDefaultCredentials = false;
                        emailSvc.Credentials = new NetworkCredential(username, password);
                        emailSvc.EnableSsl = false;
                        if (from.Contains("gmail")) { emailSvc.EnableSsl = true; }

                        //EmailMessage emailMessage = new EmailMessage();
                        MailMessage emailMessage = new MailMessage(from, from);
                        emailMessage.From = new MailAddress(from);
                        emailMessage.To.RemoveAt(0);
                        emailMessage.Subject = subject;
                        emailMessage.Body = body;
                        emailMessage.IsBodyHtml = true;

                        if (to.Count > 0)
                        {
                            foreach (var mail in to)
                            {
                                emailMessage.To.Add(new MailAddress(mail));
                            }

                            emailSvc.Send(emailMessage);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private static string emailBankReqBody(DbHelper db, BankAccount groupedEmail)
        {
            StringBuilder stringBuilder = new StringBuilder();
            string body = db.EmailTemplateRepository.Find(new { module = "Register Bank Account Notification", mail_key = "email_key" }).FirstOrDefault().mail_content;

            body = body.Replace("{0}", groupedEmail.REQ_REMAINING.ToString());

            return body;
        }
        #endregion

        #region Submission
        public JsonResult Submission(string pid, IEnumerable<PVMonitoringAttachment> docsUpload)
        {
            bool result = false;
            bool budgetChecked = false;
            bool sendEmail = false;

            IList<string> msgError = new List<string>();
            HashSet<string> hashsetMsg = new HashSet<string>();
            string pathErrTax = base.Server.MapPath("~/Content/template/err/");

            try
            {
                List<PVMonitoringAttachment> docsUploadNew = new List<PVMonitoringAttachment>();
                List<string> xpid = pid.Split(',').ToList();
                xpid.Remove("");
                string[] pidArr = xpid.ToArray();

                foreach (string xz in pidArr)
                {
                    if(Service<PVMonitoringService>().InsertPVHD(int.Parse(xz), SessionManager.Current))
                    {
                        PVHeader pvHeader = Service<PVMonitoringService>().GetPVHeader(int.Parse(xz));

                        if (pvHeader != null)
                        {
                            string referenceNo = pvHeader.PV_NO + pvHeader.PV_YEAR;
                            string reffNoOld = "PID" + xz + pvHeader.PV_YEAR;

                            foreach (PVMonitoringAttachment xd in docsUpload)
                            {
                                xd.REFERENCE_NO = referenceNo;
                            }

                            result = Service<PVMonitoringService>().ReAttachment(docsUpload, reffNoOld);

                            string stsBudget = "";
                            string budgetMesg = "";

                            if (pvHeader.BUDGET != null
                                && pvHeader.BUDGET != ""
                                && pvHeader.PROCESS_ID != null
                                && result)
                            {
                                MessageModel model = this.CheckBudget(pvHeader.BUDGET, pvHeader.PROCESS_ID.Value, 1, false, new PVHeader());

                                if (model.success)
                                {
                                    List<string> budgetMesgArr = new List<string>();

                                    if (model.message == "AVAILABLE")
                                    {
                                        char[] separator = new char[] { ';' };
                                        budgetMesgArr = model.message.Split(separator).ToList<string>();
                                        budgetMesg = budgetMesgArr[0];
                                        stsBudget = budgetMesg;
                                        budgetChecked = true;
                                    }
                                    else
                                    {
                                        char[] separator = new char[] { ';' };
                                        budgetMesgArr = model.message.Split(separator).ToList<string>();
                                        budgetMesg = "NOT AVAILABLE";
                                        stsBudget = budgetMesgArr[0];
                                        budgetChecked = true;
                                    }

                                    if (budgetChecked)
                                    {
                                        base.Service<PVMonitoringService>().UpdateBudgetPVMonitoring(pvHeader.PROCESS_ID.Value, budgetMesg, pvHeader.BUDGET);

                                        if (budgetMesgArr.Count<string>() > 1)
                                        {
                                            if (budgetMesg == "NOT AVAILABLE")
                                            {
                                                hashsetMsg.Add("BUDGET: " + pvHeader.BUDGET + " -> " + budgetMesgArr[1] + " [SUBMISSION CANCELED]");
                                            }
                                            else
                                            {
                                                hashsetMsg.Add("BUDGET: " + pvHeader.BUDGET + " -> " + budgetMesgArr[1]);
                                            }
                                        }
                                        else
                                        {
                                            if (budgetMesg == "NOT AVAILABLE")
                                            {
                                                hashsetMsg.Add("BUDGET: " + pvHeader.BUDGET + " " + stsBudget + " [SUBMISSION CANCELED]");
                                            }
                                            else
                                            {
                                                hashsetMsg.Add("BUDGET: " + pvHeader.BUDGET + " " + stsBudget);
                                            }
                                        }

                                        result = model.success;
                                    }
                                }
                            }

                            if (result && budgetMesg == "AVAILABLE"
                                && pvHeader.BUDGET != null
                                && pvHeader.BUDGET != "")
                            {
                                sendEmail = true;
                            }
                            else if (result && budgetMesg != "AVAILABLE"
                                && pvHeader.BUDGET != null
                                && pvHeader.BUDGET != "")
                            {
                                sendEmail = false;
                            }
                            else
                            {
                                if (result && (pvHeader.BUDGET == null || pvHeader.BUDGET == ""))
                                {
                                    sendEmail = result;
                                }
                            }
                        }
                    }
                }

                if (result)
                {
                    if (sendEmail)
                    {
                        SendEmailWorklistPV(pid, true);
                    }

                    if (budgetChecked && (hashsetMsg.Count<string>() > 0))
                    {
                        msgError = new List<string> { base.Service<PVMonitoringService>().ErrTxt(hashsetMsg, pathErrTax, "Check-budget", "pv_success_budget")};
                    }
                }
            }
            catch (Exception ex)
            {
                result = false;
                msgError.Add(ex.Message);
            }
            
            return Json(new { IsSuccess = result, MessageError = msgError }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Verified
        public JsonResult VerifiedPIC(List<PVHeader> pvH)
        {
            bool result = false;
            IList<string> msgerror = new List<string>();

            try
            {
                string[] pidArr = pvH.Select(x => Convert.ToString(x.PROCESS_ID)).ToArray();
                string[] pvNo = pvH.Select(x => Convert.ToString(x.PV_NO)).ToArray();

                string pathErrTax = Server.MapPath("~/Content/template/err/");

                var pvMonitorings = Service<PVMonitoringService>().GetPVMonitoring(pidArr);

                if (pvMonitorings.Exists(b => b.STATUS_FLAG != "1") || pvMonitorings.Exists(b => b.STATUS_BUDGET == "NOT AVAILABLE"))
                {
                    List<string> au = new List<string>();
                    List<string> auTemp = new List<string>();

                    au = pvMonitorings
                            .Where(i => i.STATUS_FLAG != "1")
                            .Select(i => "Cannot verify PV No: " + i.PV_NO + " with Status: " + i.STATUS_DESC).ToList();

                    auTemp = pvMonitorings
                            .Where(i => i.STATUS_BUDGET == "NOT AVAILABLE")
                            .Select(i => "Cannot verify PV No: " + i.PV_NO + " with Budget Status: " + i.STATUS_BUDGET).ToList();

                    if (auTemp.Count() > 0)
                    {
                        foreach (string xo in auTemp)
                        {
                            au.Add(xo);
                        }
                    }

                    HashSet<string> errFilexls = new HashSet<string>(au);

                    if (errFilexls.Count() > 0)
                    {
                        string err = Service<PVMonitoringService>().ErrTxt(errFilexls, pathErrTax, "Error-Verified-Budget", "pv_errmegs_regular");
                        msgerror = new List<string>();
                        msgerror.Add(err);
                    }
                }
                else
                {
                    result = Service<PVMonitoringService>().VerifiedPIC(pvNo, pathErrTax, SessionManager.Current);

                    if (result)
                    {
                        if (pidArr.Count()>0)
                        {
                            string pid="";

                            foreach (string x in pidArr)
                            {
                                pid += x + ",";
                            }

                            SendEmailWorklistPV(pid, false);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result = false;
                msgerror.Add(ex.Message);
            }

            return Json(new { IsSuccess = result, MessageError = msgerror }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult NotProper(string pids, string pvNos, string reason)
        {
            bool result = false;
            List<string> msgerror = new List<string>();

            try
            {
                string pathErrTax = Server.MapPath("~/Content/template/err/");

                List<string> xpvno = pvNos.Split(',').ToList();
                xpvno.Remove("");
                string[] pvnoArr = xpvno.ToArray();

                List<string> xpid = pids.Split(',').ToList();
                xpid.Remove("");
                string[] pidArr = xpid.ToArray();

                var pvMonitorings = Service<PVMonitoringService>().GetPVMonitoring(pidArr);
                if (pvMonitorings.Exists(b => b.STATUS_FLAG != "1"))
                {
                    List<string> au = new List<string>();

                    au = pvMonitorings
                            .Where(i => i.STATUS_FLAG != "1")
                            .Select(i => "Cannot reject PV No: " + i.PV_NO + " with Status: " + i.STATUS_DESC).ToList();

                    HashSet<string> errFilexls = new HashSet<string>(au);

                    if (errFilexls.Count() > 0)
                    {
                        string err = Service<PVMonitoringService>().ErrTxt(errFilexls, pathErrTax, "Error-NotProper", "pv_errmegs_regular");
                        msgerror = new List<string>();
                        msgerror.Add(err);
                    }
                }
                else
                {
                    bool releaseBudget = true;
                    foreach (string x in pidArr)
                    {
                        int xz = int.Parse(x);
                        PVHeader pvHeader = Service<PVMonitoringService>().GetPVHeader(xz);

                        if (pvHeader.BUDGET != null && pvHeader.BUDGET != "")
                        {
                            MessageModel check = CheckBudget(pvHeader.BUDGET, xz, 3, true, pvHeader);
                            if (!check.success)
                            {
                                releaseBudget = false;
                                msgerror.Add(check.message);
                            }
                        }
                    }

                    if (releaseBudget)
                    {
                        result = Service<PVMonitoringService>().NotProper(pvnoArr, reason, SessionManager.Current);

                        if (result)
                        {
                            foreach (string pvno in pvnoArr)
                            {
                                SendEmailRejectedBudget(pvno);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result = false;
                msgerror.Add(ex.Message);
            }

            return Json(new { IsSuccess = result, MessageError = msgerror }, JsonRequestBehavior.AllowGet);
        }

        public void SendEmailRejectedBudget(string pvno)
        {
            string username, password, domain, host, port, subject, from, url;

            try
            {
                using (var db = new DbHelper())
                {
                    username = db.ParameterRepository.Find(new { key_param = "email_username_chat_notification" }).FirstOrDefault().value1;
                    password = db.ParameterRepository.Find(new { key_param = "email_password_chat_notification" }).FirstOrDefault().value1;
                    domain = db.ParameterRepository.Find(new { key_param = "email_domain_chat_notification" }).FirstOrDefault().value1;
                    host = db.ParameterRepository.Find(new { key_param = "email_host_chat_notification" }).FirstOrDefault().value1;
                    port = db.ParameterRepository.Find(new { key_param = "email_port_chat_notification" }).FirstOrDefault().value1;

                    from = db.ParameterRepository.Find(new { key_param = "email_username" }).FirstOrDefault().value1;
                    url = db.ParameterRepository.Find(new { key_param = "pv_monitoring_url" }).FirstOrDefault().value1;

                    subject = db.EmailTemplateRepository.Find(new { module = "Reject Notification", mail_key = "email_key" }).FirstOrDefault().subject;

                    List<PVHeader> dataPVRejected = db.PVMonitoringRepository.GetDataPVRejected(pvno);

                    List<PVHeader> groupedEmail = dataPVRejected
                        .GroupBy(b => b.CREATED_BY)
                        .Select(s => s.First())
                        .ToList();

                    foreach (PVHeader bp in groupedEmail)
                    {
                        HashSet<string> to = new HashSet<string>();
                        string body = "";

                        body = emailPVRejectedBody(url, db, bp);

                        var users = db.PVMonitoringRepository.FindEmailByUsername(bp.CREATED_BY);
                        HashSet<string> xxo = new HashSet<string>(users);

                        foreach (var usr in xxo)
                        {
                            if (!string.IsNullOrEmpty(usr))
                            {
                                to.Add(usr);
                            }
                        }

                        //EmailService emailSvc = new EmailService();
                        SmtpClient emailSvc = new SmtpClient();
                        emailSvc.Host = host;
                        emailSvc.Port = int.Parse(port);
                        //emailSvc.User = username;
                        //emailSvc.Password = password;
                        emailSvc.UseDefaultCredentials = false;
                        emailSvc.Credentials = new NetworkCredential(username, password);
                        emailSvc.EnableSsl = false;
                        if (from.Contains("gmail")) { emailSvc.EnableSsl = true; }

                        //EmailMessage emailMessage = new EmailMessage();
                        MailMessage emailMessage = new MailMessage(from, from);
                        emailMessage.From = new MailAddress(from);
                        emailMessage.To.RemoveAt(0);
                        emailMessage.Subject = subject;
                        emailMessage.Body = body;
                        emailMessage.IsBodyHtml = true;

                        if (to.Count > 0)
                        {
                            foreach (var mail in to)
                            {   
                                emailMessage.To.Add(new MailAddress(mail));
                            }

                            emailSvc.Send(emailMessage);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private static string emailPVRejectedBody(string url, DbHelper db, PVHeader groupedPVEmail)
        {
            StringBuilder stringBuilder = new StringBuilder();
            string body = db.EmailTemplateRepository.Find(new { module = "Reject Notification", mail_key = "email_key" }).FirstOrDefault().mail_content;

            body = body.Replace("{0}", groupedPVEmail.PV_NO.ToString());
            body = body.Replace("{1}", groupedPVEmail.REJECTED_REASON.ToString());
            body = body.Replace("{2}", url);

            return body;
        }

        public JsonResult Submit(string pvNos, string pids)
        {
            bool result = false;
            string msgerror = "";
            string pathErrTax = Server.MapPath("~/Content/template/err/");

            string[] pidArr = pids.Split(',');
            string[] pvNoArr = pvNos.Split(',');

            List<string> au = new List<string>();
            HashSet<string> err = new HashSet<string>();

            try
            {
                var pvMonitorings = Service<PVMonitoringService>().GetPVMonitoring(pidArr);

                foreach (PVMonitoring xi in pvMonitorings)
                {
                    if (xi.STATUS_FLAG != "42" && xi.STATUS_FLAG != "3")
                    {
                        string x1 = "Cannot submit PV No: " + xi.PV_NO + " with Status: " + xi.STATUS_DESC;
                        err.Add(x1);
                    }
                    else {
                        var listDocument = Service<PVMonitoringService>().GetDocumentFileByReference(xi.PROCESS_ID.ToString(), SessionManager.UserType).ToList();
                        bool a = listDocument.Any(o => o.BY_INTERNAL != "Y");

                        if (!a)
                        {
                            var x = listDocument.First();
                            string x2 = "PV NO: " + x.PV_NO + " is must upload invoice!";
                            err.Add(x2);
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(xi.INVOICE_NO))
                            {
                                string x3;

                                if (string.IsNullOrEmpty(xi.INVOICE_NO))
                                {
                                    x3 = "PV No: " + xi.PV_NO + " invoice no, invoice date is should not be empty";
                                }
                                else
                                {
                                    x3 = "PV No: " + xi.PV_NO + " invoice no is should not be empty";
                                }

                                err.Add(x3);
                            }
                        }
                    }
                }

                if (err.Count() > 0)
                {
                    throw new Exception(base.Service<PVMonitoringService>().ErrTxt(err, pathErrTax, "Error-Submit", "pv_errmegs_submit"));
                }
                else
                {
                    string checkEFBFlag;

                    using (var db = new DbHelper())
                    {
                        checkEFBFlag = db.ParameterRepository.Find(new { key_param = "pv_check_efb" }).FirstOrDefault().value1;
                    }

                    result = Service<PVMonitoringService>().Submit(pvNoArr, pidArr, pathErrTax, SessionManager.Current, checkEFBFlag);
                }
            }
            catch (Exception ex)
            {
                result = false;
                msgerror = ex.Message;
            }

            return Json(new { IsSuccess = result, MessageError = msgerror }, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}