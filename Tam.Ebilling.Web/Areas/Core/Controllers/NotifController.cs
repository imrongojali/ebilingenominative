﻿using Kendo.Mvc.UI;
using System.Net;
using System.Web.Mvc;
using Tam.Ebilling.Domain;
using Tam.Ebilling.Service;
using Tam.Ebilling.Infrastructure.Mvc;
using Tam.Ebilling.Web.Areas.Core.Models;
using Tam.Ebilling.Infrastructure.Session;

namespace Tam.Ebilling.Web.Areas.Core.Controllers
{
    public class NotifController : WebControllerBase
    {

        [HttpPost]
        public string GetNotif()
        {
            var customerCode = SessionManager.RoleStr;

            var result = Service<NotifService>().GetNotif(customerCode);
            return result;
        }

    }
}