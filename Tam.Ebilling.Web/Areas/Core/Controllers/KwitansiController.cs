﻿using Kendo.Mvc.UI;
using Microsoft.Reporting.WebForms;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tam.Ebilling.Domain;
using Tam.Ebilling.Infrastructure.Mvc;
using Tam.Ebilling.Infrastructure.Session;
using Tam.Ebilling.Infrastructure.Enum;
using Tam.Ebilling.Service;
using Agit.Helper.Helper;
using Tam.Ebilling.Infrastructure.Helper;
using System.Net;
using Tam.Ebilling.Infrastructure.Cache;
using Tam.Ebilling.Reporting;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

namespace Tam.Ebilling.Web.Areas.Core.Controllers
{
    public class KwitansiController : WebControllerBase
    {
        // GET: Core/Kwitansi
        public ActionResult Index()
        {
            var transactiontype = Service<TransactionTypeService>().GetTransactionType();
            ViewBag.TransactionType = transactiontype;

            var arstatus = Service<ARStatusService>().GetARStatus();
            ViewBag.ARStatus = arstatus;

            return View();
        }

        [HttpPost]
        public ActionResult Read([DataSourceRequest] Kendo.Mvc.UI.DataSourceRequest request)
        {
            var result = Service<InvoiceService>().GetViewDataSourceResult(request, Convert.ToInt16(EnumHelper.GetDescription(InvoiceType.Kwitansi)), SessionManager.RoleStr);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Excel_Export_Save(string contentType, string base64, string fileName)
        {
            var fileContents = Convert.FromBase64String(base64);

            string test = "2018-01-02";
            var splitted = test.Split('-');



            return File(fileContents, contentType, fileName);
        }

        public ActionResult KwitansiDetail([DataSourceRequest] Kendo.Mvc.UI.DataSourceRequest request, string invoice_number,
            int business_unit_id, string transaction_type, string pph_no, decimal total_amount, decimal dpp, decimal tax, decimal rate, string amount_pph, string currency, int transaction_type_id)
        {
            Invoice empObj = new Invoice();
            empObj.invoice_number = invoice_number;
            empObj.business_unit_id = business_unit_id;
            empObj.business_unit = business_unit_id.ToString();
            empObj.transaction_type = transaction_type;
            //string dpps = dpp.ToString("#,#.00#");
            empObj.dpp = dpp;
            empObj.tax = tax;
            empObj.total_amount = total_amount;
            empObj.pph_no = pph_no;
            empObj.rate = rate;
            empObj.amount_pph = amount_pph == null ? 0 : Convert.ToDecimal(amount_pph);
            empObj.currency = currency;
            empObj.transaction_type_id = transaction_type_id;

            return Json(new { empObj }, JsonRequestBehavior.AllowGet);
        }
    }
}