﻿using Agit.Helper.Helper;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Tam.Ebilling.Domain;
using Tam.Ebilling.Infrastructure.Adapter;
using Tam.Ebilling.Infrastructure.Cache;
using Tam.Ebilling.Infrastructure.Enum;
using Tam.Ebilling.Infrastructure.Helper;
using Tam.Ebilling.Infrastructure.Mvc;
using Tam.Ebilling.Infrastructure.Session;
using Tam.Ebilling.Service;

namespace Tam.Ebilling.Web.Areas.Core.Controllers
{
    public class ScanInvoiceController : WebControllerBase
    {
        public ActionResult UploadFileInitial(string invoice_number)
        {
            //var Model = Service<AttachmentNotaReturService>().GetAttachment(reference);
            var listDocument = Service<ScanInvoiceService>().GetInvoiceNumber(invoice_number);

            ViewBag.ListDocument = listDocument;
            ViewBag.InvoiceNo = invoice_number;
            //ViewBag.Reference = reference;

            var url = "~/Areas/Core/Views/Invoice/Form/ScanInvoiceUpload.cshtml";
            DownloadFinish();
            return PartialView(url);
        }
        public ActionResult UploadFileInitialScan(string invoice_number)
        {
            //var Model = Service<AttachmentNotaReturService>().GetAttachment(reference);
            var listDocumentScan = Service<ScanInvoiceService>().GetInvoiceNumberNotur(invoice_number);

            ViewBag.ListDocumentScan = listDocumentScan;
            ViewBag.InvoiceNo = invoice_number;
            //ViewBag.Reference = reference;

            var url = "~/Areas/Core/Views/NotaRetur/Form/ScanInvoiceNoturUpload.cshtml";
            return PartialView(url);
        }
        [HttpPost]
        public ActionResult GetListDocuments(string invoice)
        {
            ViewBag.InvoiceNo = invoice;
            var result = Service<ScanInvoiceService>().GetDocumentFileByReference(ViewBag.InvoiceNo);

            var ds = new DataSourceResult
            {
                Data = result
            };
            return Json(ds, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetListDocumentsScan(string invoice)
        {
            ViewBag.InvoiceNo = invoice;
            var result = Service<ScanInvoiceNoturService>().GetDocumentFileByInvoice(ViewBag.InvoiceNo);

            var ds = new DataSourceResult
            {
                Data = result
            };
            return Json(ds, JsonRequestBehavior.AllowGet);
        }
        public JsonResult AllocationRead(string invoice_number)
        {
            var result = Service<ScanInvoiceService>().GetInvoiceNumber(invoice_number);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ScanRead(string invoice_number)
        {
            var result = Service<ScanInvoiceService>().GetInvoiceNumberNotur(invoice_number);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Result()
        {
            return View();
        }
        public JsonResult Upload(string invoice)
        {
            string msgerror = "";

            var files = Request.Files;
            var arrayObjects = new List<Tam.Ebilling.Domain.ScanInvoice>();
            var uploadService = Service<ScanInvoiceService>();

            if (files != null && files.Count > 0)
            {
                for (var i = 0; i < files.Count; i++)
                {
                    var file = files[i];

                    var doc = uploadService.Upload(new HttpPostedFileBaseAdapter(file), invoice, out msgerror);

                    arrayObjects.Add(doc);
                }
            }
            return Json(new { Result = arrayObjects, MessageError = msgerror }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult UploadScanNotur(string invoice)
        {
            string msgerror = "";

            var files = Request.Files;
            var arrayObjects = new List<Tam.Ebilling.Domain.ScanInvoiceNotur>();
            var uploadService = Service<ScanInvoiceService>();

            if (files != null && files.Count > 0)
            {
                for (var i = 0; i < files.Count; i++)
                {
                    var file = files[i];

                    var doc = uploadService.UploadScanNotur(new HttpPostedFileBaseAdapter(file), invoice, out msgerror);

                    arrayObjects.Add(doc);
                }
            }
            return Json(new { Result = arrayObjects, MessageError = msgerror }, JsonRequestBehavior.AllowGet);
        }
        private IEnumerable<string> Get_File_Info(IEnumerable<HttpPostedFileBase> files)
        {
            return
                from a in files
                where a != null
                select string.Format("{0} ({1} bytes)", Path.GetFileName(a.FileName), a.ContentLength);
        }
        public JsonResult SaveUploadInvoice(string invoice, IEnumerable<ScanInvoiceNotur> Docs)
        {
            string msgerror = "";
            var result = Service<ScanInvoiceNoturService>().SaveUploadScanNotur(invoice, Docs, out msgerror);
            return Json(new { IsSuccess = result, MessageError = msgerror }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SaveUploadInitial(string invoice, IEnumerable<ScanInvoice> Docs)
        {
            string msgerror = "";
            var result = Service<ScanInvoiceService>().SaveUpload(invoice, Docs, out msgerror);
            return Json(new { IsSuccess = result, MessageError = msgerror }, JsonRequestBehavior.AllowGet);
        }
        public void Document()
        {
            var username = ApplicationCacheManager.GetConfig<string>("FileUploadUserName");
            var password = ApplicationCacheManager.GetConfig<string>("FileUploadPassword");
            var domain = ApplicationCacheManager.GetConfig<string>("FileUploadDomain");
            var userdomain = domain + "\\" + username;
            var pers = new Impersonation();
            pers.Impersonate(userdomain, password);
            var credential = new NetworkCredential(userdomain, password);
            //var uploadFolder = "E:\\BES\\Attachment\\NotaRetur";

            var uploadFolder = ApplicationCacheManager.GetConfig<string>("FileUploadScan");
            using (new NetworkConnection(uploadFolder, credential))
            {
                var id = GetQueryString<string>("id");
                var doc = Service<ScanInvoiceService>().GetDocumentByID(id.ToString()).FirstOrDefault();

                var url = Path.Combine(uploadFolder, doc.filename);

                Response.Clear();
                Response.AddHeader("Access-Control-Allow-Origin", "*");
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("content-disposition", "attachment;filename=" + Path.GetFileName(doc.filename));
                Response.WriteFile(url);
                Response.Flush();
                Response.End();
            }
        }
        public void DocumentScan()
        {
            var username = ApplicationCacheManager.GetConfig<string>("FileUploadUserName");
            var password = ApplicationCacheManager.GetConfig<string>("FileUploadPassword");
            var domain = ApplicationCacheManager.GetConfig<string>("FileUploadDomain");
            var userdomain = domain + "\\" + username;
            var pers = new Impersonation();
            pers.Impersonate(userdomain, password);
            var credential = new NetworkCredential(userdomain, password);
            //var uploadFolder = "E:\\BES\\Attachment\\NotaRetur";

            var uploadFolder = ApplicationCacheManager.GetConfig<string>("FileUploadScanNotur");
            using (new NetworkConnection(uploadFolder, credential))
            {
                var id = GetQueryString<string>("id");
                var doc = Service<ScanInvoiceNoturService>().GetDocumentByID(id.ToString()).FirstOrDefault();

                var url = Path.Combine(uploadFolder, doc.filename);

                Response.Clear();
                Response.AddHeader("Access-Control-Allow-Origin", "*");
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("content-disposition", "attachment;filename=" + Path.GetFileName(doc.filename));
                Response.WriteFile(url);
                Response.Flush();
                Response.End();
            }
        }
        public void Temp()
        {
            var username = ApplicationCacheManager.GetConfig<string>("FileUploadUserName");
            var password = ApplicationCacheManager.GetConfig<string>("FileUploadPassword");
            var domain = ApplicationCacheManager.GetConfig<string>("FileUploadDomain");
            var userdomain = domain + "\\" + username;
            var pers = new Impersonation();
            pers.Impersonate(userdomain, password);
            var credential = new NetworkCredential(userdomain, password);
            //var uploadFolder = "E:\\NotaRetur";
            var uploadFolder = ApplicationCacheManager.GetConfig<string>("FileUploadScanTemp");
            using (new NetworkConnection(uploadFolder, credential))
            {
                //var filepath = GetQueryString<string>("filepath").ToString();
                var filename = GetQueryString<string>("filename").ToString();

                var url = Path.Combine(uploadFolder, filename);

                Response.Clear();
                Response.AddHeader("Access-Control-Allow-Origin", "*");
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("content-disposition", "attachment;filename=" + Path.GetFileName(filename));
                Response.WriteFile(url);
                Response.Flush();
                Response.End();
            }
        }
        public void TempScan()
        {
            var username = ApplicationCacheManager.GetConfig<string>("FileUploadUserName");
            var password = ApplicationCacheManager.GetConfig<string>("FileUploadPassword");
            var domain = ApplicationCacheManager.GetConfig<string>("FileUploadDomain");
            var userdomain = domain + "\\" + username;
            var pers = new Impersonation();
            pers.Impersonate(userdomain, password);
            var credential = new NetworkCredential(userdomain, password);
            //var uploadFolder = "E:\\NotaRetur";
            var uploadFolder = ApplicationCacheManager.GetConfig<string>("FileUploadScanNoturTemp");
            using (new NetworkConnection(uploadFolder, credential))
            {
                //var filepath = GetQueryString<string>("filepath").ToString();
                var filename = GetQueryString<string>("filename").ToString();

                var url = Path.Combine(uploadFolder, filename);

                Response.Clear();
                Response.AddHeader("Access-Control-Allow-Origin", "*");
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("content-disposition", "attachment;filename=" + Path.GetFileName(filename));
                Response.WriteFile(url);
                Response.Flush();
                Response.End();
            }
        }
        public void Download(string invoice_number)
        {
            var username = Service<ParameterService>().GetParameter("file_impersonate_user").value1;
            var domain = Service<ParameterService>().GetParameter("file_impersonate_domain").value1;
            var userdomain = domain + "\\" + username;
            var password = Service<ParameterService>().GetParameter("file_impersonate_password").value1;

            var credential = new NetworkCredential(userdomain, password);
            string fileserver = Service<ParameterService>().GetParameter("file_server").value1;
            string filepath = Service<ParameterService>().GetParameter("file_scan_path").value1;
            string file2 = "\\\\" + fileserver + "\\" + filepath;
            string user = SessionManager.Current;
            try
            {
                using (new NetworkConnection(file2, credential))

                {
                    var doc = Service<ScanInvoiceService>().GetAttachment(invoice_number);
                    var outputfile = "Invoice - " + DateTime.Now.ToString("yyyyMMdd") + ".zip";
                    var zipStream = new MemoryStream();
                    if (doc.Count() == 0)
                    {
                        DownloadFinish(false);
                        throw new Exception("Invoice doesn't have attachment");
                    }
                    var item2 = doc.First();
                    var filename2 = item2.filename;
                    var url2 = Path.Combine(file2, filename2);
                    if (System.IO.File.Exists(url2) == true)
                    {
                        if (doc.Count > 1)
                        {
                            var zip = new ZipArchive(zipStream, ZipArchiveMode.Create);
                            foreach (var item in doc)
                            {
                                var filename = item.filename;
                                var url = Path.Combine(file2, filename);
                                zip.CreateEntryFromFile(url, "Invoice - " + item.invoice_number.Replace('/', '-') + "\\" + item.filename);
                            }
                            zip.Dispose();

                        }
                        else
                        {
                            var item = doc.First();
                            var filename = item.filename;
                            var url = Path.Combine(file2, filename);
                            var file = new FileStream(url, FileMode.Open); byte[] bytes = new byte[file.Length];
                            file.Read(bytes, 0, (int)file.Length);
                            zipStream.Write(bytes, 0, (int)file.Length);
                            outputfile = item.filename;
                        }
                        DownloadFinish();
                        Response.Clear();
                        Response.AddHeader("Access-Control-Allow-Origin", "*");
                        Response.ContentType = "application/octet-stream";
                        Response.AddHeader("content-disposition", "attachment;filename=" + outputfile);
                        Response.BinaryWrite(zipStream.ToArray());
                        Response.Flush();
                        Response.End();
                        var Update = Service<InvoiceService>().GetUpdateInvoice(invoice_number, user);
                    }
                    else if (System.IO.File.Exists(url2) == false)
                    {
                        DownloadFinish(false);
                        throw new Exception("Invoice doesn't have attachment");
                    }
                }
            }
            catch (Exception e)
            {
                DownloadFinish(false);
                throw new Exception(e.Message);
            }

        }
        public void DownloadInvoiceNotur(string invoice_number)
        {
            var username = Service<ParameterService>().GetParameter("file_impersonate_user").value1;
            var domain = Service<ParameterService>().GetParameter("file_impersonate_domain").value1;
            var userdomain = domain + "\\" + username;
            var password = Service<ParameterService>().GetParameter("file_impersonate_password").value1;

            var credential = new NetworkCredential(userdomain, password);
            string fileserver = Service<ParameterService>().GetParameter("file_server").value1;
            string filepath = Service<ParameterService>().GetParameter("file_invoice_notur_path").value1;
            string file2 = "\\\\" + fileserver + "\\" + filepath;
            string user = SessionManager.Current;
            try
            {
                using (new NetworkConnection(file2, credential))

                {
                    var doc = Service<ScanInvoiceNoturService>().GetAttachment(invoice_number);
                    var outputfile = "Invoice - " + DateTime.Now.ToString("yyyyMMdd") + ".zip";
                    var zipStream = new MemoryStream();
                    var item2 = doc.First();
                    var filename2 = item2.filename;
                    var url2 = Path.Combine(file2, filename2);
                    if (System.IO.File.Exists(url2) == true)
                    {
                        if (doc.Count > 1)
                        {
                            var zip = new ZipArchive(zipStream, ZipArchiveMode.Create);

                            foreach (var item in doc)
                            {
                                var filename = item.filename;
                                var url = Path.Combine(file2, filename);
                                zip.CreateEntryFromFile(url, "Invoice - " + item.invoice_number.Replace('/', '-') + "\\" + item.filename);
                            }
                          
                            zip.Dispose();
                        }
                        else
                        {
                            var item = doc.First();
                            var filename = item.filename;
                            var url = Path.Combine(file2, filename);
                            var file = new FileStream(url, FileMode.Open); byte[] bytes = new byte[file.Length];
                            file.Read(bytes, 0, (int)file.Length);
                            zipStream.Write(bytes, 0, (int)file.Length);
                            outputfile = item.filename;
                        }
                        DownloadFinish();
                        var Update = Service<InvoiceService>().GetUpdateInvoice(invoice_number, user);
                        Response.Clear();
                        Response.AddHeader("Access-Control-Allow-Origin", "*");
                        Response.ContentType = "application/octet-stream";
                        Response.AddHeader("content-disposition", "attachment;filename=" + outputfile);
                        Response.BinaryWrite(zipStream.ToArray());
                        Response.Flush();
                        Response.End();
                    }
                    else if (System.IO.File.Exists(url2) == false)
                    {
                        DownloadFinish();
                        throw new Exception("Invoice doesn't have attachment");
                    }
                }
            }
            catch (Exception e)
            {
                DownloadFinish();
                throw new Exception(e.Message);
            }

        }
        public void DownloadAllInvoice([DataSourceRequest] DataSourceRequest request)
        {
            request.Page = 0;
            request.PageSize = 0;

            var username = Service<ParameterService>().GetParameter("file_impersonate_user").value1;
            var domain = Service<ParameterService>().GetParameter("file_impersonate_domain").value1;
            var userdomain = domain + "\\" + username;
            var password = Service<ParameterService>().GetParameter("file_impersonate_password").value1;

            var credential = new NetworkCredential(userdomain, password);
            string fileserver = Service<ParameterService>().GetParameter("file_server").value1;
            string filepath = Service<ParameterService>().GetParameter("file_scan_path").value1;
            string file = "\\\\" + fileserver + "\\" + filepath;
            var user = SessionManager.Current;
            try
            {
                using (new NetworkConnection(file, credential))
                {
                    //var result = Service<InvoiceService>().GetViewDataSourceResult(request, Convert.ToInt16(EnumHelper.GetDescription(InvoiceType.Invoice)), SessionManager.RoleStr);
                    var result = Service<InvoiceService>().GetGridInitDataSourceResult(request, Convert.ToInt16(EnumHelper.GetDescription(InvoiceType.Invoice)), SessionManager.RoleStr);

                    var outputfile = "Invoice - " + DateTime.Now.ToString("yyyyMMdd") + ".zip";
                    var zipStream = new MemoryStream();
                    var zip = new ZipArchive(zipStream, ZipArchiveMode.Create);

                    // Convert IEnumerable ke List
                    List<Invoice> data = result.Data.Cast<Invoice>().ToList();

                    //var invoice = new ScanInvoice();
                    //var invo = new Invoice();
                    //var IsPrint = true;

                    //int totalData = data.Count;
                    //var document = Service<ScanInvoiceService>().GetAttachment(invoice.invoice_number);
                    //int mvData = document.Where(x => x.invoice_number == null).Count();
                    //// 
                    //IsPrint = totalData == mvData ? false : true;

                    //if (IsPrint)
                    //{

                        foreach (Invoice inv in result.Data)
                        {
                            var id = inv.invoice_number;
                            if (id != "")
                            {
                                var unit = inv.business_unit;
                                var doc = Service<ScanInvoiceService>().GetAttachment(id);
                                if (doc.Count > 0)
                                {
                                    var item = doc.FirstOrDefault();
                                    var filename = item.filename;
                                    var url = Path.Combine(file, filename);

                                    if (System.IO.File.Exists(url) == true)
                                    {
                                        zip.CreateEntryFromFile(url, "Invoice - " + DateTime.Now.ToString("yyyyMMdd") + "\\" + "Invoice - " + inv.invoice_number.Replace('/', '-') + "\\" + item.filename);
                                    var Update = Service<InvoiceService>().GetUpdateInvoice(inv.invoice_number, user);
                                }
                                    else
                                    {
                                        zip.CreateEntry("Invoice - " + DateTime.Now.ToString("yyyyMMdd") + "\\" + "Invoice - " + inv.invoice_number.Replace('/', '-') + "\\" + Path.GetFileNameWithoutExtension(item.filename) + ".NOTFOUND");
                                    }
                                }
                                else
                                {
                                    zip.CreateEntry("Invoice - " + DateTime.Now.ToString("yyyyMMdd") + "\\" + "Invoice - " + inv.invoice_number.Replace('/', '-') + ".NOTFOUND");
                                }

                            }
                            else
                            {

                            }

                        }
                        DownloadFinish();
                        zip.Dispose();
                        Response.Clear();
                        Response.AddHeader("Access-Control-Allow-Origin", "*");
                        Response.ContentType = "application/octet-stream";
                        Response.AddHeader("content-disposition", "attachment;filename=" + outputfile);
                        Response.BinaryWrite(zipStream.ToArray());
                        Response.Flush();
                        Response.End();
                  
                    //}
                }
            }
            catch (Exception e)
            {
                DownloadFinish();
                throw new Exception(e.Message);
            }

        }
        public void DownloadAllInvoiceNotur([DataSourceRequest] DataSourceRequest request)
        {
            request.Page = 0;
            request.PageSize = 0;

            var username = Service<ParameterService>().GetParameter("file_impersonate_user").value1;
            var domain = Service<ParameterService>().GetParameter("file_impersonate_domain").value1;
            var userdomain = domain + "\\" + username;
            var password = Service<ParameterService>().GetParameter("file_impersonate_password").value1;

            var credential = new NetworkCredential(userdomain, password);
            string fileserver = Service<ParameterService>().GetParameter("file_server").value1;
            string filepath = Service<ParameterService>().GetParameter("file_invoice_notur_path").value1;
            string file = "\\\\" + fileserver + "\\" + filepath;
            var user = SessionManager.Current;
            try
            {
                using (new NetworkConnection(file, credential))
                {
                    var result = Service<InvoiceService>().GetViewDataSourceResult(request, Convert.ToInt16(EnumHelper.GetDescription(InvoiceType.NotaRetur)), SessionManager.RoleStr);

                    var outputfile = "Invoice - " + DateTime.Now.ToString("yyyyMMdd") + ".zip";
                    var zipStream = new MemoryStream();
                    var zip = new ZipArchive(zipStream, ZipArchiveMode.Create);

                    // Convert IEnumerable ke List
                    List<Invoice> data = result.Data.Cast<Invoice>().ToList();

                    //var invoice = new Invoice();
                    //var IsPrint = true;
                    //int totalData = data.Count;
                    //var document = Service<ScanInvoiceService>().GetAttachment(invoice.invoice_number);
                    //int mvData = document.Where(x => x.invoice_number == null).Count();
                    //// 
                    //IsPrint = totalData == mvData ? false : true;

                    //if (IsPrint)
                    //{
                    foreach (Invoice inv in result.Data)
                    {
                        var id = inv.invoice_number;
                        if (id != "")
                        {
                            var unit = inv.business_unit;
                            var doc = Service<ScanInvoiceNoturService>().GetAttachment(id);
                            if (doc.Count > 0)
                            {
                                var item = doc.FirstOrDefault();
                                var filename = item.filename;
                                var url = Path.Combine(file, filename);

                                if (System.IO.File.Exists(url) == true)
                                {
                                    zip.CreateEntryFromFile(url, "Invoice - " + DateTime.Now.ToString("yyyyMMdd") + "\\" + "Invoice - " + inv.invoice_number.Replace('/', '-') + "\\" + item.filename);
                                    var Update = Service<InvoiceService>().GetUpdateInvoice(inv.invoice_number, user);
                                }
                                else
                                {
                                    zip.CreateEntry("Invoice - " + DateTime.Now.ToString("yyyyMMdd") + "\\" + "Invoice - " + inv.invoice_number.Replace('/', '-') + "\\" + Path.GetFileNameWithoutExtension(item.filename) + ".NOTFOUND");
                                }
                            }
                            else
                            {

                            }
                            
                        }
                        else
                        {

                        }

                    }
                    zip.Dispose();
                    Response.Clear();
                    Response.AddHeader("Access-Control-Allow-Origin", "*");
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("content-disposition", "attachment;filename=" + outputfile);
                    Response.BinaryWrite(zipStream.ToArray());
                    Response.Flush();
                    Response.End();
                   
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

        }
    }
}