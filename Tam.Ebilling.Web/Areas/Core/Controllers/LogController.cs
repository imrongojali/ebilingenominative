﻿using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tam.Ebilling.Infrastructure.Mvc;
using Tam.Ebilling.Service;

namespace Tam.Ebilling.Web.Areas.Core.Controllers
{
    public class LogController : WebControllerBase
    {
        // GET: Core/Log
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Read([DataSourceRequest] Kendo.Mvc.UI.DataSourceRequest request)
        {
            var result = Service<LogService>().GetDataSourceResult(request);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}