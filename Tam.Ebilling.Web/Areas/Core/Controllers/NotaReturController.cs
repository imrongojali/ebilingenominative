﻿using Agit.Helper.Helper;
using Kendo.Mvc.UI;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using Tam.Ebilling.Domain;
using Tam.Ebilling.Infrastructure.Enum;
using Tam.Ebilling.Infrastructure.Mvc;
using Tam.Ebilling.Infrastructure.Session;
using Tam.Ebilling.Service;
using Tam.Ebilling.Infrastructure.Cache;
using System.Drawing;
using QRCoder;
using Tam.Ebilling.Infrastructure.Helper;
using Tam.Ebilling.Infrastructure.Barcode;
using System.Net;
using System.IO.Compression;
using Tam.Ebilling.Domain.Modules.Core.Model;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System.Globalization;
using Tam.Ebilling.Web.Areas.Core.Models;
using System.Data;
using NPOI.SS;
using System.Text.RegularExpressions;
using Agit.Helper.Extension;

namespace Tam.Ebilling.Web.Areas.Core.Controllers
{
    public class NotaReturController : CommonFeatureController
    {
        // GET: Core/NotaRetur
        public NotaReturController()
        {
            try
            {
                ViewBag.Notif_Days = Service<ParameterService>().GetParameterNotif("NOTIF_DAYS_FOR_VATCN").parameter_value;
                //ViewBag.ID = "201915594/2019000104";
            }
            catch (Exception)
            {

                ViewBag.Notif_Days = "0";
            }


        }

        public ActionResult Index()
        {
            var notifFilter = Request.QueryString["notifFilter"];
            var id = Request.QueryString["id"];
            if (notifFilter != null)
            {
                ViewBag.NotifFilter = notifFilter;
            }
            else
            {
                ViewBag.NotifFilter = "";
            }
            if (id != null)
            {
                ViewBag.ID = id;
            }
            else
            {
                ViewBag.ID = null;
            }

            var transactiontype = Service<TransactionTypeService>().GetTransactionType();
            ViewBag.TransactionType = transactiontype;

            var arstatus = Service<ARStatusService>().GetARStatus();
            ViewBag.ARStatus = arstatus;

            var customer = Service<CustomerService>().GetCustomer();
            ViewBag.CustomerName = customer;

            var dealercode = Service<DealerCodeService>().GetDealerCode();
            ViewBag.DealerCode = dealercode;

            var cust = SessionManager.RoleStr;
            ViewBag.Customer = cust;

            ViewBag.UserType = SessionManager.UserType;
            ViewBag.IsAllowedExportToExcel = IsAllowed("export_to_excel") || ViewBag.UserType == "Customer";
            ViewBag.IsAllowedDownloadAll = IsAllowed("download_all") || ViewBag.UserType == "Customer";
            ViewBag.IsAllowedDownloadInvoice = IsAllowed("download_invoice") || ViewBag.UserType == "Customer";
            ViewBag.IsAllowedDownloadInvoiceQR = IsAllowed("download_invoice_qr") || ViewBag.UserType == "Customer";
            ViewBag.IsAllowedDownloadAttachment = IsAllowed("download_attachment") || ViewBag.UserType == "Customer";
            ViewBag.IsAllowedDownloadPI = IsAllowed("download_pi") || ViewBag.UserType == "Customer";
            ViewBag.IsAllowedDownloadNotaRetur = IsAllowed("download_nota_retur") || ViewBag.UserType == "Customer";
            ViewBag.IsAllowedUploadNotaRetur = IsAllowed("upload_nota_retur") || ViewBag.UserType == "Customer";
            ViewBag.IsAllowedUploadInvoice = IsAllowed("upload_invoice") || ViewBag.UserType == "Customer";

            ViewBag.IsAllowedDownloadCreditNote = "True";


            return View();
        }

        public void RemoveSQLInjectString(string strWords)
        {
            strWords = Regex.Replace(strWords, @"[^\u0000-\u007F]+", string.Empty);

            bool isError = false;
            List<string> badChars = new List<string>() { "select", "drop", ";", "--", "insert", "delete", "xp_", "¿" };

            for (int i = 0; i < badChars.Count(); i++)
            {
                if (strWords.Contains(badChars[i]))
                    isError = true;
                break;
            }

            if (isError)
                throw new Exception("Invalid character detected :  " + strWords + "");
        }

        [HttpPost]
        public ActionResult Read([DataSourceRequest] Kendo.Mvc.UI.DataSourceRequest request, string notifFilter = null)
        {

            foreach (var item in request.Filters)
            {

                if (item.GetType().Name == "CompositeFilterDescriptor")
                {
                    var lists = (Kendo.Mvc.CompositeFilterDescriptor)item;
                    foreach (var i in lists.FilterDescriptors.ToList())
                    {
                        if (i.GetType().Name == "FilterDescriptor")
                        {
                            Kendo.Mvc.FilterDescriptor ii = (Kendo.Mvc.FilterDescriptor)i;

                            if (ii.Member == "doc_date" && ii.Operator == Kendo.Mvc.FilterOperator.IsLessThanOrEqualTo)
                            {
                                ii.Value = DateTime.Parse(ii.Value.ToString().Replace("00:00:00", "23:59:59"));
                            }
                        }
                        else
                        {
                            Kendo.Mvc.CompositeFilterDescriptor iii = (Kendo.Mvc.CompositeFilterDescriptor)i;

                            foreach (var iiii in iii.FilterDescriptors.ToList())
                            {
                                if (iiii.GetType().Name == "FilterDescriptor")
                                {
                                    Kendo.Mvc.FilterDescriptor iiiii = (Kendo.Mvc.FilterDescriptor)iiii;

                                    if (iiiii.Member == "doc_date" && iiiii.Operator == Kendo.Mvc.FilterOperator.IsLessThanOrEqualTo)
                                    {
                                        iiiii.Value = DateTime.Parse(iiiii.Value.ToString().Replace("00:00:00", "23:59:59"));
                                    }

                                }
                            }
                        }
                    }
                }

            }


            var result = Service<NoturService>().GetViewDataSourceResult(request, Convert.ToInt16(EnumHelper.GetDescription(InvoiceType.NotaRetur)), SessionManager.RoleStr, null, true, notifFilter);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult TaxNotaRetur([DataSourceRequest] Kendo.Mvc.UI.DataSourceRequest request, string invoice_number,
            int business_unit_id, string tax_invoice)
        {
            Invoice taxObjj = new Invoice();
            taxObjj.invoice_number = invoice_number;
            taxObjj.business_unit = business_unit_id.ToString();
            taxObjj.tax_invoice = tax_invoice;
            // return PartialView("~/Areas/Core/Views/Invoice/Form/TaxInvoice.cshtml", taxObjj);
            return PartialView("~/Areas/Core/Views/NotaRetur/Form/TaxNotaRetur.cshtml", taxObjj);
        }

        public ActionResult GetPDF([DataSourceRequest] Kendo.Mvc.UI.DataSourceRequest request, string id)
        {
            var pdfid = Service<NoturService>().GetDataPDF(id);
            string linkdata = string.Empty;
            string namafile = string.Empty;
            string url = string.Empty;
            var username = Service<ParameterService>().GetParameter("file_impersonate_user").value1;
            var domain = Service<ParameterService>().GetParameter("file_impersonate_domain").value1;
            var userdomain = domain + "\\" + username;
            var password = Service<ParameterService>().GetParameter("file_impersonate_password").value1;

            var credential = new NetworkCredential(userdomain, password);
            string fileserver = Service<ParameterService>().GetParameter("file_server").value1;
            string filepath = Service<ParameterService>().GetParameter("file_repo_path_tax").value1;
            string file = "\\\\" + fileserver + "\\" + filepath;


            using (new NetworkConnection(file, credential))
            {
                foreach (var datatax in pdfid)
                {
                    linkdata = datatax.tax_attc_url;
                    namafile = Path.GetFileNameWithoutExtension(linkdata) + ".pdf";
                    url = Path.Combine(file, namafile);
                }

                if (System.IO.File.Exists(url) == true)
                {
                    FileStream fs = new FileStream(url, FileMode.Open, FileAccess.Read);
                    return File(fs, "application/pdf");
                }
                else
                {
                    namafile = Server.MapPath("~/Content/img/ico-taxnotfoundd.png");
                    FileStream fs = new FileStream(namafile, FileMode.Open, FileAccess.Read);
                    return File(fs, "image/jpeg");
                }
            }
        }

        [HttpPost]
        public ActionResult ExportPDF(string id)
        {
            dynamic result;

            try
            {
                RemoveSQLInjectString(id);

                var yy = Service<NoturService>().GetDataPDF(id);
                string url = string.Empty;
                string linkdata = string.Empty;

                foreach (var data in yy)
                {
                    linkdata = data.tax_attc_url;
                }

                if (string.IsNullOrEmpty(linkdata))
                {
                    result = new { Url = Server.MapPath("~/Content/img/ico-taxnotfoundd.png"), Result = false, Message = "Tax invoice document is waiting for approval." };
                }
                else
                {
                    var username = Service<ParameterService>().GetParameter("file_impersonate_user").value1;
                    var domain = Service<ParameterService>().GetParameter("file_impersonate_domain").value1;
                    var userdomain = domain + "\\" + username;
                    var password = Service<ParameterService>().GetParameter("file_impersonate_password").value1;

                    var credential = new NetworkCredential(userdomain, password);
                    string fileserver = Service<ParameterService>().GetParameter("file_server").value1;
                    string filedir = Service<ParameterService>().GetParameter("file_repo_path_tax").value1;
                    string filepath = "\\\\" + fileserver + "\\" + filedir;

                    using (new NetworkConnection(filepath, credential))
                    {
                        var temp = Path.GetPathRoot(linkdata);

                        if (temp == "\\\\VSV-C003-016018\\Interface")
                        {
                            linkdata = Path.Combine(filepath, Path.GetFileName(linkdata));

                            if (!System.IO.File.Exists(linkdata))
                            {
                                Byte[] bytes = System.IO.File.ReadAllBytes(Server.MapPath("~/Content/img/ico-taxnotfoundd.png"));
                                String file = Convert.ToBase64String(bytes);

                                result = new { Url = file, Result = false, Message = "Error! " + Path.GetFileName(linkdata) + " is not exists on server." };
                            }
                            else
                            {
                                Byte[] bytes = System.IO.File.ReadAllBytes(linkdata);
                                String file = Convert.ToBase64String(bytes);

                                result = new { Url = file, Result = true, Message = "" };
                            }
                        }
                        else if (temp == "\\\\10.85.11.83\\Interface")
                        {
                            linkdata = Path.Combine(filepath, Path.GetFileName(linkdata));

                            if (!System.IO.File.Exists(linkdata))
                            {
                                Byte[] bytes = System.IO.File.ReadAllBytes(Server.MapPath("~/Content/img/ico-taxnotfoundd.png"));
                                String file = Convert.ToBase64String(bytes);

                                result = new { Url = file, Result = false, Message = "Error! " + Path.GetFileName(linkdata) + " is not exists on server." };
                            }
                            else
                            {
                                Byte[] bytes = System.IO.File.ReadAllBytes(linkdata);
                                String file = Convert.ToBase64String(bytes);

                                result = new { Url = file, Result = true, Message = "" };

                            }
                        }
                        else
                        {
                            if (linkdata == "")
                            {
                                Byte[] bytes = System.IO.File.ReadAllBytes(Server.MapPath("~/Content/img/ico-taxnotfoundd.png"));
                                String file = Convert.ToBase64String(bytes);

                                result = new { Url = file, Result = false, Message = "Error! " + Path.GetFileName(linkdata) + " is not exists on server." };
                            }
                            else
                            {
                                Byte[] bytes = System.IO.File.ReadAllBytes(linkdata);
                                String file = Convert.ToBase64String(bytes);

                                result = new { Url = file, Result = true, Message = "" };

                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            return Json(new { result }, JsonRequestBehavior.AllowGet);
        }

        public FileResult DownloadPDF([DataSourceRequest] Kendo.Mvc.UI.DataSourceRequest request, string id)
        {
            var db = new DbHelper();

            var invoicedata = Service<NoturService>().GetDataPDF(id);
            string linkdata = string.Empty;
            string filenamepdf = string.Empty;
            var username = Service<ParameterService>().GetParameter("file_impersonate_user").value1;
            var domain = Service<ParameterService>().GetParameter("file_impersonate_domain").value1;
            var userdomain = domain + "\\" + username;
            var password = Service<ParameterService>().GetParameter("file_impersonate_password").value1;

            var credential = new NetworkCredential(userdomain, password);
            string fileserver = Service<ParameterService>().GetParameter("file_server").value1;
            string filepath = Service<ParameterService>().GetParameter("file_repo_path_tax").value1;
            string file = "\\\\" + fileserver + "\\" + filepath;

            using (new NetworkConnection(file, credential))
            {
                foreach (var data in invoicedata)
                {
                    linkdata = data.tax_attc_url;
                    filenamepdf = Path.GetFileNameWithoutExtension(linkdata) + ".pdf";
                }

                var temp = Path.GetPathRoot(linkdata);

                if (temp == "\\\\VSV-C003-016018\\Interface")
                {
                    var url = Path.Combine(file, filenamepdf);
                    byte[] fileBytes = System.IO.File.ReadAllBytes(url);
                    string fileName = filenamepdf;
                    return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, filenamepdf);
                }
                else if (temp == "\\\\10.85.11.83\\Interface")
                {
                    var url = Path.Combine(file, filenamepdf);
                    byte[] fileBytes = System.IO.File.ReadAllBytes(url);
                    string fileName = filenamepdf;
                    return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, filenamepdf);
                }
                else
                {
                    filenamepdf = Path.GetFileName(linkdata);

                    byte[] fileBytes = System.IO.File.ReadAllBytes(linkdata);
                    string fileName = filenamepdf;
                    return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
                }
            }
        }

        [HttpPost]
        public ActionResult Excel_Export_Save(string contentType, string base64, string fileName)
        {
            var fileContents = Convert.FromBase64String(base64);
            return File(fileContents, contentType, fileName);
        }
        [HttpPost]
        public ActionResult Submit(IEnumerable<HttpPostedFileBase> files)
        {
            if (files != null)
            {
                // TempData["UploadedFiles"] = GetFileInfo(files);
            }

            return RedirectToRoute("Demo", new { section = "upload", example = "result" });
        }

        public void Download(string daNumber, string businessUnit)
        {
            Stream stream = new MemoryStream();
            Warning[] warnings;
            string mimeType, encoding, extension, repPath = string.Empty;
            string[] streamIds;
            LocalReport report = new LocalReport();
            ReportViewer reportViewer = new ReportViewer();

            //QRCodeGenerator qrGenerator = new QRCodeGenerator();
            //QRCodeData qrCodeData = qrGenerator.CreateQrCode("The text which should be encoded.", QRCodeGenerator.ECCLevel.Q);
            //QRCoder.QRCode qrCode = new QRCoder.QRCode(qrCodeData);
            //Bitmap qrCodeImage = qrCode.GetGraphic(20);

            try
            {
                RemoveSQLInjectString(daNumber);

                string user = SessionManager.Current;
                string userid = "";
                if (user != "TAM\\Mayumi" || user != "TAM\\frans" || user != "TAM\\ronnyk102181" || user != "TAM\\indah.gustiana" || user != "TAM\\aliandy.syahriel")
                {
                    user = userid;
                }
                else
                {
                    user = SessionManager.Current;
                }
                string name = SessionManager.Name;
                string nameid = "";
                if (name != "Mayumi Eguchi" || name != "Frans lhutan B" || name != "Ronny Kusgianto" || name != "Indah Sari Gustiana" || name != "Aliandy Syahriel")
                {
                    name = nameid;
                }
                else
                {
                    name = SessionManager.Name;
                }
                Infrastructure.Barcode.Barcode barcode = new Infrastructure.Barcode.Barcode();

                if (!string.IsNullOrEmpty(daNumber))// && !string.IsNullOrEmpty(user))
                {
                    if (businessUnit.Equals("Others"))
                    {
                        Reporting.Report rpt = new Reporting.Report();

                        using (var db = new DbHelper())
                        {
                            var invoice_number = db.NotaReturRepository.GetDebitAdvice(new DataSourceRequest(), daNumber, user).First().Invoice_Number;

                            var bytes = rpt.GetDebitAdviceReport(daNumber, businessUnit, name, out extension, out mimeType);

                            Response.Buffer = true;
                            Response.Clear();
                            Response.ContentType = mimeType;
                            Response.AddHeader("content-disposition", "attachment; filename= Invoice - " + invoice_number.Replace("/", "-") + "." + extension);
                            Response.OutputStream.Write(bytes, 0, bytes.Length); // create the file  
                            Response.Flush(); // send it to the client to download  
                            var Update = Service<InvoiceService>().GetUpdate(daNumber, user);
                            Response.End();
                        }
                    }
                    else if (businessUnit.Equals("Motor Vehicle"))
                    {
                        repPath = string.Format("{0}/{1}", ConfigurationManager.AppSettings["RDLPath"], ConfigurationManager.AppSettings["MvInvoicePath"]);

                        var dataSource = Service<NoturService>().GetMvInvoice(new DataSourceRequest(), daNumber, user);

                        var inv = Service<InvoiceService>().GetInvoiceByDaNumber(daNumber);

                        var queryString = string.Format("invoice/getsingleinvoice?invoice_number={0}&customer_group={1}", inv.invoice_number, inv.customer_group);
                        var apiUrl = string.Format("{0}/{1}", ConfigurationManager.AppSettings["APIUrl"], queryString);


                        report.ReportPath = repPath;
                        report.SetParameters(new ReportParameter("pQrCode", barcode.GenerateQrCode(apiUrl)));
                        report.DataSources.Clear();
                        report.DataSources.Add(new ReportDataSource("MvInvoice", dataSource));

                        report.Refresh();

                        byte[] bytes = report.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

                        Response.Buffer = true;
                        Response.Clear();
                        Response.ContentType = mimeType;
                        Response.AddHeader("content-disposition", "attachment; filename= " + daNumber.Replace("/", "-") + "." + extension);
                        Response.OutputStream.Write(bytes, 0, bytes.Length); // create the file  
                        Response.Flush(); // send it to the client to download  
                        Response.End();
                        var Update = Service<InvoiceService>().GetUpdate(daNumber, user);
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ActionResult NotaReturDetail([DataSourceRequest] Kendo.Mvc.UI.DataSourceRequest request, string invoice_number,
            int business_unit_id, string transaction_type, string pph_no, decimal total_amount, decimal dpp, decimal tax, decimal rate, string amount_pph, string currency, int transaction_type_id)
        {
            Invoice empObj = new Invoice();
            empObj.invoice_number = invoice_number;
            empObj.business_unit_id = business_unit_id;
            empObj.business_unit = business_unit_id.ToString();
            empObj.transaction_type = transaction_type;
            //string dpps = dpp.ToString("#,#.00#");
            empObj.dpp = dpp;
            empObj.tax = tax;
            empObj.total_amount = total_amount;
            //empObj.pph_no = pph_no.Replace("|", "#");
            empObj.pph_no = pph_no;
            empObj.rate = rate;
            empObj.amount_pph = amount_pph == null ? 0 : Convert.ToDecimal(amount_pph);
            empObj.currency = currency;
            empObj.transaction_type_id = transaction_type_id;

            return Json(new { empObj }, JsonRequestBehavior.AllowGet);
        }


        [FileDownload]
        public void DownloadAllNotur(string sid, Int32 total)
        {
            HttpApplicationStateBase app = HttpContext.Application;
            byte[] fileBytes = null, zipBytes;
            string mimeType, extension;


            //var outputfile = "NOTARETUR - " + DateTime.Now.ToString("yyyyMMdd") + ".zip";
            var outputfile = DateTime.Now.ToString("yyyyMMdd") + ".zip";
            var details = app[sid] as List<Invoice>;
            var user = SessionManager.Current;
            try
            {

                using (var memoryStream = new MemoryStream())
                {
                    using (var ziparchive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
                    {
                        List<Invoice> data = details.ToList();

                        // Initial variable
                        //var IsPrint = true;
                        //int totalData = data.Count;
                        //int mvData = data.Where(x => x.business_unit_id == 1).Count();
                        //// 
                        //IsPrint = totalData == mvData ? false : true;

                        //if (IsPrint)
                        //{
                            Reporting.Report report = new Reporting.Report();
                            foreach (Invoice inv in details.ToList())
                            {

                                if (inv.business_unit_id == 2)
                                {
                                    //var filename = "NOTARETUR - " + DateTime.Now.ToString("yyyyMMdd") + "\\" + "NOTARETUR -" + inv.invoice_number.Replace('/', '-') + "\\" + "NOTARETUR -" + inv.invoice_number.Replace('/', '-') + ".pdf";
                                    var filename = DateTime.Now.ToString("yyyyMMdd") + "\\" + inv.invoice_number.Replace('/', '-') + "\\" + inv.invoice_number.Replace('/', '-') + ".pdf";
                                    var fileInArchive = ziparchive.CreateEntry(filename);
                                    fileBytes = report.GetNotaRetur(
                                        inv.da_no,
                                        SessionManager.Name,
                                        out extension,
                                        out mimeType);

                                    using (var zipStream = fileInArchive.Open())
                                    using (var fileToCompressStream = new MemoryStream(fileBytes))
                                    {
                                        fileToCompressStream.CopyTo(zipStream);
                                    }

                                }
                                //else if (inv.business_unit_id == 1) // mv
                                //{
                                //    ////Add by AD
                                //    //string url = string.Empty;
                                //    //var downloadfileori = ApplicationCacheManager.GetConfig<string>("FileUpload");
                                //    ////end

                                //    List<SpNotaRetur> dataSource = new List<SpNotaRetur>();
                                //    //List<Invoice> datasource = new List<Invoice>();
                                //    dataSource = Service<NoturService>().GetNotaReturMv(new DataSourceRequest(), inv.NomorNotaRetur, user);
                                //    //dataSource = Service<NoturService>().GetNotaReturMv(new DataSourceRequest(), inv.invoice_number, inv.NomorNotaRetur, user);
                                //    //dataSource = Service<NoturService>().GetNoturUploadOri(new DataSourceRequest(), inv.invoice_number, user);

                                //    List<SpNotaRetur> distinctNomorFaktur = dataSource
                                //                                                .GroupBy(s => s.NomorRetur)
                                //                                                .Select(g => g.First())
                                //                                                .ToList();

                                //    foreach (SpNotaRetur notaRetur in distinctNomorFaktur)
                                //    {
                                //        //var filename = Path.GetFileNameWithoutExtension(outputfile) + "\\" + "NotaRetur - " + inv.invoice_number.Replace("/", "-") + "\\" + "NotaRetur - " + notaRetur.NomorRetur + ".pdf";
                                //        var filename = Path.GetFileNameWithoutExtension(outputfile) + "\\" + inv.invoice_number.Replace("/", "-") + "\\" + notaRetur.NomorRetur + ".pdf";
                                //        var fileInArchive = ziparchive.CreateEntry(filename);

                                //        fileBytes = report.GetNotaReturSpOrMv(
                                //            dataSource,
                                //            notaRetur,
                                //            out extension,
                                //            out mimeType);

                                //        using (var zipStream = fileInArchive.Open())
                                //        using (var fileToCompressStream = new MemoryStream(fileBytes))
                                //        {
                                //            fileToCompressStream.CopyTo(zipStream);
                                //        }
                                //    }
                                //}

                                //ADD by AD
                                else if (inv.business_unit_id == 1) // mv
                                {
                                    string url = string.Empty;
                                    var downloadfileori = ApplicationCacheManager.GetConfig<string>("FileUpload");
                                    List<SpNotaRetur> dataSource = new List<SpNotaRetur>();
                                    //List<Invoice> datasource = new List<Invoice>();
                                    //dataSource = Service<NoturService>().GetNotaReturMv(new DataSourceRequest(), inv.invoice_number, inv.NomorNotaRetur, user);
                                    dataSource = Service<NoturService>().GetNotaReturMv(new DataSourceRequest(), inv.NomorNotaRetur, user);
                                    //var a = inv.NomorNotaRetur;
                                    //dataSource = Service<NoturService>().GetNoturUploadOri(new DataSourceRequest(), inv.invoice_number, user);

                                    List<SpNotaRetur> distinctNomorFaktur = dataSource
                                                                                .GroupBy(s => s.da_no)
                                                                                .Select(g => g.First())
                                                                                .ToList();

                                    foreach (SpNotaRetur notaRetur in distinctNomorFaktur)
                                    {
                                        //string url = string.Empty;
                                        //var filename = Path.GetFileNameWithoutExtension(outputfile) + "\\" + "NotaRetur - " + inv.invoice_number.Replace("/", "-") + "\\" + "NotaRetur - " + notaRetur.NomorRetur + ".pdf";
                                        //var filename = Path.GetFileNameWithoutExtension(outputfile) + "\\" + inv.invoice_number.Replace("/", "-") + "\\" + notaRetur.NomorRetur + ".pdf";
                                        var filename = notaRetur.da_no + ".pdf";
                                        var fileInArchive = ziparchive.CreateEntry(filename);
                                        url = Path.Combine(downloadfileori, filename);
                                        //url = Path.Combine(fileInArchive);
                                        Byte[] bytes = System.IO.File.ReadAllBytes(url);
                                        fileBytes = bytes;

                                        using (var zipStream = fileInArchive.Open())
                                        using (var fileToCompressStream = new MemoryStream(fileBytes))
                                        {
                                            fileToCompressStream.CopyTo(zipStream);
                                        }
                                        //using (var zipStream = fileInArchive.Open())
                                        //using (var fileToCompressStream = new MemoryStream(fileBytes))
                                        //{
                                        //    fileToCompressStream.CopyTo(zipStream);
                                        //}
                                    }
                                }
                                //END

                                else if (inv.business_unit_id == 3) // spare part invoice
                                {
                                    List<SpNotaRetur> dataSource = new List<SpNotaRetur>();
                                    dataSource = Service<NoturService>().GetNotaReturSp(new DataSourceRequest(), inv.invoice_number, user);

                                    List<SpNotaRetur> distinctNomorFaktur = dataSource
                                                                                .GroupBy(s => s.NomorRetur)
                                                                                .Select(g => g.First())
                                                                                .ToList();

                                    foreach (SpNotaRetur notaRetur in distinctNomorFaktur)
                                    {
                                        //var filename = Path.GetFileNameWithoutExtension(outputfile) + "\\"+"NotaRetur - " + inv.invoice_number.Replace("/", "-") + "\\" + "NotaRetur - " + notaRetur.NomorRetur + ".pdf";
                                        var filename = Path.GetFileNameWithoutExtension(outputfile) + "\\" + inv.invoice_number.Replace("/", "-") + "\\" + notaRetur.NomorRetur + ".pdf";
                                        var fileInArchive = ziparchive.CreateEntry(filename);

                                        fileBytes = report.GetNotaReturSpOrMv(
                                            dataSource,
                                            notaRetur,
                                            out extension,
                                            out mimeType);

                                        using (var zipStream = fileInArchive.Open())
                                        using (var fileToCompressStream = new MemoryStream(fileBytes))
                                        {
                                            fileToCompressStream.CopyTo(zipStream);
                                        }
                                    }
                                }
                                else
                                {
                                    fileBytes = null;
                                }



                                if (inv.business_unit_id != 3) // not spare part invoice
                                {
                                    var Update = Service<NotaReturService>().GetUpdate(inv.da_no);
                                }
                                else
                                {
                                    var Update = Service<InvoiceService>().UpdateSparePartStatusDownload(
                                       inv.invoice_number,
                                       user,
                                       EnumHelper.GetDescription(DownloadType.NotaRetur),
                                       EnumHelper.GetDescription(InvoiceType.NotaRetur)
                                   );
                                }
                            }

                            ziparchive.Dispose();


                            Response.Clear();
                            Response.AddHeader("Access-Control-Allow-Origin", "*");
                            Response.ContentType = "application/octet-stream";
                            Response.AddHeader("content-disposition", "attachment;filename=\"" + outputfile + "\"");
                            Response.BinaryWrite(memoryStream.ToArray());
                            Response.Flush();
                            Response.End();
                        //}
                        //else
                        //{
                        //    //
                        //    throw new Exception("MV is Not Available");

                        //}
                    }
                }
            }

            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        [FileDownload]
        public void DownloadAllAttachment(string sid, Int32 total)
        {
            HttpApplicationStateBase app = HttpContext.Application;

            var username = Service<ParameterService>().GetParameter("file_impersonate_user").value1;
            var domain = Service<ParameterService>().GetParameter("file_impersonate_domain").value1;
            var userdomain = domain + "\\" + username;
            var password = Service<ParameterService>().GetParameter("file_impersonate_password").value1;

            var credential = new NetworkCredential(userdomain, password);
            string fileserver = Service<ParameterService>().GetParameter("file_server").value1;
            string filepath = Service<ParameterService>().GetParameter("file_repo_path").value1;
            string file = "\\\\" + fileserver + "\\" + filepath;

            string mimeType, encoding, extension, repPath = string.Empty;
            byte[] fileBytes;
            Warning[] warnings;
            string[] streamIds;
            LocalReport report = new LocalReport();

            try
            {

                var details = app[sid] as List<Invoice>;

                var outputfile = "AttachmentNotaRetur - " + DateTime.Now.ToString("yyyyMMdd") + ".zip";
                var zipStream = new MemoryStream();
                var zip = new ZipArchive(zipStream, ZipArchiveMode.Create);

                // Convert IEnumerable ke List
                List<Invoice> data = details.ToList();

                // Initial variable
                var IsPrint = true;
                int totalData = data.Count;
                int mvData = data.Where(x => x.business_unit_id == 1).Count();
                // 
                //IsPrint = totalData == mvData ? true : false;
                IsPrint = totalData == mvData ? false : true; // --> function OLD

                if (IsPrint)
                {
                    foreach (Invoice inv in details.ToList())
                    {
                        var id = inv.da_code;
                        if (inv.business_unit_id == 2 || inv.business_unit_id == 1)
                        {
                            using (new NetworkConnection(file, credential))
                            {
                                var unit = inv.business_unit;
                                var doc = Service<AttachmentService>().GetAttachment(id);
                                if (doc.Count > 0)
                                {
                                    var item = doc.FirstOrDefault();
                                    var filename = item.created_date.ToString("yyyyM") + "\\" + item.reference + "\\" + item.filename;
                                    var url = Path.Combine(file, filename);

                                    if (System.IO.File.Exists(url) == true)
                                    {
                                        zip.CreateEntryFromFile(url, "AttachmentNotaRetur - " + DateTime.Now.ToString("yyyyMMdd") + "\\" + "AttachmentNotaRetur - " + inv.invoice_number.Replace('/', '-') + "\\" + item.filename);
                                    }
                                    else
                                    {
                                        zip.CreateEntry("AttachmentNotaRetur - " + DateTime.Now.ToString("yyyyMMdd") + "\\" + "AttachmentNotaRetur - " + inv.invoice_number.Replace('/', '-') + "\\" + Path.GetFileNameWithoutExtension(item.filename) + ".NOTFOUND");
                                    }
                                }
                                else
                                {
                                    throw new Exception("Invoice doesn't have attachment");
                                }
                            }
                        }
                        else
                        {
                            //attchmentSparePart(username, domain, zip, inv);
                            string filename = Path.GetFileNameWithoutExtension(outputfile) + "\\" + "Attachment - " + inv.invoice_number.Replace("/", "-") + ".pdf";
                            List<AttachmentSpDebitAdvice> dataSource = new List<AttachmentSpDebitAdvice>();

                            dataSource = Service<AttachmentService>().GetSpCreditAttachment(inv.invoice_number, "");
                            repPath = string.Format("{0}/{1}", ConfigurationManager.AppSettings["RDLPath"], ConfigurationManager.AppSettings["SpAttCreditAdvicePath"]);


                            var fileInArchive = zip.CreateEntry(filename);

                            fileBytes = SaveAttachmentReportPdf(inv.invoice_number, out warnings, out mimeType, out encoding, out extension, repPath, out streamIds, report, dataSource);

                            using (var zipStreamOther = fileInArchive.Open())
                            using (var fileToCompressStream = new MemoryStream(fileBytes))
                            {
                                fileToCompressStream.CopyTo(zipStreamOther);
                            }
                        }
                    }

                    zip.Dispose();

                    Response.Clear();
                    Response.AddHeader("Access-Control-Allow-Origin", "*");
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("content-disposition", "attachment;filename=\"" + outputfile + "\"");
                    Response.BinaryWrite(zipStream.ToArray());
                    Response.Flush();
                    Response.End();
                }
                else
                {
                    //
                    throw new Exception("Invoice doesn't have attachment");

                }

            }


            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

        }

        [FileDownload]
        public void ExportZipInvoice(string sid, Int32 total)
        {
            HttpApplicationStateBase app = HttpContext.Application;
            byte[] fileBytes, zipBytes;
            string mimeType, extension;


            string filename = string.Empty;

            //var username = Service<ParameterService>().GetParameter("file_impersonate_user").value1;
            //var domain = Service<ParameterService>().GetParameter("file_impersonate_domain").value1;
            //var userdomain = domain + "\\" + username;
            //var password = Service<ParameterService>().GetParameter("file_impersonate_password").value1;

            //var credential = new NetworkCredential(userdomain, password);
            //string fileserver = Service<ParameterService>().GetParameter("file_server").value1;
            //string filepath = Service<ParameterService>().GetParameter("file_repo_path").value1;
            //string file = "\\\\" + fileserver + "\\" + filepath;
            string user = SessionManager.Current;
            string userid = "";
            if (user != "TAM\\Mayumi" || user != "TAM\\frans" || user != "TAM\\ronnyk102181" || user != "TAM\\indah.gustiana" || user != "TAM\\aliandy.syahriel")
            {
                user = userid;
            }
            else
            {
                user = SessionManager.Current;
            }
            string name = SessionManager.Name;
            string nameid = "";
            if (name != "Mayumi Eguchi" || name != "Frans lhutan B" || name != "Ronny Kusgianto" || name != "Indah Sari Gustiana" || name != "Aliandy Syahriel")
            {
                name = nameid;
            }
            else
            {
                name = SessionManager.Name;
            }
            try
            {
                //    using (new NetworkConnection(file, credential))
                //    {
                var details = app[sid] as List<Invoice>;
                var outputfile = "INVOICE_NotaRetur - " + DateTime.Now.ToString("yyyyMMdd") + ".zip";

                using (var memoryStream = new MemoryStream())
                {
                    using (var ziparchive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
                    {
                        foreach (Invoice data in details.ToList())
                        {
                            Reporting.Report report = new Reporting.Report();

                            //filename = Path.GetFileNameWithoutExtension(outputfile) + "\\" + data.doc_date.ToString("yyyyMMdd") + "\\" + data.invoice_number.Replace('/', '-') + ".pdf";
                            //filename = Path.GetFileNameWithoutExtension(outputfile) + "\\" + data.da_no.Replace('/', '-') + "\\" + data.da_no.Replace('/', '-') + ".pdf";
                            filename = Path.GetFileNameWithoutExtension(outputfile) + "\\" + "INVOICE - " + data.invoice_number.Replace('/', '-') + "\\" + "Invoice - " + data.invoice_number.Replace('/', '-') + ".pdf";
                            var fileInArchive = ziparchive.CreateEntry(filename);

                            if (data.business_unit.Equals("Others"))
                            {
                                fileBytes = report.GetDebitAdviceReport(
                                    data.da_no,
                                    data.business_unit_id.ToString(),
                                    name,
                                    out extension,
                                    out mimeType);
                            }
                            else if (data.business_unit.Equals("Motor Vehicle"))
                            {
                                fileBytes = report.GetMvInvoiceReport(
                                    data.da_no,
                                    data.business_unit_id.ToString(),
                                    name,
                                    out extension,
                                    out mimeType);
                            }
                            else if (data.business_unit.Equals("Spare Part"))
                            {
                                fileBytes = report.GetSpCreditAdviceReport(
                                    data.invoice_number,
                                    name,
                                    out extension,
                                    out mimeType);
                            }
                            else
                            {
                                fileBytes = null;
                            }

                            using (var zipStream = fileInArchive.Open())
                            using (var fileToCompressStream = new MemoryStream(fileBytes))
                            {
                                fileToCompressStream.CopyTo(zipStream);
                            }

                            if (data.business_unit_id != 3) // not spare part invoice
                            {
                                var Update = Service<InvoiceService>().GetUpdate(data.da_no, user);
                            }
                            else
                            {
                                var Update = Service<InvoiceService>().UpdateSparePartStatusDownload(
                                    data.da_no,
                                    user,
                                    EnumHelper.GetDescription(DownloadType.InvoiceQR),
                                    EnumHelper.GetDescription(InvoiceType.NotaRetur)
                                    );
                            }

                        }
                        ziparchive.Dispose();
                    }
                    Response.Clear();
                    Response.AddHeader("Access-Control-Allow-Origin", "*");
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("content-disposition", "attachment;filename=" + outputfile);
                    Response.BinaryWrite(memoryStream.ToArray());
                    Response.Flush();
                    Response.End();
                }
                //    }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        [FileDownload]
        public void DownloadAllInvoiceNotur(string sid, Int32 total)
        {
            HttpApplicationStateBase app = HttpContext.Application;

            var username = Service<ParameterService>().GetParameter("file_impersonate_user").value1;
            var domain = Service<ParameterService>().GetParameter("file_impersonate_domain").value1;
            var userdomain = domain + "\\" + username;
            var password = Service<ParameterService>().GetParameter("file_impersonate_password").value1;

            var credential = new NetworkCredential(userdomain, password);
            string fileserver = Service<ParameterService>().GetParameter("file_server").value1;
            string filepath = Service<ParameterService>().GetParameter("file_invoice_notur_path").value1;
            string file = "\\\\" + fileserver + "\\" + filepath;
            var user = SessionManager.Current;
            try
            {
                using (new NetworkConnection(file, credential))
                {
                    var details = app[sid] as List<Invoice>;

                    var outputfile = "Invoice - " + DateTime.Now.ToString("yyyyMMdd") + ".zip";
                    var zipStream = new MemoryStream();
                    var zip = new ZipArchive(zipStream, ZipArchiveMode.Create);

                    // Convert IEnumerable ke List
                    // List<Invoice> data = details.ToList();

                    //var invoice = new Invoice();
                    //var IsPrint = true;
                    //int totalData = data.Count;
                    //var document = Service<ScanInvoiceService>().GetAttachment(invoice.invoice_number);
                    //int mvData = document.Where(x => x.invoice_number == null).Count();
                    //// 
                    //IsPrint = totalData == mvData ? false : true;

                    //if (IsPrint)
                    //{
                    foreach (Invoice inv in details.ToList())
                    {
                        var id = inv.invoice_number;
                        if (id != "")
                        {
                            var unit = inv.business_unit;
                            var doc = Service<ScanInvoiceNoturService>().GetAttachment(id);
                            if (doc.Count > 0)
                            {
                                var item = doc.FirstOrDefault();
                                var filename = item.filename;
                                var url = Path.Combine(file, filename);

                                if (System.IO.File.Exists(url) == true)
                                {
                                    zip.CreateEntryFromFile(url, "Invoice - " + DateTime.Now.ToString("yyyyMMdd") + "\\" + "Invoice - " + inv.invoice_number.Replace('/', '-') + "\\" + item.filename);
                                    var Update = Service<InvoiceService>().GetUpdateInvoice(inv.invoice_number, user);
                                }
                                else
                                {
                                    zip.CreateEntry("Invoice - " + DateTime.Now.ToString("yyyyMMdd") + "\\" + "Invoice - " + inv.invoice_number.Replace('/', '-') + "\\" + Path.GetFileNameWithoutExtension(item.filename) + ".NOTFOUND");
                                }
                            }
                            else
                            {
                                throw new Exception("Invoice doesn't have attachment");
                            }

                        }
                        else
                        {

                        }

                    }
                    zip.Dispose();
                    Response.Clear();
                    Response.AddHeader("Access-Control-Allow-Origin", "*");
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("content-disposition", "attachment;filename=" + outputfile);
                    Response.BinaryWrite(zipStream.ToArray());
                    Response.Flush();
                    Response.End();

                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

        }
        [FileDownload]
        public void ExportZip(string sid, Int32 total)
        {

            HttpApplicationStateBase app = HttpContext.Application;

            string linkdata = string.Empty;
            string namapath = string.Empty;
            string filenamepdf = string.Empty;
            var username = Service<ParameterService>().GetParameter("file_impersonate_user").value1;
            var domain = Service<ParameterService>().GetParameter("file_impersonate_domain").value1;
            var userdomain = domain + "\\" + username;
            var password = Service<ParameterService>().GetParameter("file_impersonate_password").value1;

            var credential = new NetworkCredential(userdomain, password);
            string fileserver = Service<ParameterService>().GetParameter("file_server").value1;
            string filepath = Service<ParameterService>().GetParameter("file_repo_path_tax").value1;
            string file = "\\\\" + fileserver + "\\" + filepath;

            try
            {
                using (new NetworkConnection(file, credential))
                {
                    var details = app[sid] as List<Invoice>;
                    //var yy = Service<NoturService>().GetDataPDF(sid);
                    var outputfile = "TAXINVOICE_NotaRetur - " + DateTime.Now.ToString("yyyyMMdd") + ".zip";

                    using (var memoryStream = new MemoryStream())
                    {
                        using (var ziparchive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
                        {

                            foreach (Invoice inv in details.ToList()) 
                            //foreach (Invoice inv in yy)
                            {

                                if (inv.tax_attc_url != null)
                                {
                                    var value = inv.tax_attc_url;
                                    string Result = Path.GetFileNameWithoutExtension(value) + ".pdf";
                                    var url = Path.Combine(file, Result);
                                    if (System.IO.File.Exists(url) == true)
                                    {
                                        namapath = Path.GetFileNameWithoutExtension(outputfile) + "\\" + Path.GetFileNameWithoutExtension(inv.tax_attc_url) + "\\" + Path.GetFileNameWithoutExtension(inv.tax_attc_url) + ".pdf";
                                        var fileInArchive = ziparchive.CreateEntry(namapath);

                                        using (FileStream _file = new FileStream(url, FileMode.Open, FileAccess.Read))
                                        {
                                            byte[] bytes = new byte[_file.Length];

                                            _file.Read(bytes, 0, (int)_file.Length);

                                            using (var zipStream = fileInArchive.Open())
                                            using (var fileToCompressStream = new MemoryStream(bytes))
                                            {
                                                fileToCompressStream.CopyTo(zipStream);
                                            }
                                        }
                                    }
                                    else if (System.IO.File.Exists(url) == false)
                                    {
                                        ziparchive.CreateEntry(Path.GetFileNameWithoutExtension(outputfile) + "\\" + Path.GetFileNameWithoutExtension(inv.tax_attc_url) + "\\" + Path.GetFileNameWithoutExtension(inv.tax_attc_url) + ".NOTFOUND");
                                    }
                                }
                                else
                                {
                                    //ziparchive.CreateEntry(Path.GetFileNameWithoutExtension(outputfile) + "\\" + Path.GetFileNameWithoutExtension(inv.tax_attc_url) + "\\" + Path.GetFileNameWithoutExtension(inv.tax_attc_url) + ".NOTFOUND");
                                    //throw new Exception("File Not Found !");
                                }
                            }
                            ziparchive.Dispose();

                        }
                        Response.Clear();
                        Response.AddHeader("Access-Control-Allow-Origin", "*");
                        Response.ContentType = "application/octet-stream";
                        Response.AddHeader("content-disposition", "attachment;filename=" + outputfile);
                        Response.BinaryWrite(memoryStream.ToArray());
                        Response.Flush();
                        Response.End();
                    }
                }
            }

            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        [FileDownload]
        public FileResult Excel_Export(string sid, Int32 total)
        {
            HttpApplicationStateBase app = HttpContext.Application;
            //isti 20201119
            //var strclmn = new string[] { "CA No", "   Transaction Type", "Doc date", "Due Date", "Currency", "DPP", "Tax Amount", "Dormitory", "VAT Dormitory", "Total Amount", "Tax Invoice", "PPH 22 Amount", "PPH No", "AR Status", "Dealer Code", "Dealer Name"};
            var strclmn = new string[] { "Business Unit", "Transaction Type", "Credit Advice Number", "Credit Note Number", "Credit Note Date"
                            , "Tax Invoice Number", "Tax Invoice Date", "Dealer Code", "Customer Name", "Due Date"
                            , "Currency", "Return VAT Based Amount", "Return VAT Amount", "AR Status", "VAT Credit Note Original"
                            , "Invoice QR Status", "Invoice Status", "VAT Credit Note Status", "VAT Credit Note Sign Off", "VAT Credit Note DGT"};

            var details = app[sid] as List<Invoice>;

            if (total < 3000)
            {
                using (var exportData = new MemoryStream())
                {
                    var workbook = new XSSFWorkbook();
                    var sheet = workbook.CreateSheet("VATCreditNoteExport");
                    var headerRow = sheet.CreateRow(0);

                    int i = 0;

                    foreach (string c in strclmn)
                    {

                        // Create New Cell
                        var headerCell = headerRow.CreateCell(i);

                        // Set Cell Value
                        headerCell.SetCellValue(c);

                        // Create Style
                        var headerCellStyle = workbook.CreateCellStyle();
                        headerCellStyle.FillForegroundColor = IndexedColors.Aqua.Index;
                        headerCellStyle.FillPattern = FillPattern.SolidForeground;

                        // Add Style to Cell
                        headerCell.CellStyle = headerCellStyle;

                        sheet.AutoSizeColumn(i);
                        i++;
                    }
                    //var data = Service<InvoiceService>().GetViewDataSourceResult(new DataSourceRequest(), 1, SessionManager.RoleStr);

                    i = 0;
                    foreach (Invoice inv in details.ToList())
                    {

                        var content = sheet.CreateRow(i + 1);
                        content.CreateCell(0).SetCellValue(inv.business_unit);
                        content.CreateCell(1).SetCellValue(inv.transaction_type);
                        content.CreateCell(2).SetCellValue(inv.invoice_number);
                        content.CreateCell(3).SetCellValue(inv.credit_note_number);
                        content.CreateCell(4).SetCellValue(inv.doc_date.ToString("dd/MM/yyyy"));
                        content.CreateCell(5).SetCellValue(inv.tax_invoice);
                        content.CreateCell(6).SetCellValue(inv.tax_date.ToString("dd/MM/yyyy"));
                        content.CreateCell(7).SetCellValue(inv.da_code);
                        content.CreateCell(8).SetCellValue(inv.customer_name);
                        content.CreateCell(9).SetCellValue(inv.due_date.ToString("dd/MM/yyyy"));
                        content.CreateCell(10).SetCellValue(inv.currency);

                        if (inv.business_unit != "Spare Part")
                        {
                            content.CreateCell(11).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0}", inv.dpp ));
                            content.CreateCell(12).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0}", inv.tax ));
                        }
                        else
                        {
                            content.CreateCell(11).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0}", inv.dpp));
                            content.CreateCell(12).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0}", inv.tax));
                        }
                        content.CreateCell(13).SetCellValue(inv.ar_status_name);
                        //content.CreateCell(14).SetCellValue(inv.cn_approval_status); --Function OLD(Original)
                        //Change
                        if (inv.notur_uploaded == true)
                            content.CreateCell(14).SetCellValue("Exist");
                        else
                            content.CreateCell(14).SetCellValue("Not Exist");
                        //END
                        if (inv.inv_downloaded == true)
                            content.CreateCell(15).SetCellValue("Last Downloaded " + inv.downloaded_date.ToString("dd/MM/yyyy"));
                        else
                            content.CreateCell(15).SetCellValue("Not Downloaded");

                        if (inv.inv_scan_downloaded == true)
                            content.CreateCell(16).SetCellValue("Last Downloaded " + inv.downloadedscan_date.ToString("dd/MM/yyyy"));
                        else
                            content.CreateCell(16).SetCellValue("Not Downloaded");

                        if (inv.credit_note_status != "")
                        {
                            string[] cn_status = inv.credit_note_status.ToString().Split('|');
                            if (cn_status.Count() > 1)
                                content.CreateCell(17).SetCellValue(cn_status[1]);
                            else
                                content.CreateCell(17).SetCellValue(inv.credit_note_status);
                        }
                        else
                            content.CreateCell(17).SetCellValue(inv.credit_note_status);


                        if (inv.creditnotettd == true)
                            content.CreateCell(18).SetCellValue("Exist");
                        else
                            content.CreateCell(18).SetCellValue("Not Exist");

                        if (inv.creditnotedgt == true)
                            content.CreateCell(19).SetCellValue("Exist");
                        else
                            content.CreateCell(19).SetCellValue("Not Exist");
                        i++;
                    }


                    workbook.Write(exportData);
                    string saveAsFileName = string.Format("VATCreditNoteExport_{0:d}.xlsx", DateTime.Now).Replace("/", "");

                    byte[] bytes = exportData.ToArray();
                    return File(bytes, "application/vnd.ms-excel", saveAsFileName);

                }
            }

            else
            {
                throw new Exception("Export Excel Failed, Load too much data");
            }



        }

        public void DownloadSpCa(string ca_number, string business_unit)
        {
            try
            {
                string user = "";
                Invoice inv = new Invoice();
                if (!string.IsNullOrEmpty(ca_number)) //&& !string.IsNullOrEmpty(user))
                {
                    if (business_unit.Equals("Spare Part"))
                    {
                        Stream stream = new MemoryStream();
                        string mimeType, extension;

                        Reporting.Report rport = new Reporting.Report();

                        byte[] bytes = rport.GetSpCreditAdviceReport(ca_number, SessionManager.Current, out extension, out mimeType);

                        Response.Buffer = true;
                        Response.Clear();
                        Response.ContentType = mimeType;
                        //Response.AddHeader("content-disposition", "attachment; filename= " + "NotaRetur" + "-" + ca_number.Replace("/", "-") + "." + extension);
                        Response.AddHeader("content-disposition", "attachment; filename= " + ca_number.Replace("/", "-") + "." + extension);
                        Response.OutputStream.Write(bytes, 0, bytes.Length); // create the file  
                        Response.Flush(); // send it to the client to download  
                        Response.End();
                        var Update = Service<InvoiceService>().UpdateSparePartStatusDownload(
                            ca_number,
                            user,
                            EnumHelper.GetDescription(DownloadType.InvoiceQR),
                            EnumHelper.GetDescription(InvoiceType.NotaRetur)
                        );
                    }
                    else
                    {
                        throw new Exception("CA is Not Available");
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void DownloadSpNotaRetur(string ca_number,string NomorNotaRetur, string business_unit)
        {
            //HttpApplicationStateBase app = HttpContext.Application;
            try
            {
                
                string url = string.Empty;
                var downloadfileori = ApplicationCacheManager.GetConfig<string>("FileUpload");

                string user = "";
                string filename = string.Empty;
                byte[] fileBytes;
                Warning[] warnings;
                string mimeType, encoding, extension, repPath = string.Empty;
                string[] streamIds;
                LocalReport report = new LocalReport();
                //var outputfile = "NotaRetur - " + ca_number.Replace("/", "-") + ".zip";
                var outputfile = NomorNotaRetur.Replace("/", "-") + ".pdf";

                
                //var details = app[sid] as List<Invoice>;
                //List<Invoice> data = details.ToList();
               // foreach (Invoice inv in details.ToList())
                //{
                    if (!string.IsNullOrEmpty(NomorNotaRetur)) //&& !string.IsNullOrEmpty(user))
                    {
                        repPath = string.Format("{0}/{1}", ConfigurationManager.AppSettings["RDLPath"], ConfigurationManager.AppSettings["NotaReturSpPath"]);

                        List<SpNotaRetur> dataSource = new List<SpNotaRetur>();
                        if (business_unit == "Spare Part")
                        {
                            dataSource = Service<NoturService>().GetNotaReturSp(new DataSourceRequest(), NomorNotaRetur, user);

                            //new
                            List<SpNotaRetur> distinctNomorFaktur = dataSource
                                                                   .GroupBy(s => s.NomorRetur)
                                                                   .Select(g => g.First())
                                                                   .ToList();


                        url = Path.Combine(downloadfileori, outputfile);
                        Byte[] bytes = System.IO.File.ReadAllBytes(url);
                        fileBytes = bytes;
                        //byte[] bytes = report.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
                        //using (var memoryStream = new MemoryStream())
                        //{
                        //    using (var ziparchive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
                        //    {
                        //        foreach (SpNotaRetur daNo in distinctNomorFaktur)
                        //        {
                        //            filename = daNo.NomorRetur + ".pdf";
                        //            var fileInArchive = ziparchive.CreateEntry(filename);
                        //            url = Path.Combine(downloadfileori, filename);
                        //            //url = Path.Combine(fileInArchive);
                        //            Byte[] bytes = System.IO.File.ReadAllBytes(url);
                        //            fileBytes = bytes;

                        //            using (var zipStream = fileInArchive.Open())
                        //            using (var fileToCompressStream = new MemoryStream(fileBytes))
                        //            {
                        //                fileToCompressStream.CopyTo(zipStream);
                        //            }

                        //            ////filename = Path.GetFileNameWithoutExtension(outputfile) + "\\" + "NotaRetur - " + notaRetur.NomorRetur.Replace("/", "-") + ".pdf";
                        //            //filename = Path.GetFileNameWithoutExtension(outputfile) + "\\" + notaRetur.NomorRetur.Replace("/", "-") + ".pdf";
                        //            //var fileInArchive = ziparchive.CreateEntry(filename);
                        //            //url = Path.Combine(downloadfileori, filename);

                        //            //fileBytes = SaveReportPdf(out warnings, out mimeType, out encoding, out extension, repPath, out streamIds, report, dataSource, notaRetur);

                        //            //using (var zipStream = fileInArchive.Open())
                        //            //using (var fileToCompressStream = new MemoryStream(fileBytes))
                        //            //{
                        //            //    fileToCompressStream.CopyTo(zipStream);
                        //            //}
                        //        }
                        //    }


                        var Update = Service<InvoiceService>().UpdateSparePartStatusDownload(
                                    ca_number,
                                    user,
                                    EnumHelper.GetDescription(DownloadType.NotaRetur),
                                    EnumHelper.GetDescription(InvoiceType.NotaRetur)
                                );

                                Response.Clear();
                                Response.AddHeader("Access-Control-Allow-Origin", "*");
                                Response.ContentType = "application/octet-stream";
                                Response.AddHeader("content-disposition", "attachment;filename=\"" + outputfile + "\"");
                                Response.OutputStream.Write(bytes, 0, bytes.Length); // create the file  
                                //Response.BinaryWrite(memoryStream.ToArray());
                                Response.Flush();
                                Response.End();
                           // }
                            //end
                        }
                    else
                        {
                            dataSource = Service<NoturService>().GetNotaReturMv(new DataSourceRequest(),NomorNotaRetur, user);
                        //dataSource = Service<NoturService>().GetNotaReturMv(new DataSourceRequest(), ca_number, NomorNotaRetur, user);
                        //dataSource = Service<NoturService>().GetNotaReturMv(new DataSourceRequest(), inv.invoice_number, inv.NomorNotaRetur, user);

                            //new
                            List<SpNotaRetur> distinctNomorFaktur = dataSource
                                                                   .GroupBy(s => s.da_no)
                                                                   .Select(g => g.First())
                                                                   .ToList();


                        url = Path.Combine(downloadfileori, outputfile);
                        Byte[] bytes = System.IO.File.ReadAllBytes(url);
                        fileBytes = bytes;
                        //using (var memoryStream = new MemoryStream())
                        //    {
                        //        using (var ziparchive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
                        //        {
                        //            foreach (SpNotaRetur daNo in distinctNomorFaktur)
                        //            {
                        //                filename = daNo.da_no + ".pdf";
                        //                var fileInArchive = ziparchive.CreateEntry(filename);
                        //                url = Path.Combine(downloadfileori, filename);
                        //                //url = Path.Combine(fileInArchive);
                        //                Byte[] bytes = System.IO.File.ReadAllBytes(url);
                        //                fileBytes = bytes;

                        //                using (var zipStream = fileInArchive.Open())
                        //                using (var fileToCompressStream = new MemoryStream(fileBytes))
                        //                {
                        //                    fileToCompressStream.CopyTo(zipStream);
                        //                }

                        //                ////filename = Path.GetFileNameWithoutExtension(outputfile) + "\\" + "NotaRetur - " + notaRetur.NomorRetur.Replace("/", "-") + ".pdf";
                        //                //filename = Path.GetFileNameWithoutExtension(outputfile) + "\\" + notaRetur.NomorRetur.Replace("/", "-") + ".pdf";
                        //                //var fileInArchive = ziparchive.CreateEntry(filename);
                        //                //url = Path.Combine(downloadfileori, filename);

                        //                //fileBytes = SaveReportPdf(out warnings, out mimeType, out encoding, out extension, repPath, out streamIds, report, dataSource, notaRetur);

                        //                //using (var zipStream = fileInArchive.Open())
                        //                //using (var fileToCompressStream = new MemoryStream(fileBytes))
                        //                //{
                        //                //    fileToCompressStream.CopyTo(zipStream);
                        //                //}
                        //            }
                        //        }


                                var Update = Service<InvoiceService>().UpdateSparePartStatusDownload(
                                    ca_number,
                                    user,
                                    EnumHelper.GetDescription(DownloadType.NotaRetur),
                                    EnumHelper.GetDescription(InvoiceType.NotaRetur)
                                );

                                Response.Clear();
                                Response.AddHeader("Access-Control-Allow-Origin", "*");
                                Response.ContentType = "application/octet-stream";
                                Response.AddHeader("content-disposition", "attachment;filename=\"" + outputfile + "\"");
                               // Response.BinaryWrite(memoryStream.ToArray());
                                Response.OutputStream.Write(bytes, 0, bytes.Length); // create the file  
                                Response.Flush();
                                Response.End();
                           // }
                        //end
                        }

                    }
               // }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [FileDownload]
        public void DownloadNotaReturHyperlink(string ca_number, string credit_note_number, string business_unit)
        {
            string linkdata = string.Empty;
            string namafile = string.Empty;
            var username = Service<ParameterService>().GetParameter("file_impersonate_user").value1;
            var domain = Service<ParameterService>().GetParameter("file_impersonate_domain").value1;
            var userdomain = domain + "\\" + username;
            var password = Service<ParameterService>().GetParameter("file_impersonate_password").value1;

            var credential = new NetworkCredential(userdomain, password);
            string fileserver = Service<ParameterService>().GetParameter("file_server").value1;
            string filepath = Service<ParameterService>().GetParameter("file_repo_path_tax").value1;
            string file = "\\\\" + fileserver + "\\" + filepath;

            //var uploadFolder = "D:\\EFB_PATH\\TESTCREDITNOTE";
            string uploadFolder = ApplicationCacheManager.GetConfig<string>("FileUpload");

            try
            {
                using (new NetworkConnection(file, credential))
                {
                    string url = string.Empty;
                    if (credit_note_number != null)
                    {
                        url = Path.Combine(uploadFolder, credit_note_number + ".pdf");

                         
                            Response.Clear();
                            Response.AddHeader("Access-Control-Allow-Origin", "*");
                            Response.ContentType = "application/octet-stream";
                            Response.AddHeader("content-disposition", "attachment;filename=" + Path.GetFileName(credit_note_number + ".pdf"));
                            Response.WriteFile(url);
                            Response.Flush();
                            Response.End();
                        
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            /*
            HttpApplicationStateBase app = HttpContext.Application;
            byte[] fileBytes = null, zipBytes;
            string mimeType, extension;


            //var outputfile = "NOTARETUR - " + DateTime.Now.ToString("yyyyMMdd") + ".zip";
            var outputfile = DateTime.Now.ToString("yyyyMMdd") + ".zip";
            var details = app[ca_number] as List<Invoice>;
            var user = SessionManager.Current;
            try
            {

                using (var memoryStream = new MemoryStream())
                {
                    using (var ziparchive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
                    {
                        List<Invoice> data = details.ToList();

                        // Initial variable
                        var IsPrint = true;
                        int totalData = data.Count;
                        int mvData = data.Where(x => x.business_unit_id == 1).Count();
                        // 
                        IsPrint = totalData == mvData ? false : true;

                        if (IsPrint)
                        {
                            Reporting.Report report = new Reporting.Report();
                            foreach (Invoice inv in details.ToList())
                            {

                                if (inv.business_unit_id == 2)
                                {
                                    //var filename = "NOTARETUR - " + DateTime.Now.ToString("yyyyMMdd") + "\\" + "NOTARETUR -" + inv.invoice_number.Replace('/', '-') + "\\" + "NOTARETUR -" + inv.invoice_number.Replace('/', '-') + ".pdf";
                                    var filename = DateTime.Now.ToString("yyyyMMdd") + "\\" + inv.invoice_number.Replace('/', '-') + "\\" + inv.invoice_number.Replace('/', '-') + ".pdf";
                                    var fileInArchive = ziparchive.CreateEntry(filename);
                                    fileBytes = report.GetNotaRetur(
                                        inv.da_no,
                                        SessionManager.Name,
                                        out extension,
                                        out mimeType);

                                    using (var zipStream = fileInArchive.Open())
                                    using (var fileToCompressStream = new MemoryStream(fileBytes))
                                    {
                                        fileToCompressStream.CopyTo(zipStream);
                                    }

                                }
                                else if (inv.business_unit_id == 1) // mv
                                {
                                    List<SpNotaRetur> dataSource = new List<SpNotaRetur>();
                                    dataSource = Service<NoturService>().GetNotaReturMv(new DataSourceRequest(), inv.invoice_number, inv.invoice_number, user);

                                    List<SpNotaRetur> distinctNomorFaktur = dataSource
                                                                                .GroupBy(s => s.NomorRetur)
                                                                                .Select(g => g.First())
                                                                                .ToList();

                                    foreach (SpNotaRetur notaRetur in distinctNomorFaktur)
                                    {
                                        //var filename = Path.GetFileNameWithoutExtension(outputfile) + "\\" + "NotaRetur - " + inv.invoice_number.Replace("/", "-") + "\\" + "NotaRetur - " + notaRetur.NomorRetur + ".pdf";
                                        var filename = Path.GetFileNameWithoutExtension(outputfile) + "\\" + inv.invoice_number.Replace("/", "-") + "\\" + notaRetur.NomorRetur + ".pdf";
                                        var fileInArchive = ziparchive.CreateEntry(filename);

                                        fileBytes = report.GetNotaReturSpOrMv(
                                            dataSource,
                                            notaRetur,
                                            out extension,
                                            out mimeType);

                                        using (var zipStream = fileInArchive.Open())
                                        using (var fileToCompressStream = new MemoryStream(fileBytes))
                                        {
                                            fileToCompressStream.CopyTo(zipStream);
                                        }
                                    }
                                }
                                else if (inv.business_unit_id == 3) // spare part invoice
                                {
                                    List<SpNotaRetur> dataSource = new List<SpNotaRetur>();
                                    dataSource = Service<NoturService>().GetNotaReturSp(new DataSourceRequest(), inv.invoice_number, user);

                                    List<SpNotaRetur> distinctNomorFaktur = dataSource
                                                                                .GroupBy(s => s.NomorRetur)
                                                                                .Select(g => g.First())
                                                                                .ToList();

                                    foreach (SpNotaRetur notaRetur in distinctNomorFaktur)
                                    {
                                        //var filename = Path.GetFileNameWithoutExtension(outputfile) + "\\"+"NotaRetur - " + inv.invoice_number.Replace("/", "-") + "\\" + "NotaRetur - " + notaRetur.NomorRetur + ".pdf";
                                        var filename = Path.GetFileNameWithoutExtension(outputfile) + "\\" + inv.invoice_number.Replace("/", "-") + "\\" + notaRetur.NomorRetur + ".pdf";
                                        var fileInArchive = ziparchive.CreateEntry(filename);

                                        fileBytes = report.GetNotaReturSpOrMv(
                                            dataSource,
                                            notaRetur,
                                            out extension,
                                            out mimeType);

                                        using (var zipStream = fileInArchive.Open())
                                        using (var fileToCompressStream = new MemoryStream(fileBytes))
                                        {
                                            fileToCompressStream.CopyTo(zipStream);
                                        }
                                    }
                                }
                                else
                                {
                                    fileBytes = null;
                                }



                                if (inv.business_unit_id != 3) // not spare part invoice
                                {
                                    var Update = Service<NotaReturService>().GetUpdate(inv.da_no);
                                }
                                else
                                {
                                    var Update = Service<InvoiceService>().UpdateSparePartStatusDownload(
                                       inv.invoice_number,
                                       user,
                                       EnumHelper.GetDescription(DownloadType.NotaRetur),
                                       EnumHelper.GetDescription(InvoiceType.NotaRetur)
                                   );
                                }
                            }

                            ziparchive.Dispose();


                            Response.Clear();
                            Response.AddHeader("Access-Control-Allow-Origin", "*");
                            Response.ContentType = "application/octet-stream";
                            Response.AddHeader("content-disposition", "attachment;filename=\"" + outputfile + "\"");
                            Response.BinaryWrite(memoryStream.ToArray());
                            Response.Flush();
                            Response.End();
                        }
                        else
                        {
                            //
                            throw new Exception("MV is Not Available");

                        }
                    }
                }
            }

            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            */
        }

        private byte[] SaveAttachmentReportPdf(string daNumber, out Warning[] warnings, out string mimeType, out string encoding, out string extension, string repPath, out string[] streamIds, LocalReport report, List<AttachmentSpDebitAdvice> dataSource)
        {
            report.ReportPath = repPath;
            report.SetParameters(new ReportParameter("pDaNo", dataSource.First().DaNo));
            report.SetParameters(new ReportParameter("pDeliveryDate", dataSource.First().InvoiceDate.ToString("dd/MM/yyyy")));
            report.SetParameters(new ReportParameter("pCustomerName", dataSource.First().CustomerName));
            report.SetParameters(new ReportParameter("pCustomerAddress", dataSource.First().CustomerAddress));

            DataTable table = new DataTable();
            table.Columns.Add("InvoiceNo", type: typeof(string));
            table.Columns.Add("InvoiceDate", type: typeof(DateTime));
            table.Columns.Add("OT", type: typeof(string));
            table.Columns.Add("OrderNo", type: typeof(string));
            table.Columns.Add("Items", type: typeof(int));
            table.Columns.Add("GrossAmount", type: typeof(decimal));
            table.Columns.Add("NetAmount", type: typeof(decimal));

            foreach (AttachmentSpDebitAdvice a in dataSource)
            {
                DataRow row = table.NewRow();

                row["InvoiceNo"] = a.InvoiceNo;
                row["InvoiceDate"] = a.InvoiceDate;
                row["OT"] = a.OT;
                row["OrderNo"] = a.OrderNo;
                row["Items"] = a.Items;
                row["GrossAmount"] = a.GrossAmount;
                row["NetAmount"] = a.NetAmount;

                table.Rows.Add(row);
            }

            report.DataSources.Clear();
            report.DataSources.Add(new ReportDataSource("DataSetSpAttachment", table));

            report.Refresh();

            byte[] bytes = report.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            return bytes;
        }

        private byte[] SaveReportPdf(out Warning[] warnings, out string mimeType, out string encoding, out string extension, string repPath, out string[] streamIds, LocalReport report, List<SpNotaRetur> dataSource, SpNotaRetur notaRetur)
        {
            report.ReportPath = repPath;
            report.SetParameters(new ReportParameter("pNomorRetur", notaRetur.NomorRetur));
            report.SetParameters(new ReportParameter("pFakturPajakNo", notaRetur.FakturPajakNo));
            report.SetParameters(new ReportParameter("pTglRetur", notaRetur.TglRetur.ToString("dd MMMM yyyy")));
            report.SetParameters(new ReportParameter("pTglFakturPajak", notaRetur.TglFakturPajak.ToString("dd MMMM yyyy")));
            report.SetParameters(new ReportParameter("pCustName", notaRetur.CustName));
            report.SetParameters(new ReportParameter("pAddress1", notaRetur.Address1));
            report.SetParameters(new ReportParameter("pAddress2", notaRetur.Address2));
            report.SetParameters(new ReportParameter("pNpwp", notaRetur.Npwp));

            DataTable table = new DataTable();
            table.Columns.Add("NoUrut", type: typeof(string));
            table.Columns.Add("PiNo", type: typeof(string));
            table.Columns.Add("PartNo", type: typeof(string));
            table.Columns.Add("PartName", type: typeof(string));
            table.Columns.Add("Kuantum", type: typeof(int));
            table.Columns.Add("HargaSatuan", type: typeof(decimal));
            table.Columns.Add("HargaBkp", type: typeof(decimal));

            List<SpNotaRetur> distinctData = dataSource.FindAll(s => s.NomorRetur == notaRetur.NomorRetur).ToList();

            int baris = 1;
            foreach (SpNotaRetur a in distinctData)
            {
                var itemNo = baris;
                DataRow row = table.NewRow();

                row["NoUrut"] = itemNo;
                row["PiNo"] = a.PiNo;
                row["PartNo"] = a.PartNo;
                row["PartName"] = a.PartName;
                row["Kuantum"] = a.Kuantum;
                row["HargaSatuan"] = a.HargaSatuan;
                row["HargaBkp"] = a.HargaBkp;

                table.Rows.Add(row);
                baris++;
            }

            //int chunk = 15;
            //List<List<SpNotaRetur>> chunkedList = SplitList(distinctData, chunk);

            //int baris = 1;
            //foreach (List<SpNotaRetur> listNotur in chunkedList)
            //{
            //    foreach (SpNotaRetur a in listNotur)
            //    {
            //        var itemNo = baris;
            //        DataRow row = table.NewRow();

            //        row["NoUrut"] = itemNo;
            //        row["PiNo"] = a.PiNo;
            //        row["PartNo"] = a.PartNo;
            //        row["Kuantum"] = a.Kuantum;
            //        row["HargaSatuan"] = a.HargaSatuan;
            //        row["HargaBkp"] = a.HargaBkp;

            //        table.Rows.Add(row);
            //        baris++;
            //    }

            //    // add blank row
            //    if(listNotur.Count < chunk)
            //    {
            //        for(int i=0; i < (chunk - listNotur.Count); i++)
            //        {
            //            DataRow row = table.NewRow();
            //            table.Rows.Add(row);
            //            baris++;
            //        }
            //    }
            //}

            report.DataSources.Clear();
            report.DataSources.Add(new ReportDataSource("DataSetSpNotaReturDetail", table));

            report.Refresh();

            byte[] bytes = report.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            return bytes;
        }

        private static List<List<SpNotaRetur>> SplitList(List<SpNotaRetur> notaReturs, int chunk = 10)
        {
            var list = new List<List<SpNotaRetur>>();

            for (int i = 0; i < notaReturs.Count; i += chunk)
            {
                list.Add(notaReturs.GetRange(i, Math.Min(chunk, notaReturs.Count - i)));
            }

            return list;
        }

        [FileDownload]
        public FileResult ExportReportNotaRetur(string sid, Int32 total)
        {
            HttpApplicationStateBase app = HttpContext.Application;
            var strclmn = new string[] { "No", "Nomor Nota Retur", "Tanggal Retur", "Nomor Credit Advice", "No. Faktur Pajak Yang DiRetur"
                , "Nomor DA Yang DiRetur", "Tgl. Faktur Pajak Yang DiRetur", "Customer NPWP", "Customer Name", "DPP", "PPN", "BUSINESS UNIT" };

            var details = app[sid] as List<Invoice>;

            // code to create workbook 
            if (total < 3000)
            {
                using (var exportData = new MemoryStream())
                {
                    var workbook = new XSSFWorkbook();
                    var sheet = workbook.CreateSheet("VATCreditNoteReport");
                    var headerRow = sheet.CreateRow(0);

                    int i = 0;

                    foreach (string c in strclmn)
                    {

                        // Create New Cell
                        var headerCell = headerRow.CreateCell(i);

                        // Set Cell Value
                        headerCell.SetCellValue(c);

                        // Create Style
                        var headerCellStyle = workbook.CreateCellStyle();
                        headerCellStyle.FillForegroundColor = IndexedColors.Aqua.Index;
                        headerCellStyle.FillPattern = FillPattern.SolidForeground;

                        // Add Style to Cell
                        headerCell.CellStyle = headerCellStyle;

                        sheet.AutoSizeColumn(i);
                        i++;
                    }
                    //var data = Service<InvoiceService>().GetViewDataSourceResult(new DataSourceRequest(), 1, SessionManager.RoleStr);

                    i = 0;
                    foreach (Invoice inv in details.ToList())
                    {
                        var content = sheet.CreateRow(i + 1);
                        content.CreateCell(0).SetCellValue(i + 1);
                        //content.CreateCell(1).SetCellValue(inv.NomorNotaRetur);
                        content.CreateCell(1).SetCellValue(inv.credit_note_number);
                        content.CreateCell(2).SetCellValue(inv.TanggalRetur.ToString("dd/MM/yyyy"));
                        content.CreateCell(3).SetCellValue(inv.invoice_number);
                        content.CreateCell(4).SetCellValue(inv.tax_invoice);
                        content.CreateCell(5).SetCellValue(inv.NomorDAYangDiRetur);
                        //content.CreateCell(6).SetCellValue(inv.TglFaktuPajakYangDiRetur.ToString("dd/MM/yyyy"));
                        content.CreateCell(6).SetCellValue(inv.tax_date.ToString("dd/MM/yyyy"));
                        content.CreateCell(7).SetCellValue(inv.customer_npwp);
                        content.CreateCell(8).SetCellValue(inv.customer_name);
                        content.CreateCell(9).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0}", Math.Abs(inv.dpp)));
                        content.CreateCell(10).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0}", Math.Abs(inv.PPN)));
                        content.CreateCell(11).SetCellValue(inv.business_unit);
                        i++;
                    }


                    workbook.Write(exportData);
                    string saveAsFileName = string.Format("VATCreditNoteReport_{0:d}.xlsx", DateTime.Now).Replace("/", "");

                    byte[] bytes = exportData.ToArray();
                    return File(bytes, "application/vnd.ms-excel", saveAsFileName);

                }
            }

            else
            {
                throw new Exception("Export Excel Failed, Total row can not more than 3000");
            }


        }

        //isti 20201119
        [FileDownload]
        public FileResult DownloadCAReport(string sid, Int32 total)
        {

            HttpApplicationStateBase app = HttpContext.Application;

            var strclmn = new string[] { "CA No", "Transaction Type", "Doc date", "Due Date", "Currency"
                        , "DPP", "Tax Amount", "Dormitory", "VAT Dormitory", "Total Amount", "Tax Invoice", "PPH 22 Amount", "PPH No", "AR Status", "Dealer Code", "Dealer Name" };

            var details = app[sid] as List<Invoice>;



            if (total < 3000)
            {
                // code to create workbook 
                if (total < SpreadsheetVersion.EXCEL2007.MaxRows)
                {
                    using (var exportData = new MemoryStream())
                    {
                        var workbook = new XSSFWorkbook();
                        var sheet = workbook.CreateSheet("CAReport");
                        var headerRow = sheet.CreateRow(0);

                        int i = 0;

                        foreach (string c in strclmn)
                        {

                            // Create New Cell
                            var headerCell = headerRow.CreateCell(i);

                            // Set Cell Value
                            headerCell.SetCellValue(c);

                            // Create Style
                            var headerCellStyle = workbook.CreateCellStyle();
                            headerCellStyle.FillForegroundColor = IndexedColors.Aqua.Index;
                            headerCellStyle.FillPattern = FillPattern.SolidForeground;

                            // Add Style to Cell
                            headerCell.CellStyle = headerCellStyle;

                            sheet.AutoSizeColumn(i);
                            i++;
                        }
                        //var data = Service<InvoiceService>().GetViewDataSourceResult(new DataSourceRequest(), 1, SessionManager.RoleStr);

                        i = 0;
                        List<Invoice> chkData = new List<Invoice>();
                        //var _Dat = details.GroupBy(details => details.invoice_number);
                        //var dpp = chkData.Where(x => x.dpp);
                        //int totalDPP = chkData.Sum(dpp => Convert.ToInt32(dpp));
                        //var _sumDPP = new List<int>();
                        //var _sumTAX = new List<int>();
                        decimal _totalDPP = 0;
                        decimal _totalTAX = 0;
                        string _gabTaxInvoice = "";
                        string Tagar = "#";
                        //var totalTAX = chkData.Sum(x => x.tax ++);
                        //var totalTotalAmount = chkData.Sum(x => x.total_amount ++);
                        foreach (Invoice dat in details.ToList())
                        {
                            if (!chkData.Any(x => x.invoice_number == dat.invoice_number))
                            {
                                Tagar = "";
                                _totalDPP = dat.dpp;
                                _totalTAX = dat.tax;

                                if (dat.tax_invoice != null)
                                {
                                    _gabTaxInvoice = Tagar + dat.tax_invoice.Trim() ;
                                }
                                chkData.Add(dat);
                            

                            }
                            else
                            {
                                //_totalDPP = _totalDPP + dat.dpp;
                                //_totalTAX = _totalTAX + dat.tax;
                                Tagar = "#";
                                _totalDPP = _totalDPP + dat.dpp;
                                _totalTAX = _totalTAX + dat.tax;
                                if (dat.tax_invoice != null)
                                {
                                    _gabTaxInvoice = _gabTaxInvoice + Tagar + dat.tax_invoice.Trim();

                                }

                                //continue;
                                //var x = chkData;
                                var x = chkData.Where(b => b.invoice_number == dat.invoice_number).FirstOrDefault();
                                x.dpp = _totalDPP;
                                x.tax = _totalTAX;
                                x.tax_invoice = _gabTaxInvoice;


                            }
                            
                            
                        }
                        foreach (Invoice inv in chkData)
                        {
                            
                            /*isti 20201119
                            var content = sheet.CreateRow(i + 1);
                            content.CreateCell(0).SetCellValue("");
                            content.CreateCell(1).SetCellValue(notaRetur.NomorNotaRetur);
                            content.CreateCell(2).SetCellValue(notaRetur.TglRetur.ToString("dd/MM/yyyy"));
                            content.CreateCell(3).SetCellValue(notaRetur.NomorCreditAdvice);
                            content.CreateCell(4).SetCellValue(notaRetur.NomorFakturPajakRetur);
                            content.CreateCell(5).SetCellValue(notaRetur.NomorDaRetur);
                            content.CreateCell(6).SetCellValue(notaRetur.TglFakturPajakRetur.ToString("dd/MM/yyyy"));
                            content.CreateCell(7).SetCellValue(notaRetur.CustomerNpwp);
                            content.CreateCell(8).SetCellValue(notaRetur.CustomerName);
                            content.CreateCell(9).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0:N0}", notaRetur.dpp));
                            content.CreateCell(10).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0:N0}", notaRetur.ppn));
                            content.CreateCell(11).SetCellValue(notaRetur.businessUnit);
                            i++;
                            */

                            var content = sheet.CreateRow(i + 1);
                            content.CreateCell(0).SetCellValue(inv.invoice_number);
                            content.CreateCell(1).SetCellValue(inv.transaction_type);
                            content.CreateCell(2).SetCellValue(inv.doc_date.ToString("dd/MM/yyyy"));
                            content.CreateCell(3).SetCellValue(inv.due_date.ToString("dd/MM/yyyy"));
                            content.CreateCell(4).SetCellValue(inv.currency);

                            if (inv.business_unit != "Spare Part")
                            {
                                content.CreateCell(5).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0}", inv.dpp));
                                content.CreateCell(6).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0}", inv.tax));
                                //content.CreateCell(6).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0}", inv.PPN ));
                                content.CreateCell(7).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0:0}", inv.dormitory));
                                content.CreateCell(8).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0:0}", inv.vat_dormitory));
                                //content.CreateCell(9).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0}", inv.total_amount));
                                content.CreateCell(9).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0}", (inv.dpp + inv.tax) ));
                            }
                            else
                            {
                                content.CreateCell(5).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0}", inv.dpp));
                                //content.CreateCell(5).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0}", _totalDPP));
                                //content.CreateCell(6).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0}", inv.tax ));
                                content.CreateCell(6).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0}", inv.tax));
                                content.CreateCell(7).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0:0}", inv.dormitory ));
                                content.CreateCell(8).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0:0}", inv.vat_dormitory ));
                                content.CreateCell(9).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0}", inv.dpp + inv.tax));
                                //content.CreateCell(9).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0}", _totalDPP + inv.tax));
                                //content.CreateCell(9).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0}", inv.total_amount));
                            }
                            content.CreateCell(10).SetCellValue(inv.tax_invoice);

                            if (inv.business_unit != "Spare Part")
                            {
                                content.CreateCell(11).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0:0}", inv.amount_pph ));
                            }
                            else
                            {
                                content.CreateCell(11).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0:0}", inv.amount_pph));
                            }

                            content.CreateCell(12).SetCellValue(inv.pph_no);
                            content.CreateCell(13).SetCellValue(inv.ar_status_name);
                            content.CreateCell(14).SetCellValue(inv.da_code);
                            content.CreateCell(15).SetCellValue(inv.customer_name);
                            i++;
                        }


                        workbook.Write(exportData);
                        string saveAsFileName = string.Format("CAReport_{0:d}.xlsx", DateTime.Now).Replace("/", "");

                        byte[] bytes = exportData.ToArray();
                        return File(bytes, "application/vnd.ms-excel", saveAsFileName);

                    }
                }
                else
                {
                    throw new Exception("Export Excel Failed, Total row can not more than 3000");
                }
            }
            else
            {
                throw new Exception("Export Excel Failed, Total row can not more than 3000");
            }

        }


        [FileDownload]
        public FileResult DownloadCSVFile(string sid, Int32 total)
        {
            string fullPath = string.Empty;

            int totalDataCount = 0;

            HttpApplicationStateBase app = HttpContext.Application;
            String[] strclmn = new string[] { "RM", "NPWP", "NAMA", "KD_JENIS_TRANSAKSI", "FG_PENGGANTI"
                        , "NOMOR_FAKTUR", "TANGGAL_FAKTUR", "IS_CREDITABLE", "NOMOR_DOKUMEN_RETUR", "TANGGAL_RETUR"
                        , "MASA_PAJAK_RETUR", "TAHUN_PAJAK_RETUR", "NILAI_RETUR_DPP", "NILAI_RETUR_PPN", "NILAI_RETUR_PPNBM"};
            var details = app[sid] as List<Invoice>;

            List<string[]> ListArrstrclmn = new List<string[]>();
            ListArrstrclmn.Add(strclmn);
            List<string[]> ListArr = new List<string[]>();


            // code to create workbook 
            if (total < 3000)
            {
                foreach (Invoice obj in details)
                {
                    string isCreditable = string.Empty;
                    if (obj.isCreditable == true)
                        isCreditable = "0";
                    else
                        isCreditable = "1";

                    String[] myArr = new string[] {
                            string.IsNullOrWhiteSpace(obj.RM) ? "" : obj.RM,
                            string.IsNullOrWhiteSpace(obj.npwpReportCSV) ? "" : obj.npwpReportCSV.Replace(".", "").Replace("-", "").ToString(),
                            string.IsNullOrWhiteSpace(obj.nameReportCSV) ? "" : obj.nameReportCSV,
                            //string.IsNullOrWhiteSpace(Convert.ToString(obj.transaction_type_id)) ? "" : obj.transaction_type_id.ToString("00"),
                            string.IsNullOrWhiteSpace(obj.KDJenisTransaksi) ? "" : obj.KDJenisTransaksi,
                            string.IsNullOrWhiteSpace(obj.FGPengganti) ? "" : obj.FGPengganti,
                            //string.IsNullOrWhiteSpace(obj.NoFakturPajakYangDiRetur) ? "" :  obj.NoFakturPajakYangDiRetur.Replace(".", "").Replace("-", "").ToString().Trim().Right(13),
                            //string.IsNullOrWhiteSpace(obj.tax_invoice) ? "" : obj.tax_invoice.Right(15).Replace(".", "").Replace("-", "").ToString().Trim(),
                            string.IsNullOrWhiteSpace(obj.NoFakturPajakYangDiRetur) ? "" :  obj.tax_invoice.Replace(".", "").Replace("-", "").ToString().Trim().Right(13),
                            string.IsNullOrWhiteSpace(Convert.ToString(obj.tax_date)) ? "" : obj.tax_date.ToString("dd/MM/yyyy"),
                            string.IsNullOrWhiteSpace(isCreditable) ? "" : isCreditable,
                            string.IsNullOrWhiteSpace(obj.NomorNotaRetur) ? "" : obj.NomorNotaRetur,
                            string.IsNullOrWhiteSpace(Convert.ToString(obj.TanggalRetur)) ? "" : obj.TanggalRetur.ToString("dd/MM/yyyy"),
                            string.IsNullOrWhiteSpace(obj.MasaPajakRetur) ? "" : obj.MasaPajakRetur,
                            string.IsNullOrWhiteSpace(obj.TahunPajakRetur) ? "" : obj.TahunPajakRetur,
                            String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0}", Math.Abs(obj.dpp)),
                            String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0}", Math.Abs(obj.tax)),
                            String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0}", obj.JumlahReturPPNBM)
                        };
                    ListArr.Add(myArr);
                }

                var TempDownloadFolder = ApplicationCacheManager.GetConfig<string>("TempDownloadFolder");
                string saveAsFileName = string.Format("CreditNoteConvertCSV_{0:d}.csv", DateTime.Now).Replace("/", "");
                if (ListArr.Count > 0)
                {
                    fullPath = CSVHelper.PutCSV(ListArr, new Invoice(), TempDownloadFolder, saveAsFileName, ListArrstrclmn);
                }
                
                return File(fullPath, "text/csv", saveAsFileName);

                /**
                using (var exportData = new MemoryStream())
                {
                    var workbook = new XSSFWorkbook();
                    var sheet = workbook.CreateSheet("CreditNoteConvertCSV");
                    var headerRow = sheet.CreateRow(0);

                    int i = 0;

                    foreach (string c in strclmn)
                    {

                        // Create New Cell
                        var headerCell = headerRow.CreateCell(i);

                        // Set Cell Value
                        headerCell.SetCellValue(c);

                        // Create Style
                        //var headerCellStyle = workbook.CreateCellStyle();
                        //headerCellStyle.FillForegroundColor = IndexedColors.Aqua.Index;
                        //headerCellStyle.FillPattern = FillPattern.SolidForeground;

                        // Add Style to Cell
                        //headerCell.CellStyle = headerCellStyle;

                        sheet.AutoSizeColumn(i);
                        i++;
                    }
                    //var data = Service<InvoiceService>().GetViewDataSourceResult(new DataSourceRequest(), 1, SessionManager.RoleStr);

                    i = 0;
                    foreach (Invoice inv in details.ToList())
                    {

                        var content = sheet.CreateRow(i + 1);
                        content.CreateCell(0).SetCellValue(inv.RM);

                        //- Isi Default kolom NPWP = 02.116.115.3-092.000
                        content.CreateCell(1).SetCellValue("02.116.115.3-092.000");
                        /*
                        if (inv.customer_npwp != null && inv.customer_npwp != "")
                        {
                            string cust_npwp = "";
                            if (inv.customer_npwp.Contains(".") || inv.customer_npwp.Contains(","))
                            {
                                cust_npwp = inv.customer_npwp.Replace("-", "").Replace(".", "");
                                content.CreateCell(1).SetCellValue(cust_npwp);
                            }
                            else
                                content.CreateCell(1).SetCellValue(inv.customer_npwp);
                        }
                        else
                            content.CreateCell(1).SetCellValue(inv.customer_npwp);
                            

                        //- Default Kolom NAMA = PT. TOYOTA-ASTRA MOTOR
                        //content.CreateCell(2).SetCellValue(inv.customer_name);
                        content.CreateCell(2).SetCellValue("PT. TOYOTA-ASTRA MOTOR");

                        content.CreateCell(3).SetCellValue(inv.transaction_type_id);
                        content.CreateCell(4).SetCellValue(inv.FGPengganti);
                        content.CreateCell(5).SetCellValue(inv.NoFakturPajakYangDiRetur);
                        content.CreateCell(6).SetCellValue(inv.TglFaktuPajakYangDiRetur.ToString("dd/MM/yyyy"));
                        /*
                         * - Value pada field IS_CREDITABLE diganti menjadi 1 atau 0
                        False = 1
                        True = 0
                         
                        if (inv.isCreditable == true)
                            content.CreateCell(7).SetCellValue("0");
                        else
                            content.CreateCell(7).SetCellValue("1");
                        //content.CreateCell(7).SetCellValue(inv.isCreditable);

                        content.CreateCell(8).SetCellValue(inv.NomorNotaRetur);
                        content.CreateCell(9).SetCellValue(inv.TanggalRetur.ToString("dd/MM/yyyy"));
                        content.CreateCell(10).SetCellValue(inv.MasaPajakRetur);
                        content.CreateCell(11).SetCellValue(inv.TahunPajakRetur);
                        content.CreateCell(12).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0}", inv.dpp));
                        content.CreateCell(13).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0}", inv.PPN));
                        content.CreateCell(14).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), " {0}", inv.JumlahReturPPNBM));

                        i++;
                    }


                    workbook.Write(exportData);
                    string saveAsFileName = string.Format("CreditNoteConvertCSV_{0:d}.csv", DateTime.Now).Replace("/", "");

                    byte[] bytes = exportData.ToArray();
                    return File(bytes, "application/vnd.ms-excel", saveAsFileName);

                }
           */
            }

            else
            {
                throw new Exception("Export Excel Failed, Total row can not more than 3000");
            }

        }

        //public JsonResult GetPDFFileExists([DataSourceRequest] DataSourceRequest request, string checks)
        //{
        //    var result = Service<InvoiceService>().GetViewDataSourceResult(request, Convert.ToInt16(EnumHelper.GetDescription(InvoiceType.NotaRetur)), SessionManager.RoleStr);
        //    Boolean pdfExists = false;


        //    foreach (Invoice inv in result.Data)
        //    {
        //        if (inv.PdfUrl != "" && inv.PdfUrl != null && System.IO.File.Exists(inv.PdfUrl) == true && new FileInfo(inv.PdfUrl).Length > 0)
        //        {
        //            pdfExists = true;
        //            break;
        //        }
        //    }

        //    if (pdfExists)
        //        return Json("Yes", JsonRequestBehavior.AllowGet);
        //    else
        //        return Json("No", JsonRequestBehavior.AllowGet);
        //}

        public void DownloadAllSignOff(string sid, Int32 total)
        {
            HttpApplicationStateBase app = HttpContext.Application;
            var details = app[sid] as List<Invoice>;

            // Initial variable
            // var IsPrint = true;
            int totalData = total;

            String files = string.Empty;
            byte[] fileBytes;


            string filename = string.Empty;
            var outputfile = "VATCreditNoteSignOff_" + DateTime.Now.ToString("yyyyMMdd") + "_" + DateTime.Now.ToString("Hmmss") + ".zip";

            string linkdata = string.Empty;
            string namafile = string.Empty;

            var username = Service<ParameterService>().GetParameter("file_impersonate_user").value1;
            var domain = Service<ParameterService>().GetParameter("file_impersonate_domain").value1;
            var userdomain = domain + "\\" + username;
            var password = Service<ParameterService>().GetParameter("file_impersonate_password").value1;

            var credential = new NetworkCredential(userdomain, password);
            string fileserver = Service<ParameterService>().GetParameter("file_server").value1;
            string filepath = Service<ParameterService>().GetParameter("file_repo_path_tax").value1;
            string file = "\\\\" + fileserver + "\\" + filepath;

            //var uploadFolder = "D:\\EFB_PATH\\TESTCREDITNOTE";

            var uploadFolder = ApplicationCacheManager.GetConfig<string>("FileUploadCreditnoteTTD");

            if (total < 3000)
            {
                try
                {

                    using (new NetworkConnection(file, credential))
                    {
                        if (totalData > 1)
                        {
                            using (var memoryStream = new MemoryStream())
                            {
                                using (var ziparchive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
                                {
                                    foreach (Invoice inv in details.ToList())
                                    {
                                        //var attachnotur = Service<NotaReturService>().GetNoturAttachByRef(inv.credit_note_number, "NoturDGT");
                                        string url = string.Empty;
                                        filename = inv.credit_note_number + ".pdf";
                                        //if (attachnotur != null)
                                        //{
                                        url = Path.Combine(uploadFolder, filename);
                                        //}

                                        if (System.IO.File.Exists(url) == true)
                                        {
                                            var fileInArchive = ziparchive.CreateEntry(filename);

                                            Byte[] bytes = System.IO.File.ReadAllBytes(url);
                                            fileBytes = bytes;

                                            using (var zipStream = fileInArchive.Open())
                                            using (var fileToCompressStream = new MemoryStream(fileBytes))
                                            {
                                                fileToCompressStream.CopyTo(zipStream);
                                            }
                                        }
                                    }
                                    ziparchive.Dispose();

                                }
                                Response.Clear();
                                Response.AddHeader("Access-Control-Allow-Origin", "*");
                                Response.ContentType = "application/octet-stream";
                                Response.AddHeader("content-disposition", "attachment;filename=" + outputfile);
                                Response.BinaryWrite(memoryStream.ToArray());
                                Response.Flush();
                                Response.End();
                            }
                        }
                        else
                        {
                            foreach (Invoice inv in details.ToList())
                            {
                                //var attachnotur = Service<NotaReturService>().GetNoturAttachByRef(inv.credit_note_number, "NoturDGT");
                                filename = inv.credit_note_number + ".pdf";
                                var url = Path.Combine(uploadFolder, filename);

                                if (System.IO.File.Exists(url) == true)
                                {
                                    FileStream fs = new FileStream(url, FileMode.Open, FileAccess.Read);
                                    //return File(fs, "application/pdf");
                                    Response.ClearContent();
                                    Response.ContentType = "application/pdf";
                                    Response.AddHeader("Content-Disposition", string.Format("attachment; filename = {0}", System.IO.Path.GetFileName(url)));
                                    Response.TransmitFile(url);
                                    Response.Flush();
                                    Response.End();
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }

            else
            {
                throw new Exception("Export Excel Failed, Total row can not more than 3000");
            }
        }


        public void DownloadAllDGT(string sid, Int32 total)
        {
            HttpApplicationStateBase app = HttpContext.Application;
            var details = app[sid] as List<Invoice>;

            // Initial variable
            // var IsPrint = true;
            int totalData = total;

            String files = string.Empty;
            byte[] fileBytes;


            string filename = string.Empty;
            var outputfile = "VATCreditNoteDGT_" + DateTime.Now.ToString("yyyyMMdd") + "_" + DateTime.Now.ToString("Hmmss") + ".zip";

            string linkdata = string.Empty;
            string namafile = string.Empty;

            var username = Service<ParameterService>().GetParameter("file_impersonate_user").value1;
            var domain = Service<ParameterService>().GetParameter("file_impersonate_domain").value1;
            var userdomain = domain + "\\" + username;
            var password = Service<ParameterService>().GetParameter("file_impersonate_password").value1;

            var credential = new NetworkCredential(userdomain, password);
            string fileserver = Service<ParameterService>().GetParameter("file_server").value1;
            string filepath = Service<ParameterService>().GetParameter("file_repo_path_tax").value1;
            string file = "\\\\" + fileserver + "\\" + filepath;

            //var uploadFolder = "D:\\EFB_PATH\\TESTCREDITNOTE";

            var uploadFolder = ApplicationCacheManager.GetConfig<string>("FileUploadDGT");

            if (total < 3000)
            {
                try
                {

                    using (new NetworkConnection(file, credential))
                    {
                        if (totalData > 1)
                        {
                            using (var memoryStream = new MemoryStream())
                            {
                                using (var ziparchive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
                                {
                                    foreach (Invoice inv in details.ToList())
                                    {
                                        //var attachnotur = Service<NotaReturService>().GetNoturAttachByRef(inv.credit_note_number, "NoturDGT");
                                        string url = string.Empty;
                                        filename = inv.credit_note_number + ".pdf";
                                        //if (attachnotur != null)
                                        //{
                                        url = Path.Combine(uploadFolder, filename);
                                        //}

                                        if (System.IO.File.Exists(url) == true)
                                        {
                                            var fileInArchive = ziparchive.CreateEntry(filename);

                                            Byte[] bytes = System.IO.File.ReadAllBytes(url);
                                            fileBytes = bytes;

                                            using (var zipStream = fileInArchive.Open())
                                            using (var fileToCompressStream = new MemoryStream(fileBytes))
                                            {
                                                fileToCompressStream.CopyTo(zipStream);
                                            }
                                        }
                                    }
                                    ziparchive.Dispose();

                                }
                                Response.Clear();
                                Response.AddHeader("Access-Control-Allow-Origin", "*");
                                Response.ContentType = "application/octet-stream";
                                Response.AddHeader("content-disposition", "attachment;filename=" + outputfile);
                                Response.BinaryWrite(memoryStream.ToArray());
                                Response.Flush();
                                Response.End();
                            }
                        }
                        else
                        {
                            foreach (Invoice inv in details.ToList())
                            {
                                //var attachnotur = Service<NotaReturService>().GetNoturAttachByRef(inv.credit_note_number, "NoturDGT");
                                filename = inv.credit_note_number + ".pdf";
                                var url = Path.Combine(uploadFolder, filename);

                                if (System.IO.File.Exists(url) == true)
                                {
                                    FileStream fs = new FileStream(url, FileMode.Open, FileAccess.Read);
                                    //return File(fs, "application/pdf");
                                    Response.ClearContent();
                                    Response.ContentType = "application/pdf";
                                    Response.AddHeader("Content-Disposition", string.Format("attachment; filename = {0}", System.IO.Path.GetFileName(url)));
                                    Response.TransmitFile(url);
                                    Response.Flush();
                                    Response.End();
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }

            else
            {
                throw new Exception("Export Excel Failed, Total row can not more than 3000");
            }
        }
        public void DownloadNotaReturSignOff(string da_no, string business_unit, string status)
        {
            //var pdfid = Service<NotaReturService>().GetNoturAttachByRef(da_no, status);
            string linkdata = string.Empty;
            string namafile = string.Empty;
            namafile = da_no + ".pdf";
            var username = Service<ParameterService>().GetParameter("file_impersonate_user").value1;
            var domain = Service<ParameterService>().GetParameter("file_impersonate_domain").value1;
            var userdomain = domain + "\\" + username;
            var password = Service<ParameterService>().GetParameter("file_impersonate_password").value1;

            var credential = new NetworkCredential(userdomain, password);
            string fileserver = Service<ParameterService>().GetParameter("file_server").value1;
            string filepath = Service<ParameterService>().GetParameter("file_repo_path_tax").value1;
            string file = "\\\\" + fileserver + "\\" + filepath;

            //var uploadFolder = "D:\\EFB_PATH\\TESTCREDITNOTE";
            string uploadFolder = string.Empty;
            if (status == "NoturSignOff")
            {
                uploadFolder = ApplicationCacheManager.GetConfig<string>("FileUploadCreditnoteTTD");
            }
            else if (status == "NoturDGT")
            {
                uploadFolder = ApplicationCacheManager.GetConfig<string>("FileUploadDGT");
            }

            try
            {
                using (new NetworkConnection(file, credential))
                {
                    string url = string.Empty;
                    //if (pdfid != null)
                    //{
                        url = Path.Combine(uploadFolder, namafile);

                        if (System.IO.File.Exists(url) == true)
                        {
                            Response.Clear();
                            Response.AddHeader("Access-Control-Allow-Origin", "*");
                            Response.ContentType = "application/octet-stream";
                            Response.AddHeader("content-disposition", "attachment;filename=" + Path.GetFileName(namafile));
                            Response.WriteFile(url);
                            Response.Flush();
                            Response.End();
                        }
                        else
                    {
                        throw new Exception("File Not Exist");
                    }
                    //}
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        //public ActionResult downloadFileType(string filter, string sort, string page, string pageSize, string[] checks, bool checkedAll, string sid, string type)
        public ActionResult downloadFileType([DataSourceRequest] DataSourceRequest request, string checks, bool checkedAll, string sid, string type)
        {

            request.PageSize = 0;
            HttpApplicationStateBase app = HttpContext.Application;


            Boolean pdfExists = false;
            if(type == "careport")
            {
                var resultCA = Service<NoturService>().GetViewDataSourceResultDownloadCA(request, Convert.ToInt16(EnumHelper.GetDescription(InvoiceType.NotaRetur)), SessionManager.RoleStr, checks, checkedAll);
                List<Invoice> _noturCA = resultCA.Data.Cast<Invoice>().ToList();
                List<Invoice> chkData = new List<Invoice>();
                if(checkedAll == false)
                {
                    foreach (Invoice dat in _noturCA)
                        if (!chkData.Any(x => x.invoice_number == dat.invoice_number))
                        {
                            chkData.Add(dat);
                        }
                        else
                        {
                            continue;
                        }
                    app[sid] = chkData;
                }
                else
                {
                    app[sid] = _noturCA;
                }
                
                //if (type == "ttd" || type == "dgt" || type == "VatCreditNote")
                if (type == "ttd" || type == "dgt" || type == "VatCreditNote")
                {
                    var status = string.Empty;
                    string uploadFolder = string.Empty;
                    if (type == "ttd")
                    {
                        uploadFolder = ApplicationCacheManager.GetConfig<string>("FileUploadCreditnoteTTD");
                        status = "NoturSignOff";
                    }
                    else if (type == "dgt")
                    {
                        uploadFolder = ApplicationCacheManager.GetConfig<string>("FileUploadDGT");
                        status = "NoturDGT";
                    }
                    else if (type == "VatCreditNote")
                    {
                        uploadFolder = ApplicationCacheManager.GetConfig<string>("FileUpload");
                        status = "Notur";
                    }

                    foreach (Invoice inv in resultCA.Data)
                    {
                        //var attachnotur = Service<NotaReturService>().GetNoturAttachByRef(inv.credit_note_number, status);
                        string url = string.Empty;
                        url = Path.Combine(uploadFolder, inv.credit_note_number + ".pdf");

                        if (url != "" && url != null && System.IO.File.Exists(url) == true && new FileInfo(url).Length > 0)
                        {
                            pdfExists = true;
                            break;
                        }
                    }

                }

                return Json(new { result = "OK", totaldata = _noturCA.Count(), pdfstatus = pdfExists, message = "" });
            }
            else
            {
                var result = Service<NoturService>().GetViewDataSourceResult(request, Convert.ToInt16(EnumHelper.GetDescription(InvoiceType.NotaRetur)), SessionManager.RoleStr, checks, checkedAll);
                List<Invoice> _notur = result.Data.Cast<Invoice>().ToList();
                List<Invoice> chkData = new List<Invoice>();
                if (checkedAll == false)
                {
                    foreach (Invoice dat in _notur)
                        if (!chkData.Any(x => x.invoice_number == dat.invoice_number))
                        {
                            chkData.Add(dat);
                        }
                        else
                        {
                            continue;
                        }
                    app[sid] = chkData;
                }
                else
                {
                    app[sid] = _notur;
                }
                // app[sid] = chkData;

                if (type == "ttd" || type == "dgt" || type == "VatCreditNote")
                //if (type == "ttd" || type == "dgt")
                {
                    var status = string.Empty;
                    string uploadFolder = string.Empty;
                    if (type == "ttd")
                    {
                        uploadFolder = ApplicationCacheManager.GetConfig<string>("FileUploadCreditnoteTTD");
                        status = "NoturSignOff";
                    }
                    else if (type == "dgt")
                    {
                        uploadFolder = ApplicationCacheManager.GetConfig<string>("FileUploadDGT");
                        status = "NoturDGT";
                    }
                    else if (type == "VatCreditNote")
                    {
                        uploadFolder = ApplicationCacheManager.GetConfig<string>("FileUpload");
                        status = "Notur";
                    }

                    foreach (Invoice inv in result.Data)
                    {
                        //var attachnotur = Service<NotaReturService>().GetNoturAttachByRef(inv.credit_note_number, status);
                        string url = string.Empty;
                        url = Path.Combine(uploadFolder, inv.credit_note_number + ".pdf");

                        if (url != "" && url != null && System.IO.File.Exists(url) == true && new FileInfo(url).Length > 0)
                        {
                            pdfExists = true;
                            break;
                        }
                    }

                }

                return Json(new { result = "OK", totaldata = _notur.Count(), pdfstatus = pdfExists, message = "" });
            }
            

            //var uploadFolder = "D:\\EFB_PATH\\TESTCREDITNOTE";

            
        }
    }
}
