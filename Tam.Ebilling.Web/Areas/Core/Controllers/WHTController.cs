﻿using Agit.Helper.Helper;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tam.Ebilling.Domain;
using Tam.Ebilling.Infrastructure.Enum;
using Tam.Ebilling.Infrastructure.Mvc;
using Tam.Ebilling.Infrastructure.Session;
using Tam.Ebilling.Service;
using Tam.Ebilling.Domain.Modules.Core.Model;
using Tam.Ebilling.Infrastructure.Helper;
using Tam.Ebilling.Infrastructure.Barcode;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Configuration;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System.Globalization;
using System.IO.Compression;
using System.Net;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using System.Collections;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace Tam.Ebilling.Web.Areas.Core.Controllers
{
    public class WHTController : WebControllerBase
    {
        // GET: Core/WHT
        public ActionResult Index()
        {
            var WHT_tax_article = Service<WHTTaxArticleService>().GetWHTTaxArticle(SessionManager.RoleStr);
            ViewBag.WHTTaxArticle = WHT_tax_article;

            var WHT_map_code = Service<WHTMAPCodeService>().GetWHTMAPCode(SessionManager.RoleStr);
            ViewBag.WHTMAPCode = WHT_map_code;

            var WHT_dealer_name = Service<WHTDealerService>().GetWHTDealerName(SessionManager.RoleStr);
            ViewBag.WHTDealerName = WHT_dealer_name;

            var WHT_ori_dealer_name = Service<WHTDealerService>().GetWHTOriDealerName(SessionManager.RoleStr);
            ViewBag.WHTOriDealerName = WHT_ori_dealer_name;

            var WHT_dealer_npwp = Service<WHTDealerService>().GetWHTDealerNPWP(SessionManager.RoleStr);
            ViewBag.WHTDealerNPWP = WHT_dealer_npwp;

            var cust_id = SessionManager.RoleStr;
            ViewBag.Customer = cust_id;

            return View();
        }

        [HttpPost]
        public ActionResult Read([DataSourceRequest] Kendo.Mvc.UI.DataSourceRequest request)
        {
            var result = Service<WHTService>().GetViewDataSourceResult(request, SessionManager.RoleStr);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        
        public JsonResult Readid2([DataSourceRequest] DataSourceRequest request, string checks)
        {
            string[] cekcekall = checks.Split(',');

            if (!Directory.Exists(Server.MapPath("/TempWHT/")))
                Directory.CreateDirectory(Server.MapPath("/TempWHT/"));
            else if(Directory.Exists(Server.MapPath("/TempWHT/")))
            {
                DirectoryInfo dir = new DirectoryInfo(Server.MapPath("/TempWHT/"));
                foreach (FileInfo ftodelete in dir.GetFiles())
                {
                    ftodelete.Delete();
                }
            }

            DataSourceResult result;

            //if (checks == null || checks == "" || cekcekall[0] == "on")
            if (cekcekall[0] == "on")
            {
                var result2 = Service<WHTService>().GetViewDataSourceResult(request, SessionManager.RoleStr);
                List<Guid> list = new List<Guid>();
                list = result2.Data.Cast<WHT>().Select(_wht => _wht.id).ToList();

                //list = result.Data
                Guid[] intList = list.ToArray();
                result = Service<WHTService>().getchkwht(request, SessionManager.RoleStr, intList);
            }
            else
            {
                result = Service<WHTService>().GetChekWHTList(request, SessionManager.RoleStr, checks);
            }

            string linkdata = string.Empty;
            bool pdfnotexists = false;
            string mergedPDFFileName = "WHT_" + DateTime.Now.ToString("dd-MM-yyyy_HHmmss") + ".pdf";
            string mergedPDFFullPath = Path.Combine(Server.MapPath("/TempWHT/"), mergedPDFFileName);

            int i = 0;
            var username = Service<ParameterService>().GetParameter("file_impersonate_user").value1;
            var domain = Service<ParameterService>().GetParameter("file_impersonate_domain").value1;
            var userdomain = domain + "\\" + username;
            var password = Service<ParameterService>().GetParameter("file_impersonate_password").value1;

            var credential = new NetworkCredential(userdomain, password);
            string fileserver = Service<ParameterService>().GetParameter("file_server").value1;
            string filepath = Service<ParameterService>().GetParameter("file_repo_path_tax").value1;
            string file = "\\\\" + fileserver + "\\" + filepath;
            
            using (new NetworkConnection(file, credential))
            {
                try
            {
                using (PdfDocument mergedPDF = new PdfDocument())
                {
                    foreach (WHT data in result.Data)
                    {
                        linkdata = data.pdf_wht;
                        if (System.IO.File.Exists(linkdata) == true && new FileInfo(data.pdf_wht).Length > 0) //only when file exists
                        {
                            i++;
                            using (PdfDocument pdfSource = PdfReader.Open(@linkdata, PdfDocumentOpenMode.Import))
                            {
                                CopyPDFPages(pdfSource, mergedPDF);
                            }
                        }
                        else
                        {
                            pdfnotexists = true;
                        }
                    }
                    if (i > 0)
                    {
                        mergedPDF.Save(mergedPDFFullPath);
                    }
                    else if(pdfnotexists)
                    {
                        return Json("WHT File doesnt Exists", JsonRequestBehavior.AllowGet);
                    }

                }
                string fullPath = string.Empty;
                if (!string.IsNullOrEmpty(mergedPDFFileName))
                {
                    fullPath = "/TempWHT/" + mergedPDFFileName;
                }
                //return File(fullPath, System.Net.Mime.MediaTypeNames.Application.Octet, mergedPDFFileName);
                return Json(fullPath, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            }
        }

        public void DownloadWHTPDF2([DataSourceRequest] DataSourceRequest request, string checks)
        {
            string[] cekcekall = checks.Split(',');

            DataSourceResult result;

            //if (checks == null || checks == "" || cekcekall[0] == "on")
            if (cekcekall[0] == "on")
            {
                var result2 = Service<WHTService>().GetViewDataSourceResult(request, SessionManager.RoleStr);
                List<Guid> list = new List<Guid>();
                list = result2.Data.Cast<WHT>().Select(_wht => _wht.id).ToList();

                //list = result.Data
                Guid[] intList = list.ToArray();
                result = Service<WHTService>().getchkwht(request, SessionManager.RoleStr, intList);
            }
            else
            {
                result = Service<WHTService>().GetChekWHTList(request, SessionManager.RoleStr, checks);
            }

            String files = string.Empty;
            byte[] fileBytes;

            request.Page = 0;
            request.PageSize = 0;
            string filename = string.Empty;
            var outputfile = "WHT_" + DateTime.Now.ToString("yyyyMMdd") + "_" + DateTime.Now.ToString("Hmmss") + ".zip";

            string linkdata = string.Empty;
            string namafile = string.Empty;
            var username = Service<ParameterService>().GetParameter("file_impersonate_user").value1;
            var domain = Service<ParameterService>().GetParameter("file_impersonate_domain").value1;
            var userdomain = domain + "\\" + username;
            var password = Service<ParameterService>().GetParameter("file_impersonate_password").value1;

            var credential = new NetworkCredential(userdomain, password);
            string fileserver = Service<ParameterService>().GetParameter("file_server").value1;
            string filepath = Service<ParameterService>().GetParameter("file_repo_path_tax").value1;
            string file = "\\\\" + fileserver + "\\" + filepath;

            try
            {
                using (new NetworkConnection(file, credential))
                {
                    //var result = Service<WHTService>().GetDataPDF(request, SessionManager.RoleStr);

                    using (var memoryStream = new MemoryStream())
                    {
                        using (var ziparchive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
                        {
                            
                            foreach (WHT data in result.Data)
                            {
                                linkdata = data.pdf_wht;

                                if (System.IO.File.Exists(linkdata) == true && new FileInfo(data.pdf_wht).Length > 0)
                                {
                                    //filename = Path.GetFileNameWithoutExtension(outputfile) + "\\" + data.pdf_wht_fname;
                                    filename = data.pdf_wht_fname;
                                    var fileInArchive = ziparchive.CreateEntry(filename);

                                    Byte[] bytes = System.IO.File.ReadAllBytes(linkdata);
                                    fileBytes = bytes;

                                    using (var zipStream = fileInArchive.Open())
                                    using (var fileToCompressStream = new MemoryStream(fileBytes))
                                    {
                                        fileToCompressStream.CopyTo(zipStream);
                                    }
                                }
                            }
                            ziparchive.Dispose();
                        }
                        DownloadFinish();
                        Response.Clear();
                        Response.AddHeader("Access-Control-Allow-Origin", "*");
                        Response.ContentType = "application/octet-stream";
                        Response.AddHeader("content-disposition", "attachment;filename=" + outputfile);
                        Response.BinaryWrite(memoryStream.ToArray());
                        Response.Flush();
                        Response.End();
                    }
                }
            }
            catch (Exception e)
            {
                DownloadFinish();
                throw new Exception(e.Message);
            }
        }

        public FileResult DownloadReport2([DataSourceRequest] DataSourceRequest request, string checks)
        {
            string[] cekcekall = checks.Split(',');

            DataSourceResult result;
            //if (checks == null || checks == "" || cekcekall[0] == "on")
            if (cekcekall[0] == "on")
            {
                var result2 = Service<WHTService>().GetViewDataSourceResult(request, SessionManager.RoleStr);
                List<Guid> list = new List<Guid>();
                list = result2.Data.Cast<WHT>().Select(_wht => _wht.id).ToList();

                //list = result.Data
                Guid[] intList = list.ToArray();
                result = Service<WHTService>().getchkwht(request, SessionManager.RoleStr, intList);
            }
            else
            {
                result = Service<WHTService>().GetChekWHTList(request, SessionManager.RoleStr, checks);
            }

            var strclmn = new string[] { "Tax Article", "WHT Number", "WHT Date","Tax Period Month","Tax Period Year", "Dealer Name", "Original Dealer Name", "Dealer NPWP", "WHT Based Amount", "WHT Tarif", "WHT Amount", "Description", "Invoice Number", "Invoice Date", "eBupot Reference Number", "MAP Code", "Tax Type Description", "PDF WHT Status" };
            //var result = Service<WHTService>().GetViewDataSourceResult(request, SessionManager.RoleStr);

            List<WHT> total = result.Data.Cast<WHT>().ToList();

            // Initial variable
            // var IsPrint = true;
            int totalData = total.Count;

            // code to create workbook 
            if (totalData < 3000)
            {
                using (var exportData = new MemoryStream())
                {
                    var workbook = new XSSFWorkbook();
                    var sheet = workbook.CreateSheet("Report WHT");
                    var headerRow = sheet.CreateRow(0);

                    int i = 0;

                    foreach (string c in strclmn)
                    {

                        // Create New Cell
                        var headerCell = headerRow.CreateCell(i);

                        // Set Cell Value
                        headerCell.SetCellValue(c);

                        // Create Style
                        var headerCellStyle = workbook.CreateCellStyle();
                        headerCellStyle.FillForegroundColor = IndexedColors.Aqua.Index;
                        headerCellStyle.FillPattern = FillPattern.SolidForeground;

                        // Add Style to Cell
                        headerCell.CellStyle = headerCellStyle;

                        sheet.AutoSizeColumn(i);
                        i++;
                    }

                    i = 0;
                    foreach (WHT wht in result.Data)
                    {
                        var content = sheet.CreateRow(i + 1);
                        content.CreateCell(0).SetCellValue(wht.tax_article);
                        content.CreateCell(1).SetCellValue(wht.wht_number);
                        content.CreateCell(2).SetCellValue(wht.wht_date.ToString("dd/MM/yyyy"));
                        if(wht.tax_period_month.Length == 1)
                        {
                            content.CreateCell(3).SetCellValue("0" + wht.tax_period_month);
                        }
                        else
                        {
                            content.CreateCell(3).SetCellValue(wht.tax_period_month);
                        }
                        content.CreateCell(4).SetCellValue(wht.tax_period_year);
                        content.CreateCell(5).SetCellValue(wht.dealer_name);
                        content.CreateCell(6).SetCellValue(wht.original_dealer_name);
                        //content.CreateCell(7).SetCellValue(wht.dealer_address); //takeout 20190911
                        content.CreateCell(7).SetCellValue(wht.dealer_npwp);
                        content.CreateCell(8).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), "{0}", wht.wht_based_amount));
                        content.CreateCell(9).SetCellValue(String.Format("{0}", wht.wht_tarif) + "%");
                        content.CreateCell(10).SetCellValue(String.Format(CultureInfo.CreateSpecificCulture("id-id"), "{0}", wht.wht_amount));
                        content.CreateCell(11).SetCellValue(wht.wht_description);
                        content.CreateCell(12).SetCellValue(wht.invoice_number);
                        content.CreateCell(13).SetCellValue(wht.invoice_date.ToString("dd/MM/yyyy"));
                        content.CreateCell(14).SetCellValue(wht.ebupot_ref_num);
                        content.CreateCell(15).SetCellValue(wht.map_code);
                        content.CreateCell(16).SetCellValue(wht.trans_type_desc);
                        content.CreateCell(17).SetCellValue(wht.pdf_wht_status_ex);

                        i++;
                    }


                    workbook.Write(exportData);
                    string saveAsFileName = string.Format("WHT_{0:MM-dd-yyyy_HHmmss}.xlsx", DateTime.Now).Replace("/", "-");

                    byte[] bytes = exportData.ToArray();
                    DownloadFinish();
                    return File(bytes, "application/vnd.ms-excel", saveAsFileName);

                }
            }

            else
            {
                DownloadFinish();
                throw new Exception("Export Excel Failed, Total row can not more than 3000");
            }
        }
        
        public static void CopyPDFPages(PdfDocument from, PdfDocument to)
        {
            for (int i = 0; i < from.PageCount; i++)
            {
                to.AddPage(from.Pages[i]);
            }
        }

        public JsonResult GetPDFFileExists([DataSourceRequest] DataSourceRequest request, string checks)
        {
            DataSourceResult result = Service<WHTService>().GetPDFFileExists(request, SessionManager.RoleStr, checks);
            Boolean pdfExists = false;


            foreach (WHT data in result.Data)
            {
                if (data.pdf_wht != "" && data.pdf_wht != null && System.IO.File.Exists(data.pdf_wht) == true && new FileInfo(data.pdf_wht).Length > 0)
                {
                    pdfExists = true;
                    break;
                }
            }

            if(pdfExists)
                return Json("Yes", JsonRequestBehavior.AllowGet);
            else
                return Json("No", JsonRequestBehavior.AllowGet);
        }
    }
}