﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text;

namespace Tam.Ebilling.Web.Areas.Core.Controllers
{
    public class XlsHelper
    {
    }

    public static class CSVHelper
    {
        public static readonly string EXT = ".csv";

        public static string PutCSV(List<string[]> ListArr, object o, string TempDownloadFolder, string CsvName = null, List<string[]> ListHeader = null)
        {
            try
            {
                string fileName =
                        (string.IsNullOrEmpty(CsvName) ? o.GetType().Name : CsvName)
                        + "_" + DateTime.Now.ToString("dd-MM-yyyy_HHmmss")
                        + EXT;
                if (!EXT.Equals(Path.GetExtension(fileName)))
                {
                    fileName = Path.ChangeExtension(fileName, EXT);
                }
                string fullPath = Path.Combine(@TempDownloadFolder, fileName);

                var csv = new StringBuilder();

                foreach (string[] arrayStringHeader in ListHeader)
                {
                    string newLineHeader = string.Join(";", arrayStringHeader.Where(a => a != null)) + Environment.NewLine;
                    csv.Append(newLineHeader);
                }

                foreach (string[] arrayString in ListArr)
                {
                    string newLine = string.Join(";", arrayString) + Environment.NewLine;

                    csv.Append(newLine);
                }

                File.WriteAllText(fullPath, csv.ToString());

                return fullPath;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }


}
