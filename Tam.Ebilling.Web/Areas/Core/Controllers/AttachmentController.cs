﻿using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Tam.Ebilling.Infrastructure.Cache;
using Tam.Ebilling.Infrastructure.Mvc;
using Tam.Ebilling.Service;
using Tam.Ebilling.Infrastructure.Helper;
using System.IO.Compression;
using Tam.Ebilling.Domain;
using Agit.Helper.Helper;
using Tam.Ebilling.Infrastructure.Enum;
using Tam.Ebilling.Infrastructure.Session;
using Microsoft.Reporting.WebForms;
using System.Data;
using Tam.Ebilling.Web.Areas.Core.Models;

namespace Tam.Ebilling.Web.Areas.Core.Controllers
{
    public class AttachmentController : WebControllerBase
    {
        // GET: Core/Log
        public ActionResult Index()
        {
            return View();
        }


        public void Download(string id, string invoicenumber)
        {
            var username = Service<ParameterService>().GetParameter("file_impersonate_user").value1;
            var domain = Service<ParameterService>().GetParameter("file_impersonate_domain").value1;
            var userdomain = domain + "\\" + username;
            var password = Service<ParameterService>().GetParameter("file_impersonate_password").value1;

            var credential = new NetworkCredential(userdomain, password);
            string fileserver = Service<ParameterService>().GetParameter("file_server").value1;
            string filepath = Service<ParameterService>().GetParameter("file_repo_path").value1;
            string file2 = "\\\\" + fileserver + "\\" + filepath;
            try
            {
                using (new NetworkConnection(file2, credential))

                {
                    var da_code = Service<AttachmentService>().GetInvoiceRawDataByInvoice(invoicenumber) ;
                    var doc = Service<AttachmentService>().GetAttachment(da_code);
                    
                    if (doc.Count() == 0)
                    {
                        DownloadFinish(false);
                        throw new Exception("Invoice doesn't have attachment");
                    }

                    var outputfile = "Attachment - " + invoicenumber + ".zip";
                    var zipStream = new MemoryStream();
                    var item2 = doc.FirstOrDefault();
                    var filename2 = item2.created_date.ToString("yyyyM") + "\\" + item2.reference.Trim() + "\\" + item2.filename;
                    var url2 = Path.Combine(file2, filename2);
                    if (System.IO.File.Exists(url2) == true)
                    {
                        if (doc.Count > 0)
                        {
                            var zip = new ZipArchive(zipStream, ZipArchiveMode.Create);
                            var folder = doc[0].created_date;
                            foreach (var date in doc)
                            {
                                if (folder > date.created_date)
                                {
                                    folder = date.created_date;
                                }
                            }
                            foreach (var item in doc)
                            {

                                var filename = item.created_date.ToString("yyyyM") + "\\" + item.reference + "\\" + item.filename;
                                var url = Path.Combine(file2, filename);
                                zip.CreateEntryFromFile(url, "Attachment - " + invoicenumber.Replace('/', '-') + "\\" + item.filename);
                            }
                            zip.Dispose();
                        }
                        //else
                        //{
                        //    var item = doc.First();
                        //    var filename = item.created_date.ToString("yyyyM") + "\\" + item.reference + "\\" + item.filename;
                        //    var url = Path.Combine(file2, filename);
                        //    var file = new FileStream(url, FileMode.Open); byte[] bytes = new byte[file.Length];
                        //    file.Read(bytes, 0, (int)file.Length);
                        //    zipStream.Write(bytes, 0, (int)file.Length);
                        //    outputfile = item.filename;
                        //}
                        DownloadFinish();
                        
                        Response.Clear();
                        Response.AddHeader("Access-Control-Allow-Origin", "*");
                        Response.ContentType = "application/octet-stream";
                        Response.AddHeader("content-disposition", "attachment;filename=\"" + outputfile + "\"");
                        Response.BinaryWrite(zipStream.ToArray());
                        Response.Flush();
                        Response.End();
                    }
                    else if (System.IO.File.Exists(url2) == false)
                    {
                        DownloadFinish(false);
                        throw new Exception("Invoice doesn't have attachment");
                    }
                }
            }
            catch (Exception e)
            {
                DownloadFinish(false);
                throw new Exception(e.Message);
            }

        }


        public void DownloadAttachmentSparePart(string daNumber)
        {
            Stream stream = new MemoryStream();
            Warning[] warnings;
            string mimeType, encoding, extension, repPath = string.Empty;
            string[] streamIds;
            LocalReport report = new LocalReport();

            try
            {
                string user = SessionManager.Current;
                string userid = "";
                if (user != "TAM\\Mayumi" || user != "TAM\\frans" || user != "TAM\\ronnyk102181" || user != "TAM\\indah.gustiana" || user != "TAM\\aliandy.syahriel")
                {
                    user = userid;
                }
                else
                {
                    user = SessionManager.Current;
                }
                string name = SessionManager.Name;
                string nameid = "";
                if (name != "Mayumi Eguchi" || name != "Frans lhutan B" || name != "Ronny Kusgianto" || name != "Indah Sari Gustiana" || name != "Aliandy Syahriel")
                {
                    name = nameid;
                }
                else
                {
                    name = SessionManager.Name;
                }

                if (!string.IsNullOrEmpty(daNumber))// && !string.IsNullOrEmpty(user))
                {

                    repPath = string.Format("{0}/{1}", ConfigurationManager.AppSettings["RDLPath"], ConfigurationManager.AppSettings["SpAttDebitAdvicePath"]);
                    //repPath = string.Format("{0}/{1}", ConfigurationManager.AppSettings["RDLPath"], "Report1.rdlc");

                    var inv = Service<InvoiceService>().GetSingleInvoiceByInvoiceNumber(EnumHelper.GetDescription(InvoiceType.Invoice), daNumber);

                    List<AttachmentSpDebitAdvice> dataSource = Service<AttachmentService>().GetSpAttachment(daNumber, user, inv.doc_date.ToString("yyyy-MM-dd"));

                    report.ReportPath = repPath;
                    report.SetParameters(new ReportParameter("pDaNo", dataSource.First().DaNo));
                    report.SetParameters(new ReportParameter("pDeliveryDate", dataSource.First().InvoiceDate.ToString("dd/MM/yyyy")));
                    report.SetParameters(new ReportParameter("pCustomerName", dataSource.First().CustomerName));
                    report.SetParameters(new ReportParameter("pCustomerAddress", dataSource.First().CustomerAddress));

                    DataTable table = new DataTable();
                    table.Columns.Add("InvoiceNo", type: typeof(string));
                    table.Columns.Add("InvoiceDate", type: typeof(DateTime));
                    table.Columns.Add("OT", type: typeof(string));
                    table.Columns.Add("OrderNo", type: typeof(string));
                    table.Columns.Add("Items", type: typeof(int));
                    table.Columns.Add("GrossAmount", type: typeof(decimal));
                    table.Columns.Add("NetAmount", type: typeof(decimal));

                    foreach (AttachmentSpDebitAdvice a in dataSource)
                    {
                        DataRow row = table.NewRow();

                        row["InvoiceNo"] = a.InvoiceNo;
                        row["InvoiceDate"] = a.InvoiceDate;
                        row["OT"] = a.OT;
                        row["OrderNo"] = a.OrderNo;
                        row["Items"] = a.Items;
                        row["GrossAmount"] = a.GrossAmount;
                        row["NetAmount"] = a.NetAmount;

                        table.Rows.Add(row);
                    }

                    report.DataSources.Clear();
                    report.DataSources.Add(new ReportDataSource("DataSetSpAttachment", table));

                    report.Refresh();

                    byte[] bytes = report.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
                    Response.Buffer = true;
                    DownloadFinish();
                    Response.Clear();
                    Response.ContentType = mimeType;
                    Response.AddHeader("content-disposition", "attachment; filename= " + daNumber.Replace("/", "-") + "." + extension);
                    Response.OutputStream.Write(bytes, 0, bytes.Length); // create the file  
                    Response.Flush(); // send it to the client to download  
                    Response.End();
                }
            }
            catch (Exception e)
            {
                DownloadFinish();
                throw e;
            }
        }

        public void DownloadDeliveryNoteSparePart(string daNumber)
        {
            Stream stream = new MemoryStream();
            Warning[] warnings;
            string mimeType, encoding, extension, repPath = string.Empty;
            string[] streamIds;
            LocalReport report = new LocalReport();
            byte[] fileBytes;
            string filename = string.Empty;
            var outputfile = "DeliveryNote - " + daNumber.Replace("/", "-") + ".zip";
            try
            {
                string user = SessionManager.Current;
                string userid = "";
                if (user != "TAM\\Mayumi" || user != "TAM\\frans" || user != "TAM\\ronnyk102181" || user != "TAM\\indah.gustiana" || user != "TAM\\aliandy.syahriel")
                {
                    user = userid;
                }
                else
                {
                    user = SessionManager.Current;
                }
                string name = SessionManager.Name;
                string nameid = "";
                if (name != "Mayumi Eguchi" || name != "Frans lhutan B" || name != "Ronny Kusgianto" || name != "Indah Sari Gustiana" || name != "Aliandy Syahriel")
                {
                    name = nameid;
                }
                else
                {
                    name = SessionManager.Name;
                }

                List<SpDeliveryNote> dataSource = Service<AttachmentService>().GetSpDeliveryNote(daNumber, user);
                List<SpDeliveryNote> distinctInvoiceNo = dataSource
                                                            .GroupBy(s => s.InvoiceNo)
                                                            .Select(g => g.First())
                                                            .ToList();

                if (!string.IsNullOrEmpty(daNumber)
                    && dataSource != null
                    && dataSource.Count > 0)// && !string.IsNullOrEmpty(user))
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        using (var ziparchive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
                        {
                            repPath = string.Format("{0}/{1}", ConfigurationManager.AppSettings["RDLPath"], ConfigurationManager.AppSettings["SpDeliveryNotePreprintedPath"]);

                            foreach (SpDeliveryNote d in distinctInvoiceNo)
                            {
                                filename = Path.GetFileNameWithoutExtension(outputfile) + "\\" + "PI - " + d.InvoiceNo + ".pdf";
                                var fileInArchive = ziparchive.CreateEntry(filename);

                                fileBytes = SaveReportPdf(daNumber, out warnings, out mimeType, out encoding, out extension, repPath, out streamIds, report, dataSource, d);

                                using (var zipStream = fileInArchive.Open())
                                using (var fileToCompressStream = new MemoryStream(fileBytes))
                                {
                                    fileToCompressStream.CopyTo(zipStream);
                                }
                            }
                        }

                        var Update = Service<InvoiceService>().UpdateSparePartStatusDownload(
                        daNumber,
                        user,
                        EnumHelper.GetDescription(DownloadType.PI),
                        EnumHelper.GetDescription(InvoiceType.Invoice)
                        );
                        DownloadFinish();
                        Response.Clear();
                        Response.AddHeader("Access-Control-Allow-Origin", "*");
                        Response.ContentType = "application/octet-stream";
                        Response.AddHeader("content-disposition", "attachment;filename=\"" + outputfile + "\"");
                        Response.BinaryWrite(memoryStream.ToArray());
                        Response.Flush();
                        Response.End();
                    }
                }
                else
                {
                    DownloadFinish();
                    throw new Exception("No Delivery Note");
                }
            }
            catch (Exception e)
            {
                DownloadFinish();
                throw e;
            }
        }

        public void DownloadDeliveryNoteCreditSparePart(string daNumber)
        {
            Stream stream = new MemoryStream();
            Warning[] warnings;
            string mimeType, encoding, extension, repPath = string.Empty;
            string[] streamIds;
            LocalReport report = new LocalReport();
            byte[] fileBytes;
            string filename = string.Empty;
            var outputfile = "DeliveryNote - " + daNumber.Replace("/", "-") + ".zip";
            try
            {
                string user = SessionManager.Current;
                string userid = "";
                if (user != "TAM\\Mayumi" || user != "TAM\\frans" || user != "TAM\\ronnyk102181" || user != "TAM\\indah.gustiana" || user != "TAM\\aliandy.syahriel")
                {
                    user = userid;
                }
                else
                {
                    user = SessionManager.Current;
                }
                string name = SessionManager.Name;
                string nameid = "";
                if (name != "Mayumi Eguchi" || name != "Frans lhutan B" || name != "Ronny Kusgianto" || name != "Indah Sari Gustiana" || name != "Aliandy Syahriel")
                {
                    name = nameid;
                }
                else
                {
                    name = SessionManager.Name;
                }

                List<SpDeliveryNote> dataSource = Service<AttachmentService>().GetSpCreditDeliveryNote(daNumber, user);
                List<SpDeliveryNote> distinctInvoiceNo = dataSource
                                                            .GroupBy(s => s.InvoiceNo)
                                                            .Select(g => g.First())
                                                            .ToList();

                if (!string.IsNullOrEmpty(daNumber)
                    && dataSource != null
                    && dataSource.Count > 0)// && !string.IsNullOrEmpty(user))
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        using (var ziparchive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
                        {
                            repPath = string.Format("{0}/{1}", ConfigurationManager.AppSettings["RDLPath"], ConfigurationManager.AppSettings["SpDeliveryNotePreprintedPath"]);

                            foreach (SpDeliveryNote d in distinctInvoiceNo)
                            {
                                filename = Path.GetFileNameWithoutExtension(outputfile) + "\\" + "PI - " + d.InvoiceNo + ".pdf";
                                var fileInArchive = ziparchive.CreateEntry(filename);

                                fileBytes = SaveReportPdf(daNumber, out warnings, out mimeType, out encoding, out extension, repPath, out streamIds, report, dataSource, d);

                                using (var zipStream = fileInArchive.Open())
                                using (var fileToCompressStream = new MemoryStream(fileBytes))
                                {
                                    fileToCompressStream.CopyTo(zipStream);
                                }
                            }
                        }

                        var Update = Service<InvoiceService>().UpdateSparePartStatusDownload(
                        daNumber,
                        user,
                        EnumHelper.GetDescription(DownloadType.PI),
                        EnumHelper.GetDescription(InvoiceType.Invoice)
                        );
                        DownloadFinish();
                        Response.Clear();
                        Response.AddHeader("Access-Control-Allow-Origin", "*");
                        Response.ContentType = "application/octet-stream";
                        Response.AddHeader("content-disposition", "attachment;filename=\"" + outputfile + "\"");
                        Response.BinaryWrite(memoryStream.ToArray());
                        Response.Flush();
                        Response.End();
                    }
                }
                else
                {
                    DownloadFinish();
                    throw new Exception("No Delivery Note");
                }
            }
            catch (Exception e)
            {
                DownloadFinish();
                throw e;
            }
        }

        //private static void SaveReportPdf(string daNumber, string config, out Warning[] warnings, out string mimeType, out string encoding, out string extension, string repPath, out string[] streamIds, LocalReport report, List<SpDeliveryNote> dataSource, List<KeyValuePair<string, string>> urlPaths, SpDeliveryNote d)
        //{
        //    report.ReportPath = repPath;
        //    report.SetParameters(new ReportParameter("pMainDealer", dataSource.First().MainDealer));
        //    report.SetParameters(new ReportParameter("pDeliveryDate", dataSource.First().DeliveryDate.ToString("dd/MM/yyyy")));
        //    report.SetParameters(new ReportParameter("pCustomerName", dataSource.First().CustomerName));
        //    report.SetParameters(new ReportParameter("pCustomerAddress", dataSource.First().CustomerAddress));
        //    report.SetParameters(new ReportParameter("pTamRefNo", string.Empty));
        //    report.SetParameters(new ReportParameter("pInvoiceNo", d.InvoiceNo));
        //    report.SetParameters(new ReportParameter("pInvoiceDate", d.InvoiceDate.ToString("dd/MM/yyyy")));
        //    report.SetParameters(new ReportParameter("pRemarks", d.Remarks));
        //    report.SetParameters(new ReportParameter("pOrderNo", d.OrderNo));
        //    report.SetParameters(new ReportParameter("pOrderType", d.OrderType));

        //    DataTable table = new DataTable();
        //    table.Columns.Add("ItemNo", type: typeof(string));
        //    table.Columns.Add("PartNumber", type: typeof(string));
        //    table.Columns.Add("PartName", type: typeof(string));
        //    table.Columns.Add("Ordered", type: typeof(int));
        //    table.Columns.Add("Issued", type: typeof(int));
        //    table.Columns.Add("RetailPrice", type: typeof(decimal));
        //    table.Columns.Add("NetSalesPrice", type: typeof(decimal));
        //    table.Columns.Add("DiscRate", type: typeof(decimal));

        //    List<SpDeliveryNote> distinctData = dataSource.FindAll(s => s.InvoiceNo == d.InvoiceNo).ToList();

        //    foreach (SpDeliveryNote a in distinctData)
        //    {
        //        DataRow row = table.NewRow();

        //        row["ItemNo"] = "";
        //        row["PartNumber"] = a.PartNumber;
        //        row["PartName"] = a.PartName;
        //        row["Ordered"] = a.Ordered;
        //        row["Issued"] = a.Issued;
        //        row["RetailPrice"] = a.RetailPrice;
        //        row["NetSalesPrice"] = a.NetSalesPrice;
        //        row["DiscRate"] = a.DiscRate;

        //        table.Rows.Add(row);
        //    }

        //    report.DataSources.Clear();
        //    report.DataSources.Add(new ReportDataSource("DataSetDeliveryNote", table));

        //    report.Refresh();

        //    byte[] bytes = report.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

        //    var newFileNameTemp = d.InvoiceNo + "-" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + "." + extension;
        //    var newFileName = d.InvoiceNo + "." + extension;
        //    var daFileNameConcat= daNumber.Replace("/", "-") + "\\" + newFileNameTemp;
        //    var combineFileName = config + "\\" + daFileNameConcat;
        //    var url = Path.Combine(combineFileName);

        //    var dir = Path.GetDirectoryName(url);
        //    if (!Directory.Exists(dir))
        //        Directory.CreateDirectory(dir);

        //    urlPaths.Add(new KeyValuePair<string, string>(url, daFileNameConcat));

        //    System.IO.File.WriteAllBytes(url, bytes);
        //}

        private byte[] SaveReportPdf(string daNumber, out Warning[] warnings, out string mimeType, out string encoding, out string extension, string repPath, out string[] streamIds, LocalReport report, List<SpDeliveryNote> dataSource, SpDeliveryNote d)
        {
            report.ReportPath = repPath;
            report.SetParameters(new ReportParameter("pMainDealer", dataSource.First().MainDealer));
            report.SetParameters(new ReportParameter("pDeliveryDate", dataSource.First().DeliveryDate.ToString("dd/MM/yyyy")));
            report.SetParameters(new ReportParameter("pCustomerName", dataSource.First().CustomerName));
            report.SetParameters(new ReportParameter("pCustomerAddress", dataSource.First().CustomerAddress));
            report.SetParameters(new ReportParameter("pCustomerCode", dataSource.First().CustomerCode));
            report.SetParameters(new ReportParameter("pTamRefNo", string.Empty));
            report.SetParameters(new ReportParameter("pInvoiceNo", d.InvoiceNo));
            report.SetParameters(new ReportParameter("pInvoiceDate", d.InvoiceDate.ToString("dd/MM/yyyy")));
            report.SetParameters(new ReportParameter("pRemarks", d.Remarks));
            report.SetParameters(new ReportParameter("pOrderNo", d.OrderNo));
            report.SetParameters(new ReportParameter("pOrderType", d.OrderType));

            DataTable table = new DataTable();
            table.Columns.Add("ItemNo", type: typeof(string));
            table.Columns.Add("PartNumber", type: typeof(string));
            table.Columns.Add("PartName", type: typeof(string));
            table.Columns.Add("Ordered", type: typeof(int));
            table.Columns.Add("Issued", type: typeof(int));
            table.Columns.Add("RetailPrice", type: typeof(decimal));
            table.Columns.Add("NetSalesPrice", type: typeof(decimal));
            table.Columns.Add("DiscRate", type: typeof(decimal));

            List<SpDeliveryNote> distinctData = dataSource.FindAll(s => s.InvoiceNo == d.InvoiceNo).ToList();

            int baris = 1;
            var pad = '0';
            foreach (SpDeliveryNote a in distinctData)
            {
                var itemNo = baris.ToString().PadLeft(4, pad);
                DataRow row = table.NewRow();

                row["ItemNo"] = itemNo;
                row["PartNumber"] = a.PartNumber;
                row["PartName"] = a.PartName;
                row["Ordered"] = a.Ordered;
                row["Issued"] = a.Issued;
                row["RetailPrice"] = a.RetailPrice;
                row["NetSalesPrice"] = a.NetSalesPrice;
                row["DiscRate"] = a.DiscRate;

                table.Rows.Add(row);
                baris++;
            }

            report.DataSources.Clear();
            report.DataSources.Add(new ReportDataSource("DataSetDeliveryNote", table));

            report.Refresh();

            byte[] bytes = report.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            return bytes;
        }

        public void DownloadAllPI([DataSourceRequest] DataSourceRequest request)
        {
            Stream stream = new MemoryStream();
            Warning[] warnings;
            string mimeType, encoding, extension, repPath = string.Empty;
            string[] streamIds;
            LocalReport report = new LocalReport();
            byte[] fileBytes;
            string filename = string.Empty;
            var outputfile = "DeliveryNote - " + DateTime.Now.ToString("yyyyMMdd") + ".zip";

            try
            {
                string user = SessionManager.Current;
                string userid = "";
                if (user != "TAM\\Mayumi" || user != "TAM\\frans" || user != "TAM\\ronnyk102181" || user != "TAM\\indah.gustiana" || user != "TAM\\aliandy.syahriel")
                {
                    user = userid;
                }
                else
                {
                    user = SessionManager.Current;
                }
                string name = SessionManager.Name;
                string nameid = "";
                if (name != "Mayumi Eguchi" || name != "Frans lhutan B" || name != "Ronny Kusgianto" || name != "Indah Sari Gustiana" || name != "Aliandy Syahriel")
                {
                    name = nameid;
                }
                else
                {
                    name = SessionManager.Name;
                }

                var result = Service<InvoiceService>().GetViewDataSourceResult(request, Convert.ToInt16(EnumHelper.GetDescription(InvoiceType.Invoice)), SessionManager.RoleStr);

                if (result != null)// && !string.IsNullOrEmpty(user))
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        using (var ziparchive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
                        {
                            foreach (Invoice inv in result.Data)
                            {
                                repPath = string.Format("{0}/{1}", ConfigurationManager.AppSettings["RDLPath"], ConfigurationManager.AppSettings["SpDeliveryNotePreprintedPath"]);
                                List<SpDeliveryNote> dataSource = new List<SpDeliveryNote>();
                                if (inv.invoice_number.Contains("FDA")) //debit
                                {
                                    dataSource = Service<AttachmentService>().GetSpDeliveryNote(inv.invoice_number, user);
                                }
                                else
                                {
                                    dataSource = Service<AttachmentService>().GetSpCreditDeliveryNote(inv.invoice_number, user);
                                }

                                List<SpDeliveryNote> distinctInvoiceNo = dataSource
                                                                            .GroupBy(s => s.InvoiceNo)
                                                                            .Select(g => g.First())
                                                                            .ToList();
                                foreach (SpDeliveryNote d in distinctInvoiceNo)
                                {
                                    filename = Path.GetFileNameWithoutExtension(outputfile) + "\\" + "PI - " + d.InvoiceNo + ".pdf";
                                    var fileInArchive = ziparchive.CreateEntry(filename);

                                    fileBytes = SaveReportPdf(inv.da_no, out warnings, out mimeType, out encoding, out extension, repPath, out streamIds, report, dataSource, d);

                                    using (var zipStream = fileInArchive.Open())
                                    using (var fileToCompressStream = new MemoryStream(fileBytes))
                                    {
                                        fileToCompressStream.CopyTo(zipStream);
                                    }
                                }

                                var Update = Service<InvoiceService>().UpdateSparePartStatusDownload(
                                                 inv.da_no,
                                                 user,
                                                 EnumHelper.GetDescription(DownloadType.PI),
                                                 EnumHelper.GetDescription(InvoiceType.Invoice)
                                                 );
                            }

                        }
                        DownloadFinish();
                        Response.Clear();
                        Response.AddHeader("Access-Control-Allow-Origin", "*");
                        Response.ContentType = "application/octet-stream";
                        Response.AddHeader("content-disposition", "attachment;filename=\"" + outputfile + "\"");
                        Response.BinaryWrite(memoryStream.ToArray());
                        Response.Flush();
                        Response.End();
                    }
                }
            }
            catch (Exception e)
            {
                DownloadFinish();
                throw e;
            }
        }


        #region new function CR 
        public void DownloadAllPPH([DataSourceRequest] DataSourceRequest request)
        {
            var result = Service<InvoiceService>().GetViewDataSourceResultWHTPDF(request, Convert.ToInt16(EnumHelper.GetDescription(InvoiceType.Invoice)), SessionManager.RoleStr);
            //var result = Service<InvoiceService>().GetDataWHTPDF(((Kendo.Mvc.FilterDescriptor)request.Filters[0]).Value.ToString());
            //var result = Service<InvoiceService>().GetDataWHTPDF(id);
            string filenamepdf = string.Empty;
            var username = Service<ParameterService>().GetParameter("file_impersonate_user").value1;
            var domain = Service<ParameterService>().GetParameter("file_impersonate_domain").value1;
            var userdomain = domain + "\\" + username;
            var password = Service<ParameterService>().GetParameter("file_impersonate_password").value1;

            var credential = new NetworkCredential(userdomain, password);
            string fileserver = Service<ParameterService>().GetParameter("file_server").value1;
            string filepath = Service<ParameterService>().GetParameter("file_repo_path_tax").value1;
            string file = "\\\\" + fileserver + "\\" + filepath;
            var outputfile = "WHT_" + DateTime.Now.ToString("yyyyMMdd") + ".zip";
            byte[] fileBytes;
            using (new NetworkConnection(filepath, credential))
            {
                using (var memoryStream = new MemoryStream())
                {
                    using (var ziparchive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
                    {
                        foreach (Invoice item in result.Data)
                        {
                            var da_no = item.invoice_number;
                            var invoicedata = Service<InvoiceService>().GetDataWHTPDF(da_no);
                            //if(invoicedata.Count > 0)
                           // {
                                foreach (Invoice inv in invoicedata)
                                {
                                    var docdownload = inv.PDFWithholding;
                                    var temp = Path.GetPathRoot(docdownload);
                                    filenamepdf = Path.GetFileName(docdownload);
                                    if (filenamepdf != null)
                                    {
                                        //byte[] fileBytes = System.IO.File.ReadAllBytes(linkdata);

                                        // string fileName = filenamepdf;
                                        var fileInArchive = ziparchive.CreateEntry(filenamepdf);
                                        Byte[] bytes = System.IO.File.ReadAllBytes(docdownload);
                                        fileBytes = bytes;
                                        //return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
                                        using (var zipStream = fileInArchive.Open())
                                        using (var fileToCompressStream = new MemoryStream(fileBytes))
                                        {
                                            fileToCompressStream.CopyTo(zipStream);
                                        }
                                    }
                                    else
                                    {
                                        throw new Exception("Withholding Tax Art 22 document is waiting for approval.");
                                    }
                                }
                           // }
                            //else
                            //{
                            //    throw new Exception("Withholding Tax Art 22 Not Exist");
                            //}
                            
                        }
                        ziparchive.Dispose();
                    }
                    DownloadFinish();
                    Response.Clear();
                    Response.AddHeader("Access-Control-Allow-Origin", "*");
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("content-disposition", "attachment;filename=" + outputfile);
                    Response.BinaryWrite(memoryStream.ToArray());
                    Response.Flush();
                    Response.End();
                }
            }
        }
        #endregion

        public void DownloadAttachmentSparePartNotur(string caNumber)
        {
            Stream stream = new MemoryStream();
            Warning[] warnings;
            string mimeType, encoding, extension, repPath = string.Empty;
            string[] streamIds;
            LocalReport report = new LocalReport();

            try
            {
                string user = SessionManager.Current;
                string userid = "";
                if (user != "TAM\\Mayumi" || user != "TAM\\frans" || user != "TAM\\ronnyk102181" || user != "TAM\\indah.gustiana" || user != "TAM\\aliandy.syahriel")
                {
                    user = userid;
                }
                else
                {
                    user = SessionManager.Current;
                }
                string name = SessionManager.Name;
                string nameid = "";
                if (name != "Mayumi Eguchi" || name != "Frans lhutan B" || name != "Ronny Kusgianto" || name != "Indah Sari Gustiana" || name != "Aliandy Syahriel")
                {
                    name = nameid;
                }
                else
                {
                    name = SessionManager.Name;
                }

                if (!string.IsNullOrEmpty(caNumber))// && !string.IsNullOrEmpty(user))
                {

                    repPath = string.Format("{0}/{1}", ConfigurationManager.AppSettings["RDLPath"], ConfigurationManager.AppSettings["SpAttCreditAdvicePath"]);
                    //repPath = string.Format("{0}/{1}", ConfigurationManager.AppSettings["RDLPath"], "Report1.rdlc");

                    //var inv = Service<InvoiceService>().GetInvoiceByCaNumberDaNumber(EnumHelper.GetDescription(InvoiceType.NotaRetur), caNumber, daNumber);
                    //var inv = Service<InvoiceService>().GetInvoiceByInvoiceNumber(EnumHelper.GetDescription(InvoiceType.NotaRetur), caNumber);

                    List<AttachmentSpDebitAdvice> dataSource = Service<AttachmentService>().GetSpCreditAttachment(caNumber, user);

                    report.ReportPath = repPath;
                    report.SetParameters(new ReportParameter("pDaNo", dataSource.First().CaNo));
                    report.SetParameters(new ReportParameter("pDeliveryDate", dataSource.First().InvoiceDate.ToString("dd/MM/yyyy")));
                    report.SetParameters(new ReportParameter("pCustomerName", dataSource.First().CustomerName));
                    report.SetParameters(new ReportParameter("pCustomerAddress", dataSource.First().CustomerAddress));

                    DataTable table = new DataTable();
                    table.Columns.Add("InvoiceNo", type: typeof(string));
                    table.Columns.Add("InvoiceDate", type: typeof(DateTime));
                    table.Columns.Add("OT", type: typeof(string));
                    table.Columns.Add("OrderNo", type: typeof(string));
                    table.Columns.Add("Items", type: typeof(int));
                    table.Columns.Add("GrossAmount", type: typeof(decimal));
                    table.Columns.Add("NetAmount", type: typeof(decimal));

                    foreach (AttachmentSpDebitAdvice a in dataSource)
                    {
                        DataRow row = table.NewRow();

                        row["InvoiceNo"] = a.InvoiceNo;
                        row["InvoiceDate"] = a.InvoiceDate;
                        row["OT"] = a.OT;
                        row["OrderNo"] = a.OrderNo;
                        row["Items"] = a.Items;
                        row["GrossAmount"] = a.GrossAmount;
                        row["NetAmount"] = a.NetAmount;

                        table.Rows.Add(row);
                    }

                    report.DataSources.Clear();
                    report.DataSources.Add(new ReportDataSource("DataSetSpAttachment", table));

                    report.Refresh();

                    byte[] bytes = report.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
                    
                    Response.Buffer = true;

                    DownloadFinish();

                    Response.Clear();
                    Response.ContentType = mimeType;
                    Response.AddHeader("content-disposition", "attachment; filename= " + caNumber.Replace("/", "-") + "." + extension);
                    Response.OutputStream.Write(bytes, 0, bytes.Length); // create the file  
                    Response.Flush(); // send it to the client to download  
                    Response.End();

                }
            }
            catch (Exception e)
            {
                DownloadFinish();
                throw e;
            }
        }
    }
    
}
