﻿

using Kendo.Mvc.UI;
using MigraDoc.DocumentObjectModel;
using MigraDoc.Rendering;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Tam.Ebilling.Domain;
using Tam.Ebilling.Infrastructure.Session;
using Tam.Ebilling.Service;
using Tam.Ebilling.Service.Modules.Core.Helper;

namespace Tam.Ebilling.Web.Areas.Core.Controllers
{
    public class DRKBController : CommonFeatureController
    {
        // GET: Core/Log
        public ActionResult Index()
        {
            
            var customer = Service<CustomerService>().GetCustomer();
            ViewBag.CustomerName = customer;

            var cust = SessionManager.RoleStr;
            ViewBag.Customer = cust;

            ViewBag.UserType = SessionManager.UserType;
            ViewBag.IsAllowedDownload= IsAllowed("download") || ViewBag.UserType == "Customer";
            ViewBag.IsAllowedPrint= IsAllowed("print") || ViewBag.UserType == "Customer";

            ViewBag.IsAllowedDownloadToPDF= IsAllowed("download_pdf") || ViewBag.UserType == "Customer";
            ViewBag.IsAllowedDownloadToExcel = IsAllowed("download_excel") || ViewBag.UserType == "Customer";

            ViewBag.IsAllowedDownloadToPDFReport = IsAllowed("download_pdf_report") || ViewBag.UserType == "Customer";
            ViewBag.IsAllowedDownloadToExcelReport = IsAllowed("download_excel_report") || ViewBag.UserType == "Customer";

            return View();
        }

        [HttpPost]
        public ActionResult Read([DataSourceRequest] Kendo.Mvc.UI.DataSourceRequest request)
        {
            //var result = Service<DRKBService>().GetViewDataSourceResult(request, SessionManager.RoleStr);
            //return Json(result, JsonRequestBehavior.AllowGet);

            var result = Service<DRKBService>().GetDRKBList(request, SessionManager.RoleStr);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public FileResult DownloadExcel([DataSourceRequest] DataSourceRequest request, string checks, string type, string dateFrom, string dateTo, bool isPDF, string totalRows)
        {
            string[] cekcekall = checks.Split(',');
             
            DataSourceResult result;
            if (cekcekall[0] == "on")
            {
                if (int.Parse(totalRows) > 3000)
                {
                    throw new Exception("Export Excel Failed, Total row can not more than 3000");
                }

                var result2 = Service<DRKBService>().GetDRKBList(request, SessionManager.RoleStr);
                List<String> list = new List<String>();
                list = result2.Data.Cast<DRKB>().Select(_DRKB => _DRKB.Id).ToList();

                String[] intList = list.ToArray();
                //result = Service<DRKBService>().getchkDRKB(request, SessionManager.RoleStr, intList);
                result = Service<DRKBService>().GetDRKBch(request, SessionManager.RoleStr, intList, "", true);

            }
            else
            {
                // result = Service<DRKBService>().GetChekDRKBList(request, SessionManager.RoleStr, checks);
                result = Service<DRKBService>().GetDRKBch(request, SessionManager.RoleStr, null, checks,false);
            }

            var CompanyResult = Service<DRKBService>().GetCompanyDataSourceResult(request);
             
            var strclmn = new string[] { };
            var strclmn2 = new string[] { };
            var strclmnLIst = new List<string[]> { };

            if (type == "Original")
            {
                strclmn = new string[] {
                    "No", "Business Unit", "Tax Invoice or Credit Note","Tax Invoice or Credit Note", "Customer","Customer", "Frame Number", "Engine Number", "Merk Name", "Model", "Year",
                    "Vehicle Selling Price","Vehicle Selling Price","Vehicle Selling Price", "Previous Luxury Tax Amount", "Remarks"
                };

                strclmn2 = new string[]
                {
                        "", "Business Unit","Tax Number", "Tax Date", "NPWP", "Name", "Frame Number", "Engine Number","Merk Name", "Model", "Year",
                        "VAT Based Amount", "VAT Amount", "Luxury Tax Amount", "Previous Luxury Tax Amount" , "Remarks"
                }; //for header
            }
            else
            {
                strclmn = new string[] {
                    "Business Unit", "Tax Invoice or Credit Note","Tax Invoice or Credit Note", "Customer","Customer", "Frame Number","Merk Name",  "Model", "Year",
                    "Vehicle Selling Price","Vehicle Selling Price" , "Previous Luxury Tax Amount", "Remarks"
                };

                strclmn2 = new string[]
                {
                        "Business Unit", "Tax Number", "Tax Date", "NPWP", "Name", "Frame Number","Merk Name", "Model", "Year",
                        "VAT Based Amount", "VAT Amount", "Previous Luxury Tax Amount" , "Remarks"
                }; //for header

            }
            strclmnLIst.Add(strclmn);
            strclmnLIst.Add(strclmn2);

            List<DRKB> total = result.Data.Cast<DRKB>().ToList();

            MasterCompany comp = CompanyResult.Data.Cast<MasterCompany>().FirstOrDefault();


            // Initial variable
            // var IsPrint = true;
            int totalData = total.Count;

            // code to create workbook 
            if (totalData < 3000)
            {
                using (var exportData = new MemoryStream())
                {
                    string dateFromDL= "";
                    string dateToDL = "";
                    if (dateFrom == "" || dateFrom == null)
                    {
                        dateFrom = "01-01-1900";

                    }
                    if (dateTo == "" || dateTo == null)
                    {
                        dateTo = "31-12-9999";
                    }
                      
                    var fileNameDate = dateFrom  + " to " + dateTo ;

                    var fileNameDateDL = "";
                    
                    if (dateFrom != "01-01-1900" && dateTo != "31-12-9999")
                    {
                        DateTime dtFrom = new DateTime();
                        dtFrom = DateTime.ParseExact(dateFrom.Replace("/", "-"), "dd-MM-yyyy", null);

                        DateTime dtTo = new DateTime();
                        dtTo = DateTime.ParseExact(dateTo.Replace("/", "-"), "dd-MM-yyyy", null);

                        fileNameDateDL = dtFrom.ToString("MMMM dd, yyyy") + " to " + dtTo.ToString("MMMM dd, yyyy");
                    }

                    string saveAsFileName = string.Format("DRKB_{0} {1}", fileNameDate, type).Replace("/", "-").Replace("-", "");

                    string fileNameExcel = Path.ChangeExtension(saveAsFileName, ".xlsx");
                    string fileNamePdf = Path.ChangeExtension(saveAsFileName, ".pdf");

                    string TempDownloadFolder = ConfigurationManager.AppSettings["TempFileDownload"];

                    //mulai bikin excel  

                    var workbook = new XSSFWorkbook();
                    var sheet = workbook.CreateSheet("Report DRKB");

                    int i = 0;

                    //font header Center
                    var font = workbook.CreateFont();
                    var style = workbook.CreateCellStyle();
                    style.FillPattern = FillPattern.NoFill; //SOLID_FOREGROUND
                    style.Alignment = HorizontalAlignment.Center;
                    font.FontHeightInPoints = 10;
                    font.Color = IndexedColors.Black.Index;
                    font.FontName = "Arial";
                    font.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;

                    //font header Left  Bold
                    var fontReg = workbook.CreateFont();
                    var styleReg = workbook.CreateCellStyle();
                    styleReg.FillPattern = FillPattern.NoFill; //SOLID_FOREGROUND
                    fontReg.FontHeightInPoints = 10;
                    fontReg.Color = IndexedColors.Black.Index;
                    fontReg.FontName = "Arial";
                    fontReg.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;

                    //font header blue
                    var fontHeader = workbook.CreateFont();
                    var styleHeader = workbook.CreateCellStyle();
                    styleHeader.FillBackgroundColor = IndexedColors.BlueGrey.Index;
                    styleHeader.FillPattern = FillPattern.NoFill;
                    fontHeader.FontHeightInPoints = 10;
                    fontHeader.Color = IndexedColors.Black.Index;
                    fontHeader.FontName = "Arial";
                    fontHeader.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;


                    //create header
                    var headerTitle1 = sheet.CreateRow(0);//create header ======================================================
                    var cellFooterTitle1 = headerTitle1.CreateCell(0);
                    cellFooterTitle1.SetCellValue("DETAILED LIST OF MOTOR VEHICLES");
                    cellFooterTitle1.CellStyle = style;
                    cellFooterTitle1.CellStyle.SetFont(font);
 
                    var headerTitle3 = sheet.CreateRow(1);//create header ======================================================
                    var cellFooterTitle3 = headerTitle3.CreateCell(0);
                    cellFooterTitle3.SetCellValue(fileNameDateDL);
                    cellFooterTitle3.CellStyle = style;
                    cellFooterTitle3.CellStyle.SetFont(font);

                    var headerTitle4 = sheet.CreateRow(3);//create header ======================================================
                    var cellFooterTitle4 = headerTitle4.CreateCell(0);
                    cellFooterTitle4.SetCellValue("IMPORTER / BRAND HOLDING AGENT / MANUFACTURING / DISTRIBUTOR / DEALER / SUB DEALER / SHOWROOM");
                    cellFooterTitle4.CellStyle = styleReg;
                    cellFooterTitle4.CellStyle.SetFont(fontReg);

                    var headerTitle5 = sheet.CreateRow(4);//create header ======================================================
                    var cellFooterTitle5 = headerTitle5.CreateCell(0);
                    cellFooterTitle5.SetCellValue("NAME" );
                    cellFooterTitle5.CellStyle = styleReg;
                    cellFooterTitle5.CellStyle.SetFont(fontReg);
                    var cellFooterTitle5a = headerTitle5.CreateCell(2);
                    //cellFooterTitle5a.SetCellValue(": " + comp.Nama_pt);
                    cellFooterTitle5a.SetCellValue(": ");
                    cellFooterTitle5a.CellStyle = styleReg;
                    cellFooterTitle5a.CellStyle.SetFont(fontReg);

                    var headerTitle6 = sheet.CreateRow(5);//create header ======================================================
                    var cellFooterTitle6 = headerTitle6.CreateCell(0);
                    cellFooterTitle6.SetCellValue("ADDRESS");
                    cellFooterTitle6.CellStyle = styleReg;
                    cellFooterTitle6.CellStyle.SetFont(fontReg);
                    var cellFooterTitle6a = headerTitle6.CreateCell(2);
                    //cellFooterTitle6a.SetCellValue(": " + comp.alamat_pt);
                    cellFooterTitle6a.SetCellValue(": " );
                    cellFooterTitle6a.CellStyle = styleReg;
                    cellFooterTitle6a.CellStyle.SetFont(fontReg);

                    var headerTitle7 = sheet.CreateRow(6);//create header ======================================================
                    var cellFooterTitle7 = headerTitle7.CreateCell(0);
                    cellFooterTitle7.SetCellValue("NPWP");
                    cellFooterTitle7.CellStyle = styleReg;
                    cellFooterTitle7.CellStyle.SetFont(fontReg);
                    var cellFooterTitle7a = headerTitle7.CreateCell(2);
                    //cellFooterTitle7a.SetCellValue(": " + comp.Npwp_pt);
                    cellFooterTitle7a.SetCellValue(": " );
                    cellFooterTitle7a.CellStyle = styleReg;
                    cellFooterTitle7a.CellStyle.SetFont(fontReg);

                    var headerTitle8 = sheet.CreateRow(7);//create header ======================================================
                    var cellFooterTitle8 = headerTitle8.CreateCell(0);
                    cellFooterTitle8.SetCellValue("NO. PENGUKUHAN PKP");
                    cellFooterTitle8.CellStyle = styleReg;
                    cellFooterTitle8.CellStyle.SetFont(fontReg);
                    var cellFooterTitle8a = headerTitle8.CreateCell(2);
                    //cellFooterTitle8a.SetCellValue(": " + comp.no_pengukuhan_pt);
                    cellFooterTitle8a.SetCellValue(": ");
                    cellFooterTitle8a.CellStyle = styleReg;
                    cellFooterTitle8a.CellStyle.SetFont(fontReg);

                    var beginI = 9;

                    for (int ii = 0; ii < 2; ii++)
                    {
 
                        string[] columnheader1 = strclmnLIst[ii];

                        var header1 = sheet.CreateRow(beginI);//create header ======================================================
                        for (int j = 0; j < columnheader1.Count(); j++)
                        {
                            var cell = header1.CreateCell(j);
                            cell.SetCellValue(columnheader1[j]);
                            cell.CellStyle = styleHeader;
                            cell.CellStyle.SetFont(fontHeader);
                        } 

                        beginI++;
                    }


                    //sheet.SetAutoFilter(NPOI.SS.Util.CellRangeAddress.ValueOf("$A$13:$O$13"));


                    if (type == "Original")
                    {

                        mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$A$1:$P$1"));
                        mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$A$2:$P$2"));
                        mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$A$10:$A$11"));
                        mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$B$10:$B$11"));
                        mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$C$10:$D$10"));
                        mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$E$10:$F$10"));
                        mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$G$10:$G$11"));
                        mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$H$10:$H$11"));
                        mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$I$10:$I$11"));
                        mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$J$10:$J$11"));
                        mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$K$10:$K$11"));
                        mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$L$10:$N$10"));
                        mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$O$10:$O$11"));
                        mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$P$10:$P$11"));

                        //sheet.SetAutoFilter(NPOI.SS.Util.CellRangeAddress.ValueOf("$A$12:$O$12"));

                        SetBackgoundHeaderColor(sheet, beginI-1, 16);
                        SetBackgoundHeaderColor(sheet, beginI-2, 16);

                        XSSFCellStyle cellStyle = (XSSFCellStyle)sheet.Workbook.CreateCellStyle();

                        cellStyle.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
                        cellStyle.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
                        cellStyle.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
                        cellStyle.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
                        cellStyle.Alignment = HorizontalAlignment.Left;

                        XSSFCellStyle cellStyleCenter = (XSSFCellStyle)sheet.Workbook.CreateCellStyle();
                        cellStyleCenter.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
                        cellStyleCenter.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
                        cellStyleCenter.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
                        cellStyleCenter.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
                        cellStyleCenter.Alignment = HorizontalAlignment.Center;

                        XSSFCellStyle cellStyleNumeric = (XSSFCellStyle)sheet.Workbook.CreateCellStyle();
                        cellStyleNumeric.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
                        cellStyleNumeric.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
                        cellStyleNumeric.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
                        cellStyleNumeric.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
                        IDataFormat dataFormatCustom = sheet.Workbook.CreateDataFormat();
                        cellStyleNumeric.Alignment = HorizontalAlignment.Right;
                        cellStyleNumeric.DataFormat = dataFormatCustom.GetFormat("#,#0");

                        i = beginI;
                        int ii = 1;
                        foreach (DRKB DRKB in result.Data)
                        {
                           

                            var content = sheet.CreateRow(i);
                            content.CreateCell(0).SetCellValue((ii));
                            content.CreateCell(1).SetCellValue(DRKB.BusinessUnit);
                            content.CreateCell(2).SetCellValue(DRKB.TaxNumber);
                            content.CreateCell(3).SetCellValue(DRKB.TanggalFaktur.ToString("dd/MM/yyyy"));
                            content.CreateCell(4).SetCellValue(DRKB.NPWPCustomer);
                            content.CreateCell(5).SetCellValue(DRKB.NamaCustomer);
                            content.CreateCell(6).SetCellValue(DRKB.FrameNumber);
                            content.CreateCell(7).SetCellValue(DRKB.EngineNumber);
                            content.CreateCell(8).SetCellValue(DRKB.MerkOrType);
                            content.CreateCell(9).SetCellValue(DRKB.ModelType);
                            

                            content.CreateCell(10).SetCellValue(DRKB.Year.ToString()); 
                            content.CreateCell(11).SetCellValue(DRKB.JumlahDPP.HasValue ? (double)DRKB.JumlahDPP : 0);

                            content.CreateCell(12).SetCellValue(DRKB.JumlahPPN.HasValue ? (double)DRKB.JumlahPPN : 0);

                            content.CreateCell(13).SetCellValue(DRKB.LuxTaxAmount.HasValue ? (double)DRKB.LuxTaxAmount : 0);

                            content.CreateCell(14).SetCellValue(DRKB.PrevLuxTaxAmount.HasValue ? (double)DRKB.PrevLuxTaxAmount : 0 );

                            content.CreateCell(15).SetCellValue(DRKB.Remarks);

                            SetBorder(sheet, i, 16, false, cellStyle, cellStyleNumeric, cellStyleCenter);

                            ii++;
                            i++;
                            beginI++;
                        }


                        for (int k = 0; k < 16; k++)// set autozise column==================================
                        {
                            try
                            {
                                sheet.AutoSizeColumn(k, true);
                            }
                            catch { }
                        }

                        sheet.SetColumnWidth(0, 2000);
                        sheet.SetColumnWidth(1, 6000);
                        sheet.SetColumnWidth(2, 7500);

                        beginI = beginI + 2;
                        var footerData = sheet.CreateRow(beginI);
                        footerData.CreateCell(12).SetCellValue(comp.Lokasi + ", _______________________");
                        var fontfooterData = sheet.Workbook.CreateFont();
                        ICell cell = sheet.GetRow(beginI).GetCell(12);
                        fontfooterData.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
                        ICellStyle cellStylefooterData = sheet.Workbook.CreateCellStyle();
                        cellStylefooterData.SetFont(fontfooterData);
                        cell.CellStyle = cellStylefooterData;

                        beginI = beginI + 4;
                        var footerDataNama = sheet.CreateRow(beginI);
                        //footerDataNama.CreateCell(12).SetCellValue(comp.Nama_ttd);
                        footerDataNama.CreateCell(12).SetCellValue("");
                        mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$M$" + (beginI + 1) + ":$N$" + (beginI + 1) + ""));

                        beginI = beginI + 1;
                        var footerDataTTD = sheet.CreateRow(beginI);
                        //footerDataTTD.CreateCell(12).SetCellValue(comp.Jabatan_ttd);
                        footerDataTTD.CreateCell(12).SetCellValue("");
                        mergeAndCenterAndAddBorderTop(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$M$" + (beginI + 1) + ":$N$" + (beginI + 1) + ""));


                         

                    }
                    else
                    {

                        mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$A$1:$M$1"));
                        mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$A$2:$M$2"));
                        mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$A$10:$A$11"));
                        mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$B$10:$C$10"));
                        mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$D$10:$E$10"));
                        mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$F$10:$F$11"));
                        mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$G$10:$G$11"));
                        mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$H$10:$H$11"));
                        mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$I$10:$I$11"));
                        mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$J$10:$K$10"));
                        mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$L$10:$L$11"));
                        mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$M$10:$M$11"));

                        //sheet.SetAutoFilter(NPOI.SS.Util.CellRangeAddress.ValueOf("$A$12:$L$12"));

                        SetBackgoundHeaderColor(sheet, beginI - 1, 13);
                        SetBackgoundHeaderColor(sheet, beginI - 2, 13);

                        XSSFCellStyle cellStyle = (XSSFCellStyle)sheet.Workbook.CreateCellStyle();

                        cellStyle.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
                        cellStyle.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
                        cellStyle.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
                        cellStyle.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
                        cellStyle.Alignment = HorizontalAlignment.Left;


                        XSSFCellStyle cellStyleCenter = (XSSFCellStyle)sheet.Workbook.CreateCellStyle();
                        cellStyleCenter.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
                        cellStyleCenter.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
                        cellStyleCenter.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
                        cellStyleCenter.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
                        cellStyleCenter.Alignment = HorizontalAlignment.Center;

                        XSSFCellStyle cellStyleNumeric = (XSSFCellStyle)sheet.Workbook.CreateCellStyle();
                        cellStyleNumeric.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
                        cellStyleNumeric.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
                        cellStyleNumeric.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
                        cellStyleNumeric.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
                        IDataFormat dataFormatCustom = sheet.Workbook.CreateDataFormat();
                        cellStyleNumeric.DataFormat = dataFormatCustom.GetFormat("#,#0");
                        cellStyleNumeric.Alignment = HorizontalAlignment.Right;


                        i = beginI;
                        foreach (DRKB DRKB in result.Data)
                        {
                            

                            var content = sheet.CreateRow(i);
                            content.CreateCell(0).SetCellValue(DRKB.BusinessUnit);
                            content.CreateCell(1).SetCellValue(DRKB.TaxNumber);
                            content.CreateCell(2).SetCellValue(DRKB.TanggalFaktur.ToString("dd/MM/yyyy"));
                            content.CreateCell(3).SetCellValue(DRKB.NPWPCustomer);
                            content.CreateCell(4).SetCellValue(DRKB.NamaCustomer);
                            content.CreateCell(5).SetCellValue(DRKB.FrameNumber);
                            content.CreateCell(6).SetCellValue(DRKB.MerkOrType);
                            content.CreateCell(7).SetCellValue(DRKB.ModelType);
                           

                            content.CreateCell(8).SetCellValue(DRKB.Year.ToString());

                            content.CreateCell(9).SetCellValue(DRKB.JumlahDPP.HasValue ? (double)DRKB.JumlahDPP : 0);

                            content.CreateCell(10).SetCellValue(DRKB.JumlahPPN.HasValue ? (double)DRKB.JumlahPPN : 0);

                            content.CreateCell(11).SetCellValue(DRKB.PrevLuxTaxAmount.HasValue ? (double)DRKB.PrevLuxTaxAmount : 0);

                            content.CreateCell(12).SetCellValue(DRKB.Remarks);

                            SetBorder(sheet, i, 13, true, cellStyle, cellStyleNumeric, cellStyleCenter);

                            beginI++;
                            i++;
                        }

                        for (int k = 0; k < 13; k++)// set autozise column==================================
                        {
                            try
                            {
                                sheet.AutoSizeColumn(k, true);
                            }
                            catch { }

                        }
                        sheet.SetColumnWidth(0, 4000);
                        sheet.SetColumnWidth(1, 6000);
                        sheet.SetColumnWidth(2, 3000);


                        beginI = beginI + 2;
                        var footerData = sheet.CreateRow(beginI);
                        footerData.CreateCell(9).SetCellValue(comp.Lokasi + ", _______________________");
                        ICell cell = sheet.GetRow(beginI).GetCell(9);
                        var fontfooterData = sheet.Workbook.CreateFont();
                        fontfooterData.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
                        ICellStyle cellStylefooterData = sheet.Workbook.CreateCellStyle();
                        cellStylefooterData.SetFont(fontfooterData);
                        cell.CellStyle = cellStylefooterData;


                        beginI = beginI + 4;
                        var footerDataNama = sheet.CreateRow(beginI);
                        //footerDataNama.CreateCell(9).SetCellValue(comp.Nama_ttd);
                        footerDataNama.CreateCell(9).SetCellValue("");
                        mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$J$" + (beginI + 1) + ":$K$" + (beginI + 1) + ""));

                        beginI = beginI + 1;
                        var footerDataTTD = sheet.CreateRow(beginI);
                        //footerDataTTD.CreateCell(9).SetCellValue(comp.Jabatan_ttd);
                        footerDataTTD.CreateCell(9).SetCellValue("");
                        mergeAndCenterAndAddBorderTop(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$J$" + (beginI + 1) + ":$K$" + (beginI + 1) + ""));


                      

                    }




                    if (isPDF)
                    {
                       
                        string fullPath = Path.Combine(@TempDownloadFolder, fileNameExcel);
                        string fullPathPdf = Path.Combine(@TempDownloadFolder, fileNamePdf);

                        //workbook.Write(exportData);

                        //using (FileStream stream = new FileStream(fullPath, FileMode.Create, FileAccess.Write))
                        //{
                        //    workbook.Write(stream);
                        //}

                        //Workbook workbookpdf = new Workbook();
                        //workbookpdf.LoadFromFile(fullPath);
                        //workbookpdf.ConverterSetting.SheetFitToPage = true;

                        //workbookpdf.SaveToFile(fullPathPdf, FileFormat.PDF);

                        //byte[] fileBytes = System.IO.File.ReadAllBytes(fullPathPdf);

                        //return File(fileBytes, "application/pdf", fileNamePdf);


                        var document = CreatePDF(comp, strclmnLIst, result, type, fileNameDateDL);

                        var ddl = MigraDoc.DocumentObjectModel.IO.DdlWriter.WriteToString(document);
                        //MigraDoc.DocumentObjectModel.IO.DdlWriter.WriteToFile(document, "MigraDoc.mdddl");

                        var renderer = new PdfDocumentRenderer(true);
                        renderer.Document = document;

                        renderer.RenderDocument();  

                        renderer.PdfDocument.Save(fullPathPdf);

                        byte[] fileBytes = System.IO.File.ReadAllBytes(fullPathPdf);

                        DownloadFinish();

                        //ViewBag.JavaScriptFunction= "$('#DRKBGrid .k-grid-content .k-loading-mask').remove();";
                        return File(fileBytes, "application/pdf", fileNamePdf); 
                    }
                    else
                    {
                        DownloadFinish();

                        workbook.Write(exportData);
                        
                        byte[] bytes = exportData.ToArray();
                        //ViewBag.JavaScriptFunction="$('#DRKBGrid .k-grid-content .k-loading-mask').remove();";
                        return File(bytes, "application/vnd.ms-excel", fileNameExcel);
                         
                    } 
                }
            } 
            else
            {
                DownloadFinish();

                //ViewBag.JavaScriptFunction ="$('#DRKBGrid .k-grid-content .k-loading-mask').remove();";
                throw new Exception("Export Excel Failed, Total row can not more than 3000");

            }
        }

        public static Document CreatePDF(MasterCompany comp, List<string[]> strclmnLIst, DataSourceResult result, string type, string fileNameDateDL)
        {
            // Create a new MigraDoc document.
            var document = new Document();

            //document.Info.Title = "Hello, MigraDoc";
            //document.Info.Subject = "Demonstrates an excerpt of the capabilities of MigraDoc.";
            //document.Info.Author = "Stefan Lange";

            //set document orientation
            //document.DefaultPageSetup.Orientation = Orientation.Landscape;

            DefineStyles(document);

            DefineContentSection(document);

            CreateDocument(document, comp, strclmnLIst, result, type, fileNameDateDL);

            return document;
        }

        static void DefineContentSection(Document document)
        {
            var section = document.AddSection();
            section.PageSetup.OddAndEvenPagesHeaderFooter = false;
            section.PageSetup.StartingNumber = 1;
            section.PageSetup.PageFormat = PageFormat.A3; 

            //var header = section.Headers.Primary;
            //header.AddParagraph("\tOdd Page Header");

            //header = section.Headers.EvenPage;
            //header.AddParagraph("Even Page Header");

            // Create a paragraph with centered page number. See definition of style "Footer".
            var paragraph = new MigraDoc.DocumentObjectModel.Paragraph();
            paragraph.AddTab();
            paragraph.AddPageField();

            // Add paragraph to footer for odd pages.
            section.Footers.Primary.Add(paragraph);
            section.Footers.Primary.Format.Alignment = ParagraphAlignment.Center;
            // Add clone of paragraph to footer for odd pages. Cloning is necessary because an object must
            // not belong to more than one other object. If you forget cloning an exception is thrown.
            section.Footers.EvenPage.Add(paragraph.Clone());
        }

        public static void DefineStyles(Document document)
        {
            // Get the predefined style Normal.
            var style = document.Styles["Normal"];
            // Because all styles are derived from Normal, the next line changes the 
            // font of the whole document. Or, more exactly, it changes the font of
            // all styles and paragraphs that do not redefine the font.
            style.Font.Name = "Segoe UI";

            // Heading1 to Heading9 are predefined styles with an outline level. An outline level
            // other than OutlineLevel.BodyText automatically creates the outline (or bookmarks) 
            // in PDF.

            //style = document.Styles["Heading1"];
            //style.Font.Name = "Segoe UI Light";
            //style.Font.Size = 16;
            ////style.Font.Bold = true;
            //style.Font.Color = Colors.DarkBlue;
            //style.ParagraphFormat.PageBreakBefore = true;
            //style.ParagraphFormat.SpaceAfter = 6;
            //// Set KeepWithNext for all headings to prevent headings from appearing all alone
            //// at the bottom of a page. The other headings inherit this from Heading1.
            //style.ParagraphFormat.KeepWithNext = true;

            //style = document.Styles["Heading2"];
            //style.Font.Size = 14;
            ////style.Font.Bold = true;
            //style.ParagraphFormat.PageBreakBefore = false;
            //style.ParagraphFormat.SpaceBefore = 6;
            //style.ParagraphFormat.SpaceAfter = 6;

            //style = document.Styles["Heading3"];
            //style.Font.Size = 12;
            ////style.Font.Bold = true;
            //style.Font.Italic = true;
            //style.ParagraphFormat.SpaceBefore = 6;
            //style.ParagraphFormat.SpaceAfter = 3;

            //style = document.Styles[StyleNames.Header];
            //style.ParagraphFormat.AddTabStop("16cm", TabAlignment.Right);

            //style = document.Styles[StyleNames.Footer];
            //style.ParagraphFormat.AddTabStop("8cm", TabAlignment.Center);

            //// Create a new style called TextBox based on style Normal.
            //style = document.Styles.AddStyle("TextBox", "Normal");
            //style.ParagraphFormat.Alignment = ParagraphAlignment.Justify;
            //style.ParagraphFormat.Borders.Width = 2.5;
            //style.ParagraphFormat.Borders.Distance = "3pt";
            ////TODO: Colors
            //style.ParagraphFormat.Shading.Color = Colors.SkyBlue;

            //// Create a new style called TOC based on style Normal.
            //style = document.Styles.AddStyle("TOC", "Normal");
            //style.ParagraphFormat.AddTabStop("16cm", TabAlignment.Right, TabLeader.Dots);
            //style.ParagraphFormat.Font.Color = Colors.Blue;
        }

        public static void CreateDocument(Document document, MasterCompany comp, List<string[]> strclmnLIst, DataSourceResult result, string type, string fileNameDateDL)
        {
            //document.LastSection.AddParagraph("Cell Merge", "Heading2");
            document.LastSection.PageSetup.Orientation = Orientation.Landscape;
            document.LastSection.PageSetup.LeftMargin = "0.8cm";
            document.LastSection.PageSetup.RightMargin = "0.1cm";
            string[] columnheader1 = strclmnLIst[0];
            string[] columnheader2 = strclmnLIst[1];

            var table = document.LastSection.AddTable();
            table.Borders.Visible = false;
            table.TopPadding = 1;
            table.BottomPadding = 1;
            table.Format.Font.Size = 8;
            //table.Rows.Height = 10;

            MigraDoc.DocumentObjectModel.Tables.Column column;

            int headerLength = 3;

            if (type == "Original")
            {
                headerLength = 3;
                for (int j = 0; j < columnheader1.Count(); j++)
                {
                    if ( j == 9 || j == 10)
                    {
                        column = table.AddColumn(Unit.FromCentimeter(1.7));
                    }
                    else if (j == 0)
                    {
                        column = table.AddColumn(Unit.FromCentimeter(1));
                    }
                    else  if (j == 14 )
                    {
                        column = table.AddColumn(Unit.FromCentimeter(3));
                    }
                    else if (j == 5 )
                    {
                        column = table.AddColumn(Unit.FromCentimeter(3.5));
                    }
                    else if ( j == 6)
                    {
                        column = table.AddColumn(Unit.FromCentimeter(3.5));
                    }
                    else if (j == 15 || j == 8)
                    {
                        column = table.AddColumn(Unit.FromCentimeter(3.5));
                    }
                    else if (j ==3)
                    {
                        column = table.AddColumn(Unit.FromCentimeter(2));
                    }
                    else
                    {
                        column = table.AddColumn();
                    }
                }
            }
            else
            {
                headerLength = 2;
                for (int j = 0; j < columnheader1.Count(); j++)
                {
                    if (j == 0 || j == 1 || j == 3  || j == 9)
                    {
                        column = table.AddColumn(Unit.FromCentimeter(3));
                    }
                    else if (j == 11 || j== 4)
                    {
                        column = table.AddColumn(Unit.FromCentimeter(3));
                    }
                    else if (j == 6)
                    {
                        column = table.AddColumn(Unit.FromCentimeter(6));
                    }
                    else if (j == 5 || j == 7)
                    {
                        column = table.AddColumn(Unit.FromCentimeter(3.5));
                    }
                    else if (j == 12)
                    {
                        column = table.AddColumn(Unit.FromCentimeter(3));
                    }
                    else if (j == 2 || j == 8)
                    {
                        column = table.AddColumn(Unit.FromCentimeter(2));
                    }
                    else
                    {
                        column = table.AddColumn();
                    }
                }
            }

            #region Header      
            var row = table.AddRow();
            row.Format.Font.Size = 12;
            row.BottomPadding = 20;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Cells[0].MergeRight = type == "Original" ? 15 : 12;
            row.Cells[0].VerticalAlignment = MigraDoc.DocumentObjectModel.Tables.VerticalAlignment.Center;
            row.Cells[0].AddParagraph("DETAILED LIST OF MOTOR VEHICLES").Format.Font.Bold = true;

            row = table.AddRow();
            row.Format.Font.Size = 12;
            row.BottomPadding = 20;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Cells[0].MergeRight = type == "Original" ? 15 : 12;
            row.Cells[0].VerticalAlignment = MigraDoc.DocumentObjectModel.Tables.VerticalAlignment.Center;
            row.Cells[0].AddParagraph(fileNameDateDL).Format.Font.Bold = true;
             
            row = table.AddRow();
            row.Format.Font.Size = 10;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.Cells[0].MergeRight = type == "Original" ? 15 : 12;
            row.Cells[0].AddParagraph("IMPORTER / BRAND HOLDING AGENT / MANUFACTURING / DISTRIBUTOR / DEALER / SUB DEALER / SHOWROOM");

            row = table.AddRow();
            row.Format.Font.Size = 10;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.Cells[0].MergeRight = headerLength - 1;
            row.Cells[0].AddParagraph("NAME");
            row.Cells[headerLength].MergeRight = 9;
            //row.Cells[headerLength].AddParagraph(": " + comp.Nama_pt);
            row.Cells[headerLength].AddParagraph(": " );

            row = table.AddRow();
            row.Format.Font.Size = 10;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.Cells[0].MergeRight = headerLength -1 ;
            row.Cells[0].AddParagraph("ADDRESS");
            row.Cells[headerLength].MergeRight = 9;
            //row.Cells[headerLength].AddParagraph(": " + comp.alamat_pt);
            row.Cells[headerLength].AddParagraph(": " );

            row = table.AddRow();
            row.Format.Font.Size = 10;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.Cells[0].MergeRight = headerLength - 1;
            row.Cells[0].AddParagraph("NPWP");
            row.Cells[headerLength].MergeRight = 9;
            //row.Cells[headerLength].AddParagraph(": " + comp.Npwp_pt);
            row.Cells[headerLength].AddParagraph(": " );

            row = table.AddRow();
            row.Format.Font.Size = 10;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.Cells[0].MergeRight = headerLength - 1;
            row.Cells[0].AddParagraph("NO. PENGUKUHAN PKP");
            row.Cells[headerLength].MergeRight = 9;
            //row.Cells[headerLength].AddParagraph(": " + comp.no_pengukuhan_pt);
            row.Cells[headerLength].AddParagraph(": " );
            #endregion

            row = table.AddRow();
            row.Borders.Bottom.Visible = true;

            if (type == "Original")
            {
                row = table.AddRow();
                row.Format.Font.Size = 8;
                row.Borders.Visible = true;
                row.Shading.Color = Color.FromArgb(255, 28, 73, 125);
                row.VerticalAlignment = MigraDoc.DocumentObjectModel.Tables.VerticalAlignment.Center;
                row.Format.Alignment = ParagraphAlignment.Center;
                row.Format.Font.Color = Colors.White;

                for (int j = 0; j < columnheader1.Count(); j++)
                {
                    if (j == 2 || j == 4)
                    {
                        row.Cells[j].MergeRight = 1;
                    }
                    else if (j == 11)
                    {
                        row.Cells[j].MergeRight = 2;
                    }
                    else
                    {
                        row.Cells[j].MergeDown = 1;
                    }

                    row.Cells[j].AddParagraph(columnheader1[j]);
                }

                row = table.AddRow();
                row.Format.Font.Size = 8;

                row.Borders.Visible = true;
                row.VerticalAlignment = MigraDoc.DocumentObjectModel.Tables.VerticalAlignment.Center;
                row.Format.Alignment = ParagraphAlignment.Center;
                row.Format.Font.Color = Colors.White;
                row.Shading.Color = Color.FromArgb(255, 28, 73, 125);

                for (int i = 0; i < columnheader2.Count(); i++)
                {
                    row.Cells[i].AddParagraph(columnheader2[i]);
                }

                //row = table.AddRow();
                //row.Borders.Visible = true;
                //row.Format.Alignment = ParagraphAlignment.Center;
                //row.Shading.Color = Color.FromArgb(255, 253, 233, 217);
                //for (int i = 0; i < columnheader2.Count(); i++)
                //{
                //    row.Cells[i].AddParagraph("(" + (i + 1).ToString() + ")");
                //}

                int number = 0;
                foreach (DRKB DRKB in result.Data)
                {
                    row = table.AddRow();
                    row.Format.Font.Size = 8;
                    row.Borders.Visible = true;
                    row.Cells[0].AddParagraph((number + 1).ToString()).Format.Alignment = ParagraphAlignment.Center;
                    row.Cells[1].AddParagraph(DRKB.BusinessUnit != null ? DRKB.BusinessUnit : "");
                    row.Cells[2].AddParagraph(DRKB.TaxNumber != null ? DRKB.TaxNumber : "");
                    row.Cells[3].AddParagraph(DRKB.TanggalFaktur.ToString("dd/MM/yyyy")).Format.Alignment = ParagraphAlignment.Center;
                    row.Cells[4].AddParagraph(DRKB.NPWPCustomer != null ? DRKB.NPWPCustomer : "").Format.Alignment = ParagraphAlignment.Center;
                    row.Cells[5].AddParagraph(DRKB.NamaCustomer != null ? DRKB.NamaCustomer : "");
                    row.Cells[6].AddParagraph(DRKB.FrameNumber != null ? DRKB.FrameNumber : "");
                    row.Cells[7].AddParagraph(DRKB.EngineNumber != null ? DRKB.EngineNumber : "");
                    row.Cells[8].AddParagraph(DRKB.MerkOrType != null ? DRKB.MerkOrType : "");
                    row.Cells[9].AddParagraph(DRKB.ModelType != null ? DRKB.ModelType : "");
                    row.Cells[10].AddParagraph(DRKB.Year.ToString());
                    string jdpp = DRKB.JumlahDPP.HasValue ? DRKB.JumlahDPP.Value.ToString() : "0";
                    string jppn = DRKB.JumlahPPN.HasValue ? DRKB.JumlahPPN.Value.ToString() : "0";
                    string lta = DRKB.LuxTaxAmount.HasValue ? DRKB.LuxTaxAmount.Value.ToString() : "0";
                    string plta = DRKB.PrevLuxTaxAmount.HasValue ? DRKB.PrevLuxTaxAmount.Value.ToString() : "0";

                    //ToString("#,##0.00", new CultureInfo("en-US"));
                    row.Cells[11].AddParagraph(decimal.Parse(jdpp).ToString("#,#0", new CultureInfo("en-US"))).Format.Alignment = ParagraphAlignment.Right;
                    row.Cells[12].AddParagraph(decimal.Parse(jppn).ToString("#,#0", new CultureInfo("en-US"))).Format.Alignment = ParagraphAlignment.Right;
                    row.Cells[13].AddParagraph(decimal.Parse(lta).ToString("#,#0", new CultureInfo("en-US"))).Format.Alignment = ParagraphAlignment.Right;
                    row.Cells[14].AddParagraph(decimal.Parse(plta).ToString("#,#0", new CultureInfo("en-US"))).Format.Alignment = ParagraphAlignment.Right;
                    row.Cells[15].AddParagraph(DRKB.Remarks);
                    number++;
                }

                #region Footer

                row = table.AddRow();
                row.Format.Font.Size = 10;
                row.Height = 40;
                row.Borders.Bottom.Color = Colors.Transparent;
                row.Borders.Right.Color = Colors.Transparent;
                row.Borders.Left.Color = Colors.Transparent;
                row.Cells[0].MergeRight = 10;
                row.Cells[11].VerticalAlignment = MigraDoc.DocumentObjectModel.Tables.VerticalAlignment.Bottom;
                row.Cells[11].MergeRight = 1;
                row.Cells[11].AddParagraph(comp.Lokasi + ", __________________").Format.Alignment = ParagraphAlignment.Center;

                //row = table.AddRow();
                //row.Format.Font.Size = 10;
                //row.Height = 60;
                //row.Borders.Color = Colors.Transparent;
                //row.Cells[0].MergeRight = 10;
                //row.Cells[11].MergeRight = 1;
                //row.Cells[11].VerticalAlignment = MigraDoc.DocumentObjectModel.Tables.VerticalAlignment.Bottom;
                //row.Cells[11].AddParagraph(comp.Nama_ttd).Format.Alignment = ParagraphAlignment.Center;
                //row.Cells[11].AddParagraph("________________________").Format.Alignment = ParagraphAlignment.Center;
                //row.Cells[11].AddParagraph(comp.Jabatan_ttd).Format.Alignment = ParagraphAlignment.Center;


                string NamaTTD = ""; //comp.Nama_ttd;
                string JbtTTD = ""; //comp.Jabatan_ttd;
                row = table.AddRow();
                row.Format.Font.Size = 10;
                row.Height = 60;
                row.Borders.Color = Colors.Transparent;
                //row.Cells[0].MergeRight = 8;
                row.Cells[11].MergeRight = 1;
                row.Cells[11].Borders.Bottom.Color = Colors.Black;

                row.Cells[11].VerticalAlignment = MigraDoc.DocumentObjectModel.Tables.VerticalAlignment.Bottom;
                row.Cells[11].AddParagraph(NamaTTD).Format.Alignment = ParagraphAlignment.Center;

                row = table.AddRow();
                row.Format.Font.Size = 10;
                row.Height = 60;
                row.Borders.Color = Colors.Transparent;
                row.Cells[11].MergeRight = 1;
                row.Cells[11].AddParagraph(JbtTTD).Format.Alignment = ParagraphAlignment.Center;

                #endregion
            }
            else
            {
                row = table.AddRow();
                row.Format.Font.Size = 8;
                row.Borders.Visible = true;
                row.Shading.Color = Color.FromArgb(255, 28, 73, 125);
                row.VerticalAlignment = MigraDoc.DocumentObjectModel.Tables.VerticalAlignment.Center;
                row.Format.Alignment = ParagraphAlignment.Center;
                row.Format.Font.Color = Colors.White;

                for (int j = 0; j < columnheader1.Count(); j++)
                {
                    if (j == 1 || j == 3 || j == 9)
                    {
                        row.Cells[j].MergeRight = 1;
                    }
                    else
                    {
                        row.Cells[j].MergeDown = 1;
                    }

                    row.Cells[j].AddParagraph(columnheader1[j]);
                }

                row = table.AddRow();
                row.Format.Font.Size = 8;

                row.Borders.Visible = true;
                row.VerticalAlignment = MigraDoc.DocumentObjectModel.Tables.VerticalAlignment.Center;
                row.Format.Alignment = ParagraphAlignment.Center;
                row.Format.Font.Color = Colors.White;
                row.Shading.Color = Color.FromArgb(255, 28, 73, 125);

                for (int i = 0; i < columnheader2.Count(); i++)
                {
                    row.Cells[i].AddParagraph(columnheader2[i]);
                }

                //row = table.AddRow();
                //row.Borders.Visible = true;
                //row.Format.Alignment = ParagraphAlignment.Center;
                //row.Shading.Color = Color.FromArgb(255, 253, 233, 217);
                //for (int i = 0; i < columnheader2.Count(); i++)
                //{
                //    row.Cells[i].AddParagraph("(" + (i + 1).ToString() + ")");
                //}

                foreach (DRKB DRKB in result.Data)
                {
                    row = table.AddRow();
                    row.Format.Font.Size = 8;
                    row.Borders.Visible = true;
                    row.Cells[0].AddParagraph(DRKB.BusinessUnit != null ? DRKB.BusinessUnit : "");
                    row.Cells[1].AddParagraph(DRKB.TaxNumber != null ? DRKB.TaxNumber : "");
                    row.Cells[2].AddParagraph(DRKB.TanggalFaktur.ToString("dd/MM/yyyy")).Format.Alignment = ParagraphAlignment.Center;
                    row.Cells[3].AddParagraph(DRKB.NPWPCustomer != null ? DRKB.NPWPCustomer : "").Format.Alignment = ParagraphAlignment.Center;
                    row.Cells[4].AddParagraph(DRKB.NamaCustomer != null ? DRKB.NamaCustomer : "");
                    row.Cells[5].AddParagraph(DRKB.FrameNumber != null ? DRKB.FrameNumber : "");
                    row.Cells[6].AddParagraph(DRKB.MerkOrType != null ? DRKB.MerkOrType : "");
                    row.Cells[7].AddParagraph(DRKB.ModelType != null ? DRKB.ModelType : "");
                    row.Cells[8].AddParagraph(DRKB.Year.ToString());
                    string jdpp = DRKB.JumlahDPP.HasValue ? DRKB.JumlahDPP.Value.ToString() : "0";
                    string jppn = DRKB.JumlahPPN.HasValue ? DRKB.JumlahPPN.Value.ToString() : "0";
                    string plta = DRKB.PrevLuxTaxAmount.HasValue ? DRKB.PrevLuxTaxAmount.Value.ToString() : "0";

                    //ToString("#,##0.00", new CultureInfo("en-US"));
                    row.Cells[9].AddParagraph(decimal.Parse(jdpp).ToString("#,#0", new CultureInfo("en-US"))).Format.Alignment = ParagraphAlignment.Right;
                    row.Cells[10].AddParagraph(decimal.Parse(jppn).ToString("#,#0", new CultureInfo("en-US"))).Format.Alignment = ParagraphAlignment.Right;
                    row.Cells[11].AddParagraph(decimal.Parse(plta).ToString("#,#0", new CultureInfo("en-US"))).Format.Alignment = ParagraphAlignment.Right;
                    row.Cells[12].AddParagraph(DRKB.Remarks);
                }

                #region Footer

                row = table.AddRow();
                row.Format.Font.Size = 10;
                row.Height = 40;
                row.Borders.Bottom.Color = Colors.Transparent;
                row.Borders.Right.Color = Colors.Transparent;
                row.Borders.Left.Color = Colors.Transparent;
                row.Cells[0].MergeRight = 8;
                row.Cells[9].VerticalAlignment = MigraDoc.DocumentObjectModel.Tables.VerticalAlignment.Bottom;
                row.Cells[9].MergeRight = 1;
                row.Cells[9].AddParagraph(comp.Lokasi + ", __________________").Format.Alignment = ParagraphAlignment.Center;

                //row = table.AddRow();
                //row.Format.Font.Size = 10;
                //row.Height = 60;
                //row.Borders.Color = Colors.Transparent;
                //row.Cells[0].MergeRight = 8;
                //row.Cells[9].MergeRight = 1;
                //row.Cells[9].VerticalAlignment = MigraDoc.DocumentObjectModel.Tables.VerticalAlignment.Bottom;
                //row.Cells[9].AddParagraph(comp.Nama_ttd).Format.Alignment = ParagraphAlignment.Center;
                //row.Cells[9].AddParagraph("________________________").Format.Alignment = ParagraphAlignment.Center;
                //row.Cells[9].AddParagraph(comp.Jabatan_ttd).Format.Alignment = ParagraphAlignment.Center;

                string NamaTTD = "";// comp.Nama_ttd;
                string JbtTTD = "";// comp.Jabatan_ttd;
                row = table.AddRow();
                row.Format.Font.Size = 10;
                row.Height = 60;
                row.Borders.Color = Colors.Transparent;
                //row.Cells[0].MergeRight = 8;
                row.Cells[9].MergeRight = 1;
                row.Cells[9].Borders.Bottom.Color = Colors.Black;

                row.Cells[9].VerticalAlignment = MigraDoc.DocumentObjectModel.Tables.VerticalAlignment.Bottom;
                row.Cells[9].AddParagraph(NamaTTD).Format.Alignment = ParagraphAlignment.Center;

                row = table.AddRow();
                row.Format.Font.Size = 10;
                row.Height = 60;
                row.Borders.Color = Colors.Transparent;
                row.Cells[9].MergeRight = 1;
                row.Cells[9].AddParagraph(JbtTTD).Format.Alignment = ParagraphAlignment.Center;

                #endregion
            } 
        }


        private static void mergeAndCenter(ISheet sheet, NPOI.SS.Util.CellRangeAddress range, bool bold = true)
        {
            sheet.AddMergedRegion(range);
            ICellStyle style = sheet.Workbook.CreateCellStyle();
            style.Alignment = HorizontalAlignment.Center;
            style.VerticalAlignment = VerticalAlignment.Center;

            ICell cell = sheet.GetRow(range.FirstRow).GetCell(range.FirstColumn);
            ICellStyle cellStyle = sheet.Workbook.CreateCellStyle();
            cellStyle.VerticalAlignment = VerticalAlignment.Center;
            cellStyle.Alignment = HorizontalAlignment.Center;

            var font = sheet.Workbook.CreateFont();
            if (bold)
            {
                font.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
            }
            font.FontHeightInPoints = 10;
            font.Color = IndexedColors.Black.Index;
            font.FontName = "Arial";

            cellStyle.SetFont(font);
            cell.CellStyle = cellStyle;

        }

        private static void SetBackgoundHeaderColor(ISheet sheet, int row, int cellnum)
        {
            for (int i = 0; i < cellnum; i++)
            {
                ICell cell = sheet.GetRow(row).GetCell(i);
                XSSFCellStyle cellStyle = (XSSFCellStyle)sheet.Workbook.CreateCellStyle();

                var color = new NPOI.XSSF.UserModel.XSSFColor(new byte[] { 31, 73, 125 });

                cellStyle.SetFillForegroundColor(color);
                cellStyle.FillPattern = FillPattern.SolidForeground;
                cellStyle.VerticalAlignment = VerticalAlignment.Center;
                cellStyle.Alignment = HorizontalAlignment.Center;

                cellStyle.LeftBorderColor = IndexedColors.Black.Index;
                cellStyle.TopBorderColor = IndexedColors.Black.Index;
                cellStyle.RightBorderColor = IndexedColors.Black.Index;
                cellStyle.BottomBorderColor = IndexedColors.Black.Index;


                cellStyle.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
                cellStyle.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
                cellStyle.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
                cellStyle.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;


                var font = sheet.Workbook.CreateFont();
                font.FontHeightInPoints = 10;
                font.Color = NPOI.HSSF.Util.HSSFColor.White.Index; //WHITE
                font.FontName = "Arial";
                font.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;

                cellStyle.SetFont(font);
                cell.CellStyle = cellStyle;
            }
        }

        private static void SetBackgoundHeaderColorFilter(ISheet sheet, int row, int cellnum)
        {
            for (int i = 0; i < cellnum; i++)
            {
                ICell cell = sheet.GetRow(row).GetCell(i);
                XSSFCellStyle cellStyle = (XSSFCellStyle)sheet.Workbook.CreateCellStyle();

                var color = new NPOI.XSSF.UserModel.XSSFColor(new byte[] { 253, 233, 217 });

                cellStyle.SetFillForegroundColor(color);
                cellStyle.FillPattern = FillPattern.SolidForeground;
                cellStyle.VerticalAlignment = VerticalAlignment.Center;
                cellStyle.Alignment = HorizontalAlignment.Center;

                cellStyle.LeftBorderColor = IndexedColors.Black.Index;
                cellStyle.TopBorderColor = IndexedColors.Black.Index;
                cellStyle.RightBorderColor = IndexedColors.Black.Index;
                cellStyle.BottomBorderColor = IndexedColors.Black.Index;


                cellStyle.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
                cellStyle.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
                cellStyle.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
                cellStyle.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;


                var font = sheet.Workbook.CreateFont();
                font.FontHeightInPoints = 10;
                font.Color = NPOI.HSSF.Util.HSSFColor.Black.Index; //WHITE
                font.FontName = "Arial";
                font.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;

                cellStyle.SetFont(font);
                cell.CellStyle = cellStyle;
            }
        }

        private static void SetBorder(ISheet sheet, int row, int cellnum, bool isReport, XSSFCellStyle cellStyle, XSSFCellStyle cellStyleNumeric, XSSFCellStyle cellStyleCenter)
        {

            for (int i = 0; i < cellnum; i++)
            {
                ICell cell = sheet.GetRow(row).GetCell(i);
        
                if (isReport)
                {
                    if (i == 9 || i == 10 || i == 11)
                    {
                        
                        cell.CellStyle = cellStyleNumeric;
                    }
                    else if (i == 2 || i == 3 || i == 7)
                    {

                        cell.CellStyle = cellStyleCenter;
                    }
                    else
                    {
                        cell.CellStyle = cellStyle;
                    }
                }
                else
                {
                    if (i == 0)
                    {
                         
                        cell.CellStyle = cellStyleCenter;

                    }else  if (i == 11 || i == 12 || i == 13 || i == 14)
                    {
                        
                        cell.CellStyle = cellStyleNumeric;
                    }
                    else if (i == 3 || i == 4|| i == 9)
                    {

                        cell.CellStyle = cellStyleCenter;
                    }
                    else
                    {
                        cell.CellStyle = cellStyle;
                    }
                }
            }

        }

        private static void mergeAndCenterAndAddBorderTop(ISheet sheet, NPOI.SS.Util.CellRangeAddress range, bool bold = true)
        {
            sheet.AddMergedRegion(range);
            ICellStyle style = sheet.Workbook.CreateCellStyle();
            style.Alignment = HorizontalAlignment.Center;
            style.VerticalAlignment = VerticalAlignment.Center;


            ICell cell = sheet.GetRow(range.FirstRow).GetCell(range.FirstColumn);
            ICellStyle cellStyle = sheet.Workbook.CreateCellStyle();
            cellStyle.VerticalAlignment = VerticalAlignment.Center;
            cellStyle.Alignment = HorizontalAlignment.Center;
            cellStyle.TopBorderColor = IndexedColors.Black.Index;
            cellStyle.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;

            RegionUtil.SetBorderTop(1, range, sheet, sheet.Workbook);

            var font = sheet.Workbook.CreateFont();
            if (bold)
            {
                font.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
            }
            font.FontHeightInPoints = 10;
            font.Color = NPOI.HSSF.Util.HSSFColor.Black.Index; //WHITE
            font.FontName = "Arial";

            cellStyle.SetFont(font);
            style.SetFont(font);
            cell.CellStyle = cellStyle;

        }
    }
}

