﻿using Kendo.Mvc.UI;
using System.Net;
using System.Web.Mvc;
using Tam.Ebilling.Domain;
using Tam.Ebilling.Service;
using Tam.Ebilling.Infrastructure.Mvc;

namespace Tam.Ebilling.Web.Areas.Core.Controllers
{
    public class EmailTemplateController : WebControllerBase
    {

        [HttpPost]
        public ActionResult Read([DataSourceRequest] Kendo.Mvc.UI.DataSourceRequest request)
        {
            var result = Service<EmailTemplateService>().GetDataSourceResult(request);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }
}