﻿using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Tam.Ebilling.Domain;
using Tam.Ebilling.Infrastructure.Adapter;
using Tam.Ebilling.Infrastructure.Cache;
using Tam.Ebilling.Infrastructure.Helper;
using Tam.Ebilling.Infrastructure.Mvc;
using Tam.Ebilling.Service;

namespace Tam.Ebilling.Web.Areas.Core.Controllers
{
    public class AttachmentTransferController : WebControllerBase
    {
        public JsonResult AllocationRead(string invoiceNumber)
        {
            var result = Service<AttachmentTransferService>().GetAttachmentTransfer(invoiceNumber);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult UploadFileTransferInitial()
        {
            //var Model = Service<AttachmentNotaReturService>().GetAttachment(reference);
            //var listDocument = Service<AttachmentTransferService>().GetAttachmentTransfer(invoiceNumber);

            //ViewBag.ListDocument = listDocument;
            //ViewBag.Invoice = invoiceNumber;
            //ViewBag.Reference = reference;

            var url = "~/Areas/Core/Views/Invoice/Form/BuktiTransferUpload.cshtml";
            return PartialView(url);
        }

        public JsonResult Upload(string invoice)
        {
            string msgerror = "";

            var files = Request.Files;
            var arrayObjects = new List<Tam.Ebilling.Domain.AttachmentTransfer>();
            var uploadService = Service<AttachmentTransferService>();

            if (files != null && files.Count > 0)
            {
                for (var i = 0; i < files.Count; i++)
                {
                    var file = files[i];

                    var doc = uploadService.Upload(new HttpPostedFileBaseAdapter(file), invoice, out msgerror);

                    arrayObjects.Add(doc);
                }
            }
            return Json(new { Result = arrayObjects, MessageError = msgerror }, JsonRequestBehavior.AllowGet);
        }
    }
}