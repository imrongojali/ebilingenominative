﻿using Kendo.Mvc.UI;
using System.Net;
using System.Web.Mvc;
using Tam.Ebilling.Domain;
using Tam.Ebilling.Service;
using Tam.Ebilling.Infrastructure.Mvc;

namespace Tam.Ebilling.Web.Areas.Core.Controllers
{
    public class ConfigController : WebControllerBase
    {

        [HttpPost]
        public ActionResult Read([DataSourceRequest] Kendo.Mvc.UI.DataSourceRequest request)
        {
            var result = Service<ConfigService>().GetDataSourceResult(request);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }
}