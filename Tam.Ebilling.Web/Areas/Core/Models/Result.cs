﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Tam.Ebilling.Web.Areas.Core.Models
{
    public class Result
    {
        public bool ResultCode { get; set; }
        public string ResultDesc { get; set; }
        public string ResultDescs { get; set; }
    }
}