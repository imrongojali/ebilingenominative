﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Tam.Ebilling.Web.Areas.Core.Models
{
    public enum DownloadType
    {
        [Description("0")]
        InvoiceQR,
        [Description("1")]
        PI,
        [Description("2")]
        NotaRetur
    }
}