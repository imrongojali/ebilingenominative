﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tam.Ebilling.Service;
using Tam.Ebilling.Infrastructure.Mvc;
using System.Xml.Linq;
using System.Text;
using StructureMap;
using Tam.Ebilling.Domain;
using Tam.Ebilling.Reporting;
using Tam.Ebilling.Infrastructure.Helper;
using System.Net;
using System.IO;
using System.IO.Compression;

namespace Tam.Ebilling.Web.Areas.Api.Controllers
{
    [Authorize]
    public class AttachmentController : WebControllerBase
    {
        #region Attachment Download
        [AllowAnonymous]
        public void AttachmentDownload(string da_code)
        {
            var username = Service<ParameterService>().GetParameter("file_impersonate_user").value1;
            var domain = Service<ParameterService>().GetParameter("file_impersonate_domain").value1;
            var password = Service<ParameterService>().GetParameter("file_impersonate_password").value1;

            var userdomain = domain + "\\" + username;
            var credential = new NetworkCredential(userdomain, password);

            string fileserver = Service<ParameterService>().GetParameter("file_server").value1;
            string filepath = Service<ParameterService>().GetParameter("file_repo_path").value1;
            string file2 = "\\\\" + fileserver + "\\" + filepath;

            try
            {
                using (new NetworkConnection(file2, credential))
                {
                    var doc = Service<AttachmentService>().GetAttachment(da_code);
                    var outputfile = da_code + ".zip";
                    var zipStream = new MemoryStream();
                    var item2 = doc.First();
                    var filename2 = item2.created_date.ToString("yyyyM") + "\\" + item2.reference + "\\" + item2.filename;
                    var url2 = Path.Combine(file2, filename2);

                    if (System.IO.File.Exists(url2) == true)
                    {
                        if (doc.Count > 1)
                        {
                            var zip = new ZipArchive(zipStream, ZipArchiveMode.Create);

                            foreach (var item in doc)
                            {
                                var filename = item.created_date.ToString("yyyyM") + "\\" + item.reference + "\\" + item.filename;
                                var url = Path.Combine(file2, filename);
                                zip.CreateEntryFromFile(url, item.filename);
                            }

                            zip.Dispose();
                        }
                        else
                        {
                            var item = doc.First();
                            var filename = item.created_date.ToString("yyyyM") + "\\" + item.reference + "\\" + item.filename;
                            var url = Path.Combine(file2, filename);
                            var file = new FileStream(url, FileMode.Open);
                            byte[] bytes = new byte[file.Length];
                            file.Read(bytes, 0, (int)file.Length);
                            zipStream.Write(bytes, 0, (int)file.Length);
                            outputfile = item.filename;
                        }

                        Response.ClearContent();
                        Response.Clear();
                        Response.ContentType = "application/octet-stream";
                        Response.AddHeader("Content-Disposition", "attachment; filename=" + outputfile + ";");
                        Response.OutputStream.Write(zipStream.GetBuffer(), 0, (int)zipStream.Length);
                        Response.Flush();
                        Response.End();
                    }
                    else if (System.IO.File.Exists(url2) == false)
                    {
                        throw new Exception("File not found");
                        //Response.Write(@"<script language='javascript'>alert('Download Attachment is Failed.')</script>");
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region Get Attachment List
        [AllowAnonymous]
        public void GetAttachmentList(string da_code)
        {
            try
            {
                var data = Service<AttachmentNotaReturService>().GetDocumentFileByReference(da_code).FirstOrDefault();
                var xml = AttachmentToXml(data);

                Response.Clear();
                Response.ContentType = "text/xml";
                Response.ContentEncoding = Encoding.UTF8;
                Response.Write(xml);
                Response.End();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region Save Attachment
        //[HttpPost]
        //[Route("saveattachment")]
        //public void SaveAttachment(string reference, List<AttachmentNotaRetur> data)
        //{
        //    string message = string.Empty;

        //    if (!Request.Content.IsMimeMultipartContent())
        //    {
        //        throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
        //    }

        //    try
        //    {
        //        var files = HttpContext.Current.Request.Files;

        //        var arrayObjects = new List<AttachmentNotaRetur>();
        //        var uploadService = Service<AttachmentNotaReturService>();

        //        if (files != null && files.Count > 0)
        //        {
        //            for (var i = 0; i < files.Count; i++)
        //            {
        //                var file = files[i];

        //                var doc = uploadService.Upload(new HttpPostedFileBaseAdapter(new HttpPostedFileWrapper(file)), reference, out message);

        //                arrayObjects.Add(doc);
        //            }
        //        }

        //        var result = Service<AttachmentNotaReturService>().SaveUpload(reference, data, out message);
        //        return Ok(new { result, message });
        //    }
        //    catch (Exception e)
        //    {
        //        return InternalServerError(e);
        //    }
        //}
        #endregion

        [AllowAnonymous]
        public XElement AttachmentToXml(AttachmentNotaRetur att)
        {
            return new XElement("Attachment",
                       new XElement("attachment_notaretur_id", att.attachment_notaretur_id),
                       new XElement("filename", att.filename),
                       new XElement("filepath", att.filepath),
                       new XElement("reference", att.reference),
                       new XElement("created_date", att.created_date),
                       new XElement("created_by", att.created_by),
                       new XElement("modified_date", att.modified_date),
                       new XElement("modified_by", att.modified_by)
                   );
        }

        [AllowAnonymous]
        public XElement AttachmentToXml(List<AttachmentNotaRetur> atts)
        {
            XElement attachments = new XElement("Attachments");

            foreach (var att in atts)
            {
                attachments.Add(AttachmentToXml(att));
            }

            return attachments;
        }
    }
}