﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tam.Ebilling.Service;
using Tam.Ebilling.Infrastructure.Mvc;
using System.Xml.Linq;
using System.Text;
using StructureMap;
using Tam.Ebilling.Domain;
using Tam.Ebilling.Reporting;
using Tam.Ebilling.Infrastructure.Helper;
using System.Net;
using System.IO;

namespace Tam.Ebilling.Web.Areas.Api.Controllers
{
    [Authorize]
    public class InvoiceController : WebControllerBase
    {
        #region Get Single Invoice
        [AllowAnonymous]
        public void GetSingleInvoice(string invoice_number, string customer_group)
        {
            try
            {
                var inv = ObjectFactory.GetInstance<InvoiceService>().GetInvoices(customer_group, invoice_number).ToList().FirstOrDefault();
                var xml = InvoiceToXml(inv);

                Response.Clear();
                Response.ContentType = "text/xml";
                Response.ContentEncoding = Encoding.UTF8;
                Response.Write(xml);
                Response.End();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region Get Invoices
        [AllowAnonymous]
        public void GetInvoices(string customer_group, string invoice_number = null, int? business_unit_id = null, int? transaction_type_id = null, string date_from = null, string date_to = null, string ar_status = null, int? document_status = null)
        {
            try
            {
                var inv = ObjectFactory.GetInstance<InvoiceService>().GetInvoices(customer_group, invoice_number, business_unit_id, transaction_type_id, date_from, date_to, ar_status, document_status);
                var xml = InvoiceToXml(inv);

                Response.Clear();
                Response.ContentType = "text/xml";
                Response.ContentEncoding = Encoding.UTF8;
                Response.Write(xml);
                Response.End();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region Motor Vehicle & Others Invoice Download
        [AllowAnonymous]
        public void InvoiceDownload(string da_number, string business_unit, string user)
        {
            Report report = new Report();
            string mimeType, extension = string.Empty;

            try
            {
                if (!string.IsNullOrEmpty(da_number))// && !string.IsNullOrEmpty(user))
                {
                    if (business_unit == 2.ToString())
                    {
                        var othersReport = report.GetDebitAdviceReport(da_number, business_unit, user, out extension, out mimeType);

                        Response.ClearContent();
                        Response.Clear();
                        Response.ContentType = mimeType;
                        Response.AddHeader("Content-Disposition", "attachment; filename=" + da_number.Replace("/", "-") + "." + extension + ";");
                        Response.OutputStream.Write(othersReport, 0, othersReport.Length);
                        Response.Flush();
                        Response.End();

                        //Service<InvoiceService>().GetUpdate(da_number);
                    }
                    else if (business_unit == 1.ToString())
                    {
                        var mvReport = report.GetMvInvoiceReport(da_number, business_unit, user, out extension, out mimeType);

                        Response.ClearContent();
                        Response.Clear();
                        Response.ContentType = mimeType;
                        Response.AddHeader("Content-Disposition", "attachment; filename=" + da_number.Replace("/", "-") + "." + extension + ";");
                        Response.OutputStream.Write(mvReport, 0, mvReport.Length);
                        Response.Flush();
                        Response.End();

                        //var Update = Service<InvoiceService>().GetUpdate(da_number);
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region Tax Download
        [AllowAnonymous]
        public void TaxInvoiceDownload(string invoice_number)
        {
            try
            {
                string linkdata = string.Empty;
                string filename = string.Empty;
                string ext = string.Empty;
                string mimeType = string.Empty;

                var inv = ObjectFactory.GetInstance<InvoiceService>().GetInvoices("", invoice_number).FirstOrDefault();

                if (inv != null)
                {
                    linkdata = inv.tax_attc_url;

                    if (!string.IsNullOrEmpty(linkdata))
                    {
                        var username = Service<ParameterService>().GetParameter("file_impersonate_user").value1;
                        var domain = Service<ParameterService>().GetParameter("file_impersonate_domain").value1;
                        var userdomain = domain + "\\" + username;
                        var password = Service<ParameterService>().GetParameter("file_impersonate_password").value1;
                        var pers = new Impersonation();
                        pers.Impersonate(userdomain, password);

                        var credential = new NetworkCredential(userdomain, password);

                        using (new NetworkConnection(linkdata, credential))
                        {
                            FileStream fs = new FileStream(linkdata, FileMode.Open, FileAccess.Read);

                            filename = Path.GetFileName(linkdata);
                            mimeType = MimeMapping.GetMimeMapping(filename);

                            byte[] buffer = new byte[4096];
                            int count = 0;

                            while ((count = fs.Read(buffer, 0, buffer.Length)) > 0)
                            {
                                Response.ClearContent();
                                Response.Clear();
                                Response.ContentType = mimeType;
                                Response.AddHeader("Content-Disposition", "attachment; filename=" + filename + ";");
                                Response.OutputStream.Write(buffer, 0, count);
                                Response.Flush();
                                Response.End();
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region Invoice To Xml
        public XElement InvoiceToXml(List<Invoice> invs)
        {
            XElement invoices = new XElement("Invoices");

            foreach(var inv in invs)
            {
                invoices.Add(InvoiceToXml(inv));
            }

            return invoices;
        }

        public XElement InvoiceToXml(Invoice inv)
        {
            return new XElement("Invoice",
                       new XElement("amount_pph", inv.amount_pph),
                       new XElement("ar_status", inv.ar_status),
                       new XElement("ar_status_img_url", inv.ar_status_img_url),
                       new XElement("ar_status_name", inv.ar_status_name),
                       new XElement("business_unit", inv.business_unit),
                       new XElement("business_unit_id", inv.business_unit_id),
                       new XElement("currency", inv.currency),
                       new XElement("customer_group", inv.customer_group),
                       new XElement("customer_name", inv.customer_name),
                       new XElement("da_code", inv.da_code),
                       new XElement("da_no", inv.da_no),
                       new XElement("doc_date", inv.doc_date),
                       new XElement("dpp", inv.dpp),
                       new XElement("due_date", inv.due_date),
                       new XElement("email", inv.email),
                       new XElement("id", inv.id),
                       new XElement("inv_downloaded", inv.inv_downloaded),
                       new XElement("invoice_number", inv.invoice_number),
                       new XElement("invoice_type", inv.invoice_type),
                       new XElement("is_tax_attc_available", inv.is_tax_attc_available),
                       new XElement("notur_downloaded", inv.notur_downloaded),
                       new XElement("notir_uploaded", inv.notur_uploaded),
                       new XElement("pph_no", inv.pph_no),
                       new XElement("rate", inv.rate),
                       new XElement("tax", inv.tax),
                       new XElement("tax_attc_url", inv.tax_attc_url),
                       new XElement("tax_invoice", inv.tax_invoice),
                       new XElement("total_amount", inv.total_amount),
                       new XElement("transaction_type", inv.transaction_type),
                       new XElement("transaction_type_id", inv.transaction_type_id)
                   );
        }
        #endregion
    }
}