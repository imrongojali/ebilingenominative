﻿using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tam.Ebilling.Domain;

namespace Tam.Ebilling.Web
{
    public class Bootstrapper
    {
        public void Setup()
        {
            ObjectFactory.Configure(x =>
            {
                x.For<IDbHelper>().HttpContextScoped().Use(() => (new DbHelper()));

                x.Scan(scanner =>
                {
                    scanner.Assembly("Tam.Ebilling.Domain");
                    scanner.Assembly("Tam.Ebilling.Infrastructure");
                    scanner.Assembly("Tam.Ebilling.Service");
                    scanner.TheCallingAssembly();
                    scanner.WithDefaultConventions();
                });

            });
        }
        public void Dispose()
        {
            ObjectFactory.ReleaseAndDisposeAllHttpScopedObjects();
        }
    }
}