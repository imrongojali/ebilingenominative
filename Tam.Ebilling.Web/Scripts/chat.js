﻿//$(function () {
    var Model = function () {
        var self = this;
        self.message = ko.observable("");
        self.chatRooms = ko.observableArray([]);
        self.chatUsers = ko.observableArray([]);
        self.chatMessages = ko.observableArray([]);
        self.joinedRoom = ko.observable("");
        self.serverInfoMessage = ko.observable("");
        self.myName = ko.observable("");
        self.myAvatar = ko.observable("");
        self.onEnterOk = function (d, e) {
            if (e.keyCode === 13) {
                self.sendNewMessage();
            }
            return true;
        };
        self.notificationCount = ko.observableArray([]);
        //self.filter = ko.observable("");
        //self.filteredChatUsers = ko.computed(function () {
        //    if (!self.filter()) {
        //        return self.chatUsers();
        //    } else {
        //        return ko.utils.arrayFilter(self.chatUsers(), function (user) {
        //            var displayName = user.displayName().toLowerCase();
        //            return displayName.includes(self.filter().toLowerCase());
        //        });
        //    }
        //});

    };

    Model.prototype = {
        clearNotification: function () {
            chatHub.server.clearNotification($('#invoiceNo').val());
        },

        notification: function () {
            var self = this;

            chatHub.server.getNotification().done(function (r) {
                //console.log(self.joinedRoom);
                //console.log(r);
                r.forEach(function (data) {

                    //document.getElementById().innerHTML(data.count);
                    var element = $('#' + data.invoice_number.replace('/', '-').replace('/', '-'));
                    element.html("<span class='label label-sm label-danger circle'>" + data.count + "</span>");

                    if (data.count > 0) {
                        element.show();
                    } else {
                        element.hide();
                    }
                });
            });
        },

        // Server Operations
        connect: function () {
            var self = this;
            chatHub.server.connect();
        },

        disconnect: function () {
            chatHub.server.disconnect();
        },

        sendNewMessage: function () {
            var self = this;
            chatHub.server.send($('#business_unit_id').val(), $('#invoiceNo').val(), self.message(), $('#transaction_type_id').val());
            self.message("");
        },

        joinRoom: function () {
            var self = this;
            chatHub.server.join(self.joinedRoom).done(function () {
                self.userList();
                self.messageHistory();
            });
        },

        getNote: function () {
            var self = this;
            chatHub.server.getNote().done(function (result) {
                alert(result);
            });
        },

        userList: function () {
            var self = this;
            //return;

            chatHub.server.getUsers(self.joinedRoom).done(function (result) {
                self.chatUsers.removeAll();
                for (var i = 0; i < result.length; i++) {
                    self.chatUsers.push(new ChatUser(result[i].Username,
                        result[i].DisplayName,
                        result[i].Avatar,
                        result[i].CurrentRoom,
                        result[i].Device));
                }
            });

        },

        createRoom: function () {
            var name = $("#roomName").val();
            chatHub.server.createRoom(name);
        },

        deleteRoom: function () {
            var self = this;
            chatHub.server.deleteRoom(self.joinedRoom);
        },

        messageHistory: function () {
            var self = this;
            chatHub.server.getMessageHistory(self.joinedRoom).done(function (result) {
                //console.log(self.joinedRoom);
                console.log(result);
                //console.log(self.myName());

                self.chatMessages.removeAll();
                for (var i = 0; i < result.length; i++) {
                    var isMine = result[i].From == self.myName();
                    self.chatMessages.push(new ChatMessage(result[i].Content,
                        result[i].Timestamp,
                        result[i].From,
                        isMine,
                        result[i].Avatar));
                }


                $(".chat-body").animate({ scrollTop: $(".chat-body")[0].scrollHeight }, 1000);

            });
        },

        roomAdded: function (room) {
            var self = this;
            self.chatRooms.push(room);
        },

        roomDeleted: function (id) {
            var self = this;
            var temp;
            ko.utils.arrayForEach(self.chatRooms(), function (room) {
                if (room.roomId() == id)
                    temp = room;
            });
            self.chatRooms.remove(temp);
        },

        userAdded: function (user) {
            var self = this;
            self.chatUsers.push(user);
        },

        userRemoved: function (id) {
            var self = this;
            var temp;
            ko.utils.arrayForEach(self.chatUsers(), function (user) {
                if (user.userName() == id)
                    temp = user;
            });
            self.chatUsers.remove(temp);
        },
    };

    var model = new Model();
    ko.applyBindings(model);

    var chatHub = $.connection.chatHub;

    // Client Operations
    chatHub.client.newMessage = function (messageView) {
        //console.log("test");
        var isMine = messageView.From === model.myName();
        var message = new ChatMessage(messageView.Content,
            messageView.Timestamp,
            messageView.From,
            isMine,
            messageView.Avatar);
        model.chatMessages.push(message);
        $(".chat-body").animate({ scrollTop: $(".chat-body")[0].scrollHeight }, 1000);

        // Notif
        //$("#_" + messageView.InvoiceNo).append("<div style='position:absolute' class='label label-sm label-danger circle'>1</div>");
        //notificationCount = notificationCount + 1;
    };

    chatHub.client.getProfileInfo = function (displayName) {
        model.myName(displayName);
    };

    $.connection.hub.logging = true;
    $.connection.hub.start().done(function () {
        console.log("SignalR started");
        //model.getNote();
        model.connect();
        //model.joinedRoom = $('#business_unit_id').val() + "_" + $('#invoiceNo').val();
        //model.joinRoom();
        //model.userList();
    });

    chatHub.client.addUser = function (user) {
        model.userAdded(new ChatUser(user.Username,
            user.DisplayName,
            user.Avatar,
            user.CurrentRoom,
            user.Device));
    };

    chatHub.client.removeUser = function (user) {
        model.userRemoved(user.Username);
    };

    $("#NotaReturDetailModal").on("shown.bs.modal", function () {
        model.joinedRoom = $('#business_unit_id').val() + "_" + $('#invoiceNo').val();
        model.joinRoom();
        model.userList();
        model.clearNotification();
    });

    $("#InvoiceDetailModal").on("shown.bs.modal", function (e) {
        model.joinedRoom = $('#business_unit_id').val() + "_" + $('#invoiceNo').val();
        model.joinRoom();
        model.userList();
        model.clearNotification();
        console.log("called");
    });

    //chatHub.client.addChatRoom = function (room) {
    //    model.roomAdded(new ChatRoom(room.Id, room.Name));
    //};

    //chatHub.client.removeChatRoom = function (room) {
    //    model.roomDeleted(room.Id);
    //};

    chatHub.client.onError = function (message) {
        model.serverInfoMessage(message);

        $("#errorAlert").removeClass("hidden").show().delay(5000).fadeOut(500);
    };

    //chatHub.client.onRoomDeleted = function (message) {
    //    model.serverInfoMessage(message);
    //    $("#errorAlert").removeClass("hidden").show().delay(5000).fadeOut(500);

    //    // Join to the first room in list
    //    $("ul#room-list li a")[0].click();
    //};

    

    // Represent server data
    function ChatRoom(roomId, name) {
        var self = this;
        self.roomId = ko.observable(roomId);
        self.name = ko.observable(name);
    }

    function ChatUser(userName, displayName, avatar, currentRoom, device) {
        var self = this;
        self.userName = ko.observable(userName);
        self.displayName = ko.observable(displayName);
        self.avatar = ko.observable(avatar);
        self.currentRoom = ko.observable(currentRoom);
        self.device = ko.observable(device);
    }

    function ChatMessage(content, timestamp, from, isMine, avatar) {
        var self = this;
        self.content = ko.observable(content);
        self.timestamp = ko.observable(timestamp);
        self.from = ko.observable(from);
        self.isMine = ko.observable(isMine);
        self.avatar = ko.observable(avatar);
    }

    function RefreshNotification() {
        model.notification();
        //console.log(self.notification);
    }

    //$("#InvoiceDetailModal").on("hidden.bs.modal", function () {
    //    model.disconnect();
    //});
//});