﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tam.Ebilling.Domain;
using Tam.Ebilling.Infrastructure.Cache;
using Tam.Ebilling.Infrastructure.Session;

namespace Tam.Ebilling.Web
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class AppAuthorize : AuthorizeAttribute
    {
        public string[] Permissions { get; }

        public bool RequireAllPermissions { get; set; }

        public AppAuthorize(params string[] permissions)
        {
            Permissions = permissions;
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var rolePermissions = ApplicationCacheManager.Get<IEnumerable<User>>(ApplicationCacheManager.PermissionCacheKey);
            var extendedRoles = new List<string>();
            // impersonation role implementation if any
            // not yet implemented
            // end impersonation

            var roles = SessionManager.Roles.Union(extendedRoles).ToList();

            if (!roles.Any(x => rolePermissions.Any(y => y.user_role == x && Permissions.Any(p => p == y.display_name)))) return false;

            return true;
        }

        protected override void HandleUnauthorizedRequest(System.Web.Mvc.AuthorizationContext filterContext)
        {
            throw new System.Exception("You dont have privillege to access this page.");
        }
    }
}