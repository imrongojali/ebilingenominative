﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Tam.Ebilling.Web.Controllers
{
    public class ErrorController : Controller
    {
        // GET: Error
        public ActionResult Index()
        {
            return View();
        }

        // GET: Error
        public ActionResult Http() 
        {
            return View();
        }

        // GET: Error
        public ActionResult AccessDenied()
        {
            return View();
        }

        public ActionResult JsonError()
        {
            var error = ViewData.Model as HandleErrorInfo;
            return Json(new { Type = false, Message = error.Exception.Message });
        }
    }
}
