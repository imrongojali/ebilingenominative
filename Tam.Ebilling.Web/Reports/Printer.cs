﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Drawing;
using Microsoft.Reporting.WebForms;
using System.Text;
using System.Drawing.Printing;
using System.Drawing.Imaging;

namespace Tam.Ebilling.Web.Reports
{
    public class Printer<T>
    {
        private int m_currentPageIndex;
        private IList<Stream> m_streams; 

        private List<ReportParameter> _reportParams = new List<ReportParameter>();

        public string DeviceInfo { get; set; }
        public string ReportPath { get; set; }

        public List<T> DataSource { get; set; }
        public List<T> DataSource2 { get; set; }
        public List<string> ReportDataSourceName { get; set; }

        public Printer(string deviceInfo, string reportPath, List<string> reportDataSourceName, List<T> dataSource, List<T> dataSource2 = null)
        {
            DeviceInfo = deviceInfo;
            DataSource = dataSource;
            DataSource2 = dataSource2;
            ReportPath = reportPath;
            ReportDataSourceName = reportDataSourceName;
        }

        public void Run()
        {
            LocalReport report = new LocalReport();
            report.ReportPath = ReportPath;

            if (_reportParams.Count > 0)
            {
                report.EnableExternalImages = true;
                report.SetParameters(_reportParams);
            }

            if (ReportDataSourceName.Count == 1)
            {
                report.DataSources.Add(new ReportDataSource(ReportDataSourceName[0], DataSource));
            }
            //else if (ReportDataSourceName.Count > 1)
            //{
            //    report.DataSources.Add(new ReportDataSource(ReportDataSourceName[0], DataSource));
            //    report.DataSources.Add(new ReportDataSource(ReportDataSourceName[1], DataSource2));
            //}

            Export(report);
            Print();
        }

        private void Export(LocalReport report)
        {
            string deviceInfo = DeviceInfo;
            Warning[] warnings;
            m_streams = new List<Stream>();
            report.Render("Image", deviceInfo, CreateStream,
               out warnings);
            foreach (Stream stream in m_streams)
            {
                stream.Position = 0;
            }
        }

        private Stream CreateStream(string name,
          string fileNameExtension, Encoding encoding,
          string mimeType, bool willSeek)
        {
            Stream stream = new MemoryStream();
            m_streams.Add(stream);
            return stream;
        }

        private void Print()
        {
            if (m_streams == null || m_streams.Count == 0)
                throw new Exception("Error: no stream to print.");
            PrintDocument printDoc = new PrintDocument();
            
            if (!printDoc.PrinterSettings.IsValid)
            {
                throw new Exception("Error: cannot find the default printer.");
            }
            else
            {
                printDoc.DefaultPageSettings.Landscape = true;
                PageSettings x = new PageSettings();
                //x.PaperSize
                //var X = printDoc.page
                //    (new PaperSize("First custom size", 850, 49);
                printDoc.PrintPage += new PrintPageEventHandler(PrintPage);
                m_currentPageIndex = 0;
                printDoc.Print();
            }
        }

        private void PrintPage(object sender, PrintPageEventArgs ev)
        {
            Metafile pageImage = new
               Metafile(m_streams[m_currentPageIndex]);

            // Adjust rectangular area with printer margins.
            Rectangle adjustedRect = new Rectangle(
                ev.PageBounds.Left - (int)ev.PageSettings.HardMarginX,
                ev.PageBounds.Top - (int)ev.PageSettings.HardMarginY,
                ev.PageBounds.Width,
                ev.PageBounds.Height);

            // Draw a white background for the report
            ev.Graphics.FillRectangle(Brushes.White, adjustedRect);

            // Draw the report content
            ev.Graphics.DrawImage(pageImage, adjustedRect);

            // Prepare for the next page. Make sure we haven't hit the end.
            m_currentPageIndex++;
            ev.HasMorePages = (m_currentPageIndex < m_streams.Count);
        }
    }
}