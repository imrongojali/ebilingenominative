﻿using Agit.Mvc;
using Tam.Ebilling.Domain;
using Tam.Ebilling.Infrastructure.Cache;
using Tam.Ebilling.Infrastructure.Session;
using Tam.Ebilling.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;

namespace Tam.Ebilling.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private Bootstrapper _bootstrapper = new Bootstrapper();

        protected void Application_Start()
        {
            _bootstrapper.Setup();

            ConfigureApplicationSettings();


            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            //RouteTable.Routes.MapHubs();
        }

        private void ConfigureApplicationSettings()
        {
            using (var Db = new DbHelper())
            {
                var config = Db.ConfigRepository.FindAll();
                var menu = Db.MenuRepository.FindAll();

                ApplicationCacheManager.Set(ApplicationCacheManager.AppConfigCacheKey, config.ToList());
                ApplicationCacheManager.Set(ApplicationCacheManager.PermissionMenuCacheKey, menu.ToList());
            }
        }

        protected void Application_PostAuthenticateRequest(Object sender, FormsAuthenticationEventArgs e)
        {
            if (FormsAuthentication.CookiesSupported == true)
            {
                if (Request.Cookies[FormsAuthentication.FormsCookieName] != null)
                {
                    try
                    {
                        var username = FormsAuthentication.Decrypt(Request.Cookies[FormsAuthentication.FormsCookieName].Value).Name;

                        e.User = new System.Security.Principal.GenericPrincipal(new System.Security.Principal.GenericIdentity(username, "Forms"), new[] { "USER" });
                    }
                    catch (Exception)
                    {
                    }
                }
            }
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            if (HttpContext.Current.Request.IsAuthenticated)
            {
                FormsAuthentication.SignOut();

                HttpCookie cookie_auth = new HttpCookie(FormsAuthentication.FormsCookieName, string.Empty);
                cookie_auth.Expires = DateTime.Now.AddDays(6);
                Response.Cookies.Add(cookie_auth);

                FormsAuthentication.RedirectToLoginPage();
                Response.End();
            }
        }

        protected void Session_End(object sender, EventArgs e)
        {
            Session.RemoveAll();
            FormsAuthentication.SignOut();
        }

        protected void Request_End()
        {
            string ext = HttpContext.Current.Request.CurrentExecutionFilePathExtension;

            if (string.IsNullOrEmpty(ext))
                _bootstrapper.Dispose();
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            HttpApplication app = sender as HttpApplication;
            var error = new MvcException();
            var exp = app.Context.Server.GetLastError();

            using (DbHelper Db = new DbHelper())
            {
                var browser = HttpContext.Current.Request.Browser;
                var routeData = HttpContext.Current.Request.RequestContext.RouteData;
                string area = routeData.DataTokens["area"] != null ? routeData.DataTokens["area"].ToString() : string.Empty,
                        controller = routeData.Values["controller"].ToString(),
                        action = routeData.Values["action"].ToString();

                if (SessionManager.UserSession == null)
                    return;

                var errorLog = new ApplicationLog
                {
                    created_by = SessionManager.Current,
                    created_date = DateTime.Now,
                    username = SessionManager.Username,
                    message_type = "Err",
                    message_location = string.Format("<b>Module:</b> {0}<br/><b>Controller:</b> {1}<br/><b>Action:</b> {2}", area, controller, action),
                    message_description = exp.InnerException != null ? exp.InnerException.Message : exp.Message,
                    iP = HttpContext.Current.Request.UserHostAddress,
                    browser = string.Format("<b>Browser:</b> {0}<br/><b>Version:</b> {1}", browser.Browser, browser.Version)
                };

                Db.LogRepository.Add(errorLog, new string[] { "", "" });
            };


            error.HandleError(app.Context, new ErrorController());

            //actionContext.Response = actionContext.Request.CreateErrorResponse(HttpStatusCode.BadRequest, exp);
        }
    }
}
