﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using Tam.Ebilling.Domain;
using Tam.Ebilling.Infrastructure.Session;
using Tam.Ebilling.Web.ViewModels;
using System.Threading.Tasks;
using Tam.Ebilling.Infrastructure.Enum;
using Agit.Common.Email;
using System.Net.Mail;
using System.Net;
using System.ComponentModel.DataAnnotations;

namespace Tam.Ebilling.Web
{
    public class ChatHub : Hub
    {
        #region Properties
        /// <summary>
        /// List of online users
        /// </summary> 
        public readonly static List<UserViewModel> _Connections = new List<UserViewModel>();

        /// <summary>
        /// List of available chat rooms
        /// </summary>
        private readonly static List<DocumentViewModel> _Rooms = new List<DocumentViewModel>();

        /// <summary>
        /// Mapping SignalR connections to application users.
        /// (We don't want to share connectionId)
        /// </summary>
        private readonly static Dictionary<string, string> _ConnectionsMap = new Dictionary<string, string>();
        int comment_id;
        #endregion

        public string GetNote()
        {
            return "this is a note";
        }

        public void Send(int business_unit_id, string invoice_number, string message, string transaction_type_id)
        {
            bool isSendMail = false;
            var chat_id = string.Format("{0}_{1}", business_unit_id.ToString(), invoice_number);

            try
            {
                using (var db = new DbHelper())
                {
                    Comment msg = new Comment();
                    msg.business_unit_id = business_unit_id;
                    msg.invoice_number = invoice_number;
                    msg.comment = message;
                    msg.user_id = IdentityName;
                    msg.created_by = IdentityName;
                    msg.created_date = DateTime.Now;

                    comment_id = db.CommentRepository.Add(msg, new string[]
                    {
                        "business_unit_id",
                        "invoice_number",
                        "comment",
                        "user_id",
                        "created_by",
                        "created_date"
                    });

                    MessageViewModel messageViewModel = new MessageViewModel();
                    messageViewModel.Content = message;
                    messageViewModel.From = IdentityName;
                    messageViewModel.Timestamp = DateTime.Now.TimeOfDay.ToString();
                    //messageViewModel.InvoiceNo = invoice_number.Replace("/","-");

                    Clients.Group(chat_id).newMessage(messageViewModel);

                    populateRecipients(business_unit_id, invoice_number, transaction_type_id, msg);
                }
            }
            catch (Exception ex)
            {
                using (var db = new DbHelper())
                {
                    var browser = HttpContext.Current.Request.Browser;

                    var errorLog = new ApplicationLog
                    {
                        created_by = IdentityName,
                        created_date = DateTime.Now,
                        username = IdentityName,
                        message_type = "Err",
                        message_location = string.Format("<b>Module:</b> {0}<br/><b>Controller:</b> {1}<br/><b>Action:</b> {2}", "Chat Email Notification", "ChatHub", "Send"),
                        message_description = ex.InnerException != null ? ex.InnerException.Message : ex.Message,
                        iP = HttpContext.Current.Request.UserHostAddress,
                        browser = string.Format("<b>Browser:</b> {0}<br/><b>Version:</b> {1}", browser.Browser, browser.Version)
                    };

                    db.LogRepository.Add(errorLog, new string[] { "", "" });
                }
            }
        }

        public void Join(string documentName)
        {
            try
            {
                var user = _Connections.Where(u => u.Username == IdentityName).FirstOrDefault();
                if (user.CurrentDocument != documentName)
                {
                    // Remove user from others list
                    if (!string.IsNullOrEmpty(user.CurrentDocument))
                        Clients.OthersInGroup(user.CurrentDocument).removeUser(user);

                    // Join to new chat room
                    Leave(user.CurrentDocument);
                    Groups.Add(Context.ConnectionId, documentName);
                    user.CurrentDocument = documentName;

                    // Tell others to update their list of users
                    Clients.OthersInGroup(documentName).addUser(user);
                }
            }
            catch (Exception ex)
            {
                Clients.Caller.onError("You failed to join the chat room!" + ex.Message);
            }
        }

        private void Leave(string documentName)
        {
            Groups.Remove(Context.ConnectionId, documentName);
        }

        public IEnumerable<MessageViewModel> GetMessageHistory(string documentName)
        {
            List<MessageViewModel> msg = new List<MessageViewModel>();

            using (var db = new DbHelper())
            {
                string buid = documentName.Split('_')[0];
                string invno = documentName.Split('_')[1];
                var data = db.CommentRepository.GetHistoryComment(invno, buid);

                foreach(var d in data)
                {
                    MessageViewModel messageViewModel = new MessageViewModel();
                    messageViewModel.Content = d.comment;
                    messageViewModel.Timestamp = d.created_date.ToShortTimeString();
                    messageViewModel.From = d.user_id;
                    msg.Add(messageViewModel);
                }
            }

            return msg;
        }

        public IEnumerable<UserViewModel> GetUsers(string documentName)
        {
            return _Connections.Where(u => u.CurrentDocument == documentName).ToList();
        }

        public void Connect()
        {
            using (var db = new DbHelper())
            {
                try
                {
                    var user = IdentityName;

                    UserViewModel userViewModel = new UserViewModel();
                    userViewModel.Username = user;
                    userViewModel.DisplayName = user;
                    userViewModel.Avatar = null;
                    userViewModel.CurrentDocument = "";
                    userViewModel.Device = GetDevice();

                    if (_Connections.Count == 0 || _Connections.Find(x => x.Username == userViewModel.Username) == null)
                    {
                        _Connections.Add(userViewModel);
                    }

                    if (_ConnectionsMap.Count == 0 || !_ConnectionsMap.ContainsKey(IdentityName))
                    {
                        _ConnectionsMap.Add(IdentityName, Context.ConnectionId);
                    }

                    Clients.Caller.getProfileInfo(userViewModel.DisplayName);
                }
                catch (Exception ex)
                {
                    Clients.Caller.onError("OnConnected:" + ex.Message);
                }
            }
        }

        public void Disconnect()
        {
            try
            {
                var user = _Connections.Where(u => u.Username == IdentityName).First();

                if (user != null)
                {
                    _Connections.Remove(user);

                    // Tell other users to remove you from their list
                    Clients.OthersInGroup(user.CurrentDocument).removeUser(user);

                    // Remove mapping
                    _ConnectionsMap.Remove(user.Username);
                }
            }
            catch (Exception ex)
            {
                Clients.Caller.onError("OnDisconnected: " + ex.Message);
            }
        }

        #region  OnConnected/OnDisconnected
        public override Task OnConnected()
        {
            using (var dbhelper = new DbHelper())
            {
                var user = dbhelper.UserRepository.Find(new { user_id = IdentityName }).FirstOrDefault();

                if (_Connections.Where(x => x.Username == user.user_id).Count() > 0)
                {
                    var userViewModel = new UserViewModel();

                    userViewModel.Username = user.user_id;
                    userViewModel.DisplayName = user.user_id;
                    userViewModel.Device = GetDevice();
                    userViewModel.CurrentDocument = "";

                    _Connections.Add(userViewModel);

                    if (_ConnectionsMap.Where(x => x.Key == IdentityName).Count() == 0)
                    {
                        _ConnectionsMap.Add(IdentityName, Context.ConnectionId);
                    }

                    Clients.Caller.getProfileInfo(user.display_name);
                }
            }

            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            try
            {
                var user = _Connections.Where(u => u.Username == IdentityName).First();

                if (user != null)
                {
                    _Connections.Remove(user);

                    // Tell other users to remove you from their list
                    Clients.OthersInGroup(user.CurrentDocument).removeUser(user);

                    // Remove mapping
                    _ConnectionsMap.Remove(user.Username);
                }
            }
            catch (Exception ex)
            {
                Clients.Caller.onError("OnDisconnected: " + ex.Message);
            }

            return base.OnDisconnected(stopCalled);
        }

        public override Task OnReconnected()
        {
            try
            {
                var user = _Connections.Where(u => u.Username == IdentityName).First();
                if (user != null)
                {
                    Clients.Caller.getProfileInfo(user.DisplayName);
                }
                else
                {
                    Connect();
                }
            }
            catch (Exception e)
            {

            }

            return base.OnReconnected();
        }
        #endregion

        private string IdentityName
        {
            get { return Context.User.Identity.Name; }
        }

        private string GetDevice()
        {
            string device = Context.Headers.Get("Device");

            if (device != null && (device.Equals("Desktop") || device.Equals("Mobile")))
                return device;

            return "Web";
        }

        private void SendEmail(IList<string> to, Comment msg)
        {
            string username, password, domain, host, port, subject, body, from;

            // Generate email setting from tb_m_parameter
            using (var db = new DbHelper())
            {
                username = db.ParameterRepository.Find(new { key_param = "email_username_chat_notification" }).FirstOrDefault().value1;
                password = db.ParameterRepository.Find(new { key_param = "email_password_chat_notification" }).FirstOrDefault().value1;
                domain = db.ParameterRepository.Find(new { key_param = "email_domain_chat_notification" }).FirstOrDefault().value1;
                host = db.ParameterRepository.Find(new { key_param = "email_host_chat_notification" }).FirstOrDefault().value1;
                port = db.ParameterRepository.Find(new { key_param = "email_port_chat_notification" }).FirstOrDefault().value1;
                subject = db.ParameterRepository.Find(new { key_param = "email_subject_chat_notification" }).FirstOrDefault().value1;
                body = db.ParameterRepository.Find(new { key_param = "email_body_chat_notification" }).FirstOrDefault().value2;
                from = username;
            }

            // Generate body
            body = string.Format(body, msg.user_id, msg.invoice_number, msg.comment, msg.invoice_number);

            //EmailService emailSvc = new EmailService();
            SmtpClient emailSvc = new SmtpClient();
            emailSvc.Host = host;
            //emailSvc.Port = Convert.ToInt32(port);
            //emailSvc.User = username;
            //emailSvc.Password = password;
            emailSvc.UseDefaultCredentials = false;
            emailSvc.Credentials = new NetworkCredential(username, password);
            emailSvc.EnableSsl = false;
            if (from.Contains("gmail")) { emailSvc.EnableSsl = true; emailSvc.Port = Convert.ToInt32(port); }

            //EmailMessage emailMessage = new EmailMessage();
            MailMessage emailMessage = new MailMessage(from, from);
            //emailMessage.From = new MailAddress(from);
            emailMessage.To.RemoveAt(0);
            emailMessage.Subject = string.Format(subject, msg.invoice_number);
            emailMessage.Body = body;
            emailMessage.IsBodyHtml = true;

            foreach(var mail in to)
            {
                //emailMessage.Recipients.Add(mail);
                emailMessage.To.Add(mail);
            }

            emailSvc.Send(emailMessage);
        }

        private void AddCommentDetail(List<CommentDetail> commentDetails)
        {
            using (var db = new DbHelper())
            {
                foreach (var commentDetail in commentDetails)
                {
                    string[] columns = { "comment_id", "user_id" };
                    db.CommentDetailRepository.Add(commentDetail, columns);
                }
            }
        }

        public IEnumerable<Notification> GetNotification()
        {
            using(var db = new DbHelper())
            {
                var result = db.CommentDetailRepository.Get(IdentityName);

                return result.ToList();
                //Clients.Caller.getNotification(result);
                //Clients.Group(chat_id).newMessage(messageViewModel);
            }
        }

        public void ClearNotification(string invoice_no)
        {
            using (var db = new DbHelper())
            {
                db.CommentDetailRepository.Update(invoice_no, IdentityName);
            }
        }

        private void populateRecipients(int business_unit_id, string invoice_number, string transaction_type_id, Comment msg)
        {
            List<CommentDetail> commentDetails = new List<CommentDetail>();

            using (var db = new DbHelper())
            {
                List<string> listEmail = new List<string>();

                var user = db.UserRepository.Find(new { user_id = msg.user_id }).FirstOrDefault();

                if (user.user_type == "Customer")
                {
                    if (business_unit_id == 1)
                    {
                        var userAr = db.TransactionTypePicRepository.Find(new { Level_Code = "AR" }).ToList();

                        foreach (var ar in userAr)
                        {
                            if (!listEmail.Contains(ar.Email))
                            {
                                if (new EmailAddressAttribute().IsValid(ar.Email))
                                {
                                    listEmail.Add(ar.Email);
                                }
                            }
                        }
                    }
                    else if (business_unit_id == 2)
                    {
                        var userPic = db.TransactionTypePicRepository.Find(new { transaction_type_id = transaction_type_id });
                        var userAr = db.TransactionTypePicRepository.Find(new { Level_Code = "AR" }).ToList();
                        var userBm = db.TransactionTypePicRepository.Find(new { Level_Code = "BM" }).ToList();

                        foreach (var pic in userPic)
                        {
                            if (!listEmail.Contains(pic.Email))
                            {
                                if (new EmailAddressAttribute().IsValid(pic.Email))
                                {
                                    listEmail.Add(pic.Email);
                                }
                            }
                        }

                        foreach (var ar in userAr)
                        {
                            if (!listEmail.Contains(ar.Email))
                            {
                                if (new EmailAddressAttribute().IsValid(ar.Email))
                                {
                                    listEmail.Add(ar.Email);
                                }
                            }
                        }

                        foreach(var bm in userBm)
                        {
                            if (!listEmail.Contains(bm.Email))
                            {
                                if (new EmailAddressAttribute().IsValid(bm.Email))
                                {
                                    listEmail.Add(bm.Email);
                                }
                            }
                        }
                    }
                }
                else if (user.user_type == "Internal")
                {
                    var users = db.UserRepository.Find(new { customer_group = user.customer_group });

                    foreach (var usr in users)
                    {
                        if (usr.customer_group == user.customer_group)
                        {
                            if (!listEmail.Contains(usr.email))
                            {
                                if (new EmailAddressAttribute().IsValid(usr.email))
                                {
                                    listEmail.Add(usr.email);
                                }
                            }
                        }
                    }
                }

                // Notifikasi
                var listOfUser = db.UserRepository.Find(new { customer_group = user.customer_group });

                foreach (var u in listOfUser)
                {
                    if (u.user_id != user.user_id)
                    {
                        CommentDetail cd = new CommentDetail();
                        cd.comment_id = comment_id;
                        cd.user_id = u.user_id;

                        commentDetails.Add(cd);
                    }
                }

                if (commentDetails.Count > 0)
                {
                    AddCommentDetail(commentDetails);
                }

                // debug only
                // change email recepient at tb_m_parameter 
                //listEmail.Clear();
                //listEmail.Add(db.ParameterRepository.Find(new { key_param = "email_to_chat_notification" }).FirstOrDefault().value1);

                if (listEmail.Count() > 0)
                {
                    SendEmail(listEmail, msg);
                }
            }
        }
    }
}