﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Web.Mvc;
using Tam.Ebilling.Domain;

namespace Tam.Ebilling.Web.Helper
{
    public static class CustomLog
    {
        public static void WriteAppLog(string logType, string logDesc, string module)
        {
            using (var db = new DbHelper())
            {
                var log = new ApplicationLog
                {
                    created_by = "System",
                    created_date = DateTime.Now,
                    username = "System",
                    message_type = logType,
                    message_location = string.Format("<b>Module:</b> {0}", module),
                    message_description = logDesc,
                    iP = Dns.GetHostAddresses(Dns.GetHostName())[0].ToString(),
                    browser = string.Format("<b>Browser:</b> {0}<br/><b>Version:</b> {1}", "None", "None")
                };

                db.LogRepository.Add(log, null);
            }

            //using (var db = new DbHelper())
            //{
            //    var browser = HttpContext.Current.Request.Browser;

            //    var errorLog = new ApplicationLog
            //    {
            //        created_by = IdentityName,
            //        created_date = DateTime.Now,
            //        username = IdentityName,
            //        message_type = "Err_Notif_BuktiPotong",
            //        message_location = string.Format("<b>Module:</b> {0}<br/><b>Controller:</b> {1}<br/><b>Action:</b> {2}", "Chat Email Notification", "ChatHub", "Send"),
            //        message_description = ex.InnerException != null ? ex.InnerException.Message : ex.Message,
            //        iP = HttpContext.Current.Request.UserHostAddress,
            //        browser = string.Format("<b>Browser:</b> {0}<br/><b>Version:</b> {1}", browser.Browser, browser.Version)
            //    };

            //    db.LogRepository.Add(errorLog, new string[] { "", "" });
            //}
        }
    }
}