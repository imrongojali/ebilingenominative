﻿using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Tam.Ebilling.Domain;
using System.Web;
using Tam.Ebilling.Infrastructure.Mvc;
using Tam.Ebilling.Service;
using Tam.Ebilling.Infrastructure.Cache;
using Tam.Ebilling.Infrastructure.Session;

namespace Tam.Ebilling.Web
{
    public class CommonFeatureController : WebControllerBase
    {
        protected virtual bool IsAllowed(string featureCode)
        {
            var menus = ApplicationCacheManager.Get<IEnumerable<Menu>>(ApplicationCacheManager.PermissionMenuCacheKey);
            var controller = ControllerContext.RouteData.Values["controller"];

            var menu = menus.FirstOrDefault(m => m.url.Contains((string) controller));

            return Service<MenuFeatureService>().IsFeatureAllowed(menu.menu_id, featureCode, SessionManager.LevelCode);
        }

        protected virtual bool IsRoleAllowedAccessMenu()
        {
            var menus = ApplicationCacheManager.Get<IEnumerable<Menu>>(ApplicationCacheManager.PermissionMenuCacheKey);
            var controller = ControllerContext.RouteData.Values["controller"];

            var menu = menus.FirstOrDefault(m => m.url.Contains((string)controller));

            return Service<MenuRoleService>().IsRoleAllowedAccessMenu(menu.menu_id, SessionManager.LevelCode);
        }

        protected virtual bool IsRoleAllowedAccessMenu(string userType, string user)
        {
            var menus = ApplicationCacheManager.Get<IEnumerable<Menu>>(ApplicationCacheManager.PermissionMenuCacheKey);
            var controller = ControllerContext.RouteData.Values["controller"];

            var menu = menus.FirstOrDefault(m => m.url.Contains((string)controller));

            return Service<MenuRoleService>().IsRoleAllowedAccessMenu(menu.menu_id, userType, user);
        }

        protected virtual void DownloadFinish()
        {
            try
            {
                System.Web.HttpCookie myCookie = new System.Web.HttpCookie("fileDownload");
                myCookie.Value = "true";
                System.Web.HttpContext.Current.Response.Cookies.Add(myCookie);
            }
            catch (Exception)
            {
                 
            }
            
        }
    }

}
