﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Tam.Ebilling.Domain;
using Tam.Ebilling.Service;
using Tam.Ebilling.Web.Areas.Core.Controllers;

namespace Tam.Ebilling.Test
{
    [TestClass]
    public class WhenRequestingTheInvoicePage
    {
        [TestMethod]
        public void ThenReturnInvoicePage()
        {
            var transactionType = new List<TransactionType>();

            var invoiceController = new InvoiceController();
            var mockTranscationTypeService = new Mock<TransactionTypeService>();
            mockTranscationTypeService.Setup( x=> x.GetTransactionType()).Returns(transactionType);

            var result = invoiceController.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ThenReturnReadBasedOnFilter()
        {
            //var invoiceController = new InvoiceController();
        }
    }
}
