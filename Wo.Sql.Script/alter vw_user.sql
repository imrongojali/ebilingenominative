USE [BES]
GO

/****** Object:  View [dbo].[vw_User]    Script Date: 10/22/2019 2:36:02 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[vw_User]
AS


SELECT 
user_id, user_name as display_name, NULL as password, email, 'Customer' as user_type, customer_group, null as level_code
  FROM tb_m_customer_id
  --order by dealer_code
union
select username as user_id, display_name, null as password, domain as email, 'Internal' as user_type, null as customer_group,
level_code
from tb_m_security_users
-- temporary remove top 2 because is weird

GO


