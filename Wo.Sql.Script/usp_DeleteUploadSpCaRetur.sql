USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[usp_DeleteUpload]    Script Date: 9/27/2019 9:15:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		wo\dwi
-- Create date: 27 September 2019
-- Description:	Delete Upload Spare Part Nota Retur
-- =============================================
ALTER PROCEDURE [dbo].[usp_DeleteUploadSpCaRetur] 
	-- Add the parameters for the stored procedure here
	@attachment_spcaretur_id bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	delete tb_r_attachment_spca_retur
	WHERE attachment_spcaretur_id = @attachment_spcaretur_id

END


