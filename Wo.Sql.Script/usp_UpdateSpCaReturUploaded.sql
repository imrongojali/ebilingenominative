USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateNoturUploaded]    Script Date: 9/26/2019 5:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		wo\dwi
-- Create date: 26 September 2019
-- Description:	Update Spare Part Notur_uploaded
-- =============================================
ALTER PROCEDURE [dbo].[usp_UpdateSpCaReturUploaded] 
	-- Add the parameters for the stored procedure here
	@reff varchar(15)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE tb_r_spda_invoice_header
	SET NOTUR_UPLOAD = 1
	WHERE ca_no = @reff

END


