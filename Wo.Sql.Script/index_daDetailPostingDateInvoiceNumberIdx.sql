/*
Missing Index Details from SQLQuery5.sql - localhost\MSSQLSERVER2016.BES (sa)
The Query Processor estimates that implementing the following index could improve the query cost by 14.0515%.
*/


USE [BES]
GO
CREATE NONCLUSTERED INDEX daDetailPostingDateInvoiceNumberIdx
ON [dbo].[tb_r_da_detil] ([postingdate],[invoice_number])
INCLUDE ([da_detil_code],[da_number],[duedate],[customer_code],[customer_others_id],[da_code],[journal_clearing],[tax_number],[retur_date],[inv_downloaded],[notur_downloaded],[notur_uploaded],[downloaded_by],[downloaded_date],[inv_scan_downloaded],[downloadedscan_date],[downloadedscan_by])
GO