USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[usp_Print_DebitAdvice]    Script Date: 9/18/2019 2:47:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		wo\dwi
-- Create date: 2019-10-03
-- Description:	Print Nota Retur (Detail) Credit Advice
-- =============================================
ALTER PROCEDURE [dbo].[usp_Print_NotaReturSpCreditAdvice] 
	-- Add the parameters for the stored procedure here
	 @ca_number varchar(20) ='' --FCA/SP/17C0313
	,@user varchar(50)='' -- 60000
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select
		right(ltrim(rtrim(d.CA_NO)), 7) + '-' + right(ltrim(rtrim(h.FAKTUR_PAJAK_NO)), 8) as NomorRetur,
		h.FAKTUR_PAJAK_NO as FakturPajakNo,
		h.DLV_DATE as TglRetur, -- TODO
		d.DELIVERY_DATE as TglFakturPajak, -- TODO
		h.CUST_NAME as CustName,
		h.CUST_ADDR1 as Address1,
		h.CUST_ADDR2 as Address2,
		h.NPWP as Npwp,
		d.PI_NO as PiNo,
		d.PART_NO as PartNo,
		(select top 1 p.PART_NAME from [10.85.40.55].[DB_SP_INF].[dbo].[TB_I_H01_02_PNO] p 
			where p.PNO = d.PART_NO) as PartName,
		d.PICKING_QTY as Kuantum,
		d.SALES_PRICE as HargaSatuan,
		(d.PICKING_QTY * d.SALES_PRICE) as HargaBkp
		from tb_r_spda_invoice_detail d
		inner join tb_r_spda_invoice_header h 
			on h.CA_NO = d.CA_NO
				and h.DA_NO = d.DA_NO
		left join [10.85.40.55].[DB_SP_INF].[dbo].[TB_I_H01_02_PNO] p 
			on p.PNO = d.PART_NO
	 where d.CA_NO = @ca_number
END

