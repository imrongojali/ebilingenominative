USE [BES]
GO

/****** Object:  Table [dbo].[tb_r_bukti_potong]    Script Date: 11/4/2019 9:41:17 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tb_r_bukti_potong](
	[invoice_number] [varchar](20) NOT NULL,
	[materai_amount] [numeric](17, 3) NOT NULL,
	[amount_wht] [numeric](17, 3) NOT NULL,
	[tgl_payment] [datetime] NULL,
	[is_active] [bit] NOT NULL,
	[bukti_potong_uploaded] [bit] NULL,
	[submit_date] [datetime] NULL,
	[submit_by] [varchar](50) NULL,
	[created_date] [datetime] NOT NULL,
	[created_by] [varchar](50) NOT NULL,
	[modified_date] [datetime] NULL,
	[modified_by] [varchar](50) NULL,
	[row_status] [smallint] NOT NULL,
 CONSTRAINT [PK_tb_r_bukti_potong] PRIMARY KEY CLUSTERED 
(
	[invoice_number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[tb_r_bukti_potong] ADD  CONSTRAINT [DF_tb_r_bukti_potong_is_active]  DEFAULT ((0)) FOR [is_active]
GO

ALTER TABLE [dbo].[tb_r_bukti_potong] ADD  CONSTRAINT [DF_tb_r_bukti_potong_bukti_potong_uploaded]  DEFAULT ((0)) FOR [bukti_potong_uploaded]
GO

ALTER TABLE [dbo].[tb_r_bukti_potong] ADD  CONSTRAINT [DF_tb_r_bukti_potong_row_status]  DEFAULT ((0)) FOR [row_status]
GO


