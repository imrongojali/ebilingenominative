USE [BES]
GO

/****** Object:  UserDefinedFunction [dbo].[udf_SumInvoice]    Script Date: 4/16/2020 02:09:49 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE FUNCTION [dbo].[udf_SumInvoice]
(
    @invoice_type smallint, -- 0: all, 1: invoice, 2: nota retur
	@customer_group nvarchar(50), -- if '' = internal TAM
	@totAmount nvarchar(50)
)
RETURNS table as return
(
    -- Fill the table variable with the rows for your result set
	
	with cte as
	(
		SELECT  --TOP 1000
			Convert(varchar(1000), b.da_number) as id,
			'Others' as business_unit,
			2 as business_unit_id,
			B.invoice_number AS invoice_number,
			d.transaction_name AS transaction_type,
			d.transaction_type_id AS transaction_type_id,
			case when b.retur_date is null then B.postingdate else b.retur_date end AS doc_date,
			B.duedate AS due_date,
			a.invoice_currency AS currency,
			sum(c.price_invoice) AS dpp,
			sum(c.vat_invoice) AS tax, 
			sum(c.total_invoice) AS total_amount,
			CASE	when p.da_detil_code = b.da_detil_code THEN 'InvStat05'	
					WHEN [journal_clearing] is not null and [journal_clearing] != '' THEN 'InvStat01'
					WHEN duedate = CONVERT(DATE, GETDATE()) THEN 'InvStat04'
					WHEN duedate >= CONVERT(DATE, GETDATE()) THEN 'InvStat03'
					WHEN duedate <= CONVERT(DATE, GETDATE()) THEN 'InvStat02'
									
			END AS ar_status,
			B.tax_number AS tax_invoice,
			'' AS pph_no,
			a.da_code as da_code,
			b.da_number as da_no,
			m.customer_code,
			m.customer_name,
			m.customer_group,
			f.rate as exc_rate,
			'' as amount_pph,
			b.inv_downloaded as inv_downloaded,
			--ef.ApprovalStatus as approvalstatus,
			b.notur_uploaded as notur_uploaded,
			case when b.notur_uploaded is null then 0 else 1 end as is_notur_upload_done,
			b.notur_downloaded as notur_downloaded,
			b.downloaded_date as downloaded_date,
			b.downloaded_by as downloaded_by,
			CASE	WHEN b.inv_downloaded is null THEN 0
				ELSE 1 END AS is_inv_available,
				b.inv_scan_downloaded as inv_scan_downloaded,
				b.downloadedscan_date as downloadedscan_date,
				b.downloadedscan_by as downloadedscan_by,
				CASE	WHEN b.inv_scan_downloaded is null THEN 0
				ELSE 1 END AS is_inv_scan_available,
				'1.00' as rate_pph,
				c.vat_dormitory as vat_dormitory,
				c.dormitory,
				null as pi_downloaded,
				null as pi_downloaded_date,
				null as pi_downloaded_by,
				case when b.customer_others_id is null 
				then 
					(select c.street from tb_m_customer c where c.customer_code = m.customer_code)
				else 
					(select co.street from tb_r_customer_others co where co.customer_others_id = b.customer_others_id)
				end as customer_address,
				case when b.retur_date is null then 0 else 1 end as is_retur
		FROM	[BES].[dbo].[tb_r_da] a
		INNER JOIN [dbo].[tb_r_da_detil] b ON a.da_code = b.da_code
		INNER JOIN [dbo].[tb_r_da_detil_item] c ON b.da_detil_code = c.da_detil_code
		LEFT JOIN [dbo].tb_r_propose_reverse p ON p.da_detil_code = b.da_detil_code
		INNER JOIN [dbo].[tb_m_transaction_type] d ON a.transaction_type_id = d .transaction_type_id
		LEFT JOIN [dbo].tb_m_customer_mapping m on m.customer_code = b.customer_code and m.business_unit_id = 2
		LEFT JOIN [LS_COINS_TAM].[TAM].[dbo].[tb_m_currency] f on a.invoice_currency = f.currency_code
		LEFT JOIN [TAM_EFAKTUR].[dbo].[TB_R_VATOut] ef on b.tax_number = ef.NomorFakturGabungan and ef.ApprovalStatus = 'Approval Sukses'
		where b.Invoice_number is not null and b.Invoice_number != '' and b.postingdate >= '2019-02-01'
		 --and ef.ApprovalStatus != 'Reject'
		and (
				(@customer_group != '' and m.customer_group = @customer_group)
				or 
				(@customer_group = '' and b.customer_code is not null)
			)
		and
			(
				-- seperti request dari mas Sabid, 
				--Di menu Invoice, data yang ditampilkan adalah data DA dan CA berlaku untuk other, MV, SP
				(@invoice_type = 1 and (b.retur_date is null or b.retur_date is not null))
				or
				(@invoice_type = 2 and b.retur_date is not null)
				or
				(@invoice_type = 3 and [journal_clearing] is not null and [journal_clearing] != '')
				or
				(@invoice_type = 0 and 1=1)
			)
		GROUP BY a.da_code, a.invoice_currency, inv_downloaded,
		b.da_number, b.invoice_number, b.tax_number,  b.postingdate, b.duedate, b.journal_clearing, b.tax_number,
		d.transaction_name, d.transaction_type_id,
		f.rate,
		m.customer_code, m.customer_name, m.customer_group, ef.ApprovalStatus, b.notur_uploaded, b.notur_downloaded,
		b.downloaded_date, b.downloaded_by, b.inv_scan_downloaded, b.downloadedscan_date, b.downloadedscan_by, c.vat_dormitory,
		c.dormitory ,p.da_detil_code, b.da_detil_code, b.customer_others_id, b.retur_date

		UNION

		SELECT --TOP 1000
			--Convert(varchar(1000), A.detail_id) as id,
			'' as id,
			'Motor Vehicle' as business_unit,
			1 as business_unit_id,
			A.[da_no] AS invoice_number,
			'SALES MV' AS transaction_type,
			0 AS transaction_type_id,
			A.[do_date] AS doc_date,
			a.[due_date] AS due_date,
			'IDR' AS currency,
			sum (A.[distribution_price]) AS dpp,
			sum (A.[amount_vat]) AS tax,
			sum(A.[total_amount]) AS total_amount,
			
			CASE	WHEN [journal_clearing] is not null and [journal_clearing] != '' THEN 'InvStat01'
					WHEN due_date = CONVERT(DATE, GETDATE()) THEN 'InvStat04'
					WHEN due_date >= CONVERT(DATE, GETDATE()) THEN 'InvStat03'
					WHEN due_date <= CONVERT(DATE, GETDATE()) THEN 'InvStat02' /*  else 'Settlement'*/
					
			END AS ar_status,
			case when A.da_no like '%c%' then (select top 1 t.tax_no from tb_r_mvda_invoice t where t.da_no = a.da_cn_no)
			else A.[tax_no] end AS tax_invoice,
			B.pph_serial_no AS pph_no,
			'' as da_code,
			A.da_no as da_no,
			m.customer_code,
			m.customer_name,
			m.customer_group,
			'1.00' as exc_rate,
			case when @totAmount is not null and @totAmount = 'Exclude' then
				0
			else
			B.amount_pph	
			end as amount_pph,
			A.inv_downloaded as inv_downloaded,
			--ef.ApprovalStatus as approvalstatus,
			A.notur_uploaded as notur_uploaded,
			case when A.notur_uploaded is null then 0 else 1 end as is_notur_upload_done,
			A.notur_downloaded as notur_downloaded,			
			A.downloaded_date as downloaded_date,
			A.downloaded_by as downloaded_by,
			CASE	WHEN A.inv_downloaded is null THEN 0
				ELSE 1 END AS is_inv_available,
				A.inv_scan_downloaded as inv_scan_downloaded,
				A.downloadedscan_date as downloadedscan_date,
				A.downloadedscan_by as downloadedscan_by,
				CASE	WHEN A.inv_scan_downloaded is null THEN 0
				ELSE 1 END AS is_inv_scan_available,
				B.rate_pph as rate_pph,
				'' as vat_dormitory,
				'' as dormitory,
				null as pi_downloaded,
				null as pi_downloaded_date,
				null as pi_downloaded_by,
				A.address,
			case when A.da_no not like '%c%' then 0 else 1 end as is_retur
		FROM [BES].[dbo].[tb_r_mvda_invoice] A
		LEFT JOIN [dbo].[tb_r_mvda_pph] B ON A.da_no = B.da_no
		left join (select distinct * from dbo.tb_m_customer_mapping c where c.business_unit_id = 1)m on m.customer_code = a.dealer_code
		LEFT JOIN [TAM_EFAKTUR].[dbo].[TB_R_VATOut] ef on A.tax_no = ef.NomorFakturGabungan and ef.ApprovalStatus = 'Approval Sukses'
		where A.do_date >= '2019-02-01' and
		--ef.ApprovalStatus != 'Reject'
		(
				(@customer_group != '' and m.customer_group = @customer_group)
				or 
				(@customer_group = '' and a.dealer_code is not null)
			)
		and
		(
				-- seperti request dari mas Sabid, 
				--Di menu Invoice, data yang ditampilkan adalah data DA dan CA berlaku untuk other, MV, SP
				(@invoice_type = 1 and (A.da_no not like '%c%' or A.da_no like '%c%'))	-- invoice
				or  
				(@invoice_type = 2 and A.da_no like '%c%' )	-- nota retur
				or
				(@invoice_type = 3 and  [journal_clearing] is not null and [journal_clearing] != '')
				or
				(@invoice_type = 0 and 1=1)
		)
		group by A.da_no, A.due_date, A.do_date,  A.journal_clearing, A.tax_no
		,B.pph_serial_no, m.customer_code, m.customer_group, m.customer_name, A.inv_downloaded, A.notur_downloaded, A.notur_uploaded,
		A.notur_downloaded, A.downloaded_by, A.downloaded_date, A.inv_scan_downloaded, A.downloadedscan_date, A.downloadedscan_by, B.amount_pph,
		B.rate_pph,A.address,a.da_cn_no

		UNION

		-- ## Spare Part START ##
		-- ## wo\dwi
		select --TOP 1000
		--Convert(varchar(1000), A.detail_id) as id,
		null as id,
		'Spare Part' as business_unit,
		3 as business_unit_id,
		A.[da_no] AS invoice_number,
		'SALES SP' AS transaction_type,
		2 AS transaction_type_id, -- ??
		A.DLV_DATE AS doc_date,
		A.TO_DUE_DATE AS due_date,
		'IDR' AS currency,
		A.TO_AMT AS dpp, --??
		A.VAT_AMT AS tax,
		A.TO_AMT + A.VAT_AMT AS total_amount,
			
		CASE	WHEN A.JOURNAL_CLEARING is not null and A.JOURNAL_CLEARING != '' THEN 'InvStat01'
				WHEN a.TO_DUE_DATE = CONVERT(DATE, GETDATE()) THEN 'InvStat04'
				WHEN a.TO_DUE_DATE >= CONVERT(DATE, GETDATE()) THEN 'InvStat03'
				WHEN a.TO_DUE_DATE <= CONVERT(DATE, GETDATE()) THEN 'InvStat02' /*  else 'Settlement'*/
					
		END AS ar_status,
		A.FAKTUR_PAJAK_NO tax_invoice,  -- debit
		'' AS pph_no, --?? pph nya dari mana?
		'' as da_code,
		'' as da_no,
		A.BILL_TO_CUST as customer_code,
		A.CUST_NAME, --m.customer_name,
		m.customer_group as customer_group, --m.customer_group,
		null as exc_rate,
		null as amount_pph, --??
		A.INV_DOWNLOAD as inv_downloaded,
		--ef.ApprovalStatus as approvalstatus,
		a.NOTUR_UPLOAD as notur_uploaded, --A.notur_uploaded as notur_uploaded,
		case when a.NOTUR_UPLOAD is null then 0 else 1 end as is_notur_upload_done,
		a.NOTUR_DOWNLOAD as notur_downloaded, --A.notur_downloaded as notur_downloaded,			
		A.INV_DOWNLOAD_DATE as downloaded_date,
		A.INV_DOWNLOAD_BY as downloaded_by,
		CASE	WHEN A.INV_DOWNLOAD is null THEN 0
			ELSE 1 END AS is_inv_available,
		'' inv_scan_downloaded,	--A.inv_scan_downloaded as inv_scan_downloaded,
		'' downloadedscan_date,	--A.downloadedscan_date as downloadedscan_date,
		'' downloadedscan_by,	--A.downloadedscan_by as downloadedscan_by,
		'' is_inv_scan_available,	--CASE	WHEN A.inv_scan_downloaded is null THEN 0
			--ELSE 1 END AS is_inv_scan_available,
			null as rate_pph, --?? pph32 sama pph sama atau tidak?
			'' as vat_dormitory, --?? ini apa?
			'' as dormitory, --?? ini apa?
			A.PI_DOWNLOAD as pi_downloaded,
			A.PI_DOWNLOAD_DATE as pi_downloaded_date,
			A.PI_DOWNLOAD_BY as pi_downloaded_by,
			A.CUST_ADDR1,
		0 as is_retur
	from tb_r_spda_invoice_header A 
	LEFT JOIN [dbo].tb_m_customer_mapping m on m.customer_code = A.BILL_TO_CUST and m.business_unit_id = 3
	where 1=1
		and A.DLV_DATE >= '2019-09-01'
		and
			(
				(@customer_group != '' and m.customer_group = @customer_group)
				or 
				(@customer_group = '' and a.BILL_TO_CUST is not null)
			)
		and
			(
					(@invoice_type = 1 and A.DR_CR = 'D' and a.DA_NO like 'FDA%')	-- invoice
					or
					(@invoice_type = 3 and A.JOURNAL_CLEARING is not null and A.JOURNAL_CLEARING != '')
					or
					(@invoice_type = 0 and 1=1)
			)

	union 
		-- credit sp
		select null as id,
				'Spare Part' as business_unit,
				3 as business_unit_id,
				A.CA_NO AS invoice_number,
				'SALES SP' AS transaction_type,
				2 AS transaction_type_id, -- ??
				A.DLV_DATE AS doc_date,
				A.TO_DUE_DATE AS due_date,
				'IDR' AS currency,
				SUM(A.TO_AMT) AS dpp, --??
				SUM(A.VAT_AMT) AS tax,
				SUM(A.TO_AMT + A.VAT_AMT) AS total_amount,
			
				CASE	WHEN A.JOURNAL_CLEARING is not null and A.JOURNAL_CLEARING != '' THEN 'InvStat01'
						WHEN a.TO_DUE_DATE = CONVERT(DATE, GETDATE()) THEN 'InvStat04'
						WHEN a.TO_DUE_DATE >= CONVERT(DATE, GETDATE()) THEN 'InvStat03'
						WHEN a.TO_DUE_DATE <= CONVERT(DATE, GETDATE()) THEN 'InvStat02' /*  else 'Settlement'*/
					
				END AS ar_status,
				STUFF ((SELECT DISTINCT '#' + RTRIM(LTRIM(h1.FAKTUR_PAJAK_NO))  FROM tb_r_spda_invoice_header h1
								WHERE A.CA_NO = h1.CA_NO and h1.DR_CR = 'C' -- credit
								FOR XML PATH('')), 1, 1, '') as tax_invoice,
				'' AS pph_no, --?? pph nya dari mana?
				'' as da_code,
				'' as da_no,
				A.BILL_TO_CUST as customer_code,
				A.CUST_NAME, --m.customer_name,
				m.customer_group as customer_group, --m.customer_group,
				null as exc_rate,
				null as amount_pph, --??
				A.INV_DOWNLOAD as inv_downloaded,
				--ef.ApprovalStatus as approvalstatus,
				a.NOTUR_UPLOAD as notur_uploaded, --A.notur_uploaded as notur_uploaded,
				case when a.NOTUR_UPLOAD is null then 0 else 1 end as is_notur_upload_done,
				a.NOTUR_DOWNLOAD as notur_downloaded, --A.notur_downloaded as notur_downloaded,			
				A.INV_DOWNLOAD_DATE as downloaded_date,
				A.INV_DOWNLOAD_BY as downloaded_by,
				CASE	WHEN A.INV_DOWNLOAD is null THEN 0
					ELSE 1 END AS is_inv_available,
				'' inv_scan_downloaded,	--A.inv_scan_downloaded as inv_scan_downloaded,
				'' downloadedscan_date,	--A.downloadedscan_date as downloadedscan_date,
				'' downloadedscan_by,	--A.downloadedscan_by as downloadedscan_by,
				'' is_inv_scan_available,	--CASE	WHEN A.inv_scan_downloaded is null THEN 0
					--ELSE 1 END AS is_inv_scan_available,
					null as rate_pph, --?? pph32 sama pph sama atau tidak?
					'' as vat_dormitory, --?? ini apa?
					'' as dormitory, --?? ini apa?
					A.PI_DOWNLOAD as pi_downloaded,
					A.PI_DOWNLOAD_DATE as pi_downloaded_date,
					A.PI_DOWNLOAD_BY as pi_downloaded_by,
					A.CUST_ADDR1,
				1 as is_retur
			from tb_r_spda_invoice_header A 
			LEFT JOIN [dbo].tb_m_customer_mapping m on m.customer_code = A.BILL_TO_CUST and m.business_unit_id = 3
			where 1=1
				and A.DLV_DATE >= '2019-09-01' -- TODO open to see all data
				and
					(
						(@customer_group != '' and m.customer_group = @customer_group)
						or 
						(@customer_group = '' and a.BILL_TO_CUST is not null)
					)
				and
					(
						((@invoice_type = 2 or @invoice_type = 1) and A.DR_CR = 'C' and a.CA_NO like 'FCA%' and a.DA_NO like 'FDA%')	-- invoice
						or
						(@invoice_type = 3 and A.JOURNAL_CLEARING is not null and A.JOURNAL_CLEARING != '')
						or
						(@invoice_type = 0 and 1=1)
					)
			group by 
				A.CA_NO
				,A.CUST_NAME,A.PPH23_AMT,A.PPH23_RATE, A.DLV_DATE, A.TO_DUE_DATE, A.BILL_TO_CUST
				,A.JOURNAL_CLEARING, A.INV_DOWNLOAD, A.INV_DOWNLOAD_DATE, A.INV_DOWNLOAD_BY, A.PI_DOWNLOAD,A.PI_DOWNLOAD_DATE
				,A.PI_DOWNLOAD_BY, a.NOTUR_UPLOAD, a.NOTUR_DOWNLOAD, A.CUST_ADDR1, m.customer_group
		-- ## Spare Part END ##
	)


	SELECT cte.*,
		arStat.config_text AS ar_status_name,
		arStat.config_description AS ar_status_img_url,
		t.PdfUrl AS tax_attc_url,
		CASE	WHEN t.PdfUrl is null THEN 0
				ELSE 1 END AS is_tax_attc_available
	FROM cte
	outer apply (
		select ar.config_text,ar.config_description from vw_AR_Status ar where cte.ar_status = ar.config_value
	) arStat
	--LEFT JOIN vw_AR_Status ar on cte.ar_status = ar.config_value
	--LEFT JOIN [TAM_EFAKTUR].[dbo].[TB_R_VATOut] ef on cte.tax_invoice = ef.NomorFakturGabungan and ApprovalStatus = 'Approval Sukses'
	outer apply (
		select ef.PdfUrl from [TAM_EFAKTUR].[dbo].[TB_R_VATOut] ef where cte.tax_invoice = ef.NomorFakturGabungan and ApprovalStatus = 'Approval Sukses'
	) t
	

)







GO

