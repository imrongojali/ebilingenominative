USE [BES]
GO
/****** Object:  UserDefinedFunction [dbo].[udf_ReportNotaRetur]    Script Date: 11/6/2019 10:15:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		wo\dwi
-- Create date: 2019-11-06
-- Description:	<Description,,>
-- =============================================
ALTER FUNCTION [dbo].[udf_ReportNotaRetur]
(	
	@customer_group nvarchar(50) -- if '' = internal TAM
)
RETURNS TABLE 
AS
RETURN 
(
	-- mv
	select
		'FDA/MV/' + m.da_no as NomorNotaRetur, 
		m.do_date as TglRetur,
		'FDA/MV/' + m.da_no as NomorCreditAdvice, 
		(select top 1 t.tax_no from tb_r_mvda_invoice t where t.da_no = m.da_cn_no) as NomorFakturPajakRetur,
		'FDA/MV/' + m.da_cn_no as NomorDaRetur,
		(select top 1 t.do_date from tb_r_mvda_invoice t where t.da_no = m.da_cn_no) as TglFakturPajakRetur,
		m.npwp_no as CustomerNpwp,
		m.dealer_name as CustomerName,
		sum(m.distribution_price) as dpp,
		sum(m.amount_vat) as ppn,
		'Motor Vehicle' as businessUnit,
		--for filter only
		m.do_date as doc_date,
		'Motor Vehicle' as business_unit,
		0 as transaction_type_id,
		m.da_no as invoice_number,
		c.customer_group as customer_group,
		case when m.notur_uploaded is null then 0 else 1 end as is_notur_upload_done
	from tb_r_mvda_invoice m WITH (NOLOCK)
	left join (select DISTINCT * from dbo.tb_m_customer_mapping) c on c.customer_code = m.dealer_code and c.business_unit_id = 1 --data di customer mapping dev double2, jadi didisticnt
	where 1=1
		and (m.da_no like '%C%')
		and m.do_date >= '2019-02-01'
		and (
				(@customer_group != '' and c.customer_group = @customer_group)
				or 
				(@customer_group = '' and m.dealer_code is not null)
			)
	group by 
		m.da_no, m.do_date, m.da_cn_no, m.npwp_no, m.dealer_name, c.customer_group, m.notur_uploaded

	union all

	-- spare part nota retur
	select
		DISTINCT
		right(ltrim(rtrim(h.CA_NO)), 7) + '-' + right(ltrim(rtrim(h.FAKTUR_PAJAK_NO)), 8) as NomorNotaRetur, 
		h.DLV_DATE as TglRetur,
		h.CA_NO as NomorCreditAdvice, 
		ltrim(rtrim(h.FAKTUR_PAJAK_NO)) as NomorFakturPajakRetur,
		h.DA_NO as NomorDaRetur,
		d.DELIVERY_DATE as TglFakturPajakRetur,
		h.NPWP as CustomerNpwp,
		h.CUST_NAME as CustomerName,
		abs(h.TO_AMT) as dpp,
		abs(h.VAT_AMT) as ppn,
		'Spare Part' as businessUnit,
		--for filter only
		h.DLV_DATE as doc_date,
		'Spare Part' as business_unit,
		2 as transaction_type_id,
		h.CA_NO as invoice_number,
		m.customer_group as customer_group,
		case when h.NOTUR_UPLOAD is null then 0 else 1 end as is_notur_upload_done

	from tb_r_spda_invoice_header h WITH (NOLOCK)
	inner join tb_r_spda_invoice_detail d WITH (NOLOCK)
		on h.CA_NO = d.CA_NO
				and h.DA_NO = d.DA_NO
	LEFT JOIN [dbo].tb_m_customer_mapping m on m.customer_code = h.BILL_TO_CUST and m.business_unit_id = 3
	where 1=1
		and h.DLV_DATE >= '2019-09-01'
		and (h.DR_CR = 'C' and h.CA_NO like 'FCA%')
		and
			(
				(@customer_group != '' and m.customer_group = @customer_group)
				or 
				(@customer_group = '' and h.BILL_TO_CUST is not null)
			)

	union all

	-- others
	SELECT 
		b.retur_number as NomorNotaRetur, 
		b.retur_date as TglRetur,
		b.invoice_number as NomorCreditAdvice, 
		b.tax_number as NomorFakturPajakRetur,
		b.da_number as NomorDaRetur,
		b.postingdate as TglFakturPajakRetur,
		CASE WHEN b.[customer_others_id] IS NULL THEN cu.[npwp]
		  ELSE co.[npwp]
		END as CustomerNpwp,
		CASE WHEN b.[customer_others_id] IS NULL THEN cu.[name]
		  ELSE co.[name]
		END as CustomerName,
		c.price_invoice + c.dormitory as dpp,
		c.vat_dormitory + c.vat_invoice as ppn, 
		'Others' as businessUnit,
		--for filter only
		b.postingdate as doc_date, -- todo
		'Others' as business_unit,
		a.transaction_type_id as transaction_type_id,
		b.invoice_number as invoice_number,
		m.customer_group as customer_group,
		case when b.notur_uploaded is null then 0 else 1 end as is_notur_upload_done
	FROM [dbo].[tb_r_da] a WITH (NOLOCK)
			INNER JOIN [dbo].[tb_r_da_detil] b WITH (NOLOCK) ON a.da_code = b.da_code 
			INNER JOIN [dbo].[tb_r_da_detil_item] c WITH (NOLOCK) ON b.da_detil_code = c.da_detil_code
	LEFT JOIN [LS_COINS_TAM].[TAM].[dbo].[tb_m_customer] cu ON cu.[customer_code] = b.[customer_code]
	LEFT JOIN [tb_r_customer_others] co ON co.[customer_others_id] = b.[customer_others_id]
	--distinct because data from dev is duplicate,, don't know if in production are clear
	LEFT JOIN (select distinct * from tb_m_customer_mapping) m on m.customer_code = b.customer_code and m.business_unit_id = 2 
	where b.[row_status] = 0
		and (b.da_number is not null and b.da_number !='')
		and b.postingdate >= '2019-02-01'
		and b.retur_date is not null
		and
			(
				(@customer_group != '' and m.customer_group = @customer_group)
				or 
				(@customer_group = '' and b.customer_code is not null)
			)
)
