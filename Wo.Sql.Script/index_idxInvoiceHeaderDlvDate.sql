/*
Missing Index Details from SQLQuery5.sql - localhost\MSSQLSERVER2016.BES (sa)
The Query Processor estimates that implementing the following index could improve the query cost by 36.9455%.
*/


USE [BES]
GO
CREATE NONCLUSTERED INDEX idxInvoiceHeaderDlvDate
ON [dbo].[tb_r_spda_invoice_header] ([DLV_DATE])
INCLUDE ([CA_NO],[DA_NO],[DR_CR],[BILL_TO_CUST],[CUST_NAME],[CUST_ADDR1],[TO_AMT],[VAT_AMT],[PPH23_AMT],[TO_DUE_DATE],[PPH23_RATE],[JOURNAL_CLEARING],[INV_DOWNLOAD],[INV_DOWNLOAD_DATE],[INV_DOWNLOAD_BY],[PI_DOWNLOAD],[PI_DOWNLOAD_DATE],[PI_DOWNLOAD_BY],[NOTUR_DOWNLOAD],[NOTUR_UPLOAD])
GO

