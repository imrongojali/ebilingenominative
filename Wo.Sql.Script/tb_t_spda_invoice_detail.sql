USE [BES]
GO

/****** Object:  Table [dbo].[tb_t_spda_invoice_detail]    Script Date: 11/4/2019 9:37:58 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tb_t_spda_invoice_detail](
	[PI_NO] [varchar](6) NOT NULL,
	[PI_TYPE] [varchar](1) NOT NULL,
	[PI_DATE] [datetime] NOT NULL,
	[DELIVERY_DATE] [datetime] NULL,
	[ORDER_DATE] [datetime] NULL,
	[CUST_ORDER_NO] [varchar](8) NOT NULL,
	[ORDER_ITEM_NO] [varchar](4) NULL,
	[SHIP_PNO] [varchar](15) NULL,
	[PART_NO] [varchar](15) NOT NULL,
	[PICKING_QTY] [numeric](8, 0) NULL,
	[LANDED_COST] [numeric](17, 3) NULL,
	[RETAIL_PRICE] [numeric](17, 3) NULL,
	[SALES_PRICE] [numeric](17, 3) NULL,
	[TO_AMT] [numeric](17, 0) NULL,
	[DISC_RATE] [numeric](17, 3) NULL,
	[DISC_AMT] [numeric](17, 3) NULL,
	[DA_NO] [varchar](14) NULL,
	[ORI_DESTINATION_CODE] [varchar](7) NULL,
	[CUSTOMER_ORDER_NO] [varchar](8) NULL,
	[TOPAS_ORDER_TYPE] [varchar](1) NULL,
	[DELIVERY_NOTE_NO] [varchar](15) NULL,
	[DEST_CODE] [varchar](7) NULL,
	[DEST_GROUP_CODE] [varchar](7) NULL,
	[CUST_CODE] [varchar](7) NULL,
	[BILL_TO_CUST] [varchar](5) NULL,
	[CUST_GROUP_TYPE] [varchar](1) NULL,
	[VAT_RATE] [numeric](5, 2) NULL,
	[VAT_AMOUNT] [numeric](17, 2) NULL,
	[CREATED_DATE] [datetime] NULL,
	[CHANGED_DATE] [datetime] NULL,
	[PI_FLAG] [varchar](1) NULL,
	[PAYMENT_CODE] [varchar](2) NULL,
	[DEPOT_CODE] [varchar](2) NULL,
	[REMAKS] [varchar](30) NULL,
	[TRIP_REFF_NO] [varchar](10) NULL,
	[SHIP_CASE_NO] [varchar](8) NOT NULL,
	[SHIP_DOC_NO] [varchar](7) NULL,
	[CASE_OUTER_LENGTH] [numeric](5, 0) NULL,
	[CASE_OUTER_WIDTH] [numeric](5, 0) NULL,
	[CASE_OUTER_GROSS_HEIGHT] [numeric](5, 0) NULL,
	[CASE_GROSS_WEIGHT] [numeric](8, 2) NULL,
	[VOLUME_WEIGHT] [numeric](8, 2) NULL,
	[MAX_VOLUME_WEIGHT] [numeric](8, 2) NULL,
	[MAKER_CODE] [varchar](1) NULL,
	[TRUCK_NO] [varchar](4) NULL,
	[SHIP_AGENT] [varchar](5) NULL,
	[SHIP_AGENT_NAME] [varchar](20) NULL,
	[ROUTE_NAME] [varchar](30) NULL,
	[UOM] [varchar](5) NULL,
	[PRICE_COST] [numeric](15, 2) NULL,
	[PENALTY_RATE] [numeric](5, 2) NULL,
	[PENALTY_AMOUNT] [numeric](17, 2) NULL,
	[PPH23_RATE] [numeric](5, 2) NULL,
	[PPH23_AMOUNT] [numeric](17, 2) NULL,
	[CA_NO] [varchar](14) NULL,
	[PI_PROCESS_FLAG] [varchar](50) NULL,
	[CURRENCY_CODE] [varchar](3) NULL,
	[DANGER_GOOD_CLASS] [varchar](4) NULL,
	[CASE_PACKAGE_TYPE] [varchar](30) NULL,
	[HS_NO] [varchar](15) NULL,
	[COUNTRY_OF_ORIGIN_CODE] [varchar](3) NULL,
	[TRANSPORT_CODE] [varchar](1) NULL,
	[CONTAINER_NO] [varchar](12) NULL,
	[CONTAINER_TYPE] [varchar](1) NULL,
	[CASE_NET_WEIGHT] [varchar](8) NULL,
	[CASE_MEASUREMENT] [varchar](7) NULL,
	[VOLUME] [numeric](8, 2) NULL,
	[DOMESTIC_OVERSEAS_CODE] [char](1) NULL,
	[LC_AMOUNT] [numeric](18, 0) NULL,
	[SEAL_NO] [varchar](10) NULL,
 CONSTRAINT [PK_TB_T_SPDA_INVOICE_DETAIL] PRIMARY KEY CLUSTERED 
(
	[PI_NO] ASC,
	[PI_TYPE] ASC,
	[PI_DATE] ASC,
	[CUST_ORDER_NO] ASC,
	[PART_NO] ASC,
	[SHIP_CASE_NO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


