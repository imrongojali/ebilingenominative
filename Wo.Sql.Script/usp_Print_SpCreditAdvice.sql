USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[usp_Print_DebitAdvice]    Script Date: 9/18/2019 2:47:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		wo\dwi
-- Create date: 2019-09-25
-- Description:	Print Credit Advice
-- =============================================
ALTER PROCEDURE [dbo].[usp_Print_SpCreditAdvice] 
	-- Add the parameters for the stored procedure here
	 @ca_number varchar(20) =''
	,@user varchar(50)=''
	,@type int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--select 
	--	h.CA_NO as CaNo,
	--	h.DLV_DATE as DeliveryDate,
	--	h.CUST_NAME as CustomerName,
	--	h.CUST_ADDR1 as CustomerAddress,
	--	h.FAKTUR_PAJAK_NO as FakturPajakNo,
	--	h.TO_AMT as TurnOverAmount,
	--	(h.VAT_AMT * -1) as Vat10,
	--	h.TO_AMT + (h.VAT_AMT * -1) as Total,
	--	h.TO_DUE_DATE as DueDateTo,
	--	h.VAT_DUE_DATE as DueDateVat,
	--	h.COLLECT_DATE as CollectionDate
	--from tb_r_spda_invoice_header h
	--where DR_CR = 'C' --credit advice
	--	and (nullif(@ca_number, '') is null or h.CA_NO = @ca_number)


	select 
		h.CA_NO as CaNo,
		h.DLV_DATE as DeliveryDate,
		h.CUST_NAME as CustomerName,
		h.CUST_ADDR1 as CustomerAddress,
		--h.FAKTUR_PAJAK_NO as FakturPajakNo,
		sum(h.TO_AMT) as TurnOverAmount,
		sum((h.VAT_AMT)) as Vat10,
		sum(h.TO_AMT + (h.VAT_AMT)) as Total,
		h.TO_DUE_DATE as DueDateTo,
		h.VAT_DUE_DATE as DueDateVat,
		h.COLLECT_DATE as CollectionDate
	from tb_r_spda_invoice_header h
	where DR_CR = 'C' --credit advice
		and (nullif(@ca_number, '') is null or h.CA_NO = @ca_number)
	group by 
		h.CA_NO,
		h.DLV_DATE,
		h.CUST_NAME,
		h.CUST_ADDR1,
		--h.FAKTUR_PAJAK_NO,
		h.TO_DUE_DATE,
		h.VAT_DUE_DATE ,
		h.COLLECT_DATE
END

