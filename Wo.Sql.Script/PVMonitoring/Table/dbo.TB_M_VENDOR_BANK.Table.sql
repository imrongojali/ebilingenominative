USE [BES]
GO
/****** Object:  Table [dbo].[TB_M_VENDOR_BANK]    Script Date: 6/29/2020 10:47:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TB_M_VENDOR_BANK](
	[vendor_cd] [varchar](10) NOT NULL,
	[ctry] [varchar](3) NOT NULL,
	[bank_key] [varchar](15) NOT NULL,
	[bank_account] [varchar](20) NOT NULL,
	[bank_type] [varchar](4) NULL,
	[acct_holder] [varchar](60) NULL,
	[name_of_bank] [varchar](60) NOT NULL,
	[currency] [varchar](30) NULL,
	[status_req] [varchar](1) NULL,
	[created_by] [varchar](50) NULL,
	[created_dt] [datetime] NULL,
	[beneficiaries] [varchar](60) NULL,
	[directory] [varchar](75) NULL,
	[file_name] [varchar](75) NULL,
	[guid_filename] [varchar](75) NULL,
 CONSTRAINT [PK_TB_M_VENDOR_BANK] PRIMARY KEY CLUSTERED 
(
	[vendor_cd] ASC,
	[ctry] ASC,
	[bank_key] ASC,
	[bank_account] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
