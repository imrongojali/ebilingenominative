USE [BES]
GO
/****** Object:  Table [dbo].[TB_R_LOG_D]    Script Date: 6/29/2020 10:47:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TB_R_LOG_D](
	[tb_r_log_d_id] [int] IDENTITY(1,1) NOT NULL,
	[process_id] [int] NOT NULL,
	[seq_no] [int] NOT NULL,
	[msg_id] [varchar](12) NOT NULL,
	[msg_type] [varchar](3) NOT NULL,
	[err_location] [varchar](8000) NOT NULL,
	[err_message] [varchar](max) NOT NULL,
	[err_dt] [datetime] NOT NULL,
 CONSTRAINT [PK_TB_R_LOG_D] PRIMARY KEY CLUSTERED 
(
	[tb_r_log_d_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[TB_R_LOG_D]  WITH CHECK ADD  CONSTRAINT [FK_TB_R_LOG_D_PID] FOREIGN KEY([process_id])
REFERENCES [dbo].[TB_R_LOG_H] ([process_id])
GO
ALTER TABLE [dbo].[TB_R_LOG_D] CHECK CONSTRAINT [FK_TB_R_LOG_D_PID]
GO
