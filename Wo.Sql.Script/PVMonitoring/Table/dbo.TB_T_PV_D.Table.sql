USE [BES]
GO
/****** Object:  Table [dbo].[TB_T_PV_D]    Script Date: 6/29/2020 10:47:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TB_T_PV_D](
	[PROCESS_ID] [int] NOT NULL,
	[SEQ_NO] [int] NOT NULL,
	[COST_CENTER_CD] [varchar](10) NULL,
	[CURRENCY_CD] [varchar](3) NULL,
	[DESCRIPTION] [varchar](50) NULL,
	[AMOUNT] [numeric](16, 2) NULL,
	[WBS_NO] [varchar](24) NULL,
	[INVOICE_NO] [varchar](16) NULL,
	[INVOICE_DATE] [datetime] NULL,
	[ITEM_TRANSACTION_CD] [int] NULL,
	[TAX_NO] [varchar](25) NULL,
	[GL_ACCOUNT] [int] NULL,
	[TAX_CD] [varchar](2) NULL,
	[ITEM_NO] [int] NULL,
	[DPP_AMOUNT] [numeric](16, 2) NULL,
	[TAX_ASSIGNMENT] [varchar](18) NULL,
	[ACC_INF_ASSIGNMENT] [varchar](18) NULL,
	[TAX_DT] [date] NULL,
	[USEQ] [int] NULL,
	[ERRMSG] [varchar](max) NULL,
	[CREATED_DT] [datetime] NULL,
	[CREATED_BY] [varchar](20) NULL,
	[CHANGED_DT] [datetime] NULL,
	[CHANGED_BY] [varchar](20) NULL,
	[WHT_TAX_CODE] [varchar](10) NULL,
	[WHT_TAX_TARIFF] [decimal](16, 4) NULL,
	[WHT_TAX_ADDITIONAL_INFO] [varchar](500) NULL,
	[WHT_DPP_PPh_AMOUNT] [numeric](16, 2) NULL,
 CONSTRAINT [PK_TB_T_PV_D] PRIMARY KEY CLUSTERED 
(
	[PROCESS_ID] ASC,
	[SEQ_NO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
