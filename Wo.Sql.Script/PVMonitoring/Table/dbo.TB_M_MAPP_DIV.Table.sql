USE [BES]
GO
/****** Object:  Table [dbo].[TB_M_MAPP_DIV]    Script Date: 6/29/2020 10:47:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TB_M_MAPP_DIV](
	[DIV_ID] [int] NOT NULL,
	[DEPARTMENT_ID] [int] NULL,
	[CREATED_BY] [varchar](20) NOT NULL,
	[CREATED_DT] [datetime] NOT NULL,
	[CHANGED_BY] [varchar](20) NULL,
	[CHANGED_DT] [datetime] NULL,
	[username] [varchar](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
