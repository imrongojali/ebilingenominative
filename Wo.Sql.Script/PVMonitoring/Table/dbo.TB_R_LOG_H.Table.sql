USE [BES]
GO
/****** Object:  Table [dbo].[TB_R_LOG_H]    Script Date: 6/29/2020 10:47:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TB_R_LOG_H](
	[process_id] [int] IDENTITY(1,1) NOT NULL,
	[process_dt] [datetime] NOT NULL,
	[function_id] [varchar](10) NOT NULL,
	[process_sts] [varchar](1) NOT NULL,
	[user_id] [varchar](20) NOT NULL,
	[end_dt] [datetime] NOT NULL,
	[remarks] [varchar](200) NULL,
 CONSTRAINT [PK_TB_R_LOG_H] PRIMARY KEY CLUSTERED 
(
	[process_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
