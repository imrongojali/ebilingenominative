USE [BES]
GO
/****** Object:  Table [dbo].[TB_T_UPLOAD_D]    Script Date: 6/29/2020 10:47:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TB_T_UPLOAD_D](
	[PROCESS_ID] [int] NOT NULL,
	[SEQ_NO] [int] NOT NULL,
	[INVOICE_NO] [varchar](16) NULL,
	[INVOICE_DATE] [varchar](25) NULL,
	[COST_CENTER_CD] [varchar](10) NULL,
	[CURRENCY_CD] [varchar](3) NULL,
	[AMOUNT] [numeric](18, 2) NULL,
	[DESCRIPTION] [varchar](50) NULL,
	[OK] [bit] NULL CONSTRAINT [DF__TB_T_UPLOAD___OK__2F2FFC0C]  DEFAULT ((0)),
	[GL_ACCOUNT] [int] NULL,
	[DPP_AMOUNT] [numeric](18, 2) NULL,
	[ACC_INF_ASSIGNMENT] [varchar](18) NULL,
	[TAX_NO] [varchar](25) NULL,
	[TAX_DT] [varchar](25) NULL,
	[AMOUNT_TURN_OVER] [numeric](18, 2) NULL,
	[AMOUNT_PPN] [numeric](18, 2) NULL,
	[AMOUNT_SEAL] [numeric](18, 2) NULL,
	[AMOUNT_PPh21_INTERN] [numeric](18, 2) NULL,
	[AMOUNT_PPh21_EXTERN] [numeric](18, 2) NULL,
	[AMOUNT_PPh26] [numeric](18, 2) NULL,
	[AMOUNT_PPh_FINAL] [numeric](18, 2) NULL,
	[AMOUNT_BM] [numeric](18, 2) NULL,
	[AMOUNT_PPN_IMPORT] [numeric](18, 2) NULL,
	[AMOUNT_PPnBM] [numeric](18, 2) NULL,
	[AMOUNT_PPh22] [numeric](18, 2) NULL,
	[AMOUNT_PnPB] [numeric](18, 2) NULL,
	[AMOUNT_BANK_CHARGE] [numeric](18, 2) NULL,
	[ERRMSG] [varchar](max) NULL,
	[CREATED_BY] [varchar](20) NULL,
	[CREATED_DT] [datetime] NULL,
	[CHANGED_BY] [varchar](20) NULL,
	[CHANGED_DT] [datetime] NULL,
	[AMOUNT_SALES_OTHER] [numeric](18, 2) NULL,
	[AMOUNT_RENTAL_EXPENSE] [numeric](18, 2) NULL,
	[AMOUNT_TRANSPORT_OPERATIONAL] [numeric](18, 2) NULL,
	[AMOUNT_TRANSPORT_TOLL] [numeric](18, 2) NULL,
	[AMOUNT_CONSUMPTION] [numeric](18, 2) NULL,
	[AMOUNT_MEALS_PAPER] [numeric](18, 2) NULL,
	[AMOUNT_OFFICE_SUPPLIES] [numeric](18, 2) NULL,
	[AMOUNT_COMMUNICATION] [numeric](18, 2) NULL,
	[AMOUNT_NEWSPAPER] [numeric](18, 2) NULL,
	[AMOUNT_REPAIR] [numeric](18, 2) NULL,
	[AMOUNT_OTHER] [numeric](18, 2) NULL,
	[DPP_PPh_AMOUNT] [numeric](18, 2) NULL,
	[TAX_CODE_PPh23] [varchar](10) NULL,
	[TAX_TARIFF_PPh23] [numeric](16, 4) NULL,
	[TAX_CODE_PPh21_EXTERN] [varchar](10) NULL,
	[TAX_DPP_ACCUMULATION_PPH21_EXTERN] [numeric](18, 2) NULL,
	[TAX_TARIFF_PPh21_EXTERN] [numeric](16, 4) NULL,
	[NIK_PPh21_EXTERN] [varchar](50) NULL,
	[TAX_CODE_PPh26] [varchar](10) NULL,
	[TAX_TARIFF_PPh26] [numeric](16, 4) NULL,
	[TAX_CODE_PPh_FINAL] [varchar](10) NULL,
	[TAX_TARIFF_PPh_FINAL] [numeric](16, 4) NULL,
	[INFO_PPh_FINAL] [varchar](500) NULL,
	[NPWP_Available] [varchar](50) NULL,
	[attachment_uploaded] [varchar](1) NULL,
	[seq_xcl] [int] NULL,
	[account_info] [varchar](16) NULL,
 CONSTRAINT [PK_TB_T_UPLOAD_D] PRIMARY KEY CLUSTERED 
(
	[PROCESS_ID] ASC,
	[SEQ_NO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
