USE [BES]
GO
/****** Object:  Table [dbo].[TB_R_PV_MONITORING_ATTACHMENT]    Script Date: 6/29/2020 10:47:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TB_R_PV_MONITORING_ATTACHMENT](
	[REFERENCE_NO] [varchar](34) NOT NULL,
	[REF_SEQ_NO] [numeric](4, 0) NOT NULL,
	[ATTACH_CD] [varchar](2) NOT NULL,
	[DESCRIPTION] [varchar](100) NOT NULL,
	[DIRECTORY] [varchar](75) NOT NULL,
	[FILE_NAME] [varchar](75) NOT NULL,
	[CREATED_BY] [varchar](20) NOT NULL,
	[CREATED_DT] [datetime] NOT NULL,
	[CHANGED_BY] [varchar](20) NULL,
	[CHANGED_DT] [datetime] NULL,
	[by_internal] [varchar](1) NULL,
 CONSTRAINT [PK_TB_R_PV_MONITORING_ATTACHMENT] PRIMARY KEY CLUSTERED 
(
	[REFERENCE_NO] ASC,
	[REF_SEQ_NO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
