USE [BES]
GO
/****** Object:  Table [dbo].[TB_T_UPLOAD_H]    Script Date: 6/29/2020 10:47:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TB_T_UPLOAD_H](
	[PROCESS_ID] [int] NOT NULL,
	[TRANSACTION_CD] [int] NULL,
	[VENDOR_CD] [varchar](10) NULL,
	[UPLOAD_FILENAME] [varchar](512) NULL,
	[REFF_NO] [numeric](20, 0) NULL,
	[ROWS_COUNT] [int] NULL,
	[OK] [bit] NULL DEFAULT ((0)),
	[CREATED_BY] [varchar](20) NULL,
	[CREATED_DT] [datetime] NULL,
	[CHANGED_BY] [varchar](20) NULL,
	[CHANGED_DT] [datetime] NULL,
	[bank_key] [varchar](15) NULL,
	[bank_account] [varchar](20) NULL,
	[pv_no] [int] NULL,
	[status_budget] [varchar](50) NULL,
	[bank_type] [int] NULL,
	[rejected_reason] [text] NULL,
	[rejected_by] [varchar](20) NULL,
	[rejected_dt] [datetime] NULL,
	[budget_no] [varchar](24) NULL,
 CONSTRAINT [PK_TB_T_UPLOAD_H] PRIMARY KEY CLUSTERED 
(
	[PROCESS_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
