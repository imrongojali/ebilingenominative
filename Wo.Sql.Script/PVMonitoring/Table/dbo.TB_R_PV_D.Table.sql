USE [BES]
GO
/****** Object:  Table [dbo].[TB_R_PV_D]    Script Date: 6/29/2020 10:47:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TB_R_PV_D](
	[SEQ_NO] [int] NOT NULL,
	[PV_NO] [int] NOT NULL,
	[PV_YEAR] [int] NOT NULL,
	[COST_CENTER_CD] [varchar](10) NULL,
	[CURRENCY_CD] [varchar](3) NOT NULL,
	[DESCRIPTION] [varchar](50) NULL,
	[AMOUNT] [numeric](16, 2) NOT NULL,
	[WBS_NO] [varchar](24) NULL,
	[INVOICE_NO] [varchar](16) NULL,
	[INVOICE_DATE] [datetime] NULL,
	[ITEM_TRANSACTION_CD] [int] NULL,
	[TAX_NO] [varchar](25) NULL,
	[GL_ACCOUNT] [int] NULL,
	[TAX_CD] [varchar](2) NULL,
	[FROM_CURR] [varchar](3) NULL,
	[FROM_SEQ] [int] NULL,
	[ITEM_NO] [int] NULL,
	[DPP_AMOUNT] [numeric](16, 2) NULL,
	[TAX_ASSIGNMENT] [varchar](18) NULL,
	[ACC_INF_ASSIGNMENT] [varchar](18) NULL,
	[TAX_DT] [date] NULL,
	[CREATED_DT] [datetime] NOT NULL,
	[CREATED_BY] [varchar](20) NOT NULL,
	[CHANGED_DT] [datetime] NULL,
	[CHANGED_BY] [varchar](20) NULL,
	[WHT_TAX_CODE] [varchar](10) NULL,
	[WHT_TAX_TARIFF] [decimal](16, 4) NULL,
	[WHT_TAX_ADDITIONAL_INFO] [varchar](500) NULL,
	[WHT_DPP_PPh_AMOUNT] [numeric](16, 2) NULL,
 CONSTRAINT [PK_TB_R_PV_D] PRIMARY KEY CLUSTERED 
(
	[SEQ_NO] ASC,
	[PV_NO] ASC,
	[PV_YEAR] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
