USE [BES]
GO

/****** Object:  Table [dbo].[tb_t_udf_invoice]    Script Date: 7/6/2020 02:01:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tb_t_udf_invoice](
	[id] [varchar](max) NULL,
	[business_unit] [varchar](max) NULL,
	[business_unit_id] [int] NULL,
	[invoice_number] [varchar](max) NULL,
	[transaction_type] [nvarchar](max) NULL,
	[transaction_type_id] [int] NULL,
	[doc_date] [datetime] NULL,
	[due_date] [datetime] NULL,
	[currency] [char](3) NULL,
	[dpp] [money] NULL,
	[tax] [money] NULL,
	[total_amount] [money] NULL,
	[ar_status] [varchar](max) NULL,
	[tax_invoice] [varchar](max) NULL,
	[pph_no] [varchar](max) NULL,
	[da_code] [varchar](max) NULL,
	[da_no] [varchar](max) NULL,
	[customer_code] [varchar](max) NULL,
	[customer_name] [nvarchar](max) NULL,
	[customer_group] [nvarchar](max) NULL,
	[exc_rate] [money] NULL,
	[amount_pph] [money] NULL,
	[inv_downloaded] [bit] NULL,
	[notur_uploaded] [bit] NULL,
	[is_notur_upload_done] [bit] NULL,
	[notur_downloaded] [bit] NULL,
	[downloaded_date] [date] NULL,
	[downloaded_by] [varchar](max) NULL,
	[is_inv_available] [bit] NULL,
	[inv_scan_downloaded] [bit] NULL,
	[downloadedscan_date] [date] NULL,
	[downloadedscan_by] [varchar](max) NULL,
	[is_inv_scan_available] [bit] NULL,
	[rate_pph] [decimal](5, 2) NULL,
	[vat_dormitory] [money] NULL,
	[dormitory] [money] NULL,
	[pi_downloaded] [bit] NULL,
	[pi_downloaded_date] [date] NULL,
	[pi_downloaded_by] [varchar](max) NULL,
	[customer_address] [nvarchar](max) NULL,
	[is_retur] [bit] NULL,
	[ar_status_name] [nvarchar](max) NULL,
	[ar_status_img_url] [nvarchar](max) NULL,
	[tax_attc_url] [varchar](max) NULL,
	[is_tax_attc_available] [bit] NULL,
	[created_date] [datetime] NULL,
	[created_by] [varchar](50) NULL,
	[modified_date] [datetime] NULL,
	[modified_by] [varchar](50) NULL,
	[concatIdx] [nvarchar](850) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

