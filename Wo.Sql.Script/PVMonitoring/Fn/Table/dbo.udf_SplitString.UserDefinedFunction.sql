USE [BES]
GO
/****** Object:  UserDefinedFunction [dbo].[udf_SplitString]    Script Date: 6/2/2020 10:16:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE Function [dbo].[udf_SplitString] 
(
    @Value Varchar(max),
	@Separator varchar(1) = ';'
)
RETURNS @Table TABLE (ColumnData VARCHAR(100))
AS
BEGIN
    IF RIGHT(@Value, 1) <> @Separator
    SELECT @Value = @Value + @Separator

    DECLARE @Pos    BIGINT,
            @OldPos BIGINT
    SELECT  @Pos    = 1,
            @OldPos = 1

    WHILE   @Pos < LEN(@Value)
        BEGIN
            SELECT  @Pos = CHARINDEX(@Separator, @Value, @OldPos)
            INSERT INTO @Table
            SELECT  LTRIM(RTRIM(SUBSTRING(@Value, @OldPos, @Pos - @OldPos))) Col001

            SELECT  @OldPos = @Pos + 1
        END

    RETURN
END



GO
