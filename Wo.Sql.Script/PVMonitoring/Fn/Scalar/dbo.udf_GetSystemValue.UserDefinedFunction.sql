USE [BES]
GO
/****** Object:  UserDefinedFunction [dbo].[udf_GetSystemValue]    Script Date: 6/2/2020 10:16:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[udf_GetSystemValue](
	@STYPE VARCHAR(50), @SCODE VARCHAR(50) 
) 
RETURNS VARCHAR(2000)
BEGIN
   RETURN (
	   SELECT ISNULL(Y.V, x.z) V 
		   FROM 
		   (SELECT NULL AS z) x 
		   LEFT JOIN
		   (SELECT TOP 1 S.SYSTEM_VALUE_TXT V
		             FROM vw_TbMSystemElvis S 
					WHERE S.SYSTEM_CD   = @SCODE
					  AND S.SYSTEM_TYPE = @STYPE
			) Y  ON 1 = 1
   )
	
END;

GO
