USE [BES]
GO
/****** Object:  UserDefinedFunction [dbo].[udf_ValidCostCenter]    Script Date: 6/2/2020 10:16:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[udf_ValidCostCenter](@costCenterCd VARCHAR(10), @glAccount INT) 
returns VARCHAR(10) 
AS 
BEGIN
	DECLARE   @r VARCHAR(10)
			, @x VARCHAR(20);
	
	SET @x = CONVERT(VARCHAR(20), @glAccount); 
	
	IF (@x IS NOT NULL) AND (LEN(@x) > 1)
		AND (SUBSTRING(@x, 1,1) in ('5','6','7'))
		SET @r = @costCenterCd; 
	ELSE 
		SET @r = NULL;
	
	RETURN @r; 	
END; 


GO
