USE [BES]
GO
/****** Object:  UserDefinedFunction [dbo].[udf_DtImport]    Script Date: 6/2/2020 10:16:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[udf_DtImport](@dt varchar(25))
RETURNS DATETIME 
AS 
BEGIN 
	DECLARE @d AS DATETIME = NULL, @x VARCHAR(25) = '';
	IF (@dt IS NOT NULL) 
	BEGIN
		IF (PATINDEX('[0-9][0-9][\-\.\/][0-9][0-9][\-\.\/][0-9][0-9][0-9][0-9]%', @dt) = 1)
			AND (LEN(@dt) >= 10)
		BEGIN 
			-- 1234567890123456789012345
			-- dd-mm-yyyy HH:MM:SS.ZZZ
			IF (LEN(@dt) <= 12)
			BEGIN
				SET @x = SUBSTRING (@dt, 7, 4) + SUBSTRING(@dt, 4, 2) + SUBSTRING(@dt, 1,2);
				IF (ISDATE(@x) = 1) 
					SET @d = CONVERT(datetime, @x, 112);
			END
			ELSE
			BEGIN
				DECLARE @h VARCHAR(2) = '00'
					  , @m VARCHAR(2) = '00'
					  , @s VARCHAR(2) = '00'
					  , @z VARCHAR(2) = '000';
				
				
			    if (LEN(@dt)>=23) 
					SET @z = SUBSTRING(@dt, 21, 3);
				if (LEN(@dt)>=18) 
					SET @s = SUBSTRING(@dt, 18, 2);
				if (LEN(@dt)>=15) 
					SET @m = SUBSTRING(@dt, 15, 2);
				if (LEN(@dt)>=12) 
					SET @h = SUBSTRING(@dt, 12, 2);
				
				SET @x = SUBSTRING(@dt, 7, 4) + '-' + SUBSTRING(@dt, 4, 2) + '-' + SUBSTRING(@dt, 1, 2) + ' ' 
					 + @h + ':' + @m + ':' + @s + ':' + @z; 
				IF (ISDATE(@x) = 1) 
					SET @d = CONVERT(datetime, @x, 121);
			END; 
		END;
	END;
	
	RETURN @d;
	
	--- test scripts [[
	--declare @dt varchar(25) = '28.10.2013';
	--declare @dt1 varchar(25) = '28.10.2013 23:30';
	--declare @dt2 varchar(25) = '28.10.2013 12:34:50';
	--declare @dt3 varchar(25) = '28.10.2013 23:45:06.700';
	 
	-- select 
	--	dbo.fn_DtImport(@dt) z0, 
	--	dbo.fn_DtImport(@dt1) z1, 
	--	dbo.fn_DtImport(@dt2) z2, 
	--	dbo.fn_DtImport(@dt3) z3 
	
	---- must yield : 
	---- 2013-10-28 00:00:00.000	2013-10-28 23:30:00.000	2013-10-28 12:34:50.000	2013-10-28 23:45:06.070

	-- test scripts  ]] 
END;

GO
