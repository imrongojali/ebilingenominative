USE [BES]
GO
/****** Object:  UserDefinedFunction [dbo].[udf_ConcatName]    Script Date: 6/2/2020 10:16:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[udf_ConcatName](
	@first varchar(50), 
	@last varchar(50))
RETURNS VARCHAR(100) 
AS
BEGIN
 	DECLARE @name VARCHAR(40);
	
	SELECT TOP 1 
		@name = 
		(ISNULL(LTRIM(RTRIM(@first)),'')
		+ CASE
			  WHEN @first IS NOT NULL THEN ' ' 
			  ELSE ''
		  END)
		+ (ISNULL(@last, ''));
	
	RETURN COALESCE(LTRIM(RTRIM(@name)),'');
END;

GO
