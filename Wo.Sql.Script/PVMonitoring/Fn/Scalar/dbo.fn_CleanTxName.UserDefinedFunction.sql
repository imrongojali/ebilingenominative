USE [BES]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CleanTxName]    Script Date: 6/2/2020 10:16:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fn_CleanTxName](
	@n VARCHAR(100)
)
RETURNS VARCHAR(100)
-- returns int
AS
BEGIN
	DECLARE @r VARCHAR(100);

	DECLARE @i INT;
	SET @i = CHARINDEX('(', @n);

	IF (@i > 0)
	BEGIN
		SET @r = SUBSTRING(@n, 1, @i -1);
	END
	ELSE
	BEGIN
		SET @r = @n;

	END;

	RETURN @r;
END;

GO
