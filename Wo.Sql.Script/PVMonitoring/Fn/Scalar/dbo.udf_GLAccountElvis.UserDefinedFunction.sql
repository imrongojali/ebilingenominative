USE [BES]
GO
/****** Object:  UserDefinedFunction [dbo].[udf_GLAccountElvis]    Script Date: 6/2/2020 10:16:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[udf_GLAccountElvis]
(
    @CostCenterCd varchar(10),
    @TransactionCd INT,
    @ItemTrans INT
)
RETURNS INT
AS
BEGIN
    DECLARE @tn VARCHAR(100);
    DECLARE @tname VARCHAR(100);
    DECLARE @gl INT;
    DECLARE @tx INT;
    DECLARE @txCount INT;
    DECLARE @item INT;
    IF @ItemTrans IS NULL
    BEGIN
        SET @item = 1;
    END
    ELSE
    BEGIN
        SET @item = @ItemTrans;
    END;
    SET @CostCenterCd = NULLIF(@CostCenterCd, '');
    SET @tx = @TransactionCd;
    SET @gl= NULL;
	
    IF (@CostCenterCd IS NOT NULL)
    BEGIN
        DECLARE @prod INT;
        DECLARE @isUPLOAD INT;
        SELECT TOP 1 @prod = PRODUCTION_FLAG
        FROM dbo.vw_CostCenterElvis
        WHERE LTRIM(RTRIM(COST_CENTER)) = LTRIM(RTRIM(@CostCenterCd));

        SELECT TOP 1 @tn = LOWER(TRANSACTION_NAME)
        FROM dbo.vw_TransactionTypeElvis
        WHERE TRANSACTION_CD = @TransactionCd;
        SET @isUPLOAD = CHARINDEX('(upload)', @tn);
        IF ( @isUPLOAD > 0 )
        BEGIN
            SET @tn = dbo.fn_CleanTxName(@tn);
        END;


        IF (@prod = 1)
        BEGIN
            SET @tname = @tn + '%(PRD)';
        END
        ELSE
        BEGIN
            SET @tname = @tn + '%(Non%PRD)';
        END;
        SET @tname = LOWER(@tname);

        SELECT @txCount = COUNT(1)
        FROM TB_M_TRANSACTION_TYPE
        WHERE LOWER(TRANSACTION_NAME) LIKE @tname;

        IF (@txCount > 0)
        BEGIN
            SELECT TOP 1 @tx = TRANSACTION_CD
            FROM dbo.vw_TransactionTypeElvis
            WHERE LOWER(TRANSACTION_NAME) LIKE @tname;
        END
        ELSE
        BEGIN
            IF (@isUPLOAD < 1)
            BEGIN
                SET @tx = @TransactionCd
            END
            ELSE
            BEGIN
                SET @tx = -1;
            END;
        END;
    END;

    SELECT TOP 1 @gl = GL_ACCOUNT
    FROM dbo.vw_GLAccountMappingElvis
    WHERE TRANSACTION_CD = @tx
    AND ITEM_TRANSACTION_CD = @item;

    RETURN @gl;
END;



GO
