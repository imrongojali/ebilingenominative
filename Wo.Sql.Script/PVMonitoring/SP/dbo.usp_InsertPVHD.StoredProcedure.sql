USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertPVHD]    Script Date: 6/29/2020 10:20:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_InsertPVHD]
	-- Add the parameters for the stored procedure here
	@pid int, 
	@uid varchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	declare @pv int, 
			@vendor_cd varchar(10),
			@transaction_cd  int,
		    @dateNow date = cast(getdate() as date), 
		    @totalAmount numeric(16,2), 
		    @vendorGroup int, 
		    @vendorDesc varchar(225),
		    @currCd varchar(3),
		    @divId int,
			@budget_no varchar(24);

    -- Insert statements for procedure here

	select distinct @divId = isnull(aa.div_id,bb.div_id)
	from (
		select b.div_id, a.username
		from tb_m_security_users a
		left join tb_m_mapp_div b on a.departement_id = b.department_id
		where a.username = @uid
	) aa
	outer apply
	(
		select b.div_id, b.department_id, b.[username] 
		from tb_m_mapp_div b
		where b.[username] = aa.[username]
	) bb
	
	set @uid = replace(@uid,'TAM\','');
	
	select top 1 @vendor_cd = vendor_cd, @transaction_cd= transaction_cd, @budget_no = budget_no
		from tb_t_upload_h
	where process_id = @pid
		
	select @totalAmount = sum(amount), @currCd = currency_cd from tb_t_pv_d 
	where process_id=@pid
	group by currency_cd

	select top 1 @vendorGroup = convert(varchar(1),value1), @vendorDesc = value2
	from tb_m_parameter
	where key_param = 'pv_vendor_grp'
		and value1 = isnull((select top 1 vendor_group_cd from vw_VendorSAPElvis where vendor_cd = @vendor_cd),0)

	exec elvis_db.[dbo].[sp_GetDocNumber] @doc_cd = 1, @user = @uid, @no = @pv output
	
	update tb_t_upload_h set pv_no = @pv where process_id = @pid

	insert tb_r_pv_h (
		 pv_no
		,pv_year
		,pay_method_cd
		,vendor_group_cd
		,vendor_cd
		,pv_type_cd
		,transaction_cd
		,budget_no
		,division_id
		,pv_date
		,refference_no
		,status_cd
		,created_date
		,created_by
	) 
	values 
	(
		 @pv
		,cast(year(getdate()) as varchar)
		,'T' --dihardcode T-> Transfer
		,@vendorGroup
		,@vendor_cd
		,'1' --dihardcode 1-> Direct
		,@transaction_cd
		,@budget_no
		,@divId
		,@dateNow
		,null
		,'1' --Draft
		,getdate()
		,@uid
	)

	exec [usp_InsertPVRD] @pid, @uid;
	
	if not exists (select top 1 1 from TB_R_PVRV_AMOUNT where doc_no = @pv)
	begin
		insert TB_R_PVRV_AMOUNT values
		(@pv, cast(year(getdate()) as varchar), @currCd, @totalAmount, '1', getdate(),@uid, null, null)
	end
	else
	begin		
		update TB_R_PVRV_AMOUNT set TOTAL_AMOUNT = @totalAmount
		where DOC_NO = @pv
	end

	update TB_R_PV_H set TOTAL_AMOUNT = @totalAmount
		where PV_NO = @pv

	declare @reff_no varchar(max)
	set @reff_no = cast(@pv as varchar) + cast(year(getdate()) as varchar);

	insert tb_h_distribution_status (reff_no, seq_no, status_cd, [date],[created_by],[created_dt]) values
	(@reff_no,1,1,getdate(),@uid, getdate())

END



GO
