USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetPVEmail]    Script Date: 6/29/2020 10:20:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--USE [BES]
--GO
--
--/****** Object:  StoredProcedure [dbo].[usp_GetPVEmail]    Script Date: 6/7/2020 12:45:25 AM ******/
--SET ANSI_NULLS ON
--GO
--
--SET QUOTED_IDENTIFIER ON
--GO
--
--
---- =============================================
---- Author:		<Author,,Name>
---- Create date: <Create Date,,>
---- Description:	<Description,,>
---- =============================================
CREATE PROCEDURE [dbo].[usp_GetPVEmail]
	-- Add the parameters for the stored procedure here
	@pid varchar(max),
	@tobudget int,
	@expired varchar(2)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	declare @xven table
	(
		val varchar(10)
	)

	insert @xven
	select items from dbo.udf_explode(',',@pid);

    -- Insert statements for procedure here
	if (@tobudget = 1)
		begin
			select count(1) [TOTAL_ALL], 
				(select count(1) from tb_r_pv_h b
					where b.status_cd = a.status_cd
					and b.pv_date = cast(getdate() as date)
				) [TOTAL_NEW],
				'BG' [GROUP]
			from tb_r_pv_h a
			where a.status_cd = 
				(select top 1 value1 from tb_m_parameter where key_param='pv_status' and cast(value2 as varchar)='pv_submission')
			group by a.status_cd
		end
	else
		begin
			
			select count(1) [TOTAL_ALL],
				(select count(1) from tb_r_pv_h x
					where x.status_cd = a.status_cd
						and cast(x.changed_date as date) = cast(getdate() as date)
						and x.vendor_cd = a.vendor_cd
					group by x.vendor_cd
				) [TOTAL_NEW],
				b.customer_group_id [GROUP],
				replace(replace(replace(x.remaining, '{0}','<tr><td style="width:100px;">'),'{1}','</td><td style="width:80px;">'),'{2}','</td></tr>') [DAY_REMAINING]
			from tb_r_pv_h a
			join
			(
				select distinct vendor_cd, customer_group_id from tb_t_mapp_vendor
			) b on a.vendor_cd = b.vendor_cd
			outer apply (
				select (
					select '' + val 
						from (
							select '{0}' + cast(aa.pv_no as varchar) + '{1}' + cast(datediff(day, getdate(), dateadd(day, cast(@expired as int), aa.changed_date)) as varchar) + '{2}' [val]
							from tb_r_pv_h aa
							where aa.vendor_cd= a.vendor_cd
								and aa.status_cd = a.status_cd
						  ) z
						for xml path ('')
				) [remaining]
			) x
			where a.status_cd = 
				(select top 1 value1 from tb_m_parameter where key_param='pv_status' and cast(value2 as varchar)='pv_verified_budget')
				and a.vendor_cd in 
				(
				select vendor_cd from tb_t_upload_h where process_id in 
					(
					select distinct val from @xven
					)
				)
			group by b.customer_group_id, a.vendor_cd, x.remaining, a.status_cd
		end

END




GO
