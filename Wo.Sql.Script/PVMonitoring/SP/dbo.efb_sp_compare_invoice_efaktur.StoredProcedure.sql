USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[efb_sp_compare_invoice_efaktur]    Script Date: 6/29/2020 10:20:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE PROC [dbo].[efb_sp_compare_invoice_efaktur](@PV_NO VARCHAR(100))
AS
--DECLARE @PV_NO VARCHAR(100)= '500140238';
DECLARE @CompareDataInvoice TABLE
(PV_NO        VARCHAR(100),
 INVOICE_NO   VARCHAR(100),
 TAX_NO       VARCHAR(100),
 PPH          DECIMAL(18, 2),
 TURNOVER     DECIMAL(18, 2),
 VAT          DECIMAL(18, 2),
 TAX_NO_INPUT VARCHAR(100),
 TAX_DT       DATETIME
);

--get data elvis
INSERT INTO @CompareDataInvoice
EXEC [dbo].[efb_sp_compare_data_invoice]
     @PV_NO = @PV_NO;
DECLARE @CompareDataEFB TABLE
(FGPengganti         VARCHAR(100),
 NomorFaktur         VARCHAR(100),
 NomorFakturGabungan VARCHAR(100),
 TanggalFaktur       DATETIME,
 NPWPPenjual         VARCHAR(100),
 NamaPenjual         VARCHAR(500),
 AlamatPenjual       VARCHAR(MAX),
 JumlahDPP           DECIMAL(18, 2),
 JumlahPPN           DECIMAL(18, 2),
 JumlahPPNBM         DECIMAL(18, 2),
 StatusApprovalXML   VARCHAR(200),
 StatusFakturXML     VARCHAR(200),
 TanggalExpired      DATETIME,
 FakturURLGabungan   VARCHAR(MAX),
 FakturType          VARCHAR(25)
);
DECLARE @TAX_NO VARCHAR(MAX);
SET @TAX_NO =
(
    SELECT TAX_NO+';'
    FROM @CompareDataInvoice
    ORDER BY TAX_NO FOR XML PATH('')
);

-- Get data efaktur
INSERT INTO @CompareDataEFB
EXEC TAM_EFAKTUR.[dbo].efb_sp_compare_data_efaktur
     @TAX_NO;
DECLARE @bufferDPP DECIMAL(18, 2)= 9;
DECLARE @bufferPPN DECIMAL(18, 2)= 0.9;

--Update Previous Data
UPDATE TB_T_EFB_COMPARE_LIST
  SET
      IS_DELETED = 1
WHERE TB_T_EFB_COMPARE_LIST.PV_NO = @PV_NO;

--Data Generator
INSERT INTO dbo.TB_T_EFB_COMPARE_LIST
       SELECT @PV_NO,
              CASE
                  WHEN RIGHT(RTRIM(ValidasiMessage), 1) = '.'
                  THEN 'MATCHED'
                  ELSE 'UNMATCHED'
              END AS ValidasiData,
              maindata.INVOICE_NO,
              maindata.TAX_NO,
              maindata.DPP,
              maindata.PPN,
              maindata.PPH,
              maindata.ValidasiMessage,
              maindata.IS_DELETED,
              maindata.CREATED_DT
       FROM
(
    SELECT elp.INVOICE_NO,
           isnull(efc.FakturURLGabungan, elp.TAX_NO_INPUT) AS TAX_NO,
           elp.TURNOVER AS DPP,
           elp.VAT AS PPN,
           elp.PPH,
           CASE
               -- #1
               WHEN efc.FakturURLGabungan IS NULL AND LTRIM(elp.TAX_NO) <> ''
               THEN 'Tax Invoice not found, please check invoice tax number or scan said tax invoice on TAM EFAKTUR!'
               
			   -- #2
               WHEN efc.TanggalExpired IS NOT NULL --3
                    AND efc.TanggalExpired <= GETDATE()
               THEN 'Tax invoice expired, please replace tax invoice number and rescan!'
               
			   -- #3
               WHEN efc.FakturType = 'EFaktur'
                    AND (efc.FGPengganti = 1
                         --AND LEFT(LTRIM(elp.TAX_NO_INPUT), 3) != '011')
                         AND RIGHT(LEFT(LTRIM(elp.TAX_NO_INPUT), 3),1) != '1')
               THEN 'Please replace Tax Invoice Number and Tax Date to latest faktur pengganti scanned on EFB!'
			  
			   -- #4
               WHEN elp.VAT IS NOT NULL
                    AND ABS(isnull(elp.VAT, 0) - isnull(efc.JumlahPPN, 0)) > @bufferPPN
               THEN 'Tax invoice PPN value mismatch with Faktur on EFB, please check invoice PPN Value!'
               
			   -- #5
               WHEN --efc.FakturType = 'EFaktur'
                    --AND 
					(efc.TanggalFaktur <> elp.TAX_DT)
               THEN 'Please make sure inputted Tax Date similar to Faktur Date! If Faktur Pengganti Exist please use Tax Invoice Pengganti Tax Date'

			   -- #6
               WHEN elp.TURNOVER IS NULL
                    AND ISNULL(elp.VAT, 0) > 0
               THEN 'PPN inputted without Turnover/ DPP, please check data!'
               
			   -- #7
               WHEN isnull(elp.tax_no, '') = ''
                    AND elp.VAT IS NULL
                    AND elp.PPH IS NULL
               THEN 'Passed non taxable transaction.'

			   -- #8
               WHEN isnull(elp.tax_no, '') <> ''
                    AND isnull(elp.tax_no, '') = isnull(efc.NomorFaktur, '')
                    AND elp.VAT IS NULL
               THEN 'VAT (PPN) value not found for tax invoice , please review data!' 

			   -- #9
               WHEN isnull(elp.tax_no, '') = ''
                    AND elp.VAT IS NULL
                    AND elp.PPH IS NOT NULL
               THEN 'Pass, withholding tax only.'

			   -- #10
               WHEN efc.StatusFakturXML LIKE '%BATAL%'
               THEN 'Tax invoice status are cancelled, please replace tax invoice number and rescan!'

			   -- #11
               WHEN efc.FakturType = 'Non eFaktur'
               THEN 'Passed (Non Efaktur Tax Invoice).'

			   -- #13
               WHEN isnull(elp.tax_no, '') <> ''
                    AND elp.VAT IS NULL
               THEN 'Please check VAT (PPN amount) on template'

			   -- #14
               ELSE 'Passed.'
           END AS ValidasiMessage,
           0 IS_DELETED, -- IS_DELETED - bit
           GETDATE() CREATED_DT -- CREATED_DT - datetime
    FROM @CompareDataInvoice elp
         LEFT JOIN @CompareDataEFB efc ON(CASE
                                              WHEN efc.FakturType = 'eFAKTUR'
                                              THEN RIGHT(dbo.udf_GetNumeric(elp.TAX_NO), 13)
                                              ELSE elp.TAX_NO
                                          END) = efc.NomorFaktur
) maindata;

--GETDATA
--SELECT *
--FROM dbo.vw_efb_compare_faktur_list vec
--WHERE vec.PV_NO = @PV_NO
--      AND vec.IS_DELETED = 0;
--SELECT *
--FROM @CompareDataEFB cde;
--SELECT *
--FROM @CompareDataInvoice cdi;
               












GO
