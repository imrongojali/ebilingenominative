USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[usp_error_handler]    Script Date: 6/29/2020 10:20:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_error_handler] 
	@pid INT = 0
AS
BEGIN  TRY 
	DECLARE 
		@errmsg   nvarchar(2048),
		@proc     sysname,
		@errState INT;
		
	SET @proc = ISNULL(ERROR_PROCEDURE(),'?') + char(9) + '@' + CONVERT(VARCHAR, ISNULL(ERROR_LINE(),0));
	SET @errState = ERROR_STATE();
	SET @errmsg = ISNULL(ERROR_MESSAGE(), '') 
		+ ' #' + + ISNULL(CONVERT(VARCHAR, ERROR_NUMBER()), '')
		+ ' ' + ISNULL(CONVERT(VARCHAR, ERROR_SEVERITY()), '') 
		+ ', ' + ISNULL(CONVERT(VARCHAR, @errState), '')
	;
   
   EXEC dbo.usp_PutLog @errmsg, 'err',
	 @proc, @pid OUTPUT, 'MSTD00002ERR', 'ERR', '2', '0';
   RETURN @errState;
END TRY 
BEGIN CATCH 
	PRINT @errmsg;	
END CATCH

GO
