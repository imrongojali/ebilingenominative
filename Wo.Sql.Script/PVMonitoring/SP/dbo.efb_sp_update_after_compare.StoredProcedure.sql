USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[efb_sp_update_after_compare]    Script Date: 6/29/2020 10:20:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Stored Procedure

CREATE proc [dbo].[efb_sp_update_after_compare]
(
@PV_NO varchar(100)
)
as
--DECLARE @CompareDataInvoice TABLE
--(PV_NO      VARCHAR(100),
-- INVOICE_NO VARCHAR(100),
-- TAX_NO     VARCHAR(100),
-- PPH        DECIMAL(18, 2),
-- TURNOVER   DECIMAL(18, 2),
-- VAT        DECIMAL(18, 2),
-- TAX_NO_INPUT VARCHAR(100),
-- TAX_DT       DATETIME
--);
----get data elvis
--INSERT INTO @CompareDataInvoice
--EXEC [dbo].[efb_sp_compare_data_invoice]
--     @PV_NO = @PV_NO;--
--DECLARE @CompareDataEFB TABLE
--(FGPengganti      VARCHAR(100),
-- NomorFaktur     VARCHAR(100),
-- NomorFakturGabungan     VARCHAR(100),
-- TanggalFaktur datetime,
-- NPWPPenjual varchar(100),
-- NamaPenjual varchar(500),
-- AlamatPenjual varchar(max),
-- JumlahDPP DECIMAL(18, 2),
-- JumlahPPN DECIMAL(18, 2),
-- JumlahPPNBM DECIMAL (18,2),
-- StatusApprovalXML varchar(200),
-- StatusFakturXML varchar(200),
-- TanggalExpired datetime,
-- FakturURLGabungan varchar(max),
-- FakturType varchar(25)
--);
--declare @TAX_NO varchar(max)
--set @TAX_NO = (SELECT TAX_NO+';'
--		FROM
--			@CompareDataInvoice
--		ORDER BY TAX_NO FOR XML PATH(''))
----get data efaktur
--insert into @CompareDataEFB
--exec --EFAKTUR_SERVER.
--TAM_EFAKTUR.[dbo].efb_sp_compare_data_efaktur @TAX_NO
------------------------------------------update EXPIRED_DATE and URL_WHT-----------------------------------------------
declare @TanggalExpired datetime
--select @TanggalExpired = min(TanggalExpired) from @CompareDataEFB

select @TanggalExpired = min(TanggalExpired) from --EFAKTUR_SERVER.
TAM_EFAKTUR.[dbo].tb_r_vatin 
where rowstatus = 0
and pvnumber = @PV_NO 
and TanggalExpired is not null

if @TanggalExpired is not null
begin
	update TB_R_PV_H set EXPIRED_DATE = @TanggalExpired where PV_NO = @PV_NO
end
else
begin
	update TB_R_PV_H set EXPIRED_DATE = dateadd(day, -day(getdate()), dateadd(month,4,getdate())) where PV_NO = @PV_NO -- 20190422
end

declare @PDFWithholding varchar(max)
set @PDFWithholding = 
(
SELECT PDFWithholding +';'
		FROM
			(
				select * from --efaktur_server.
				tam_efaktur.dbo.tb_r_wht where NomorPV = @PV_NO
			) dt
ORDER BY NomorInvoice FOR XML PATH('')
)
if	isnull(@PDFWithholding,'') <>''
begin
	update TB_R_PV_H set URL_WHT = @PDFWithholding where PV_NO = @PV_NO 
end
---------------------------------------------------------


GO
