USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetPVD]    Script Date: 6/29/2020 10:20:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_GetPVD]    
   @pid int
AS 
BEGIN 
	SET NOCOUNT ON;

	select 
		 a.SEQ_NO	
		,a.PV_NO	
		,a.PV_YEAR	
		,a.COST_CENTER_CD	
		,a.CURRENCY_CD	
		,case when c.account_info is not null then
			cast(c.account_info as varchar) + ': ' + a.[DESCRIPTION]
			else a.[DESCRIPTION]
		 end [DESCRIPTION]
		,a.AMOUNT	
		,a.WBS_NO	
		,a.INVOICE_NO	
		,a.INVOICE_DATE	
		,a.ITEM_TRANSACTION_CD	
		,a.TAX_NO	
		,a.GL_ACCOUNT	
		,a.TAX_CD	
		,a.FROM_CURR	
		,a.FROM_SEQ	
		,a.ITEM_NO	
		,a.DPP_AMOUNT	
		,a.TAX_ASSIGNMENT	
		,a.ACC_INF_ASSIGNMENT	
		,a.TAX_DT	
		,a.WHT_TAX_CODE	
		,a.WHT_TAX_TARIFF	
		,a.WHT_TAX_ADDITIONAL_INFO	
		,a.WHT_DPP_PPh_AMOUNT
		,a.CREATED_DT	
		,a.CREATED_BY	
		,a.CHANGED_DT	
		,a.CHANGED_BY	
	from tb_r_pv_d a
	join tb_t_upload_h b
		on a.pv_no = b.pv_no
	outer apply 
	(
	  select account_info from tb_t_upload_d d 
		where d.process_id = b.process_id and seq_no='1'
	) c
	where b.process_id = @pid	

END;







GO
