USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetPVTHeader]    Script Date: 6/29/2020 10:20:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[usp_GetPVTHeader]    
   @pid int
AS 
BEGIN 
	SET NOCOUNT ON;

	select
		[process_id],
		[transaction_cd],
		[vendor_cd],
		[upload_filename],
		[reff_no],
		[rows_count],
		[ok],
		[bank_key],
		[bank_account],
		[pv_no],
		[status_budget],
		[bank_type],
		[rejected_reason],
		[rejected_by],
		[rejected_dt]
	from tb_t_upload_h 
	where process_id = @pid
	

END;








GO
