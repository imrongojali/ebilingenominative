USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[usp_SavePVH]    Script Date: 6/29/2020 10:20:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[usp_SavePVH](		
		@pid int,
		@transaction_cd int,
		@vendor_cd varchar(10),
		@upload_filename varchar(255),
		@user varchar(20),
		@budget_no varchar(24),
		@process_status int OUTPUT,
	    @err_mesg varchar(2000) OUTPUT
) 
AS 
BEGIN
	SET NOCOUNT ON;

	declare @pv int, 
			@username varchar(50), 
			@dateNow date = cast(getdate() as date), 
			@totalAmount numeric(16,2), 
			@vendorGroup int, 
			@vendorDesc varchar(225),
			@currCd varchar(3),
			@divId int;

	declare
		@log VARCHAR(8000),
		@ErrInsDist VARCHAR(40),
		@na VARCHAR(50) = 'usp_SavePVH';

	set @log = 
		'@pid:' + isnull(cast(@pid as varchar),'') + ', ' +
		'@transaction_cd:' + isnull(cast(@transaction_cd as varchar),'') + ', ' +
		'@vendor_cd:' + isnull(@vendor_cd, '') + ', ' +  
		'@upload_filename:' + isnull(@upload_filename, '') + ', ' +  
		'@user:' + isnull(@user, '');
		
	exec [usp_PutLog] @log, @user, @na, @pid, 'INF', 'INF';

	if not exists
	(
		select top 1 1
		from tb_m_customer_id a
		join tb_t_mapp_vendor b on a.customer_group = b.customer_group_id
		where b.vendor_cd = @vendor_cd
			and b.is_viewed_pv_monitoring = 1
	)
	begin
		set @process_status = 1
		set @err_mesg = 'Not registered user login to vendor, ' + @vendor_cd;
		
		exec usp_PutLog @err_mesg, @user, @na, @pid output, 'ERR', 'ERR';
	end

	if @process_status <> 0
	begin
		return		
	end

	--13052020 select distinct @divId = isnull(aa.div_id,bb.div_id)
	--13052020 from (
	--13052020 	select b.div_id, a.username
	--13052020 	from tb_m_security_users a
	--13052020 	left join tb_m_mapp_div b on a.departement_id = b.department_id
	--13052020 	where a.username = @user
	--13052020 ) aa
	--13052020 outer apply
	--13052020 (
	--13052020 	select b.div_id, b.department_id, b.[username] 
	--13052020 	from tb_m_mapp_div b
	--13052020 	where b.[username] = aa.[username]
	--13052020 ) bb

	if exists (select top 1 1 from tb_t_upload_d where process_id = @pid)
	begin
		set @username = replace(@user,'TAM\','');
		
		select @totalAmount = sum(amount), @currCd = currency_cd from tb_t_pv_d 
		where process_id=@pid
		group by currency_cd

		select top 1 @vendorGroup = convert(varchar(1),value1), @vendorDesc = value2
		from tb_m_parameter
		where key_param = 'pv_vendor_grp'
			and value1 = isnull((select top 1 vendor_group_cd from vw_VendorSAPElvis where vendor_cd = @vendor_cd),0)

		if not exists (
			select top 1 1 
			from tb_t_upload_h 
			where process_id = @pid
			)
		begin

			--13052020 exec elvis_db.[dbo].[sp_GetDocNumber] @doc_cd = 1, @user = @username, @no = @pv output		

			insert tb_t_upload_h 
			(
				 process_id
				,transaction_cd
				,vendor_cd
				,upload_filename
				,created_by 
				,created_dt 
				--13052020 ,pv_no	
				,budget_no
			)
			values 
			(
				 @pid
				,@transaction_cd
				,@vendor_cd
				,@upload_filename
				,@user
				,getdate()
				--13052020 ,@pv
				,@budget_no
			);

			if @@ROWCOUNT >= 1
			begin
				set @err_mesg = 'Inserted data tb_t_upload_h successfully';
				exec usp_PutLog @err_mesg, @user, @na, @pid output, 'INF', 'INF';

				exec usp_PutUpload_TAXNew @pid, @uid = @username; --13052020
			end
		end

		/*
		if (@pv != null or @pv != 0)
		begin
			insert tb_r_pv_h (
				 pv_no
				,pv_year
				,pay_method_cd
				,vendor_group_cd
				,vendor_cd
				,pv_type_cd
				,transaction_cd
				,budget_no
				,division_id
				,pv_date
				,refference_no
				,status_cd
				,created_date
				,created_by
			) 
			values 
			(
				 @pv
				,cast(year(getdate()) as varchar)
				,'T' --dihardcode T-> Transfer
				,@vendorGroup
				,@vendor_cd
				,'1' --dihardcode 1-> Direct
				,@transaction_cd
				,null
				,@divId
				,@dateNow
				,null
				,'1' --Draft
				,getdate()
				,@username
			)

			if @@ROWCOUNT >= 1
			begin
				set @err_mesg = 'Inserted data tb_r_pv_h successfully';
				exec usp_PutLog @err_mesg, @user, @na, @pid output, 'INF', 'INF';

				if (@process_status = 0)
				begin
					exec usp_PutUpload_TAXNew @pid, @uid = @username;
				end
			end
		end
		else
		begin

			select top 1 @pv = pv_no 
			from tb_t_upload_h 
			where process_id = @pid

			update tb_r_pv_h set
				 total_amount = @totalAmount
				,changed_date = getdate()
				,changed_by = @username
			where pv_no = @pv

			if @@ROWCOUNT >= 1
			begin
				set @err_mesg = 'Updated data tb_r_pv_h successfully';
				exec usp_PutLog @err_mesg, @user, @na, @pid output, 'INF', 'INF';
			end

			update tb_r_pvrv_amount set 
				 total_amount = @totalAmount
				,changed_date = getdate()
				,changed_by = @user
			where doc_no = @pv

			if @@ROWCOUNT >= 1
			begin
				set @err_mesg = 'Updated data tb_r_pvrv_amount successfully';
				exec usp_PutLog @err_mesg, @user, @na, @pid output, 'INF', 'INF';
			end

			update tb_t_upload_h set 
				changed_by = @user,
				changed_dt = getdate() 
			where pv_no = @pv
			
			if @@ROWCOUNT >= 1
			begin
				set @err_mesg = 'Updated data tb_t_upload_h successfully';
				exec usp_PutLog @err_mesg, @user, @na, @pid output, 'INF', 'INF';
			end
		end
		*/
	end

END;




GO
