USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetEmailBankRequested]    Script Date: 6/29/2020 10:20:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetEmailBankRequested]
	-- Add the parameters for the stored procedure here	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	

	declare @emailTo table
	(
		val varchar(125)
	)

	declare @x varchar(max)

	select @x = value1 from tb_m_parameter where key_param='pv_emailto_bank_req'

	if @x is not null
	begin
		insert @emailTo
		select items from dbo.udf_explode(',',@x)
	end 

	if exists (select top 1 1 from @emailTo)
	begin
		select a.val [EMAIL_TO], 
			replace(replace(replace(replace(b.[desc], '{0}','<tr><td style="width:150px;">'),'{1}','</td><td style="width:200px;">'),'{2}','</td><td style="width:100px;">'),'{3}','</td></tr>') [REQ_REMAINING]
		from @emailTo a
		outer apply (
			select (
				select '' + val 
					from (
							select '{0}' + a.bank_account + '{1}' + a.name_of_bank + '{2}On Request{3}'[val]
								from tb_m_vendor_bank a
								left join SAP_DB.dbo.tb_m_vendor_bank b
									on a.bank_account = b.bank_account
										and a.vendor_cd = b.vendor_cd
								where 1=1
									and status_req = '1'
									and b.bank_account is null
						) z
					for xml path ('')
			) [desc]
		) b
	end

END




GO
