USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[efb_sp_vw_compare_faktur_list]    Script Date: 6/29/2020 10:20:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO







-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[efb_sp_vw_compare_faktur_list] 
	-- Add the parameters for the stored procedure here
	@pv_no int--,
	--@result varchar(max) output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @x table
	(
		val varchar(255),
		pv_no int
	)

    -- Insert statements for procedure here
	insert @x
	select 'PV_NO: ' + cast(pv_no as varchar) + ', INVOICE_NO: ' + invoice_no + ', MSG: ' + validasi_message, cl.PV_NO
    from tb_t_efb_compare_list cl
    where cl.is_deleted = 0
		and cl.PV_NO = @pv_no
		--and VALIDASI_MESSAGE != 'Passed.'

	if (select count(val) from @x where val not like '%Passed.%') >= 1
	begin
		/*
		set @result = (select val + '<br/>' 
						  from @x
						  for xml path ('')) */

		update tb_r_pv_h set status_compare = 'UNMATCHED' where pv_no = (select distinct pv_no from @x)
	end
	else
	begin
		declare @pvno int
		select top 1 @pvno= pv_no from @x

		update tb_r_pv_h set status_compare = 'MATCHED' where pv_no = @pvno

		exec [efb_sp_update_after_compare] @pv_no = @pvno
	end

	select val from @x
END





GO
