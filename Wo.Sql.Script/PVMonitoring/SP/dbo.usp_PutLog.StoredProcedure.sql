USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[usp_PutLog]    Script Date: 6/29/2020 10:20:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_PutLog] 
	@what VARCHAR(8000),
	@user VARCHAR(20),
	@where VARCHAR(512), 	
	@pid INT OUTPUT ,
	@id VARCHAR(12) = null, 
	@type VARCHAR(3) = null, 
	@func VARCHAR(10) = null,
	@sts VARCHAR(1) = null, 	
	@rem VARCHAR(200) = null
AS
BEGIN 
	-- WHEN ALL PARAMETER IS NULL DON'T BOTHER EXECUTING 
	IF (@what IS NULL  
		AND @user IS NULL 
		AND @where IS NULL 
		AND @pid IS NULL 
		AND @id IS NULL 
		AND @type IS NULL 
	    AND @func IS NULL 
	    AND @sts IS NULL 
	    AND @rem IS NULL)
	BEGIN 
		RETURN;
	END;
	
	DECLARE @now DATETIME;
	SET @now = SYSDATETIME();
	DECLARE @u VARCHAR(50);
	DECLARE @lastPid INT, @lastSeq INT;
	     
	if @user IS NULL OR (LEN(@user) < 1)
	BEGIN
		SELECT @u = ORIGINAL_LOGIN();
	END
	ELSE 
	BEGIN
		SET @u = @user;
	END;
	
	IF @func IS NULL 
	BEGIN
		SET @func = '';
	END;
	
	IF @id IS NULL 
	BEGIN
		SET @id = '';
	END;
	
	IF @type IS NULL OR LEN(@type) < 1
	BEGIN
		IF @id IS NOT NULL AND LEN(@id) >3
		BEGIN
			SET @type=SUBSTRING(@id, LEN(@id)-2, 3);
		END
		ELSE 
		BEGIN
			SET @type = 'INF';
		END;
	END;
	
	IF @sts IS  NULL
	BEGIN
		IF @type = 'ERR' 
			SET @sts  = '1';
		ELSE IF @type = 'WRN'
			SET @sts = '1';
		ELSE IF @type = 'INF'
			SET @sts = '0';
		ELSE IF @type = 'SYS'
			SET @sts = '2';
		ELSE
			SET @sts = '0';
	END;
	
	-- BEGIN TRANSACTION;	
	
	IF (@pid IS NULL) OR (@pid < 1) 
	BEGIN
		INSERT dbo.TB_R_LOG_H (PROCESS_DT, FUNCTION_ID, PROCESS_STS, [USER_ID], END_DT, REMARKS)
		VALUES (@now, @func, @sts, @u, @now, @rem);
		
		SELECT @lastPid = SCOPE_IDENTITY();				
	END 
	ELSE
	BEGIN
		SET @lastPid = @pid;
		
		DECLARE @_func VARCHAR(10), @_sts VARCHAR(1),  @_rem VARCHAR(200), @_user VARCHAR(20);
		SELECT @_func = FUNCTION_ID,
			@_sts = process_sts,
			@_user = [user_id],
			@_rem = [remarks]
		FROM dbo.TB_R_LOG_H 
		WHERE process_id = @lastPid;
		
		IF ((@func <> @_func) or (@sts <> @_sts) or (@u <> @_user) or (@rem <> @_rem))
		BEGIN 
			IF (@func is null or (LEN(@func) < 1)) 
			BEGIN
				SET @func = @_func;
			END;
			IF (@u IS NULL or (LEN(@u) < 1)) 
			BEGIN
				SET @u = @_user;
			END;		
			IF (@rem IS NULL OR (LEN(@rem) < 1)) 
			BEGIN
				SET @rem = @_rem;
			END
			UPDATE dbo.TB_R_LOG_H 
			SET 
				FUNCTION_ID = @func,
				PROCESS_STS = @sts,
				[USER_ID] = @u,
				[REMARKS] = @rem,
				END_DT = @now
			WHERE PROCESS_ID = @lastPid;
		END
		ELSE 
		BEGIN
			UPDATE dbo.TB_R_LOG_H
			SET end_dt = @now
			WHERE process_id = @lastPid;
		END;
	END;
	
	IF (@what IS NOT NULL) AND (LEN(@what) > 0) AND (@lastPid > 0)
	BEGIN
		SELECT @lastSeq = MAX(SEQ_NO) 
		FROM dbo.TB_R_LOG_D WHERE process_id = @lastPid;
		
		IF @lastSeq IS NULL 
		BEGIN
			SET @lastSeq = 0;
		END;		
		
		INSERT dbo.TB_R_LOG_D (process_id, seq_no, msg_id, msg_type, err_location, err_message, err_dt) 
		VALUES (@lastPid, @lastSeq + 1, @id, @type, @where, @what, @now);
	END;
	
	-- COMMIT;	
	
	SET @pid = @lastPid;
END 


GO
