USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[efb_sp_remove_after_compare]    Script Date: 6/29/2020 10:20:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE proc [dbo].[efb_sp_remove_after_compare]
(
	@PV_NO varchar(100),
	@INVOICE_NO varchar(max)
)
as

--efb_sp_remove_after_compare '500140205',''

declare @year int, @TotalAmount numeric (16,2) 
select top 1 @year = PV_YEAR from tb_r_pv_h where pv_no=@PV_NO order by CREATED_DATE desc

delete
--select * from 
TB_R_PV_D
where PV_NO = @PV_NO AND INVOICE_NO IN(
select * from dbo.udf_SplitString(@INVOICE_NO,';') where isnull(ColumnData,'')<>''
)


--Recalculate Total Amount PV Header

  select @TotalAmount = sum(
  case when a.CURRENCY_CD like '%IDR%' then 
	d.AMOUNT 
  else 
	d.AMOUNT*a.EXCHANGE_RATE
  end) from tb_r_pv_d d
  inner join TB_R_PVRV_AMOUNT a
  on  a.CURRENCY_CD = d.CURRENCY_CD
  AND a.DOC_NO = D.PV_NO AND A.DOC_YEAR = D.PV_YEAR
  where d.PV_NO=@PV_NO
  and D.PV_YEAR = @year

update tb_r_pv_h set TOTAL_AMOUNT = @TotalAmount where pv_no =@PV_NO and PV_YEAR = @year

--PV Cover
update TB_R_PVRV_AMOUNT set TOTAL_AMOUNT = @TotalAmount where DOC_NO =@PV_NO and DOC_YEAR = @year

-- dipindah ke server efaktur biar ga lambat
EXEC TAM_EFAKTUR.DBO.efb_sp_remove_after_compare @PV_NO, @INVOICE_NO
/*
--update VATIN
UPDATE --EFAKTUR_SERVER.
EFAKTUR_SERVER.TAM_EFAKTUR.DBO.TB_R_VATIN 
SET 
PVNumber = NULL,
CreatedByPV = NULL,
NomorInvoice = NULL,
StatusInvoice = NULL,
SAPDocNumber = NULL,
PostingDate = NULL,
TAXPeriodMonth = NULL,
TAXPeriodYear = NULL,
RelatedGLAccount = NULL,
JumlahTurnover = NULL,
ModifiedOn = GETDATE(),
ModifiedBy = 'EFB-ELVIS Sync',
DataSource = NULL
WHERE ISNULL(PVNUMBER,'') = @PV_NO
and NomorInvoice IN(
select * from dbo.udf_SplitString(@INVOICE_NO,';') where isnull(ColumnData,'')<>''
)

--delete WHT
update --EFAKTUR_SERVER.
EFAKTUR_SERVER.TAM_EFAKTUR.DBO.TB_R_WHT 
set RowStatus='1'
WHERE ISNULL(NomorPV,'') = @PV_NO
and NomorInvoice IN(
select * from dbo.udf_SplitString(@INVOICE_NO,';') where isnull(ColumnData,'')<>''
)
*/


GO
