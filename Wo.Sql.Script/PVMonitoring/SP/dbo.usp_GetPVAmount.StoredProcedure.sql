USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetPVAmount]    Script Date: 6/29/2020 10:20:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_GetPVAmount]    
   @pid int
AS 
BEGIN 
	SET NOCOUNT ON;

	select 
		 a.DOC_NO	
		,a.DOC_YEAR	
		,a.CURRENCY_CD	
		,a.TOTAL_AMOUNT	
		,a.EXCHANGE_RATE
	from TB_R_PVRV_AMOUNT a
	join TB_T_UPLOAD_H b
		on a.DOC_NO = b.pv_no
	where b.process_id = @pid

END;



GO
