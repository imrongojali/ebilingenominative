USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[efb_sp_compare_data_invoice]    Script Date: 6/29/2020 10:20:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[efb_sp_compare_data_invoice](@PV_NO VARCHAR(100))
AS

--DECLARE @PV_NO VARCHAR(100)= '500140235';
     BEGIN
         DECLARE @InvoiceSummary TABLE
(PV_NO      VARCHAR(100),
 INVOICE_NO VARCHAR(100),
 PPH        DECIMAL(18, 2),
 TURNOVER   DECIMAL(18, 2),
 VAT        DECIMAL(18, 2)
);
         INSERT INTO @InvoiceSummary
                SELECT PV_NO,
                       INVOICE_NO,
                       PPH,
                       TURNOVER,
                       VAT
                FROM
(
    SELECT PV_NO,
           INVOICE_NO,
           FLAG,
           AMOUNT
    FROM
(
    SELECT PV_NO,
           INVOICE_NO,
           FLAG,
           SUM(ABS(AMOUNT)) AMOUNT
    FROM
(
    SELECT H.PV_NO,
           INVOICE_NO,
           GL_ACCOUNT,
           CASE
               WHEN GL_ACCOUNT IN('2140011', '2140011', '2140020', '2140030', '2140031', '2140032', '2140040')
               THEN 'PPH'
               WHEN GL_ACCOUNT IN('1153050')
               THEN 'VAT'
               WHEN GL_ACCOUNT IN(dbo.udf_GLAccountElvis(D.COST_CENTER_CD, H.TRANSACTION_CD, 1))
               THEN 'TURNOVER'--DPP
           END AS FLAG,
           AMOUNT
    FROM TB_R_PV_D D
         INNER JOIN TB_R_PV_H H ON D.PV_NO = H.PV_NO
    WHERE D.PV_NO = @PV_NO
) main
    WHERE FLAG IS NOT NULL
          AND LTRIM(INVOICE_NO) <> ''
    GROUP BY PV_NO,
             INVOICE_NO,
             FLAG
) DATA
) src PIVOT(SUM(amount) FOR flag IN([PPH],
                                    [TURNOVER],
                                    [VAT])) piv;
        
--CONSTRUCT
         DECLARE @Return AS TABLE
(PV_NO        VARCHAR(100),
 INVOICE_NO   VARCHAR(100),
 TAX_NO       VARCHAR(100),
 PPH          VARCHAR(100),
 TURNOVER     DECIMAL(18, 2),
 VAT          DECIMAL(18, 2),
 TAX_NO_INPUT VARCHAR(100),
 TAX_DT       DATETIME
);
         INSERT INTO @Return
                SELECT anc.PV_NO,
                       anc.INVOICE_NO,
                       anc.TAX_NO,
                       inv.PPH,
                       inv.TURNOVER,
                       inv.VAT,
                       anc.TAX_NO,
                       anc.TAX_DT
                FROM
(
    SELECT head.PV_NO,
           det.INVOICE_NO,
           RTRIM(LTRIM(det.TAX_NO)) TAX_NO,
           det.TAX_DT,
           --RIGHT(dbo.udf_GetNumeric(det.TAX_NO), 13) TAX_NO, -- perlu diganti
           ROW_NUMBER() OVER(PARTITION BY head.PV_NO,
                                          det.INVOICE_NO ORDER BY TAX_NO DESC) AS "IDX"
    FROM TB_R_PV_H head
         INNER JOIN TB_R_PV_D det ON det.PV_NO = head.PV_NO
    WHERE LTRIM(det.INVOICE_NO) <> ''
) anc
INNER JOIN @InvoiceSummary inv ON anc.PV_NO = inv.PV_NO
                                  AND anc.INVOICE_NO = inv.INVOICE_NO
                WHERE IDX = 1;
        
-- UPDATE IF TAX NO ARE EFAKTUR IN EFB		
--         UPDATE @Return
--           SET
--               TAX_NO_INPUT = TAX_NO,
--               TAX_NO = RIGHT(dbo.udf_GetNumeric(TAX_NO), 13)
--         FROM
--(
--    SELECT NomorFakturGabungan,
--           trv.FakturType
--    FROM TAM_EFAKTUR.dbo.TB_R_VATIn trv
--    WHERE dbo.udf_GetNumeric(trv.NomorFakturGabungan) IN
--(
--    SELECT dbo.udf_GetNumeric(TAX_NO) TAX_NO
--    FROM @Return
--)
--    AND trv.FakturType = 'EFAKTUR'
--) ef
--         WHERE [@Return].TAX_NO = dbo.udf_GetNumeric(ef.NomorFakturGabungan)
--         OR TAX_NO = ef.NomorFakturGabungan;

--RETURN
         SELECT r.PV_NO,
                r.INVOICE_NO,
                r.TAX_NO,
                r.PPH,
                r.TURNOVER,
                r.VAT,
                r.TAX_NO_INPUT,
                r.TAX_DT
         FROM @Return r;
     END; 






GO
