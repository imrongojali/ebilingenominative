USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[usp_SaveAttachmentInfo]    Script Date: 6/29/2020 10:20:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[usp_SaveAttachmentInfo] 
	@referenceNumber varchar(34),
    @fileName varchar(75),
    @categoryCode varchar(2),
    @description varchar(100),
    @path varchar(75),
    @username varchar(20),
	@byInternal varchar(1)

AS
BEGIN 
	SET NOCOUNT ON;

	--DELETE TB_R_PV_MONITORING_ATTACHMENT WHERE REFERENCE_NO = @referenceNumber AND FILE_NAME = @fileName; 

	INSERT TB_R_PV_MONITORING_ATTACHMENT (
			REFERENCE_NO,
			REF_SEQ_NO,
			ATTACH_CD,
			DESCRIPTION,
			DIRECTORY, 
			FILE_NAME, 
			CREATED_BY, 
			CREATED_DT,
			CHANGED_BY,
			CHANGED_DT,
			by_internal
	) VALUES 
	(        
			@referenceNumber,
			ISNULL((SELECT MAX(REF_SEQ_NO)+1 FROM TB_R_PV_MONITORING_ATTACHMENT WHERE REFERENCE_NO = @referenceNumber ), 1), 
			@categoryCode, 
			@description, 
			@path,
			@fileName, 
			@userName, 
			GETDATE(),
			@username, 
			GETDATE(),
			@byInternal
	); 


END 





GO
