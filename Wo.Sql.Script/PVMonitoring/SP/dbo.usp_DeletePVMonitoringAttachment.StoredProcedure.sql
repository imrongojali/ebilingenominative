USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[usp_DeletePVMonitoringAttachment]    Script Date: 6/29/2020 10:20:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[usp_DeletePVMonitoringAttachment] 
	-- Add the parameters for the stored procedure here
	@referenceNo varchar(34),
	@refSeqNo int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DELETE TB_R_PV_MONITORING_ATTACHMENT
	WHERE REFERENCE_NO = @referenceNo and REF_SEQ_NO = @refSeqNo

END



GO
