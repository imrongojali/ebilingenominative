USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetPVTransactionType]    Script Date: 6/29/2020 10:20:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_GetPVTransactionType] 
   @divId varchar(125)
AS 
BEGIN 
	SET NOCOUNT ON;

	declare @y table 
	(
		Code int,
		Name varchar(150),
		StandardWording varchar(100)
	)

	declare @x table
	(
		[transaction_cd] [int] not null,
		[transaction_name] [varchar](150) not null,
		[transaction_level_cd] [int] null,
		[std_wording] [varchar](100) not null,
		[group_cd] [int] null,
		[ft_cd] [int] null,
		[template_cd] [int] null,
		[close_flag] [int] null,
		[service_flag] [int] null,
		[budget_flag] [int] null,
		[attachment_flag] [int] null,
		[production_flag] [int] null,
		[cost_center_flag] [int] null,
		[assignment_tax_cd] [varchar](18) null
	)

	declare @z table
	(
		[transaction_cd] [int] not null
	)

	declare @zz varchar(max)
	select top 1 @zz = value1 from tb_m_parameter where key_param='allowed_trans_type'

	if @zz is not null
	begin
		insert @z
		select items from dbo.udf_explode(',',@zz)
	end

	insert @y 
	exec usp_GetTransactionTypesAccr
				@divCd = @divId
			   ,@isPVFormList = 1
			   ,@userDIVs = @divId
			   ,@userDivisionCanUploadDetail = 1
			   ,@isSecretary = 1
			   ,@tag = 0
			   ,@isSuspense = 0

	select a.[transaction_cd],
		   a.[transaction_name],
		   a.[transaction_level_cd],
		   a.[std_wording],
		   a.[group_cd],
		   a.[ft_cd],
		   a.[template_cd],
		   a.[close_flag],
		   a.[service_flag],
		   a.[budget_flag],
		   a.[attachment_flag],
		   a.[production_flag],
		   a.[cost_center_flag],
		   a.[assignment_tax_cd],
		   z.transaction_cd
	from vw_TransactionTypeElvis a
	join @y b on a.transaction_cd = b.Code
	left join @z z on z.transaction_cd = b.Code
	where 1=1 --and a.template_cd = '8'
		and nullif(@zz,'') is null or z.transaction_cd is not null
	
END;



GO
