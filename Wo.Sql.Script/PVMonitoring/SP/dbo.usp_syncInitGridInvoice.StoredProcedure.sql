USE [BES]
GO

/****** Object:  StoredProcedure [dbo].[usp_syncInitGridInvoice]    Script Date: 7/20/2020 03:31:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_syncInitGridInvoice] 
	-- Add the parameters for the stored procedure here
	@date date,
	@user varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	if object_id('tempdb..#udf_invoice') is not null
    drop table #udf_invoice

	if object_id('tempdb..#concatId') is not null
    drop table #concatId

	if object_id('tempdb..#concatIdIssue') is not null
    drop table #concatIdIssue

	if object_id('tempdb..#issueIdx') is not null
    drop table #issueIdx

	if exists(select top 1 1 
			  from tb_m_parameter 
				where key_param='grid_init_tb_t_udf_invoice')
	begin
		if (select value1 
			from tb_m_parameter 
				where key_param='grid_init_tb_t_udf_invoice') = '1'
		begin
			select *, 
				concat(id,
					   business_unit,
					   cast(business_unit_id as varchar),
					   invoice_number,
					   transaction_type,
					   transaction_type_id,
					   doc_date,
					   due_date, 
					   pph_no, 
					   customer_code, 
					   customer_name
				)[concatId]
			into #udf_invoice
			from udf_Invoice(1,'')
			where 1=1

			if exists (select top 1 1 from #udf_invoice)
			begin

				select [concatId]
				into #concatIdIssue
				from #udf_invoice
				group by [concatId]
				having count([concatId])>1

				select distinct a.[concatId]
				into #concatId
				from #udf_invoice a
				left join #concatIdIssue b
					on a.concatId = b.concatId
				where b.concatId is null

				select concatIdx 
					into #issueIdx
							from tb_t_udf_invoice
								where concatIdx in (select concatId from #concatIdIssue)

				if exists (select top 1 1 
							from tb_t_udf_invoice
								where concatIdx in (select concatId from #concatIdIssue))
				begin
								
					delete tb_t_udf_invoice 
					where concatIdx in (select concatId from #concatIdIssue)
					
					print 'delete data with issue : ' + cast(@@ROWCOUNT as varchar) + ' data'

					insert tb_t_udf_invoice
					select
						[id],
						[business_unit],
						[business_unit_id],
						[invoice_number],
						[transaction_type],
						[transaction_type_id],
						[doc_date],
						[due_date],
						[currency],
						[dpp],
						[tax],
						[total_amount],
						[ar_status],
						[tax_invoice],
						[pph_no],
						[da_code],
						[da_no],
						[customer_code],
						[customer_name],
						[customer_group],
						[exc_rate],
						[amount_pph],
						[inv_downloaded],
						[notur_uploaded],
						[is_notur_upload_done],
						[notur_downloaded],
						[downloaded_date],
						[downloaded_by],
						[is_inv_available],
						[inv_scan_downloaded],
						[downloadedscan_date],
						[downloadedscan_by],
						[is_inv_scan_available],
						[rate_pph],
						[vat_dormitory],
						[dormitory],
						[pi_downloaded],
						[pi_downloaded_date],
						[pi_downloaded_by],
						[customer_address],
						[is_retur],
						[ar_status_name],
						[ar_status_img_url],
						[tax_attc_url],
						[is_tax_attc_available], 
						getdate(), 
						@user, 
						getdate(), 
						'_syncInitGridInvoice_reInsert',
						a.concatId
					from #udf_invoice a
					join #concatIdIssue b
						on a.concatId =  b.concatId 	
						
					print 're-insert data with issue : ' + cast(@@ROWCOUNT as varchar) + ' data'			
				end

				if exists (select top 1 1 
							 from #concatIdIssue)
				begin
					insert tb_t_udf_invoice
					select
						[id],
						[business_unit],
						[business_unit_id],
						[invoice_number],
						[transaction_type],
						[transaction_type_id],
						[doc_date],
						[due_date],
						[currency],
						[dpp],
						[tax],
						[total_amount],
						[ar_status],
						[tax_invoice],
						[pph_no],
						[da_code],
						[da_no],
						[customer_code],
						[customer_name],
						[customer_group],
						[exc_rate],
						[amount_pph],
						[inv_downloaded],
						[notur_uploaded],
						[is_notur_upload_done],
						[notur_downloaded],
						[downloaded_date],
						[downloaded_by],
						[is_inv_available],
						[inv_scan_downloaded],
						[downloadedscan_date],
						[downloadedscan_by],
						[is_inv_scan_available],
						[rate_pph],
						[vat_dormitory],
						[dormitory],
						[pi_downloaded],
						[pi_downloaded_date],
						[pi_downloaded_by],
						[customer_address],
						[is_retur],
						[ar_status_name],
						[ar_status_img_url],
						[tax_attc_url],
						[is_tax_attc_available], 
						getdate(), 
						'',--@user, 
						null, 
						null,
						a.concatId
					from #udf_invoice a
					join (
						select a.* from #concatIdIssue a
						left join #issueIdx b on a.concatId = b.concatIdx
						where b.concatIdx is null
					) b on a.concatId = b.concatId

					print 'insert data with issue : ' + cast(@@ROWCOUNT as varchar) + ' data'
				end
				
				if exists (
					select top 1 1 
					from tb_t_udf_invoice 
					where concatIdx
						in 
						(
							select a.[concatId]
							from #udf_invoice a
							join #concatId b on a.concatId = b.concatId
						)
				)
				begin

					update a 
					set
						a.[id]=b.[id],
						a.[business_unit]=b.[business_unit],
						a.[business_unit_id]=b.[business_unit_id],
						a.[invoice_number]=b.[invoice_number],
						a.[transaction_type]=b.[transaction_type],
						a.[transaction_type_id]=b.[transaction_type_id],
						a.[doc_date]=b.[doc_date],
						a.[due_date]=b.[due_date],
						a.[currency]=b.[currency],
						a.[dpp]=b.[dpp],
						a.[tax]=b.[tax],
						a.[total_amount]=b.[total_amount],
						a.[ar_status]=b.[ar_status],
						a.[tax_invoice]=b.[tax_invoice],
						a.[pph_no]=b.[pph_no],
						a.[da_code]=b.[da_code],
						a.[da_no]=b.[da_no],
						a.[customer_code]=b.[customer_code],
						a.[customer_name]=b.[customer_name],
						a.[customer_group]=b.[customer_group],
						a.[exc_rate]=b.[exc_rate],
						a.[amount_pph]=b.[amount_pph],
						a.[inv_downloaded]=b.[inv_downloaded],
						a.[notur_uploaded]=b.[notur_uploaded],
						a.[is_notur_upload_done]=b.[is_notur_upload_done],
						a.[notur_downloaded]=b.[notur_downloaded],
						a.[downloaded_date]=b.[downloaded_date],
						a.[downloaded_by]=b.[downloaded_by],
						a.[is_inv_available]=b.[is_inv_available],
						a.[inv_scan_downloaded]=b.[inv_scan_downloaded],
						a.[downloadedscan_date]=b.[downloadedscan_date],
						a.[downloadedscan_by]=b.[downloadedscan_by],
						a.[is_inv_scan_available]=b.[is_inv_scan_available],
						a.[rate_pph]=b.[rate_pph],
						a.[vat_dormitory]=b.[vat_dormitory],
						a.[dormitory]=b.[dormitory],
						a.[pi_downloaded]=b.[pi_downloaded],
						a.[pi_downloaded_date]=b.[pi_downloaded_date],
						a.[pi_downloaded_by]=b.[pi_downloaded_by],
						a.[customer_address]=b.[customer_address],
						a.[is_retur]=b.[is_retur],
						a.[ar_status_name]=b.[ar_status_name],
						a.[ar_status_img_url]=b.[ar_status_img_url],
						a.[tax_attc_url]=b.[tax_attc_url],
						a.[is_tax_attc_available]=b.[is_tax_attc_available],
						a.[modified_date]=getdate(),
						a.[modified_by]='_syncInitGridInvoice_update'
					from tb_t_udf_invoice  a
					join
					(
						select a.*
						from #udf_invoice a 
						join #concatId b on a.concatId = b.concatId
					) b on a.concatIdx = b.concatId

					print 'update data prev : ' + cast(@@ROWCOUNT as varchar) + ' data'
				end
			end

			if exists (
					select a.*
					from (
						select a.[concatId]
						from #udf_invoice a
						join #concatId b on a.concatId = b.concatId
					) a
					left join tb_t_udf_invoice b 
						on b.concatIdx = a.concatId
					where b.business_unit is null					
				)
			begin

				insert tb_t_udf_invoice
				select
					a.[id],
					a.[business_unit],
					a.[business_unit_id],
					a.[invoice_number],
					a.[transaction_type],
					a.[transaction_type_id],
					a.[doc_date],
					a.[due_date],
					a.[currency],
					a.[dpp],
					a.[tax],
					a.[total_amount],
					a.[ar_status],
					a.[tax_invoice],
					a.[pph_no],
					a.[da_code],
					a.[da_no],
					a.[customer_code],
					a.[customer_name],
					a.[customer_group],
					a.[exc_rate],
					a.[amount_pph],
					a.[inv_downloaded],
					a.[notur_uploaded],
					a.[is_notur_upload_done],
					a.[notur_downloaded],
					a.[downloaded_date],
					a.[downloaded_by],
					a.[is_inv_available],
					a.[inv_scan_downloaded],
					a.[downloadedscan_date],
					a.[downloadedscan_by],
					a.[is_inv_scan_available],
					a.[rate_pph],
					a.[vat_dormitory],
					a.[dormitory],
					a.[pi_downloaded],
					a.[pi_downloaded_date],
					a.[pi_downloaded_by],
					a.[customer_address],
					a.[is_retur],
					a.[ar_status_name],
					a.[ar_status_img_url],
					a.[tax_attc_url],
					a.[is_tax_attc_available], 
					getdate(), 
					@user, 
					null, 
					null,
					a.concatId
				from 
				(
					select *
					from #udf_invoice 
					where concatId in (select concatId from #concatId)
				) a
				left join tb_t_udf_invoice b 
						on b.concatIdx = a.concatId
					where b.business_unit is null

				print 'insert new : ' + cast(@@ROWCOUNT as varchar) + ' data'
			end

		end
	end
END

GO

