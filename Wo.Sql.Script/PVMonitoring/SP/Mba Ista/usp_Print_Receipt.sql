USE [BES]
GO

/****** Object:  StoredProcedure [dbo].[usp_Print_Receipt]    Script Date: 7/6/2020 01:25:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		wo\dwi
-- Create date: 2019-10-23
-- Description:	Print Receipt
-- =============================================
CREATE PROCEDURE [dbo].[usp_Print_Receipt] 
	-- Add the parameters for the stored procedure here
	@dateFrom varchar(15),
	@dateTo varchar(15),
	@businessType varchar(max),
	@customerId varchar(max),
	@totAmount varchar(25)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if (@customerId = '''ALL-CUST''')
		set @customerId = '';

	DECLARE @@QUERY VARCHAR(MAX);
	SET @@QUERY = 'SELECT * FROM udf_SumInvoice(''1'', '''', '''+@totAmount+''') WHERE 1=1 ';

	IF ISNULL(@dateFrom, '') <> '' BEGIN
		IF ISNULL(@dateTo, '') <> '' BEGIN
			SET @@QUERY = @@QUERY + ' AND DOC_DATE BETWEEN ''' + @dateFrom + ''' AND ''' + @dateTo + ''''
		END ELSE BEGIN
			SET @@QUERY = @@QUERY + ' AND DOC_DATE = ''' + @dateFrom + ''''
		END
	END

	IF ISNULL(@businessType, '') <> '' BEGIN
		SET @@QUERY = @@QUERY + ' AND business_unit IN (' + @businessType + ') '
	END

	IF ISNULL(@customerId, '') <> '' BEGIN
		SET @@QUERY = @@QUERY + ' AND customer_group IN (' + @customerId + ') '
	END

	SET @@QUERY = @@QUERY + 'and ar_status=''InvStat01'' 
							 and customer_code is not null
							 order by business_unit_id, invoice_number';

	PRINT @@QUERY;
	EXEC(@@QUERY);
END



GO

