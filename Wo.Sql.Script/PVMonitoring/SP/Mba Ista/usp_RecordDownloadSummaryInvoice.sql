USE [BES]
GO

/****** Object:  StoredProcedure [dbo].[usp_RecordDownloadSummaryInvoice]    Script Date: 7/6/2020 01:44:43 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		wo\dwi
-- Create date: 7 Oktober 2019
-- Description:	Update Notur_downloaded
-- =============================================
CREATE PROCEDURE [dbo].[usp_RecordDownloadSummaryInvoice]
	-- Add the parameters for the stored procedure here
	@dateFrom varchar(15),
	@dateTo varchar(15),
	@businessType varchar(max),
	@customerGroup varchar(5),
	@user varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF @customerGroup = 'ALL-C'
		SET @customerGroup = ''

    INSERT INTO [dbo].[tb_r_history_summary_invoice]
           ([date_from]
           ,[date_to]
           ,[business_type]
           ,[customer_group]
           ,[created_date]
           ,[created_by])
     VALUES
           (@dateFrom
           ,@dateTo
           ,@businessType
           ,@customerGroup
           ,getdate()
           ,@user
	)

END




GO

