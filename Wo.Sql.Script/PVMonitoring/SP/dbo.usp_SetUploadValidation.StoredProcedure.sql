USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[usp_SetUploadValidation]    Script Date: 6/29/2020 10:20:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_SetUploadValidation](
	@pid INT, 
	@uid VARCHAR(20), 
	@itemCode INT = 0, 
	@itemField VARCHAR(20) = ''
) AS
BEGIN
	DECLARE @R INT = 0;
	IF (@itemCode = 0 OR @itemCode = 1) 
	BEGIN 
		UPDATE D
		SET d.ERRMSG = 
			ISNULL(d.ERRMSG, '') 
			+ 'NO:' + ISNULL(convert(varchar, D.SEQ_XCL), '')
			+ ' Invalid Cost Center: ' + D.COST_CENTER_CD  			
			+ ' ;'
		FROM TB_T_UPLOAD_D d
		LEFT JOIN dbo.vw_CostCenterElvis c ON C.COST_CENTER = D.COST_CENTER_CD
		WHERE c.COST_CENTER IS NULL AND NULLIF(LTRIM(RTRIM(d.COST_CENTER_CD)) ,'') IS NOT NULL 
		AND d.PROCESS_ID = @pid;
		
		SET @R = @R + @@ROWCOUNT;
	END; 
	
	IF (@itemCode = 0 OR @itemCode = 2)
	BEGIN 
		UPDATE D 
		SET d.ERRMSG = 
			ISNULL(d.ERRMSG, '') 
			+ 'NO:' + ISNULL(convert(varchar, D.SEQ_XCL), '') 
			+ '  Invalid Currency: ' + D.CURRENCY_CD
			+ ' ;'
		FROM TB_T_UPLOAD_D d 
		WHERE d.PROCESS_ID = @pid
		and d.CURRENCY_CD NOT IN (select CURRENCY_CD from dbo.vw_ExchangeRateElvis);
		
		SET @R = @R + @@ROWCOUNT;
	END; 
	DECLARE @round VARCHAR(1000); 
	DECLARE @X TABLE (CURRENCY_CD VARCHAR(100));
	IF (@itemCode = 0 OR @itemCode >= 4)
	BEGIN
		
		SELECT TOP 1 @round = LTRIM(RTRIM(ISNULL(SYSTEM_VALUE_TXT ,''))) 
		FROM vw_TbMSystemElvis WHERE SYSTEM_TYPE = 'INVOICE_UPLOAD' AND SYSTEM_CD = 'NO_FRACTION_CURRENCY';
		
		INSERT @X 
		SELECT * FROM dbo.udf_explode(',', @round);
	END;
	
	IF (@itemCode = 0 OR @itemCode = 4)
	BEGIN 
		-- VALIDATE ROUND CURRENCIES 
		
		UPDATE D 
		SET d.ERRMSG = 
			ISNULL(d.ERRMSG, '') 
			+ 'NO:' + ISNULL(convert(varchar, D.SEQ_XCL), '') 
			+ ' Amount must be round for Currency: ' + D.CURRENCY_CD
			+ ' ;'
		FROM TB_T_UPLOAD_D d 
		WHERE d.PROCESS_ID = @pid
		AND d.CURRENCY_CD IN (select CURRENCY_CD from @X)
		AND (d.AMOUNT- ROUND(ISNULL(d.AMOUNT,0),0)) > 0;
		
		SET @R = @R + @@ROWCOUNT;
	END;
	
	IF (@itemCode = 0 OR @itemCode = 5)
	BEGIN 
		UPDATE D 
		SET d.ERRMSG = 
			ISNULL(d.ERRMSG, '') 
			+ 'NO:' + ISNULL(convert(varchar, D.SEQ_XCL), '') 
			+ ' Turn Over Amount must be round for Currency: ' + D.CURRENCY_CD
			+ ' ;'
		FROM TB_T_UPLOAD_D d 
		WHERE d.PROCESS_ID = @pid
		AND d.CURRENCY_CD IN (select CURRENCY_CD from @X)
		AND (d.AMOUNT_TURN_OVER - ROUND(ISNULL(d.AMOUNT_TURN_OVER,0),0)) > 0;
		
		SET @R = @R + @@ROWCOUNT;
	END;
	
	IF (@itemCode = 0 OR @itemCode = 6)
	BEGIN 
		UPDATE D 
		SET d.ERRMSG = 
			ISNULL(d.ERRMSG, '') 
			+ 'NO:' + ISNULL(convert(varchar, D.SEQ_XCL), '') 
			+ ' PPN Amount must be round for Currency: ' + D.CURRENCY_CD
			+ ' ;'
		FROM TB_T_UPLOAD_D d 
		WHERE d.PROCESS_ID = @pid
		AND d.CURRENCY_CD IN (select CURRENCY_CD from @X)
		AND (d.AMOUNT_PPN - ROUND(ISNULL(d.AMOUNT_PPN,0),0)) > 0;
		
		SET @R = @R + @@ROWCOUNT;
	END;
	
	IF (@itemCode = 0 OR @itemCode = 7)
	BEGIN 
		UPDATE D 
		SET d.ERRMSG = 
			ISNULL(d.ERRMSG, '') 
			+ 'NO:' + ISNULL(convert(varchar, D.SEQ_XCL), '') 
			+ ' Seal Amount must be round for Currency: ' + D.CURRENCY_CD
			+ ' ;'
		FROM TB_T_UPLOAD_D d 
		WHERE d.PROCESS_ID = @pid
		AND d.CURRENCY_CD IN (select CURRENCY_CD from @X)
		AND (d.AMOUNT_SEAL - ROUND(ISNULL(d.AMOUNT_SEAL,0),0)) > 0;
		
		SET @R = @R + @@ROWCOUNT;
	END;
	
	IF (@itemCode = 0 OR @itemCode = 8)
	BEGIN 
		UPDATE D 
		SET d.ERRMSG = 
			ISNULL(d.ERRMSG, '') 
			+ 'NO:' + ISNULL(convert(varchar, D.SEQ_XCL), '') 
			+ ' DPP or PPH Amount must be round for Currency: ' + D.CURRENCY_CD
			+ ' ;'
		FROM TB_T_UPLOAD_D d 
		WHERE d.PROCESS_ID = @pid
		AND d.CURRENCY_CD IN (select CURRENCY_CD from @X)
		AND (d.DPP_AMOUNT - ROUND(ISNULL(d.DPP_AMOUNT,0),0)) > 0;
		
		SET @R = @R + @@ROWCOUNT;
	END;
	
	IF (@itemCode = 0 OR @itemCode = 9)
	BEGIN 
		UPDATE D 
		SET d.ERRMSG = 
			ISNULL(d.ERRMSG, '') 
			+ 'NO:' + ISNULL(convert(varchar, D.SEQ_XCL), '') 
			+ ' PPH 21 Intern Amount must be round for Currency: ' + D.CURRENCY_CD
			+ ' ;'
		FROM TB_T_UPLOAD_D d 
		WHERE d.PROCESS_ID = @pid
		AND d.CURRENCY_CD IN (select CURRENCY_CD from @X)
		AND (d.AMOUNT_PPh21_INTERN - ROUND(ISNULL(d.AMOUNT_PPh21_INTERN,0),0)) > 0;
		
		SET @R = @R + @@ROWCOUNT;
	END;

	IF (@itemCode = 0 OR @itemCode = 10)
	BEGIN 
		UPDATE D 
		SET d.ERRMSG = 
			ISNULL(d.ERRMSG, '') 
			+ 'NO:' + ISNULL(convert(varchar, D.SEQ_XCL), '') 			
			+ ' PPH 21 Extern Amount must be round for Currency: ' + D.CURRENCY_CD
			+ ' ;'
		FROM TB_T_UPLOAD_D d 
		WHERE d.PROCESS_ID = @pid
		AND d.CURRENCY_CD IN (select CURRENCY_CD from @X)
		AND (d.AMOUNT_PPh21_EXTERN - ROUND(ISNULL(d.AMOUNT_PPh21_EXTERN,0),0)) > 0;
		
		SET @R = @R + @@ROWCOUNT;
	END;

	
	IF (@itemCode = 0 OR @itemCode = 11)
	BEGIN 
		UPDATE D 
		SET d.ERRMSG = 
			ISNULL(d.ERRMSG, '') 
			+ 'NO:' + ISNULL(convert(varchar, D.SEQ_XCL), '') 
			+ ' PPH 26 Amount must be round for Currency: ' + D.CURRENCY_CD
			+ ' ;'
		FROM TB_T_UPLOAD_D d 
		WHERE d.PROCESS_ID = @pid
		AND d.CURRENCY_CD IN (select CURRENCY_CD from @X)
		AND (d.AMOUNT_PPh26 - ROUND(ISNULL(d.AMOUNT_PPh26,0),0)) > 0;
		
		SET @R = @R + @@ROWCOUNT;
	END;
	
	IF (@itemCode = 0 OR @itemCode = 12)
	BEGIN 
		UPDATE D 
		SET d.ERRMSG = 
			ISNULL(d.ERRMSG, '') 
			+ 'NO:' + ISNULL(convert(varchar, D.SEQ_XCL), '') 
			+ ' PPH Final Amount must be round for Currency: ' + D.CURRENCY_CD
			+ ' ;'
		FROM TB_T_UPLOAD_D d 
		WHERE d.PROCESS_ID = @pid
		AND d.CURRENCY_CD IN (select CURRENCY_CD from @X)
		AND (d.AMOUNT_PPh_FINAL - ROUND(ISNULL(d.AMOUNT_PPh_FINAL,0),0)) > 0;
		
		SET @R = @R + @@ROWCOUNT;
	END;

	-- Modify EFB
	IF (@itemCode = 0 OR @itemCode = 13)
	BEGIN 
		UPDATE D 
		SET d.ERRMSG = 
			ISNULL(d.ERRMSG, '') 
			+ 'NO:' + ISNULL(convert(varchar, D.SEQ_XCL), '') 
			+ ' Invalid TAX Code inputted : ' + D.TAX_CODE_PPh23
			+ ' Article 23 (Please Input tax code related to article 23) ;'
		FROM TB_T_UPLOAD_D d 
		WHERE d.PROCESS_ID = @pid
		and TAX_CODE_PPh23 not in(
		select distinct TransaksiTypeCode from ELVIS_DB.dbo.fn_GetTransaksiTypeCode('23') WHERE ESPTType <> 'PPh Badan'
		) and isnull(TAX_CODE_PPh23,'') <> '' ;
		
		SET @R = @R + @@ROWCOUNT;

		UPDATE D 
		SET d.ERRMSG = 
			ISNULL(d.ERRMSG, '') 
			+ 'NO:' + ISNULL(convert(varchar, D.SEQ_XCL), '') 
			+ ' Amount ' + D.TAX_CODE_PPh23
			+ ' must be filled ;'
		FROM TB_T_UPLOAD_D d 
		WHERE d.PROCESS_ID = @pid
		and isnull(TAX_CODE_PPh23,'') <> ''
		and DPP_AMOUNT is null
		
		SET @R = @R + @@ROWCOUNT;

	END;
	IF (@itemCode = 0 OR @itemCode = 14)
	BEGIN 
		UPDATE D 
		SET d.ERRMSG = 
			ISNULL(d.ERRMSG, '') 
			+ 'NO:' + ISNULL(convert(varchar, D.SEQ_XCL), '') 
			+ ' Invalid TAX Code Inputted: ' + D.TAX_CODE_PPh21_EXTERN
			+ ' Article 21 (Please Input tax code related to article 21);'
		FROM TB_T_UPLOAD_D d 
		WHERE d.PROCESS_ID = @pid
		and TAX_CODE_PPh21_EXTERN not in(
		select distinct TransaksiTypeCode from ELVIS_DB.dbo.fn_GetTransaksiTypeCode('21') WHERE ESPTType <> 'PPh Badan'
		) and isnull(TAX_CODE_PPh21_EXTERN,'')<> '';
		
		SET @R = @R + @@ROWCOUNT;
		
		UPDATE D 
		SET d.ERRMSG = 
			ISNULL(d.ERRMSG, '') 
			+ 'NO:' + ISNULL(convert(varchar, D.SEQ_XCL), '') 
			+ ' Amount ' + D.TAX_CODE_PPh21_EXTERN
			+ ' must be filled ;'
		FROM TB_T_UPLOAD_D d 
		WHERE d.PROCESS_ID = @pid
		and isnull(TAX_CODE_PPh21_EXTERN,'') <> ''
		and AMOUNT_PPh21_EXTERN is null

		SET @R = @R + @@ROWCOUNT;

	END;
	IF (@itemCode = 0 OR @itemCode = 15)
	BEGIN 
		UPDATE D 
		SET d.ERRMSG = 
			ISNULL(d.ERRMSG, '') 
			+ 'NO:' + ISNULL(convert(varchar, D.SEQ_XCL), '') 
			+ ' Invalid TAX Code Inputted:  ' + D.TAX_CODE_PPh26
			+ ' Article 26 (Please Input tax code related to article 26);'
		FROM TB_T_UPLOAD_D d 
		WHERE d.PROCESS_ID = @pid
		and TAX_CODE_PPh26 not in(
		select distinct TransaksiTypeCode from ELVIS_DB.dbo.fn_GetTransaksiTypeCode('26') WHERE ESPTType <> 'PPh Badan'
		) and isnull(TAX_CODE_PPh26,'') <> ''  --and AMOUNT_PPh26 is not null;
		
		SET @R = @R + @@ROWCOUNT;

		UPDATE D 
		SET d.ERRMSG = 
			ISNULL(d.ERRMSG, '') 
			+ 'NO:' + ISNULL(convert(varchar, D.SEQ_XCL), '') 
			+ ' Amount ' + D.TAX_CODE_PPh26
			+ ' must be filled ;'
		FROM TB_T_UPLOAD_D d 
		WHERE d.PROCESS_ID = @pid
		and isnull(TAX_CODE_PPh26,'') <> ''
		and AMOUNT_PPh26 is null 

		SET @R = @R + @@ROWCOUNT;
	END;
	IF (@itemCode = 0 OR @itemCode = 16)
	BEGIN 
		UPDATE D 
		SET d.ERRMSG = 
			ISNULL(d.ERRMSG, '') 
			+ 'NO:' + ISNULL(convert(varchar, D.SEQ_XCL), '') 
			+ ' Invalid TAX Code Inputted: ' + D.TAX_CODE_PPh_FINAL
			+ ' Article 4 ayat 2 (Please Input tax code related to article 4 ayat 2);'
		FROM TB_T_UPLOAD_D d 
		WHERE d.PROCESS_ID = @pid
		and TAX_CODE_PPh_FINAL not in(
		select distinct TransaksiTypeCode from  ELVIS_DB.dbo.fn_GetTransaksiTypeCode('4 ayat 2') WHERE ESPTType <> 'PPh Badan'
		) and isnull(TAX_CODE_PPh_FINAL,'') <> '';
		
		SET @R = @R + @@ROWCOUNT;

		UPDATE D 
		SET d.ERRMSG = 
			ISNULL(d.ERRMSG, '') 
			+ 'NO:' + ISNULL(convert(varchar, D.SEQ_XCL), '') 
			+ ' Amount ' + D.TAX_CODE_PPh_FINAL
			+ ' must be filled ;'
		FROM TB_T_UPLOAD_D d 
		WHERE d.PROCESS_ID = @pid
		and isnull(TAX_CODE_PPh_FINAL,'') <> ''
		and AMOUNT_PPh_FINAL is null

		SET @R = @R + @@ROWCOUNT;
	END;

	IF (@itemCode = 0 OR @itemCode = 17)--additional info pph 21 external (text)
	BEGIN 
		UPDATE D 
		SET d.ERRMSG = 
			ISNULL(d.ERRMSG, '') 
			+ 'NO:' + ISNULL(convert(varchar, D.SEQ_XCL), '') 
			+ ' ' + b.DESCRIPTION + ' must be filled ' + b.TextType + ' Max ' + b.LengthType + ' ' + b.DataType
			+ ';'
		From TB_T_UPLOAD_D d
		join ELVIS_DB.dbo.fn_GetTransaksiTypeCode('21') b
		on d.TAX_CODE_PPh21_EXTERN = b.TransaksiTypeCode
		AND b.ESPTType <> 'PPh Badan'
		where d.PROCESS_ID = @pid
		and ISNULL(b.TextType,'') = 'Text' 
		and (len(d.NIK_PPh21_EXTERN) < 1 or len(d.NIK_PPh21_EXTERN) > 
				( case when isnull(b.LengthType,'') = '' then 0 else b.LengthType end)
			)
		and ISNULL(b.DefaultValue,'') = ''
		and d.AMOUNT_PPh21_EXTERN is not null;
		
		SET @R = @R + @@ROWCOUNT;
	END;
	IF (@itemCode = 0 OR @itemCode = 18)--additional info pph final (text)
	BEGIN 
		UPDATE D 
		SET d.ERRMSG = 
			ISNULL(d.ERRMSG, '') 
			+ 'NO: ' + ISNULL(convert(varchar, D.SEQ_XCL), '') 
			+ ' ' + b.DESCRIPTION + ' must be filled ' + b.TextType + ' Max ' + b.LengthType + ' ' + b.DataType
			+ ';'
		From TB_T_UPLOAD_D d
		join ELVIS_DB.dbo.fn_GetTransaksiTypeCode('4 ayat 2') b
		on d.TAX_CODE_PPh_FINAL = b.TransaksiTypeCode
		AND b.ESPTType <> 'PPh Badan'
		where d.PROCESS_ID = @pid
		and ISNULL(b.TextType,'') = 'Text' 
		and (len(d.INFO_PPh_FINAL) < 1 or len(d.INFO_PPh_FINAL) > 
				( case when isnull(b.LengthType,'') = '' then 0 else b.LengthType end)
			)
		and ISNULL(b.DefaultValue,'') = ''
		and d.AMOUNT_PPh_FINAL is not null;
		
		SET @R = @R + @@ROWCOUNT;
	END;
	IF (@itemCode = 0 OR @itemCode = 19)--additional info pph 21 external (option)
	BEGIN 
		UPDATE D 
		SET d.ERRMSG = 
			ISNULL(d.ERRMSG, '') 
			+ 'NO: ' + ISNULL(convert(varchar, D.SEQ_XCL), '') 
			+ ' ' + b.DESCRIPTION + ' must be filled ' + b.TextType + ' ' + c.OptionValue
			+ ';'
		From TB_T_UPLOAD_D d
		join ELVIS_DB.dbo.fn_GetTransaksiTypeCode('21') b
		on d.TAX_CODE_PPh21_EXTERN = b.TransaksiTypeCode
		AND b.ESPTType <> 'PPh Badan'
		outer apply ELVIS_DB.dbo.fn_GetAdditionalOption(b.TransaksiTypeCode) c
		where d.PROCESS_ID = @pid
		and ISNULL(b.TextType,'') = 'Option' 
		and isnull(c.OptionValue,'') <> ''
		and (	isnull(d.NIK_PPh21_EXTERN,'') = '' 
				or d.NIK_PPh21_EXTERN not in(select * from dbo.udf_SplitString(c.OptionValue,',')) 
			)
		and ISNULL(b.DefaultValue,'') = ''
		and d.AMOUNT_PPh21_EXTERN is not null;
		
		SET @R = @R + @@ROWCOUNT;
	END;
	IF (@itemCode = 0 OR @itemCode = 20)--additional info pph final (option)
	BEGIN 
		UPDATE D 
		SET d.ERRMSG = 
			ISNULL(d.ERRMSG, '') 
			+ 'NO: ' + ISNULL(CONVERT(VARCHAR, D.SEQ_XCL), '') 
			+ ' ' + b.DESCRIPTION + ' must be filled ' + b.TextType + ' ' + c.OptionValue
			+ ';'
		FROM TB_T_UPLOAD_D d
		JOIN ELVIS_DB.dbo.fn_GetTransaksiTypeCode('4 ayat 2') b
		ON d.TAX_CODE_PPh_FINAL = b.TransaksiTypeCode
		AND b.ESPTType <> 'PPh Badan'
		OUTER APPLY ELVIS_DB.dbo.fn_GetAdditionalOption(b.TransaksiTypeCode) c
		WHERE d.PROCESS_ID = @pid
		AND ISNULL(b.TextType,'') = 'Option' 
		AND ISNULL(c.OptionValue,'') <> ''
		AND (	ISNULL(d.INFO_PPh_FINAL,'') = '' 
				OR d.INFO_PPh_FINAL NOT IN(SELECT * FROM dbo.udf_SplitString(c.OptionValue,',')) 
			)
		AND ISNULL(b.DefaultValue,'') = ''
		AND d.AMOUNT_PPh_FINAL IS NOT NULL;
		
		SET @R = @R + @@ROWCOUNT;
	END;
	IF (@itemCode = 0 OR @itemCode = 21)--DPP PPh null
	BEGIN 
		UPDATE D 
		SET d.ERRMSG = 
			ISNULL(d.ERRMSG, '') 
			+ 'NO: ' + ISNULL(CONVERT(VARCHAR, D.SEQ_XCL), '') 
			+ ' DPP PPh must be filled;'
		FROM TB_T_UPLOAD_D d
		WHERE d.PROCESS_ID = @pid
		AND DPP_PPh_AMOUNT IS NULL
		AND (
			d.DPP_AMOUNT IS NOT NULL 
			OR d.AMOUNT_PPh21_EXTERN IS NOT NULL 
			OR d.AMOUNT_PPh22 IS NOT NULL 
			OR d.AMOUNT_PPh26 IS NOT NULL
			OR d.AMOUNT_PPh_FINAL IS NOT NULL
			);
		
		SET @R = @R + @@ROWCOUNT;
	END;

	IF (@itemCode = 0 OR @itemCode = 22)--Duplicate Tax No 'Excel'
	BEGIN 
		UPDATE D 
		SET d.ERRMSG = 
			ISNULL(d.ERRMSG, '') 
			+ 'NO: ' + ISNULL(CONVERT(VARCHAR, D.SEQ_XCL), '') 
			+ ' Duplicate Tax No;'
		FROM TB_T_UPLOAD_D d
		WHERE d.PROCESS_ID = @pid
		AND TAX_NO IN(
			select DISTINCT TAX_NO as total
			from TB_T_UPLOAD_D 
			where PROCESS_ID = @pid
			and isnull(TAX_NO,'') <> ''
			group by TAX_NO
			having count(1)>1
		);
		SET @R = @R + @@ROWCOUNT;

	END;

	IF (@itemCode = 0 OR @itemCode = 23)--cek NPWP
	BEGIN 

		--UPDATE D 
		--SET d.ERRMSG = 
		--ISNULL(d.ERRMSG, '') 
		--+ 'NO: ' + ISNULL(CONVERT(VARCHAR, D.SEQ_NO), '') 
		--+ ' Invalid Input (Please Input YES or NO on "NPWP Available" column);'
		--from tb_t_upload_h h
		--join tb_t_upload_d d
		--on h.process_id = d.process_id
		--and h.process_id = @pid
		--and ISNULL(d.NPWP_AVAILABLE,'') = ''
		--AND ( 
		--		ISNULL(TAX_CODE_PPh23,'')<>'' 
		--		OR ISNULL(TAX_CODE_PPh21_EXTERN,'')<>'' 
		--		OR ISNULL(TAX_CODE_PPh26,'')<>'' 
		--		OR ISNULL(TAX_CODE_PPh_FINAL,'')<>''
		--	)

		--SET @R = @R + @@ROWCOUNT;

		declare @NPWP varchar(50)--,@pid varchar(50) = '1091424'
		select top 1 @NPWP = isnull(V.NPWP,'') from ELVIS_DB.dbo.vw_vendor v
		join tb_t_upload_h h
		on isnull(v.VENDOR_CD,'') = isnull(h.VENDOR_CD,'')
		where h.process_id=@pid
		if (@NPWP != '')
		--begin
		--	UPDATE D 
		--	SET d.ERRMSG = 
		--	ISNULL(d.ERRMSG, '') 
		--	+ 'NO: ' + ISNULL(CONVERT(VARCHAR, D.SEQ_NO), '') 
		--	+ ' NPWP status mismatch with ELVIS vendor data (Please Input NO on "NPWP Available" column);'
		--	from tb_t_upload_h h
		--	join tb_t_upload_d d
		--	on h.process_id = d.process_id
		--	and h.process_id = @pid
		--	and ISNULL(d.NPWP_AVAILABLE,'') = 'YES'
		--	and ISNULL(d.NPWP_AVAILABLE,'') <> ''
		--	AND ( 
		--		ISNULL(TAX_CODE_PPh23,'')<>'' 
		--		OR ISNULL(TAX_CODE_PPh21_EXTERN,'')<>'' 
		--		OR ISNULL(TAX_CODE_PPh26,'')<>'' 
		--		OR ISNULL(TAX_CODE_PPh_FINAL,'')<>''
		--	)
		--end
		--else
		begin
			UPDATE D 
			SET d.ERRMSG = 
			ISNULL(d.ERRMSG, '') 
			+ 'NO: ' + ISNULL(CONVERT(VARCHAR, D.SEQ_XCL), '') 
			+ ' NPWP status mismatch with ELVIS vendor data (Please check data Vendor Master);'
			from tb_t_upload_h h
			join tb_t_upload_d d
			on h.process_id = d.process_id
			and h.process_id = @pid
			and ISNULL(d.NPWP_AVAILABLE,'') <> 'YES'
			--and ISNULL(d.NPWP_AVAILABLE,'') <> '' blank
			AND ( 
				ISNULL(TAX_CODE_PPh23,'')<>'' 
				OR ISNULL(TAX_CODE_PPh21_EXTERN,'')<>'' 
				OR ISNULL(TAX_CODE_PPh26,'')<>'' 
				OR ISNULL(TAX_CODE_PPh_FINAL,'')<>''
			)
		end

		SET @R = @R + @@ROWCOUNT;
	END;

	IF (@itemCode = 0 OR @itemCode = 24)--expired Date PIB
	BEGIN 
		UPDATE D 
		SET d.ERRMSG = 
			ISNULL(d.ERRMSG, '') 
			+ 'NO: ' + ISNULL(CONVERT(VARCHAR, D.SEQ_XCL), '') 
			+ ' Tax Date '+d.TAX_DT+' expired;'
		FROM TB_T_UPLOAD_D d
		WHERE d.PROCESS_ID = @pid
		AND isnull(TAX_DT,'') <> ''
		AND EOMONTH(dateadd(month,3, ELVIS_DB.dbo.fn_DtImport(TAX_DT))) <= getdate();
		
		SET @R = @R + @@ROWCOUNT;
	END;
	IF (@itemCode = 0 OR @itemCode = 25)--Tax No create by Imron
	BEGIN 
		UPDATE D 
		SET d.ERRMSG = 
			ISNULL(d.ERRMSG, '') 
			+ 'NO: ' + ISNULL(convert(varchar, D.SEQ_XCL), '') 
			+ ' Tax No. must be filled Text 16 Character'
			+ ';'
		From TB_T_UPLOAD_D d
		where d.PROCESS_ID = @pid AND len(ELVIS_DB.dbo.RemoveSpecialChars(LTRIM(RTRIM(ISNULL(d.TAX_NO,'')))))<>16
		SET @R = @R + @@ROWCOUNT;
	END;

	RETURN @R;
END;







GO
