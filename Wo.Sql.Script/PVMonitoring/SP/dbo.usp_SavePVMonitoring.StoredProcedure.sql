USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[usp_SavePVMonitoring]    Script Date: 6/29/2020 10:20:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[usp_SavePVMonitoring]
	-- Add the parameters for the stored procedure here
	 @process_status int OUTPUT
	,@err_mesg varchar(2000) OUTPUT
	,@pidi int
	,@seq_no int
	,@seq_xcl int
	,@invoice_no varchar(16)
    ,@invoice_date varchar(25)
    ,@tax_no varchar(25)
    ,@tax_dt varchar(25)
    ,@currency_cd varchar(10)
    ,@amount_turn_over numeric(18,2)
    ,@amount_ppn numeric(18,2)
    ,@materai numeric(18,2) --tidak ada dikolom
    ,@amount numeric(18,2)
    ,@description varchar(50)
    ,@account_info varchar(16)
    ,@cost_center_cd varchar(10)
    ,@dpp_pph_amount numeric(18,2)
    ,@npwp_available varchar(50)
    ,@tax_code_pph23 varchar(10)
    ,@tax_tariff_pph23 numeric(16,4)
    ,@amount_pph23 numeric(18,2) --tidak ada dikolom
    ,@amount_pph21_intern numeric(18,2)
    ,@tax_code_pph21_extern varchar(10)
    ,@tax_dpp_accu_pph21_extern numeric(18,2)
    ,@tax_tariff_pph21_extern numeric(16,4)
    ,@amount_pph21_extern numeric(18,2)
    ,@nik_pph21_extern varchar(50)
    ,@tax_code_pph26 varchar(10)
    ,@tax_tariff_pph26 numeric(16,4)
    ,@amount_pph26 numeric(18,2)
    ,@tax_code_pph_final varchar(10)
    ,@tax_tariff_pph_final numeric(16,4)
    ,@amount_pph_final numeric(18,2)
    ,@info_pph_final varchar(500)
    ,@user varchar(50)
AS
BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--set @user = replace(@user,'\\','\');

	declare
		@log VARCHAR(8000), 
		@pid INT = @pidi,
		@ErrInsDist VARCHAR(40),
		@na VARCHAR(50) = 'usp_SavePVMonitoring';

	set @log = 
		'@pid:' + isnull(cast(@pidi as varchar),'') + ', ' +
		'@seq_no:' + isnull(cast(@seq_no as varchar),'') + ', ' +
		'@invoice_no:' + isnull(@invoice_no, '') + ', ' +  
		'@invoice_date:' + isnull(@invoice_date, '') + ', ' +  
		'@tax_no:' + isnull(@tax_no, '') + ', ' +  
		'@tax_dt:' + isnull(@tax_dt, '') + ', ' +  
		'@currency_cd:' + isnull(@currency_cd, '') + ', ' +  
		'@amount_turn_over:' + isnull(cast(@amount_turn_over as varchar),'') + ', ' +
		'@amount_ppn:' + isnull(cast(@amount_ppn as varchar),'') + ', ' +
		'@materai:' + isnull(cast(@materai as varchar),'') + ', ' +
		'@amount:' + isnull(cast(@amount as varchar),'') + ', ' +
		'@description:' + isnull(@description, '') + ', ' +  
		'@account_info:' + isnull(cast(@account_info as varchar),'') + ', ' +
		'@cost_center_cd:' + isnull(@cost_center_cd, '') + ', ' +  
		'@dpp_pph_amount:' + isnull(cast(@dpp_pph_amount as varchar),'') + ', ' +
		'@npwp_available:' + isnull(@npwp_available, '') + ', ' +  
		'@tax_code_pph23:' + isnull(@tax_code_pph23, '') + ', ' +  
		'@tax_tariff_pph23:' + isnull(cast(@tax_tariff_pph23 as varchar),'') + ', ' +
		'@amount_pph23:' + isnull(cast(@amount_pph23 as varchar),'') + ', ' +
		'@amount_pph21_intern:' + isnull(cast(@amount_pph21_intern as varchar),'') + ', ' +
		'@tax_code_pph21_extern:' + isnull(@tax_code_pph21_extern, '') + ', ' +  
		'@tax_dpp_accu_pph21_extern:' + isnull(cast(@tax_dpp_accu_pph21_extern as varchar),'') + ', ' +
		'@tax_tariff_pph21_extern:' + isnull(cast(@tax_tariff_pph21_extern as varchar),'') + ', ' +
		'@amount_pph21_extern:' + isnull(cast(@amount_pph21_extern as varchar),'') + ', ' +
		'@nik_pph21_extern:' + isnull(@nik_pph21_extern, '') + ', ' +  
		'@tax_code_pph26:' + isnull(@tax_code_pph26, '') + ', ' +  
		'@tax_tariff_pph26:' + isnull(cast(@tax_tariff_pph26 as varchar),'') + ', ' +
		'@amount_pph26:' + isnull(cast(@amount_pph26 as varchar),'') + ', ' +
		'@tax_code_pph_final:' + isnull(@tax_code_pph_final, '') + ', ' +  
		'@tax_tariff_pph_final:' + isnull(cast(@tax_tariff_pph_final as varchar),'') + ', ' +
		'@amount_pph_final:' + isnull(cast(@amount_pph_final as varchar),'') + ', ' +
		'@info_pph_final:' + isnull(@info_pph_final, '') + ', ' +  
		'@user:' + isnull(@user, '');
		
	exec [usp_PutLog] @log, @user, @na, @pidi, 'INF', 'INF';

	if (
		/*
		select isnull(b.div_id,'0')
		from tb_m_security_users a
		left join tb_m_mapp_div b on a.departement_id = b.department_id
		where username = @user
		*/
		select distinct isnull(isnull(aa.div_id,bb.div_id),'0')
		from (
			select b.div_id, a.username
			from tb_m_security_users a
			left join tb_m_mapp_div b on a.departement_id = b.department_id
			where a.username = @user
		) aa
		outer apply
		(
			select b.div_id, b.department_id, b.[username] 
			from tb_m_mapp_div b
			where b.[username] = aa.[username]
		) bb
	) <= 0
	begin
		set @process_status = 1
		set @err_mesg = 'User departement not authorized to Upload Letter of Disbursement'

		exec usp_PutLog @err_mesg, @user, @na, @pid output, 'ERR', 'ERR';
	end

	if @process_status <> 0
	begin
		return		
	end

	set @user = replace(@user,'TAM\','');

	if not exists (
		select top 1 1
		from vw_UserElvis a
		left join 
		(
			select replace(username,'TAM\','')[username]
			from bes.dbo.tb_m_security_users
			where username is not null
		) b
		on a.username = b.username
		where b.username = @user and role_id = 'ELVIS_SECRETARY'
		and b.username is not null
	)
	begin
		set @process_status = 1
		set @err_mesg = 'User not authorized to Upload Letter of Disbursement'

		exec usp_PutLog @err_mesg, @user, @na, @pid output, 'ERR', 'ERR';
	end

	if @process_status <> 0
	begin
		return
	end

	if exists(
		select top 1 1 
		from tb_r_pv_h a
		join tb_r_pv_d b on a.pv_no = b.pv_no
							and b.invoice_no = @invoice_no
		where 1=1
			and status_cd != '7'
	)
	begin
		set @process_status = 1
		set @err_mesg = 'Invoice no: ' +@invoice_no+ ' already exists!'
		
		exec usp_PutLog @err_mesg, @user, @na, @pid output, 'ERR', 'ERR';
	end
	else
	begin
		insert into [dbo].[tb_t_upload_d]
			   (
				 process_id
				,seq_no
				,seq_xcl
				,invoice_no
				,invoice_date
				,tax_no
				,tax_dt
				,currency_cd
				,amount_turn_over
				,amount_ppn
				,amount_seal
				,amount
				,[description]
				,account_info
				,cost_center_cd
				,dpp_pph_amount
				,npwp_available
				,tax_code_pph23
				,tax_tariff_pph23
				,dpp_amount
				,amount_pph21_intern
				,tax_code_pph21_extern
				,tax_dpp_accumulation_pph21_extern
				,tax_tariff_pph21_extern
				,amount_pph21_extern
				,nik_pph21_extern
				,tax_code_pph26
				,tax_tariff_pph26
				,amount_pph26
				,tax_code_pph_final
				,tax_tariff_pph_final
				,amount_pph_final
				,info_pph_final
				,created_by
				,created_dt
				--,status_flag
			   )
		 values
			   (
				 @pidi
				,@seq_no
				,@seq_xcl
				,@invoice_no
				,@invoice_date
				,@tax_no
				,@tax_dt
				,@currency_cd
				,@amount_turn_over
				,@amount_ppn
				,@materai
				,@amount
				,@description
				,@account_info
				,@cost_center_cd
				,@dpp_pph_amount
				,@npwp_available
				,@tax_code_pph23
				,@tax_tariff_pph23
				,@amount_pph23
				,@amount_pph21_intern
				,@tax_code_pph21_extern
				,@tax_dpp_accu_pph21_extern
				,@tax_tariff_pph21_extern
				,@amount_pph21_extern
				,@nik_pph21_extern
				,@tax_code_pph26
				,@tax_tariff_pph26
				,@amount_pph26
				,@tax_code_pph_final
				,@tax_tariff_pph_final
				,@amount_pph_final
				,@info_pph_final
				,@user
				,getdate()
				--,1
			   )

		if @@ROWCOUNT >= 1
		begin
			set @err_mesg = 'Inserted data tb_t_upload_d successfully';
			exec usp_PutLog @err_mesg, @user, @na, @pid output, 'INF', 'INF';
		end
	end

END TRY
BEGIN CATCH	
	
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	
	SELECT @ErrorMessage = ERROR_MESSAGE(),
		   @ErrorSeverity = ERROR_SEVERITY(),
		   @ErrorState = ERROR_STATE(),
		   @ErrorLine = ERROR_LINE()

	SET @process_status = 2	
	SET @err_mesg = 'ERROR: usp_SavePVMonitoring: ' + @ErrorMessage + ', at line = ' +  cast (@ErrorLine as varchar)

	exec usp_PutLog @err_mesg, @user, @na, @pid output, 'CATCH', 'ERR';

	RETURN 
	
END CATCH





GO
