USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[efb_sp_val_tax_no]    Script Date: 6/29/2020 10:20:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE proc [dbo].[efb_sp_val_tax_no]
(
@PV_NO varchar(100),--'=0, 
@TAX_NO varchar(500)-- = '010.002.14.27128866; '
)
as

--SELECT TOP 100 * FROM TB_R_PV_D ORDER BY CREATED_DT DESC
--exec efb_sp_val_tax_no '0','010.000-17.00721986'
--declare @PV_NO int=0, 
--@TAX_NO varchar(500)= '010.000-17.00365379;010.000-17.01421809'

declare @count int
, @result varchar(250) = ''
, @resultTaxNo varchar(500)=''

DECLARE @ListTaxNo TABLE
(
TAX_NO               VARCHAR(50)
)

insert into @ListTaxNo
select RTRIM(LTRIM(ColumnData)) from dbo.udf_SplitString(@TAX_NO,';') where isnull(ColumnData,'') <> ''

DECLARE @DuplicateTaxNo TABLE
(
TAX_NO               VARCHAR(50),
PV_NO varchar(100)
)

--ELVIS
if @PV_NO ='0'
begin
print 1
	insert into @DuplicateTaxNo
	select distinct TAX_NO, D.PV_NO from 
	TB_R_PV_H H
	inner JOIN TB_R_PV_D D
	on h.PV_NO = d.PV_NO 
	where RTRIM(LTRIM(TAX_NO)) in(
	select TAX_NO from @ListTaxNo
	)
	and (ISNULL(h.CANCEL_FLAG,0) = 0 AND ISNULL(h.DELETED,0) = 0)
end
else
begin
print 2
	insert into @DuplicateTaxNo
	select distinct TAX_NO, D.PV_NO from 
	TB_R_PV_H H
	JOIN TB_R_PV_D D
	on h.PV_NO = d.PV_NO 
	where RTRIM(LTRIM(TAX_NO)) in(
	select TAX_NO from @ListTaxNo
	)
	and h.PV_NO <> @PV_NO
	and (ISNULL(h.CANCEL_FLAG,0) = 0 AND ISNULL(h.DELETED,0) = 0)
end

select @count = count(1) from @DuplicateTaxNo

if(@count > 0)
begin
	--set @resultTaxNo = (select TAX_NO + ' ' + case when isnull(PV_NO,'') = '' then PV_NO else 'in PV ' + PV_NO end + '; ' from @DuplicateTaxNo order by TAX_NO FOR XML PATH(''))
	--set @result = 'Tax Invoice '+@resultTaxNo+' Already Used in ELVIS';
	set @result = 'error'
	select 'Tax Invoice '+TAX_NO + ' ' + case when isnull(PV_NO,'') = '' then PV_NO else 'in PV ' + PV_NO end + ' Already Used in E-Billing' as Result from @DuplicateTaxNo order by TAX_NO
end

--EFB
else IF(@PV_NO ='0')
begin
print 3
	insert into @DuplicateTaxNo 
	--EFAKTUR
	select distinct NOMORFAKTURGABUNGAN, ISNULL(PVNUMBER,'') FROM --EFAKTUR_SERVER.
	TAM_EFAKTUR.dbo.TB_R_VATIN V
	WHERE dbo.udf_GetNumeric(NOMORFAKTURGABUNGAN) IN(
		select dbo.udf_GetNumeric(TAX_NO) from @ListTaxNo
	) AND ISNULL(StatusInvoice,'') <> 'CANCELLED'
	AND ISNULL(PVNUMBER,'') <> ''
	AND ISNULL(RowStatus,0) = 0
	AND FakturType = 'EFAKTUR'
	UNION ALL
	--NON EFAKTUR
	select distinct NOMORFAKTURGABUNGAN, ISNULL(PVNUMBER,'') FROM --EFAKTUR_SERVER.
	TAM_EFAKTUR.dbo.TB_R_VATIN V
	WHERE NOMORFAKTURGABUNGAN IN(
		select TAX_NO from @ListTaxNo
	) AND ISNULL(StatusInvoice,'') <> 'CANCELLED'
	AND ISNULL(PVNUMBER,'') <> ''
	AND ISNULL(RowStatus,0) = 0
	AND FakturType = 'NON EFAKTUR'  
end
else IF( @PV_NO <> '0')
begin
print 4
	insert into @DuplicateTaxNo 
	--EFAKTUR
	select distinct NOMORFAKTURGABUNGAN, ISNULL(PVNUMBER,'') FROM --EFAKTUR_SERVER.
	TAM_EFAKTUR.dbo.TB_R_VATIN V
	WHERE dbo.udf_GetNumeric(NOMORFAKTURGABUNGAN) IN(
		select dbo.udf_GetNumeric(TAX_NO) from @ListTaxNo
	) 
	AND ISNULL(StatusInvoice,'') <> 'CANCELLED'
	AND ISNULL(V.PVNumber,'') not in(@PV_NO,'')
	AND ISNULL(RowStatus,0) = 0 
	AND FakturType = 'EFAKTUR'
	UNION ALL
	--NON EFAKTUR
	select distinct NOMORFAKTURGABUNGAN, ISNULL(PVNUMBER,'') FROM --EFAKTUR_SERVER.
	TAM_EFAKTUR.dbo.TB_R_VATIN V
	WHERE NOMORFAKTURGABUNGAN IN(
		select TAX_NO from @ListTaxNo
	) 
	AND ISNULL(StatusInvoice,'') <> 'CANCELLED'
	AND ISNULL(V.PVNumber,'') not in(@PV_NO,'')
	AND ISNULL(RowStatus,0) = 0 
	AND FakturType = 'NON EFAKTUR'
end

select @count = count(1) from @DuplicateTaxNo
if(@count > 0 and @result ='')
begin
	--set @resultTaxNo = (select TAX_NO + ' ' + case when isnull(PV_NO,'') = '' then PV_NO else 'in PV ' + PV_NO end + '; ' from @DuplicateTaxNo order by TAX_NO FOR XML PATH(''))
	select 'Tax Invoice '+TAX_NO + ' ' + case when isnull(PV_NO,'') = '' then PV_NO else 'in PV ' + PV_NO end + ' Already Used in EFB' as Result from @DuplicateTaxNo order by TAX_NO
	--set @result = 'Tax Invoice '+@resultTaxNo+' Already Used in EFB';
end

if @count = 0
begin
	select '' as Result
end
--select @result as Result




GO
