USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[usp_SaveBankAttachmentInfo]    Script Date: 6/29/2020 10:20:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_SaveBankAttachmentInfo] 
	 @vendorCd varchar(10)
    ,@directory varchar(75)
    ,@fileName varchar(75)
    ,@guidFilename varchar(75)
    ,@username varchar(20)
AS
BEGIN 
	SET NOCOUNT ON;

	INSERT TB_T_BANK_ATTACHMENT (
			VENDOR_CD,
			SEQ_NO,
			DIRECTORY,
			FILE_NAME,
			GUID_FILENAME, 
			CREATED_BY, 
			CREATED_DT
	) VALUES 
	(        
			@vendorCd,
			ISNULL((SELECT MAX(SEQ_NO)+1 FROM TB_T_BANK_ATTACHMENT WHERE VENDOR_CD = @vendorCd), 1), 
			@directory, 
			@fileName, 
			@guidFilename,
			@userName, 
			GETDATE()
	); 


END 


GO
