USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetPID]    Script Date: 6/29/2020 10:20:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_GetPID] 
	@pidi int output,
	@invoice_no varchar(16),
	@function varchar(10) = null,
	@username varchar(20)

AS
BEGIN 
	declare	@pid int=0

	set @username = replace(@username,'TAM\','');


	if (@invoice_no is null or @invoice_no = '')
	begin
		exec usp_PutLog @what=null, @user=@username, @where= null, @id=null, @type=null, @func = @function, @sts=null, @rem=null, @pid = @pid output 
	end;
	else
	begin
		if exists (
				select top 1 1 
				from tb_r_pv_h a
				join tb_r_pv_d b on a.pv_no = b.pv_no
									and b.invoice_no = @invoice_no
				where 1=1
					and status_cd = '7')
		begin
			exec usp_PutLog @what=null, @user=@username, @where= null, @id=null, @type=null, @func = @function, @sts=null, @rem=null, @pid = @pid output 
		end
		else
		begin
			select top 1 @pid = c.process_id 
			from tb_r_pv_h a
			join tb_r_pv_d b on a.PV_NO = b.PV_NO
								and b.INVOICE_NO = @invoice_no
			join tb_t_upload_h c on a.PV_NO = c.pv_no
				where 1=1
					and a.status_cd = '1'
		end
	end

	set @pidi = @pid;
END 




GO
