USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[usp_PutUpload_TAXNew]    Script Date: 6/29/2020 10:20:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[usp_PutUpload_TAXNew](
	@pid				INT, 
	@uid				VARCHAR(20)
) AS 
BEGIN TRY
	SET NOCOUNT ON;

	set @uid = replace(@uid,'TAM\','');
	
	--BEGIN TRANSACTION txUPLOAD_TAX;

	if exists (select top 1 1 from TB_T_PV_D where process_id = @pid)
	begin
		delete TB_T_PV_D where process_id = @pid
		delete TB_R_PV_D where pv_no = (select top 1 PV_NO from tb_t_upload_h where process_id=@pid)
	end	

	DECLARE 
		  @tt INT = 0
		, @assignment_tax_cd varchar(18)=''
		, @VENDOR_WITH_NPWP INT = 1 -- TEMPORARY VALUE - REPLACE WITH VENDOR'S NPWP FIELD IS NOT NULL
		, @tGLACC VARCHAR(200)= 'TEMPLATE.GL_ACC.TAX'
		, @tDPP VARCHAR(50) = 'DPP_WITHHOLDING_TAX_PERCENT'
		, @taxCd VARCHAR(10) = 'V0'	
		, @ERR VARCHAR(MAX) = ''
		, @withHoldingTaxPercent INT
		, @eRAISED INT = 1 
		, @inValid INT = 0		
		, @glaTurnOver int
		, @glaPPN int
		, @glaSEAL int
		, @glaDPP int
		, @glaPPhInt int
		, @glaPPhExt int
		, @glaPPh26 int 
		, @glaPPhFin int 
		, @SEQ INT = 0
		, @SEQ_NO	INT
		, @INVOICE_NO	VARCHAR(16)
		, @INVOICE_DATE	DATETIME 
		, @TAX_NO	VARCHAR(25)
		, @TAX_DT	DATE
		, @CURRENCY_CD	VARCHAR(3) 
		, @AMOUNT	NUMERIC(16, 2)
		, @DESCRIPTION	VARCHAR(50) 
		, @AMOUNT_TURN_OVER	NUMERIC(16, 2) 
		, @AMOUNT_PPN	NUMERIC(16, 2) 
		, @AMOUNT_SEAL	NUMERIC(16, 2) 
		, @DPP_AMOUNT	NUMERIC(16, 2) 
		, @ACC_INF_ASSIGNMENT	VARCHAR(18)
		, @AMOUNT_PPh21_INTERN	NUMERIC(16, 2) 
		, @AMOUNT_PPh21_EXTERN	NUMERIC(16, 2) 
		, @AMOUNT_PPh26	NUMERIC(16, 2)
		, @AMOUNT_PPh_FINAL	NUMERIC(16, 2) 
		, @COST_CENTER_CD	VARCHAR(10)
		, @ERRMSG		   VARCHAR(MAX)	
		, @DPP_PPh_AMOUNT NUMERIC(16, 2) 
		, @TAX_CODE_PPh23 varchar(10)
		, @TAX_TARIFF_PPh23 NUMERIC(16, 2) 
		, @TAX_CODE_PPh21_EXTERN varchar(10) 
		, @TAX_TARIFF_PPh21_EXTERN NUMERIC(16, 2) 
		, @NIK_PPh21_EXTERN varchar(50)
		, @TAX_CODE_PPh26 varchar(10)
		, @TAX_TARIFF_PPh26 NUMERIC(16, 2) 
		, @TAX_CODE_PPh_FINAL varchar(10)
		, @TAX_TARIFF_PPh_FINAL NUMERIC(16, 2) 
		, @INFO_PPh_FINAL varchar(500); 
		
	WHILE @eRAISED = 1 
	BEGIN
		EXEC usp_GetTaxInfos @pid, @tDPP
			, @tt OUTPUT
			, @VENDOR_WITH_NPWP OUTPUT
			, @assignment_tax_cd OUTPUT
			, @withHoldingTaxPercent OUTPUT; 	
		
		/*
		IF EXISTS(
			SELECT 1 
			FROM TB_T_UPLOAD_D 
			WHERE AMOUNT_PPN <> 0 
			AND (TAX_NO IS NULL OR TAX_DT IS NULL)
			AND PROCESS_ID = @pid
		) 
		BEGIN 
			UPDATE TB_T_UPLOAD_D 
			SET ERRMSG = ISNULL(ERRMSG, '') + ' Tax Code and Tax Date is mandatory when PPn Amount non Zero'
			WHERE AMOUNT_PPN <> 0 
			AND (TAX_NO IS NULL OR TAX_DT IS NULL)
			AND PROCESS_ID = @pid;
			
			SET @inValid = @@ROWCOUNT;
			SET @ERR ='Tax Code and Tax Data not filled for some PPn Amount filled data'; 
			BREAK;
		END;
		*/	
		
		DECLARE @ERR1 INT = 0;
		exec @ERR1= dbo.usp_SetUploadValidation @pid, @uid, 1;
		SET @inValid = @inValid + @ERR1;
		exec @ERR1= dbo.usp_SetUploadValidation @pid, @uid, 2;		 
		SET @inValid = @inValid + @ERR1;
		exec @ERR1= dbo.usp_SetUploadValidation @pid, @uid, 4;
		SET @inValid = @inValid + @ERR1;
		exec @ERR1= dbo.usp_SetUploadValidation @pid, @uid, 5;
		SET @inValid = @inValid + @ERR1;
		exec @ERR1= dbo.usp_SetUploadValidation @pid, @uid, 6;
		SET @inValid = @inValid + @ERR1;
		exec @ERR1= dbo.usp_SetUploadValidation @pid, @uid, 7;
		SET @inValid = @inValid + @ERR1;
		exec @ERR1= dbo.usp_SetUploadValidation @pid, @uid, 8;
		SET @inValid = @inValid + @ERR1;
		exec @ERR1= dbo.usp_SetUploadValidation @pid, @uid, 9;
		SET @inValid = @inValid + @ERR1;
		exec @ERR1= dbo.usp_SetUploadValidation @pid, @uid, 10;
		SET @inValid = @inValid + @ERR1;
		exec @ERR1= dbo.usp_SetUploadValidation @pid, @uid, 11;
		SET @inValid = @inValid + @ERR1;
		exec @ERR1= dbo.usp_SetUploadValidation @pid, @uid, 12;
		SET @inValid = @inValid + @ERR1;
		exec @ERR1= dbo.usp_SetUploadValidation @pid, @uid, 13;
		SET @inValid = @inValid + @ERR1;
		exec @ERR1= dbo.usp_SetUploadValidation @pid, @uid, 14;
		SET @inValid = @inValid + @ERR1;
		exec @ERR1= dbo.usp_SetUploadValidation @pid, @uid, 15;
		SET @inValid = @inValid + @ERR1;
		exec @ERR1= dbo.usp_SetUploadValidation @pid, @uid, 16;
		SET @inValid = @inValid + @ERR1;
		exec @ERR1= dbo.usp_SetUploadValidation @pid, @uid, 17;
		SET @inValid = @inValid + @ERR1;
		exec @ERR1= dbo.usp_SetUploadValidation @pid, @uid, 18;
		SET @inValid = @inValid + @ERR1;
		exec @ERR1= dbo.usp_SetUploadValidation @pid, @uid, 19;
		SET @inValid = @inValid + @ERR1;
		exec @ERR1= dbo.usp_SetUploadValidation @pid, @uid, 20;
		SET @inValid = @inValid + @ERR1;
		exec @ERR1= dbo.usp_SetUploadValidation @pid, @uid, 21;
		SET @inValid = @inValid + @ERR1;
		exec @ERR1= dbo.usp_SetUploadValidation @pid, @uid, 22;
		SET @inValid = @inValid + @ERR1;
		exec @ERR1= dbo.usp_SetUploadValidation @pid, @uid, 23;
		SET @inValid = @inValid + @ERR1;
		exec @ERR1= dbo.usp_SetUploadValidation @pid, @uid, 25;
		SET @inValid = @inValid + @ERR1;

		IF @inValid > 0
		BEGIN
			SET @ERR = 'Some data is not valid'; 
			--BREAK;
		END;
		
		DECLARE CURX CURSOR LOCAL FAST_FORWARD FOR 
		SELECT
			SEQ_NO,
			INVOICE_NO,
			dbo.udf_DtImport(INVOICE_DATE),
			TAX_NO,
			dbo.udf_DtImport(TAX_DT),
			CURRENCY_CD,
			AMOUNT, 
			[DESCRIPTION],
			AMOUNT_TURN_OVER, 
			AMOUNT_PPN,
			AMOUNT_SEAL,
			DPP_AMOUNT,
			ACC_INF_ASSIGNMENT,
			AMOUNT_PPh21_INTERN,
			AMOUNT_PPh21_EXTERN,
			AMOUNT_PPh26,
			AMOUNT_PPh_FINAL,
			COST_CENTER_CD,
			DPP_PPh_AMOUNT,
			TAX_CODE_PPh23,
			TAX_TARIFF_PPh23,
			TAX_CODE_PPh21_EXTERN,
			TAX_TARIFF_PPh21_EXTERN, 
			NIK_PPh21_EXTERN,
			TAX_CODE_PPh26,
			TAX_TARIFF_PPh26,
			TAX_CODE_PPh_FINAL,
			TAX_TARIFF_PPh_FINAL,
			INFO_PPh_FINAL
		FROM TB_T_UPLOAD_D WHERE PROCESS_ID = @pid;
		
		SELECT  
			@glaPPN = dbo.udf_GetSystemValue(@tGLACC, 'AMOUNT_PPN'), 
			@glaSEAL = dbo.udf_GetSystemValue(@tGLACC, 'AMOUNT_SEAL'),
			@glaDPP= dbo.udf_GetSystemValue(@tGLACC, 'DPP_AMOUNT'), 
			@glaPPhInt= dbo.udf_GetSystemValue(@tGLACC, 'AMOUNT_PPh21_INTERN'), 
			@glaPPhExt= dbo.udf_GetSystemValue(@tGLACC, 'AMOUNT_PPh21_EXTERN'), 
			@glaPPh26= dbo.udf_GetSystemValue(@tGLACC, 'AMOUNT_PPh26'), 
			@glaPPhFin= dbo.udf_GetSystemValue(@tGLACC, 'AMOUNT_PPh_FINAL');
				
		OPEN CURX;
		FETCH NEXT FROM CURX INTO 	
			  @SEQ_NO
			, @INVOICE_NO
			, @INVOICE_DATE
			, @TAX_NO
			, @TAX_DT
			, @CURRENCY_CD
			, @AMOUNT
			, @DESCRIPTION
			, @AMOUNT_TURN_OVER
			, @AMOUNT_PPN
			, @AMOUNT_SEAL
			, @DPP_AMOUNT
			, @ACC_INF_ASSIGNMENT
			, @AMOUNT_PPh21_INTERN
			, @AMOUNT_PPh21_EXTERN
			, @AMOUNT_PPh26
			, @AMOUNT_PPh_FINAL
			, @COST_CENTER_CD
			, @DPP_PPh_AMOUNT
			, @TAX_CODE_PPh23
			, @TAX_TARIFF_PPh23
			, @TAX_CODE_PPh21_EXTERN
			, @TAX_TARIFF_PPh21_EXTERN
			, @NIK_PPh21_EXTERN
			, @TAX_CODE_PPh26
			, @TAX_TARIFF_PPh26
			, @TAX_CODE_PPh_FINAL
			, @TAX_TARIFF_PPh_FINAL
			, @INFO_PPh_FINAL;
			
		WHILE @@FETCH_STATUS = 0
		BEGIN 
			
			SET @glaTurnOver = dbo.udf_GLAccountElvis(@COST_CENTER_CD, @tt, 1)
			IF (ISNULL(@AMOUNT_TURN_OVER, 0) > 0 ) 
			BEGIN
				SET @SEQ = @SEQ + 1;
				
				exec dbo.usp_PutTempUploadExt
					 @pid, @uid, @seq, @seq_no, @COST_CENTER_CD, 
					 @invoice_no, @invoice_date, @tax_no, @TAX_DT, @currency_cd, 
					 @description, @dpp_amount, @assignment_tax_cd, @acc_inf_assignment
					 , @amount_turn_over
					 , @glaTurnOver
					 , 'TURN_OVER';
			END;
			
			IF (ISNULL(@AMOUNT_PPN, 0) > 0) 
			BEGIN
				SET @SEQ = @SEQ + 1;
				
			    exec dbo.usp_PutTempUploadExt
					@pid, @uid, @seq, @seq_no, @COST_CENTER_CD, 
					@invoice_no, @invoice_date, @tax_no, @TAX_DT, @currency_cd, 
					@description, @dpp_amount, @assignment_tax_cd, @acc_inf_assignment
					, @AMOUNT_PPN
					, @glaPPN
					, 'AMOUNT_PPN';

			END;
			
			IF (ISNULL(@AMOUNT_SEAL, 0) > 0) 
			BEGIN
				SET @SEQ = @SEQ + 1;
				
					
			    exec dbo.usp_PutTempUploadExt
				 @pid, @uid, @seq, @seq_no,  @COST_CENTER_CD, 
				 @invoice_no, @invoice_date, @tax_no, @TAX_DT, @currency_cd, 
				 @description, @dpp_amount, @assignment_tax_cd, @acc_inf_assignment
				 , @AMOUNT_SEAL
				 , @glaSEAL
				 , 'AMOUNT_SEAL';
				
			END;
			
			
			IF (ISNULL(@DPP_AMOUNT, 0) > 0) 
			BEGIN
				DECLARE @withholdingTax NUMERIC(16,2);
				--SET @withholdingTax = -1 * ROUND( (isnull(@DPP_AMOUNT,0) * isnull(@withHoldingTaxPercent,0)) / 100, 0);
				SET @withholdingTax = -1 * @DPP_AMOUNT;

				SET @SEQ = @SEQ + 1;
				
				exec dbo.usp_PutTempUploadExt_New
					@pid, @uid, @seq, @seq_no,  @COST_CENTER_CD, 
					@invoice_no, @invoice_date, @tax_no, @TAX_DT, @currency_cd, 
					@description, @dpp_amount, @assignment_tax_cd, @acc_inf_assignment
					, @withholdingTax
					, @glaDPP
					, 'WITHHOLDING_TAX'
					, @TAX_CODE_PPh23
					, @TAX_TARIFF_PPh23
					, NULL
					, @DPP_PPh_AMOUNT;
			END;
			
			IF (ISNULL(@AMOUNT_PPh21_INTERN, 0) > 0) 
			BEGIN
				SET @SEQ = @SEQ + 1;
				
				SET @AMOUNT_PPh21_INTERN = -1 * @AMOUNT_PPh21_INTERN;
				
				exec dbo.usp_PutTempUploadExt_New
					@pid, @uid, @seq, @seq_no, @COST_CENTER_CD, 
					@invoice_no, @invoice_date, @tax_no, @TAX_DT, @currency_cd, 
					@description, @dpp_amount, @assignment_tax_cd, @acc_inf_assignment
					, @AMOUNT_PPh21_INTERN
					, @glaPPhInt 
					,  'AMOUNT PPH21 INTERN'
					, NULL
					, NULL
					, NULL
					, @DPP_PPh_AMOUNT;
			END;
			
			IF (ISNULL(@AMOUNT_PPh21_EXTERN, 0) > 0) 
			BEGIN
				SET @SEQ = @SEQ + 1;
				
				SET @AMOUNT_PPh21_EXTERN = -1 * @AMOUNT_PPh21_EXTERN;
				
			    exec dbo.usp_PutTempUploadExt_New
					 @pid, @uid, @seq, @seq_no, @COST_CENTER_CD, 
					 @invoice_no, @invoice_date, @tax_no, @TAX_DT, @currency_cd, 
					 @description, @dpp_amount, @assignment_tax_cd, @acc_inf_assignment
					 , @AMOUNT_PPh21_EXTERN
					 , @glaPPhExt
					 , 'AMOUNT PPH21 EXTERN'
					 , @TAX_CODE_PPh21_EXTERN
					 , @TAX_TARIFF_PPh21_EXTERN
					 , @NIK_PPh21_EXTERN
					 , @DPP_PPh_AMOUNT;
			END;
		
		
			IF (ISNULL(@AMOUNT_PPh26, 0) > 0) 
			BEGIN
				SET @SEQ = @SEQ + 1;
				
				set @AMOUNT_PPh26 = -1 * @AMOUNT_PPh26;
				
				 exec dbo.usp_PutTempUploadExt_New
					 @pid, @uid, @seq, @seq_no, @COST_CENTER_CD, 
					 @invoice_no, @invoice_date, @tax_no, @TAX_DT, @currency_cd, 
					 @description, @dpp_amount, @assignment_tax_cd, @acc_inf_assignment
					 , @AMOUNT_PPh26
					 , @glaPPh26 
					 ,'AMOUNT PPH 26'
					 , @TAX_CODE_PPh26
					 , @TAX_TARIFF_PPh26
					 , NULL
					 , @DPP_PPh_AMOUNT;
			END;
			
			
			IF (ISNULL(@AMOUNT_PPh_FINAL, 0) > 0) 
			BEGIN
				SET @SEQ = @SEQ + 1;
				
				set @AMOUNT_PPh_FINAL = -1 * @AMOUNT_PPh_FINAL;
				
				 exec dbo.usp_PutTempUploadExt_New
					 @pid, @uid, @seq, @seq_no, @COST_CENTER_CD, 
					 @invoice_no, @invoice_date, @tax_no, @TAX_DT, @currency_cd, 
					 @description, @dpp_amount, @assignment_tax_cd, @acc_inf_assignment
					 , @AMOUNT_PPh_FINAL
					 , @glaPPhFin
					 ,'AMOUNT_PPH_FINAL'
					 , @TAX_CODE_PPh_FINAL
					 , @TAX_TARIFF_PPh_FINAL
					 , @INFO_PPh_FINAL
					 , @DPP_PPh_AMOUNT;
				
			END;
			
			FETCH NEXT FROM CURX INTO 	
				  @SEQ_NO
				, @INVOICE_NO
				, @INVOICE_DATE
				, @TAX_NO
				, @TAX_DT
				, @CURRENCY_CD
				, @AMOUNT
				, @DESCRIPTION
				, @AMOUNT_TURN_OVER
				, @AMOUNT_PPN
				, @AMOUNT_SEAL
				, @DPP_AMOUNT
				, @ACC_INF_ASSIGNMENT
				, @AMOUNT_PPh21_INTERN
				, @AMOUNT_PPh21_EXTERN
				, @AMOUNT_PPh26
				, @AMOUNT_PPh_FINAL
				, @COST_CENTER_CD
				, @DPP_PPh_AMOUNT
				, @TAX_CODE_PPh23
				, @TAX_TARIFF_PPh23
				, @TAX_CODE_PPh21_EXTERN
				, @TAX_TARIFF_PPh21_EXTERN
				, @NIK_PPh21_EXTERN
				, @TAX_CODE_PPh26
				, @TAX_TARIFF_PPh26
				, @TAX_CODE_PPh_FINAL
				, @TAX_TARIFF_PPh_FINAL
				, @INFO_PPh_FINAL;
		END;

		SET @eRAISED = 0;
		BREAK;
	END; 
	/*
	IF (LEN(@ERR) > 0)
	BEGIN
		EXEC dbo.sp_PutLog @err, @uid, 'sp_PutUpload_TAXNew', @pid, 'MSTD00002ERR', 'ERR';
	END;
	*/
	--COMMIT TRANSACTION txUPLOAD_TAX;
	
	/*13052020
	exec [usp_InsertPVRD] @pid, @uid;

	declare @pv_no int, 
			@totalAmount numeric(16,2), 
			@vendorGroup int, 
			@currCd varchar(3);

	select @totalAmount = sum(amount), @currCd = currency_cd from tb_t_pv_d 
	where process_id=@pid
	group by currency_cd

	select @pv_no = pv_no from tb_t_upload_h where process_id = @pid
	
	if not exists (select top 1 1 from TB_R_PVRV_AMOUNT where doc_no = @pv_no)
	begin
		insert TB_R_PVRV_AMOUNT values
		(@pv_no, cast(year(getdate()) as varchar), @currCd, @totalAmount, '1', getdate(),@uid, null, null)
	end
	else
	begin		
		update TB_R_PVRV_AMOUNT set TOTAL_AMOUNT = @totalAmount
		where DOC_NO=@pv_no
	end

	update TB_R_PV_H set TOTAL_AMOUNT = @totalAmount
	where PV_NO = @pv_no
	*/

	SELECT 1;
END TRY
BEGIN CATCH 
	--ROLLBACK TRANSACTION txUPLOAD_TAX;
	
	--EXEC sp_error_handler @pid;
	
	SELECT 0;
END CATCH






GO
