USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdatePVAttachment]    Script Date: 6/29/2020 10:20:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_UpdatePVAttachment]
	-- Add the parameters for the stored procedure here
	@oldReffNo varchar(34), 
	@newReffNo varchar(34),
	@directory varchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	update tb_r_pv_monitoring_attachment set REFERENCE_NO = @newReffNo, directory = @directory where REFERENCE_NO = @oldReffNo

END


GO
