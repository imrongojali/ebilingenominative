USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[usp_PutTempUploadExt_New]    Script Date: 6/29/2020 10:20:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[usp_PutTempUploadExt_New]
	  @pid					INT
	, @uid					VARCHAR(20)
	, @SEQ					INT = 0
	, @SEQ_NO				INT
	, @COST_CENTER_CD       VARCHAR(10) 
	, @INVOICE_NO			VARCHAR(16)
	, @INVOICE_DATE			DATETIME 
	, @TAX_NO				VARCHAR(25)
	, @TAX_DT				DATE
	, @CURRENCY_CD			VARCHAR(3) 
	, @DESCRIPTION			VARCHAR(50) 
	, @DPP_AMOUNT			NUMERIC(18, 2) 
	, @ASSIGNMENT_TAX_CD	VARCHAR(18)
	, @ACC_INF_ASSIGNMENT	VARCHAR(18)
	, @AMOUNT				NUMERIC(18, 2)
	, @GL_ACCOUNT			INT
	, @ERRMSG				VARCHAR(max)
	, @WHT_TAX_CODE varchar(10)
	, @WHT_TAX_TARIFF numeric(18,4)
	, @WHT_TAX_ADDITIONAL_INFO varchar(500)
	, @WHT_DPP_PPh_AMOUNT numeric(18,2)
AS 
BEGIN 

set @uid = replace(@uid,'TAM\','');

declare @WHT_TAX_ADDITIONAL_INFO_DEFAULT varchar(500)
select  @WHT_TAX_ADDITIONAL_INFO_DEFAULT = DefaultValue
from ELVIS_DB.dbo.fn_GetTransaksiTypeCode('0') 
where TransaksiTypeCode = @WHT_TAX_CODE and isnull(DefaultValue,'') <> ''

if isnull(@WHT_TAX_ADDITIONAL_INFO_DEFAULT,'') <> ''
begin
	set @WHT_TAX_ADDITIONAL_INFO = @WHT_TAX_ADDITIONAL_INFO_DEFAULT
end 

INSERT TB_T_PV_D 
	(PROCESS_ID, SEQ_NO
	, COST_CENTER_CD
	, CURRENCY_CD
	, DPP_AMOUNT
	, [DESCRIPTION]
	, INVOICE_NO, INVOICE_DATE
	, TAX_NO, TAX_DT, TAX_CD
	, USEQ
	, ITEM_TRANSACTION_CD
	, ACC_INF_ASSIGNMENT
	, TAX_ASSIGNMENT
	, AMOUNT
	, GL_ACCOUNT
	, ERRMSG
	, CREATED_BY, CREATED_DT
	, WHT_TAX_CODE
	, WHT_TAX_TARIFF
	, WHT_TAX_ADDITIONAL_INFO
	, WHT_DPP_PPh_AMOUNT
	) 
VALUES (@pid, @SEQ
	 , dbo.udf_ValidCostCenter(@COST_CENTER_CD, @GL_ACCOUNT) 
	 , @CURRENCY_CD
	 , @DPP_AMOUNT
	 , @DESCRIPTION
	 , @INVOICE_NO, @INVOICE_DATE
	 , @TAX_NO, @TAX_DT, 'V0'
	 , @SEQ_NO
	 , 1
	 , @ACC_INF_ASSIGNMENT
	 , @assignment_tax_cd
	 , @AMOUNT
	 , @GL_ACCOUNT
	 , @errmsg
	 , @uid, GETDATE()
	 , @WHT_TAX_CODE
	 , @WHT_TAX_TARIFF
	 , @WHT_TAX_ADDITIONAL_INFO
	 , @WHT_DPP_PPh_AMOUNT
	 );
END;




GO
