USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdatePVMonitoring]    Script Date: 6/29/2020 10:20:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[usp_UpdatePVMonitoring] 
	-- Add the parameters for the stored procedure here
	@pid int,	
	@pvNo int,
	@seqNo varchar(max),
	@invoiceNo varchar(50),
    @invoiceDate varchar(25),
    @taxNo varchar(25),
    @taxDt varchar(25),
	@bankType varchar(1),
	@bankKey varchar(20),
	@user varchar(20),
	@process_status int OUTPUT,
	@err_mesg varchar(2000) OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if (@invoiceNo is not null and len(@invoiceNo) > 16)
	begin
		set @process_status = 1
		set @err_mesg = 'Invoice number character length should not be more than 16 characters';
	end

	if @process_status <> 0
	begin
		return		
	end
	
	declare @x varchar(20), @vendor varchar(10), @invDate varchar(25), @taxDate varchar(25)

	if (@invoiceDate is not null)
	begin
		set @invDate = cast(substring(@invoiceDate, 7, 4) +'-'+ substring(@invoiceDate, 4, 2) +'-'+ substring(@invoiceDate, 1, 2) as date)
	end

	if (@taxDt is not null)
	begin
		set @taxDate = cast(substring(@taxDt, 7, 4) +'-'+ substring(@taxDt, 4, 2) +'-'+ substring(@taxDt, 1, 2) as date)
	end

	select top 1
		@vendor = vendor_cd 
	from tb_t_upload_d a
	join tb_t_upload_h b 
		on a.process_id = b.process_id
	where b.process_id = @pid

	select @x = bank_account 
	from sap_db.dbo.tb_m_vendor_bank 
	where vendor_cd = @vendor and bank_key= @bankKey

	update ud set
		   ud.attachment_uploaded = '1', 
		   ud.invoice_no = @invoiceNo, 
		   ud.invoice_date = @invoiceDate,
		   ud.tax_no = @taxNo,
		   ud.tax_dt = @taxDt,
		   ud.changed_by = @user,
		   ud.changed_dt = getdate()		   
	from tb_t_upload_d ud
	join tb_t_upload_h uh
		on ud.process_id =  uh.process_id
	where uh.process_id = @pid
		and ud.seq_no = @seqNo

	update pd set
		   pd.invoice_no = @invoiceNo, 
		   pd.invoice_date = @invDate,
		   pd.tax_no = @taxNo,
		   pd.tax_dt = @taxDate,
		   pd.changed_by = @user,
		   pd.changed_dt = getdate()	
	from (
		select
			   pd.useq,
			   pd.seq_no,
			   uh.pv_no
		from tb_t_upload_h uh
		left join tb_t_pv_d pd
			on pd.process_id = uh.process_id
		where uh.process_id = @pid
			and pd.useq = @seqNo
	) uh
	left join tb_r_pv_d pd
		on pd.pv_no = uh.pv_no
			and pd.seq_no = uh.seq_no

	update tpd set
		   tpd.invoice_no = @invoiceNo, 
		   tpd.invoice_date = @invDate,
		   tpd.tax_no = @taxNo,
		   tpd.tax_dt = @taxDate,
		   tpd.changed_by = @user,
		   tpd.changed_dt = getdate()	
	from tb_t_upload_h uh
	left join tb_t_pv_d tpd
		on tpd.process_id = uh.process_id
	where uh.process_id= @pid
		  and tpd.useq = @seqNo

	update tb_t_upload_h set bank_key = @bankKey, bank_type = @bankType, bank_account = @x, changed_by = @user, changed_dt= getdate() 
	where process_id = @pid

	update x set bank_type = a.bank_type
	from tb_r_pv_h x
	join tb_t_upload_h a
	on x.pv_no = a.pv_no
	where a.process_id = @pid

END






GO
