USE [BES]
GO

/****** Object:  StoredProcedure [dbo].[usp_GetAllowedMenuBy_UserType]    Script Date: 7/24/2020 05:27:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetAllowedMenuBy_UserType] 
	-- Add the parameters for the stored procedure here
	@menuId int,
	@userType varchar(50),
	@user varchar(75)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare
	@isPermission bit = 0;

    -- Insert statements for procedure here
	if (@userType = 'Internal')
	begin
		print 'a'
		if exists (
			select top 1 1 
			from tb_m_security_users a
			join tb_m_mapp_div div 
				on div.username = a.username
			left join tb_r_menu_role b on a.level_code = b.level_code
			left join tb_r_menu_role c on b.menu_id = c.menu_id
			where 1=1
				and a.[username]=@user
				and c.[menu_id]=@menuId
		)
		begin
			set @isPermission = 1;
		end
		else
		begin
			if exists (
				select top 1 1 
				from tb_m_security_users a
				left join tb_r_menu_role b on a.level_code = b.level_code
				left join tb_r_menu_role c on b.menu_id = c.menu_id
				left join tb_m_menu	d on d.menu_id = c.menu_id		
				where 1=1
					and a.[username]=@user
					and c.[menu_id]=@menuId
					and d.url <> '~/Core/PVMonitoring'
			)
			begin
				print 'b'
				set @isPermission = 1;
			end
		end
	end
	else
	begin
		if exists (
			select top 1 1
			from tb_m_customer_id a
			left join tb_t_mapp_vendor b on a.customer_group = b.customer_group_id
			where 1=1
				and a.[user_id]=@user
				and b.vendor_cd is not null
		)
		begin
			print 'c'
			set @isPermission = 1;
		end
	end

	select @isPermission
END

GO

