USE [BES]
GO

/****** Object:  StoredProcedure [dbo].[usp_GetDataUDFWithSP]    Script Date: 7/15/2020 01:14:53 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetDataUDFWithSP]  
	-- Add the parameters for the stored procedure here
	@sqlQuery nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	if (@sqlQuery is not null)
		execute(@sqlQuery);

END

GO

