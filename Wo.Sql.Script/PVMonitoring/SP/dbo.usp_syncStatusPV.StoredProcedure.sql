USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[usp_syncStatusPV]    Script Date: 6/29/2020 10:20:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_syncStatusPV] 
	-- Add the parameters for the stored procedure here
	@date date,
	@user varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	set nocount on;
	
	select 
	   pv_no
	  ,pv_year	
	  ,status_cd	
	  ,pay_method_cd	
	  ,vendor_group_cd	
	  ,vendor_cd	
	  ,pv_type_cd	
	  ,transaction_cd	
	  ,suspense_no	
	  ,suspense_year	
	  ,set_no_pv	
	  ,set_no_rv	
	  ,budget_no	
	  ,activity_date_from	
	  ,activity_date_to	
	  ,division_id	
	  ,pv_date	
	  ,planning_payment_date	
	  ,posting_date	
	  ,submit_date	
	  ,paid_date	
	  ,settlement_status	
	  ,workflow_status	
	  ,deleted	
	  ,print_ticket_flag	
	  ,refference_no	
	  ,submit_hc_doc_date	
	  ,bank_type	
	  ,hold_by	
	  ,hold_flag	
	  ,total_amount	
	  ,dpp_amount	
	  ,cancel_flag	
	  ,counter_flag	
	  ,print_ticket_by	
	  ,next_approver	
	  ,status_compare	
	  ,expired_date	
	  ,url_wht
	into #tb_t_pv_h
	from tb_t_pv_h		
	
	if (select count(1) 
		from dbo.tb_r_pv_h a
		join #tb_t_pv_h	b
			on a.pv_no = b.pv_no
			and a.pv_year = b.pv_year	
		) > 0	
	begin

		begin transaction
		begin try
				
				update a set a.status_cd = b.status_cd
							,a.pay_method_cd =a.pay_method_cd	
							,a.vendor_group_cd=b.vendor_group_cd	
							,a.vendor_cd=b.vendor_cd	
							,a.pv_type_cd=b.pv_type_cd	
							,a.transaction_cd=b.transaction_cd	
							,a.suspense_no=b.suspense_no	
							,a.suspense_year=b.suspense_year	
							,a.set_no_pv=b.set_no_pv	
							,a.set_no_rv=b.set_no_rv	
							,a.budget_no=b.budget_no	
							,a.activity_date_from=b.activity_date_from	
							,a.activity_date_to=b.activity_date_to	
							,a.division_id=b.division_id	
							,a.pv_date=b.pv_date	
							,a.planning_payment_date=b.planning_payment_date	
							,a.posting_date=b.posting_date	
							,a.submit_date=b.submit_date	
							,a.paid_date=b.paid_date	
							,a.settlement_status=b.settlement_status	
							,a.workflow_status=b.workflow_status	
							,a.deleted=b.deleted	
							,a.print_ticket_flag=b.print_ticket_flag	
							,a.refference_no=b.refference_no	
							,a.submit_hc_doc_date=b.submit_hc_doc_date	
							,a.bank_type=b.bank_type	
							,a.hold_by=b.hold_by	
							,a.hold_flag=b.hold_flag	
							,a.total_amount=b.total_amount	
							,a.dpp_amount=b.dpp_amount	
							,a.cancel_flag=b.cancel_flag	
							,a.counter_flag=b.counter_flag	
							,a.print_ticket_by=b.print_ticket_by	
							,a.next_approver=b.next_approver	
							,a.status_compare=b.status_compare	
							,a.expired_date=b.expired_date	
							,a.url_wht=b.url_wht
							,a.changed_date = getdate()
							,a.changed_by = @user
				from dbo.tb_r_pv_h a
				join #tb_t_pv_h	b
					on a.pv_no = b.pv_no
					and a.pv_year = b.pv_year	

				delete tb_t_pv_h					
				
			commit;
		end try
		begin catch
			rollback transaction;
		end catch

	end

	if exists (
		select top 1 1 from tb_r_pv_h where status_cd in (
				select value1 from tb_m_parameter 
					where key_param='pv_status' 
						and cast(value2 as varchar) in ('pv_verified_budget'))
	)
	begin
		update h set h.status_cd= a.value1, h.changed_date=@date, h.changed_by=@user
			from tb_r_pv_h h 
			outer apply (
				select [value1] from tb_m_parameter 
					where key_param='pv_status' 
						and cast(value2 as varchar)='pv_expired'
			) a
			where h.pv_no in (
				select pv_no 
				from tb_r_pv_h
					where status_cd in 
						(
							select value1 from tb_m_parameter 
							where key_param='pv_status' 
								and cast(value2 as varchar) in ('pv_verified_budget')
						)
						and datediff(day, changed_date, getdate()) 
							>= (
								select value1 from tb_m_parameter 
									where key_param='pv_expired'
							)
			)
	end

	select 
		reff_no,
		seq_no,
		module_cd,
		status_cd,
		plan_date,
		actual_date,
		actual_lt,
		actual_uom,
		comment,
		created_by,
		created_dt,
		changed_by,
		changed_dt
	into #tb_t_distribution_status
	from tb_t_distribution_status

	if exists(select top 1 1 from #tb_t_distribution_status)
	begin	
	
		select reff_no
		into #tb_reff_no
		from #tb_t_distribution_status
		group by reff_no
	
		declare @minReff numeric(20,0), @subtring int, @pvno_subtring varchar(50);

		declare @tempInsert table
		(
			reff_no numeric(20,0),
			seq_no int,
			status_cd int,
			actual_date datetime,
			created_by varchar(20),
			seq_latest int
		)

		select @minReff = min(reff_no) from #tb_reff_no

		while(@minReff is not null)
		begin

			set @subtring = null;
			select @subtring = len(@minReff) - 4;
			set @pvno_subtring = null;

			select @pvno_subtring = substring(cast(@minReff as varchar),1,@subtring);
			print 'pv_no: ' + @pvno_subtring;

			if exists(select top 1 1 from tb_r_pv_h where pv_no = @pvno_subtring) --'500271838'
			begin 
				print 'reff_no: ' + cast(@minReff as varchar);

				delete @tempInsert
				--select * from #tb_t_distribution_status
				insert @tempInsert
				select a.reff_no, a.seq_no, a.status_cd, a.actual_date, a.created_by, ins.seq_latest
				from #tb_t_distribution_status a
				outer apply (
					select top 1 aa.reff_no
					from tb_h_distribution_status aa
					where aa.reff_no = a.reff_no
						and aa.status_cd = a.status_cd
				) x
				outer apply (
					select top 1 bb.reff_no, bb.seq_no [seq_latest]
					from tb_h_distribution_status bb
					where bb.reff_no = a.reff_no
						and bb.status_cd != a.status_cd
					order by seq_no desc
				) ins
				where a.status_cd not in ('0','10','17','20')
					and a.reff_no = @minReff
					and a.actual_date is not null
					and x.reff_no is null

				--select * from @tempInsert --return

				declare @seqLatest int, @seq int;

				select @seq = min(seq_no) from @tempInsert

				select top 1 @seqLatest = seq_latest from @tempInsert
				set @seqLatest = isnull(@seqLatest,0);
				
				while(@seq is not null)
				begin
					print 'seq: ' + cast(@seq as varchar)
					set @seqLatest += 1;
					
					print 'seq latest: ' + cast(@seqLatest as varchar)
					--select * from tb_h_distribution_status
					insert tb_h_distribution_status
					select reff_no,@seqLatest,status_cd,actual_date,created_by, getdate(),null,null
					from @tempInsert where seq_no = @seq and reff_no = @minReff

					select @seq = min(seq_no) from @tempInsert
						where seq_no > @seq

				end
			end
	
			select @minReff = min(reff_no) from #tb_reff_no
				where reff_no > @minReff
		end
	end

	delete tb_t_distribution_status
	if object_id('tempdb..#tb_t_pv_h') is not null
        drop table #tb_t_pv_h
	if object_id('tempdb..#tb_t_distribution_status') is not null
        drop table #tb_t_distribution_status
	if object_id('tempdb..#tb_reff_no') is not null
        drop table #tb_reff_no

END


GO
