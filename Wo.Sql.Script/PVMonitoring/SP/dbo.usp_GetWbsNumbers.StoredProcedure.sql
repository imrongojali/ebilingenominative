USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetWbsNumbers]    Script Date: 6/29/2020 10:20:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[usp_GetWbsNumbers] 
   @fiscalYear int = NULL
,  @isFinanceDirectorate INT = 0
,  @division VARCHAR(5) = NULL
,  @isProject INT = 0
,  @transactionCode INT = 0
AS 
BEGIN 
	SET NOCOUNT ON;
  
	declare @x table
	(
		WbsNumber varchar(25),
		[Description] varchar(125) 
	)
	
	insert @x
	exec ELVIS_DB.dbo.[sp_GetWbsNumbers] @fiscalYear, @isFinanceDirectorate, @division, @isProject, @transactionCode

	select WbsNumber, [Description]
	from @x

END;



GO
