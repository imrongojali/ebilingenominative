USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertPVRD]    Script Date: 6/29/2020 10:20:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_InsertPVRD]
	-- Add the parameters for the stored procedure here
	@pid INT, 
	@uid VARCHAR(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	set @uid = replace(@uid,'TAM\','');
	declare @year varchar(4), @pv int
	
	select distinct @year = b.pv_year, @pv = b.pv_no 
	from tb_t_upload_h a
	left join tb_r_pv_h b on a.pv_no = b.pv_no
	where a.process_id = @pid

    -- Insert statements for procedure here
	insert tb_r_pv_d
	select seq_no, 
		@pv, 
		@year, 
		isnull(cost_center_cd, '')[cost_center_cd],
		currency_cd,
		[description],
		amount,
		wbs_no,
		invoice_no,
		invoice_date,
		item_transaction_cd,
		tax_no,
		gl_account,
		tax_cd,
		null [from_curr],
		null [from_seq],
		useq [item_no],
		isnull(dpp_amount,'0.00')[dpp_amount],
		tax_assignment,
		acc_inf_assignment,
		tax_dt,
		getdate()[created_dt],
		@uid [created_by],
		null changed_dt,
		null changed_by,
		wht_tax_code,	
		wht_tax_tariff,
		wht_tax_additional_info,
		wht_dpp_pph_amount
	from tb_t_pv_d
	where process_id = @pid

END


GO
