USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[usp_DeleteUnSubmittedPVMonitoring]    Script Date: 6/29/2020 10:20:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO







CREATE PROCEDURE [dbo].[usp_DeleteUnSubmittedPVMonitoring] 
	-- Add the parameters for the stored procedure here
	@pvno varchar(25),
	@delete int,
	@user varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	declare @process_id int, @i varchar(50), @reff_no varchar(50), @pv_no int

	set @user = replace(@user,'TAM\','');

	declare @x table
	(
		val varchar(50)
	)
	
	select @process_id = b.process_id, @pv_no = a.pv_no, @reff_no = (cast(a.pv_no as varchar) + cast(a.pv_year as varchar))
	from tb_r_pv_h a
	join tb_t_upload_h b
		on a.PV_NO = b.pv_no
	where a.pv_no = @pvno
	
	if (@delete = 1)
	begin

		delete tb_t_upload_h where process_id = @process_id
		delete tb_r_pv_h where pv_no = @pvno
		delete tb_r_pvrv_amount where doc_no = @pvno
		
		delete tb_t_upload_d where process_id = @process_id
		delete tb_t_pv_d where process_id = @process_id
		delete tb_r_pv_d where pv_no = @pvno

		delete ELVIS_DB.dbo.TB_R_PV_ID where id = @pvno and created_by = @user
	end
	else
	begin
		update h set h.status_cd='7' 
		from tb_r_pv_h h
		join tb_t_upload_h b on h.pv_no = b.pv_no
		where b.process_id = @process_id

		declare @seqLatest int;

		select @reff_no = cast(@pv_no as varchar) + cast([pv_year] as varchar)
		from tb_r_pv_h 
		where pv_no = @pv_no

		select top 1 @seqLatest = seq_no
		from tb_h_distribution_status
		where reff_no = @reff_no
		order by seq_no desc

		if (@seqLatest = null)
		begin
			set @seqLatest = 0;
		end

		insert tb_h_distribution_status
		(
			 reff_no	
			,seq_no	
			,status_cd	
			,date	
			,created_by	
			,created_dt
		)
		values
		(
			 @reff_no
			,@seqlatest + 1
			,7
			,getdate()
			,replace(@user,'TAM\','')
			,getdate()
		)
	end

	insert @x
	select invoice_no from tb_t_upload_d where process_id = @process_id

	select @i = min(val) from @x

	while(@i is not null)
	begin
		exec [efb_sp_remove_after_compare] @pv_no = @pvno, @invoice_no = @i

		select @i = min(val) from @x
			where val > @i
	end
	
END





GO
