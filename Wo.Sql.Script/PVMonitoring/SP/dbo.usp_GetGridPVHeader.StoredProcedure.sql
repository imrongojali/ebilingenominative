USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetGridPVHeader]    Script Date: 6/29/2020 10:20:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[usp_GetGridPVHeader]   
		@user varchar(50),
		@roleStr varchar(50),
		@userType varchar(50),
		@levelCode varchar(50),
		@statusFlag varchar(2)
AS 
BEGIN 
	SET NOCOUNT ON;

	declare @x table
	(
		process_id int,
		pv_type varchar(max),
		pv_date date,
		pv_no int,
		transaction_desc varchar(max),
		status_budget varchar(max),
		[budget] varchar(max),
		vendor_cd varchar(max),
		vendor_name varchar(max),
		[vendor_grp] varchar(max),
		status_flag varchar(max),
		status_desc varchar(max),
		[is_usertam] varchar(max),
		gl_account varchar(max),
		invoice_no varchar(max),
		pv_date_dt date,
		account_info varchar(max),
		amount numeric(16,2),
		div_id varchar(10),
		planning_payment_date datetime
	)

	if @userType = 'Internal'
	begin
		insert @x 
		select distinct
				uh.process_id
			,cast(tpe.value2 as varchar)[pv_type]
			,pv_date
			,h.pv_no
			,tt.std_wording [transaction_desc]
			,uh.status_budget
			,h.budget_no [budget]
			,h.vendor_cd
			,v.vendor_name
			,cast(vgrp.value2 as varchar)[vendor_grp]
			,h.[status_cd][status_flag]
			,case when len(h.status_cd) >= 2
				then (select top 1 status_name from vw_TbMStatusElvis v where v.status_cd=h.status_cd)
				else cast(p.[description] as varchar) 
			 end
			 status_desc
			,case when @userType = 'Internal' then 'Y' else 'N' end [is_usertam]
			,null--sf.account_info
			,null--sf.invoice_no
			,h.pv_date
			,null--sf.account_info
			,h.total_amount
			,h.division_id
			,h.planning_payment_date
		from tb_r_pv_h h
		join tb_t_upload_h uh
			on h.pv_no = uh.pv_no
		left join tb_m_parameter tpe 
			on tpe.value1 = h.pv_type_cd
				and tpe.key_param = 'pv_type'
		left join vw_TransactionTypeElvis tt
			on h.transaction_cd = tt.transaction_cd
		left join vw_VendorSAPElvis v
			on h.vendor_cd = v.vendor_cd
		left join tb_m_parameter vgrp 
			on vgrp.value1 = h.vendor_group_cd
				and vgrp.key_param = 'pv_vendor_grp'
		--outer apply
		--(
		--	select process_id, 
		--			stuff((
		--				select ',' + invoice_no 
		--				from tb_t_upload_d x 
		--				where x.process_id = ud.process_id 
		--				for xml path ('')), 1, 1, ''
		--			) [invoice_no], 
		--			--stuff((
		--			--	select ',' + cast(account_info as varchar)
		--			--	from tb_t_upload_d x 
		--			--	where x.process_id = ud.process_id 
		--			--	for xml path ('')), 1, 1, ''
		--			--) 
		--			[account_info]--, 
		--			--cast(substring(max(isnull(invoice_date,(replace(convert(varchar, getdate(), 105),'-','.')))), 7, 4) +'-'+ substring(max(isnull(invoice_date,(replace(convert(varchar, getdate(), 105),'-','.')))), 4, 2) +'-'+ substring(max(isnull(invoice_date,(replace(convert(varchar, getdate(), 105),'-','.')))), 1, 2) as date)
		--			--[invoice_date_dt], 
		--			--status_flag
		--	from tb_t_upload_d ud where ud.process_id=uh.process_id
		--	group by process_id, account_info--, status_flag
		--) sf
		left join tb_t_mapp_vendor mapven 
			on uh.vendor_cd = mapven.vendor_cd
		left join tb_m_customer_id ci 
			on ci.customer_group = mapven.customer_group_id
		left join tb_m_parameter p 
			on p.value1 = h.status_cd and p.key_param = 'pv_status'
		where 1=1
	end
	else
	begin
		insert @x 
		select distinct
				uh.process_id
			,cast(tpe.value2 as varchar)[pv_type]
			,pv_date
			,h.pv_no
			,tt.std_wording [transaction_desc]
			,uh.status_budget
			,h.budget_no [budget]
			,h.vendor_cd
			,v.vendor_name
			,cast(vgrp.value2 as varchar)[vendor_grp]
			,h.[status_cd][status_flag]
			,case when len(h.status_cd) >= 2
				then (select top 1 status_name from vw_TbMStatusElvis v where v.status_cd=h.status_cd)
				else cast(p.[description] as varchar) 
			 end
			 status_desc
			,case when @userType = 'Internal' then 'Y' else 'N' end [is_usertam]
			,null--sf.account_info
			,null--sf.invoice_no
			,h.pv_date
			,null--sf.account_info
			,h.total_amount
			,h.division_id
			,h.planning_payment_date
		from tb_r_pv_h h
		join tb_t_upload_h uh
			on h.pv_no = uh.pv_no
		left join tb_m_parameter tpe 
			on tpe.value1 = h.pv_type_cd
				and tpe.key_param = 'pv_type'
		left join vw_TransactionTypeElvis tt
			on h.transaction_cd = tt.transaction_cd
		left join vw_VendorSAPElvis v
			on h.vendor_cd = v.vendor_cd
		left join tb_m_parameter vgrp 
			on vgrp.value1 = h.vendor_group_cd
				and vgrp.key_param = 'pv_vendor_grp'
		--outer apply
		--(
		--	select process_id, 
		--			stuff((
		--				select ',' + invoice_no 
		--				from tb_t_upload_d x 
		--				where x.process_id = ud.process_id 
		--				for xml path ('')), 1, 1, ''
		--			) [invoice_no], 
		--			--stuff((
		--			--	select ',' + cast(account_info as varchar)
		--			--	from tb_t_upload_d x 
		--			--	where x.process_id = ud.process_id 
		--			--	for xml path ('')), 1, 1, ''
		--			--) 
		--			[account_info]--, 
		--			--cast(substring(max(isnull(invoice_date,(replace(convert(varchar, getdate(), 105),'-','.')))), 7, 4) +'-'+ substring(max(isnull(invoice_date,(replace(convert(varchar, getdate(), 105),'-','.')))), 4, 2) +'-'+ substring(max(isnull(invoice_date,(replace(convert(varchar, getdate(), 105),'-','.')))), 1, 2) as date)
		--			--[invoice_date_dt], 
		--			--status_flag
		--	from tb_t_upload_d ud where ud.process_id=uh.process_id
		--	group by process_id, account_info--, status_flag
		--) sf
		left join tb_t_mapp_vendor mapven 
			on uh.vendor_cd = mapven.vendor_cd
		left join tb_m_customer_id ci 
			on ci.customer_group = mapven.customer_group_id
		left join tb_m_parameter p 
			on p.value1 = h.status_cd and p.key_param = 'pv_status'
		where 1=1
			and ci.customer_group = @roleStr and h.status_cd not in ('1','2','6','7')
	end

	if (@userType = 'Internal' and @levelCode = 'MD' and @statusFlag = '2')
	begin
		select a.process_id,
			   pv_type,
			   a.pv_date,
			   a.pv_no,
			   transaction_desc,
			   status_budget,
			   [budget],
			   a.vendor_cd,
			   vendor_name,
			   [vendor_grp],
			   '2'[status_flag],
			   status_desc,
			   [is_usertam],
			   sf.invoice_no,
			   pv_date_dt,
			   sf.account_info,
			   amount,
			   b.[div_id],
			   a.planning_payment_date [planning_payment_dt],
			   bdgDesc.[description] [budget_description]
		from @x a
		--join tb_r_pv_h b on a.pv_no = b.pv_no
		
		join 
		(
			/*
			select o.DIV_ID from tb_m_security_users x
				join TB_M_MAPP_DIV o on o.DEPARTMENT_ID = x.departement_id
			where username=@user
			*/
			select distinct isnull(isnull(aa.div_id,bb.div_id),'0')[div_id]
				from (
					select b.div_id, a.username
					from tb_m_security_users a
					left join tb_m_mapp_div b on a.departement_id = b.department_id
					where a.username = @user
				) aa
				outer apply
				(
					select b.div_id, b.department_id, b.[username] 
					from tb_m_mapp_div b
					where b.[username] = aa.[username]
				) bb
		) b on a.div_id = b.div_id
		outer apply
		(
			select process_id, 
					stuff((
						select ',' + invoice_no 
						from tb_t_upload_d x 
						where x.process_id = ud.process_id 
						for xml path ('')), 1, 1, ''
					) [invoice_no], 
					--stuff((
					--	select ',' + cast(account_info as varchar)
					--	from tb_t_upload_d x 
					--	where x.process_id = ud.process_id 
					--	for xml path ('')), 1, 1, ''
					--) 
					[account_info]--, 
					--cast(substring(max(isnull(invoice_date,(replace(convert(varchar, getdate(), 105),'-','.')))), 7, 4) +'-'+ substring(max(isnull(invoice_date,(replace(convert(varchar, getdate(), 105),'-','.')))), 4, 2) +'-'+ substring(max(isnull(invoice_date,(replace(convert(varchar, getdate(), 105),'-','.')))), 1, 2) as date)
					--[invoice_date_dt], 
					--status_flag
			from tb_t_upload_d ud where ud.process_id=a.process_id
			group by process_id, account_info--, status_flag
		) sf
		outer apply
		(
			select top 1 vw.[description] from vw_WBSElvis vw where vw.WbsNumber = a.budget
		) bdgDesc
		where b.div_id is not null
			and a.status_flag in ('31','32','33','35','36','40','41','42','43','44','45','46','6')
	end	
	else if (@userType = 'Internal' and @levelCode = 'BG' and @statusFlag = '1')
	begin
		select a.process_id,
			   pv_type,
			   pv_date,
			   pv_no,
			   transaction_desc,
			   status_budget,
			   [budget],
			   vendor_cd,
			   vendor_name,
			   [vendor_grp],
			   [status_flag],
			   status_desc,
			   [is_usertam],
			   sf.invoice_no,
			   pv_date_dt,
			   sf.account_info,
			   amount,
			   a.planning_payment_date [planning_payment_dt],
			   bdgDesc.[description] [budget_description]
		from @x a
		outer apply
		(
			select process_id, 
					stuff((
						select ',' + invoice_no 
						from tb_t_upload_d x 
						where x.process_id = ud.process_id 
						for xml path ('')), 1, 1, ''
					) [invoice_no], 
					--stuff((
					--	select ',' + cast(account_info as varchar)
					--	from tb_t_upload_d x 
					--	where x.process_id = ud.process_id 
					--	for xml path ('')), 1, 1, ''
					--) 
					[account_info]--, 
					--cast(substring(max(isnull(invoice_date,(replace(convert(varchar, getdate(), 105),'-','.')))), 7, 4) +'-'+ substring(max(isnull(invoice_date,(replace(convert(varchar, getdate(), 105),'-','.')))), 4, 2) +'-'+ substring(max(isnull(invoice_date,(replace(convert(varchar, getdate(), 105),'-','.')))), 1, 2) as date)
					--[invoice_date_dt], 
					--status_flag
			from tb_t_upload_d ud where ud.process_id=a.process_id
			group by process_id, account_info--, status_flag
		) sf
		outer apply
		(
			select top 1 vw.[description] from vw_WBSElvis vw where vw.WbsNumber = a.budget
		) bdgDesc
		where status_flag = '1';
	end
	else
	begin
		if (@levelCode = 'MD')
		begin
			select a.process_id,
				   pv_type,
				   a.pv_date,
				   a.pv_no,
				   transaction_desc,
				   status_budget,
				   [budget],
				   a.vendor_cd,
				   vendor_name,
				   [vendor_grp],
				   [status_flag],
				   status_desc,
				   [is_usertam],
				   sf.invoice_no,
				   pv_date_dt,
				   sf.account_info,
				   amount,
				   a.planning_payment_date [planning_payment_dt],
				   bdgDesc.[description] [budget_description]
			from @x a
			join --tb_r_pv_h b on a.pv_no = b.pv_no
			(
				/*
				select o.DIV_ID from tb_m_security_users x
					join TB_M_MAPP_DIV o on o.DEPARTMENT_ID = x.departement_id
				where username=@user
				*/
				
				select distinct isnull(isnull(aa.div_id,bb.div_id),'0')[div_id]
				from (
					select b.div_id, a.username
					from tb_m_security_users a
					left join tb_m_mapp_div b on a.departement_id = b.department_id
					where a.username = @user
				) aa
				outer apply
				(
					select b.div_id, b.department_id, b.[username] 
					from tb_m_mapp_div b
					where b.[username] = aa.[username]
				) bb
			) b on a.div_id = b.div_id
			outer apply
			(
				select process_id, 
						stuff((
							select ',' + invoice_no 
							from tb_t_upload_d x 
							where x.process_id = ud.process_id 
							for xml path ('')), 1, 1, ''
						) [invoice_no], 
						--stuff((
						--	select ',' + cast(account_info as varchar)
						--	from tb_t_upload_d x 
						--	where x.process_id = ud.process_id 
						--	for xml path ('')), 1, 1, ''
						--) 
						[account_info]--, 
						--cast(substring(max(isnull(invoice_date,(replace(convert(varchar, getdate(), 105),'-','.')))), 7, 4) +'-'+ substring(max(isnull(invoice_date,(replace(convert(varchar, getdate(), 105),'-','.')))), 4, 2) +'-'+ substring(max(isnull(invoice_date,(replace(convert(varchar, getdate(), 105),'-','.')))), 1, 2) as date)
						--[invoice_date_dt], 
						--status_flag
				from tb_t_upload_d ud where ud.process_id=a.process_id
				group by process_id, account_info--, status_flag
			) sf
			outer apply
			(
				select top 1 vw.[description] from vw_WBSElvis vw where vw.WbsNumber = a.budget
			) bdgDesc
			where b.div_id is not null
				and isnull(@statusFlag, '') = '' or status_flag = @statusFlag	
		end
		else
		begin
			select a.process_id,
				   pv_type,
				   a.pv_date,
				   a.pv_no,
				   transaction_desc,
				   status_budget,
				   [budget],
				   a.vendor_cd,
				   vendor_name,
				   [vendor_grp],
				   [status_flag],
				   status_desc,
				   [is_usertam],
				   sf.invoice_no,
				   pv_date_dt,
				   sf.account_info,
				   amount,
				   a.planning_payment_date [planning_payment_dt],
				   bdgDesc.[description] [budget_description]
			from @x a
			outer apply
			(
				select process_id, 
						stuff((
							select ',' + invoice_no 
							from tb_t_upload_d x 
							where x.process_id = ud.process_id 
							for xml path ('')), 1, 1, ''
						) [invoice_no], 
						--stuff((
						--	select ',' + cast(account_info as varchar)
						--	from tb_t_upload_d x 
						--	where x.process_id = ud.process_id 
						--	for xml path ('')), 1, 1, ''
						--) 
						[account_info]--, 
						--cast(substring(max(isnull(invoice_date,(replace(convert(varchar, getdate(), 105),'-','.')))), 7, 4) +'-'+ substring(max(isnull(invoice_date,(replace(convert(varchar, getdate(), 105),'-','.')))), 4, 2) +'-'+ substring(max(isnull(invoice_date,(replace(convert(varchar, getdate(), 105),'-','.')))), 1, 2) as date)
						--[invoice_date_dt], 
						--status_flag
				from tb_t_upload_d ud where ud.process_id=a.process_id
				group by process_id, account_info--, status_flag
			) sf
			outer apply
			(
				select top 1 vw.[description] from vw_WBSElvis vw where vw.WbsNumber = a.budget
			) bdgDesc
			where isnull(@statusFlag, '') = '' or status_flag = @statusFlag
		end
	end

END;










GO
