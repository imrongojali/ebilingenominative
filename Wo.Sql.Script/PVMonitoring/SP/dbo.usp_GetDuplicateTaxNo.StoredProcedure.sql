USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetDuplicateTaxNo]    Script Date: 6/29/2020 10:20:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[usp_GetDuplicateTaxNo] 
	@result varchar(max) output,
	@pv_no int=0,
	@tax_no varchar(25)

AS
BEGIN
	declare @x varchar(max)
	
	declare	@output table
	(
		val varchar(255)
	)

	insert @output
	exec efb_sp_val_tax_no @pv_no = @pv_no, @tax_no = @tax_no
	
	if (select count(val) from @output) > 1
	begin
		set @result = (select val + '<br/>'
				  from @output
				  for xml path (''))
	end
	else
	begin
		set @result = (select val from @output)
	end
END 


GO
