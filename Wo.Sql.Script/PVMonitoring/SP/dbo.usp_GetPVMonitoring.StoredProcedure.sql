USE [BES]
GO

/****** Object:  StoredProcedure [dbo].[usp_GetPVMonitoring]    Script Date: 7/7/2020 04:01:13 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[usp_GetPVMonitoring]      
   @pid int
AS 
BEGIN 
	SET NOCOUNT ON;

	select distinct
		 h.process_id
		,h.[pv_no]
		,d.seq_no
        ,[invoice_no]
        ,[invoice_date]
        ,cast(substring(invoice_date, 7, 4) +'-'+ substring(invoice_date, 4, 2) +'-'+ substring(invoice_date, 1, 2) as date) as [invoice_date_dt]
        ,[cost_center_cd]
        ,[currency_cd]
        ,[amount]
        ,d.[description]
        ,d.[ok]
        ,[gl_account]
        ,d.[dpp_amount]
        ,[acc_inf_assignment]
        ,[tax_no]
        ,[tax_dt]
        ,[amount_turn_over]
        ,[amount_ppn]
        ,[amount_seal]
        ,[amount_pph21_intern]
        ,[amount_pph21_extern]
        ,[amount_pph26]
        ,[amount_pph_final]
        ,[amount_bm]
        ,[amount_ppn_import]
        ,[amount_ppnbm]
        ,[amount_pph22]
        ,[amount_pnpb]
        ,[amount_bank_charge]
        ,[errmsg]
        ,[amount_sales_other]
        ,[amount_rental_expense]
        ,[amount_transport_operational]
        ,[amount_transport_toll]
        ,[amount_consumption]
        ,[amount_meals_paper]
        ,[amount_office_supplies]
        ,[amount_communication]
        ,[amount_newspaper]
        ,[amount_repair]
        ,[amount_other]
        ,[dpp_pph_amount]
        ,[tax_code_pph23]
        ,[tax_tariff_pph23]
        ,[tax_code_pph21_extern]
        ,[tax_dpp_accumulation_pph21_extern]
        ,[tax_tariff_pph21_extern]
        ,[nik_pph21_extern]
        ,[tax_code_pph26]
        ,[tax_tariff_pph26]
        ,[tax_code_pph_final]
        ,[tax_tariff_pph_final]
        ,[info_pph_final]
        ,[npwp_available]
        ,d.[created_by]
        ,d.[created_dt] as [created_date]
        ,d.[changed_by]
        ,d.[changed_dt] as [changed_date]
		,ph.[status_cd][status_flag]
		,cast(p.[description] as varchar) [status_desc]
		,d.account_info
    from [dbo].[tb_t_upload_d] d 
	    join tb_t_upload_h h on d.process_id = h.process_id
		join tb_r_pv_h ph on h.pv_no = ph.pv_no
	    left join tb_m_parameter p on p.value1 = ph.status_cd and p.key_param = 'pv_status'
	    left join tb_t_mapp_vendor mapven on h.vendor_cd = mapven.vendor_cd
	    left join tb_m_customer_id ci on ci.customer_group = mapven.customer_group_id
	    left join dbo.vw_VendorSAPElvis mv on mv.vendor_cd = h.vendor_cd
	where d.process_id = @pid
	
END;







GO


