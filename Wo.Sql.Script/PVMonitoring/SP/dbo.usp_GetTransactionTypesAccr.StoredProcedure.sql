USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetTransactionTypesAccr]    Script Date: 6/29/2020 10:20:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_GetTransactionTypesAccr]
      @divCd varchar(10) = ''
    , @isPVFormList INT = 0
    , @userDIVs VARCHAR(200) = ''
    , @userDivisionCanUploadDetail int = 0
    , @isSecretary INT = 0
    , @tag INT = 0
    , @isSuspense INT = 0
AS 
BEGIN
SELECT 
	tt.TRANSACTION_CD       [Code]
  , tt.TRANSACTION_NAME     [Name]
  , tt.STD_WORDING          [StandardWording]
 FROM vw_TransactionTypeElvis tt
 LEFT JOIN (SELECT TOP 1 ISNULL(PRODUCTION_FLAG, 0) AS FLAG
            FROM vw_DivisionElvis WHERE DIVISION_ID = @divCd) PROD ON 1 = 1
 LEFT JOIN (
    SELECT DISTINCT
         CASE WHEN ISNUMERIC(items)=1
         THEN CONVERT(int,items)
         ELSE NULL END [TRANSACTION_CD]
    FROM dbo.udf_explode(',',
        (
        SELECT TOP 1 SYSTEM_VALUE_TXT
        FROM vw_TbMSystemElvis
        WHERE SYSTEM_TYPE = 'PvForm' AND SYSTEM_CD = 'SUSPENSE_TRANSACTION_CD'
        )
    ) TX
 ) STX ON tt.TRANSACTION_CD = STX.TRANSACTION_CD
 WHERE    ISNULL(tt.CLOSE_FLAG,0)=0

 AND (    (@isPVFormList = 0)    
       OR tt.TRANSACTION_CD!=219  -- can input Accrued, except in in PvFormList 
     )
AND (    (ISNULL(tt.TRANSACTION_LEVEL_CD, 0) = 0) --- special transaction per division 
      OR (tt.TRANSACTION_LEVEL_CD IN
            (
                (
                    SELECT ITEMS
                    FROM dbo.udf_explode(',',isnull(@userDIVs,'0'))
                )
            )

         )
      OR (     ISNULL(tt.TRANSACTION_LEVEL_CD, 0)= 99  -- only secretary can do it
           AND @isSecretary = 1)
    )
AND (   (@userDivisionCanUploadDetail=0)
     OR
        (    (NULLIF(tt.PRODUCTION_FLAG,0) IS NULL)
         OR (tt.PRODUCTION_FLAG = ISNULL(PROD.FLAG, 0))
        )
    )
AND (   (ISNULL(NULLIF(@tag,-1),0) = 0)
     OR
        (tt.TEMPLATE_CD IS NOT NULL)
    )
AND (   (@isSuspense=0)
     OR (stx.TRANSACTION_CD IS NOT NULL)
    )
END; 


GO
