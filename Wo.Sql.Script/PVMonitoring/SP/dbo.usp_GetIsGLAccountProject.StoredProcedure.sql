USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetIsGLAccountProject]    Script Date: 6/29/2020 10:20:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_GetIsGLAccountProject] 
   @div int = NULL
AS 
BEGIN 
	SET NOCOUNT ON;
  
	select count(glap.gl_acc) is_gl_account_project
    from vw_GLAccountMappingElvis  m
    left join (select ltrim(rtrim(system_value_txt)) gl_acc from vw_TbMSystemElvis 
				    where system_type = 'gl_account_project') glap on glap.gl_acc= m.gl_account
    where m.item_transaction_cd = 1  and m.transaction_cd = @div
    group by m.transaction_cd, m.gl_account

END;

GO
