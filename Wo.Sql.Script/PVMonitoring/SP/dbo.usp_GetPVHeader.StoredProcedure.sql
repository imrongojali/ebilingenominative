USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetPVHeader]    Script Date: 6/29/2020 10:20:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[usp_GetPVHeader]    
   @pid int
AS 
BEGIN 
	SET NOCOUNT ON;

	select distinct
	   x.process_id,
	   a.pv_no,
	   a.pv_year, 
	   a.status_cd,
	   a.pay_method_cd,
	   a.vendor_group_cd,
	   a.pv_type_cd,
	   cast(b.value2 as varchar) [pv_type], 
	   a.transaction_cd,
	   a.suspense_no,
	   a.activity_date_from, 
	   a.activity_date_to, 
	   a.division_id,
	   a.refference_no,
	   a.bank_type,
	   a.posting_date,
	   cast(c.value2 as varchar)[payment_method], 
	   a.vendor_cd,
	   d.vendor_name,
	   cast(e.value2 as varchar) [vendor_grp],
	   f.std_wording [transaction_desc],
	   a.budget_no [budget],
	   total_amount [amount],
	   a.created_date [pv_date],
	   cast(x.rejected_reason as varchar) [rejected_reason],
	   z.total_turnover,
	   a.created_by,
	   a.status_compare,
	   a.expired_date,
	   a.url_wht,
	   convert(varchar,x.rejected_dt,105) [rejected_dt],
	   replace(x.[rejected_by],'TAM\','') [rejected_by],
	   dsc.[description],
	   div.[division_name]
	from tb_r_pv_h a
	join tb_t_upload_h x
		on a.pv_no = x.pv_no
	left join tb_m_parameter b 
		on b.value1 = a.pv_type_cd
			and b.key_param = 'pv_type'
	left join tb_m_parameter c 
		on c.value1 = a.pay_method_cd
			and c.key_param = 'pv_pay_method'
	left join vw_vendorSAPElvis d 
		on a.vendor_cd = d.vendor_cd
	left join tb_m_parameter e 
		on e.value1 = a.vendor_group_cd
			and e.key_param = 'pv_vendor_grp'
	left join vw_TransactionTypeElvis f
		on a.transaction_cd = f.transaction_cd
	outer apply
	(
		select pvd.process_id, sum(pvd.amount) [total_turnover]
		from tb_t_pv_d pvd
		where pvd.process_id = x.process_id
			and pvd.errmsg='TURN_OVER'
		group by pvd.process_id
	) z
	outer apply
	(
		select top 1 dd.[description] from tb_t_upload_d dd where dd.process_id = x.process_id
	) dsc
	outer apply
	(
		select top 1 division_name from vw_DivisionElvis vv where vv.division_id = a.division_id
	) div
	where x.process_id = @pid

END;








GO
