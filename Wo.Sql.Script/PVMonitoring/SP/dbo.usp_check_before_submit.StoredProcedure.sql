USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[usp_check_before_submit]    Script Date: 6/29/2020 10:20:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_check_before_submit] 
	-- Add the parameters for the stored procedure here
	@pv_no int--,
	--@result varchar(max) output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @x table
	(
		val varchar(255),
		pv_no int
	)

    -- Insert statements for procedure here

	insert @x
	select case when bank_key is null then 
				'PV_NO: ' + cast(pv_no as varchar) +  ' MSG: No selected bank account'
				else
				null
		   end, pv_no
	from TB_T_UPLOAD_H 
	where pv_no=@pv_no	

	insert @x
	select case when b.REFERENCE_NO is null then 
				'PV_NO: ' + cast(@pv_no as varchar) +  ' MSG: No uploaded attachment'
				else
				null
		   end, @pv_no
	from TB_R_PV_H a
	left join TB_R_PV_MONITORING_ATTACHMENT b
	on b.REFERENCE_NO = (cast(a.PV_NO as varchar) + cast(a.PV_YEAR as varchar))
	where a.pv_no=@pv_no

	select val from @x
	where val is not null
END




GO
