USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateBudgetPVMonitoring]    Script Date: 6/29/2020 10:20:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_UpdateBudgetPVMonitoring]    
   @pid int,
   @statusBudget varchar(225),
   @budgetNo varchar(125)
AS 
BEGIN 
	SET NOCOUNT ON;
	update tb_t_upload_h set status_budget = @statusBudget
	where process_id = @pid

	update tb_r_pv_h set BUDGET_NO = @budgetNo 
	where PV_NO = (select top 1 PV_NO from TB_T_UPLOAD_H where process_id=@pid)
END;






GO
