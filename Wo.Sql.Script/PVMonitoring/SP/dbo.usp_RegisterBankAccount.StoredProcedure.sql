USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[usp_RegisterBankAccount]    Script Date: 6/29/2020 10:20:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_RegisterBankAccount](
		@pid varchar(16),
		@vendor varchar(10),
        @bank varchar(15),
        @bankAccount varchar(20),
        @username varchar(20),
		@beneficiaries varchar(60)
) 
AS 
BEGIN
	SET NOCOUNT ON;

	declare @bankName varchar(60), @directory varchar(75), @file_name varchar(75), @guid_filename varchar(75), @seq_no int
	select @bankName = bank_name from sap_db.dbo.tb_m_bank where bank_key = @bank

	select top 1 @seq_no= seq_no, @directory = directory, @file_name =file_name, @guid_filename =guid_filename 
	from tb_t_bank_attachment
	where cast(created_dt as date) = cast(getdate() as date)
		and vendor_cd = @vendor
		and created_by = @username
		and used is null
	order by seq_no desc

	--select @seq_no, @directory, @file_name, @guid_filename, @vendor return
	
	insert TB_M_VENDOR_BANK (vendor_cd, ctry, bank_key, bank_account, name_of_bank, currency, status_req, beneficiaries, directory, file_name, guid_filename, created_by, created_dt)
		values (@vendor, 'ID', @bank, @bankAccount, @bankName, 'IDR', '1', @beneficiaries, @directory, @file_name, @guid_filename, @username, getdate())

	if(@seq_no is not null)
	begin
		update tb_t_bank_attachment set used='Y' where vendor_cd= @vendor and seq_no = @seq_no
	end
END;



GO
