USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetPVAttachment]    Script Date: 6/29/2020 10:20:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[usp_GetPVAttachment]    
   @pid int
AS 
BEGIN 
	SET NOCOUNT ON;

	declare @reffNo varchar(max);

	if exists (
		select distinct cast(b.pv_no as varchar) + cast(b.pv_year as varchar)[reff_no]
		from tb_t_upload_h a
		join tb_r_pv_h b
			on a.pv_no = b.pv_no
		where a.process_id = @pid
	)
	begin
		select distinct  @reffNo = cast(b.pv_no as varchar) + cast(b.pv_year as varchar)
		from tb_t_upload_h a
		join tb_r_pv_h b
			on a.pv_no = b.pv_no
		where a.process_id = @pid
	end
	else
	begin
		set @reffNo = '[PID]' + cast(@pid as varchar) + cast(year(getdate()) as varchar)
	end

	select 
		REFERENCE_NO	
		,REF_SEQ_NO	
		,ATTACH_CD	
		,[DESCRIPTION]	
		,DIRECTORY	
		,[FILE_NAME]
	from TB_R_PV_MONITORING_ATTACHMENT x
	where x.REFERENCE_NO = @reffNo


END;





GO
