USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[usp_NotProperPVMonitoring]    Script Date: 6/29/2020 10:20:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_NotProperPVMonitoring] 
	-- Add the parameters for the stored procedure here
	@pv_no int,
	@reason varchar(max),
	@user varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		
		update tb_t_upload_h set rejected_reason = @reason, changed_by = @user, changed_dt = getdate(), rejected_by = @user, rejected_dt = getdate()
		where pv_no = @pv_no

		update tb_r_pv_h set status_cd = '6', changed_by = @user, changed_date = getdate()
		where pv_no = @pv_no

		declare @reff_no varchar(max), @seqLatest int;

		select @reff_no = cast(@pv_no as varchar) + cast([pv_year] as varchar)
		from tb_r_pv_h 
		where pv_no = @pv_no

		select top 1 @seqLatest = seq_no
		from tb_h_distribution_status
		where reff_no = @reff_no
		order by seq_no desc

		if (@seqLatest is null)
		begin
			set @seqLatest = 0;
		end

		--select * from tb_m_parameter where key_param='pv_status'
		insert tb_h_distribution_status
		(
			 reff_no	
			,seq_no	
			,status_cd	
			,date	
			,created_by	
			,created_dt
		)
		values
		(
			 @reff_no
			,@seqlatest + 1
			,6
			,getdate()
			,replace(@user,'TAM\','')
			,getdate()
		)

		/*
		update d set status_flag = '6'
		from tb_t_upload_h h
			left join tb_t_upload_d d on h.PROCESS_ID = d.PROCESS_ID
		where pv_no=@pv_no
		*/
END




GO
