USE [BES]
GO
/****** Object:  View [dbo].[vw_CostCenterElvis]    Script Date: 6/2/2020 10:16:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vw_CostCenterElvis]
AS

select 
	 cost_center	
	,[description]
	,division	
	,production_flag
from elvis_db.dbo.vw_CostCenter




GO
