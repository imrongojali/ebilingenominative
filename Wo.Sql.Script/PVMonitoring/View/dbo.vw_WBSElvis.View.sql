USE [BES]
GO
/****** Object:  View [dbo].[vw_WBSElvis]    Script Date: 6/2/2020 10:16:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[vw_WBSElvis]
AS

select 
	 WbsNumber	
	,[Year]
	,EOA	
	,Division	
	,[Description]
from elvis_db.dbo.vw_WBS


GO
