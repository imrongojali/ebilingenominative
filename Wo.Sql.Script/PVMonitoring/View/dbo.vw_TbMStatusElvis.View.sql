USE [BES]
GO
/****** Object:  View [dbo].[vw_TbMStatusElvis]    Script Date: 6/2/2020 10:16:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_TbMStatusElvis]
AS

SELECT 
	 STATUS_CD	
	,STATUS_NAME	
	,MODULE_CD	
	,SLA_UNIT	
	,SLA_UOM
FROM ELVIS_DB.dbo.TB_M_STATUS


GO
