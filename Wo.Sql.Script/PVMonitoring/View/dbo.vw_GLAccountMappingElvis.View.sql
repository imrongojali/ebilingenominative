USE [BES]
GO
/****** Object:  View [dbo].[vw_GLAccountMappingElvis]    Script Date: 6/2/2020 10:16:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_GLAccountMappingElvis]
AS

select 
	 TRANSACTION_CD	
	,ITEM_TRANSACTION_CD	
	,GL_ACCOUNT	
	,NEW_ITEM_TRANSACTION_CD	
	,NEW_FORMULA	
	,SUM_UP_FLAG
from elvis_db.dbo.tb_m_gl_account_mapping



GO
