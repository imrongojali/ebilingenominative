USE [BES]
GO
/****** Object:  View [dbo].[vw_ExchangeRateElvis]    Script Date: 6/2/2020 10:16:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_ExchangeRateElvis] AS 
	
	select 
		 currency_cd	
		,exchange_rate	
		,valid_from
	from ELVIS_DB.dbo.vw_ExchangeRate
	

GO
