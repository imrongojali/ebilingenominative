USE [BES]
GO
/****** Object:  View [dbo].[vw_WbsNumberFromAccrBalance]    Script Date: 6/2/2020 10:16:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vw_WbsNumberFromAccrBalance] AS

	SELECT DISTINCT w.[WbsNumber], w.[Description], a.[booking_no]
	FROM ELVIS_DB.dbo.[TB_R_ACCR_BALANCE] a 
	JOIN ELVIS_DB.dbo.[vw_WBS] w on a.[WBS_NO_OLD] = w.[WbsNumber] 



GO
