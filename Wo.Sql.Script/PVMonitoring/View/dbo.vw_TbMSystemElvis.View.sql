USE [BES]
GO
/****** Object:  View [dbo].[vw_TbMSystemElvis]    Script Date: 6/2/2020 10:16:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[vw_TbMSystemElvis]
AS

SELECT 
	ID	
	,SYSTEM_TYPE	
	,SYSTEM_CD	
	,SYSTEM_VALUE_TXT	
	,SYSTEM_VALUE_NUM	
	,SYSTEM_VALUE_DT
FROM ELVIS_DB.dbo.TB_M_SYSTEM



GO
