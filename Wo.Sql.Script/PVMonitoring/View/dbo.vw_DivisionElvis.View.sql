USE [BES]
GO
/****** Object:  View [dbo].[vw_DivisionElvis]    Script Date: 6/2/2020 10:16:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_DivisionElvis]
AS
SELECT 
	d.DIVISION_ID
	,CASE
		WHEN CHARINDEX ('-',REVERSE (d.DIVISION_NAME)) > 0 
		THEN RTRIM (SUBSTRING(d.DIVISION_NAME, 0, LEN (d.DIVISION_NAME) - 
				CHARINDEX ('-',REVERSE (d.DIVISION_NAME)) +1))
		ELSE d.DIVISION_NAME
	END AS DIVISION_NAME
	, ISNULL(d.PRODUCTION_FLAG, 0) AS PRODUCTION_FLAG
	, FULL_NAME
FROM TMMIN_ROLE.dbo.TB_M_DIVISION d


GO
