insert tb_m_parameter values
 ('pv_errmegs_regular','Error! download error message here : <a id=''download-errtxt'' onclick=''onDownloadErrTxt(this);'' data-id=''{0}''>{1}</a>','1',NULL,getdate(),'system',NULL,NULL,NULL)
,('pv_success_budget','Upload success. download success message here :<br/><a id=''download-errtxt'' onclick=''onDownloadErrTxt(this);'' data-id=''{0}''>{1}</a>','1',NULL,getdate(),'system',NULL,NULL,'0')
,('pv_errmegs_submit','Submit failed. download error message here : <a id=''download-errtxt'' onclick=''onDownloadErrTxt(this);'' data-id=''{0}''>{1}</a>','1',NULL,getdate(),'system',NULL,NULL,NULL)
,('pv_errmegs_uploadxls','Upload failed. download error message here :<br/><a id=''download-errtxt'' onclick=''onDownloadErrTxt(this);'' data-id=''{0}''>{1}</a>','1',NULL,getdate(),'system',NULL,NULL,NULL)
,('pv_errmegs_tax','Error validation Tax. download error message here :<br/><a id=''download-errtxt'' onclick=''onDownloadErrTxt(this);'' data-id=''{0}''>{1}</a>','1',NULL,getdate(),'system',NULL,NULL,NULL)
,('pv_emailto_bank_req','Email to bank requested','herlina.sembiring@toyota.astra.co.id',NULL,getdate(),'system',NULL,NULL,NULL)
,('pv_check_efb','Validasi EFB','0','pv_check_faktur',getdate(),'system',NULL,NULL,NULL)
,('allowed_trans_type','allowed transaction type ebilling pv','105,214,219,233,111,117,135,161,211,220,169,176,202,170,171,172,221,222,173',NULL,getdate(),'system',NULL,NULL,'0')
,('pv_monitoring_url','url pv monitoring','http://localhost:60534/Core/PVMonitoring',NULL,getdate(),'wo.apiyudin',NULL,NULL,NULL) --dipastikan di PROD
,('pv_expired','Expired day','30',NULL,getdate(),'system',NULL,NULL,NULL)
,('pv_vendor_grp','Vendor Group','4','One Time Vendor',getdate(),'system',NULL,NULL,'0')
,('pv_vendor_grp','Vendor Group','3','External',getdate(),'system',NULL,NULL,'0')
,('pv_vendor_grp','Vendor Group','2','Internal-Division',getdate(),'system',NULL,NULL,'0')
,('pv_vendor_grp','Vendor Group','1','Internal-Individual',getdate(),'system',NULL,NULL,'0')
,('pv_pay_method','Payment Method','T','TRANSFER',getdate(),'system',NULL,NULL,'0')
,('pv_pay_method','Payment Method','C','CASH',getdate(),'system',NULL,NULL,'0')
,('pv_pay_method','Payment Method','A','TRANSFER MANUAL',getdate(),'system',NULL,NULL,'0')
,('pv_type','PV Type','3','Settlement',getdate(),'system',NULL,NULL,'0')
,('pv_type','PV Type','2','Suspense',getdate(),'system',NULL,NULL,'0')
,('pv_type','PV Type','1','Direct',getdate(),'system',NULL,NULL,'0')
,('pv_status','Expired','8','pv_expired',getdate(),'system',NULL,NULL,NULL)
,('pv_status','Deleted','7','pv_deleted',getdate(),'system',NULL,NULL,NULL)
,('pv_status','Rejected Budget','6','pv_rejected_budget',getdate(),'system',NULL,NULL,NULL)
,('pv_status','Rejected FD Staff (ELVIS)','42','pv_rejected_elvis',getdate(),'system',NULL,NULL,NULL)
,('pv_status','Submit to TAM Finance','4','pv_submited_dealer',getdate(),'system',NULL,NULL,NULL)
,('pv_status','Verified Budget','3','pv_verified_budget',getdate(),'system',NULL,NULL,NULL)
,('pv_status','Only Rejected','2','pv_only_rejected',getdate(),'system',NULL,NULL,NULL)
,('pv_status','Submission','1','pv_submission',getdate(),'system',NULL,NULL,NULL)
--,('pv_posting_planning_date','Hardcode posting date and planning payment date','10','30',getdate(),'system',NULL,NULL,NULL)
--,('pv_activity_date','Hardcode activity date from and to','0','1',getdate(),'system',NULL,NULL,NULL)
,('pv_url_elvis','http://localhost:8361/','1',NULL,getdate(),'system',NULL,NULL,NULL) --dipastikan di PROD untuk web services nya
,('pv_bank_status','On Request','1',NULL,getdate(),'system',NULL,NULL,NULL)
,('pv_attachment_type','General Attachment','4',NULL,getdate(),'system',NULL,NULL,NULL)
-- mba ista
--,('summary_customer_group_all','tb_m_customer_group all','ALL-CUST','ALL',getdate(),'system',NULL,NULL,NULL)
,('summary_inv_sign','AR Controller','Erfelinda Noorkhaista','TAM\erfelinda.n',getdate(),'system',NULL,NULL,'0')
,('summary_inv_sign','Section Head','Denny Pratama Solihin','TAM\denny.solihin',getdate(),'system',NULL,NULL,'0')
,('summary_tot_amount','Included pph','Include','Include pph 22',getdate(),'system',NULL,NULL,'0')
,('summary_tot_amount','Excluded pph','Exclude','Exclude pph 22',getdate(),'system',NULL,NULL,'0')
,('grid_init_tb_t_udf_invoice','Initial grid screen with data tb_t_udf_invoice','1',NULL,getdate(),'system',NULL,NULL,'0')
,('exclude_vat','VAT for Batam Customer','3105','',getdate(),'system',NULL,'','0')

insert tb_m_config values
 ('Core','FileFPFolder','Folder','/efb_path (Vsv-c003-016018)/',NULL,'text',NULL,NULL,NULL,NULL,NULL) --dipastikan di PROD
,('Core','FileFPUserName','UserID','ftpuser',NULL,'text',NULL,NULL,NULL,NULL,NULL) --dipastikan di PROD
,('Core','FileFPPassword','Pass','P@ssw0rd',NULL,'text',NULL,NULL,NULL,NULL,NULL) --dipastikan di PROD
,('Core','FileFPServer','Server','10.85.40.199',NULL,'text',NULL,NULL,NULL,NULL,NULL) --dipastikan di PROD

,('Core','FilePVMonitoringServer','Server','VSV-C005-014002:8023',NULL,'text',NULL,NULL,NULL,NULL,NULL) --dipastikan di PROD [updated]
,('Core','FilePVMonitoringPassword','Pass','Toyota2013',NULL,'text',NULL,NULL,NULL,NULL,NULL) --dipastikan di PROD [updated]
,('Core','FilePVMonitoringUserName','UserID','K2.admin',NULL,'text',NULL,NULL,NULL,NULL,NULL) --dipastikan di PROD [updated]

insert tb_m_email_template values
 ('Notification work list for PIC Budget','email_key','Noreply TAM E-billing ',NULL,NULL,'[EBilling] Worklist for PIC Budget',
 '
 <p>Dear Ma''am / Sir,<p/><p>You have {0}({1} new) worklist to be approved</p><p><a href="{2}">[Go to worklist] Click this link to display all worklist on Ebiling.</a></p><p><br/>Best Regards,</p><p>Ebilling Admin</p>
 '
 ,NULL,NULL,NULL,NULL,'0')

,('Notification work list for PIC Dealer','email_key','Noreply TAM E-billing ',NULL,NULL,'[EBilling] Worklist for PIC Dealer',
 '
 <p>Dear Ma''am / Sir,<p/><p>You have {0}({1} new) worklist to be completed<br/>Please complete the data within 30 days after this message sent. Data above 30 days will be expire.</p><p><table><tr><th align="left" style="width:100px;">PV No</th><th align="left" style="width:80px;">Age (day)</th></tr>{2}</table></p><p><a href="{3}">[Go to worklist] Click this link to display all worklist on Ebiling.</a></p><p><br/>Best Regards,</p><p>Ebilling Admin</p>
 '
 ,NULL,NULL,NULL,NULL,'0')

,('Register Bank Account Notification','email_key','Noreply TAM E-billing ',NULL,NULL,'[EBilling] Register Bank Account Notification',
'
<p>Kepada Bapak/Ibu,<p/><p>Berikut request terkait penambahan akun rekening.<p><table><tr><th align="left" style="width:150px;">Bank Account</th><th align="left" style="width:200px;">Bank</th><th align="left" style="width:100px;">Status</th></tr>{0}</table></p><p><br/>Salam,<br/>Ebilling Admin</p>
'
,NULL,NULL,NULL,NULL,'0')

,('Reject Notification','email_key','Noreply TAM E-billing ',NULL,NULL,'[EBilling] Reject Notification',
'
<p>Kepada Bapak/Ibu,<p/><p>Berikut notice terkait dengan PV No {0},<p><table style="border:solid;color:black;"><tr><td align="left" style="width:350px;">Reject reason:<br/> {1}</td></tr></table></p><p>Pastikan anda sudah login ke E-Billing sebelum masuk ke link di bawah ini.<br/>Silakan klik link berikut ini :<br/><a href="{2}">Click</a></p><p><br/>Salam,<br/>Ebilling Admin</p>
'
,NULL,NULL,NULL,NULL,'0')

insert tb_m_menu values
(NULL,'1','ELVIS','~/Core/PVMonitoring','ELVIS','fa fa-copy','7','1',NULL,NULL,NULL,NULL,NULL)

insert tb_m_feature values
 ('[menu_id]','upload_lod','Upload Letter of Disbursment',getdate(),'system',NULL,NULL,'0') --dipastikan di PROD [menu_id]
,('[menu_id]','download_template','Download Template',getdate(),'system',NULL,NULL,'0') --dipastikan di PROD [menu_id]
,('[menu_id]','verified_pic','Verified PIC',getdate(),'system',NULL,NULL,'0') --dipastikan di PROD [menu_id]
,('[menu_id]','delete_lod','Delete Letter of Disbursment',getdate(),'system',NULL,NULL,'0') --dipastikan di PROD [menu_id]
,('[menu_id]','notproper_bg','Not proper by BG',getdate(),'system',NULL,NULL,'0') --dipastikan di PROD [menu_id]

insert tb_r_feature_role values
 ('[internal_id]','BG',getdate(),'system',NULL,NULL,'0') --verified_pic --dipastikan di PROD [internal_id]
,('[internal_id]','MD',getdate(),'system',NULL,NULL,'0') --upload_lod --dipastikan di PROD [internal_id]
,('[internal_id]','MD',getdate(),'system',NULL,NULL,'0') --download_template --dipastikan di PROD [internal_id]
,('[internal_id]','MD',getdate(),'system',NULL,NULL,'0') --delete_lod --dipastikan di PROD [internal_id]
,('[internal_id]','BG',getdate(),'system',NULL,NULL,'0') --notproper_bg --dipastikan di PROD [internal_id]

insert tb_m_level values
 ('BG','0','Budget Section',getdate(),'sistem',NULL,NULL,'0')
,('AD','0','Admin Dealer Claim',getdate(),'sistem',NULL,NULL,'0')



insert tb_r_menu_role values
 ('[menu_id]','MD',getdate(),'system',NULL,NULL,'0')
,('[menu_id]','BG',getdate(),'system',NULL,NULL,'0')
,('[menu_id]','AD',getdate(),'system',NULL,NULL,'0')

/*
insert bes.dbo.tb_m_security_users values
 ('Sulihati',NULL,'MD',NULL,'TAM\sulihati',NULL,'sulihati@toyota.astra.co.id','0',NULL,getdate(),'system',NULL,NULL,'0')
,('Sulihati',NULL,'MD',NULL,'TAM\sulihatiaa',NULL,'sulihati@toyota.astra.co.id','0',NULL,getdate(),'system',NULL,NULL,'0')
,('Sulihati',NULL,'MD',NULL,'TAM\sulihatiab',NULL,'sulihati@toyota.astra.co.id','0',NULL,getdate(),'system',NULL,NULL,'0')
,('Sulihati',NULL,'MD',NULL,'TAM\sulihatiac',NULL,'sulihati@toyota.astra.co.id','0',NULL,getdate(),'system',NULL,NULL,'0')
,('Sulihati',NULL,'MD',NULL,'TAM\sulihatiad',NULL,'sulihati@toyota.astra.co.id','0',NULL,getdate(),'system',NULL,NULL,'0')
,('Sulihati',NULL,'MD',NULL,'TAM\sulihatiam',NULL,'sulihati@toyota.astra.co.id','0',NULL,getdate(),'system',NULL,NULL,'0')
,('Putri Fitri',NULL,'MD',NULL,'TAM\yuyun2',NULL,'yuyun2@toyota.astra.co.id','0',NULL,getdate(),'system',NULL,NULL,'0')
,('Yuyun',NULL,'MD',NULL,'TAM\putrif102331',NULL,'putri.fitri@toyota.astra.co.id','0',NULL,getdate(),'system',NULL,NULL,'0')
,('Elvira Handayani',NULL,'MD',NULL,'TAM\elvira.handayani',NULL,'putri.fitri@toyota.astra.co.id','0',NULL,getdate(),'system',NULL,NULL,'0')
,('Eka Utari',NULL,'MD',NULL,'TAM\eka.utari',NULL,'eka.utari@toyota.astra.co.id','0',NULL,getdate(),'system',NULL,NULL,'0')
,('Achmad Mauludin',NULL,'MD',NULL,'TAM\achmad.mauludin',NULL,'eka.utari@toyota.astra.co.id','0',NULL,getdate(),'system',NULL,NULL,'0')
,('Ariani Uki Wiyanti',NULL,'AD',NULL,'TAM\ariani.wiyanti',NULL,'ariani.wiyanti@toyota.astra.co.id','0',NULL,getdate(),'system',NULL,NULL,'0')
,('Dewi Nova Sitorus',NULL,'BG',NULL,'TAM\dewi.sitorus',NULL,'xxxx@toyota.astra.co.id','0',NULL,getdate(),'system',NULL,NULL,'0')
*/

insert tb_t_mapp_vendor values
 ('0000150018','AAP','system',getdate(),NULL,NULL,'1')
,('0000150045','NRM','system',getdate(),NULL,NULL,'1')
,('0000150064','KDA','system',getdate(),NULL,NULL,'1')
,('0000150065','SIM','system',getdate(),NULL,NULL,'1')
,('0000150205','INC','system',getdate(),NULL,NULL,'1')
,('0000150140','HSJ','system',getdate(),NULL,NULL,'1')
,('0000150462','KSM','system',getdate(),NULL,NULL,'1')
,('0000251581','SAM','system',getdate(),NULL,NULL,'1')
,('0000251793','SJM','system',getdate(),NULL,NULL,'1')
,('0000251811','BJM','system',getdate(),NULL,NULL,'1')
,('0000252464','RJM','system',getdate(),NULL,NULL,'1')
,('0000252466','DLT','system',getdate(),NULL,NULL,'1')
,('0000252472','DCM','system',getdate(),NULL,NULL,'1')
,('0000252473','SLM','system',getdate(),NULL,NULL,'1')
,('0000252477','WLD','system',getdate(),NULL,NULL,'1')
,('0000252480','TAG','system',getdate(),NULL,NULL,'1')
,('0000252481','IAB','system',getdate(),NULL,NULL,'1')
,('0000252490','ANA','system',getdate(),NULL,NULL,'1')
,('0000252495','GMA','system',getdate(),NULL,NULL,'1')
,('0000150585','DAS','system',getdate(),NULL,NULL,'1')
,('0000250062','HKL','system',getdate(),NULL,NULL,'1')
,('0000250211','ASR','system',getdate(),NULL,NULL,'1')
,('0000250772','PAR','system',getdate(),NULL,NULL,'1')
,('0000252926','MMR','system',getdate(),NULL,NULL,'1')
,('0000150170','AR','system',getdate(),NULL,NULL,'1') 
,('0000150067','SM','system',getdate(),NULL,NULL,'1') 
,('0000150027','LM','system',getdate(),NULL,NULL,'1') 
,('0000150031','SS','system',getdate(),NULL,NULL,'1') 
,('0000150441','WP','system',getdate(),NULL,NULL,'1') 
,('0000150490','DB','system',getdate(),NULL,NULL,'1') 
,('0000200014','AI','system',getdate(),NULL,NULL,'1') 
,('0000250001','AA','system',getdate(),NULL,NULL,'1') 
,('0000250507','PP','system',getdate(),NULL,NULL,'1') 
,('0000251485','SH','system',getdate(),NULL,NULL,'1') 
,('0000252506','MM','system',getdate(),NULL,NULL,'1') 
,('0000252826','LM','system',getdate(),NULL,NULL,'1') 
,('0000251203','TNS','system',getdate(),NULL,NULL,'1')

insert bes.dbo.tb_m_mapp_div values
 ('54',NULL,'system',getdate(),NULL,NULL,'TAM\della.seko')
,('56',NULL,'system',getdate(),NULL,NULL,'TAM\eneng')
,('48',NULL,'system',getdate(),NULL,NULL,'TAM\ernawati')
,('66',NULL,'system',getdate(),NULL,NULL,'TAM\sulihati')
,('71',NULL,'system',getdate(),NULL,NULL,'TAM\sulihatiaa')
,('92',NULL,'system',getdate(),NULL,NULL,'TAM\sulihatiab')
,('93',NULL,'system',getdate(),NULL,NULL,'TAM\sulihatiac')
,('94',NULL,'system',getdate(),NULL,NULL,'TAM\sulihatiad')
,('66',NULL,'system',getdate(),NULL,NULL,'TAM\sulihatiam')
,('97',NULL,'system',getdate(),NULL,NULL,'TAM\yuyun2')
,('42',NULL,'system',getdate(),NULL,NULL,'TAM\putrif102331')
,('98',NULL,'system',getdate(),NULL,NULL,'TAM\harpina.kemit')
,('52',NULL,'system',getdate(),NULL,NULL,'TAM\elvira.handayani')
,('58',NULL,'system',getdate(),NULL,NULL,'TAM\danny.hendrata')
,('57',NULL,'system',getdate(),NULL,NULL,'TAM\eka.utari')
,('57',NULL,'system',getdate(),NULL,NULL,'TAM\achmad.mauludin')

use bes
select * from tb_m_security_users

select * from tb_m_customer_id

select * from tb_m_parameter 

select a.CUSTOMER_GROUP_ID, b.customer_group_name 
from TB_T_MAPP_VENDOR a
left join tb_m_customer_group b
	on a.CUSTOMER_GROUP_ID = b.customer_group_id
where b.customer_group_name like '%AKASTRA%'

insert TB_T_MAPP_VENDOR values
('0000252505','AKG','system',getdate(),NULL,NULL,'1')

select * from FD