USE [BES]
GO

/****** Object:  Table [dbo].[tb_m_feature]    Script Date: 10/28/2019 3:28:07 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tb_m_feature](
	[internal_id] [int] IDENTITY(1,1) NOT NULL,
	[menu_id] [int] NULL,
	[feature_code] [nvarchar](200) NOT NULL,
	[feature_desc] [nvarchar](200) NULL,
	[created_date] [datetime] NOT NULL,
	[created_by] [varchar](50) NOT NULL,
	[modified_date] [datetime] NULL,
	[modified_by] [varchar](50) NULL,
	[row_status] [smallint] NOT NULL,
 CONSTRAINT [PK_tb_m_feature] PRIMARY KEY CLUSTERED 
(
	[internal_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[tb_m_feature] ADD  CONSTRAINT [DF_tb_m_feature_row_status]  DEFAULT ((0)) FOR [row_status]
GO

ALTER TABLE [dbo].[tb_m_feature]  WITH CHECK ADD  CONSTRAINT [FK_tb_m_feature_tb_m_menu] FOREIGN KEY([menu_id])
REFERENCES [dbo].[tb_m_menu] ([menu_id])
GO

ALTER TABLE [dbo].[tb_m_feature] CHECK CONSTRAINT [FK_tb_m_feature_tb_m_menu]
GO


