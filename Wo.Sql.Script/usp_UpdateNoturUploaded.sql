USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateNoturUploaded]    Script Date: 10/4/2019 4:15:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		isna
-- Create date: 22 Oktober 2018
-- Description:	Update Notur_uploaded
-- =============================================
ALTER PROCEDURE [dbo].[usp_UpdateNoturUploaded] 
	-- Add the parameters for the stored procedure here
	@reff varchar(15)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [tb_r_mvda_invoice]
	SET [tb_r_mvda_invoice].notur_uploaded = 1
	WHERE [tb_r_mvda_invoice].da_no = @reff

	UPDATE [tb_r_da_detil]
	SET [tb_r_da_detil].notur_uploaded = 1
	WHERE [tb_r_da_detil].da_number = @reff

	-- add by wo\dwi 20191004
	UPDATE tb_r_spda_invoice_header
	SET NOTUR_UPLOAD = 1
	WHERE ca_no = @reff
END


