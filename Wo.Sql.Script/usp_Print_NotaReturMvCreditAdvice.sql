USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[usp_Print_DebitAdvice]    Script Date: 9/18/2019 2:47:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		wo\dwi
-- Create date: 2019-10-30
-- Description:	Print Nota Retur (MV) Credit Advice
-- =============================================
ALTER PROCEDURE [dbo].[usp_Print_NotaReturMvCreditAdvice] 
	-- Add the parameters for the stored procedure here
	 @ca_number varchar(20) ='' --FCA/SP/17C0313
	,@user varchar(50)='' -- 60000
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select
		'FDA/MV/' + m.da_no as NomorRetur,
		(select top 1 t.tax_no from tb_r_mvda_invoice t where t.da_no = m.da_cn_no) as FakturPajakNo,
		m.do_date as TglRetur,
		(select top 1 t.do_date from tb_r_mvda_invoice t where t.da_no = m.da_cn_no) as TglFakturPajak, 
		m.dealer_name as CustName,
		m.address as Address1,
		'' as Address2,
		m.npwp_no as Npwp,
		m.rrn_no as PiNo,
		m.frame_no as PartNo,
		m.model_code as PartName,
		1 as Kuantum,
		m.distribution_price as HargaSatuan,
		(1 * m.distribution_price) as HargaBkp
		from tb_r_mvda_invoice m
	 where m.da_no = @ca_number
END

