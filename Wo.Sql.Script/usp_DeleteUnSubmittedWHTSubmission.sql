USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[usp_DeleteUpload]    Script Date: 9/27/2019 9:15:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		wo\dwi
-- Create date: 30 Oktober 2019
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[usp_DeleteUnSubmittedWHTSubmission] 
	-- Add the parameters for the stored procedure here
	@invoice_number varchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	delete from tb_r_bukti_potong
	WHERE invoice_number = @invoice_number and is_active = 0



END


