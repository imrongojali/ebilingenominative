UPDATE c 
SET c.customer_group = l.customer_group
FROM tb_m_customer_mapping c
INNER JOIN (SELECT
		*
	FROM (SELECT
			(SELECT DISTINCT
					t.customer_group
				FROM tb_m_customer_mapping t
				WHERE t.business_unit_id = 2
				AND r.customer_name = t.customer_name)
			AS customer_group
		   ,r.customer_code
		FROM tb_m_customer_mapping r
		WHERE r.business_unit_id = 3) t) l
	ON l.customer_code = c.customer_code
	AND c.business_unit_id = 3