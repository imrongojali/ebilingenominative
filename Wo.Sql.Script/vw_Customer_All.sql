USE [BES]
GO

/****** Object:  View [dbo].[vw_Customer_All]    Script Date: 11/11/2019 2:11:06 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER VIEW [dbo].[vw_Customer_All]
AS
SELECT DISTINCT a.dealer_code AS customer_code, a.dealer_name AS customer_name, 1 AS business_unit_id, ISNULL(b.customer_group, '') AS CUSTOMER_GROUP
FROM            [BES].[dbo].[tb_r_mvda_invoice] a LEFT JOIN tb_m_customer_mapping b on a.dealer_code = b.customer_code and b.business_unit_id = 1
UNION
SELECT        a.customer_code AS customer_code, a.name AS customer_name, 2 AS business_unit_id, ISNULL(b.customer_group, '') AS CUSTOMER_GROUP
FROM            LS_COINS_TAM.TAM.dbo.tb_m_customer a LEFT JOIN tb_m_customer_mapping b on a.customer_code = b.customer_code and b.business_unit_id = 2

-- add sparepart
union
select		h.BILL_TO_CUST, h.CUST_NAME, 3 as business_unit_id, ISNULL(b.customer_group, '') AS CUSTOMER_GROUP from tb_r_spda_invoice_header h
				LEFT JOIN tb_m_customer_mapping b on h.BILL_TO_CUST = b.customer_code and b.business_unit_id = 3
GO


