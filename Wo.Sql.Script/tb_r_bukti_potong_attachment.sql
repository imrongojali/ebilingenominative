USE [BES]
GO

/****** Object:  Table [dbo].[tb_r_bukti_potong_attachment]    Script Date: 10/10/2019 10:20:53 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tb_r_bukti_potong_attachment](
	[attachment_bukti_potong_id] [bigint] IDENTITY(1,1) NOT NULL,
	[invoice_number] [varchar](20) NOT NULL,
	[filename] [nvarchar](255) NULL,
	[server_filename] [nvarchar](max) NULL,
	[created_date] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[modified_date] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[row_status] [smallint] NULL,
	[status] [nvarchar](50) NULL,
 CONSTRAINT [PK_tb_r_bukti_potong_attachment] PRIMARY KEY CLUSTERED 
(
	[attachment_bukti_potong_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[tb_r_bukti_potong_attachment]  WITH CHECK ADD  CONSTRAINT [FK_tb_r_bukti_potong_attachment_tb_r_bukti_potong] FOREIGN KEY([invoice_number])
REFERENCES [dbo].[tb_r_bukti_potong] ([invoice_number])
GO

ALTER TABLE [dbo].[tb_r_bukti_potong_attachment] CHECK CONSTRAINT [FK_tb_r_bukti_potong_attachment_tb_r_bukti_potong]
GO


