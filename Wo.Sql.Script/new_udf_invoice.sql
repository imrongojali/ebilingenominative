DECLARE @invoice_type smallint = 1, -- 0: all, 1: invoice, 2: nota retur
	@customer_group nvarchar(50) = '' -- if '' = internal TAM
select 
	'' as id,
	'Motor Vehicle' as business_unit,
	1 as business_unit_id,
	a.invoice_number,
	'SALES MV' AS transaction_type,
	0 AS transaction_type_id,
	a.doc_date,
	a.due_date,
	'IDR' AS currency,
	a.dpp,
	a.tax,
	a.total_amount,
	a.ar_status,
	a.tax_invoice,
	B.pph_serial_no AS pph_no,
	'' as da_code,
	a.da_no as da_no,
	m.customer_code,
	m.customer_name,
	m.customer_group,
	'1.00' as exc_rate,
	B.amount_pph as amount_pph,
	a.inv_downloaded,
	--ef.ApprovalStatus as approvalstatus,
	a.notur_uploaded,
	a.notur_downloaded,			
	a.downloaded_date,
	a.downloaded_by,
	a.is_inv_available,
	a.inv_scan_downloaded,
	a.downloadedscan_date,
	a.downloadedscan_by,
	a.is_inv_scan_available,
	B.rate_pph as rate_pph,
	'' as vat_dormitory,
	'' as dormitory,
	null as pi_downloaded,
	null as pi_downloaded_date,
	null as pi_downloaded_by,
	a.address,a.dealer_code
 from 
(
	SELECT 
		A.[da_no] AS invoice_number,
		
		A.[do_date] AS doc_date,
		a.[due_date] AS due_date,
		
		sum (A.[distribution_price]) AS dpp,
		sum (A.[amount_vat]) AS tax,
		sum(A.[total_amount]) AS total_amount,
			
		CASE	WHEN a.journal_clearing is not null and a.journal_clearing != '' THEN 'InvStat01'
				WHEN due_date = CONVERT(DATE, GETDATE()) THEN 'InvStat04'
				WHEN due_date >= CONVERT(DATE, GETDATE()) THEN 'InvStat03'
				WHEN due_date <= CONVERT(DATE, GETDATE()) THEN 'InvStat02' /*  else 'Settlement'*/
					
		END AS ar_status,
		A.[tax_no] AS tax_invoice,
		
		A.da_no as da_no,
		
		A.inv_downloaded as inv_downloaded,
		--ef.ApprovalStatus as approvalstatus,
		A.notur_uploaded as notur_uploaded,
		A.notur_downloaded as notur_downloaded,			
		A.downloaded_date as downloaded_date,
		A.downloaded_by as downloaded_by,
		CASE	WHEN A.inv_downloaded is null THEN 0
		ELSE 1 END AS is_inv_available,
		A.inv_scan_downloaded as inv_scan_downloaded,
		A.downloadedscan_date as downloadedscan_date,
		A.downloadedscan_by as downloadedscan_by,
		CASE	WHEN A.inv_scan_downloaded is null THEN 0
		ELSE 1 END AS is_inv_scan_available,
		
		null as pi_downloaded,
		null as pi_downloaded_date,
		null as pi_downloaded_by,
		A.address,
		a.dealer_code
	FROM [BES].[dbo].[tb_r_mvda_invoice] A
	where A.do_date >= '2019-02-01' and 
	(
		-- seperti request dari mas Sabid, 
		--Di menu Invoice, data yang ditampilkan adalah data DA dan CA berlaku untuk other, MV, SP
		(@invoice_type = 1 and (A.da_no != '%c%' or A.da_no like '%c%'))	-- invoice
		or  
		(@invoice_type = 2 and A.da_no like '%c%' )	-- nota retur
		or
		(@invoice_type = 3 and  a.[journal_clearing] is not null and a.[journal_clearing] != '')
		or
		(@invoice_type = 0 and 1=1)
	)
	group by A.da_no, A.due_date, A.do_date,  A.journal_clearing, A.tax_no
	,A.inv_downloaded, A.notur_downloaded, A.notur_uploaded,
	A.notur_downloaded, A.downloaded_by, A.downloaded_date, A.inv_scan_downloaded, A.downloadedscan_date, A.downloadedscan_by
	,A.address,a.dealer_code
) a

-- master nya dobel2
left join (select distinct * from  dbo.tb_m_customer_mapping) m on m.customer_code = a.dealer_code and m.business_unit_id = 1

OUTER APPLY (
	select 
		pph.pph_serial_no
		, pph.amount_pph
		, pph.rate_pph
		from [dbo].[tb_r_mvda_pph] pph where A.da_no = pph.da_no
		group by pph.pph_serial_no
		, pph.amount_pph
		, pph.rate_pph
)B
--LEFT JOIN [dbo].[tb_r_mvda_pph] B ON A.da_no = B.da_no

--ef.ApprovalStatus != 'Reject'
where 1=1 and
(
		(@customer_group != '' and m.customer_group = @customer_group)
		or 
		(@customer_group = '' and a.dealer_code is not null)
)
--and a.invoice_number = '201912666'

