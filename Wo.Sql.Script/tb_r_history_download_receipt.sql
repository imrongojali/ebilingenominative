USE [BES]
GO

/****** Object:  Table [dbo].[tb_r_history_download_receipt]    Script Date: 10/21/2019 10:09:02 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tb_r_history_download_receipt](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[date_from] [datetime] NOT NULL,
	[date_to] [datetime] NOT NULL,
	[business_type] [varchar](max) NOT NULL,
	[customer_group] [varchar](max) NOT NULL,
	[created_date] [datetime] NOT NULL,
	[created_by] [varchar](50) NOT NULL,
	[modified_date] [datetime] NULL,
	[modified_by] [varchar](50) NULL,
	[row_status] [smallint] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


