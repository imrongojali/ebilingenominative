USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[usp_Print_DebitAdvice]    Script Date: 9/18/2019 2:47:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		wo\dwi
-- Create date: 2019-09-19
-- Description:	Print Attachment (Detail) Debit Advice
-- =============================================
ALTER PROCEDURE [dbo].[usp_Print_AttachmentSpDebitAdvice] 
	-- Add the parameters for the stored procedure here
	 @da_number varchar(20) ='' --FDA/SP/17D1387
	,@user varchar(50)='' -- 60000
	,@delivery_date varchar(10) ='' -- 2017-08-01
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select 
		h.DA_NO as DaNo,
		h.CUST_NAME as CustomerName,
		h.CUST_ADDR1 as CustomerAddress,
		d.PI_NO as InvoiceNo,
		d.PI_DATE as InvoiceDate,
		d.TOPAS_ORDER_TYPE as OT,
		d.CUST_ORDER_NO as OrderNo,
		sum(d.PICKING_QTY) as Items,
		sum(d.PICKING_QTY * d.RETAIL_PRICE) as GrossAmount,
		sum(d.TO_AMT) as NetAmount 
	from tb_r_spda_invoice_header h
		inner join tb_r_spda_invoice_detail d 
			on d.DA_NO = h.DA_NO
			and d.PI_TYPE = h.DR_CR
			and CAST(d.PI_DATE as DATE) = CAST(h.DLV_DATE as DATE)
	where h.DR_CR = 'D' --debit advice
		and (nullif(@da_number, '') is null or h.DA_NO = @da_number)
		and (nullif(@user, '') is null or h.BILL_TO_CUST = @user)
		and (nullif(@delivery_date, '') is null or h.DLV_DATE = @delivery_date)
	group by h.DA_NO, h.CUST_NAME, h.CUST_ADDR1 , d.PI_NO, d.PI_DATE, d.CUST_ORDER_NO, d.TOPAS_ORDER_TYPE
END

