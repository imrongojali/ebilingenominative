USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[usp_UploadNotaRetur]    Script Date: 9/26/2019 4:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
ALTER PROCEDURE [dbo].[usp_UploadSpCaRetur]
	@filename nvarchar(255)
	,@reference char(14)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
    
	-- Insert statements for procedure here
	INSERT INTO [tb_r_attachment_spca_retur]
	(	
	   [filename]
	   ,[reference]
	   ,[created_date]
       ,[created_by])	
	VALUES
	(	
	   @filename
	   ,@reference
	   ,GETDATE()
	   ,'SYSTEM')
END


