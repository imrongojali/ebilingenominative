USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[usp_Print_DebitAdvice]    Script Date: 9/18/2019 2:47:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		wo\dwi
-- Create date: 2019-10-02
-- Description:	Print Attachment (Detail) Credit Advice
-- =============================================
ALTER PROCEDURE [dbo].[usp_Print_AttachmentSpCreditAdvice] 
	-- Add the parameters for the stored procedure here
	 @ca_number varchar(20) ='' --FCA/SP/17C0313
	,@user varchar(50)='' -- 60000
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select 
		h.CA_NO as CaNo,
		h.DA_NO as DaNo,
		h.CUST_NAME as CustomerName,
		h.CUST_ADDR1 as CustomerAddress,
		d.PI_NO as InvoiceNo,
		d.PI_DATE as InvoiceDate,
		--sum(cast(case when isnumeric(d.TOPAS_ORDER_TYPE) = 1 then d.TOPAS_ORDER_TYPE else 0 end as int)) as OT,
		d.TOPAS_ORDER_TYPE as OT,
		d.CUST_ORDER_NO as OrderNo,
		sum(d.PICKING_QTY) as Items,
		sum(d.PICKING_QTY * d.RETAIL_PRICE) as GrossAmount,
		sum(d.TO_AMT) as NetAmount 
	from tb_r_spda_invoice_header h
		inner join tb_r_spda_invoice_detail d 
			on d.DA_NO = h.DA_NO
			and h.CA_NO = d.CA_NO
			and d.PI_TYPE = h.DR_CR
	where h.DR_CR = 'C' --debit advice
		and (nullif(@ca_number, '') is null or h.CA_NO = @ca_number)
		and (nullif(@user, '') is null or h.BILL_TO_CUST = @user)
	group by h.CA_NO,h.DA_NO, d.PI_DATE, h.CUST_NAME, h.CUST_ADDR1 , d.PI_NO, d.DELIVERY_DATE, d.CUST_ORDER_NO, d.TOPAS_ORDER_TYPE
END

