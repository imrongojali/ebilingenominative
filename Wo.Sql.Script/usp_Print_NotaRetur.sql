USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[usp_Print_NotaRetur]    Script Date: 11/29/2019 5:07:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[usp_Print_NotaRetur]
	--@tax_number varchar(100)
	@da_number varchar(20)='DAR/2018/02595',
	@user varchar(50)='TAM\Arifudin'
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @position varchar(max) = ''
	       ,@name varchar(max) = ''
		   ,@transaction_name varchar(max)
		   ,@total_Dormitory money
		   ,@total_vatDormitory money
		   ,@total_Vat money
		   ,@total_sales_vat money

	--DECLARE @da_number varchar(20) = ''
	--SELECT @da_number = da_number FROM tb_r_da_detil where tax_number = @tax_number;

	-- TIDAK MEMBUTUHKAN DATA USER
	--SELECT @position = [tb_m_parameter].[description]
	--      ,@name = [tb_m_parameter].[value1]
	--FROM [tb_m_parameter]
	--WHERE CONVERT(VARCHAR(max), [tb_m_parameter].value2) = @user

	SELECT @total_Dormitory = SUM([tb_r_da_detil_item].[dormitory])
		  ,@total_vatDormitory = SUM([tb_r_da_detil_item].[vat_dormitory])
		  ,@total_Vat = SUM([tb_r_da_detil_item].[vat_local])
	FROM [tb_r_da_detil]
	INNER JOIN [tb_r_da_detil_item] ON [tb_r_da_detil_item].[da_detil_code] = [tb_r_da_detil].[da_detil_code]
	WHERE [tb_r_da_detil].[da_number] = @da_number
		  AND [tb_r_da_detil].[row_status] = 0
		  AND [tb_r_da_detil_item].row_status = 0
	GROUP BY [tb_r_da_detil_item].[da_detil_code]

	SELECT @total_sales_vat = SUM([tb_r_da_detil_item].[price_subtotal_local])
	FROM [tb_r_da_detil]
	INNER JOIN [tb_r_da_detil_item] ON [tb_r_da_detil_item].[da_detil_code] = [tb_r_da_detil].[da_detil_code]
	WHERE [tb_r_da_detil].[da_number] = @da_number
		  AND [tb_r_da_detil].[row_status] = 0
		  AND [tb_r_da_detil_item].[vat_invoice] <> 0.00 
		  AND [tb_r_da_detil_item].row_status = 0
	GROUP BY [tb_r_da_detil_item].[da_detil_code]

	SELECT @transaction_name = 
		   CASE [tb_r_da].[transaction_text] 
		   WHEN '' THEN [tb_m_transaction_type].[transaction_name]
		   ELSE [tb_r_da].[transaction_text]
		   END
	FROM [tb_r_da_detil]
	LEFT JOIN [tb_r_da] ON [tb_r_da].[da_code] = [tb_r_da_detil].[da_code]
	LEFT JOIN [tb_m_transaction_type] ON [tb_m_transaction_type].[transaction_type_id] = [tb_r_da].[transaction_type_id]
	WHERE [tb_r_da_detil].[da_number] = @da_number
		  AND [tb_r_da_detil].[row_status] = 0

	SELECT
	-- START HEADER
	 [tb_r_da_detil].[retur_number] as ReturNumber
	,[tb_r_da_detil].[tax_number] as TaxNumber
	,[tb_r_da_detil].[postingdate] as PostingDate
	,[tb_r_da_detil].retur_date as CaDate
	,CASE WHEN [tb_r_da_detil].[customer_others_id] IS NULL THEN [tb_m_customer].[name]
		  ELSE [tb_r_customer_others].[name]
	 END as CustomerName
	,CASE WHEN [tb_r_da_detil].[customer_others_id] IS NULL THEN [tb_m_customer].[street]
		  ELSE [tb_r_customer_others].[street]
	 END as CustomerStreet
	,CASE WHEN [tb_r_da_detil].[customer_others_id] IS NULL THEN [tb_m_customer].[city]
		  ELSE [tb_r_customer_others].[city]
	 END as CustomerCity
	,CASE WHEN [tb_r_da_detil].[customer_others_id] IS NULL THEN [tb_m_customer].[npwp]
		  ELSE [tb_r_customer_others].[npwp]
	 END as CustomerNPWP
	,[tb_r_da].[is_dormintory] as IsDormitory
	-- END HEADER

	-- START BODY
	,@transaction_name as TransactionName
	,[tb_r_da_detil_item].[qty] as Qty
	,[tb_r_da_detil_item].[price_local] as UnitPrice
	,[tb_r_da_detil_item].[price_subtotal_local] as TotalPrice
	,@total_Dormitory as TotalDormitory
	,CASE WHEN @total_Dormitory <> 0.00 THEN 'DORMITORY'
		  ELSE '' 
	 END as Name_Dormitory
	,FLOOR(@total_sales_vat + @total_Dormitory) as TotalAmount
	,FLOOR(@total_Vat + @total_vatDormitory) as TotalVat
	-- END BODY

	FROM [tb_r_da_detil]
	LEFT JOIN [tb_r_da] ON [tb_r_da].[da_code] = [tb_r_da_detil].[da_code]
	LEFT JOIN [tb_r_da_detil_item] ON [tb_r_da_detil_item].[da_detil_code] = [tb_r_da_detil].[da_detil_code]
	LEFT JOIN [tb_m_transaction_type] ON [tb_m_transaction_type].[transaction_type_id] = [tb_r_da].[transaction_type_id]
	LEFT JOIN [LS_COINS_TAM].[TAM].[dbo].[tb_m_customer] [tb_m_customer] ON [tb_m_customer].[customer_code] = [tb_r_da_detil].[customer_code]
	LEFT JOIN [tb_r_customer_others] ON [tb_r_customer_others].[customer_others_id] = [tb_r_da_detil].[customer_others_id]

	WHERE [tb_r_da_detil].[da_number] = @da_number
		  AND [tb_r_da_detil].[row_status] = 0
		  AND [tb_r_da_detil_item].[vat_invoice] <> 0.00 
		  AND [tb_r_da_detil_item].row_status = 0
END
