USE [BES]
GO

/****** Object:  Table [dbo].[tb_r_attachment_notaretur]    Script Date: 9/26/2019 4:47:22 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tb_r_attachment_spca_retur](
	[attachment_spcaretur_id] [bigint] IDENTITY(1,1) NOT NULL,
	[filename] [nvarchar](255) NULL,
	[reference] [char](14) NULL,
	[created_date] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[modified_date] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[row_status] [smallint] NULL,
	[status] [nvarchar](50) NULL,
 CONSTRAINT [PK_tb_r_attachment_spca_retur] PRIMARY KEY CLUSTERED 
(
	[attachment_spcaretur_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


