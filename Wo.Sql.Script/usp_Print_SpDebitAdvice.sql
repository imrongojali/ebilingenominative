USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[usp_Print_DebitAdvice]    Script Date: 9/18/2019 2:47:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		wo\dwi
-- Create date: 2019-09-18
-- Description:	Print Debit Advice
-- =============================================
ALTER PROCEDURE [dbo].[usp_Print_SpDebitAdvice] 
	-- Add the parameters for the stored procedure here
	 @da_number varchar(20) =''
	,@user varchar(50)=''
	,@type int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select 
		h.DA_NO as DaNo,
		h.DLV_DATE as DeliveryDate,
		h.CUST_NAME as CustomerName,
		h.CUST_ADDR1 as CustomerAddress,
		h.FAKTUR_PAJAK_NO as FakturPajakNo,
		h.TO_AMT as TurnOverAmount,
		h.VAT_AMT as Vat10,
		h.TO_AMT + VAT_AMT as Total,
		h.TO_DUE_DATE as DueDateTo,
		h.VAT_DUE_DATE as DueDateVat,
		h.COLLECT_DATE as CollectionDate
	from tb_r_spda_invoice_header h
	where DR_CR = 'D' --debit advice
		and (nullif(@da_number, '') is null or h.DA_NO = @da_number)
END

