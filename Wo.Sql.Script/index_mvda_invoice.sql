/*
Missing Index Details from SQLQuery8.sql - localhost\MSSQLSERVER2016.BES (sa)
The Query Processor estimates that implementing the following index could improve the query cost by 69.4837%.
*/


USE [BES]
GO
CREATE NONCLUSTERED INDEX groupbyDoDate
ON [dbo].[tb_r_mvda_invoice] ([do_date])
INCLUDE ([da_no],[dealer_code],[address],[due_date],[tax_no],[distribution_price],[amount_vat],[total_amount],[journal_clearing],[downloaded_date],[downloaded_by],[notur_uploaded],[notur_downloaded],[inv_downloaded],[inv_scan_downloaded],[downloadedscan_date],[downloadedscan_by])
GO

