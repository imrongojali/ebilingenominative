USE [BES]
GO

/****** Object:  Table [dbo].[tb_r_menu_role]    Script Date: 10/30/2019 2:15:15 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tb_r_menu_role](
	[internal_id] [int] IDENTITY(1,1) NOT NULL,
	[menu_id] [int] NOT NULL,
	[level_code] [char](2) NOT NULL,
	[created_date] [datetime] NOT NULL,
	[created_by] [varchar](50) NOT NULL,
	[modified_date] [datetime] NULL,
	[modified_by] [varchar](50) NULL,
	[row_status] [smallint] NULL,
 CONSTRAINT [PK_tb_r_menu_role] PRIMARY KEY CLUSTERED 
(
	[internal_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[tb_r_menu_role] ADD  CONSTRAINT [DF_tb_r_menu_role_created_date]  DEFAULT (getdate()) FOR [created_date]
GO

ALTER TABLE [dbo].[tb_r_menu_role] ADD  CONSTRAINT [DF_tb_r_menu_role_created_by]  DEFAULT ('') FOR [created_by]
GO

ALTER TABLE [dbo].[tb_r_menu_role] ADD  CONSTRAINT [DF_tb_r_menu_role_row_status]  DEFAULT ((0)) FOR [row_status]
GO

ALTER TABLE [dbo].[tb_r_menu_role]  WITH CHECK ADD  CONSTRAINT [FK_tb_r_menu_role_tb_m_level] FOREIGN KEY([level_code])
REFERENCES [dbo].[tb_m_level] ([level_code])
GO

ALTER TABLE [dbo].[tb_r_menu_role] CHECK CONSTRAINT [FK_tb_r_menu_role_tb_m_level]
GO

ALTER TABLE [dbo].[tb_r_menu_role]  WITH CHECK ADD  CONSTRAINT [FK_tb_r_menu_role_tb_m_menu] FOREIGN KEY([menu_id])
REFERENCES [dbo].[tb_m_menu] ([menu_id])
GO

ALTER TABLE [dbo].[tb_r_menu_role] CHECK CONSTRAINT [FK_tb_r_menu_role_tb_m_menu]
GO


