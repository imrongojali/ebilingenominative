-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		wo\dwi
-- Create date: 2019-10-11
-- Description:	save bukti potong
-- =============================================
ALTER PROCEDURE usp_SaveBuktiPotong
	-- Add the parameters for the stored procedure here
	@invoice_number varchar(20), 
	@materai_amount numeric(17,3),
	@amount_wht numeric(17,3),
	@tgl_payment datetime,
	@user varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    INSERT INTO [dbo].[tb_r_bukti_potong]
           ([invoice_number]
           ,[materai_amount]
           ,[amount_wht]
           ,[tgl_payment]
           ,[created_date]
           ,[created_by])
     VALUES
           (@invoice_number
           ,@materai_amount
           ,@amount_wht
           ,@tgl_payment
           ,getdate()
           ,@user)
END
GO
