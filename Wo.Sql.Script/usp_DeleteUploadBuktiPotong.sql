USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[usp_DeleteUpload]    Script Date: 9/27/2019 9:15:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		wo\dwi
-- Create date: 10 Oktober 2019
-- Description:	Delete Upload Spare Part Nota Retur
-- =============================================
ALTER PROCEDURE [dbo].[usp_DeleteUploadBuktiPotong] 
	-- Add the parameters for the stored procedure here
	@attachment_bukti_potong_id bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @uploadId varchar(50);
	select @uploadId = upload_id from tb_r_bukti_potong_attachment
	WHERE attachment_bukti_potong_id = @attachment_bukti_potong_id

    -- Insert statements for procedure here
	delete tb_r_bukti_potong_attachment
	WHERE upload_id = @uploadId



END


