USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateNoturUploaded]    Script Date: 10/4/2019 4:15:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		wo\dwi
-- Create date: 10 Oktober 2019
-- Description:	Update Status bukti potong upload 
-- =============================================
ALTER PROCEDURE [dbo].[usp_UpdateBuktiPotongUploaded] 
	-- Add the parameters for the stored procedure here
	@invoiceNumber varchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- add by wo\dwi 20191004
	UPDATE tb_r_bukti_potong
	SET bukti_potong_uploaded = 1
	WHERE invoice_number = @invoiceNumber
END


