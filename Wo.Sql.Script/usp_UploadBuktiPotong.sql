USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[usp_UploadNotaRetur]    Script Date: 9/26/2019 4:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
ALTER PROCEDURE [dbo].[usp_UploadBuktiPotong]
	@filename nvarchar(255)
	,@server_filename nvarchar(max)
	,@invoiceNumber varchar(20)
	,@uploadId varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
    
	-- Insert statements for procedure here
	INSERT INTO [tb_r_bukti_potong_attachment]
	(	
		upload_id
	   ,invoice_number
	   ,[filename]
	   ,[server_filename]
	   ,[created_date]
       ,[created_by])	
	VALUES
	(	
		@uploadId
	   ,@invoiceNumber
	   ,@filename
	   ,@server_filename
	   ,GETDATE()
	   ,'SYSTEM')
END


