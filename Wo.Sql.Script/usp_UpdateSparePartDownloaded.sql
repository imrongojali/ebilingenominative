USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateDownloaded]    Script Date: 9/25/2019 10:45:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		wo\dwi
-- Create date: 25 September 2019
-- Description:	Update Spare Part Downloaded
-- =============================================
ALTER PROCEDURE [dbo].[usp_UpdateSparePartDownloaded] 
	-- Add the parameters for the stored procedure here
	@daNumber varchar(15),
	@user varchar(50),
	@downloadType varchar(1), -- 0 = Invoice QR(Debit Advice), 1 = PI, 2=NotaRetur
	@invoiceType varchar(1) -- 1: invoice, 2: nota retur
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@downloadType = 0)
	begin
		UPDATE tb_r_spda_invoice_header
		SET [INV_DOWNLOAD] = 1, [INV_DOWNLOAD_DATE] =GETDATE(), [INV_DOWNLOAD_BY] = @user
		WHERE da_no = @daNumber
			and DR_CR = case when @invoiceType = '1' then 'D' else 'C' end
	end
	else if(@downloadType = 1)
	begin
		UPDATE tb_r_spda_invoice_header
		SET [PI_DOWNLOAD] = 1, [PI_DOWNLOAD_DATE] =GETDATE(), [PI_DOWNLOAD_BY] = @user
		WHERE da_no = @daNumber
			and DR_CR = case when @invoiceType = '1' then 'D' else 'C' end
	end
	else if(@downloadType = 2)
	begin
		UPDATE tb_r_spda_invoice_header
		SET NOTUR_DOWNLOAD = 1, NOTUR_DOWNLOAD_DATE =GETDATE(), NOTUR_DOWNLOAD_BY = @user
		WHERE CA_NO = @daNumber
	end
END


