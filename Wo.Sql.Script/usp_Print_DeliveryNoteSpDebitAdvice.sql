USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[usp_Print_AttachmentSpDebitAdvice]    Script Date: 9/23/2019 1:38:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		wo\dwi
-- Create date: 2019-09-23
-- Description:	Print Delivery Note Spare Part Debit Advice
-- =============================================
ALTER PROCEDURE [dbo].[usp_Print_DeliveryNoteSpDebitAdvice] 
	-- Add the parameters for the stored procedure here
	 @da_number varchar(20) ='' --FDA/SP/17D1387
	,@user varchar(50)='' -- 60000
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select 
		h.BILL_TO_CUST as MainDealer,
		h.CUST_NAME as CustomerName,
		h.CUST_ADDR1 as CustomerAddress,
		d.CUST_CODE as CustomerCode,
		d.DELIVERY_DATE as DeliveryDate,
		d.CUST_ORDER_NO as OrderNo, 
		d.TOPAS_ORDER_TYPE as OrderType,
		d.REMAKS as Remarks,
		d.PI_NO as InvoiceNo,
		d.PI_DATE as InvoiceDate, 
		d.PART_NO as PartNumber,
		p.[PART_NAME] as PartName, -- TODO Dapet dari table apa??
		sum(d.PICKING_QTY) as Ordered,
		sum(d.PICKING_QTY) as Issued,
		sum(d.RETAIL_PRICE) as RetailPrice,
		sum(d.SALES_PRICE) as NetSalesPrice,
		d.DISC_RATE as DiscRate
	from tb_r_spda_invoice_header h
		inner join tb_r_spda_invoice_detail d 
			on d.DA_NO = h.DA_NO
			and d.PI_TYPE = h.DR_CR
			and CAST(d.PI_DATE as DATE) = CAST(h.DLV_DATE as DATE)
		left join [10.85.40.55].[DB_SP_INF].[dbo].[TB_I_H01_02_PNO] p on p.PNO = d.PART_NO
	where h.DR_CR = 'D' --debit advice
		--and d.PI_NO = 'EE6546'
		and (nullif(@da_number, '') is null or h.DA_NO = @da_number)
		--and (nullif(@user, '') is null or h.BILL_TO_CUST = @user)
	group by h.DA_NO, h.DLV_DATE, h.CUST_NAME, h.CUST_ADDR1 , d.CUST_CODE, d.TOPAS_ORDER_TYPE
		, d.PI_NO, d.DELIVERY_DATE, d.CUST_ORDER_NO, d.REMAKS, d.DISC_RATE, d.PART_NO, h.BILL_TO_CUST,p.[PART_NAME], d.PI_DATE
END

