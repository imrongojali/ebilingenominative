USE [BES]
GO
/****** Object:  StoredProcedure [dbo].[usp_syncSparePartHeader]    Script Date: 11/1/2019 10:10:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		wo\dwi
-- Create date: 2019-11-01
-- Description:	sync invoices spare part from gitopas
-- =============================================
ALTER PROCEDURE [dbo].[usp_syncSparePart] 
	-- Add the parameters for the stored procedure here
	@date date,
	@user varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @errMessage varchar(max)
			,@isValid BIT = 1
	DECLARE  @errMsg VARCHAR(MAX) = NULL
				,@errSeverity INT 
				,@errState INT

    CREATE TABLE #SP_INVOICE_HEADER
	(
		[CA_NO] [varchar](14)  NULL,
		[DA_NO] [varchar](14)  NULL,
		[DR_CR] [varchar](1)  NULL,
		[DLV_DATE] [datetime]  NULL,
		[BILL_TO_CUST] [varchar](5)  NULL,
		[CUST_NAME] [varchar](40)  NULL,
		[CUST_ADDR1] [varchar](50)  NULL,
		[CUST_ADDR2] [varchar](50)  NULL,
		[NPWP] [varchar](30)  NULL,
		[TO_AMT] [numeric](11, 0)  NULL,
		[VAT_AMT] [numeric](11, 0)  NULL,
		[PEN_AMT] [numeric](11, 0)  NULL,
		[PPH23_AMT] [numeric](11, 0)  NULL,
		[PEN_DUE_DATE] [datetime]  NULL,
		[TO_DUE_DATE] [datetime]  NULL,
		[COLLECT_DATE] [datetime]  NULL,
		[FAKTUR_PAJAK_NO] [varchar](30) NULL,
		[VAT_DUE_DATE] [datetime] NULL,
		[DA_SIGN_NAME] [varchar](15) NULL,
		[PPH23_DUE_DATE] [datetime] NULL,
		[TAX_SIGN_NAME] [varchar](15) NULL,
		[VAT_RATE] [numeric](5, 2) NULL,
		[PEN_RATE] [numeric](5, 2) NULL,
		[PPH23_RATE] [numeric](5, 2) NULL,
		[PRINT_DATE] [datetime] NULL,
		[PRINT_TIME] [datetime] NULL,
		[FLAG_EFAKTUR] [varchar](1) NULL,
		[FLAG_DF] [varchar](1) NULL
	)

	CREATE TABLE #SP_INVOICE_DETAIL
	(
		[PI_NO] [varchar](6)  NULL,
		[PI_TYPE] [varchar](1)  NULL,
		[PI_DATE] [datetime]  NULL,
		[DELIVERY_DATE] [datetime] NULL,
		[ORDER_DATE] [datetime] NULL,
		[CUST_ORDER_NO] [varchar](8)  NULL,
		[ORDER_ITEM_NO] [varchar](4) NULL,
		[SHIP_PNO] [varchar](15) NULL,
		[PART_NO] [varchar](15)  NULL,
		[PICKING_QTY] [numeric](8, 0) NULL,
		[LANDED_COST] [numeric](17, 3) NULL,
		[RETAIL_PRICE] [numeric](17, 3) NULL,
		[SALES_PRICE] [numeric](17, 3) NULL,
		[TO_AMT] [numeric](17, 0) NULL,
		[DISC_RATE] [numeric](17, 3) NULL,
		[DISC_AMT] [numeric](17, 3) NULL,
		[DA_NO] [varchar](14) NULL,
		[ORI_DESTINATION_CODE] [varchar](7) NULL,
		[CUSTOMER_ORDER_NO] [varchar](8) NULL,
		[TOPAS_ORDER_TYPE] [varchar](1) NULL,
		[DELIVERY_NOTE_NO] [varchar](15) NULL,
		[DEST_CODE] [varchar](7) NULL,
		[DEST_GROUP_CODE] [varchar](7) NULL,
		[CUST_CODE] [varchar](7) NULL,
		[BILL_TO_CUST] [varchar](5) NULL,
		[CUST_GROUP_TYPE] [varchar](1) NULL,
		[VAT_RATE] [numeric](5, 2) NULL,
		[VAT_AMOUNT] [numeric](17, 2) NULL,
		[CREATED_DATE] [datetime] NULL,
		[CHANGED_DATE] [datetime] NULL,
		[PI_FLAG] [varchar](1) NULL,
		[PAYMENT_CODE] [varchar](2) NULL,
		[DEPOT_CODE] [varchar](2) NULL,
		[REMAKS] [varchar](30) NULL,
		[TRIP_REFF_NO] [varchar](10) NULL,
		[SHIP_CASE_NO] [varchar](8)  NULL,
		[SHIP_DOC_NO] [varchar](7) NULL,
		[CASE_OUTER_LENGTH] [numeric](5, 0) NULL,
		[CASE_OUTER_WIDTH] [numeric](5, 0) NULL,
		[CASE_OUTER_GROSS_HEIGHT] [numeric](5, 0) NULL,
		[CASE_GROSS_WEIGHT] [numeric](8, 2) NULL,
		[VOLUME_WEIGHT] [numeric](8, 2) NULL,
		[MAX_VOLUME_WEIGHT] [numeric](8, 2) NULL,
		[MAKER_CODE] [varchar](1) NULL,
		[TRUCK_NO] [varchar](4) NULL,
		[SHIP_AGENT] [varchar](5) NULL,
		[SHIP_AGENT_NAME] [varchar](20) NULL,
		[ROUTE_NAME] [varchar](30) NULL,
		[UOM] [varchar](5) NULL,
		[PRICE_COST] [numeric](15, 2) NULL,
		[PENALTY_RATE] [numeric](5, 2) NULL,
		[PENALTY_AMOUNT] [numeric](17, 2) NULL,
		[PPH23_RATE] [numeric](5, 2) NULL,
		[PPH23_AMOUNT] [numeric](17, 2) NULL,
		[CA_NO] [varchar](14) NULL,
		[PI_PROCESS_FLAG] [varchar](50) NULL,
		[CURRENCY_CODE] [varchar](3) NULL,
		[DANGER_GOOD_CLASS] [varchar](4) NULL,
		[CASE_PACKAGE_TYPE] [varchar](30) NULL,
		[HS_NO] [varchar](15) NULL,
		[COUNTRY_OF_ORIGIN_CODE] [varchar](3) NULL,
		[TRANSPORT_CODE] [varchar](1) NULL,
		[CONTAINER_NO] [varchar](12) NULL,
		[CONTAINER_TYPE] [varchar](1) NULL,
		[CASE_NET_WEIGHT] [varchar](8) NULL,
		[CASE_MEASUREMENT] [varchar](7) NULL,
		[VOLUME] [numeric](8, 2) NULL,
		[DOMESTIC_OVERSEAS_CODE] [char](1) NULL,
		[LC_AMOUNT] [numeric](18, 0) NULL,
		[SEAL_NO] [varchar](10) NULL
	)

	if (select count(1) from DBO.tb_t_spda_invoice_header
		where cast([DLV_DATE] as DATE) = COALESCE(NULLIF(@date,''), cast(dateadd(day,datediff(day,1,GETDATE()),0) as date))) > 0
	begin

		BEGIN TRANSACTION
		BEGIN TRY
		-- sync header
		INSERT INTO #SP_INVOICE_HEADER
		SELECT [CA_NO]
			,[DA_NO]
			,[DR_CR]
			,[DLV_DATE]
			,[BILL_TO_CUST]
			,[CUST_NAME]
			,[CUST_ADDR1]
			,[CUST_ADDR2]
			,[NPWP]
			,[TO_AMT]
			,[VAT_AMT]
			,[PEN_AMT]
			,[PPH23_AMT]
			,[PEN_DUE_DATE]
			,[TO_DUE_DATE]
			,[COLLECT_DATE]
			,[FAKTUR_PAJAK_NO]
			,[VAT_DUE_DATE]
			,[DA_SIGN_NAME]
			,[PPH23_DUE_DATE]
			,[TAX_SIGN_NAME]
			,[VAT_RATE]
			,[PEN_RATE]
			,[PPH23_RATE]
			,[PRINT_DATE]
			,[PRINT_TIME]
			,[FLAG_EFAKTUR]
			,[FLAG_DF]
		FROM DBO.tb_t_spda_invoice_header
		where cast([DLV_DATE] as DATE) = COALESCE(NULLIF(@date,''), cast(dateadd(day,datediff(day,1,GETDATE()),0) as date))

	

			MERGE tb_r_spda_invoice_header T
			USING #SP_INVOICE_HEADER S
			ON T.DA_NO = S.DA_NO AND T.CA_NO = S.CA_NO
			WHEN MATCHED
			THEN UPDATE set 
				T.[CA_NO] = S.CA_NO
				,T.[DR_CR]= S.DR_CR
				,T.[DLV_DATE]= S.DLV_DATE
				,T.[BILL_TO_CUST]= S.BILL_TO_CUST
				,T.[CUST_NAME]= S.CUST_NAME
				,T.[CUST_ADDR1]= S.CUST_ADDR1
				,T.[CUST_ADDR2]= S.CUST_ADDR2
				,T.[NPWP]= S.NPWP
				,T.[TO_AMT]= S.TO_AMT
				,T.[VAT_AMT]= S.VAT_AMT
				,T.[PEN_AMT]= S.PEN_AMT
				,T.[PPH23_AMT]= S.PPH23_AMT
				,T.[PEN_DUE_DATE]= S.PEN_DUE_DATE
				,T.[TO_DUE_DATE]= S.TO_DUE_DATE
				,T.[COLLECT_DATE]= S.COLLECT_DATE
				,T.[FAKTUR_PAJAK_NO]= S.FAKTUR_PAJAK_NO
				,T.[VAT_DUE_DATE]= S.VAT_DUE_DATE
				,T.[DA_SIGN_NAME]= S.DA_SIGN_NAME
				,T.[PPH23_DUE_DATE]= S.PPH23_DUE_DATE
				,T.[TAX_SIGN_NAME]= S.TAX_SIGN_NAME
				,T.[VAT_RATE]= S.VAT_RATE
				,T.[PEN_RATE]= S.PEN_RATE
				,T.[PPH23_RATE]= S.PPH23_RATE
				,T.[PRINT_DATE]= S.PRINT_DATE
				,T.[PRINT_TIME]= S.PRINT_TIME
				,T.[FLAG_EFAKTUR]= S.FLAG_EFAKTUR
				,T.[FLAG_DF]= S.FLAG_DF
				,T.MODIFIED_DATE = GETDATE()
				,T.MODIFIED_BY = @user
			WHEN NOT MATCHED BY TARGET
			THEN INSERT ([CA_NO]
						,[DA_NO]
						,[DR_CR]
						,[DLV_DATE]
						,[BILL_TO_CUST]
						,[CUST_NAME]
						,[CUST_ADDR1]
						,[CUST_ADDR2]
						,[NPWP]
						,[TO_AMT]
						,[VAT_AMT]
						,[PEN_AMT]
						,[PPH23_AMT]
						,[PEN_DUE_DATE]
						,[TO_DUE_DATE]
						,[COLLECT_DATE]
						,[FAKTUR_PAJAK_NO]
						,[VAT_DUE_DATE]
						,[DA_SIGN_NAME]
						,[PPH23_DUE_DATE]
						,[TAX_SIGN_NAME]
						,[VAT_RATE]
						,[PEN_RATE]
						,[PPH23_RATE]
						,[PRINT_DATE]
						,[PRINT_TIME]
						,[FLAG_EFAKTUR]
						,[FLAG_DF]
						,[CREATED_DATE]
						,[CREATED_BY])
				VALUES
						(S.CA_NO
						,S.DA_NO
						,S.DR_CR
						,S.DLV_DATE
						,S.BILL_TO_CUST
						,S.CUST_NAME
						,S.CUST_ADDR1
						,S.CUST_ADDR2
						,S.NPWP
						,S.TO_AMT
						,S.VAT_AMT
						,S.PEN_AMT
						,S.PPH23_AMT
						,S.PEN_DUE_DATE
						,S.TO_DUE_DATE
						,S.COLLECT_DATE
						,S.FAKTUR_PAJAK_NO
						,S.VAT_DUE_DATE
						,S.DA_SIGN_NAME
						,S.PPH23_DUE_DATE
						,S.TAX_SIGN_NAME
						,S.VAT_RATE
						,S.PEN_RATE
						,S.PPH23_RATE
						,S.PRINT_DATE
						,S.PRINT_TIME
						,S.FLAG_EFAKTUR
						,S.FLAG_DF
						,GETDATE()
						,@user
						);

			
				delete from tb_t_spda_invoice_header where cast([DLV_DATE] as DATE) = COALESCE(NULLIF(@date,''), cast(dateadd(day,datediff(day,1,GETDATE()),0) as date))
				

			COMMIT;
		END TRY
		BEGIN CATCH
			SET @isValid = 0;
			set @errMessage = ERROR_MESSAGE();
			
			ROLLBACK TRANSACTION;
		END CATCH

	END
	ELSE
	BEGIN
		SET @isValid = 0;
		set @errMessage = 'No data synced';
	END

	BEGIN TRANSACTION
	BEGIN TRY
		INSERT INTO [dbo].[tb_r_spda_invoice_sync]
				   ([sync_id]
				   ,[type_code]
				   ,[is_success]
				   ,[message_result]
				   ,[sync_date]
				   ,[data_count]
				   ,[created_date]
				   ,[created_by])
				VALUES
				   (NEWID()
				   ,'A01' -- ??? dont know the type code
				   ,@isValid
				   ,@errMessage
				   ,GETDATE()
				   ,(select count(1) from #SP_INVOICE_HEADER)
				   ,GETDATE()
				   ,@user)
		COMMIT;
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		
		SELECT  @errMsg = ERROR_MESSAGE() + ':'
						+ CAST(ERROR_LINE() AS VARCHAR(10)) + ':'
						+ ERROR_PROCEDURE() ,
				@errSeverity = ERROR_SEVERITY() ,
				@errState = ERROR_STATE();
		RAISERROR(@errMsg, @errSeverity, @errState);
	END CATCH

	if (select count(1) from DBO.tb_t_spda_invoice_detail
		where cast(PI_DATE as DATE) = COALESCE(NULLIF(@date,''), cast(dateadd(day,datediff(day,1,GETDATE()),0) as date))) > 0
	begin

		BEGIN TRANSACTION
		BEGIN TRY
		-- sync detail
						INSERT INTO #SP_INVOICE_DETAIL
						SELECT d.[PI_NO]
							  ,d.[PI_TYPE]
							  ,d.[PI_DATE]
							  ,d.[DELIVERY_DATE]
							  ,d.[ORDER_DATE]
							  ,d.[CUST_ORDER_NO]
							  ,d.[ORDER_ITEM_NO]
							  ,d.[SHIP_PNO]
							  ,d.[PART_NO]
							  ,d.[PICKING_QTY]
							  ,d.[LANDED_COST]
							  ,d.[RETAIL_PRICE]
							  ,d.[SALES_PRICE]
							  ,d.[TO_AMT]
							  ,d.[DISC_RATE]
							  ,d.[DISC_AMT]
							  ,d.[DA_NO]
							  ,d.[ORI_DESTINATION_CODE]
							  ,d.[CUSTOMER_ORDER_NO]
							  ,d.[TOPAS_ORDER_TYPE]
							  ,d.[DELIVERY_NOTE_NO]
							  ,d.[DEST_CODE]
							  ,d.[DEST_GROUP_CODE]
							  ,d.[CUST_CODE]
							  ,d.[BILL_TO_CUST]
							  ,d.[CUST_GROUP_TYPE]
							  ,d.[VAT_RATE]
							  ,d.[VAT_AMOUNT]
							  ,d.[CREATED_DATE]
							  ,d.[CHANGED_DATE]
							  ,d.[PI_FLAG]
							  ,d.[PAYMENT_CODE]
							  ,d.[DEPOT_CODE]
							  ,d.[REMAKS]
							  ,d.[TRIP_REFF_NO]
							  ,d.[SHIP_CASE_NO]
							  ,d.[SHIP_DOC_NO]
							  ,d.[CASE_OUTER_LENGTH]
							  ,d.[CASE_OUTER_WIDTH]
							  ,d.[CASE_OUTER_GROSS_HEIGHT]
							  ,d.[CASE_GROSS_WEIGHT]
							  ,d.[VOLUME_WEIGHT]
							  ,d.[MAX_VOLUME_WEIGHT]
							  ,d.[MAKER_CODE]
							  ,d.[TRUCK_NO]
							  ,d.[SHIP_AGENT]
							  ,d.[SHIP_AGENT_NAME]
							  ,d.[ROUTE_NAME]
							  ,d.[UOM]
							  ,d.[PRICE_COST]
							  ,d.[PENALTY_RATE]
							  ,d.[PENALTY_AMOUNT]
							  ,d.[PPH23_RATE]
							  ,d.[PPH23_AMOUNT]
							  ,d.[CA_NO]
							  ,d.[PI_PROCESS_FLAG]
							  ,d.[CURRENCY_CODE]
							  ,d.[DANGER_GOOD_CLASS]
							  ,d.[CASE_PACKAGE_TYPE]
							  ,d.[HS_NO]
							  ,d.[COUNTRY_OF_ORIGIN_CODE]
							  ,d.[TRANSPORT_CODE]
							  ,d.[CONTAINER_NO]
							  ,d.[CONTAINER_TYPE]
							  ,d.[CASE_NET_WEIGHT]
							  ,d.[CASE_MEASUREMENT]
							  ,d.[VOLUME]
							  ,d.[DOMESTIC_OVERSEAS_CODE]
							  ,d.[LC_AMOUNT]
							  ,d.[SEAL_NO]
						  FROM tb_t_spda_invoice_detail d
						  where cast(d.PI_DATE as DATE) = COALESCE(NULLIF(@date,''), cast(dateadd(day,datediff(day,1,GETDATE()),0) as date))
			  
						  MERGE tb_r_spda_invoice_detail T
							USING #SP_INVOICE_DETAIL S
							ON T.PI_NO = S.PI_NO 
								AND T.PI_TYPE = S.PI_TYPE
								AND T.PI_DATE = S.PI_DATE
								AND T.CUST_ORDER_NO = S.CUST_ORDER_NO
								AND T.PART_NO = S.PART_NO
								AND T.SHIP_CASE_NO = S.SHIP_CASE_NO
							WHEN MATCHED
							THEN UPDATE set 
							   T.[DELIVERY_DATE] = S.[DELIVERY_DATE]
							  ,T.[ORDER_DATE] = S.[ORDER_DATE]
							  ,T.[ORDER_ITEM_NO] = S.[ORDER_ITEM_NO]
							  ,T.[SHIP_PNO] = S.[SHIP_PNO]
							  ,T.[PICKING_QTY] = S.[PICKING_QTY]
							  ,T.[LANDED_COST] = S.[LANDED_COST]
							  ,T.[RETAIL_PRICE] = S.[RETAIL_PRICE]
							  ,T.[SALES_PRICE] = S.[SALES_PRICE]
							  ,T.[TO_AMT] = S.[TO_AMT]
							  ,T.[DISC_RATE] = S.[DISC_RATE]
							  ,T.[DISC_AMT] = S.[DISC_AMT]
							  ,T.[DA_NO] = S.[DA_NO]
							  ,T.[ORI_DESTINATION_CODE] = S.[ORI_DESTINATION_CODE]
							  ,T.[CUSTOMER_ORDER_NO] = S.[CUSTOMER_ORDER_NO]
							  ,T.[TOPAS_ORDER_TYPE] = S.[TOPAS_ORDER_TYPE]
							  ,T.[DELIVERY_NOTE_NO] = S.[DELIVERY_NOTE_NO]
							  ,T.[DEST_CODE] = S.[DEST_CODE]
							  ,T.[DEST_GROUP_CODE] = S.[DEST_GROUP_CODE]
							  ,T.[CUST_CODE] = S.[CUST_CODE]
							  ,T.[BILL_TO_CUST] = S.[BILL_TO_CUST]
							  ,T.[CUST_GROUP_TYPE] = S.[CUST_GROUP_TYPE]
							  ,T.[VAT_RATE] = S.[VAT_RATE]
							  ,T.[VAT_AMOUNT] = S.[VAT_AMOUNT]
							  ,T.[CREATED_DATE] = S.[CREATED_DATE]
							  ,T.[CHANGED_DATE] = S.[CHANGED_DATE]
							  ,T.[PI_FLAG] = S.[PI_FLAG]
							  ,T.[PAYMENT_CODE] = S.[PAYMENT_CODE]
							  ,T.[DEPOT_CODE] = S.[DEPOT_CODE]
							  ,T.[REMAKS] = S.[REMAKS]
							  ,T.[TRIP_REFF_NO] = S.[TRIP_REFF_NO]
							  ,T.[SHIP_DOC_NO] = S.[SHIP_DOC_NO]
							  ,T.[CASE_OUTER_LENGTH] = S.[CASE_OUTER_LENGTH]
							  ,T.[CASE_OUTER_WIDTH] = S.[CASE_OUTER_WIDTH]
							  ,T.[CASE_OUTER_GROSS_HEIGHT] = S.[CASE_OUTER_GROSS_HEIGHT]
							  ,T.[CASE_GROSS_WEIGHT] = S.[CASE_GROSS_WEIGHT]
							  ,T.[VOLUME_WEIGHT] = S.[VOLUME_WEIGHT]
							  ,T.[MAX_VOLUME_WEIGHT] = S.[MAX_VOLUME_WEIGHT]
							  ,T.[MAKER_CODE] = S.[MAKER_CODE]
							  ,T.[TRUCK_NO] = S.[TRUCK_NO]
							  ,T.[SHIP_AGENT] = S.[SHIP_AGENT]
							  ,T.[SHIP_AGENT_NAME] = S.[SHIP_AGENT_NAME]
							  ,T.[ROUTE_NAME] = S.[ROUTE_NAME]
							  ,T.[UOM] = S.[UOM]
							  ,T.[PRICE_COST] = S.[PRICE_COST]
							  ,T.[PENALTY_RATE] = S.[PENALTY_RATE]
							  ,T.[PENALTY_AMOUNT] = S.[PENALTY_AMOUNT]
							  ,T.[PPH23_RATE] = S.[PPH23_RATE]
							  ,T.[PPH23_AMOUNT] = S.[PPH23_AMOUNT]
							  ,T.[CA_NO] = S.[CA_NO]
							  ,T.[PI_PROCESS_FLAG] = S.[PI_PROCESS_FLAG]
							  ,T.[CURRENCY_CODE] = S.[CURRENCY_CODE]
							  ,T.[DANGER_GOOD_CLASS] = S.[DANGER_GOOD_CLASS]
							  ,T.[CASE_PACKAGE_TYPE] = S.[CASE_PACKAGE_TYPE]
							  ,T.[HS_NO] = S.[HS_NO]
							  ,T.[COUNTRY_OF_ORIGIN_CODE] = S.[COUNTRY_OF_ORIGIN_CODE]
							  ,T.[TRANSPORT_CODE] = S.[TRANSPORT_CODE]
							  ,T.[CONTAINER_NO] = S.[CONTAINER_NO]
							  ,T.[CONTAINER_TYPE] = S.[CONTAINER_TYPE]
							  ,T.[CASE_NET_WEIGHT] = S.[CASE_NET_WEIGHT]
							  ,T.[CASE_MEASUREMENT] = S.[CASE_MEASUREMENT]
							  ,T.[VOLUME] = S.[VOLUME]
							  ,T.[DOMESTIC_OVERSEAS_CODE] = S.[DOMESTIC_OVERSEAS_CODE]
							  ,T.[LC_AMOUNT] = S.[LC_AMOUNT]
							  ,T.[SEAL_NO] = S.[SEAL_NO]
							  ,T.SYNC_MODIFIED_DATE = GETDATE()
							  ,T.SYNC_MODIFIED_BY = @user
							WHEN NOT MATCHED BY TARGET
							THEN INSERT ([PI_NO]
									   ,[PI_TYPE]
									   ,[PI_DATE]
									   ,[DELIVERY_DATE]
									   ,[ORDER_DATE]
									   ,[CUST_ORDER_NO]
									   ,[ORDER_ITEM_NO]
									   ,[SHIP_PNO]
									   ,[PART_NO]
									   ,[PICKING_QTY]
									   ,[LANDED_COST]
									   ,[RETAIL_PRICE]
									   ,[SALES_PRICE]
									   ,[TO_AMT]
									   ,[DISC_RATE]
									   ,[DISC_AMT]
									   ,[DA_NO]
									   ,[ORI_DESTINATION_CODE]
									   ,[CUSTOMER_ORDER_NO]
									   ,[TOPAS_ORDER_TYPE]
									   ,[DELIVERY_NOTE_NO]
									   ,[DEST_CODE]
									   ,[DEST_GROUP_CODE]
									   ,[CUST_CODE]
									   ,[BILL_TO_CUST]
									   ,[CUST_GROUP_TYPE]
									   ,[VAT_RATE]
									   ,[VAT_AMOUNT]
									   ,[CREATED_DATE]
									   ,[CHANGED_DATE]
									   ,[PI_FLAG]
									   ,[PAYMENT_CODE]
									   ,[DEPOT_CODE]
									   ,[REMAKS]
									   ,[TRIP_REFF_NO]
									   ,[SHIP_CASE_NO]
									   ,[SHIP_DOC_NO]
									   ,[CASE_OUTER_LENGTH]
									   ,[CASE_OUTER_WIDTH]
									   ,[CASE_OUTER_GROSS_HEIGHT]
									   ,[CASE_GROSS_WEIGHT]
									   ,[VOLUME_WEIGHT]
									   ,[MAX_VOLUME_WEIGHT]
									   ,[MAKER_CODE]
									   ,[TRUCK_NO]
									   ,[SHIP_AGENT]
									   ,[SHIP_AGENT_NAME]
									   ,[ROUTE_NAME]
									   ,[UOM]
									   ,[PRICE_COST]
									   ,[PENALTY_RATE]
									   ,[PENALTY_AMOUNT]
									   ,[PPH23_RATE]
									   ,[PPH23_AMOUNT]
									   ,[CA_NO]
									   ,[PI_PROCESS_FLAG]
									   ,[CURRENCY_CODE]
									   ,[DANGER_GOOD_CLASS]
									   ,[CASE_PACKAGE_TYPE]
									   ,[HS_NO]
									   ,[COUNTRY_OF_ORIGIN_CODE]
									   ,[TRANSPORT_CODE]
									   ,[CONTAINER_NO]
									   ,[CONTAINER_TYPE]
									   ,[CASE_NET_WEIGHT]
									   ,[CASE_MEASUREMENT]
									   ,[VOLUME]
									   ,[DOMESTIC_OVERSEAS_CODE]
									   ,[LC_AMOUNT]
									   ,[SEAL_NO]
									   ,[SYNC_CREATED_DATE]
									   ,[SYNC_CREATED_BY])
								 VALUES
									   (S.[PI_NO]
									   ,S.[PI_TYPE]
									   ,S.[PI_DATE]
									   ,S.[DELIVERY_DATE]
									   ,S.[ORDER_DATE]
									   ,S.[CUST_ORDER_NO]
									   ,S.[ORDER_ITEM_NO]
									   ,S.[SHIP_PNO]
									   ,S.[PART_NO]
									   ,S.[PICKING_QTY]
									   ,S.[LANDED_COST]
									   ,S.[RETAIL_PRICE]
									   ,S.[SALES_PRICE]
									   ,S.[TO_AMT]
									   ,S.[DISC_RATE]
									   ,S.[DISC_AMT]
									   ,S.[DA_NO]
									   ,S.[ORI_DESTINATION_CODE]
									   ,S.[CUSTOMER_ORDER_NO]
									   ,S.[TOPAS_ORDER_TYPE]
									   ,S.[DELIVERY_NOTE_NO]
									   ,S.[DEST_CODE]
									   ,S.[DEST_GROUP_CODE]
									   ,S.[CUST_CODE]
									   ,S.[BILL_TO_CUST]
									   ,S.[CUST_GROUP_TYPE]
									   ,S.[VAT_RATE]
									   ,S.[VAT_AMOUNT]
									   ,S.[CREATED_DATE]
									   ,S.[CHANGED_DATE]
									   ,S.[PI_FLAG]
									   ,S.[PAYMENT_CODE]
									   ,S.[DEPOT_CODE]
									   ,S.[REMAKS]
									   ,S.[TRIP_REFF_NO]
									   ,S.[SHIP_CASE_NO]
									   ,S.[SHIP_DOC_NO]
									   ,S.[CASE_OUTER_LENGTH]
									   ,S.[CASE_OUTER_WIDTH]
									   ,S.[CASE_OUTER_GROSS_HEIGHT]
									   ,S.[CASE_GROSS_WEIGHT]
									   ,S.[VOLUME_WEIGHT]
									   ,S.[MAX_VOLUME_WEIGHT]
									   ,S.[MAKER_CODE]
									   ,S.[TRUCK_NO]
									   ,S.[SHIP_AGENT]
									   ,S.[SHIP_AGENT_NAME]
									   ,S.[ROUTE_NAME]
									   ,S.[UOM]
									   ,S.[PRICE_COST]
									   ,S.[PENALTY_RATE]
									   ,S.[PENALTY_AMOUNT]
									   ,S.[PPH23_RATE]
									   ,S.[PPH23_AMOUNT]
									   ,S.[CA_NO]
									   ,S.[PI_PROCESS_FLAG]
									   ,S.[CURRENCY_CODE]
									   ,S.[DANGER_GOOD_CLASS]
									   ,S.[CASE_PACKAGE_TYPE]
									   ,S.[HS_NO]
									   ,S.[COUNTRY_OF_ORIGIN_CODE]
									   ,S.[TRANSPORT_CODE]
									   ,S.[CONTAINER_NO]
									   ,S.[CONTAINER_TYPE]
									   ,S.[CASE_NET_WEIGHT]
									   ,S.[CASE_MEASUREMENT]
									   ,S.[VOLUME]
									   ,S.[DOMESTIC_OVERSEAS_CODE]
									   ,S.[LC_AMOUNT]
									   ,S.[SEAL_NO]
									   ,GETDATE()
									   ,@user);

			
				delete from tb_t_spda_invoice_detail where cast(PI_DATE as DATE) = COALESCE(NULLIF(@date,''), cast(dateadd(day,datediff(day,1,GETDATE()),0) as date))
				
			COMMIT;
		END TRY
		BEGIN CATCH
			SET @isValid = 0;
			set @errMessage = ERROR_MESSAGE();
			
			ROLLBACK TRANSACTION;
		END CATCH

	END
	ELSE
	BEGIN
		SET @isValid = 0;
		set @errMessage = 'No data synced';
	END

	BEGIN TRANSACTION
	BEGIN TRY
		INSERT INTO [dbo].[tb_r_spda_invoice_sync]
				   ([sync_id]
				   ,[type_code]
				   ,[is_success]
				   ,[message_result]
				   ,[sync_date]
				   ,[data_count]
				   ,[created_date]
				   ,[created_by])
				VALUES
				   (NEWID()
				   ,'A02' -- ??? dont know the type code
				   ,@isValid
				   ,@errMessage
				   ,GETDATE()
				   ,(select count(1) from #SP_INVOICE_DETAIL)
				   ,GETDATE()
				   ,@user)
		COMMIT;
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		SELECT  @errMsg = ERROR_MESSAGE() + ':'
						+ CAST(ERROR_LINE() AS VARCHAR(10)) + ':'
						+ ERROR_PROCEDURE() ,
				@errSeverity = ERROR_SEVERITY() ,
				@errState = ERROR_STATE();
		RAISERROR(@errMsg, @errSeverity, @errState);
	END CATCH

	
	DROP TABLE #SP_INVOICE_HEADER
	DROP TABLE #SP_INVOICE_DETAIL
END
