USE [BES]
GO

/****** Object:  Table [dbo].[tb_r_spda_invoice_header]    Script Date: 10/31/2019 6:07:06 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tb_t_spda_invoice_header](
	[CA_NO] [varchar](14) NOT NULL,
	[DA_NO] [varchar](14) NOT NULL,
	[DR_CR] [varchar](1) NOT NULL,
	[DLV_DATE] [datetime] NOT NULL,
	[BILL_TO_CUST] [varchar](5) NOT NULL,
	[CUST_NAME] [varchar](40) NOT NULL,
	[CUST_ADDR1] [varchar](50) NOT NULL,
	[CUST_ADDR2] [varchar](50) NOT NULL,
	[NPWP] [varchar](30) NOT NULL,
	[TO_AMT] [numeric](11, 0) NOT NULL,
	[VAT_AMT] [numeric](11, 0) NOT NULL,
	[PEN_AMT] [numeric](11, 0) NOT NULL,
	[PPH23_AMT] [numeric](11, 0) NOT NULL,
	[PEN_DUE_DATE] [datetime] NOT NULL,
	[TO_DUE_DATE] [datetime] NOT NULL,
	[COLLECT_DATE] [datetime] NOT NULL,
	[FAKTUR_PAJAK_NO] [varchar](30) NULL,
	[VAT_DUE_DATE] [datetime] NULL,
	[DA_SIGN_NAME] [varchar](15) NULL,
	[PPH23_DUE_DATE] [datetime] NULL,
	[TAX_SIGN_NAME] [varchar](15) NULL,
	[VAT_RATE] [numeric](5, 2) NULL,
	[PEN_RATE] [numeric](5, 2) NULL,
	[PPH23_RATE] [numeric](5, 2) NULL,
	[PRINT_DATE] [datetime] NULL,
	[PRINT_TIME] [datetime] NULL,
	[FLAG_EFAKTUR] [varchar](1) NULL,
	[FLAG_DF] [varchar](1) NULL
 CONSTRAINT [PK_TB_T_SPDA_INVOICE_HEADER] PRIMARY KEY CLUSTERED 
(
	[CA_NO] ASC,
	[DA_NO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


