USE [BES]
GO

/****** Object:  Table [dbo].[tb_r_history_summary_invoice]    Script Date: 10/7/2019 5:35:58 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tb_r_history_summary_invoice](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[date_from] [datetime] NOT NULL,
	[date_to] [datetime] NOT NULL,
	[business_type] [varchar](max) NOT NULL,
	[customer_group] [varchar](max) NOT NULL,
	[created_date] [datetime] NOT NULL,
	[created_by] [varchar](50) NOT NULL,
	[modified_date] [datetime] NULL,
	[modified_by] [varchar](50) NULL,
	[row_status] [smallint] NOT NULL,
 CONSTRAINT [PK__tb_r_his__3213E83FD29E24BF] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


