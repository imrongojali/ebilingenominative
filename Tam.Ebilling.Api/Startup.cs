﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Net.Http.Headers;
using Swashbuckle.Application;
using Ninject.Web.WebApi;
using Tam.Ebilling.Api.Utility;
using Ninject.Web.Common.OwinHost;
using Ninject.Web.WebApi.OwinHost;

[assembly: OwinStartup(typeof(Tam.Ebilling.Api.Startup))]

namespace Tam.Ebilling.Api
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=316888
            HttpConfiguration config = new HttpConfiguration();

            // Enable Cors
            var corsAttr = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(corsAttr);


            //config.IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.LocalOnly;

            // Web API routes
            config.MapHttpAttributeRoutes();

            // Enable Swagger
            config.EnableSwagger(c =>
            {
                c.SingleApiVersion("v1", "Agit API");
            })
            .EnableSwaggerUi(c =>
            {
                c.EnableApiKeySupport("X-ApiKey", "header");
            });

            config.Routes.MapHttpRoute(
            name: "swagger_root",
            routeTemplate: "",
            defaults: null,
            constraints: null,
            handler: new RedirectHandler((message => message.RequestUri.ToString()), "swagger"));

            // Set Dafault Formatter
            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional, action = "Index" }
            );

            try
            {
                config.Filters.Add(new ApiKeyAuthenticationFilter());
            }catch(Exception e)
            {
                throw e;
            }
            app.UseNinjectMiddleware(NinjectConfig.CreateKernel);
            app.UseNinjectWebApi(config);
        }
    }
}
