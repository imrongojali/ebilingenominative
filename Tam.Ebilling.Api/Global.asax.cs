﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;

namespace Tam.Ebilling.Api
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        private Bootstrapper _bootstrapper = new Bootstrapper();
        protected void Application_Start()
        {
            _bootstrapper.Setup();
            GlobalConfiguration.Configure(WebApiConfig.Register);
        }
    }
}
