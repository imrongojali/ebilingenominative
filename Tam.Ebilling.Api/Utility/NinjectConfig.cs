﻿using Ninject;
using Tam.Ebilling.Service;

namespace Tam.Ebilling.Api.Utility
{
    public static class NinjectConfig
    {
        public static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();

            //Create the bindings
            kernel.Bind<InvoiceService>().To<InvoiceService>();
            kernel.Bind<NoturService>().To<NoturService>();
            kernel.Bind<AttachmentService>().To<AttachmentService>();
            //GlobalConfiguration.Configuration.DependencyResolver = new NinjectDependencyResolver(kernel);

            return kernel;
        }
    }
}
