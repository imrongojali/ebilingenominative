﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http.Filters;
using System.Web.Http.Results;
using WebApi.AuthenticationFilter;

namespace Tam.Ebilling.Api.Utility
{
    public class ApiKeyAuthenticationFilter : AuthenticationFilterAttribute
    {
        public override void OnAuthentication(HttpAuthenticationContext context)
        {
            if (!Authenticate(context))
            {
                context.ErrorResult = new StatusCodeResult(HttpStatusCode.Unauthorized,
                    context.Request);
            }
        }

        private bool Authenticate(HttpAuthenticationContext context)
        {
            var apikey = context.Request?
                .Headers?
                .SingleOrDefault(x => x.Key == "X-ApiKey")
                .Value?
                .FirstOrDefault();

            if (string.IsNullOrWhiteSpace(apikey) || apikey != ConfigurationManager.AppSettings["api_key"]) return false;

            return true;
        }
    }

}