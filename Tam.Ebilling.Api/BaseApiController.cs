﻿using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Tam.Ebilling.Domain;

namespace Tam.Ebilling.Api
{
    public abstract class BaseApiController : ApiController
    {
        protected T Service<T>() where T : DbServiceBase
        {
            return ObjectFactory.GetInstance<T>();
        }
    }
}