﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using Tam.Ebilling.Infrastructure.Barcode;
using Tam.Ebilling.Service;
using Tam.Ebilling.Infrastructure.Helper;
using Tam.Ebilling.Reporting;
using System.Web;

namespace Tam.Ebilling.Api.Controllers
{
    [RoutePrefix("api/notaretur")]
    public class NotaReturController : BaseApiController
    {
        #region Get List of Nota Retur
        [HttpGet]
        [Route("getnotareturlist")]
        public IHttpActionResult GetNotaReturs(string customer_group, string invoice_number = null, int? business_unit_id = null, int? transaction_type_id = null, string date_from = null, string date_to = null, string ar_status = null, int? document_status = null)
        {
            try
            {
                return Ok(Service<NoturService>().GetNotaReturs(
                    customer_group,
                    invoice_number,
                    business_unit_id,
                    transaction_type_id,
                    date_from,
                    date_to,
                    ar_status,
                    document_status));
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
        #endregion   

        #region Get Single Invoice
        [HttpGet]
        [Route("getsinglenotaretur")]
        public IHttpActionResult GetSingleNotaRetur(string invoice_number, string customer_group)
        {
            try
            {
                return Ok(Service<NoturService>().GetNotaReturs(customer_group, invoice_number).ToList().FirstOrDefault());
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
        #endregion

        #region Tax Nota Retur Download
        [HttpGet]
        [Route("taxnotareturdownload")]
        public HttpResponseMessage TaxNotaReturDownload(string invoice_number)
        {
            HttpResponseMessage httpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);

            try
            {
                string linkdata = string.Empty;
                string filename = string.Empty;
                string ext = string.Empty;
                string mimeType = string.Empty;

                var yy = Service<NoturService>().GetDataPDF(invoice_number);

                foreach (var data in yy)
                {
                    linkdata = data.invoice_number;
                }

                if (linkdata == "")
                {
                    httpResponseMessage = Request.CreateResponse(HttpStatusCode.InternalServerError);

                    return httpResponseMessage;
                }
                else
                {
                    var username = Service<ParameterService>().GetParameter("file_impersonate_user").value1;
                    var domain = Service<ParameterService>().GetParameter("file_impersonate_domain").value1;
                    var userdomain = domain + "\\" + username;
                    var password = Service<ParameterService>().GetParameter("file_impersonate_password").value1;
                    var pers = new Impersonation();
                    pers.Impersonate(userdomain, password);

                    var credential = new NetworkCredential(userdomain, password);

                    using (new NetworkConnection(linkdata, credential))
                    {
                        FileStream fs = new FileStream(linkdata, FileMode.Open, FileAccess.Read);

                        filename = Path.GetFileName(linkdata);
                        mimeType = MimeMapping.GetMimeMapping(filename);

                        httpResponseMessage.Content = new StreamContent(fs);
                        httpResponseMessage.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                        httpResponseMessage.Content.Headers.ContentDisposition.FileName = filename;
                        httpResponseMessage.Content.Headers.ContentType = new MediaTypeHeaderValue(mimeType);
                        httpResponseMessage.Content.Headers.ContentLength = fs.Length;

                        return httpResponseMessage;
                    }
                }
            }
            catch (Exception e)
            {
                httpResponseMessage = Request.CreateResponse(HttpStatusCode.InternalServerError);
                return httpResponseMessage;
            }
        }
        #endregion

        #region Motor Vehicle and Others Invoice Nota Retur Download
        [HttpGet]
        [Route("invoicenotareturdownload")]
        public HttpResponseMessage NotaReturDownload(string da_number, string business_unit, string user)
        {
            Report report = new Report();
            HttpResponseMessage httpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            string mimeType, extension = string.Empty;

            try
            {
                if (!string.IsNullOrEmpty(da_number))// && !string.IsNullOrEmpty(user))
                {
                    if (business_unit.Equals("Others"))
                    {
                        var othersReport = report.GetDebitAdviceReport(da_number, business_unit, user, out extension, out mimeType);

                        httpResponseMessage.Content = new StreamContent(new MemoryStream(othersReport));
                        httpResponseMessage.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                        httpResponseMessage.Content.Headers.ContentDisposition.FileName = da_number.Replace("/", "-") + "." + extension;
                        httpResponseMessage.Content.Headers.ContentType = new MediaTypeHeaderValue(mimeType);
                        httpResponseMessage.Content.Headers.ContentLength = new MemoryStream(othersReport).Length;

                        //Service<InvoiceService>().GetUpdate(da_number);
                    }
                    else if (business_unit.Equals("Motor Vehicle"))
                    {
                        var mvReport = report.GetMvInvoiceReport(da_number, business_unit, user, out extension, out mimeType);

                        httpResponseMessage.Content = new StreamContent(new MemoryStream(mvReport));
                        httpResponseMessage.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                        httpResponseMessage.Content.Headers.ContentDisposition.FileName = da_number.Replace("/", "-") + "." + extension;
                        httpResponseMessage.Content.Headers.ContentType = new MediaTypeHeaderValue(mimeType);
                        httpResponseMessage.Content.Headers.ContentLength = new MemoryStream(mvReport).Length;

                        //var Update = Service<InvoiceService>().GetUpdate(da_number);
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            return httpResponseMessage;
        }
        #endregion

        #region Nota Retur Download
        [HttpGet]
        [Route("notareturdownload")]
        public HttpResponseMessage DownloadNotaRetur(string da_number, string user)
        {
            Report report = new Report();

            HttpResponseMessage httpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            string mimeType, extension = string.Empty;

            try
            {
                if (!string.IsNullOrEmpty(da_number))
                {
                    var notaReturReport = report.GetNotaRetur(da_number, user, out extension, out mimeType);

                    httpResponseMessage.Content = new StreamContent(new MemoryStream(notaReturReport));
                    httpResponseMessage.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                    httpResponseMessage.Content.Headers.ContentDisposition.FileName = da_number.Replace("/", "-") + "." + extension;
                    httpResponseMessage.Content.Headers.ContentType = new MediaTypeHeaderValue(mimeType);
                    httpResponseMessage.Content.Headers.ContentLength = new MemoryStream(notaReturReport).Length;

                    var Update = Service<NotaReturService>().GetUpdate(da_number);
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            return httpResponseMessage;
        }
        #endregion
    }
}
