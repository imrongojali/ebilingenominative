﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using Tam.Ebilling.Domain;
using Tam.Ebilling.Infrastructure.Session;
using Tam.Ebilling.Infrastructure.Barcode;
using Tam.Ebilling.Service;
using Tam.Ebilling.Infrastructure.Helper;
using System.IO.Compression;
using Tam.Ebilling.Infrastructure.Adapter;
using System.Web;

namespace Tam.Ebilling.Api.Controllers
{
    [RoutePrefix("api/attachment")]
    public class AttachmentController : BaseApiController
    {
        #region Attachment Download
        [HttpGet]
        [Route("attachmentdownload")]
        public HttpResponseMessage AttachmentDownload(string da_code)
        {
            var username = Service<ParameterService>().GetParameter("file_impersonate_user").value1;
            var domain = Service<ParameterService>().GetParameter("file_impersonate_domain").value1;
            var password = Service<ParameterService>().GetParameter("file_impersonate_password").value1;

            var userdomain = domain + "\\" + username;
            var credential = new NetworkCredential(userdomain, password);

            string fileserver = Service<ParameterService>().GetParameter("file_server").value1;
            string filepath = Service<ParameterService>().GetParameter("file_repo_path").value1;
            string file2 = "\\\\" + fileserver + "\\" + filepath;

            HttpResponseMessage httpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);

            try
            {
                using (new NetworkConnection(file2, credential))
                {
                    var doc = Service<AttachmentService>().GetAttachment(da_code);
                    var outputfile = da_code + ".zip";
                    var zipStream = new MemoryStream();
                    var item2 = doc.First();
                    var filename2 = item2.created_date.ToString("yyyyM") + "\\" + item2.reference + "\\" + item2.filename;
                    var url2 = Path.Combine(file2, filename2);

                    if (System.IO.File.Exists(url2) == true)
                    {
                        if (doc.Count > 1)
                        {
                            var zip = new ZipArchive(zipStream, ZipArchiveMode.Create);

                            foreach (var item in doc)
                            {
                                var filename = item.created_date.ToString("yyyyM") + "\\" + item.reference + "\\" + item.filename;
                                var url = Path.Combine(file2, filename);
                                zip.CreateEntryFromFile(url, item.filename);
                            }

                            zip.Dispose();
                        }
                        else
                        {
                            var item = doc.First();
                            var filename = item.created_date.ToString("yyyyM") + "\\" + item.reference + "\\" + item.filename;
                            var url = Path.Combine(file2, filename);
                            var file = new FileStream(url, FileMode.Open);
                            byte[] bytes = new byte[file.Length];
                            file.Read(bytes, 0, (int)file.Length);
                            zipStream.Write(bytes, 0, (int)file.Length);
                            outputfile = item.filename;
                        }

                        httpResponseMessage.Content = new StreamContent(zipStream);
                        httpResponseMessage.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                        httpResponseMessage.Content.Headers.ContentDisposition.FileName = outputfile;
                        httpResponseMessage.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                        httpResponseMessage.Content.Headers.ContentLength = zipStream.Length;
                    }
                    else if (System.IO.File.Exists(url2) == false)
                    {
                        throw new Exception("File not found");
                        //Response.Write(@"<script language='javascript'>alert('Download Attachment is Failed.')</script>");
                    }
                }
            }
            catch (Exception e)
            {
                //return InternalServerError(e);
            }

            return httpResponseMessage;
        }
        #endregion

        #region Get Attachment List
        [HttpGet]
        [Route("getattachmentlist")]
        public IHttpActionResult GetAttachmentList(string da_code)
        {
            try
            {
                return Ok(Service<AttachmentNotaReturService>().GetDocumentFileByReference(da_code));
            }
            catch(Exception e)
            {
                return InternalServerError(e);
            }
        }
        #endregion

        #region Save Attachment
        [HttpPost]
        [Route("saveattachment")]
        public IHttpActionResult SaveAttachment(string reference, List<AttachmentNotaRetur> data, string documenttype, string TypeDoc)
        {
            string message = string.Empty;

            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            try
            {
                var files = HttpContext.Current.Request.Files;

                var arrayObjects = new List<AttachmentNotaRetur>();
                var uploadService = Service<AttachmentNotaReturService>();

                if (files != null && files.Count > 0)
                {
                    for (var i = 0; i < files.Count; i++)
                    {
                        var file = files[i];

                        var doc = uploadService.Upload(new HttpPostedFileBaseAdapter(new HttpPostedFileWrapper(file)),reference,TypeDoc,out message);

                        arrayObjects.Add(doc);
                    }
                }

                var result = Service<AttachmentNotaReturService>().SaveUpload(reference, data, documenttype, out message);
                return Ok(new { result, message });
            }
            catch(Exception e)
            {
                return InternalServerError(e);
            }
        }
        #endregion
    }
}
