﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tam.Ebilling.Domain
{
    public class udf_GetParameter
    {
        public int parameter_id { get; set; }
        public string key_param { get; set; }
        public string description { get; set; }
        public string value1 { get; set; }
        public string value2 { get; set; }
        public DateTime created_date { get; set; }
    }

    public class udf_GetParameterNotif
	{
		public Guid Id { get; set; }
		public string parameter_code { get; set; }
		public string parameter_name { get; set; }
		public string parameter_value { get; set; }
		public string from_field_name { get; set; }
		public string parameter_category_name { get; set; }
		public string color { get; set; }
		public string color_status { get; set; }
		public string icon { get; set; }
		public string note_1 { get; set; }
		public string note_2 { get; set; }
		public string note_3 { get; set; }
		public string note_4 { get; set; }
		public string note_5 { get; set; }
		public string transaction_code { get; set; }
		public string transaction_code_Name { get; set; }

		public string business_unit { get; set; }
		public string module_name { get; set; }
		public Int32 parameter_deadline { get; set; }
		public string ColorStatus { get; set; }

		public DateTime created_date { get; set; }
		public string created_by { get; set; }

		public DateTime modified_date { get; set; }
		public string modified_by { get; set; }

		public int row_status { get; set; }

	}
}
