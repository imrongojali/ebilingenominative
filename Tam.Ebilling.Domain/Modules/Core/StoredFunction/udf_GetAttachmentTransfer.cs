﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tam.Ebilling.Domain
{
    public class udf_GetAttachmentTransfer
    {
        public long ID { get; set; }
        public string invoiceNumber { get; set; }
        public int trans_type { get; set; }
        public string businessUnit { get; set; }
        public DateTime doc_date { get; set; }
        public string currency { get; set; }
        public decimal total_amount { get; set; }
        public DateTime payment_date { get; set; }
        public decimal total_payment { get; set; }
        public string attachment { get; set; }
        public bool status_payment { get; set; }
        public DateTime created_date { get; set; }
        public string created_by { get; set; }
        public DateTime modified_date { get; set; }
        public string modified_by { get; set; }
        public int row_status { get; set; }
    }
}
