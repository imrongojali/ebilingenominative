﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tam.Ebilling.Domain
{
    public class udf_GetAttachmentNotaRetur
    {
        public long attachment_notaretur_id { get; set; }
        public string filename { get; set; }
        public string reference { get; set; }
        public DateTime created_date { get; set; }
        public string document_type { get;set; }
    }
}
