﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tam.Ebilling.Domain
{
    public class udf_GetInvoiceNumber
    {
        public long attachment_invoice_id { get; set; }
        public string filename { get; set; }
        public string invoice_number { get; set; }
        public DateTime created_date { get; set; }
    }
}
