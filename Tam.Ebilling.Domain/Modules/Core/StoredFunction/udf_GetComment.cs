﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tam.Ebilling.Domain
{
    public class udf_GetComment
    {
        public Int64 comment_id { get; set; }
        public int business_unit_id { get; set; }
        public string invoice_number { get; set; }
        public int user_type { get; set; }
        public string user_id { get; set; }
        public string user_name { get; set; }
        public string comment { get; set; }
        public DateTime created_date { get; set; }
    }
}
