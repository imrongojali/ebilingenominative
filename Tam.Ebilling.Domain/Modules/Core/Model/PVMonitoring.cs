﻿using Agit.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tam.Ebilling.Domain
{
    public partial class PVMonitoring
    {
        [Key]
        public int? PROCESS_ID { get; set; }
        public int SEQ_NO { get; set; } //
        public int SEQ_XCL { get; set; }
        public string INVOICE_NO { get; set; }//
        public string INVOICE_DATE { get; set; }//
        public string COST_CENTER_CD { get; set; }//
        public string CURRENCY_CD { get; set; }//
        public Double? AMOUNT { get; set; }//
        public string DESCRIPTION { get; set; }//
        public byte OK { get; set; }
        public int? GL_ACCOUNT { get; set; }//
        public Double? DPP_AMOUNT { get; set; }//
        public string ACC_INF_ASSIGNMENT { get; set; }//
        public string TAX_NO { get; set; }//
        public string TAX_DT { get; set; }//
        public Double? AMOUNT_TURN_OVER { get; set; }
        public Double? AMOUNT_PPN { get; set; }
        public Double? AMOUNT_SEAL { get; set; }
        public Double? AMOUNT_PPh21_INTERN { get; set; }
        public Double? AMOUNT_PPh21_EXTERN { get; set; }
        public Double? AMOUNT_PPh26 { get; set; }
        public Double? AMOUNT_PPh_FINAL { get; set; }
        public Double? AMOUNT_BM { get; set; }
        public Double? AMOUNT_PPN_IMPORT { get; set; }
        public Double? AMOUNT_PPnBM { get; set; }
        public Double? AMOUNT_PPh22 { get; set; }
        public Double? AMOUNT_PnPB { get; set; }
        public Double? AMOUNT_BANK_CHARGE { get; set; }
        public string ERRMSG { get; set; }

        public Double? AMOUNT_SALES_OTHER { get; set; }
        public Double? AMOUNT_RENTAL_EXPENSE { get; set; }
        public Double? AMOUNT_TRANSPORT_OPERATIONAL { get; set; }
        public Double? AMOUNT_TRANSPORT_TOLL { get; set; }
        public Double? AMOUNT_CONSUMPTION { get; set; }
        public Double? AMOUNT_MEALS_PAPER { get; set; }
        public Double? AMOUNT_OFFICE_SUPPLIES { get; set; }
        public Double? AMOUNT_COMMUNICATION { get; set; }
        public Double? AMOUNT_NEWSPAPER { get; set; }
        public Double? AMOUNT_REPAIR { get; set; }
        public Double? AMOUNT_OTHER { get; set; }
        public Double? DPP_PPh_AMOUNT { get; set; }
        public string TAX_CODE_PPh23 { get; set; }
        public Double? TAX_TARIFF_PPh23 { get; set; }
        public string TAX_CODE_PPh21_EXTERN { get; set; }
        public Double? TAX_DPP_ACCUMULATION_PPH21_EXTERN { get; set; }
        public Double? TAX_TARIFF_PPh21_EXTERN { get; set; }
        public string NIK_PPh21_EXTERN { get; set; }
        public string TAX_CODE_PPh26 { get; set; }
        public Double? TAX_TARIFF_PPh26 { get; set; }
        public string TAX_CODE_PPh_FINAL { get; set; }
        public Double? TAX_TARIFF_PPh_FINAL { get; set; }
        public string INFO_PPh_FINAL { get; set; }
        public string NPWP_Available { get; set; }

        public DateTime CREATED_DATE { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? MODIFIED_DATE { get; set; }
        public string MODIFIED_BY { get; set; }

        //Gak ada ditable
        public Double? MATERAI { get; set; }
        public Double? AMOUNT_PPh23 { get; set; }

        //Helper
        public DateTime INVOICE_DATE_DT { get; set; }
        public string STATUS_FLAG { get; set; }
        public string STATUS_DESC { get; set; }
        public string ATTACHMENT_UPLOADED { get; set; }
        public string PV_NO { get; set; } //
        public string PV_YEAR { get; set; } //
        public string WBS_NO { get; set; } //
        public int? ITEM_TRANSACTION_CD { get; set; }//
        public string TAX_CD { get; set; }//
        public string FROM_CURR { get; set; }//
        public string FROM_SEQ { get; set; }//
        public int? ITEM_NO { get; set; }//
        public string TAX_ASSIGNMENT { get; set; }//
        public string WHT_TAX_CODE { get; set; }//
        public string WHT_TAX_TARIFF { get; set; }//
        public string WHT_TAX_ADDITIONAL_INFO { get; set; }//
        public string WHT_DPP_PPh_AMOUNT { get; set; }//

        public string VENDOR_DESC { get; set; }
        public string STATUS_BUDGET { get; set; }
        public string IS_USERTAM { get; set; }

        public string ACCOUNT_INFO { get; set; }
    }

    public partial class PVHeader
    {
        [Key]
        public int? PROCESS_ID { get; set; }        
        
        public string UPLOAD_FILENAME { get; set; }
        public int? REFF_NO { get; set; }
        public int? ROWS_COUNT { get; set; }
        public byte? OK { get; set; }

        public string PV_NO { get; set; }//
        public string PV_YEAR { get; set; }//
        public int? STATUS_CD { get; set; }//
        public string PAY_METHOD_CD { get; set; }//
        public int? VENDOR_GROUP_CD { get; set; }//
        public string VENDOR_CD { get; set; }//
        public int? PV_TYPE_CD { get; set; }//
        public int? TRANSACTION_CD { get; set; }//
        public int? SUSPENSE_NO { get; set; }//
        public DateTime? ACTIVITY_DATE_FROM { get; set; }//
        public DateTime? ACTIVITY_DATE_TO { get; set; }//
        public string DIVISION_ID { get; set; }//
        public DateTime? PV_DATE { get; set; }//
        public int? REFFERENCE_NO { get; set; }//
        public int? BANK_TYPE { get; set; }//
        public DateTime? POSTING_DATE { get; set; }//
        public decimal AMOUNT { get; set; }//

        public string PV_TYPE { get; set; }//
        public string PAYMENT_METHOD { get; set; }//
        public string BUDGET { get; set; }//
        public string TRANSACTION_DESC { get; set; }//
        public string VENDOR_NAME { get; set; }//
        public string VENDOR_GRP { get; set; }

        // 2020-03-02
        public string STATUS_FLAG { get; set; }
        public string STATUS_DESC { get; set; }
        public bool IS_BUDGET { get; set; }
        public string STATUS_BUDGET { get; set; }
        public string INVOICE_NO { get; set; }
        public string PV_DATE_DT { get; set; }
        public string REJECTED_REASON { get; set; }//
        public string IS_USERTAM { get; set; }
        public decimal TOTAL_TURNOVER { get; set; }// 
        public string IS_EBILLING { get; set; }

        public string CREATED_BY { get; set; }
        public string CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public string CHANGED_DT { get; set; }

        public int approveLevel { get; set; }
        public bool financeHeader { get; set; }

        public List<PVMonitoring> pVMonitorings { get; set; }
        public List<PVAmount> pvAmounts { get; set; }
        public List<PVMonitoringAttachment> pvAttachments { get; set; }

        // 2020-03-30
        public string ACCOUNT_INFO { get; set; }

        //2020-03-31
        public string STATUS_COMPARE { get; set; }
        public DateTime? EXPIRED_DATE { get; set; }
        public string URL_WHT { get; set; }

        public string REJECTED_BY { get; set; }
        public string REJECTED_DT { get; set; }

        //Email
        public int TOTAL_NEW { get; set; }
        public int TOTAL_ALL { get; set; }
        public int REMAINING_DAY { get; set; }
        public string GROUP { get; set; }
        public string DAY_REMAINING { get; set; }

        public string BUDGET_NO_FORMAT { get; set; }
        public string BUDGET_DESCRIPTION { get; set; }
        public string PV_TYPE_NAME { get; set; }
        public string DESCRIPTION { get; set; }
        public string DIVISION_NAME { get; set; }

        public DateTime? PLANNING_PAYMENT_DT { get; set; }
        public DateTime? ACTUAL_PAYMENT_DT { get; set; }
    }

    public partial class PVAmount
    {
        public int? DOC_NO { get; set; }
        public int? DOC_YEAR { get; set; }
        public string CURRENCY_CD { get; set; }
        public decimal TOTAL_AMOUNT { get; set; }
        public decimal EXCHANGE_RATE { get; set; }
    }

    public partial class PVMonitoringBankAccount
    {
        public string INVOICE_NO { get; set; }
        public string VENDOR_CD { get; set; }
        public string CTRY { get; set; }
        public string BANK_KEY { get; set; }
        public string BANK_ACCOUNT { get; set; }
        public string BANK_TYPE { get; set; }
        public string ACCT_HOLDER { get; set; }
        public string NAME_OF_BANK { get; set; }
        public string CURRENCY { get; set; }
        public string STATUS_REQ { get; set; }

        public string BENEFICIARIES { get; set; }
        public string FILE_NAME { get; set; }
        public string GUID_FILENAME { get; set; }
        public string DIRECTORY { get; set; }
    }

    public class PVCoverData
    {
        public string INVOICE_NO { get; set; }
        public string CURRENCY { get; set; }
        public decimal? AMOUNT { get; set; }
        public string TAX_NO { get; set; }
        public DateTime? INVOICE_DATE { get; set; }
        public decimal? TAX_AMOUNT { get; set; }
    }

    [Serializable]
    public class CodeConstant
    {
        public string Code { set; get; }
        public string Description { set; get; }
    }

    [Serializable]
    public class OutstandingSuspense
    {
        public string DIVISION_ID { get; set; }
        public string DOC_NO { get; set; }
        public string DOC_YEAR { get; set; }
        public string VENDOR_CD { get; set; }
        public string VENDOR_NAME { get; set; }
        public int DAYS_OUTSTANDING { get; set; }
        public decimal TOTAL_AMOUNT { get; set; }
    }

    public partial class ComboEx
    {
        public string CODE { get; set; }
        public string NAME { get; set; }
    }

    public class WBSStructure
    {
        public string WbsNumber { set; get; }
        public string Description { set; get; }
        public int Year { set; get; }
        public string EOA { set; get; }
        public int Division { set; get; }
    }
}
