﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tam.Ebilling.Domain
{
    [Table("tb_r_comment")]
    public partial class Conversation
    {
        public int comment_Id { get; set; }
        public int business_unit_id { get; set; }
        public string invoice_number { get; set; }
        public int user_type { get; set; }
        public string user_id { get; set; }
        public string comment { get; set; }
        public DateTime created_date { get; set; }
        public string created_by { get; set; }
        public DateTime modified_date { get; set; }
        public string modified_by { get; set; }
        public int row_status { get; set; }
    }
}
