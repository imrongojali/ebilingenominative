﻿using Agit.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tam.Ebilling.Domain
{
    [Table("tb_r_pv_monitoring_attachment")]
    public partial class PVMonitoringAttachment
    {
        [Key]
        public string REFERENCE_NO { get; set; }
        public int REF_SEQ_NO { get; set; }
        public string ATTACH_CD { get; set; }
        public string DESCRIPTION { get; set; }
        public string DIRECTORY { get; set; }
        public string FILE_NAME { get; set; }

        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }

        //helper
        public string ATTACH_DESC { get; set; }
        public string STATUS { get; set; }
        public string PV_NO { get; set; }
        public string IS_USERTAM { get; set; }
        public string BY_INTERNAL { get; set; }
    }

    public partial class PVHDistribusionStatus
    {
        [Key]
        public string REFF_NO { get; set; }
        public int SEQ_NO { get; set; }
        public string STATUS_CD { get; set; }
        public DateTime? DATE { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }        
    }
}
