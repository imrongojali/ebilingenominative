﻿using Agit.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tam.Ebilling.Domain
{
    [Table("tb_r_bukti_potong")]
    public partial class BuktiPotong
    {
        [Key]
        public string invoice_number { get; set; }
        public decimal materai_amount { get; set; }
        public decimal amount_wht { get; set; }
        public DateTime tgl_payment { get; set; }
        public bool is_active { get; set; }
        public bool bukti_potong_uploaded { get; set; }
        
        public DateTime created_date { get; set; }
        public string created_by { get; set; }
        public DateTime? modified_date { get; set; }
        public string modified_by { get; set; }
        public int row_status { get; set; }

        //transient
        public string customer_group { get; set; }
        public string customer_name { get; set; }
        public string upload_id { get; set; }
        public string ref_no { get; set; }
        public int CountRelatedInvoice { get; set; }

        //add by RG 12102020
        public string jenis_pajak { get; set; }
        public string nomor_bukti_potong { get; set; }
        public DateTime tanggal_bukti_potong { get; set; }
        public string masa_pajak { get; set; }
        public string tahun_pajak { get; set; }
        public decimal jumlah_dpp { get; set; }
        public decimal tarif { get; set; }
        public decimal jumlah_pph { get; set; } 
        public DateTime invoice_date { get; set; }
        public string flag_potong_pajak { get; set; }
        public string description { get; set; }  

        public string no_npwp { get; set; }
        public string alamat_npwp { get; set; }
        public string nama_npwp { get; set; }
        public string wht_pdf_22 { get; set; }
        public string wht_pdf_23 { get; set; }
        public string wht_pdf_42 { get; set; }

        public string wht_pdf_22_status { get; set; }
        public string wht_pdf_23_status { get; set; }
        public string wht_pdf_42_status { get; set; } 
        public string wht_prepaid_status { get; set; }
        public string wht_pdf_status { get; set; }

        public string business_unit { get; set; }

        public string tax_article { get; set; }

        public bool IsSelected { get; set; }
    }
}


