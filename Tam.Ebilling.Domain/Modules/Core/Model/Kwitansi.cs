﻿using Agit.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tam.Ebilling.Domain
{
    //[("udf_Invoice")]
    public partial class Kwitansi
    {

        public string id { get; set; }
        public string invoice_number { get; set; }
        public string transaction_type { get; set; }
        public DateTime doc_date { get; set; }
        public DateTime due_date { get; set; }
        public string currency { get; set; }
        public decimal dpp { get; set; }
        public decimal tax { get; set; }
        public decimal total_amount { get; set; }
        public string ar_status { get; set; }
        public string tax_invoice { get; set; }
        public string pph_no { get; set; }
        public string da_code { get; set; }
        public string business_unit { get; set; }
        public int business_unit_id { get; set; }
        public string ar_status_name { get; set; }
        public string ar_status_img_url { get; set; }
        public string da_no { get; set; }
        public string tax_attc_url { get; set; }
        public int is_tax_attc_available { get; set; }
        public int invoice_type { get; set; }       // 1: invoice, 2: nota retur

        // Added by Angga 20181013.1336
        public List<udf_GetComment> comments { get; set; }
        public decimal rate { get; set; }
        public bool inv_downloaded { get; set; }
        public bool notur_downloaded { get; set; }
        public bool notur_uploaded { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{dd/MM/yyyy}")]
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0: d}")]
        public DateTime downloaded_date { get; set; }
        public string downloaded_by { get; set; }
        public decimal amount_pph { get; set; }
        public int transaction_type_id { get; set; }

        //
        public string customer_group { get; set; }
        public string email { get; set; }
        public string customer_name { get; set; }

        public string transaction_name { get; set; }
    }
}
