﻿using Agit.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tam.Ebilling.Domain
{
    //[("udf_WHT")]
    public partial class WHT
    {
        public string idseq { get; set; }
        public Guid id { get; set; }
        public bool IsSelected { get; set; }
        public string tax_article { get; set; }
        public string wht_number { get; set; }
        public DateTime wht_date { get; set; }
        public string dealer_name { get; set; }
        public string original_dealer_name { get; set; }
        public string dealer_address { get; set; }
        public string dealer_npwp { get; set; }
        public decimal wht_based_amount { get; set; }
        public decimal wht_tarif { get; set; }
        public decimal wht_amount { get; set; }
        public string wht_description { get; set; }
        public string invoice_number { get; set; }
        public DateTime invoice_date { get; set; }
        public string ebupot_ref_num { get; set; }
        public string map_code { get; set; }
        public string tax_type { get; set; }
        public string map_code_tax_type { get; set; }
        public string pdf_wht_status { get; set; }
        public string pdf_wht_status_ex { get; set; }
        public string pdf_wht { get; set; }
        public string pdf_wht_fname { get; set; }
        public string trans_type_desc { get; set; }
        public string jenis_pajak { get; set; }
        public string tax_period_month { get; set; }
        public string tax_period_year { get; set; }
    }
}
