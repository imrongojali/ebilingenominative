﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tam.Ebilling.Domain
{
    public partial class DebitAdvice
    {
        public String Transaction_Name;
        public Decimal Total;
        public Decimal Total_Non_VAT;
        public Decimal Total_VAT;
        public Decimal Total_Dormitory;
        public Decimal Total_VatDormitory;
        public Decimal Total_Micellenious;
        public String Name_Stamp;
        public Decimal Stamp;
        public String Customer_Code;
        public String Tax_Number;
        public String Invoice_Number;
        public String Da_Number; //awalia 24.05.2016
        public DateTime Posting_Date;
        public String Users;
        public Decimal TotalAll;
        public String Name;
        public String Street;
        public String Word;
        public String Name_Total_Include_VAT;
        public String Name_Total_Exclude_VAT;
        public String Name_VAT;
        public String Name_Dormitory;
        public String Name_VatDormitory;
        public String Name_Micellenious;
        public String City;
        public DateTime Date_Now;
        public String Currency_Code;
        public String Signature;
    }
}
