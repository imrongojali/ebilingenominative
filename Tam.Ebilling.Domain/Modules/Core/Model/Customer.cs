﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tam.Ebilling.Domain
{
    [Table("vw_Customer_All")]
    public partial class Customer
    {
        public string customer_code { get; set; }
        public string customer_name { get; set; }
        public string customer_group { get; set; }
        public int business_unit_id { get; set; }
        public DateTime created_date { get; set; }
        public string created_by { get; set; }
        public DateTime modified_date { get; set; }
        public string modified_by { get; set; }
        public int row_status { get; set; }


        

    }

    [Table("tb_m_customer_group")]
    public partial class CustomerGroup
    {
        public string customer_group_id { get; set; }
        public string customer_group_name { get; set; }
        public string spv_email { get; set; }
        public string depthead_email { get; set; }
        public string divhead_email { get; set; }
        public string created_by { get; set; }
        public DateTime created_date { get; set; }
        public string modified_by { get; set; }
        public DateTime modified_date { get; set; }
        public int row_status { get; set; }
    }
}
