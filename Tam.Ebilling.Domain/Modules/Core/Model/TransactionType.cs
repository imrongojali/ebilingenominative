﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tam.Ebilling.Domain
{
    [Table("vw_Transaction_Type")]
    public partial class TransactionType
    {
        public int transaction_type_id { get; set; }
        public string transaction_name { get; set; }
    }
}
