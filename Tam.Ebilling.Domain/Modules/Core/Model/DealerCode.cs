﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tam.Ebilling.Domain
{
    [Table("vw_DealerCode")]
    public partial class DealerCode
    {
        public string dealer_code_id { get; set; }
        public string dealer_code_name { get; set; }
    }
}
