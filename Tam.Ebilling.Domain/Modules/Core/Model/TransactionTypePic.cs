﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tam.Ebilling.Domain
{
    [Table("vw_TransactionType_PIC")]
    public partial class TransactionTypePIC
    {
        public int Transaction_Type_ID { get; set; }
        public string Transaction_Code { get; set; }
        public string Transaction_Name { get; set; }
        public string Transaction_Description { get; set; }
        public int Departement_ID { get; set; }
        public string Departement_Name { get; set; }
        public string Level_Code { get; set; }
        public string Level_Description { get; set; }
        public string Display_Name { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
    }
}
