﻿using System;

namespace Tam.Ebilling.Domain
{
    //[("udf_Invoice")]
    public partial class DRKB
    {

        public String Id { get; set; }
        public bool IsSelected { get; set; }

        public string TaxNumber { get; set; }
        public DateTime TanggalFaktur { get; set; }
        public string NPWPCustomer { get; set; }
        public string NamaCustomer { get; set; }
        public string FrameNumber { get; set; }
        public string EngineNumber { get; set; }
        public string MerkOrType { get; set; }
        public string ModelType { get; set; }
        public int Year { get; set; }
        public string BusinessUnit { get; set; }

        public Nullable<Decimal> JumlahDPP { get; set; }

        public Nullable<Decimal> JumlahPPN { get; set; }

        public Nullable<Decimal> LuxTaxAmount { get; set; }

        public Nullable<Decimal> PrevLuxTaxAmount { get; set; }

        public string Remarks { get; set; }

    }
}
