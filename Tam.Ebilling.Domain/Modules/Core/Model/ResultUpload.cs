﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Tam.Ebilling.Domain
{
    public partial class ResultUpload
    {
        public int Success { get; set; }
        public int Error { get; set; }
        public string NamaFile { get; set; }

    }
}
