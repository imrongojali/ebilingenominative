﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tam.Ebilling.Domain
{
    [Table("[TAM_EFAKTUR].[dbo].[tb_m_jenispajak]")]
    public partial class TransactionTypeCode
    {
        public Guid id { get; set; }
        public string TransaksiTypeCode { get; set; }
    }
}