﻿using Agit.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Tam.Ebilling.Domain
{
    [Table("tb_m_email_template")]
    public partial class EmailTemplate
    {
        [Key]
        public int mal_id { get; set; }
        public string module { get; set; }
        public string mail_key { get; set; }
        public string mail_from { get; set; }
        public string display_name { get; set; }
        public string title { get; set; }
        public string subject { get; set; }
        public string subject_notur { get; set; }
        public string mail_content { get; set; }
        public string mail_content_notur { get; set; }
        public string created_by { get; set; }
        public DateTime? created_date { get; set; }
        public string modified_by { get; set; }
        public DateTime? modified_date { get; set; }
        public int row_status { get; set; }
    }
}
