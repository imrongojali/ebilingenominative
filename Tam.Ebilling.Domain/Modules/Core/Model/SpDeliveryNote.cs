﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tam.Ebilling.Domain
{
    public partial class SpDeliveryNote
    {
        public String MainDealer;
        public String CustomerName;
        public String CustomerAddress;
        public String CustomerCode;
        public DateTime DeliveryDate;
        public String OrderNo;
        public String OrderType;
        public String Remarks;
        public String InvoiceNo;
        public DateTime InvoiceDate;
        public String PartNumber;
        public String PartName;
        public Int32 Ordered;
        public Int32 Issued;
        public Decimal RetailPrice;
        public Decimal NetSalesPrice;
        public Decimal DiscRate;
    }
}
