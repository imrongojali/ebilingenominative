﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tam.Ebilling.Domain
{
    public partial class SpNotaRetur
    {
        public String NomorRetur;
        public String FakturPajakNo;
        public DateTime TglRetur;
        public DateTime TglFakturPajak;
        public String CustName;
        public String Address1;
        public String Address2;
        public String Npwp;
        public String PiNo;
        public String PartNo;
        public String PartName;
        public int Kuantum;
        public Decimal HargaSatuan;
        public Decimal HargaBkp;
        public String da_no;
    }
}

