﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tam.Ebilling.Domain
{
    public partial class ViewTaxTransactionCode
    {
        public string tax_transaction_code { get; set; }
        public string description { get; set; }
    }
}
