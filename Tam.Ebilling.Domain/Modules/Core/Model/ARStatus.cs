﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tam.Ebilling.Domain
{
    [Table("vw_ar_status")]
    public partial class ARStatus
    {
        public string config_value { get; set; }
        public string config_text { get; set; }
        public string config_description { get; set; }
    }
}
