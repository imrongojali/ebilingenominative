﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Tam.Ebilling.Domain
{
    [Table("tb_m_feature")]
    public partial class MenuFeature
    {
        [Key]
        public int internal_id { get; set; }
        public int menu_id { get; set; }
        public string feature_code { get; set; }
        public string feature_desc { get; set; }
        public List<MenuLevelFeature> level_codes { get; set; }
    }
}
