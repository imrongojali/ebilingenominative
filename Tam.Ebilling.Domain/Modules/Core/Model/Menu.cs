﻿using Agit.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tam.Ebilling.Domain
{
    [Table("tb_m_menu")]
    public partial class Menu
    {
        [Key]
        public int menu_id { get; set; }
        public string group_name { get; set; }
        public int parent_id { get; set; }
        public int permission_id { get; set; }
        public int menu_group_id { get; set; }
        public string title { get; set; }
        public string url { get; set; }
        public string description { get; set; }
        public string icon_class { get; set; }
        public Int16 order_index { get; set; }
        public Int16 visible { get; set; }
        public DateTime created_date { get; set; }
        public string created_by { get; set; }
        public DateTime modified_date { get; set; }
        public string modified_by { get; set; }
        public int row_status { get; set; }
    }
}
