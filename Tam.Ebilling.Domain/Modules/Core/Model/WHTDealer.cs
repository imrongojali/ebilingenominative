﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tam.Ebilling.Domain
{
    [Table("vw_WHTDealer")]
    public partial class WHTDealer
    {
        public string dealer_npwp { get; set; }
        public string dealer_name { get; set; }
        public string original_dealer_name { get; set; }
    }
}
