﻿using Agit.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tam.Ebilling.Domain
{
    [Table("tb_m_config")]
    public partial class Config
    { 
        [Key]
        public int config_id { get; set; }
        public string module { get; set; }
        public string config_key { get; set; }
        public string config_text { get; set; }
        public string config_value { get; set; }
        public string config_data_type { get; set; }
        public string created_by { get; set; }
        public DateTime? created_date { get; set; }
        public string modified_by { get; set; }
        public DateTime? modified_date { get; set; }
        public int? row_status { get; set; }
    }
}
