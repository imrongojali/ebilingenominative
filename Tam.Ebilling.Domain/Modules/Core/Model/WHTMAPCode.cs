﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tam.Ebilling
{
    [Table("vw_WHTMAPCode")]
    public partial class WHTMAPCode
    {
        public string config_value { get; set; }
        public string config_text { get; set; }
        public string config_description { get; set; }
    }
}
