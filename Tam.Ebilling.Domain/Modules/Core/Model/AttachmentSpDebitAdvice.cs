﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tam.Ebilling.Domain
{
    public partial class AttachmentSpDebitAdvice
    {
        public String DaNo;
        public String CaNo;
        public String CustomerName;
        public String CustomerAddress;
        public String InvoiceNo;
        public DateTime InvoiceDate;
        public String OT;
        public String OrderNo;
        public Int32 Items;
        public Decimal GrossAmount;
        public Decimal NetAmount;
    }
}
