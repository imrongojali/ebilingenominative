﻿using Agit.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tam.Ebilling.Domain
{
    //Table("tb_t_upload_d")
    public partial class BankAccount
    {
        //Key
        public string VENDOR_CD { get; set; }
        public string CTRY { get; set; }
        public string BANK_KEY { get; set; }
        public string BANK_ACCOUNT { get; set; }
        public string BANK_TYPE { get; set; }
        public string ACCT_HOLDER { get; set; }
        public string NAME_OF_BANK { get; set; }
        public string CURRENCY { get; set; }

        //helper
        public string BANK_NAME { get; set; }
        //public string Value { get; set; }
        //public string Text { get; set; }

        public string REQ_REMAINING { get; set; }
        public string EMAIL_TO { get; set; }
    }

    public partial class TransactionType
    {
        public int transaction_cd { get; set; }
        public string transaction_name_elvis { get; set; }
        public int transaction_level_cd { get; set; }
	    public string std_wording { get; set; }
	    public int group_cd { get; set; }
	    public int ft_cd { get; set; }
	    public int template_cd { get; set; }
	    public int close_flag { get; set; }
	    public int service_flag { get; set; }
	    public int budget_flag { get; set; }
	    public int attachment_flag { get; set; }
	    public int production_flag { get; set; }
	    public int cost_center_flag { get; set; }
	    public string assignment_tax_cd { get; set; }
    }
}
