﻿using Agit.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tam.Ebilling.Domain
{
    [Table("tb_r_application_log")]
    public partial class ApplicationLog
    {
        [Key]
        public int app_log_id { get; set; }
        public string username { get; set; }
        public string iP { get; set; }
        public string browser { get; set; }
        public string message_type { get; set; }
        public string message_location { get; set; }
        public string message_description { get; set; }
        public DateTime created_date { get; set; }
        public string created_by { get; set; }
        public DateTime modified_date { get; set; }
        public string modified_by { get; set; }
        public int row_status { get; set; }
    }
}
