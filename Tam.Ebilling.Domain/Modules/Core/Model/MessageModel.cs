﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Tam.Ebilling.Domain
{
    [DataContract]
    public class MessageModel
    {
        [DataMember]
        public string message { get; set; }
        [DataMember]
        public bool success { get; set; }
        [DataMember]
        public string data { get; set; }
        [DataMember]
        public Dictionary<string, string> errors { get; set; }
    }

    public class PVOutput
    {
        public int PV_NO { get; set; }
        public int PV_YEAR { get; set; }
        public string STATUS_PV { get; set; }
        public string ERR_MESSAGE { get; set; }
    }
}
