﻿using Agit.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Tam.Ebilling.Domain.Modules.Core.Model
{
    [Table("tb_r_attachment")]
    public partial class Attachment
    {
        [Key]
        public long attachment_id { get; set; }
        public string filename { get; set; }
        public string reference { get; set; }
        public DateTime created_date { get; set; }
        public string created_by { get; set; }
        public DateTime modified_date { get; set; }
        public string modified_by { get; set; }
        public int row_status { get; set; }
    }
}
