﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Tam.Ebilling.Domain
{
    [Table("tb_r_attachment_invoice_notaretur")]
    public partial class ScanInvoiceNotur
    {
        [Key]
        public long attachment_invoice_notur_id { get; set; }
        public string filename { get; set; }
        public string filepath { get; set; }
        public string invoice_number { get; set; }
        public DateTime created_date { get; set; }
        public string created_by { get; set; }
        public DateTime modified_date { get; set; }
        public string modified_by { get; set; }
        public string Status { get; set; }
        public int row_status { get; set; }
    }
}