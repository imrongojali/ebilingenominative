﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tam.Ebilling.Domain
{
    public partial class SpDebitCreditAdvice
    {
        public String DaNo;
        public String CaNo;
        public DateTime DeliveryDate;
        public String CustomerName;
        public String CustomerAddress;
        public String FakturPajakNo;
        public Decimal TurnOverAmount;
        public Decimal Vat10;
        public Decimal Total;
        public DateTime DueDateTo;
        public DateTime DueDateVat;
        public DateTime CollectionDate;
    }
}

