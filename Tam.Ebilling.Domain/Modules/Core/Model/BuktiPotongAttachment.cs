﻿using Agit.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tam.Ebilling.Domain
{
    [Table("tb_r_bukti_potong_attachment")]
    public partial class BuktiPotongAttachment
    {
        [Key]
        public long attachment_bukti_potong_id { get; set; }

        public string upload_id { get; set; }

        [ForeignKey("BuktiPotong")]
        public string invoice_number { get; set; }

        public string filename { get; set; }
        public string server_filename { get; set; }
        public DateTime created_date { get; set; }
        public string created_by { get; set; }
        public DateTime modified_date { get; set; }
        public string modified_by { get; set; }
        public int row_status { get; set; }
        public string status { get; set; }
        public string DocumentTypeText { get; set; }

    }
}
