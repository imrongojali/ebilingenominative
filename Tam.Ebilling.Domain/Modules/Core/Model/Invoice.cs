﻿using Agit.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tam.Ebilling.Domain
{
    //[("udf_Invoice")]
    public partial class Invoice
    {
      
        public string id { get; set; }
        public string invoice_number { get; set; }
        public string transaction_type { get; set; }
        public DateTime doc_date { get; set; }
        public DateTime due_date { get; set; }
        public string currency { get; set; }
        public decimal dpp { get; set; }
        public decimal tax { get; set; }
        public decimal total_amount { get; set; }
        public string ar_status { get; set; }
        public string tax_invoice { get; set; }
        public string pph_no { get; set; }
        public string da_code { get; set; }
        public string business_unit { get; set; }
        public int business_unit_id { get; set; }
        public string ar_status_name { get; set; }
        public string ar_status_img_url { get; set; }
        public string da_no { get; set; }
        public string tax_attc_url { get; set; }
        public int is_tax_attc_available { get; set; }
        public int invoice_type { get; set; }       // 1: invoice, 2: nota retur

        // Added by Angga 20181013.1336
        public List<udf_GetComment> comments { get; set; }
        public decimal rate { get; set; }
        public bool inv_downloaded { get; set; }
        public string is_inv_available { get; set; }
        public string is_inv_scan_available { get; set; }
        public bool notur_downloaded { get; set; }
        public bool notur_uploaded { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{dd/MM/yyyy}")]
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0: d}")]
        public DateTime downloaded_date { get; set; }
        public string downloaded_by { get; set; }
        public decimal amount_pph { get; set; }
        public int transaction_type_id { get; set; }
        public bool inv_scan_downloaded { get; set; }
        public DateTime downloadedscan_date { get; set; }
        public string downloadedscan_by { get; set; }
        //
        public string customer_group { get; set; }
        public string email { get; set; }
        public string customer_code { get; set; }
        public string customer_name { get; set; }
        public string transaction_name { get; set; }
        public decimal rate_pph { get; set; }
        public decimal vat_dormitory { get; set; }
        public decimal dormitory { get; set; }

        public bool pi_downloaded { get; set; }
        public DateTime pi_downloaded_date { get; set; }
        public string pi_downloaded_by { get; set; }

        public string customer_address { get; set; }

        public int is_notur_upload_done { get; set; }
        public bool is_retur { get; set; }
        //public string pdf_wht { get; set; } //add new wht
        //public string pdf_wht_fname { get; set; } //add new wht
        public string keyword { get; set; }

        # region  //region 20201009
        public string ApprovalStatus { get; set; }
        public string PDFWithholding { get; set; }
        public string pdf_wht_fname { get; set; }
        #endregion

        //add by hendry
        public string credit_note_status { get; set; }

        //isti
        public bool creditnotettd { get; set; }
        public bool creditnotedgt { get; set; }
        public string credit_note_number { get; set; }
        public DateTime tax_date { get; set; }
        public string cn_approval_status { get; set; }


        //Download CA Report 
        public string NomorNotaRetur { get; set; }
        public DateTime TanggalRetur { get; set; }
        public string NomorCreditAdvice { get; set; }
        public string NoFakturPajakYangDiRetur { get; set; }
        public string NomorDAYangDiRetur { get; set; }
        public DateTime TglFaktuPajakYangDiRetur { get; set; }
        public decimal PPN { get; set; }
        public string customer_npwp { get; set; }

        //download csv file
        public string RM { get; set; }
        //NEW 24042021
        public string KDJenisTransaksi { get; set; }
        //END
        public string FGPengganti { get; set; }
        public bool isCreditable { get; set; }
        public string MasaPajakRetur { get; set; }
        public string TahunPajakRetur { get; set; }
        public decimal JumlahReturPPNBM { get; set; }
        public bool IsSelected { get; set; }
        public string PdfUrl { get; set; }
        public string PdfUrl_fname { get; set; }

        //public string credit_note_status_dec { get; set; }

        public string nameReportCSV { get; set; }
        public string npwpReportCSV { get; set; }
  


    }
}
