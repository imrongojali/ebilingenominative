﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tam.Ebilling.Domain
{
    public partial class MvInvoice
    {
        public Guid DetailId { get; set; }
        public Int16 PrintInvoice { get; set; }
        public Int16 PrintPph { get; set; }
        public String DealerCode { get; set; }
        public String DealerName { get; set; }
        public String Address { get; set; }
        public String NpwpNo { get; set; }
        public String DoDate { get; set; }
        public String DueDate { get; set; }
        //public String TaxCollectDate { get; set; }
        public String TaxNo { get; set; }
        public String DaNo { get; set; }
        public String DaCnNo { get; set; }
        public String DoNo { get; set; }
        public String FrameNo { get; set; }
        public String RrnNo { get; set; }
        public String BranchCode { get; set; }
        public String ModelName { get; set; }
        public String ModelCodeSuffix { get; set; }
        public Decimal DistributionPrice { get; set; }
        public Decimal AmountVat { get; set; }
        public Decimal TotalAmount { get; set; }
        public String RatePph { get; set; }
        public Decimal AmountPph { get; set; }

    }
}
