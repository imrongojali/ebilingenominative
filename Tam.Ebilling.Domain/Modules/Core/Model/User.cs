﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Tam.Ebilling.Domain
{
    [Table("vw_User")]
    public partial class User
    {
        public string user_id { get; set; }
        public string display_name { get; set; }
        public string user_role { get; set; }
        public string customer_group { get; set; }
        public string user_type { get; set; }
        public string email { get; set; }
        public string level_code { get; set; }
    }
}
