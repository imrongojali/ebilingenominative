﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Tam.Ebilling.Domain
{
    [Table("tb_r_feature_role")]
    public partial class MenuLevelFeature
    {
        [Key]
        public int internal_id { get; set; }
        public int feature_id { get; set; }
        public string level_code { get; set; }
    }
}
