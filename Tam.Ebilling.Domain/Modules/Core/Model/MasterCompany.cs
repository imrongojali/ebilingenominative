﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tam.Ebilling.Domain
{
    public partial class MasterCompany
    {
        public string Lokasi { get; set; }
        public string Code { get; set; }
        public string Nama_pt { get; set; }
        public string Jabatan_ttd { get; set; }
        public string Npwp_pt { get; set; }
        public string Nama_ttd { get; set; }
        public string Image_ttd { get; set; }
        public string Image_cap { get; set; }
        public string no_pengukuhan_pt { get; set; }
        public string alamat_pt { get; set; }
    }
}
