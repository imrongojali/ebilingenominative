﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tam.Ebilling.Domain
{

    [Table("tb_r_comment_detail")]
    public partial class CommentDetail
    {
        public int comment_detail_id { get; set; }
        public int comment_id { get; set; }
        public string user_id { get; set; }
        public bool is_read { get; set; }
    }
    public class Notification
    {
        public int comment_id { get; set; }
        public string invoice_number { get; set; }
        public int count { get; set; }
    }
}
