﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tam.Ebilling.Domain
{
    public class Budget
    {
        public string no { get; set; }
        public string year { get; set; }
        public string wbs { get; set; }
        public string budget { get; set; }

        public int doc_no { get; set; }
        public string wbs_no { get; set; }
        public DateTime? date { get; set; }
        public decimal amount { get; set; }
        public int status { get; set; }
        public string username { get; set; }
    }

    public class BudgetResponse
    {
        public object data { get; set; }
        public string message { get; set; }
        public bool success { get; set; }
        public bool error { get; set; }
    }
}
