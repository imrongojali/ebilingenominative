﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tam.Ebilling.Domain
{
    public partial class PrintNotur
    {
        public String ReturNumber { get; set; }
        public String TaxNumber { get; set; }
        public DateTime PostingDate { get; set; }
        public DateTime CaDate { get; set; }
        public String CustomerName { get; set; }
        public String CustomerStreet { get; set; }
        public String CustomerCity { get; set; }
        public String CustomerNPWP { get; set; }
        public bool IsDormitory { get; set; }
        public String TransactionName { get; set; }
        public Int32 Qty { get; set; }
        public Decimal UnitPrice { get; set; }
        public Decimal TotalPrice { get; set; }
        public Decimal TotalDormitory { get; set; }
        public String Name_Dormitory { get; set; }
        public Decimal TotalAmount { get; set; }
        public Decimal TotalVat { get; set; }
        //public String Users { get; set; }
        //public String Position { get; set; }
    }
}
