﻿using Agit.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tam.Ebilling.Domain
{
    public partial class ReportNotaRetur
    {
      
        public string row { get; set; }
        public string NomorNotaRetur { get; set; }
        public DateTime TglRetur { get; set; }
        public string NomorCreditAdvice { get; set; }
        public string NomorFakturPajakRetur { get; set; }
        public string NomorDaRetur { get; set; }
        public DateTime TglFakturPajakRetur { get; set; }
        public string CustomerNpwp { get; set; }
        public string CustomerName { get; set; }
        public decimal dpp { get; set; }
        public decimal ppn { get; set; }
        public string businessUnit { get; set; }

        //for filter only
        public DateTime doc_date { get; set; }
        public string business_unit { get; set; }
        public int transaction_type_id { get; set; }
        public string invoice_number { get; set; }
        public string customer_group { get; set; }
    }
}
