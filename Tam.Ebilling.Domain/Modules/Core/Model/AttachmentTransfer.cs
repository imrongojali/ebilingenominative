﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Tam.Ebilling.Domain
{
    [Table("tb_r_attachment_transfer")]
    public partial class AttachmentTransfer
    {
        [Key]
        public long ID { get; set; }
        public string invoiceNumber { get; set; }
        public int trans_type{ get; set; }
        public string businessUnit { get; set; }
        public DateTime doc_date { get; set; }
        public string currency { get; set; }
        public decimal total_amount { get; set; }
        public DateTime payment_date { get; set; }
        public decimal total_payment { get; set; }
        public string attachment { get; set; }
        public bool status_payment { get; set; }
        public DateTime created_date { get; set; }
        public string created_by { get; set; }
        public DateTime modified_date { get; set; }
        public string modified_by { get; set; }
        public int row_status { get; set; }
        public string Status { get; set; }
    }
}