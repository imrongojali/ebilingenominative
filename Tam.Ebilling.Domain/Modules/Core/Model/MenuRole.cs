﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Tam.Ebilling.Domain
{
    [Table("tb_r_menu_role")]
    public partial class MenuRole
    {
        [Key]
        public int internal_id { get; set; }
        public int menu_id { get; set; }
        public string level_code { get; set; }
    }
}
