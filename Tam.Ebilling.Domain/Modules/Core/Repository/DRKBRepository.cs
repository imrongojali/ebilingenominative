﻿using Kendo.Mvc.UI;
using System.Data;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Collections;
using Kendo.Mvc.Extensions;

namespace Tam.Ebilling.Domain
{
    public partial interface IDRKBRepository : IDapperRepository<DRKB>
    {
        DataSourceResult GetViewDataSourceResult(DataSourceRequest request, string customer_group, object parameter = null);
        DataSourceResult GetDRKBList(DataSourceRequest request, string customer_group);
        DataSourceResult GetDataPDFResult(DataSourceRequest request, string customer_group, object parameter = null);
        DataSourceResult getchkDRKB(DataSourceRequest request, string customer_group, String[] id);
        DataSourceResult GetDRKBch(DataSourceRequest request, string customer_group, String[] id, string rowid, bool rowall);
        DataSourceResult GetChekDRKBList(DataSourceRequest request, string customer_group, string id);
        DataSourceResult GetCompany(DataSourceRequest request);
    }

    public partial class DRKBRepository : DapperRepository<DRKB>, IDRKBRepository
    {
        public DRKBRepository(IDbConnection connection, IDbTransaction transaction)
            : base(connection, transaction)
        {
        }

        public DataSourceResult GetCompany(DataSourceRequest request)
        {

            var fetchquery = @"select * from udf_GetCompany ('{0}')";

            var query = string.Format(fetchquery, "");

            var genericDataTableQuery = new GenericDataSourceQuery(Connection, request);
            var output = genericDataTableQuery.GetDataFromUDF<MasterCompany>(query);

            return output;
        }
        
        public DataSourceResult GetViewDataSourceResult(DataSourceRequest request, string customer_group, object parameter = null)
        {

            var fetchquery = @"select * from udf_DRKB ('{0}')";

            var query = string.Format(fetchquery, customer_group);

            var genericDataTableQuery = new GenericDataSourceQuery(Connection, request);
            var output = genericDataTableQuery.GetData<DRKB>(query);

            return output;
        }

        public DataSourceResult GetChekDRKBList(DataSourceRequest request, string customer_group, string id)
        {
            string[] list = id.Split(',');
            var idTemp = new ArrayList();

            foreach (string d in list)
            {
                idTemp.Add("'" + d + "'");
            }
            var param = string.Join(",", idTemp.ToArray());
            var fetchquery = @"select * from udf_DRKB ('{0}') where cast(id as varchar(50)) in (" + param.ToString() + ")";

            var query = string.Format(fetchquery, customer_group);

            var genericDataTableQuery = new GenericDataSourceQuery(Connection, request);
            var output = genericDataTableQuery.GetData<DRKB>(query);

            return output;
        }

        public DataSourceResult getchkDRKB(DataSourceRequest request, string customer_group, String[] id)
        {
            var idTemp = new ArrayList();
            foreach(String d in id)
            {
                idTemp.Add("'" + d + "'");
            }
            var param = string.Join(",", idTemp.ToArray());

            var query = "select * from udf_DRKB ('" + customer_group + "') where id in ("+ param.ToString() +")";
            var genericDataTableQuery = new GenericDataSourceQuery(Connection, request);
            var output = genericDataTableQuery.GetData<DRKB>(query);

            return output;
        }
        
        public DataSourceResult GetDataPDFResult(DataSourceRequest request, string customer_group, object parameter = null)
        {
            var fetchquery = @"select * from udf_DRKB ('{0}')";

            var query = string.Format(fetchquery, customer_group);

            var genericDataTableQuery = new GenericDataSourceQuery(Connection, request);
            var output = genericDataTableQuery.GetData<DRKB>(query);

            return output;
        }

        public DataSourceResult GetDRKBList(DataSourceRequest request, string customer_group)
        {

            var gridBinder = new GridBinder(request);
            var filters = gridBinder.GetFilterDescriptor();

            // var sorting = gridBinder.SortInfo.Member.HasValue() ? string.Format("{0} {1}", gridBinder.SortInfo.Member, gridBinder.SortInfo.Direction) : "";
            var sorting = gridBinder.GetSortDescriptorArry();

             var p = new DynamicParameters();
            p.Add("page", gridBinder.PageNumber, DbType.Int32, ParameterDirection.Input);
            p.Add("pageSize", gridBinder.PageSize, DbType.Int32, ParameterDirection.Input);
            p.Add("sorts", sorting);
            p.Add("filters", filters);
            p.Add("customer_group", customer_group);
            p.Add("totalRecords", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return new DataSourceResult
            {

                Data = Connection.Query<DRKB>("usp_DRKBData_List", p, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure).ToList(),
                Total = p.Get<int>("totalRecords")
            };

        }

        public DataSourceResult GetDRKBch(DataSourceRequest request, string customer_group, String[] id, string rowid, bool rowall)
        {
            var gridBinder = new GridBinder(request);
            var filters = gridBinder.GetFilterDescriptor();
            string[] list;
            var idTemp = new ArrayList();
            if (rowall==true)
            {
              list = id;
            }
            else
            {
               list = rowid.Split(',');
            }
            foreach (String d in list)
            {
                idTemp.Add("'" + d + "'");
            }
            var param = string.Join(",", idTemp.ToArray());

            var p = new DynamicParameters();
            p.Add("filters", filters);
            p.Add("allch", param);
            p.Add("customer_group", customer_group);
            return new DataSourceResult
            {

                Data = Connection.Query<DRKB>("usp_GetChkDRKB", p, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure).ToList(),
                Total = 0
            };

        }

    }
}
