﻿using Kendo.Mvc.UI;
using System.Data;
using Dapper;
using Tam.Ebilling.Domain.Modules.Core.Model;
using System.Collections.Generic;
using System.Linq;

namespace Tam.Ebilling.Domain
{
    public partial interface IAttachmentRepository : IDapperRepository<Attachment>
    {
        DataSourceResult GetDataSourceResult(DataSourceRequest request, object parameter = null);
        List<AttachmentSpDebitAdvice> GetSpDebitAdvice(DataSourceRequest request, string daNumber, string user
            , string deliveryDate);
        List<SpDeliveryNote> GetSpDeliveryNote(DataSourceRequest request, string daNumber, string user);
        List<AttachmentSpDebitAdvice> GetSpCreditAdvice(DataSourceRequest request, string caNumber, string user);
        List<SpDeliveryNote> GetSpCreditDeliveryNote(DataSourceRequest request, string daNumber, string user);
    }

    public partial class AttachmentRepository : DapperRepository<Attachment>, IAttachmentRepository
    {
        public AttachmentRepository(IDbConnection connection, IDbTransaction transaction)
            : base(connection, transaction)
        {
        }

        public DataSourceResult GetDataSourceResult(DataSourceRequest request, object parameter = null)
        {
            var genericDataTableQuery = new GenericDataSourceQuery(Connection, request);
            var output = genericDataTableQuery.GetData<Attachment>();

            return output;
        }

        public List<AttachmentSpDebitAdvice> GetSpDebitAdvice(DataSourceRequest request, string daNumber, string user
            , string deliveryDate)
        {
            var genericDataTableQUery = new GenericDataSourceQuery(Connection, request);
            var parameters = new Dictionary<string, object>();
            parameters.Add("da_number", daNumber);
            parameters.Add("user", user);
            parameters.Add("delivery_date", deliveryDate); // Todo: Remove Hardcoded 

            var output = Connection.Query<AttachmentSpDebitAdvice>("usp_Print_AttachmentSpDebitAdvice", parameters, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);

            return output.ToList();
        }

        public List<AttachmentSpDebitAdvice> GetSpCreditAdvice(DataSourceRequest request, string caNumber, string user)
        {
            var genericDataTableQUery = new GenericDataSourceQuery(Connection, request);
            var parameters = new Dictionary<string, object>();
            parameters.Add("ca_number", caNumber);
            parameters.Add("user", user);

            var output = Connection.Query<AttachmentSpDebitAdvice>("usp_Print_AttachmentSpCreditAdvice", parameters, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);

            return output.ToList();
        }

        public List<SpDeliveryNote> GetSpDeliveryNote(DataSourceRequest request, string daNumber, string user)
        {
            var genericDataTableQUery = new GenericDataSourceQuery(Connection, request);
            var parameters = new Dictionary<string, object>();
            parameters.Add("da_number", daNumber);
            parameters.Add("user", user);

            var output = Connection.Query<SpDeliveryNote>("usp_Print_DeliveryNoteSpDebitAdvice", parameters, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);

            return output.ToList();
        }

        public List<SpDeliveryNote> GetSpCreditDeliveryNote(DataSourceRequest request, string daNumber, string user)
        {
            var genericDataTableQUery = new GenericDataSourceQuery(Connection, request);
            var parameters = new Dictionary<string, object>();
            parameters.Add("da_number", daNumber);
            parameters.Add("user", user);

            var output = Connection.Query<SpDeliveryNote>("usp_Print_DeliveryNoteSpCreditAdvice", parameters, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);

            return output.ToList();
        }
    }
}
