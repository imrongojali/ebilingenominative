﻿using Kendo.Mvc.UI;
using System.Data;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Collections;

namespace Tam.Ebilling.Domain
{
    public partial interface IWHTRepository : IDapperRepository<WHT>
    {
        DataSourceResult GetViewDataSourceResult(DataSourceRequest request, string customer_group, object parameter = null);
        DataSourceResult GetDataPDFResult(DataSourceRequest request, string customer_group, object parameter = null);
        List<WHTTaxArticle> GetWHTTaxArticle( string customer_group);
        List<WHTMAPCode> GetWHTMAPCode(string customer_group);
        DataSourceResult getchkwht(DataSourceRequest request, string customer_group, Guid[] id);
        DataSourceResult GetChekWHTList(DataSourceRequest request, string customer_group, string id);
    }

    public partial class WHTRepository : DapperRepository<WHT>, IWHTRepository
    {
        public WHTRepository(IDbConnection connection, IDbTransaction transaction)
            : base(connection, transaction)
        {
        }

        public DataSourceResult GetViewDataSourceResult(DataSourceRequest request, string customer_group, object parameter = null)
        {
            var fetchquery = @"select * from udf_WHT ('{0}')";

            var query = string.Format(fetchquery, customer_group);

            var genericDataTableQuery = new GenericDataSourceQuery(Connection, request);
            var output = genericDataTableQuery.GetData<WHT>(query);

            return output;
        }

        public DataSourceResult GetChekWHTList(DataSourceRequest request, string customer_group, string id)
        {
            string[] list = id.Split(',');
            var idTemp = new ArrayList();

            foreach (string d in list)
            {
                idTemp.Add("'" + d + "'");
            }
            var param = string.Join(",", idTemp.ToArray());
            var fetchquery = @"select * from udf_WHT ('{0}') where cast(id as varchar(50)) in (" + param.ToString() + ")";

            var query = string.Format(fetchquery, customer_group);

            var genericDataTableQuery = new GenericDataSourceQuery(Connection, request);
            var output = genericDataTableQuery.GetData<WHT>(query);

            return output;
        }

        public DataSourceResult getchkwht(DataSourceRequest request, string customer_group, Guid[] id)
        {
            var idTemp = new ArrayList();
            foreach(Guid d in id)
            {
                idTemp.Add("'" + d + "'");
            }
            var param = string.Join(",", idTemp.ToArray());

            var query = "select * from udf_WHT ('" + customer_group + "') where id in ("+ param.ToString() +")";
            var genericDataTableQuery = new GenericDataSourceQuery(Connection, request);
            var output = genericDataTableQuery.GetData<WHT>(query);

            return output;
        }
        
        public DataSourceResult GetDataPDFResult(DataSourceRequest request, string customer_group, object parameter = null)
        {
            var fetchquery = @"select * from udf_WHT ('{0}')";

            var query = string.Format(fetchquery, customer_group);

            var genericDataTableQuery = new GenericDataSourceQuery(Connection, request);
            var output = genericDataTableQuery.GetData<WHT>(query);

            return output;
        }

        public List<WHTTaxArticle> GetWHTTaxArticle(string customer_group)
        {
            var fetchquery = string.Empty;
            var query = string.Empty;

            if (customer_group == null)
            {
                fetchquery = @"SELECT * FROM vw_WHTTaxArticle order by config_value asc";
                query = string.Format(fetchquery);
            }
            else
            {
                fetchquery = @"SELECT * FROM vw_WHTTaxArticle order by config_value asc";
                query = string.Format(fetchquery, customer_group);
            }
            var output = Connection.Query<WHTTaxArticle>(query).ToList();

            return output;
        }

        public List<WHTMAPCode> GetWHTMAPCode(string customer_group)
        {
            var fetchquery = string.Empty;
            var query = string.Empty;

            if (customer_group == null)
            {
                fetchquery = @"SELECT * FROM vw_WHTMAPCode order by config_text asc";
                query = string.Format(fetchquery);
            }
            else
            {
                fetchquery = @"SELECT * FROM vw_WHTMAPCode order by config_text asc";
                query = string.Format(fetchquery, customer_group);
            }
            var output = Connection.Query<WHTMAPCode>(query).ToList();

            return output;
        }
    }
}
