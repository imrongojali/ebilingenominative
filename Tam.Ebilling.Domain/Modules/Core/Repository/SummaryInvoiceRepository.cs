﻿
using Kendo.Mvc.UI;
using System.Data;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Text;

namespace Tam.Ebilling.Domain
{
    public partial interface ISummaryInvoiceRepository: IDapperRepository<SummaryInvoiceHistory>
    {
        DataSourceResult GetViewDataSourceResult(DataSourceRequest request, string customer_group, string userType);
        void RecordHistory(string dateFrom, string dateTo, string businessType, string customerId, string user);
    }

    public partial class SummaryInvoiceRepository : DapperRepository<SummaryInvoiceHistory>, ISummaryInvoiceRepository
    {
        public SummaryInvoiceRepository(IDbConnection connection, IDbTransaction transaction)
            : base(connection, transaction)
        {
        }
        
        public DataSourceResult GetViewDataSourceResult(DataSourceRequest request, string customer_group, string userType)
        {
            StringBuilder fetchquery = new StringBuilder(@"select id
                , date_from as dateFrom
                , date_to as dateTo
                , business_type as businessType
                , customer_group as customerGroup
                , (select top 1 customer_name from tb_m_customer_mapping m where m.customer_group = i.customer_group) as customerGroupDesc
                , created_by
                , created_date
                from tb_r_history_summary_invoice i
                where 1=1 ");
            if (!string.IsNullOrEmpty(customer_group) && userType != "Internal")
            {
                fetchquery.Append(" and customer_group = '{0}' ");
            }

            string query = string.Format(fetchquery.ToString(), customer_group);

            var genericDataTableQuery = new GenericDataSourceQuery(Connection, request);
            var output = genericDataTableQuery.GetData<SummaryInvoiceHistory>(query);

            return output;
        }

        public void RecordHistory(string dateFrom, string dateTo, string businessType, string customerId, string user)
        {

            var parameters = new Dictionary<string, object>();
            parameters.Add("dateFrom", dateFrom);
            parameters.Add("dateTo", dateTo);
            parameters.Add("businessType", businessType);
            parameters.Add("customerGroup", customerId);
            parameters.Add("user", user);

            Connection.Query<Invoice>("usp_RecordDownloadSummaryInvoice", parameters, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);

        }
    }
}
