﻿using Agit.Mvc;
using Dapper;
using Kendo.Mvc.UI;
using System.Collections.Generic;
using System.Data;

namespace Tam.Ebilling.Domain
{
    public partial interface IConfigRepository : IDapperRepository<Config>
    {
        DataSourceResult GetDataSourceResult(DataSourceRequest request, object parameter = null);
    }

    public partial class ConfigRepository : DapperRepository<Config>, IConfigRepository
    {
        public ConfigRepository(IDbConnection connection, IDbTransaction transaction)
            : base(connection, transaction)
        {
        }

        public DataSourceResult GetDataSourceResult(DataSourceRequest request, object parameter = null)
        {
            var genericDataTableQuery = new GenericDataSourceQuery(Connection, request);
            var output = genericDataTableQuery.GetData<Config>();

            return output;
        }

    }
}
