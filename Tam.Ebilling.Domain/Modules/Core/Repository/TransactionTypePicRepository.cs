﻿using Kendo.Mvc.UI;
using System.Data;
using Dapper;
using System.Collections.Generic;

namespace Tam.Ebilling.Domain
{
    public partial interface ITransactionTypePicRepository : IDapperRepository<TransactionTypePIC>
    {
        DataSourceResult GetDataSourceResult(DataSourceRequest request);
    }

    public partial class TransactionTypePicRepository : DapperRepository<TransactionTypePIC>, ITransactionTypePicRepository
    {
        public TransactionTypePicRepository(IDbConnection connection, IDbTransaction transaction)
            : base(connection, transaction)
        {
        }

        public DataSourceResult GetDataSourceResult(DataSourceRequest request)
        {
            var genericDataTableQuery = new GenericDataSourceQuery(Connection, request);
            var output = genericDataTableQuery.GetData<TransactionTypePIC>();

            return output;
        }
    }
}
