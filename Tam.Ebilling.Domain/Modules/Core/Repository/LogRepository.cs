﻿using Agit.Domain;
using Agit.Mvc;
using Kendo.Mvc.UI;
using System.Data;
using Dapper;
using System.Collections.Generic;

namespace Tam.Ebilling.Domain
{
    public partial interface ILogRepository : IDapperRepository<ApplicationLog>
    {
        DataSourceResult GetDataSourceResult(DataSourceRequest request, object parameter = null);
        int Add(ApplicationLog item, string[] columns);
    }

    public partial class LogRepository : DapperRepository<ApplicationLog>, ILogRepository
    {
        public LogRepository(IDbConnection connection, IDbTransaction transaction)
            : base(connection, transaction)
        {
        }

        public int Add(ApplicationLog item, string[] columns)
        {
            var parameters = (object)Mapping(item);

            columns = new string[] { "username", "ip", "browser", "message_type", "message_description", "message_location", "created_date", "created_by", "row_status" };

            return Connection.Execute(DynamicQuery.GetInsertQuery(TableName, parameters, "app_log_id", columns), parameters, Transaction);
        }

        public DataSourceResult GetDataSourceResult(DataSourceRequest request, object parameter = null)
        {
            var genericDataTableQuery = new GenericDataSourceQuery(Connection, request);
            var output = genericDataTableQuery.GetData<ApplicationLog>();

            return output;
        }
    }
}
