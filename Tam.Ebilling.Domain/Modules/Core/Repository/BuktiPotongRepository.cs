﻿
using Kendo.Mvc.UI;
using System.Data;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Collections;

namespace Tam.Ebilling.Domain
{
    public partial interface IBuktiPotongRepository : IDapperRepository<BuktiPotong>
    {
        DataSourceResult GetViewDataSourceResult(DataSourceRequest request, string invoiceType, string customerGroup, string notifFilter = null);
        List<BuktiPotong> GetBuktiPotong(string invoiceNumbers, string customerGroup);
        void UpdateBuktiPotong(IDbTransaction transaction, BuktiPotong buktiPotong, string user);
        void insertBuktiPotong(IDbTransaction transaction, BuktiPotong buktiPotong, string user);
        List<BuktiPotong> GetDueDatedBuktiPotong(string deadline);
        BuktiPotong GetSingleBuktiPotong(string invoiceNumber);
        void SubmitWHTSubmission(IDbTransaction transaction, string invoice_number, string user);
        void DeleteUnSubmittedWHTSubmission(IDbTransaction transaction, string invoice_number);

        List<ViewTaxTransactionCode> ViewTaxTransactionCode();

        decimal GetTarifNPWP(string transactionTypeCode);
        //int CheckNomorBuktiPotongWHT(string nomorBuktiPotong); 
        string GetTransactionCode(string invoiceNumber);
        //string GetTaxArticle(string transactionTypeCode);

        List<BuktiPotong> CheckTaxArticle(string invoiceNumber);
        //List<string> GetInvoiceNumbers(string nomorBuktiPotong);

        void UpdatePdfWht(IDbTransaction transaction, string nomorBuktiPotong, string type, string url);
        void UpdatePdfWhtTMS(IDbTransaction transaction, string invoiceNumber, string url);

        List<BuktiPotong> GetBuktiPotongByNomorBuktiPotong(string nomorBuktiPotong, string customerGroup);
        DataSourceResult GetChekBuktiPotongList(DataSourceRequest request, string invoiceType, string customer_group, string id);
        DataSourceResult getchkbuktipotong(DataSourceRequest request, string invoiceType, string customer_group, String[] id);
    }

    public partial class BuktiPotongRepository : DapperRepository<BuktiPotong>, IBuktiPotongRepository
    {
        public BuktiPotongRepository(IDbConnection connection, IDbTransaction transaction)
            : base(connection, transaction)
        {
        }

        public DataSourceResult GetViewDataSourceResult(DataSourceRequest request, string invoiceType, string customerGroup, string notifFilter = null)
        {
            //string query = string.Format(@"select df.customer_name, df.customer_group, bt.upload_id, SUBSTRING(bt.upload_id,1, 8) ref_no
            //                ,b.* from BES.dbo.tb_r_bukti_potong b 
            //                inner join BES.dbo.udf_Invoice('{0}', '{1}') df 
            //                on b.invoice_number = df.invoice_number
            //                left join BES.dbo.tb_r_bukti_potong_attachment bt on bt.invoice_number = b.invoice_number
            //                where 1=1 ", invoiceType, customerGroup);
            string chk2 = "";

            if (notifFilter != null)
            {
                if (notifFilter != "")
                {
                    //chk2 = " AND PARSENAME(REPLACE(([TAM_EFAKTUR].dbo.[udf_GetColorFromParam]('EBILL_DWHTS',df.invoice_number,b.invoice_date,b.flag_potong_pajak,b.wht_pdf_22,b.wht_pdf_23,b.wht_pdf_42,b.business_unit,null)),'|', '.'), 1) like '%" + notifFilter + "%' ";
                    //chk2 = " AND wht_prepaid_status like '%" + notifFilter + "%' ";
                }
            }

            #region Old
            //       string queryOld = string.Format(
            //           @"SELECT
            //                 *
            //           FROM (SELECT
            //               distinct trvo.NamaCustomer customer_name,
            //               tmcmn.customer_group,
            //               '' upload_id,
            //               0 CountRelatedInvoice,
            //               trvo.danumber invoice_number,
            //               0 materai_amount,
            //               trvo.JumlahDPP + trvo.JumlahPPN amount_wht,
            //               trvo.TanggalFaktur tgl_payment,
            //               1 is_active,
            //               0 bukti_potong_uploaded,
            //               trvo.CreatedOn submit_date,
            //               trvo.CreatedBy submit_by,
            //               trvo.CreatedOn created_date,
            //               trvo.CreatedBy created_by,
            //               trvo.ModifiedOn modified_date,
            //               trvo.ModifiedBy modified_by,
            //               trvo.RowStatus row_status,
            //               tmjp.TransaksiTypeCode jenis_pajak,
            //               trvo.NomorFakturGabungan nomor_bukti_potong,
            //               trvo.TanggalFaktur tanggal_bukti_potong,
            //               trvo.MasaPajak masa_pajak,
            //               --trvo.TahunPajak tahun_pajak ,
            //               CONCAT(trvo.TahunPajak, '') AS tahun_pajak,
            //               trvo.JumlahDPP jumlah_dpp,
            //               0 tarif,
            //               0 jumlah_pph,
            //               trvo.TanggalFaktur invoice_date,
            //               'N/A' flag_potong_pajak,
            //               tmjp.Description description,
            //               trvo.BusinessUnit business_unit,
            //               trvo.NPWPCustomer no_npwp,
            //               trvo.AlamatLawanTransaksi alamat_npwp,
            //               trvo.NamaCustomer nama_npwp,
            //               '0' wht_pdf_22_status,
            //               '0' wht_pdf_23_status,
            //               '0' wht_pdf_42_status,
            //               '0' wht_pdf_status,
            //               warna.wht_prepaid_status
            //           FROM TAM_EFAKTUR.dbo.TB_R_VATOut AS trvo
            //           LEFT JOIN BES.dbo.tb_m_customer_map_npwp AS tmcmn
            //               ON trvo.NPWPCustomer = tmcmn.customer_npwp
            //           LEFT JOIN TAM_EFAKTUR.dbo.TB_M_JenisPajak AS tmjp
            //               ON trvo.Id = trvo.KDJenisTransaksiID
            //           OUTER APPLY (SELECT  [TAM_EFAKTUR].dbo.[udf_GetColorFromParam]('TMS_VATOUTSALES',
            //											  trvo.NomorFakturGabungan,
            //											  trvo.TanggalFaktur,
            //											  trvo.ApprovalStatus,
            //											  CASE ISNULL(trvo.PdfUrl,'') WHEN '' THEN 'Not Exist' ELSE 'Exist' END,trvo.TransitoryNumber,NULL,
            //											  trvo.BusinessUnit,
            //											  NULL,trvo.ApprovalTime,trvo.RecordTime,trvo.CreatedOn,trvo.ModifiedOn,trvo.TransitoryDate,NULL,NULL,NULL,NULL) AS wht_prepaid_status) AS  warna
            //           WHERE trvo.TaxInvoiceStatus IN ('Normal', 'Normal-Pengganti')
            //           AND trvo.BusinessUnit IN ('Lexus-Workshop', 'Others')
            //           AND trvo.ApprovalStatus = 'Approval Sukses'
            //           AND ISNULL(trvo.NPWPCustomer, '') != '00.000.000-0.000.000'
            //           UNION ALL
            //           SELECT
            //               distinct a.customer_name,
            //               a.customer_group,
            //               '' upload_id,
            //               0 CountRelatedInvoice,
            //               a.da_no invoice_number,
            //               0 materai_amount,
            //               a.total_amount amount_wht,
            //               a.doc_date,
            //               1 is_active,
            //               a.notur_uploaded bukti_potong_uploaded,
            //               a.created_date submit_date,
            //               a.created_by submit_by,
            //               a.created_date created_date,
            //               a.created_by created_by,
            //               a.modified_date modified_date,
            //               a.modified_by modified_by,
            //               0 row_status,
            //               a.JenisPajak jenis_pajak,
            //               a.invoice_number nomor_bukti_potong,
            //               a.doc_date tanggal_bukti_potong,
            //               a.MasaPajak masa_pajak,
            //               --a.TahunPajak tahun_pajak ,
            //               CONCAT(a.TahunPajak, '') AS tahun_pajak,
            //               a.dpp jumlah_dpp,
            //               CAST(a.rate_pph AS decimal(18, 2)) AS tarif, 
            //               a.total_amount,
            //               a.doc_date invoice_date,
            //               'N/A' flag_potong_pajak,
            //               '' description,
            //               a.business_unit,
            //               npwp.customer_npwp no_npwp,
            //               a.customer_address alamat_npwp,
            //               a.customer_name nama_npwp,
            //               '0' wht_pdf_22_status,
            //               '0' wht_pdf_23_status,
            //               '0' wht_pdf_42_status,
            //               '0' wht_pdf_status,
            //               warna.wht_prepaid_status
            //           FROM BES.dbo.[udf_Invoice_wht]('{1}') a
            //           LEFT OUTER JOIN BES.dbo.tb_m_customer_map_npwp npwp
            //               ON npwp.customer_group = a.customer_group
            //        OUTER APPLY (SELECT  [TAM_EFAKTUR].dbo.[udf_GetColorFromParam]('EBILL_DWHTS',a.da_no,a.doc_date,b.flag_potong_pajak,b.wht_pdf_22,b.wht_pdf_23,b.wht_pdf_42,b.business_unit,NULL,b.tgl_payment,b.submit_date,b.created_date,b.modified_date,b.tanggal_bukti_potong,NULL,NULL,NULL,NULL)  AS wht_prepaid_status) AS  warna
            //           WHERE a.business_unit IN ('Others')

            //           UNION ALL

            //           SELECT
            //               distinct df.customer_name,
            //               df.customer_group,
            //               (SELECT TOP 1
            //               bt.upload_id
            //               FROM BES.dbo.tb_r_bukti_potong_attachment bt
            //               WHERE bt.invoice_number = df.invoice_number)
            //               AS upload_id,
            //               (SELECT
            //               COUNT(1)
            //               FROM BES.dbo.tb_r_bukti_potong_attachment bt
            //               WHERE bt.invoice_number = df.invoice_number
            //               AND bt.upload_id = (SELECT TOP 1
            //               bt.upload_id
            //               FROM BES.dbo.tb_r_bukti_potong_attachment bt
            //               WHERE bt.invoice_number =
            //               df.invoice_number))
            //               AS
            //               CountRelatedInvoice,
            //               b.invoice_number,
            //               b.materai_amount,
            //               b.amount_wht,
            //               b.tgl_payment,
            //               b.is_active,
            //               b.bukti_potong_uploaded,
            //               b.submit_date,
            //               b.submit_by,
            //               b.created_date,
            //               b.created_by,
            //               b.modified_date,
            //               b.modified_by,
            //               b.row_status,
            //               b.jenis_pajak,
            //               b.nomor_bukti_potong,
            //               b.tanggal_bukti_potong,
            //               b.masa_pajak,
            //               CONCAT(b.tahun_pajak, '') AS tahun_pajak,
            //               b.jumlah_dpp,
            //               CAST(b.tarif * 100 AS decimal(18, 2)) AS tarif,
            //               --b.tarif,  
            //               b.jumlah_pph,
            //               b.invoice_date,
            //               --CONCAT(b.flag_potong_pajak, '') AS 'ab', 
            //               --ISNULL(b.flag_potong_pajak, 'N/A') as 'ab',
            //               CASE WHEN b.flag_potong_pajak = 'Y' THEN 'Y'
            //ELSE CASE WHEN b.flag_potong_pajak = 'N' THEN 'N'
            //ELSE CASE WHEN b.flag_potong_pajak IS NULL THEN 'N/A'
            //END
            //END
            //END as 'ab',
            //               b.description,
            //               b.business_unit,
            //               CASE
            //               WHEN b.no_npwp IS NOT NULL THEN b.no_npwp
            //               ELSE df.customer_code
            //               END
            //               AS no_npwp,
            //               CASE
            //               WHEN b.alamat_npwp IS NOT NULL THEN b.alamat_npwp
            //               ELSE df.customer_group
            //               END
            //               AS alamat_npwp,
            //               CASE
            //               WHEN b.nama_npwp IS NOT NULL THEN b.nama_npwp
            //               ELSE df.customer_name
            //               END
            //               AS nama_npwp,
            //               --b.no_npwp,  
            //               --b.alamat_npwp,  
            //               --b.nama_npwp,  
            //               CASE
            //               WHEN b.wht_pdf_22 IS NOT NULL THEN '1'
            //               ELSE '0'
            //               END
            //               AS wht_pdf_22_status,
            //               CASE
            //               WHEN b.wht_pdf_23 IS NOT NULL THEN '1'
            //               ELSE '0'
            //               END
            //               AS wht_pdf_23_status,
            //               CASE
            //               WHEN b.wht_pdf_42 IS NOT NULL THEN '1'
            //               ELSE '0'
            //               END
            //               AS wht_pdf_42_status,
            //               CASE
            //               WHEN b.wht_pdf_22 IS NOT NULL THEN '1'
            //               ELSE CASE
            //                   WHEN b.wht_pdf_23 IS NOT NULL THEN '1'
            //                   ELSE CASE
            //                       WHEN b.wht_pdf_42 IS NOT NULL THEN '1'
            //                       ELSE '0'
            //                       END
            //                   END
            //               END
            //               AS wht_pdf_status,
            //               [TAM_EFAKTUR].dbo.[udf_GetColorFromParam]('EBILL_DWHTS', df.invoice_number, b.invoice_date, b.flag_potong_pajak, b.wht_pdf_22, b.wht_pdf_23, b.wht_pdf_42, b.business_unit, NULL,b.tgl_payment,b.submit_date,b.created_date,b.modified_date,b.tanggal_bukti_potong,NULL,NULL,NULL,NULL) AS wht_prepaid_status
            //           FROM BES.dbo.tb_r_bukti_potong b
            //           LEFT OUTER JOIN BES.dbo.udf_Invoice('{0}', '{1}') df
            //               ON b.invoice_number = df.invoice_number) a
            //           WHERE 1 = 1 " + chk2, invoiceType, customerGroup);
            #endregion

            string query = string.Format(
                        @"SELECT
                            *
                        FROM (SELECT DISTINCT
                            trvo.NamaCustomer customer_name,
                            --tmcmn.customer_group,
                            (select top 1 customer_group from BES.dbo.tb_m_customer_map_npwp AS tmcmn WHERE trvo.NPWPCustomer = tmcmn.customer_npwp )  customer_group,

                            '' upload_id,
                            0 CountRelatedInvoice,
                            trvo.danumber invoice_number,
                            CAST(b.materai_amount AS decimal(18, 2)) as materai_amount,
                            CAST(b.amount_wht AS decimal(18, 2)) as amount_wht,
                            b.tgl_payment,
                            1 is_active,
                            0 bukti_potong_uploaded,
                            trvo.CreatedOn submit_date,
                            trvo.CreatedBy submit_by,
                            trvo.CreatedOn created_date,
                            trvo.CreatedBy created_by,
                            trvo.ModifiedOn modified_date,
                            trvo.ModifiedBy modified_by,
                            trvo.RowStatus row_status,
                            b.jenis_pajak,
                            b.nomor_bukti_potong,
                            b.tanggal_bukti_potong,
                            b.masa_pajak,
                            CONCAT(b.tahun_pajak, '') AS tahun_pajak,
                            CAST(b.jumlah_dpp AS decimal(18, 2)) as jumlah_dpp,
                            CAST(b.tarif * 100 AS decimal(18, 2)) AS tarif,
                            CAST(b.jumlah_pph AS varchar(max)) as jumlah_pph,
                            isnull(b.invoice_date,trvo.TanggalFaktur) as invoice_date, 
                            CASE WHEN b.flag_potong_pajak = 'Y' THEN 'Y'
					                            ELSE CASE WHEN b.flag_potong_pajak = 'N' THEN 'N'
					                            ELSE CASE WHEN b.flag_potong_pajak IS NULL THEN 'N/A'
					                            END
					                            END
					                            END as 'flag_potong_pajak',
                            b.description,
                            trvo.BusinessUnit business_unit,
                            trvo.NPWPCustomer no_npwp,
                            trvo.AlamatLawanTransaksi alamat_npwp,
                            trvo.NamaCustomer nama_npwp,
                            CASE
                            WHEN b.wht_pdf_22 IS NOT NULL THEN '1'
                            ELSE '0'
                            END
                            AS wht_pdf_22_status,
                            CASE
                            WHEN b.wht_pdf_23 IS NOT NULL THEN '1'
                            ELSE '0'
                            END
                            AS wht_pdf_23_status,
                            CASE
                            WHEN b.wht_pdf_42 IS NOT NULL THEN '1'
                            ELSE '0'
                            END
                            AS wht_pdf_42_status,
                            CASE
                            WHEN b.wht_pdf_22 IS NOT NULL THEN '1'
                            ELSE CASE
                                WHEN b.wht_pdf_23 IS NOT NULL THEN '1'
                                ELSE CASE
                                    WHEN b.wht_pdf_42 IS NOT NULL THEN '1'
                                    ELSE '0'
                                    END
                                END
                            END
                            AS wht_pdf_status,
                            warna.wht_prepaid_status
                        FROM TAM_EFAKTUR.dbo.TB_R_VATOut AS trvo
                        LEFT JOIN BES.dbo.tb_r_bukti_potong b ON trvo.DANumber = b.invoice_number
                        --LEFT JOIN BES.dbo.tb_m_customer_map_npwp AS tmcmn ON trvo.NPWPCustomer = tmcmn.customer_npwp
                        LEFT JOIN TAM_EFAKTUR.dbo.TB_M_JenisPajak AS tmjp ON trvo.Id = trvo.KDJenisTransaksiID
                        OUTER APPLY (
						
						SELECT [TAM_EFAKTUR].dbo.[udf_GetColorFromParam]('EBILL_DWHTS',b.invoice_number,b.invoice_date,b.flag_potong_pajak,b.wht_pdf_22,b.wht_pdf_23,b.wht_pdf_42,b.business_unit,NULL,b.tgl_payment,b.submit_date,b.created_date,b.modified_date,b.tanggal_bukti_potong,NULL,NULL,NULL,NULL) AS wht_prepaid_status
							
							) AS warna
                        WHERE trvo.TaxInvoiceStatus IN ('Normal', 'Normal-Pengganti')
                            AND trvo.BusinessUnit IN ('Lexus-Workshop', 'Others')
                            AND trvo.ApprovalStatus = 'Approval Sukses'
                            AND ISNULL(trvo.NPWPCustomer, '') != '00.000.000-0.000.000'  
                            AND ISNULL(trvo.NPWPCustomer, '') != '00.000.000.0-000.000' ) a
                        WHERE 1 = 1 " + chk2, invoiceType, customerGroup);

            var genericDataTableQuery = new GenericDataSourceQuery(Connection, request);
            var output = genericDataTableQuery.GetData<BuktiPotong>(query);

            return output;
        }

        public List<BuktiPotong> GetBuktiPotong(string invoiceNumbers, string customerGroup)
        {
            #region Old
            //string queryOld = string.Format(
            //    @"SELECT
            //          *
            //        FROM (SELECT
            //          distinct trvo.NamaCustomer customer_name,
            //          tmcmn.customer_group,
            //          trvo.danumber invoice_number,
            //          0 materai_amount,
            //          trvo.JumlahDPP + trvo.JumlahPPN amount_wht,
            //          trvo.TanggalFaktur tgl_payment,
            //          1 is_active,
            //          0 bukti_potong_uploaded,
            //          trvo.CreatedOn submit_date,
            //          trvo.CreatedBy submit_by,
            //          trvo.CreatedOn created_date,
            //          trvo.CreatedBy created_by,
            //          trvo.ModifiedOn modified_date,
            //          trvo.ModifiedBy modified_by,
            //          trvo.RowStatus row_status,
            //          tmjp.TransaksiTypeCode jenis_pajak,
            //          trvo.NomorFakturGabungan nomor_bukti_potong,
            //          trvo.TanggalFaktur tanggal_bukti_potong,
            //          trvo.MasaPajak masa_pajak,
            //          trvo.TahunPajak tahun_pajak,
            //          trvo.JumlahDPP jumlah_dpp,
            //          0 tarif,
            //          0 jumlah_pph,
            //          trvo.TanggalFaktur invoice_date,
            //          '' flag_potong_pajak,
            //          tmjp.Description description,
            //          trvo.NPWPCustomer no_npwp,
            //          trvo.AlamatLawanTransaksi alamat_npwp,
            //          trvo.NamaCustomer nama_npwp,
            //          NULL AS wht_pdf_22,
            //          NULL AS wht_pdf_23,
            //          NULL AS wht_pdf_42,
            //          --'0' wht_pdf_status,
            //          trvo.BusinessUnit business_unit,
            //          NULL AS tax_article
            //        FROM TAM_EFAKTUR.dbo.TB_R_VATOut AS trvo
            //        LEFT JOIN BES.dbo.tb_m_customer_map_npwp AS tmcmn
            //          ON trvo.NPWPCustomer = tmcmn.customer_npwp
            //        LEFT JOIN TAM_EFAKTUR.dbo.TB_M_JenisPajak AS tmjp
            //          ON trvo.Id = trvo.KDJenisTransaksiID

            //        WHERE trvo.TaxInvoiceStatus IN ('Normal', 'Normal-Pengganti')
            //        AND trvo.BusinessUnit IN ('Lexus-Workshop', 'Others')
            //        AND trvo.ApprovalStatus = 'Approval Sukses'
            //        AND ISNULL(trvo.NPWPCustomer, '') != '00.000.000-0.000.000'

            //        UNION ALL

            //        SELECT
            //          distinct a.customer_name,
            //          a.customer_group,
            //          --'' upload_id,
            //          --0 CountRelatedInvoice,
            //          a.da_no invoice_number,
            //          0 materai_amount,
            //          a.total_amount amount_wht,
            //          a.doc_date,
            //          1 is_active,
            //          a.notur_uploaded bukti_potong_uploaded,
            //          a.created_date submit_date,
            //          a.created_by submit_by,
            //          a.created_date created_date,
            //          a.created_by created_by,
            //          a.modified_date modified_date,
            //          a.modified_by modified_by,
            //          0 row_status,
            //          a.JenisPajak jenis_pajak,
            //          a.invoice_number nomor_bukti_potong,
            //          a.doc_date tanggal_bukti_potong,
            //          a.MasaPajak masa_pajak,
            //          a.TahunPajak tahun_pajak,
            //          a.dpp jumlah_dpp,
            //          CAST(a.rate_pph AS decimal(18, 2)) AS tarif,
            //          a.total_amount,
            //          a.doc_date invoice_date,
            //          '' flag_potong_pajak,
            //          '' description,
            //          --a.business_unit,
            //          npwp.customer_npwp no_npwp,
            //          a.customer_address alamat_npwp,
            //          a.customer_name nama_npwp,
            //          NULL AS wht_pdf_22,
            //          NULL AS wht_pdf_23,
            //          NULL AS wht_pdf_42,
            //          --'0' wht_pdf_22_status,
            //          --'0' wht_pdf_23_status,
            //          --'0' wht_pdf_42_status,
            //          --'0' wht_pdf_status,
            //          a.business_unit,
            //          NULL AS tax_article
            //        FROM BES.dbo.[udf_Invoice_wht]('{1}') a
            //        LEFT OUTER JOIN BES.dbo.tb_m_customer_map_npwp npwp
            //          ON npwp.customer_group = a.customer_group

            //        WHERE a.business_unit IN ('Others')

            //        UNION ALL

            //        SELECT
            //          distinct i.customer_group,
            //          i.customer_name,
            //          b.*
            //        FROM BES.dbo.tb_r_bukti_potong b
            //        LEFT JOIN BES.dbo.udf_Invoice('1', '') i
            //          ON b.invoice_number = i.invoice_number) a
            //        WHERE invoice_number IN ({0})", invoiceNumbers, customerGroup);

            #endregion

            string query = string.Format(
                @"SELECT
                      *
                    FROM (SELECT
                    DISTINCT
                      trvo.NamaCustomer customer_name,
                      --tmcmn.customer_group,
                      (select top 1 customer_group from BES.dbo.tb_m_customer_map_npwp AS tmcmn WHERE trvo.NPWPCustomer = tmcmn.customer_npwp )  customer_group,

                      trvo.danumber invoice_number,
                      b.materai_amount,
                      b.amount_wht,
                      b.tgl_payment,
                      1 is_active,
                      0 bukti_potong_uploaded,
                      trvo.CreatedOn submit_date,
                      trvo.CreatedBy submit_by,
                      trvo.CreatedOn created_date,
                      trvo.CreatedBy created_by,
                      trvo.ModifiedOn modified_date,
                      trvo.ModifiedBy modified_by,
                      trvo.RowStatus row_status,
                      b.jenis_pajak,
                      b.nomor_bukti_potong,
                      b.tanggal_bukti_potong,
                      b.masa_pajak,
                      b.tahun_pajak,
                      b.jumlah_dpp,
                      CAST(b.tarif * 100 AS decimal(18, 2)) AS tarif,
                      b.jumlah_pph,
                      trvo.TanggalFaktur invoice_date,
                      b.flag_potong_pajak,
                      b.description,
                      trvo.NPWPCustomer no_npwp,
                      trvo.AlamatLawanTransaksi alamat_npwp,
                      trvo.NamaCustomer nama_npwp,
                      b.wht_pdf_22,
                      b.wht_pdf_23,
                      b.wht_pdf_42,
                      trvo.BusinessUnit business_unit,
                      b.tax_article
                    FROM TAM_EFAKTUR.dbo.TB_R_VATOut AS trvo
                    LEFT JOIN BES.dbo.tb_r_bukti_potong b ON trvo.DANumber = b.invoice_number
                    --LEFT JOIN BES.dbo.tb_m_customer_map_npwp AS tmcmn ON trvo.NPWPCustomer = tmcmn.customer_npwp
                    LEFT JOIN TAM_EFAKTUR.dbo.TB_M_JenisPajak AS tmjp ON trvo.Id = trvo.KDJenisTransaksiID 
                    WHERE trvo.TaxInvoiceStatus IN ('Normal', 'Normal-Pengganti')
                    AND trvo.BusinessUnit IN ('Lexus-Workshop', 'Others')
                    AND trvo.ApprovalStatus = 'Approval Sukses'
                    AND ISNULL(trvo.NPWPCustomer, '') != '00.000.000-0.000.000'
                    AND ISNULL(trvo.NPWPCustomer, '') != '00.000.000.0-000.000'

                    --UNION ALL

                    --SELECT DISTINCT
                    --    a.customer_name,
                    --    a.customer_group,
                    --    a.da_no invoice_number,
                    --    0 materai_amount,
                    --    b.amount_wht,
                    --    a.doc_date,
                    --    1 is_active,
                    --    a.notur_uploaded bukti_potong_uploaded,
                    --    a.created_date submit_date,
                    --    a.created_by submit_by,
                    --    a.created_date created_date,
                    --    a.created_by created_by,
                    --    a.modified_date modified_date,
                    --    a.modified_by modified_by,
                    --    0 row_status,
                    --    b.jenis_pajak,
                    --    b.nomor_bukti_potong,
                    --    b.tanggal_bukti_potong,
                    --    b.masa_pajak,
                    --    CONCAT(b.tahun_pajak, '') AS tahun_pajak,
                    --    b.jumlah_dpp,
                    --    b.tarif,
                    --    b.jumlah_pph, 
                    --    a.doc_date invoice_date,
                    --    b.flag_potong_pajak,
                    --    b.description,
                    --    npwp.customer_npwp no_npwp,
                    --    a.customer_address alamat_npwp,
                    --    a.customer_name nama_npwp,
                    --    b.wht_pdf_22,
                    --    b.wht_pdf_23,
                    --    b.wht_pdf_42,
                    --    a.business_unit,
                    --    NULL AS tax_article
                    --FROM BES.dbo.[udf_Invoice_wht_sub]('{1}') a
                    --LEFT JOIN BES.dbo.tb_r_bukti_potong b
                    --    ON a.da_no = b.invoice_number
                    --LEFT OUTER JOIN BES.dbo.tb_m_customer_map_npwp npwp
                    --    ON npwp.customer_group = a.customer_group
                    --WHERE a.business_unit IN ('Others')
                    ) a
                     WHERE a.invoice_number IN ({0})", invoiceNumbers, customerGroup);

            var output = Connection.Query<BuktiPotong>(query).ToList();

            return output;
        }

        public List<BuktiPotong> GetBuktiPotongByNomorBuktiPotong(string nomorBuktiPotong, string customerGroup)
        {
            #region Old
            //string queryOld = string.Format(
            //    @"SELECT
            //          *
            //        FROM (SELECT
            //          distinct trvo.NamaCustomer customer_name,
            //          tmcmn.customer_group,
            //          trvo.danumber invoice_number,
            //          0 materai_amount,
            //          trvo.JumlahDPP + trvo.JumlahPPN amount_wht,
            //          trvo.TanggalFaktur tgl_payment,
            //          1 is_active,
            //          0 bukti_potong_uploaded,
            //          trvo.CreatedOn submit_date,
            //          trvo.CreatedBy submit_by,
            //          trvo.CreatedOn created_date,
            //          trvo.CreatedBy created_by,
            //          trvo.ModifiedOn modified_date,
            //          trvo.ModifiedBy modified_by,
            //          trvo.RowStatus row_status,
            //          tmjp.TransaksiTypeCode jenis_pajak,
            //          trvo.NomorFakturGabungan nomor_bukti_potong,
            //          trvo.TanggalFaktur tanggal_bukti_potong,
            //          trvo.MasaPajak masa_pajak,
            //          trvo.TahunPajak tahun_pajak,
            //          trvo.JumlahDPP jumlah_dpp,
            //          0 tarif,
            //          0 jumlah_pph,
            //          trvo.TanggalFaktur invoice_date,
            //          '' flag_potong_pajak,
            //          tmjp.Description description,
            //          trvo.NPWPCustomer no_npwp,
            //          trvo.AlamatLawanTransaksi alamat_npwp,
            //          trvo.NamaCustomer nama_npwp,
            //          NULL AS wht_pdf_22,
            //          NULL AS wht_pdf_23,
            //          NULL AS wht_pdf_42,
            //          --'0' wht_pdf_status,
            //          trvo.BusinessUnit business_unit,
            //          NULL AS tax_article
            //        FROM TAM_EFAKTUR.dbo.TB_R_VATOut AS trvo
            //        LEFT JOIN BES.dbo.tb_m_customer_map_npwp AS tmcmn
            //          ON trvo.NPWPCustomer = tmcmn.customer_npwp
            //        LEFT JOIN TAM_EFAKTUR.dbo.TB_M_JenisPajak AS tmjp
            //          ON trvo.Id = trvo.KDJenisTransaksiID

            //        WHERE trvo.TaxInvoiceStatus IN ('Normal', 'Normal-Pengganti')
            //        AND trvo.BusinessUnit IN ('Lexus-Workshop', 'Others')
            //        AND trvo.ApprovalStatus = 'Approval Sukses'
            //        AND ISNULL(trvo.NPWPCustomer, '') != '00.000.000-0.000.000'

            //        UNION ALL

            //        SELECT
            //          distinct a.customer_name,
            //          a.customer_group,
            //          --'' upload_id,
            //          --0 CountRelatedInvoice,
            //          a.da_no invoice_number,
            //          0 materai_amount,
            //          a.total_amount amount_wht,
            //          a.doc_date,
            //          1 is_active,
            //          a.notur_uploaded bukti_potong_uploaded,
            //          a.created_date submit_date,
            //          a.created_by submit_by,
            //          a.created_date created_date,
            //          a.created_by created_by,
            //          a.modified_date modified_date,
            //          a.modified_by modified_by,
            //          0 row_status,
            //          a.JenisPajak jenis_pajak,
            //          a.invoice_number nomor_bukti_potong,
            //          a.doc_date tanggal_bukti_potong,
            //          a.MasaPajak masa_pajak,
            //          a.TahunPajak tahun_pajak,
            //          a.dpp jumlah_dpp,
            //          CAST(a.rate_pph AS decimal(18, 2)) AS tarif,
            //          a.total_amount,
            //          a.doc_date invoice_date,
            //          '' flag_potong_pajak,
            //          '' description,
            //          --a.business_unit,
            //          npwp.customer_npwp no_npwp,
            //          a.customer_address alamat_npwp,
            //          a.customer_name nama_npwp,
            //          NULL AS wht_pdf_22,
            //          NULL AS wht_pdf_23,
            //          NULL AS wht_pdf_42,
            //          --'0' wht_pdf_22_status,
            //          --'0' wht_pdf_23_status,
            //          --'0' wht_pdf_42_status,
            //          --'0' wht_pdf_status,
            //          a.business_unit,
            //          NULL AS tax_article
            //        FROM BES.dbo.[udf_Invoice_wht]('{1}') a
            //        LEFT OUTER JOIN BES.dbo.tb_m_customer_map_npwp npwp
            //          ON npwp.customer_group = a.customer_group

            //        WHERE a.business_unit IN ('Others')

            //        UNION ALL

            //        SELECT
            //          distinct i.customer_group,
            //          i.customer_name,
            //          b.*
            //        FROM BES.dbo.tb_r_bukti_potong b
            //        LEFT JOIN BES.dbo.udf_Invoice('1', '') i
            //          ON b.invoice_number = i.invoice_number) a
            //        WHERE nomor_bukti_potong IN ('{0}') 
            //              AND (wht_pdf_22 is not null or wht_pdf_23 is not null or wht_pdf_42 is not null)", nomorBuktiPotong, customerGroup);
            #endregion

            string query = string.Format(
                @"SELECT
                  *
                FROM (SELECT DISTINCT
                  trvo.NamaCustomer customer_name,
                  --tmcmn.customer_group,
                  (select top 1 customer_group from BES.dbo.tb_m_customer_map_npwp AS tmcmn WHERE trvo.NPWPCustomer = tmcmn.customer_npwp )  customer_group,

                  trvo.danumber invoice_number,
                  b.materai_amount,
                  b.amount_wht,
                  b.tgl_payment,
                  1 is_active,
                  0 bukti_potong_uploaded,
                  trvo.CreatedOn submit_date,
                  trvo.CreatedBy submit_by,
                  trvo.CreatedOn created_date,
                  trvo.CreatedBy created_by,
                  trvo.ModifiedOn modified_date,
                  trvo.ModifiedBy modified_by,
                  trvo.RowStatus row_status, 
                  b.jenis_pajak,
                  b.nomor_bukti_potong,
                  b.tanggal_bukti_potong,
                  b.masa_pajak,
                  b.tahun_pajak,
                  b.jumlah_dpp,
                  CAST(b.tarif * 100 AS decimal(18, 2)) AS tarif,
                  b.jumlah_pph, 
                  trvo.TanggalFaktur invoice_date,
                  b.flag_potong_pajak,
                  b.description,
                  trvo.NPWPCustomer no_npwp,
                  trvo.AlamatLawanTransaksi alamat_npwp,
                  trvo.NamaCustomer nama_npwp,
                  b.wht_pdf_22,
                  b.wht_pdf_23,
                  b.wht_pdf_42,
                  trvo.BusinessUnit business_unit,
                  b.tax_article
                FROM TAM_EFAKTUR.dbo.TB_R_VATOut AS trvo
                LEFT JOIN BES.dbo.tb_r_bukti_potong b ON trvo.DANumber = b.invoice_number
                --LEFT JOIN BES.dbo.tb_m_customer_map_npwp AS tmcmn ON trvo.NPWPCustomer = tmcmn.customer_npwp
                LEFT JOIN TAM_EFAKTUR.dbo.TB_M_JenisPajak AS tmjp ON trvo.Id = trvo.KDJenisTransaksiID 
                WHERE trvo.TaxInvoiceStatus IN ('Normal', 'Normal-Pengganti')
	                AND trvo.BusinessUnit IN ('Lexus-Workshop', 'Others')
	                AND trvo.ApprovalStatus = 'Approval Sukses'
	                AND ISNULL(trvo.NPWPCustomer, '') != '00.000.000-0.000.000'
	                AND ISNULL(trvo.NPWPCustomer, '') != '00.000.000.0-000.000'

                --UNION ALL

                --SELECT DISTINCT
                --    a.customer_name,
                --    a.customer_group, 
                --    a.da_no invoice_number,
                --    0 materai_amount,
                --    b.amount_wht,
                --    a.doc_date,
                --    1 is_active,
                --    a.notur_uploaded bukti_potong_uploaded,
                --    a.created_date submit_date,
                --    a.created_by submit_by,
                --    a.created_date created_date,
                --    a.created_by created_by,
                --    a.modified_date modified_date,
                --    a.modified_by modified_by,
                --    0 row_status, 
                --    b.jenis_pajak,
                --    b.nomor_bukti_potong,
                --    b.tanggal_bukti_potong,
                --    b.masa_pajak,
                --    b.tahun_pajak,
                --    b.jumlah_dpp,
                --    b.tarif,
                --    b.jumlah_pph, 
                --    a.doc_date invoice_date,
                --    b.flag_potong_pajak,
                --    b.description,
                --    npwp.customer_npwp no_npwp,
                --    a.customer_address alamat_npwp,
                --    a.customer_name nama_npwp,
                --    b.wht_pdf_22,
                --    b.wht_pdf_23,
                --    b.wht_pdf_42,
                --    a.business_unit,
                --    NULL AS tax_article
                --FROM BES.dbo.[udf_Invoice_wht_sub]('{1}') a
                --LEFT JOIN BES.dbo.tb_r_bukti_potong b ON a.da_no = b.invoice_number
                --LEFT OUTER JOIN BES.dbo.tb_m_customer_map_npwp npwp ON npwp.customer_group = a.customer_group 
                --WHERE a.business_unit IN ('Others')
                ) a
                WHERE nomor_bukti_potong IN ('{0}')
	                AND (wht_pdf_22 IS NOT NULL OR wht_pdf_23 IS NOT NULL OR wht_pdf_42 IS NOT NULL)", nomorBuktiPotong, customerGroup);

            var output = Connection.Query<BuktiPotong>(query).ToList();

            return output;
        }

        public List<ViewTaxTransactionCode> ViewTaxTransactionCode()
        {
            string query = string.Format(
                @"SELECT
                    transaksitypecode AS tax_transaction_code,
                    TransaksiTypeDescShort AS description
                FROM [TAM_EFAKTUR].dbo.tb_m_jenispajak
                WHERE espttype = 'PPh Badan'");

            var output = Connection.Query<ViewTaxTransactionCode>(query).ToList();

            return output;
        }

        public decimal GetTarifNPWP(string transactionTypeCode)
        {
            string query = string.Format(
                @"SELECT
                  --CAST(REPLACE(TarifNPWP, ',', '.') AS DECIMAL(18, 2))
                  TarifNPWP
                FROM [TAM_EFAKTUR].[dbo].[tb_m_jenispajak]
                WHERE transaksitypecode = '{0}' ", transactionTypeCode);

            var output = Connection.Query<string>(query).FirstOrDefault();

            if (output == null)
            {
                output = "0";
            }

            return decimal.Parse(output.Replace(",", "."));
        }

        //public int CheckNomorBuktiPotongWHT(string nomorBuktiPotong)
        //{
        //    string query = string.Format(
        //        @"SELECT Count(*) 
        //            FROM TAM_EFAKTUR.dbo.TB_R_WHT 
        //            WHERE NomorBuktiPotong = '{0}'
        //             AND ApprovalStatus = 'Approval Sukses' ", nomorBuktiPotong);

        //    var output = Connection.Query<int>(query).FirstOrDefault();

        //    return output;
        //}

        public string GetTransactionCode(string invoiceNumber)
        {
            string query = string.Format(
            //@"SELECT
            //      *
            //    FROM (SELECT

            //      tmjp.TransaksiTypeCode jenis_pajak,
            //      trvo.DANumber invoice_number

            //    FROM TAM_EFAKTUR.dbo.TB_R_VATOut AS trvo
            //    LEFT JOIN TAM_EFAKTUR.dbo.TB_M_JenisPajak AS tmjp
            //      ON trvo.Id = trvo.KDJenisTransaksiID

            //    UNION ALL

            //    SELECT
            //      b.jenis_pajak,
            //      b.invoice_number
            //    FROM BES.dbo.tb_r_bukti_potong b
            //    LEFT JOIN BES.dbo.udf_Invoice('1', '') i
            //      ON b.invoice_number = i.invoice_number) A
            //    WHERE invoice_number IN ('{0}')", invoiceNumber); 

            @"SELECT
                b.jenis_pajak,
                b.invoice_number
                FROM BES.dbo.tb_r_bukti_potong b
                --LEFT JOIN BES.dbo.[udf_Invoice_wht_sub]('') i
                --ON b.invoice_number = i.da_no 
                WHERE b.invoice_number IN ('{0}')", invoiceNumber);

            var output = Connection.Query<string>(query).FirstOrDefault();

            return output;
        }

        //public string GetTaxArticle(string transactionCode)
        //{
        //    string query = string.Format(
        //        @"SELECT
        //          Article
        //        FROM [TAM_EFAKTUR].[dbo].[TB_M_JenisPajak]
        //        WHERE transaksitypecode = '{0}' ", transactionCode);

        //    var output = Connection.Query<string>(query).FirstOrDefault();

        //    return output;
        //}

        public List<BuktiPotong> CheckTaxArticle(string invoiceNumber)
        {
            string query = string.Format(
                @"SELECT *
                    FROM BES.dbo.tb_r_bukti_potong b 
                    WHERE b.invoice_number IN ({0})", invoiceNumber);

            var output = Connection.Query<BuktiPotong>(query).ToList();

            return output;
        }

        public string GetDetailNPWP(string npwp)
        {
            string query = string.Format(
                @"SELECT
                    tarifnpwp
                FROM [TAM_EFAKTUR].[dbo].[tb_m_jenispajak]
                WHERE transaksitypecode = '{0}'", npwp);

            var output = Connection.Query<string>(query).FirstOrDefault();

            return output;
        }

        public BuktiPotong GetSingleBuktiPotong(string invoiceNumber)
        {
            #region Old
            //string queryOld = string.Format(
            //    @"SELECT
            //          *
            //        FROM (SELECT
            //          distinct trvo.NamaCustomer customer_name,
            //          tmcmn.customer_group,
            //          trvo.danumber invoice_number,
            //          0 materai_amount,
            //          trvo.JumlahDPP + trvo.JumlahPPN amount_wht,
            //          trvo.TanggalFaktur tgl_payment,
            //          1 is_active,
            //          0 bukti_potong_uploaded,
            //          trvo.CreatedOn submit_date,
            //          trvo.CreatedBy submit_by,
            //          trvo.CreatedOn created_date,
            //          trvo.CreatedBy created_by,
            //          trvo.ModifiedOn modified_date,
            //          trvo.ModifiedBy modified_by,
            //          trvo.RowStatus row_status,
            //          tmjp.TransaksiTypeCode jenis_pajak,
            //          trvo.NomorFakturGabungan nomor_bukti_potong,
            //          trvo.TanggalFaktur tanggal_bukti_potong,
            //          trvo.MasaPajak masa_pajak,
            //          trvo.TahunPajak tahun_pajak,
            //          trvo.JumlahDPP jumlah_dpp,
            //          0 tarif,
            //          0 jumlah_pph,
            //          trvo.TanggalFaktur invoice_date,
            //          '' flag_potong_pajak,
            //          tmjp.Description description,
            //          trvo.NPWPCustomer no_npwp,
            //          trvo.AlamatLawanTransaksi alamat_npwp,
            //          trvo.NamaCustomer nama_npwp,
            //          '0' wht_pdf_22,
            //          '0' wht_pdf_23,
            //          '0' wht_pdf_42,
            //          --'0' wht_pdf_status,
            //          trvo.BusinessUnit business_unit,
            //          NULL AS tax_article
            //        FROM TAM_EFAKTUR.dbo.TB_R_VATOut AS trvo
            //        LEFT JOIN BES.dbo.tb_m_customer_map_npwp AS tmcmn
            //          ON trvo.NPWPCustomer = tmcmn.customer_npwp
            //        LEFT JOIN TAM_EFAKTUR.dbo.TB_M_JenisPajak AS tmjp
            //          ON trvo.Id = trvo.KDJenisTransaksiID

            //        WHERE trvo.TaxInvoiceStatus IN ('Normal', 'Normal-Pengganti')
            //        AND trvo.BusinessUnit IN ('Lexus-Workshop', 'Others')
            //        AND trvo.ApprovalStatus = 'Approval Sukses'
            //        AND ISNULL(trvo.NPWPCustomer, '') != '00.000.000-0.000.000'

            //        UNION ALL
            //        SELECT
            //         distinct  a.customer_name,
            //          a.customer_group,
            //          --'' upload_id,
            //          --0 CountRelatedInvoice,
            //          a.da_no invoice_number,
            //          0 materai_amount,
            //          a.total_amount amount_wht,
            //          a.doc_date,
            //          1 is_active,
            //          a.notur_uploaded bukti_potong_uploaded,
            //          a.created_date submit_date,
            //          a.created_by submit_by,
            //          a.created_date created_date,
            //          a.created_by created_by,
            //          a.modified_date modified_date,
            //          a.modified_by modified_by,
            //          0 row_status,
            //          a.JenisPajak jenis_pajak,
            //          a.invoice_number nomor_bukti_potong,
            //          a.doc_date tanggal_bukti_potong,
            //          a.MasaPajak masa_pajak,
            //          a.TahunPajak tahun_pajak,
            //          a.dpp jumlah_dpp,
            //          CAST(a.rate_pph AS decimal(18, 2)) AS tarif,
            //          a.total_amount,
            //          a.doc_date invoice_date,
            //          '' flag_potong_pajak,
            //          '' description,
            //          --a.business_unit,
            //          npwp.customer_npwp no_npwp,
            //          a.customer_address alamat_npwp,
            //          a.customer_name nama_npwp,
            //          '0' wht_pdf_22_status,
            //          '0' wht_pdf_23_status,
            //          '0' wht_pdf_42_status,
            //          --'0' wht_pdf_status,
            //          a.business_unit,
            //          NULL AS tax_article
            //        FROM BES.dbo.[udf_Invoice_wht]('') a
            //        LEFT OUTER JOIN BES.dbo.tb_m_customer_map_npwp npwp
            //          ON npwp.customer_group = a.customer_group

            //        WHERE a.business_unit IN ('Others')

            //        UNION ALL

            //        SELECT
            //          distinct i.customer_group,
            //          i.customer_name,
            //          b.*
            //        FROM BES.dbo.tb_r_bukti_potong b
            //        LEFT JOIN BES.dbo.udf_Invoice('1', '') i
            //          ON b.invoice_number = i.invoice_number) A
            //        WHERE invoice_number = '{0}'", invoiceNumber);
            #endregion

            string query = string.Format(
                @"SELECT
                  *
                FROM (SELECT DISTINCT
                  trvo.NamaCustomer customer_name,
                  --tmcmn.customer_group,
                  (select top 1 customer_group from BES.dbo.tb_m_customer_map_npwp AS tmcmn WHERE trvo.NPWPCustomer = tmcmn.customer_npwp )  customer_group,

                  trvo.danumber invoice_number,
                  b.materai_amount,
                  b.amount_wht,
                  b.tgl_payment,
                  1 is_active,
                  0 bukti_potong_uploaded,
                  trvo.CreatedOn submit_date,
                  trvo.CreatedBy submit_by,
                  trvo.CreatedOn created_date,
                  trvo.CreatedBy created_by,
                  trvo.ModifiedOn modified_date,
                  trvo.ModifiedBy modified_by,
                  trvo.RowStatus row_status,
                  b.jenis_pajak,
                  b.nomor_bukti_potong,
                  b.tanggal_bukti_potong,
                  b.masa_pajak,
                  b.tahun_pajak,
                  b.jumlah_dpp,
                  CAST(b.tarif * 100 AS decimal(18, 2)) AS tarif,
                  b.jumlah_pph, 
                  trvo.TanggalFaktur invoice_date, 
                  b.flag_potong_pajak,
                  b.description, 
                  trvo.NPWPCustomer no_npwp,
                  trvo.AlamatLawanTransaksi alamat_npwp,
                  trvo.NamaCustomer nama_npwp,
                  b.wht_pdf_22,
                  b.wht_pdf_23,
                  b.wht_pdf_42,
                  trvo.BusinessUnit business_unit,
                  b.tax_article
                FROM TAM_EFAKTUR.dbo.TB_R_VATOut AS trvo
                LEFT JOIN BES.dbo.tb_r_bukti_potong b ON trvo.DANumber = b.invoice_number
                --LEFT JOIN BES.dbo.tb_m_customer_map_npwp AS tmcmn ON trvo.NPWPCustomer = tmcmn.customer_npwp
                LEFT JOIN TAM_EFAKTUR.dbo.TB_M_JenisPajak AS tmjp ON trvo.Id = trvo.KDJenisTransaksiID
                WHERE trvo.TaxInvoiceStatus IN ('Normal', 'Normal-Pengganti')
	                AND trvo.BusinessUnit IN ('Lexus-Workshop', 'Others')
	                AND trvo.ApprovalStatus = 'Approval Sukses'
	                AND ISNULL(trvo.NPWPCustomer, '') != '00.000.000-0.000.000'
	                AND ISNULL(trvo.NPWPCustomer, '') != '00.000.000.0-000.000'

                --UNION ALL

                --SELECT DISTINCT
                --    a.customer_name,
                --    a.customer_group, 
                --    a.da_no invoice_number,
                --    0 materai_amount,
                --    b.amount_wht,
                --    a.doc_date,
                --    1 is_active,
                --    a.notur_uploaded bukti_potong_uploaded,
                --    a.created_date submit_date,
                --    a.created_by submit_by,
                --    a.created_date created_date,
                --    a.created_by created_by,
                --    a.modified_date modified_date,
                --    a.modified_by modified_by,
                --    0 row_status, 
                --    b.jenis_pajak,
                --    b.nomor_bukti_potong,
                --    b.tanggal_bukti_potong,
                --    b.masa_pajak,
                --    CONCAT(b.tahun_pajak, '') AS tahun_pajak,
                --    b.jumlah_dpp,
                --    b.tarif,
                --    b.jumlah_pph,
                --    a.doc_date invoice_date,
                --    b.flag_potong_pajak,
                --    b.description,
                --    npwp.customer_npwp no_npwp,
                --    a.customer_address alamat_npwp,
                --    a.customer_name nama_npwp,
                --    b.wht_pdf_22,
                --    b.wht_pdf_23,
                --    b.wht_pdf_42,
                --    a.business_unit,
                --    NULL AS tax_article
                --FROM BES.dbo.[udf_Invoice_wht_sub]('') a
                --LEFT JOIN BES.dbo.tb_r_bukti_potong b ON a.da_no = b.invoice_number
                --LEFT OUTER JOIN BES.dbo.tb_m_customer_map_npwp npwp ON npwp.customer_group = a.customer_group 
                --WHERE a.business_unit IN ('Others')
                ) a
                WHERE a.invoice_number = '{0}'", invoiceNumber);

            var output = Connection.ExecuteScalar<BuktiPotong>(query);

            return output;
        }

        public List<BuktiPotong> GetDueDatedBuktiPotong(string deadline)
        {
            #region
            //string queryOld = string.Format(
            //    @"SELECT
            //          *
            //        FROM (SELECT
            //          distinct trvo.NamaCustomer customer_name,
            //          tmcmn.customer_group,
            //          trvo.danumber invoice_number,
            //          0 materai_amount,
            //          trvo.JumlahDPP + trvo.JumlahPPN amount_wht,
            //          trvo.TanggalFaktur tgl_payment,
            //          1 is_active,
            //          0 bukti_potong_uploaded,
            //          trvo.CreatedOn submit_date,
            //          trvo.CreatedBy submit_by,
            //          trvo.CreatedOn created_date,
            //          trvo.CreatedBy created_by,
            //          trvo.ModifiedOn modified_date,
            //          trvo.ModifiedBy modified_by,
            //          trvo.RowStatus row_status,
            //          tmjp.TransaksiTypeCode jenis_pajak,
            //          trvo.NomorFakturGabungan nomor_bukti_potong,
            //          trvo.TanggalFaktur tanggal_bukti_potong,
            //          trvo.MasaPajak masa_pajak,
            //          trvo.TahunPajak tahun_pajak,
            //          trvo.JumlahDPP jumlah_dpp,
            //          0 tarif,
            //          0 jumlah_pph,
            //          trvo.TanggalFaktur invoice_date,
            //          '' flag_potong_pajak,
            //          tmjp.Description description,
            //          trvo.NPWPCustomer no_npwp,
            //          trvo.AlamatLawanTransaksi alamat_npwp,
            //          trvo.NamaCustomer nama_npwp,
            //          '0' wht_pdf_22,
            //          '0' wht_pdf_23,
            //          '0' wht_pdf_42,
            //          --'0' wht_pdf_status,
            //          trvo.BusinessUnit business_unit,
            //          NULL AS tax_article
            //        FROM TAM_EFAKTUR.dbo.TB_R_VATOut AS trvo
            //        LEFT JOIN BES.dbo.tb_m_customer_map_npwp AS tmcmn
            //          ON trvo.NPWPCustomer = tmcmn.customer_npwp
            //        LEFT JOIN TAM_EFAKTUR.dbo.TB_M_JenisPajak AS tmjp
            //          ON trvo.Id = trvo.KDJenisTransaksiID

            //        WHERE trvo.TaxInvoiceStatus IN ('Normal', 'Normal-Pengganti')
            //        AND trvo.BusinessUnit IN ('Lexus-Workshop', 'Others')
            //        AND trvo.ApprovalStatus = 'Approval Sukses'
            //        AND ISNULL(trvo.NPWPCustomer, '') != '00.000.000-0.000.000'

            //        UNION ALL
            //        SELECT
            //         distinct  a.customer_name,
            //          a.customer_group,
            //          --'' upload_id,
            //          --0 CountRelatedInvoice,
            //          a.da_no invoice_number,
            //          0 materai_amount,
            //          a.total_amount amount_wht,
            //          a.doc_date,
            //          1 is_active,
            //          a.notur_uploaded bukti_potong_uploaded,
            //          a.created_date submit_date,
            //          a.created_by submit_by,
            //          a.created_date created_date,
            //          a.created_by created_by,
            //          a.modified_date modified_date,
            //          a.modified_by modified_by,
            //          0 row_status,
            //          a.JenisPajak jenis_pajak,
            //          a.invoice_number nomor_bukti_potong,
            //          a.doc_date tanggal_bukti_potong,
            //          a.MasaPajak masa_pajak,
            //          a.TahunPajak tahun_pajak,
            //          a.dpp jumlah_dpp,
            //          CAST(a.rate_pph AS decimal(18, 2)) AS tarif,
            //          a.total_amount,
            //          a.doc_date invoice_date,
            //          '' flag_potong_pajak,
            //          '' description,
            //          --a.business_unit,
            //          npwp.customer_npwp no_npwp,
            //          a.customer_address alamat_npwp,
            //          a.customer_name nama_npwp,
            //          '0' wht_pdf_22_status,
            //          '0' wht_pdf_23_status,
            //          '0' wht_pdf_42_status,
            //          --'0' wht_pdf_status,
            //          a.business_unit,
            //          NULL AS tax_article
            //        FROM BES.dbo.[udf_Invoice_wht]('') a
            //        LEFT OUTER JOIN BES.dbo.tb_m_customer_map_npwp npwp
            //          ON npwp.customer_group = a.customer_group
            //        WHERE a.business_unit IN ('Others')
            //        UNION ALL
            //        SELECT
            //          distinct i.customer_group,
            //          i.customer_name,
            //          b.*
            //        FROM BES.dbo.tb_r_bukti_potong b
            //        LEFT JOIN BES.dbo.udf_Invoice('1', '') i
            //          ON b.invoice_number = i.invoice_number) a
            //        WHERE CASE
            //          WHEN (YEAR(tgl_payment) - YEAR(GETDATE())) <= -1 THEN ((ABS((YEAR(tgl_payment) - YEAR(GETDATE()))) * 12) - MONTH(tgl_payment)) + MONTH(GETDATE())
            //          ELSE MONTH(GETDATE()) - MONTH(tgl_payment)
            //        END > 1
            //        AND is_active = {0}
            //        AND bukti_potong_uploaded = 0", deadline);
            #endregion

            string query = string.Format(
                @"SELECT
                  *
                FROM (SELECT DISTINCT
                  trvo.NamaCustomer customer_name,
                  --tmcmn.customer_group,
                  (select top 1 customer_group from BES.dbo.tb_m_customer_map_npwp AS tmcmn WHERE trvo.NPWPCustomer = tmcmn.customer_npwp )  customer_group,

                  trvo.danumber invoice_number,
                  b.materai_amount,
                  b.amount_wht,
                  b.tgl_payment,
                  b.is_active,
                  0 bukti_potong_uploaded,
                  trvo.CreatedOn submit_date,
                  trvo.CreatedBy submit_by,
                  trvo.CreatedOn created_date,
                  trvo.CreatedBy created_by,
                  trvo.ModifiedOn modified_date,
                  trvo.ModifiedBy modified_by,
                  trvo.RowStatus row_status,
                  b.jenis_pajak,
                  b.nomor_bukti_potong,
                  b.tanggal_bukti_potong,
                  b.masa_pajak,
                  b.tahun_pajak,
                  b.jumlah_dpp,
                  CAST(b.tarif * 100 AS decimal(18, 2)) AS tarif,
                  b.jumlah_pph,
                  trvo.TanggalFaktur invoice_date,
                  b.flag_potong_pajak,
                  b.description,
                  trvo.NPWPCustomer no_npwp,
                  trvo.AlamatLawanTransaksi alamat_npwp,
                  trvo.NamaCustomer nama_npwp,
                  CASE
                    WHEN b.wht_pdf_22 IS NOT NULL THEN '1'
                    ELSE '0'
                  END
                  AS wht_pdf_22_status,
                  CASE
                    WHEN b.wht_pdf_23 IS NOT NULL THEN '1'
                    ELSE '0'
                  END
                  AS wht_pdf_23_status,
                  CASE
                    WHEN b.wht_pdf_42 IS NOT NULL THEN '1'
                    ELSE '0'
                  END
                  AS wht_pdf_42_status,
                  trvo.BusinessUnit business_unit,
                  b.tax_article
                FROM TAM_EFAKTUR.dbo.TB_R_VATOut AS trvo
                LEFT JOIN BES.dbo.tb_r_bukti_potong b ON trvo.DANumber = b.invoice_number
                --LEFT JOIN BES.dbo.tb_m_customer_map_npwp AS tmcmn ON trvo.NPWPCustomer = tmcmn.customer_npwp
                LEFT JOIN TAM_EFAKTUR.dbo.TB_M_JenisPajak AS tmjp ON trvo.Id = trvo.KDJenisTransaksiID
                WHERE trvo.TaxInvoiceStatus IN ('Normal', 'Normal-Pengganti')
	                AND trvo.BusinessUnit IN ('Lexus-Workshop', 'Others')
	                AND trvo.ApprovalStatus = 'Approval Sukses'
	                AND ISNULL(trvo.NPWPCustomer, '') != '00.000.000-0.000.000'
	                AND ISNULL(trvo.NPWPCustomer, '') != '00.000.000.0-000.000'

                --UNION ALL

                --SELECT DISTINCT
                --    a.customer_name,
                --    a.customer_group,
                --    a.da_no invoice_number,
                --    0 materai_amount,
                --    b.amount_wht,
                --    a.doc_date,
                --    b.is_active,
                --    a.notur_uploaded bukti_potong_uploaded,
                --    a.created_date submit_date,
                --    a.created_by submit_by,
                --    a.created_date created_date,
                --    a.created_by created_by,
                --    a.modified_date modified_date,
                --    a.modified_by modified_by,
                --    0 row_status,
                --    b.jenis_pajak,
                --    b.nomor_bukti_potong,
                --    b.tanggal_bukti_potong,
                --    b.masa_pajak,
                --    b.tahun_pajak,
                --    b.jumlah_dpp,
                --    b.tarif,
                --    b.jumlah_pph,
                --    a.doc_date invoice_date,
                --    b.flag_potong_pajak,
                --    b.description,
                --    npwp.customer_npwp no_npwp,
                --    a.customer_address alamat_npwp,
                --    a.customer_name nama_npwp,
                --    CASE
                --    WHEN b.wht_pdf_22 IS NOT NULL THEN '1'
                --    ELSE '0'
                --    END
                --    AS wht_pdf_22_status,
                --    CASE
                --    WHEN b.wht_pdf_23 IS NOT NULL THEN '1'
                --    ELSE '0'
                --    END
                --    AS wht_pdf_23_status,
                --    CASE
                --    WHEN b.wht_pdf_42 IS NOT NULL THEN '1'
                --    ELSE '0'
                --    END
                --    AS wht_pdf_42_status,
                --    a.business_unit,
                --    NULL AS tax_article
                --FROM BES.dbo.[udf_Invoice_wht_sub]('') a
                --LEFT JOIN BES.dbo.tb_r_bukti_potong b ON a.da_no = b.invoice_number
                --LEFT OUTER JOIN BES.dbo.tb_m_customer_map_npwp npwp ON npwp.customer_group = a.customer_group
                --WHERE a.business_unit IN ('Others')
                ) a
                WHERE CASE
                  WHEN (YEAR(tgl_payment) - YEAR(GETDATE())) <= -1 THEN ((ABS((YEAR(tgl_payment) - YEAR(GETDATE()))) * 12) - MONTH(tgl_payment)) + MONTH(GETDATE())
                  ELSE MONTH(GETDATE()) - MONTH(tgl_payment)
                END > 1
                AND is_active = {0}
                AND bukti_potong_uploaded = 0", deadline);

            var output = Connection.Query<BuktiPotong>(query).ToList();

            return output;
        }

        public void UpdateBuktiPotong(IDbTransaction transaction, BuktiPotong buktiPotong, string user)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("business_unit", buktiPotong.business_unit);
            parameters.Add("no_npwp", buktiPotong.no_npwp);
            parameters.Add("invoice_number", buktiPotong.invoice_number);
            parameters.Add("invoice_date", buktiPotong.invoice_date.ToString("yyyy-MM-dd"));

            parameters.Add("flag_potong_pajak", buktiPotong.flag_potong_pajak);

            parameters.Add("materai_amount", buktiPotong.materai_amount);
            parameters.Add("tgl_payment", buktiPotong.tgl_payment.ToString("yyyy-MM-dd"));
            parameters.Add("nomor_bukti_potong", buktiPotong.nomor_bukti_potong);
            parameters.Add("tanggal_bukti_potong", buktiPotong.tanggal_bukti_potong.ToString("yyyy-MM-dd"));
            parameters.Add("jumlah_dpp", buktiPotong.jumlah_dpp);
            parameters.Add("tarif", buktiPotong.tarif);
            parameters.Add("jumlah_pph", buktiPotong.jumlah_pph);
            parameters.Add("jenis_pajak", buktiPotong.jenis_pajak);
            parameters.Add("description", buktiPotong.description);
            parameters.Add("user", user);
            var output = Connection.Execute("usp_UpdateBuktiPotongUploaded", parameters, transaction, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);
        }

        public void insertBuktiPotong(IDbTransaction transaction, BuktiPotong buktiPotong, string user)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("business_unit", buktiPotong.business_unit);
            parameters.Add("no_npwp", buktiPotong.no_npwp);
            parameters.Add("invoice_number", buktiPotong.invoice_number);
            parameters.Add("invoice_date", buktiPotong.invoice_date.ToString("yyyy-MM-dd"));

            parameters.Add("flag_potong_pajak", buktiPotong.flag_potong_pajak);

            parameters.Add("materai_amount", buktiPotong.materai_amount);
            parameters.Add("tgl_payment", buktiPotong.tgl_payment.ToString("yyyy-MM-dd"));
            parameters.Add("nomor_bukti_potong", buktiPotong.nomor_bukti_potong);
            parameters.Add("tanggal_bukti_potong", buktiPotong.tanggal_bukti_potong.ToString("yyyy-MM-dd"));
            parameters.Add("jumlah_dpp", buktiPotong.jumlah_dpp);
            parameters.Add("tarif", buktiPotong.tarif);
            parameters.Add("jumlah_pph", buktiPotong.jumlah_pph);
            parameters.Add("jenis_pajak", buktiPotong.jenis_pajak);
            parameters.Add("description", buktiPotong.description);
            parameters.Add("user", user);

            var output = Connection.Execute("usp_SaveBuktiPotong", parameters, transaction, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);
        }

        public void SubmitWHTSubmission(IDbTransaction transaction, string invoice_number, string user)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("invoice_number", invoice_number);
            parameters.Add("user", user);
            var output = Connection.Execute("usp_SubmitWHTSubmission", parameters, transaction, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);
        }

        public void DeleteUnSubmittedWHTSubmission(IDbTransaction transaction, string invoice_number)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("invoice_number", invoice_number);
            var output = Connection.Execute("usp_DeleteUnSubmittedWHTSubmission", parameters, transaction, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);
        }

        public void UpdatePdfWht(IDbTransaction transaction, string nomorBuktiPotong, string type, string url)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("nomorBuktiPotong", nomorBuktiPotong);
            parameters.Add("url", url);
            parameters.Add("type", type);
            var output = Connection.Execute("usp_UpdatePdfWht", parameters, transaction, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);
        }
        public void UpdatePdfWhtTMS(IDbTransaction transaction, string invoiceNumber, string url)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("invoiceNumber", invoiceNumber);
            parameters.Add("url", url);
            var output = Connection.Execute("usp_UpdatePdfWhtTMS", parameters, transaction, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);
        }

        //public List<string> GetInvoiceNumbers(string nomorBuktiPotong)
        //{
        //    string query = string.Format(
        //        @"SELECT
        //              *
        //            FROM (SELECT

        //              trvo.NomorFakturGabungan nomor_bukti_potong

        //            FROM TAM_EFAKTUR.dbo.TB_R_VATOut AS trvo

        //            UNION ALL

        //            SELECT
        //              b.invoice_number
        //            FROM BES.dbo.tb_r_bukti_potong b
        //            LEFT JOIN BES.dbo.udf_Invoice('1', '') i
        //              ON b.invoice_number = i.invoice_number) a
        //            WHERE nomor_bukti_potong IN ('{0}')", nomorBuktiPotong);

        //    var output = Connection.Query<string>(query).ToList();

        //    return output;
        //}

        string queryExport = @"
                SELECT *
                FROM (SELECT DISTINCT
                  trvo.NamaCustomer customer_name,
                  --tmcmn.customer_group,
                  (select top 1 customer_group from BES.dbo.tb_m_customer_map_npwp AS tmcmn WHERE trvo.NPWPCustomer = tmcmn.customer_npwp )  customer_group,

                  '' upload_id,
                  0 CountRelatedInvoice,
                  trvo.danumber invoice_number,
                  CAST(b.materai_amount AS decimal(18, 0)) as materai_amount,
                  CAST(b.amount_wht AS decimal(18, 0)) as amount_wht,
                  b.tgl_payment,
                  1 is_active,
                  0 bukti_potong_uploaded,
                  trvo.CreatedOn submit_date,
                  trvo.CreatedBy submit_by,
                  trvo.CreatedOn created_date,
                  trvo.CreatedBy created_by,
                  trvo.ModifiedOn modified_date,
                  trvo.ModifiedBy modified_by,
                  trvo.RowStatus row_status, 
                  b.jenis_pajak,
                  b.nomor_bukti_potong,
                  b.tanggal_bukti_potong,
                  b.masa_pajak,
                  b.tahun_pajak,
                  b.jumlah_dpp,
                  CAST(b.tarif * 100 AS decimal(18, 2)) AS tarif,
                  b.jumlah_pph, 
                  trvo.TanggalFaktur invoice_date,
                  CASE
                    WHEN b.flag_potong_pajak = 'Y' THEN 'Y'
                    ELSE CASE
                        WHEN b.flag_potong_pajak = 'N' THEN 'N'
                        ELSE CASE
                            WHEN b.flag_potong_pajak IS NULL THEN 'N/A'
                          END
                      END
                  END AS 'flag_potong_pajak',
                  b.description,
                  trvo.BusinessUnit business_unit,
                  b.tax_article,
                  trvo.NPWPCustomer no_npwp,
                  trvo.AlamatLawanTransaksi alamat_npwp,
                  trvo.NamaCustomer nama_npwp,
                  b.wht_pdf_22,
                  b.wht_pdf_23,
                  b.wht_pdf_42,
                  CASE
                    WHEN b.wht_pdf_22 IS NOT NULL THEN '1'
                    ELSE CASE
                        WHEN B.wht_pdf_23 IS NOT NULL THEN '1'
                        ELSE CASE
                            WHEN B.wht_pdf_42 IS NOT NULL THEN '1'
                            ELSE '0'
                          END
                      END
                  END
                  AS wht_pdf_status,
                  warna.wht_prepaid_status
                FROM TAM_EFAKTUR.dbo.TB_R_VATOut AS trvo
                LEFT JOIN BES.dbo.tb_r_bukti_potong b ON trvo.DANumber = b.invoice_number
                --LEFT JOIN BES.dbo.tb_m_customer_map_npwp AS tmcmn ON trvo.NPWPCustomer = tmcmn.customer_npwp
                LEFT JOIN TAM_EFAKTUR.dbo.TB_M_JenisPajak AS tmjp ON trvo.Id = trvo.KDJenisTransaksiID
              OUTER APPLY (
						
						SELECT [TAM_EFAKTUR].dbo.[udf_GetColorFromParam]('EBILL_DWHTS',b.invoice_number,b.invoice_date,b.flag_potong_pajak,b.wht_pdf_22,b.wht_pdf_23,b.wht_pdf_42,b.business_unit,NULL,b.tgl_payment,b.submit_date,b.created_date,b.modified_date,b.tanggal_bukti_potong,NULL,NULL,NULL,NULL) AS wht_prepaid_status
							
							) AS warna
                WHERE trvo.TaxInvoiceStatus IN ('Normal', 'Normal-Pengganti')
                AND trvo.BusinessUnit IN ('Lexus-Workshop', 'Others')
                AND trvo.ApprovalStatus = 'Approval Sukses'
                AND ISNULL(trvo.NPWPCustomer, '') != '00.000.000-0.000.000'
                AND ISNULL(trvo.NPWPCustomer, '') != '00.000.000.0-000.000'

                --UNION ALL

                --SELECT DISTINCT
                --    a.customer_name,
                --    a.customer_group,
                --    '' upload_id,
                --    0 CountRelatedInvoice,
                --    a.da_no invoice_number,
                --    0 materai_amount,
                --    b.amount_wht,
                --    a.doc_date,
                --    1 is_active,
                --    a.notur_uploaded bukti_potong_uploaded,
                --    a.created_date submit_date,
                --    a.created_by submit_by,
                --    a.created_date created_date,
                --    a.created_by created_by,
                --    a.modified_date modified_date,
                --    a.modified_by modified_by,
                --    0 row_status,
                --    b.jenis_pajak,
                --    b.nomor_bukti_potong,
                --    b.tanggal_bukti_potong,
                --    b.masa_pajak,
                --    b.tahun_pajak,
                --    b.jumlah_dpp,
                --    b.tarif,
                --    b.jumlah_pph,
                --    a.doc_date invoice_date,
                --    CASE
                --    WHEN b.flag_potong_pajak = 'Y' THEN 'Y'
                --    ELSE CASE
                --        WHEN b.flag_potong_pajak = 'N' THEN 'N'
                --        ELSE CASE
                --            WHEN b.flag_potong_pajak IS NULL THEN 'N/A'
                --            END
                --        END
                --    END AS 'ab',
                --    b.description,
                --    a.business_unit,
                --    NULL AS tax_article,
                --    npwp.customer_npwp no_npwp,
                --    a.customer_address alamat_npwp,
                --    a.customer_name nama_npwp,
                --    b.wht_pdf_22,
                --    b.wht_pdf_23,
                --    b.wht_pdf_42,
                --    CASE
                --    WHEN b.wht_pdf_22 IS NOT NULL THEN '1'
                --    ELSE CASE
                --        WHEN B.wht_pdf_23 IS NOT NULL THEN '1'
                --        ELSE CASE
                --            WHEN B.wht_pdf_42 IS NOT NULL THEN '1'
                --            ELSE '0'
                --            END
                --        END
                --    END
                --    AS wht_pdf_status,
                --    warna.wht_prepaid_status
                --FROM BES.dbo.[udf_Invoice_wht_sub]('{1}') a
                --LEFT JOIN BES.dbo.tb_r_bukti_potong b
                --    ON a.da_no = b.invoice_number
                --LEFT OUTER JOIN BES.dbo.tb_m_customer_map_npwp npwp
                --    ON npwp.customer_group = a.customer_group
                --OUTER APPLY (SELECT
                --    [TAM_EFAKTUR].dbo.[udf_GetColorFromParam]('EBILL_DWHTS',a.da_no,a.doc_date,b.flag_potong_pajak,b.wht_pdf_22,b.wht_pdf_23,b.wht_pdf_42,b.business_unit,NULL,b.tgl_payment,b.submit_date,b.created_date,b.modified_date,b.tanggal_bukti_potong,NULL,NULL,NULL,NULL) AS wht_prepaid_status) AS warna
                --WHERE a.business_unit IN ('Others')
                ) a
                WHERE invoice_number IN ({2}) ";

        public DataSourceResult GetChekBuktiPotongList(DataSourceRequest request, string invoiceType, string customer_group, string id)
        {
            string[] list = id.Split(',');
            var idTemp = new ArrayList();

            foreach (string d in list)
            {
                idTemp.Add("'" + d + "'");
            }
            var param = string.Join(",", idTemp.ToArray());

            #region Old
            //       var fetchqueryOld = string.Format(
            //           @"SELECT
            //                 *
            //               FROM (SELECT
            //                 distinct trvo.NamaCustomer customer_name,
            //                 tmcmn.customer_group,
            //                 '' upload_id,
            //                 0 CountRelatedInvoice,
            //                 trvo.danumber invoice_number,
            //                 0 materai_amount,
            //                 trvo.JumlahDPP + trvo.JumlahPPN amount_wht,
            //                 trvo.TanggalFaktur tgl_payment,
            //                 1 is_active,
            //                 0 bukti_potong_uploaded,
            //                 trvo.CreatedOn submit_date,
            //                 trvo.CreatedBy submit_by,
            //                 trvo.CreatedOn created_date,
            //                 trvo.CreatedBy created_by,
            //                 trvo.ModifiedOn modified_date,
            //                 trvo.ModifiedBy modified_by,
            //                 trvo.RowStatus row_status,
            //                 tmjp.TransaksiTypeCode jenis_pajak,
            //                 trvo.NomorFakturGabungan nomor_bukti_potong,
            //                 trvo.TanggalFaktur tanggal_bukti_potong,
            //                 trvo.MasaPajak masa_pajak,
            //                 trvo.TahunPajak tahun_pajak,
            //                 trvo.JumlahDPP jumlah_dpp,
            //                 0 tarif,
            //                 0 jumlah_pph,
            //                 trvo.TanggalFaktur invoice_date,
            //                 '' flag_potong_pajak,
            //                 tmjp.Description description,
            //                 trvo.BusinessUnit business_unit,
            //                 NULL AS tax_article,
            //                 trvo.NPWPCustomer no_npwp,
            //                 trvo.AlamatLawanTransaksi alamat_npwp,
            //                 trvo.NamaCustomer nama_npwp,
            //                 '0' wht_pdf_22,
            //                 '0' wht_pdf_23,
            //                 '0' wht_pdf_42,
            //                 '0' wht_pdf_status,
            //                 warna.wht_prepaid_status
            //               FROM TAM_EFAKTUR.dbo.TB_R_VATOut AS trvo
            //               LEFT JOIN BES.dbo.tb_m_customer_map_npwp AS tmcmn
            //                 ON trvo.NPWPCustomer = tmcmn.customer_npwp
            //               LEFT JOIN TAM_EFAKTUR.dbo.TB_M_JenisPajak AS tmjp
            //                 ON trvo.Id = trvo.KDJenisTransaksiID
            //             	OUTER APPLY (SELECT  [TAM_EFAKTUR].dbo.[udf_GetColorFromParam]('TMS_VATOUTSALES',
            //											  trvo.NomorFakturGabungan,
            //											  trvo.TanggalFaktur,
            //											  trvo.ApprovalStatus,
            //											  CASE ISNULL(trvo.PdfUrl,'') WHEN '' THEN 'Not Exist' ELSE 'Exist' END,trvo.TransitoryNumber,NULL,
            //											  trvo.BusinessUnit,
            //											  NULL,trvo.ApprovalTime,trvo.RecordTime,trvo.CreatedOn,trvo.ModifiedOn,trvo.TransitoryDate,NULL,NULL,NULL,NULL) AS wht_prepaid_status) AS  warna
            //               WHERE trvo.TaxInvoiceStatus IN ('Normal', 'Normal-Pengganti')
            //               AND trvo.BusinessUnit IN ('Lexus-Workshop', 'Others')
            //               AND trvo.ApprovalStatus = 'Approval Sukses'
            //               AND ISNULL(trvo.NPWPCustomer, '') != '00.000.000-0.000.000'

            //               UNION ALL

            //               SELECT
            //                 distinct a.customer_name,
            //                 a.customer_group,
            //                 '' upload_id,
            //                 0 CountRelatedInvoice,
            //                 a.da_no invoice_number,
            //                 0 materai_amount,
            //                 a.total_amount amount_wht,
            //                 a.doc_date,
            //                 1 is_active,
            //                 a.notur_uploaded bukti_potong_uploaded,
            //                 a.created_date submit_date,
            //                 a.created_by submit_by,
            //                 a.created_date created_date,
            //                 a.created_by created_by,
            //                 a.modified_date modified_date,
            //                 a.modified_by modified_by,
            //                 0 row_status,
            //                 a.JenisPajak jenis_pajak,
            //                 a.invoice_number nomor_bukti_potong,
            //                 a.doc_date tanggal_bukti_potong,
            //                 a.MasaPajak masa_pajak,
            //                 a.TahunPajak tahun_pajak,
            //                 a.dpp jumlah_dpp,
            //                 CAST(a.rate_pph AS decimal(18, 2)) AS tarif,
            //                 a.total_amount,
            //                 a.doc_date invoice_date,
            //                 '' flag_potong_pajak,
            //                 '' description,
            //                 a.business_unit,
            //                 NULL AS tax_article,
            //                 npwp.customer_npwp no_npwp,
            //                 a.customer_address alamat_npwp,
            //                 a.customer_name nama_npwp,
            //                 '0' wht_pdf_22_status,
            //                 '0' wht_pdf_23_status,
            //                 '0' wht_pdf_42_status,
            //                 '0' wht_pdf_status,
            //                 warna.wht_prepaid_status
            //               FROM BES.dbo.[udf_Invoice_wht]('{1}') a
            //               LEFT OUTER JOIN BES.dbo.tb_m_customer_map_npwp npwp
            //                 ON npwp.customer_group = a.customer_group
            //               OUTER APPLY (SELECT
            //                 [TAM_EFAKTUR].dbo.[udf_GetColorFromParam]('EBILL_DWHTS',a.da_no,a.doc_date,b.flag_potong_pajak,b.wht_pdf_22,b.wht_pdf_23,b.wht_pdf_42,b.business_unit,NULL,b.tgl_payment,b.submit_date,b.created_date,b.modified_date,b.tanggal_bukti_potong,NULL,NULL,NULL,NULL) AS wht_prepaid_status) AS warna
            //               WHERE a.business_unit IN ('Others')
            //               UNION ALL
            //               SELECT
            //                 distinct df.customer_name,
            //                 df.customer_group,
            //                 (SELECT TOP 1
            //                   bt.upload_id
            //                 FROM BES.dbo.tb_r_bukti_potong_attachment bt
            //                 WHERE bt.invoice_number = df.invoice_number)
            //                 AS upload_id,
            //                 (SELECT
            //                   COUNT(1)
            //                 FROM BES.dbo.tb_r_bukti_potong_attachment bt
            //                 WHERE bt.invoice_number = df.invoice_number
            //                 AND bt.upload_id = (SELECT TOP 1
            //                   bt.upload_id
            //                 FROM BES.dbo.tb_r_bukti_potong_attachment bt
            //                 WHERE bt.invoice_number =
            //                 df.invoice_number))
            //                 AS CountRelatedInvoice,
            //                 b.invoice_number,
            //                 b.materai_amount,
            //                 b.amount_wht,
            //                 b.tgl_payment,
            //                 b.is_active,
            //                 b.bukti_potong_uploaded,
            //                 b.submit_date,
            //                 b.submit_by,
            //                 b.created_date,
            //                 b.created_by,
            //                 b.modified_date,
            //                 b.modified_by,
            //                 b.row_status,
            //                 b.jenis_pajak,
            //                 b.nomor_bukti_potong,
            //                 b.tanggal_bukti_potong,
            //                 b.masa_pajak,
            //                 b.tahun_pajak,
            //                 b.jumlah_dpp,
            //                 CAST(b.tarif * 100 AS decimal(18, 2)) AS Tarif,
            //                 --b.tarif, 
            //                 b.amount_wht,
            //                 b.invoice_date,
            //                 --b.flag_potong_pajak,
            //               CASE WHEN b.flag_potong_pajak = 'Y' THEN 'Y'
            //ELSE CASE WHEN b.flag_potong_pajak = 'N' THEN 'N'
            //ELSE CASE WHEN b.flag_potong_pajak IS NULL THEN 'N/A'
            //END
            //END
            //END as 'ab',
            //                 b.description,
            //                 b.business_unit,
            //                 b.tax_article,
            //                 CASE
            //                   WHEN b.no_npwp IS NOT NULL THEN b.no_npwp
            //                   ELSE df.customer_code
            //                 END AS no_npwp,
            //                 CASE
            //                   WHEN b.alamat_npwp IS NOT NULL THEN b.alamat_npwp
            //                   ELSE df.customer_group
            //                 END AS alamat_npwp,
            //                 CASE
            //                   WHEN b.nama_npwp IS NOT NULL THEN b.nama_npwp
            //                   ELSE df.customer_name
            //                 END AS nama_npwp,
            //                 b.wht_pdf_22,
            //                 b.wht_pdf_23,
            //                 b.wht_pdf_42,
            //                 CASE
            //                   WHEN b.wht_pdf_22 IS NOT NULL THEN '1'
            //                   ELSE CASE
            //                       WHEN B.wht_pdf_23 IS NOT NULL THEN '1'
            //                       ELSE CASE
            //                           WHEN B.wht_pdf_42 IS NOT NULL THEN '1'
            //                           ELSE '0'
            //                         END
            //                     END
            //                 END
            //                 AS wht_pdf_status,
            //                 [TAM_EFAKTUR].dbo.[udf_GetColorFromParam]('EBILL_DWHTS', df.invoice_number, df.doc_date, b.flag_potong_pajak, b.wht_pdf_22, b.wht_pdf_23, b.wht_pdf_42, b.business_unit, NULL,df.due_date,df.downloaded_date,df.downloadedscan_date,df.pi_downloaded_date,b.created_date,b.modified_date,NULL,NULL,NULL) AS wht_prepaid_status
            //               FROM BES.dbo.tb_r_bukti_potong b
            //               LEFT JOIN BES.dbo.udf_Invoice('{0}', '{1}') df
            //                 ON b.invoice_number = df.invoice_number) a
            //               WHERE invoice_number IN ({2}) ", invoiceType, customer_group, param.ToString());
            #endregion

            string fetchquery = string.Format(queryExport, invoiceType, customer_group, param.ToString());

            var genericDataTableQuery = new GenericDataSourceQuery(Connection, request);
            var output = genericDataTableQuery.GetData<BuktiPotong>(fetchquery);

            return output;
        }

        public DataSourceResult getchkbuktipotong(DataSourceRequest request, string invoiceType, string customer_group, String[] id)
        {
            var idTemp = new ArrayList();
            foreach (String d in id)
            {
                idTemp.Add("'" + d + "'");
            }
            var param = string.Join(",", idTemp.ToArray());

            #region Old
            //       var queryOld = string.Format(
            //            @"SELECT
            //                 *
            //               FROM (SELECT
            //                 distinct trvo.NamaCustomer customer_name,
            //                 tmcmn.customer_group,
            //                 '' upload_id,
            //                 0 CountRelatedInvoice,
            //                 trvo.danumber invoice_number,
            //                 0 materai_amount,
            //                 trvo.JumlahDPP + trvo.JumlahPPN amount_wht,
            //                 trvo.TanggalFaktur tgl_payment,
            //                 1 is_active,
            //                 0 bukti_potong_uploaded,
            //                 trvo.CreatedOn submit_date,
            //                 trvo.CreatedBy submit_by,
            //                 trvo.CreatedOn created_date,
            //                 trvo.CreatedBy created_by,
            //                 trvo.ModifiedOn modified_date,
            //                 trvo.ModifiedBy modified_by,
            //                 trvo.RowStatus row_status,
            //                 tmjp.TransaksiTypeCode jenis_pajak,
            //                 trvo.NomorFakturGabungan nomor_bukti_potong,
            //                 trvo.TanggalFaktur tanggal_bukti_potong,
            //                 trvo.MasaPajak masa_pajak,
            //                 trvo.TahunPajak tahun_pajak,
            //                 trvo.JumlahDPP jumlah_dpp,
            //                 0 tarif,
            //                 0 jumlah_pph,
            //                 trvo.TanggalFaktur invoice_date,
            //                 '' flag_potong_pajak,
            //                 tmjp.Description description,
            //                 trvo.BusinessUnit business_unit,
            //                 NULL AS tax_article,
            //                 trvo.NPWPCustomer no_npwp,
            //                 trvo.AlamatLawanTransaksi alamat_npwp,
            //                 trvo.NamaCustomer nama_npwp,
            //                 '0' wht_pdf_22,
            //                 '0' wht_pdf_23,
            //                 '0' wht_pdf_42,
            //                 '0' wht_pdf_status,
            //                 warna.wht_prepaid_status
            //               FROM TAM_EFAKTUR.dbo.TB_R_VATOut AS trvo
            //               LEFT JOIN BES.dbo.tb_m_customer_map_npwp AS tmcmn
            //                 ON trvo.NPWPCustomer = tmcmn.customer_npwp
            //               LEFT JOIN TAM_EFAKTUR.dbo.TB_M_JenisPajak AS tmjp
            //                 ON trvo.Id = trvo.KDJenisTransaksiID
            //            OUTER APPLY (SELECT  [TAM_EFAKTUR].dbo.[udf_GetColorFromParam]('TMS_VATOUTSALES',
            //											  trvo.NomorFakturGabungan,
            //											  trvo.TanggalFaktur,
            //											  trvo.ApprovalStatus,
            //											  CASE ISNULL(trvo.PdfUrl,'') WHEN '' THEN 'Not Exist' ELSE 'Exist' END,trvo.TransitoryNumber,NULL,
            //											  trvo.BusinessUnit,
            //											  NULL,trvo.ApprovalTime,trvo.RecordTime,trvo.CreatedOn,trvo.ModifiedOn,trvo.TransitoryDate,NULL,NULL,NULL,NULL) AS wht_prepaid_status) AS  warna
            //               WHERE trvo.TaxInvoiceStatus IN ('Normal', 'Normal-Pengganti')
            //               AND trvo.BusinessUnit IN ('Lexus-Workshop', 'Others')
            //               AND trvo.ApprovalStatus = 'Approval Sukses'
            //               AND ISNULL(trvo.NPWPCustomer, '') != '00.000.000-0.000.000'

            //               UNION ALL

            //               SELECT
            //                distinct  a.customer_name,
            //                 a.customer_group,
            //                 '' upload_id,
            //                 0 CountRelatedInvoice,
            //                 a.da_no invoice_number,
            //                 0 materai_amount,
            //                 a.total_amount amount_wht,
            //                 a.doc_date,
            //                 1 is_active,
            //                 a.notur_uploaded bukti_potong_uploaded,
            //                 a.created_date submit_date,
            //                 a.created_by submit_by,
            //                 a.created_date created_date,
            //                 a.created_by created_by,
            //                 a.modified_date modified_date,
            //                 a.modified_by modified_by,
            //                 0 row_status,
            //                 a.JenisPajak jenis_pajak,
            //                 a.invoice_number nomor_bukti_potong,
            //                 a.doc_date tanggal_bukti_potong,
            //                 a.MasaPajak masa_pajak,
            //                 a.TahunPajak tahun_pajak,
            //                 a.dpp jumlah_dpp,
            //                 CAST(a.rate_pph AS decimal(18, 2)) AS tarif,
            //                 a.total_amount,
            //                 a.doc_date invoice_date,
            //                 '' flag_potong_pajak,
            //                 '' description,
            //                 a.business_unit,
            //                 NULL AS tax_article,
            //                 npwp.customer_npwp no_npwp,
            //                 a.customer_address alamat_npwp,
            //                 a.customer_name nama_npwp,
            //                 '0' wht_pdf_22_status,
            //                 '0' wht_pdf_23_status,
            //                 '0' wht_pdf_42_status,
            //                 '0' wht_pdf_status,
            //                 warna.wht_prepaid_status
            //               FROM BES.dbo.[udf_Invoice_wht]('{1}') a
            //               LEFT OUTER JOIN BES.dbo.tb_m_customer_map_npwp npwp
            //                 ON npwp.customer_group = a.customer_group
            //               OUTER APPLY (SELECT
            //                 [TAM_EFAKTUR].dbo.[udf_GetColorFromParam]('EBILL_DWHTS',a.da_no,a.doc_date,b.flag_potong_pajak,b.wht_pdf_22,b.wht_pdf_23,b.wht_pdf_42,b.business_unit,NULL,b.tgl_payment,b.submit_date,b.created_date,b.modified_date,b.tanggal_bukti_potong,NULL,NULL,NULL,NULL) AS wht_prepaid_status) AS warna
            //               WHERE a.business_unit IN ('Others')
            //               UNION ALL
            //               SELECT
            //                distinct  df.customer_name,
            //                 df.customer_group,
            //                 (SELECT TOP 1
            //                   bt.upload_id
            //                 FROM BES.dbo.tb_r_bukti_potong_attachment bt
            //                 WHERE bt.invoice_number = df.invoice_number)
            //                 AS upload_id,
            //                 (SELECT
            //                   COUNT(1)
            //                 FROM BES.dbo.tb_r_bukti_potong_attachment bt
            //                 WHERE bt.invoice_number = df.invoice_number
            //                 AND bt.upload_id = (SELECT TOP 1
            //                   bt.upload_id
            //                 FROM BES.dbo.tb_r_bukti_potong_attachment bt
            //                 WHERE bt.invoice_number =
            //                 df.invoice_number))
            //                 AS CountRelatedInvoice,
            //                 b.invoice_number,
            //                 b.materai_amount,
            //                 b.amount_wht,
            //                 b.tgl_payment,
            //                 b.is_active,
            //                 b.bukti_potong_uploaded,
            //                 b.submit_date,
            //                 b.submit_by,
            //                 b.created_date,
            //                 b.created_by,
            //                 b.modified_date,
            //                 b.modified_by,
            //                 b.row_status,
            //                 b.jenis_pajak,
            //                 b.nomor_bukti_potong,
            //                 b.tanggal_bukti_potong,
            //                 b.masa_pajak,
            //                 b.tahun_pajak,
            //                 b.jumlah_dpp,
            //                 CAST(b.tarif * 100 AS decimal(18, 2)) AS Tarif,
            //                 --b.tarif, 
            //                 b.amount_wht,
            //                 b.invoice_date,
            //                 --b.flag_potong_pajak,
            //               CASE WHEN b.flag_potong_pajak = 'Y' THEN 'Y'
            //ELSE CASE WHEN b.flag_potong_pajak = 'N' THEN 'N'
            //ELSE CASE WHEN b.flag_potong_pajak IS NULL THEN 'N/A'
            //END
            //END
            //END as 'ab',
            //                 b.description,
            //                 b.business_unit,
            //                 b.tax_article,
            //                 CASE
            //                   WHEN b.no_npwp IS NOT NULL THEN b.no_npwp
            //                   ELSE df.customer_code
            //                 END AS no_npwp,
            //                 CASE
            //                   WHEN b.alamat_npwp IS NOT NULL THEN b.alamat_npwp
            //                   ELSE df.customer_group
            //                 END AS alamat_npwp,
            //                 CASE
            //                   WHEN b.nama_npwp IS NOT NULL THEN b.nama_npwp
            //                   ELSE df.customer_name
            //                 END AS nama_npwp,
            //                 b.wht_pdf_22,
            //                 b.wht_pdf_23,
            //                 b.wht_pdf_42,
            //                 CASE
            //                   WHEN b.wht_pdf_22 IS NOT NULL THEN '1'
            //                   ELSE CASE
            //                       WHEN B.wht_pdf_23 IS NOT NULL THEN '1'
            //                       ELSE CASE
            //                           WHEN B.wht_pdf_42 IS NOT NULL THEN '1'
            //                           ELSE '0'
            //                         END
            //                     END
            //                 END
            //                 AS wht_pdf_status,
            //                 [TAM_EFAKTUR].dbo.[udf_GetColorFromParam]('EBILL_DWHTS', df.invoice_number, df.doc_date, b.flag_potong_pajak, b.wht_pdf_22, b.wht_pdf_23, b.wht_pdf_42, b.business_unit, NULL,df.due_date,df.downloaded_date,df.downloadedscan_date,df.pi_downloaded_date,b.created_date,b.modified_date,NULL,NULL,NULL) AS wht_prepaid_status
            //               FROM BES.dbo.tb_r_bukti_potong b
            //               LEFT JOIN BES.dbo.udf_Invoice('{0}', '{1}') df
            //                 ON b.invoice_number = df.invoice_number) a
            //               WHERE invoice_number IN ({2}) ", invoiceType, customer_group, param.ToString());
            #endregion

            string query = string.Format(queryExport, invoiceType, customer_group, param.ToString());

            var genericDataTableQuery = new GenericDataSourceQuery(Connection, request);
            var output = genericDataTableQuery.GetData<BuktiPotong>(query);

            return output;
        }
    }
}
