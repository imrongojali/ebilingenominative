﻿using Agit.Domain;
using Agit.Mvc;
using Kendo.Mvc.UI;
using System.Data;
using Dapper;
using System.Collections.Generic;
using System.Linq;

namespace Tam.Ebilling.Domain
{
    public partial interface IMenuLevelFeatureRepository : IDapperRepository<MenuLevelFeature>
    {
        List<MenuLevelFeature> GetMenuLevelFeatureByFeatureId(int featureId);
    }

    public partial class MenuLevelFeatureRepository : DapperRepository<MenuLevelFeature>, IMenuLevelFeatureRepository
    {
        public MenuLevelFeatureRepository(IDbConnection connection, IDbTransaction transaction)
            : base(connection, transaction)
        {
        }

        public List<MenuLevelFeature> GetMenuLevelFeatureByFeatureId(int featureId)
        {
            List<MenuLevelFeature> items = new List<MenuLevelFeature>();

            var query = string.Format(@"
                SELECT        
                    *
                FROM
                    dbo.tb_r_feature_role 
                WHERE 
                    feature_id = {0}", featureId);

            items = Connection.Query<MenuLevelFeature>(query).ToList();

            return items;
        }

    }
}
