﻿using Kendo.Mvc.UI;
using System.Data;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Collections;

namespace Tam.Ebilling.Domain
{
    public partial interface INotifRepository : IDapperRepository<Notif>
    {
        string GetNotif(string customerCode);
    }

    public partial class NotifRepository : DapperRepository<Notif>, INotifRepository
    {
        public NotifRepository(IDbConnection connection, IDbTransaction transaction)
            : base(connection, transaction)
        {
        }

        public string GetNotif(string customerCode)
        {
            if (customerCode == null)
            {
                customerCode = "";
            }
            var parameters = new Dictionary<string, object>();
            parameters.Add("customerCode", customerCode);

            var genericDataTableQuery = new GenericDataSourceQuery(Connection, new DataSourceRequest());
           
            var output = Connection.Query<Notif>("TAM_EFAKTUR.dbo.efb_sp_GetAllNotificationsForEbill ", parameters, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);
             
            return output.Select(x=>x.ReturnMessage).FirstOrDefault();
        }
         
         
    }
}
