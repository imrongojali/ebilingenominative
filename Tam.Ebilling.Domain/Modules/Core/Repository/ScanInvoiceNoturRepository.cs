﻿using System;
using Kendo.Mvc.UI;
using System.Data;
using Dapper;
using Tam.Ebilling.Domain.Modules.Core.Model;
using System.Collections.Generic;
using System.Linq;

namespace Tam.Ebilling.Domain
{
    public partial interface IScanInvoiceNoturRepository : IDapperRepository<ScanInvoiceNotur>
    {
        DataSourceResult GetDataSourceResult(DataSourceRequest request, object parameter = null);
        List<ScanInvoiceNotur> UploadScan(string filename, string invoice_number);
        List<ScanInvoiceNotur> RemoveScanNotur(long attachment_invoice_notur_id);
    }

    public partial class ScanInvoiceNoturRepository : DapperRepository<ScanInvoiceNotur>, IScanInvoiceNoturRepository
    {
        public ScanInvoiceNoturRepository(IDbConnection connection, IDbTransaction transaction)
            : base(connection, transaction)
        {
        }

        public DataSourceResult GetDataSourceResult(DataSourceRequest request, object parameter = null)
        {
            var genericDataTableQuery = new GenericDataSourceQuery(Connection, request);
            var output = genericDataTableQuery.GetData<ScanInvoiceNotur>();

            return output;
        }
        public List<ScanInvoiceNotur> UploadScan(string filename, string invoice_number)
        {

            var parameters = new Dictionary<string, object>();
            parameters.Add("filename", filename);
            parameters.Add("invoice_number", invoice_number);
            var output = Connection.Query<ScanInvoiceNotur>("usp_UploadScanInvoiceNotur", parameters, null, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);

            return output.ToList();
        }
        public List<ScanInvoiceNotur> RemoveScanNotur(long attachment_invoice_notur_id)
        {

            var parameters = new Dictionary<string, object>();
            parameters.Add("attachment_invoice_notur_id", attachment_invoice_notur_id);
            var output = Connection.Query<ScanInvoiceNotur>("usp_DeleteUploadScanNotur", parameters, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);

            return output.ToList();
        }
       
    }
}
