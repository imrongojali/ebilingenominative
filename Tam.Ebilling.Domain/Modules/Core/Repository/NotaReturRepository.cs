﻿using Kendo.Mvc.UI;
using System.Data;
using Dapper;
using System.Collections.Generic;
using System.Linq;

namespace Tam.Ebilling.Domain
{
    public partial interface INotaReturRepository : IDapperRepository<Invoice>
    {
        //DataSourceResult GetDataSourceResult(DataSourceRequest request, object parameter = null);
        List<MvInvoice> GetMvInvoice(DataSourceRequest request, string daNumber, string user);
        List<MvInvoice> GetMvInvoice(string daNumber, string user);
        List<PrintNotur> GetPrintNotaRetur(DataSourceRequest request, string da_number,string user);
        List<PrintNotur> GetPrintNotaRetur(string da_number, string user);
        List<DebitAdvice> GetDebitAdvice(DataSourceRequest request, string daNumber, string user);
        List<Invoice> GetUpdateDownloadNotur(string da_number);
        List<Invoice> GetNotaRetur(string customer_group, string invoice_number = null, int? business_unit_id = null, int? transaction_type_id = null, string date_from = null, string date_to = null, string ar_status = null, int? document_status = null);
        List<Invoice> GetNoturByUploadStatus(string status, int invoice_type);
        List<Invoice> GetNoturUpload(string status, string customer_group);
        List<SpDebitCreditAdvice> GetSpDebitAdvice(DataSourceRequest request, string daNumber, string user);
        List<SpDebitCreditAdvice> GetSpDebitAdvice(string daNumber, string user);
        List<SpDebitCreditAdvice> GetSpCreditAdvice(DataSourceRequest request, string caNumber, string user);
        List<SpNotaRetur> GetNotaReturSp(DataSourceRequest request, string caNumber, string user);
        List<SpNotaRetur> GetNotaReturMv(DataSourceRequest request, string NomorNotaRetur, string user);
        DataSourceResult GetReportNotaReturDataSourceResult(DataSourceRequest request, string customer_group);
        AttachmentNotaRetur GetNoturAttachByRef(string da_no, string status);
    }

    public partial class NotaReturRepository : DapperRepository<Invoice>, INotaReturRepository
    {
        public NotaReturRepository(IDbConnection connection, IDbTransaction transaction)
            : base(connection, transaction)
        {
        }

        //public DataSourceResult GetDataSourceResult(DataSourceRequest request, object parameter = null)
        //{
        //    var genericDataTableQuery = new GenericDataSourceQuery(Connection, request);
        //    var output = genericDataTableQuery.GetData<Invoice>();

        //    return output;
        //}
        public List<Invoice> GetUpdateDownloadNotur(string da_number)
        {

            var parameters = new Dictionary<string, object>();
            parameters.Add("da_number", da_number);
            var output = Connection.Query<Invoice>("usp_UpdateNoturDownloaded", parameters, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);

            return output.ToList();
        }
        public List<MvInvoice> GetMvInvoice(DataSourceRequest request, string daNumber, string user)
        {
            var genericDataTableQuery = new GenericDataSourceQuery(Connection, request);

            var parameters = new Dictionary<string, object>();
            parameters.Add("daNumber", daNumber);
            parameters.Add("username", user);

            //var output = genericDataTableQuery.GetData<MvInvoice>("exec usp_Print_MvInvoice @0, @1", parameters);
            //var output = genericDataTableQuery.GetData<MvInvoice>("usp_Print_MvInvoice", parameters, commandType: CommandType.StoredProcedure);


            var output = Connection.Query<MvInvoice>("usp_Print_MvInvoice", parameters, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);

            //    var user = cnn.Query<User>("spGetUser", new { Id = 1 },
            //commandType: CommandType.StoredProcedure).First();

            return output.ToList();
        }

        public List<MvInvoice> GetMvInvoice(string daNumber, string user)
        {
            var genericDataTableQuery = new GenericDataSourceQuery(Connection, new DataSourceRequest());

            var parameters = new Dictionary<string, object>();
            parameters.Add("daNumber", daNumber);
            parameters.Add("username", user);

            //var output = genericDataTableQuery.GetData<MvInvoice>("exec usp_Print_MvInvoice @0, @1", parameters);
            //var output = genericDataTableQuery.GetData<MvInvoice>("usp_Print_MvInvoice", parameters, commandType: CommandType.StoredProcedure);


            var output = Connection.Query<MvInvoice>("usp_Print_MvInvoice", parameters, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);

            //    var user = cnn.Query<User>("spGetUser", new { Id = 1 },
            //commandType: CommandType.StoredProcedure).First();

            return output.ToList();
        }
        public List<PrintNotur> GetPrintNotaRetur(DataSourceRequest request,string da_number, string user)
        {
            var genericDataTableQuery = new GenericDataSourceQuery(Connection, request);

            var parameters = new Dictionary<string, object>();
            parameters.Add("da_number", da_number);
            parameters.Add("user", user);

            //var output = genericDataTableQuery.GetData<MvInvoice>("exec usp_Print_MvInvoice @0, @1", parameters);
            //var output = genericDataTableQuery.GetData<MvInvoice>("usp_Print_MvInvoice", parameters, commandType: CommandType.StoredProcedure);


            var output = Connection.Query<PrintNotur>("usp_Print_NotaRetur", parameters, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);

            //    var user = cnn.Query<User>("spGetUser", new { Id = 1 },
            //commandType: CommandType.StoredProcedure).First();

            return output.ToList();
        }

        public List<PrintNotur> GetPrintNotaRetur(string da_number, string user)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("da_number", da_number);
            parameters.Add("user", user);

            var output = Connection.Query<PrintNotur>("usp_Print_NotaRetur", parameters, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);

            return output.ToList();
        }

        public List<DebitAdvice> GetDebitAdvice(DataSourceRequest request, string daNumber, string user)
        {
            var genericDataTableQUery = new GenericDataSourceQuery(Connection, request);
            var parameters = new Dictionary<string, object>();
            parameters.Add("da_number", daNumber);
            parameters.Add("user", user);
            parameters.Add("type", 0); // Todo: Remove Hardcoded 

            var output = Connection.Query<DebitAdvice>("usp_Print_DebitAdvice", parameters, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);

            return output.ToList();
        }

        public List<SpDebitCreditAdvice> GetSpDebitAdvice(DataSourceRequest request, string daNumber, string user)
        {
            var genericDataTableQUery = new GenericDataSourceQuery(Connection, request);
            var parameters = new Dictionary<string, object>();
            parameters.Add("da_number", daNumber);
            parameters.Add("user", user);
            parameters.Add("type", 0); // Todo: Remove Hardcoded 

            var output = Connection.Query<SpDebitCreditAdvice>("usp_Print_SpDebitAdvice", parameters, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);

            return output.ToList();
        }

        public List<SpDebitCreditAdvice> GetSpDebitAdvice(string daNumber, string user)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("da_number", daNumber);
            parameters.Add("user", user);
            parameters.Add("type", 0); // Todo: Remove Hardcoded 

            var output = Connection.Query<SpDebitCreditAdvice>("usp_Print_SpDebitAdvice", parameters, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);

            return output.ToList();
        }

        public List<SpDebitCreditAdvice> GetSpCreditAdvice(DataSourceRequest request, string caNumber, string user)
        {
            var genericDataTableQUery = new GenericDataSourceQuery(Connection, request);
            var parameters = new Dictionary<string, object>();
            parameters.Add("ca_number", caNumber);
            parameters.Add("user", user);
            parameters.Add("type", 0); // Todo: Remove Hardcoded 

            var output = Connection.Query<SpDebitCreditAdvice>("usp_Print_SpCreditAdvice", parameters, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);

            return output.ToList();
        }

        public List<SpNotaRetur> GetNotaReturSp(DataSourceRequest request, string caNumber, string user)
        {
            var genericDataTableQUery = new GenericDataSourceQuery(Connection, request);
            var parameters = new Dictionary<string, object>();
            parameters.Add("ca_number", caNumber);
            parameters.Add("user", user);

            var output = Connection.Query<SpNotaRetur>("usp_Print_NotaReturSpCreditAdvice", parameters, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);

            return output.ToList();
        }

        public List<SpNotaRetur> GetNotaReturMv(DataSourceRequest request,string NomorNotaRetur, string user)
        {
            var genericDataTableQUery = new GenericDataSourceQuery(Connection, request);
            var parameters = new Dictionary<string, object>();
            //parameters.Add("ca_number", caNumber);
            parameters.Add("NomorNotaRetur", NomorNotaRetur);
            parameters.Add("user", user);

            var output = Connection.Query<SpNotaRetur>("usp_Print_NotaReturMvCreditAdvice", parameters, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);
           
            return output.ToList();
        }


        public List<Invoice> GetNotaRetur(string customer_group, string invoice_number = null, int? business_unit_id = null, int? transaction_type_id = null, string date_from = null, string date_to = null, string ar_status = null, int? document_status = null)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("TYPE", 2);
            parameters.Add("CUSTOMER_GROUP", customer_group);
            parameters.Add("BUSINESS_UNIT_ID", business_unit_id);
            parameters.Add("INVOICE_NUMBER", invoice_number);
            parameters.Add("TRANSACTION_TYPE_ID", transaction_type_id);
            parameters.Add("DATE_FROM", date_from);
            parameters.Add("DATE_TO", date_to);
            parameters.Add("AR_STATUS", ar_status);
            parameters.Add("DOCUMENT_STATUS", document_status);

            var output = Connection.Query<Invoice>("usp_GetInvoice", parameters, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);

            return output.ToList();
        }

        public List<Invoice> GetNoturUpload(string status, string customer_group)
        {
            var fetchquery = string.Empty;
            var query = string.Empty;
            string customer_gr = "'" + customer_group + "'";

            if (status == "upload")
            {
                fetchquery = @"select notur_uploaded, COUNT(*) as invoice_number from udf_Invoice(2, {0}) 
                               WHERE notur_uploaded is not null and customer_group is not null  group by notur_uploaded";
                query = string.Format(fetchquery, customer_gr);
            }
            else
            {
                
            }

            var output = Connection.Query<Invoice>(query).ToList();

            return output;
        }

        public List<Invoice> GetNoturByUploadStatus(string status, int invoice_type)
        {
            var fetchquery = string.Empty;
            var query = string.Empty;

            if (status == null)
            {
                fetchquery = @"SELECT distinct customer_group  FROM udf_Invoice({0}, '') WHERE notur_uploaded is null and customer_group is not null";
                query = string.Format(fetchquery, invoice_type);
            }
            else
            {
                fetchquery = @"SELECT distinct customer_group FROM udf_Invoice({0}, '') WHERE notur_uploaded = {2} and customer_group is not null";
                query = string.Format(fetchquery, invoice_type, status);
            }

            //var genericDataTableQuery = new GenericDataSourceQuery(Connection, request);
            //var output = genericDataTableQuery.GetData<Invoice>(query);

            var output = Connection.Query<Invoice>(query).ToList();

            return output;
        }

        public DataSourceResult GetReportNotaReturDataSourceResult(DataSourceRequest request, string customer_group)
        {
            var fetchquery = @"select * from udf_ReportNotaRetur ('{0}')";

            var query = string.Format(fetchquery, customer_group);

            var genericDataTableQuery = new GenericDataSourceQuery(Connection, request);
            var output = genericDataTableQuery.GetData<ReportNotaRetur>(query);

            return output;
        }

        public AttachmentNotaRetur GetNoturAttachByRef(string da_no, string status)
        {
            string query = string.Format(@"SELECT * FROM tb_r_attachment_notaretur WHERE reference = '{0}' and document_type = '{1}'", da_no, status);
            return Connection.Query<AttachmentNotaRetur>(query).FirstOrDefault();
        }
    }
}
