﻿using System.Data;
using Dapper;
using System.Collections.Generic;
using System.Linq;

namespace Tam.Ebilling.Domain
{
    public partial interface IPVMonitoringAttachmentRepository : IDapperRepository<PVMonitoringAttachment>
    {
        List<PVMonitoringAttachment> GetPVAttachment(int pid);        
        void Remove(IDbTransaction transaction, string referenceNo, int refSeqNo);
        void UpdatePVAttachment(IDbTransaction transaction, string oldReffNo, string newReffNo, string directory);
    }

    public partial class PVMonitoringAttachmentRepository : DapperRepository<PVMonitoringAttachment>, IPVMonitoringAttachmentRepository
    {
        public PVMonitoringAttachmentRepository(IDbConnection connection, IDbTransaction transaction)
            : base(connection, transaction)
        {
            
        }

        public void Remove(IDbTransaction transaction, string referenceNo, int refSeqNo)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("referenceNo", referenceNo);
            parameters.Add("refSeqNo", refSeqNo);
            var output = Connection.Execute("usp_DeletePVMonitoringAttachment", parameters, transaction, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);

        }

        public List<PVMonitoringAttachment> GetPVAttachment(int pid)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("pid", pid);
            var output = Connection.Query<PVMonitoringAttachment>("usp_GetPVAttachment", parameters, null, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);

            return output.ToList();
        }

        public void UpdatePVAttachment(IDbTransaction transaction, string oldReffNo, string newReffNo, string directory)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("oldReffNo", oldReffNo);
            parameters.Add("newReffNo", newReffNo);
            parameters.Add("directory", directory);
            var output = Connection.Query<PVMonitoringAttachment>("usp_UpdatePVAttachment", parameters, transaction, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);
        }
    }
}