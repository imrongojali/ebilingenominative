﻿
using Kendo.Mvc.UI;
using System.Data;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using System;

namespace Tam.Ebilling.Domain
{
    public partial interface IBuktiPotongAttachmentRepository : IDapperRepository<BuktiPotongAttachment>
    {
        List<BuktiPotongAttachment> UploadBuktiPotong(IDbTransaction transaction, string filename, string serverFilename, string invoiceNumber
            , string uploadId);
        void Remove(IDbTransaction transaction, long attachmentId);
    }

    public partial class BuktiPotongAttachmentRepository : DapperRepository<BuktiPotongAttachment>, IBuktiPotongAttachmentRepository
    {
        public BuktiPotongAttachmentRepository(IDbConnection connection, IDbTransaction transaction)
            : base(connection, transaction)
        {
        }

        public void Remove(IDbTransaction transaction, long attachmentId)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("attachment_bukti_potong_id", attachmentId);
            var output = Connection.Execute("usp_DeleteUploadBuktiPotong", parameters, transaction, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);
            
        }

        public List<BuktiPotongAttachment> UploadBuktiPotong(IDbTransaction transaction, string filename, string serverFilename, string invoiceNumber
            , string uploadId)
        {

            var parameters = new Dictionary<string, object>();
            parameters.Add("filename", filename);
            parameters.Add("server_filename", serverFilename);
            parameters.Add("invoiceNumber", invoiceNumber);
            parameters.Add("uploadId", uploadId);
            var output = Connection.Query<BuktiPotongAttachment>("usp_UploadBuktiPotong", parameters, transaction, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);

            return output.ToList();
        }

    }
}
