﻿using Kendo.Mvc.UI;
using System.Data;
using Dapper;
using System.Collections.Generic;
using System.Linq;

namespace Tam.Ebilling.Domain
{
    public partial interface IKwitansiRepository : IDapperRepository<Invoice>
    {
        List<Invoice> GetNotaRetur(string customer_group, string invoice_number = null, int? business_unit_id = null, int? transaction_type_id = null, string date_from = null, string date_to = null, string ar_status = null, int? document_status = null);
       
    }

    public partial class KwitansiRepository : DapperRepository<Invoice>, IKwitansiRepository
    {
        public KwitansiRepository(IDbConnection connection, IDbTransaction transaction)
            : base(connection, transaction)
        {
        }
        
        
        public List<Invoice> GetNotaRetur(string customer_group, string invoice_number = null, int? business_unit_id = null, int? transaction_type_id = null, string date_from = null, string date_to = null, string ar_status = null, int? document_status = null)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("TYPE", 2);
            parameters.Add("CUSTOMER_GROUP", customer_group);
            parameters.Add("BUSINESS_UNIT_ID", business_unit_id);
            parameters.Add("INVOICE_NUMBER", invoice_number);
            parameters.Add("TRANSACTION_TYPE_ID", transaction_type_id);
            parameters.Add("DATE_FROM", date_from);
            parameters.Add("DATE_TO", date_to);
            parameters.Add("AR_STATUS", ar_status);
            parameters.Add("DOCUMENT_STATUS", document_status);

            var output = Connection.Query<Invoice>("usp_GetInvoice", parameters, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);

            return output.ToList();
        }
        
    }
}
