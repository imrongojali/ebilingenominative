﻿
using Kendo.Mvc.UI;
using System.Data;
using Dapper;
using System.Collections.Generic;
using System.Linq;

namespace Tam.Ebilling.Domain
{
    public partial interface ICustomerGroupRepository : IDapperRepository<CustomerGroup>
    {
        List<CustomerGroup> Get();
    }

    public partial class CustomerGroupRepository : DapperRepository<CustomerGroup>, ICustomerGroupRepository
    {
        public CustomerGroupRepository(IDbConnection connection, IDbTransaction transaction)
            : base(connection, transaction)
        {
        }

        public List<CustomerGroup> Get()
        {
            var output = Connection.Query<CustomerGroup>("SELECT * FROM tb_m_customer_group");
           
            return output.AsList<CustomerGroup>();
        }

        
    }
}
