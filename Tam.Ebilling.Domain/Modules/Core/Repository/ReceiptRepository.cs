﻿
using Kendo.Mvc.UI;
using System.Data;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Text;

namespace Tam.Ebilling.Domain
{
    public partial interface IReceiptRepository: IDapperRepository<SummaryInvoiceHistory>
    {
        DataSourceResult GetViewDataSourceResult(DataSourceRequest request, string customer_group, string userType);
        void RecordHistory(string dateFrom, string dateTo, string businessType, string customerId, string user);
    }

    public partial class ReceiptRepository : DapperRepository<SummaryInvoiceHistory>, IReceiptRepository
    {
        public ReceiptRepository(IDbConnection connection, IDbTransaction transaction)
            : base(connection, transaction)
        {
        }
        
        public DataSourceResult GetViewDataSourceResult(DataSourceRequest request, string customer_group, string userType)
        {
            StringBuilder fetchquery = new StringBuilder(@"select id
                                        , date_from as dateFrom
                                        , date_to as dateTo
                                        , business_type as businessType
                                        , customer_group as customerGroup
                                        , created_by
                                        , created_date
                                        from tb_r_history_download_receipt 
                                        where 1=1 ");

            if (!string.IsNullOrEmpty(customer_group) && userType != "Internal")
            {
                fetchquery.AppendFormat(" and customer_group = '{0}' ", customer_group);
            }

            var genericDataTableQuery = new GenericDataSourceQuery(Connection, request);
            var output = genericDataTableQuery.GetData<SummaryInvoiceHistory>(fetchquery.ToString());

            return output;
        }
        
        public void RecordHistory(string dateFrom, string dateTo, string businessType, string customerId, string user)
        {

            var parameters = new Dictionary<string, object>();
            parameters.Add("dateFrom", dateFrom);
            parameters.Add("dateTo", dateTo);
            parameters.Add("businessType", businessType);
            parameters.Add("customerGroup", customerId);
            parameters.Add("user", user);

            Connection.Query<Invoice>("usp_RecordDownloadReceipt", parameters, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);

        }
    }
}
