﻿using Kendo.Mvc.UI;
using System.Data;
using Dapper;

namespace Tam.Ebilling.Domain
{
    public partial interface IParameterRepository : IDapperRepository<Parameter>
    {
        DataSourceResult GetDataSourceResult(DataSourceRequest request, object parameter = null);
    }

    public partial class ParameterRepository : DapperRepository<Parameter>, IParameterRepository
    {
        public ParameterRepository(IDbConnection connection, IDbTransaction transaction)
            : base(connection, transaction)
        {
        }

        public DataSourceResult GetDataSourceResult(DataSourceRequest request, object parameter = null)
        {
            var genericDataTableQuery = new GenericDataSourceQuery(Connection, request);
            var output = genericDataTableQuery.GetData<Parameter>();

            return output;
        }
    }
}
