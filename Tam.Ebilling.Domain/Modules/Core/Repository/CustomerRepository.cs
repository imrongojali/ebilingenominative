﻿
using Kendo.Mvc.UI;
using System.Data;
using Dapper;
using System.Collections.Generic;
using System.Linq;

namespace Tam.Ebilling.Domain
{
    public partial interface ICustomerRepository : IDapperRepository<Customer>
    {
        DataSourceResult GetDataSourceResult(DataSourceRequest request, object parameter = null);
        int Add(Customer item, string[] columns);
        int Update(Customer item, string[] columns);
    }

    public partial class CustomerRepository : DapperRepository<Customer>, ICustomerRepository
    {
        public CustomerRepository(IDbConnection connection, IDbTransaction transaction)
            : base(connection, transaction)
        {
        }

        public DataSourceResult GetDataSourceResult(DataSourceRequest request, object parameter = null)
        {
            var genericDataTableQuery = new GenericDataSourceQuery(Connection, request);
            var output = genericDataTableQuery.GetData<Customer>();
            return output;
        }

        public int Add(Customer item, string[] columns)
        {
            var parameters = (object)Mapping(item);
            return Connection.Execute(DynamicQuery.GetInsertQuery("tb_m_customer_mapping", parameters, "customer_code", columns), parameters, Transaction);
        }

        public int Update(Customer item, string[] columns)
        {
            var parameters = (object)Mapping(item);
            return Connection.Execute(DynamicQuery.GetUpdateQuery("tb_m_customer_mapping", item, "customer_code", columns), parameters, Transaction);
        }
    }
}
