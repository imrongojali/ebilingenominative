﻿using Agit.Domain;
using Agit.Mvc;
using Dapper;
using Kendo.Mvc.UI;
using System.Collections.Generic;
using System.Data;

namespace Tam.Ebilling.Domain
{
    public partial interface IMenuGroupRepository : IDapperRepository<MenuGroup>
    {
    }

    public partial class MenuGroupRepository : DapperRepository<MenuGroup>, IMenuGroupRepository
    {
        public MenuGroupRepository(IDbConnection connection, IDbTransaction transaction)
            : base(connection, transaction)
        {
        }

    }
}
