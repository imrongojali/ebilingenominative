﻿using Agit.Domain;
using Agit.Mvc;
using Kendo.Mvc.UI;
using System.Data;
using Dapper;
using System.Collections.Generic;
using System.Linq;

namespace Tam.Ebilling.Domain
{
    public partial interface IMenuFeatureRepository : IDapperRepository<MenuFeature>
    {
        List<MenuFeature> GetMenuFeature(int menuId);
        MenuFeature GetMenuFeatureByMenuAndFeature(int menuId, string featureCode);
    }

    public partial class MenuFeatureRepository : DapperRepository<MenuFeature>, IMenuFeatureRepository
    {
        public MenuFeatureRepository(IDbConnection connection, IDbTransaction transaction)
            : base(connection, transaction)
        {
        }

        public List<MenuFeature> GetMenuFeature(int menuId)
        {
            List<MenuFeature> items = new List<MenuFeature>();

            var query = string.Format(@"
                SELECT        
                    *
                FROM
                    dbo.tb_m_feature 
                WHERE 
                    menu_id = {0}", menuId);

            items = Connection.Query<MenuFeature>(query).ToList();

            return items;
        }

        public MenuFeature GetMenuFeatureByMenuAndFeature(int menuId, string featureCode)
        {
            var query = string.Format(@"
                SELECT        
                    *
                FROM
                    dbo.tb_m_feature 
                WHERE 
                    menu_id = {0} and feature_code = '{1}' ", menuId, featureCode);

            return Connection.QuerySingle<MenuFeature>(query);
        }
    }
}
