﻿
using Kendo.Mvc.UI;
using System.Data;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Text;
using System.Collections;

namespace Tam.Ebilling.Domain
{
    public partial interface IInvoiceRepository : IDapperRepository<Invoice>
    {
        DataSourceResult GetDataSourceResult(DataSourceRequest request, object parameter = null);
        // DataSourceResult GetDataUrl(DataSourceRequest request, object parameter = null);
        List<Invoice> GetUpdate(string daNumber, string user);
        List<Invoice> GetUpdateInvoice(string invoice_number, string user);
        List<Invoice> GetUploadNotur(string reff);
        //AD 20200311
        List<Invoice> GetUploadNoturTTD(string reff);
        List<Invoice> GetUploadNoturDGT(string reff);
        //end
        //Add by AD 06022021
        List<Invoice> DeleteUploadNoturOri(string reff);
        List<Invoice> DeleteUploadNoturTTD(string reff);
        List<Invoice> DeleteUploadNoturDGT(string reff);
        //end
        List<AttachmentNotaRetur> GetRowStatus(string daNumber, string filename);
        List<DebitAdvice> GetUpdateOther(string daNumber);
        DataSourceResult GetViewDataSourceResult(DataSourceRequest request, int invoice_type, string customer_group, string checkedRecords = "", bool checkedAll = true, object parameter = null,string notifFilter = null);

        #region AD ADD 23042021
        DataSourceResult GetViewDataSourceResultDownloadCA(DataSourceRequest request, int invoice_type, string customer_group, string checkedRecords = "", bool checkedAll = true, object parameter = null, string notifFilter = null);

        #endregion

        //tes AD
        List<Invoice> GetViewDataSourceResultVAT( int invoice_type, string customer_group, object parameter = null);
        //end
        DataSourceResult GetGridInitDataSourceResult(DataSourceRequest request, int invoice_type, string customer_group, object parameter = null);
        List<Invoice> GetInvoiceByDlStatus(string status, int invoice_type);
        List<Invoice> GetInvoiceByMv(int invoice_type);
        List<Invoice> GetInvoiceUser();
        List<Invoice> GetInvoiceByDueDate(string status, int invoice_type);
        List<Invoice> GetDueDateData(string customer_group);
        List<Invoice> GetInvoiceDownload(string status, string customer_group);
        List<Invoice> GetInvoice(string customer_group, string invoice_number = null, int? business_unit_id = null, int? transaction_type_id = null, string date_from = null, string date_to = null, string ar_status = null, int? document_status = null);
        List<Invoice> GetDataMV(string customer_group);
        List<Invoice> GetDueDateInvoice(string customer_group);
        List<Invoice> GetCust(int invoice_type);
        Invoice GetInvoiceByDaNumber(string da_no);
        Invoice GetInvoiceByCaNumberDaNumber(string invoiceType, string caNo, string daNo);
        List<Invoice> GetInvoiceByInvoiceNumber(string invoiceType, string caNo);
        List<Invoice> UpdateSparePartStatusDownload(string daNumber, string user, string downloadType, string invoiceType);
        List<Invoice> GetUploadSpCaRetur(string reff);
        Invoice GetSingleInvoiceByInvoiceNumber(string invoiceType, string invoiceNo);
       
        List<Invoice> GetSummaryInvoice(string dateFrom, string dateTo, string businessType, string customerGroup, string totalAmount);
        List<Invoice> GetReceipt(string dateFrom, string dateTo, string businessType, string customerGroup, string totalAmount);

        List<Invoice> GetInvoicesAvailableYesterday(string invoiceType);
        List<Invoice> GetInvoicesDueDate(string invoiceType, string dueDate);
        List<Invoice> GetInvoicesDueDateVAT(string invoiceType, string dueDate);
    }

    public partial class InvoiceRepository : DapperRepository<Invoice>, IInvoiceRepository
    {
        public InvoiceRepository(IDbConnection connection, IDbTransaction transaction)
            : base(connection, transaction)
        {
        }

        public DataSourceResult GetDataSourceResult(DataSourceRequest request, object parameter = null)
        {
            var genericDataTableQuery = new GenericDataSourceQuery(Connection, request);
            var output = genericDataTableQuery.GetData<Invoice>();
            return output;
        }
        public List<Invoice> GetDueDateInvoice(string customer_group)
        {
            var fetchquery = string.Empty;
            var query = string.Empty;
            string customer_gr = "'" + customer_group + "'";

            fetchquery = @"select due_date, COUNT(*) as invoice_number from udf_Invoice(1, {0}) 
                               WHERE ar_status_name != 'Settlement' and customer_group is not null  group by due_date";
            query = string.Format(fetchquery, customer_gr);
            
            //var genericDataTableQuery = new GenericDataSourceQuery(Connection, request);
            //var output = genericDataTableQuery.GetData<Invoice>(query);

            var output = Connection.Query<Invoice>(query).ToList();

            return output;
        }

        public DataSourceResult GetGridInitDataSourceResult(DataSourceRequest request, int invoice_type, string customer_group, object parameter = null)
        {
            //StringBuilder fetchquery = null;

            // NEW Function AD
            //var fetchquery = @"select * from udf_Invoice (1,'{0}') ";
            var fetchquery = @"udf_Invoice(1,'{0}')";

            var query = string.Format(fetchquery, customer_group);

            var genericDataTableQuery = new GenericDataSourceQuery(Connection, request);
            var output = genericDataTableQuery.GetData<Invoice>(query);
                
            return output;
            //END
        }


        #region AD ADD 23042021
        public DataSourceResult GetViewDataSourceResultDownloadCA(DataSourceRequest request, int invoice_type, string customer_group, string checkedRecords = "", bool checkedAll = true, object parameter = null, string notifFilter = null)
        {
            string chk = "";
            string chk2 = "";

            if (checkedAll == false)
            {
                string[] list = checkedRecords.Split(',');
                var idTemp = new ArrayList();
                foreach (string d in list)
                
                {
                    idTemp.Add("'" + d + "'");
                }
                var param = string.Join(",", idTemp.ToArray());
                chk = " and cast(id as varchar(100)) in (" + param.ToString() + ")";
                chk2 = " and cast(id as varchar(100)) in (" + param.ToString() + ") group by credit_note_number, business_unit, invoice_number,dormitory,vat_dormitory,transaction_type,doc_date,due_date,currency,tax_invoice,amount_pph,pph_no,ar_status_name,da_code,customer_name, ([TAM_EFAKTUR].dbo.[udf_GetColorFromParam]('EBILL_VATCN',invoice_number,tax_date,NULL,creditnotettd,creditnotedgt,NULL,business_unit,NULL,doc_date,due_date,downloaded_date,downloadedscan_date,pi_downloaded_date,TanggalRetur,TglFaktuPajakYangDiRetur,null,null))";
            }
            else
            {
                string[] list = checkedRecords.Split(',');
                var idTemp = new ArrayList();
                foreach (string d in list)
                {
                    idTemp.Add("'" + d + "'");
                }
                var param = string.Join(",", idTemp.ToArray());
                //chk2 = " and cast(id as varchar(100)) in (" + param.ToString() + ") group by invoice_number,dormitory,vat_dormitory,transaction_type,doc_date,due_date,currency,tax_invoice,amount_pph,pph_no,ar_status_name,da_code,customer_name";
                //chk2 = " and cast(id as varchar(100)) in (" + param.ToString() + ") group by credit_note_number,business_unit,invoice_number,dormitory,vat_dormitory,transaction_type,doc_date,due_date,currency,tax_invoice,amount_pph,pph_no,ar_status_name,da_code,customer_name, ([TAM_EFAKTUR].dbo.[udf_GetColorFromParam]('EBILL_VATCN',invoice_number,tax_date,NULL,creditnotettd,creditnotedgt,NULL,business_unit,NULL,doc_date,due_date,downloaded_date,downloadedscan_date,pi_downloaded_date,TanggalRetur,TglFaktuPajakYangDiRetur,null,null))";
                chk2 = " and cast(id as varchar(100)) in (" + param.ToString() + ") group by credit_note_number, business_unit, invoice_number,dormitory,vat_dormitory,transaction_type,doc_date,due_date,currency,tax_invoice,amount_pph,pph_no,ar_status_name,da_code,customer_name, ([TAM_EFAKTUR].dbo.[udf_GetColorFromParam]('EBILL_VATCN',invoice_number,tax_date,NULL,creditnotettd,creditnotedgt,NULL,business_unit,NULL,doc_date,due_date,downloaded_date,downloadedscan_date,pi_downloaded_date,TanggalRetur,TglFaktuPajakYangDiRetur,null,null))";

            }

            //if (notifFilter != null)
            //{
            //    if (notifFilter != "")
            //    {

            //        chk2 = " and PARSENAME(REPLACE(([TAM_EFAKTUR].dbo.[udf_GetColorFromParam]('EBILL_VATCN',invoice_number,tax_date,NULL,creditnotettd,creditnotedgt,NULL,business_unit,NULL,doc_date,due_date,downloaded_date,downloadedscan_date,pi_downloaded_date,TanggalRetur,TglFaktuPajakYangDiRetur,null,null)),'|', '.'), 1) = '" + notifFilter + "' ";
            //    }
            //}
            //var fetchquery = @"select distinct  *, ([TAM_EFAKTUR].dbo.[udf_GetColorFromParam]('EBILL_VATCN',invoice_number,tax_date,NULL,creditnotettd,creditnotedgt,NULL,business_unit,NULL,doc_date,due_date,downloaded_date,downloadedscan_date,pi_downloaded_date,TanggalRetur,TglFaktuPajakYangDiRetur,null,null))  as credit_note_status from udf_Invoice ('{0}','{1}') where 1=1   " + chk + " ";
            //var fetchquery = @"select distinct  *, ([TAM_EFAKTUR].dbo.[udf_GetColorFromParam]('EBILL_VATCN',invoice_number,tax_date,NULL,creditnotettd,creditnotedgt,NULL,business_unit,NULL,doc_date,due_date,downloaded_date,downloadedscan_date,pi_downloaded_date,TanggalRetur,TglFaktuPajakYangDiRetur,null,null))  as credit_note_status from udf_Invoice ('{0}','{1}') where 1=1   " + chk + chk2 + " ";
            //var fetchquery = @"select invoice_number,sum(DPP) as DPP from udf_Invoice ('{0}','{1}') where 1=1 order by invoice_number" + chk + " ";
            //var fetchquery = @"select invoice_number, sum(DPP) as DPP,sum(tax) as tax_amount,sum(total_amount)as total_amount,dormitory,vat_dormitory,transaction_type,doc_date,due_date,currency,tax_invoice,amount_pph,pph_no,ar_status_name,da_code,customer_name from udf_Invoice (2,'') where 1=1" + chk + chk2 + " ";
            var fetchquery = @"select credit_note_number, business_unit,invoice_number,sum(DPP) as DPP,sum(tax) as tax,sum(total_amount)as total_amount,dormitory,vat_dormitory,transaction_type,doc_date,due_date,currency,tax_invoice,amount_pph,pph_no,ar_status_name,da_code,customer_name, ([TAM_EFAKTUR].dbo.[udf_GetColorFromParam]('EBILL_VATCN',invoice_number,tax_date,NULL,creditnotettd,creditnotedgt,NULL,business_unit,NULL,doc_date,due_date,downloaded_date,downloadedscan_date,pi_downloaded_date,TanggalRetur,TglFaktuPajakYangDiRetur,null,null)) as credit_note_status from udf_Invoice ({0},'{1}')  where 1=1 " + chk + chk2 + " ";
            var query = string.Format(fetchquery, invoice_type, customer_group);

            var genericDataTableQuery = new GenericDataSourceQuery(Connection, request);
            var output = genericDataTableQuery.GetData<Invoice>(query);



            return output;
        }
        #endregion


        public DataSourceResult GetViewDataSourceResult(DataSourceRequest request, int invoice_type, string customer_group, string checkedRecords = "", bool checkedAll = true, object parameter = null, string notifFilter = null)
        {
            string chk = "";
            string chk2 = "";

            if (checkedAll == false)
            {
                string[] list = checkedRecords.Split(',');
                var idTemp = new ArrayList();
                foreach (string d in list)
                {
                    idTemp.Add("'" + d + "'");
                }
                var param = string.Join(",", idTemp.ToArray());
                chk = "and cast(id as varchar(100)) in (" + param.ToString() + ")";
            }

            if (notifFilter != null)
            {
                if (notifFilter != "")
                {
                   
                    chk2 = " and PARSENAME(REPLACE(([TAM_EFAKTUR].dbo.[udf_GetColorFromParam]('EBILL_VATCN',invoice_number,tax_date,NULL,creditnotettd,creditnotedgt,NULL,business_unit,NULL,doc_date,due_date,downloaded_date,downloadedscan_date,pi_downloaded_date,TanggalRetur,TglFaktuPajakYangDiRetur,null,null)),'|', '.'), 1) = '" + notifFilter + "' ";
                }
            }
            var fetchquery = @"select distinct  *, ([TAM_EFAKTUR].dbo.[udf_GetColorFromParam]('EBILL_VATCN',invoice_number,tax_date,NULL,creditnotettd,creditnotedgt,NULL,business_unit,NULL,doc_date,due_date,downloaded_date,downloadedscan_date,pi_downloaded_date,TanggalRetur,TglFaktuPajakYangDiRetur,null,null))  as credit_note_status from udf_Invoice ('{0}','{1}') where 1=1   " + chk + chk2 + " ";
            //var fetchquery = @"select distinct  *, ([TAM_EFAKTUR].dbo.[udf_GetColorFromParam]('EBILL_VATCN',invoice_number,tax_date,NULL,creditnotettd,creditnotedgt,NULL,business_unit,NULL,doc_date,due_date,downloaded_date,downloadedscan_date,pi_downloaded_date,TanggalRetur,TglFaktuPajakYangDiRetur,null,null))  as credit_note_status from udf_Invoice ('{0}','{1}') where 1=1   " + chk + chk2 + " ";

            var query = string.Format(fetchquery, invoice_type, customer_group);

            var genericDataTableQuery = new GenericDataSourceQuery(Connection, request);
            var output = genericDataTableQuery.GetData<Invoice>(query);

            

            return output;
        }

        //TES AD
        public List<Invoice> GetViewDataSourceResultVAT( int invoice_type, string customer_group, object parameter = null)
        {
            var fetchquery = @"select * from udf_Invoice_VATCR ('{0}','{1}')";

            var query = string.Format(fetchquery, invoice_type, customer_group);
            var output = Connection.Query<Invoice>(query).ToList();


            return output;
        }
        //END

        //private void ModifyFilters(IEnumerable<Kendo.Mvc.IFilterDescriptor> filters)
        //{
        //    if (filters.Any())
        //    {
        //        foreach (var filter in filters)
        //        {
        //            var tgl = filter as Kendo.Mvc.FilterDescriptor;
        //            if (tgl != null && tgl.Member== "doc_date")
        //            {
        //                tgl.Member = "doc_date";
        //            }
        //            else if (filter is Kendo.Mvc.CompositeFilterDescriptor)
        //            {
        //                ModifyFilters(((Kendo.Mvc.CompositeFilterDescriptor)filter).FilterDescriptors);
        //            }
        //        }
        //    }
        //}

        public DataSourceResult GetDataUrl(DataSourceRequest request, string inv_number)
        {
            //var genericDataTableQuery = new GenericDataSourceQuery(Connection, request);
            //var output = genericDataTableQuery.GetData<Invoice>();
            var parameters = new Dictionary<string, object>();
            parameters.Add("invoice_number", inv_number);
            var genericDataTableQuery = new GenericDataSourceQuery(Connection, request);
            var output = genericDataTableQuery.GetData<Comment>(parameters);

            return output;
        }
        public List<Invoice> GetUpdate(string daNumber, string user)
        {
           
            var parameters = new Dictionary<string, object>();
            parameters.Add("daNumber", daNumber);
            parameters.Add("user", user);
            var output = Connection.Query<Invoice>("usp_UpdateDownloaded", parameters, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);

            return output.ToList();
        }
        public List<Invoice> UpdateSparePartStatusDownload(string daNumber, string user, string downloadType, string invoiceType)
        {

            var parameters = new Dictionary<string, object>();
            parameters.Add("daNumber", daNumber);
            parameters.Add("user", user);
            parameters.Add("downloadType", downloadType);
            parameters.Add("invoiceType", invoiceType);
            var output = Connection.Query<Invoice>("usp_UpdateSparePartDownloaded", parameters, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);

            return output.ToList();
        }
        public List<Invoice> GetUpdateInvoice(string invoice_number, string user)
        {

            var parameters = new Dictionary<string, object>();
            parameters.Add("invoice_number", invoice_number);
            parameters.Add("user", user);
            var output = Connection.Query<Invoice>("usp_UpdateInvoiceScanDownloaded", parameters, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);

            return output.ToList();
        }
        public List<DebitAdvice> GetUpdateOther(string daNumber)
        {

            var parameters = new Dictionary<string, object>();
            parameters.Add("daNumber", daNumber);
            var output = Connection.Query<DebitAdvice>("usp_UpdateDownloaded", parameters, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);

            return output.ToList();
        }
        public List<Invoice> GetUploadNotur(string reff)
        {

            var parameters = new Dictionary<string, object>();
            //var parameterpdate = parameters.Values;
            parameters.Add("reff",reff);
            var output = Connection.Query<Invoice>("usp_UpdateNoturUploaded", parameters, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);

            return output.ToList();
        }

        #region AD 2020 SP TTD dan DGT
        public List<Invoice> GetUploadNoturTTD(string reff)
        {

            var parameters = new Dictionary<string, object>();
            parameters.Add("reff", reff);
            var output = Connection.Query<Invoice>("usp_UpdateNoturUploadedTTD", parameters, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);

            return output.ToList();
        }

        public List<Invoice> GetUploadNoturDGT(string reff)
        {

            var parameters = new Dictionary<string, object>();
            parameters.Add("reff", reff);
            var output = Connection.Query<Invoice>("usp_UpdateNoturUploadedDGT", parameters, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);

            return output.ToList();
        }
        #endregion

        #region Function Delete Notur All 06022021 by AD
        public List<Invoice> DeleteUploadNoturOri(string reff)
        {

            var parameters = new Dictionary<string, object>();
            parameters.Add("reff", reff);
            var output = Connection.Query<Invoice>("usp_DeleteNoturUploadedOri", parameters, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);

            return output.ToList();
        }
        public List<Invoice> DeleteUploadNoturTTD(string reff)
        {

            var parameters = new Dictionary<string, object>();
            parameters.Add("reff", reff);
            var output = Connection.Query<Invoice>("usp_DeleteNoturUploadedTTD", parameters, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);

            return output.ToList();
        }
        public List<Invoice> DeleteUploadNoturDGT(string reff)
        {

            var parameters = new Dictionary<string, object>();
            parameters.Add("reff", reff);
            var output = Connection.Query<Invoice>("usp_DeleteNoturUploadedDGT", parameters, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);

            return output.ToList();
        }
        #endregion

        public List<Invoice> GetUploadSpCaRetur(string reff)
        {

            var parameters = new Dictionary<string, object>();
            parameters.Add("reff", reff);
            var output = Connection.Query<Invoice>("usp_UpdateSpCaReturUploaded", parameters, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);

            return output.ToList();
        }
        public List<AttachmentNotaRetur> GetRowStatus(string daNumber, string filename)
        {

            var parameters = new Dictionary<string, object>();
            parameters.Add("daNumber", daNumber);
            parameters.Add("filename", filename);
            var output = Connection.Query<AttachmentNotaRetur>("usp_UpdateRowStatusNoturUpload", parameters, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);

            return output.ToList();
        }

        public List<Invoice> GetInvoiceByDlStatus(string status, int invoice_type)
        {
            var fetchquery = string.Empty;
            var query = string.Empty;

            if (status == null)
            {
                fetchquery = @"SELECT distinct customer_group  FROM udf_Invoice({0}, '') WHERE inv_downloaded is null and customer_group is not null";
                query = string.Format(fetchquery, invoice_type);
            }
            else
            {
                fetchquery = @"SELECT distinct customer_group FROM udf_Invoice({0}, '') WHERE inv_downloaded = {2} and customer_group is not null";
                query = string.Format(fetchquery, invoice_type, status);
            }

            //var genericDataTableQuery = new GenericDataSourceQuery(Connection, request);
            //var output = genericDataTableQuery.GetData<Invoice>(query);

            var output = Connection.Query<Invoice>(query).ToList();

            return output;
        }
        public List<Invoice> GetInvoiceUser()
        {
            var fetchquery = string.Empty;
            var query = string.Empty;

           
                fetchquery = @"SELECT email from tb_m_customer_id where user_id in ('erfelinda.n','ulfah','arifudin','ANDIPE102088','Ryan.Permadi','Isnaini.Khalifah')";
                query = string.Format(fetchquery);

            fetchquery = @"SELECT email from tb_m_customer_id where user_id in ('erfelinda.n','ulfah','arifudin','ANDIPE102088','Ryan.Permadi','Isnaini.Khalifah')";
            query = string.Format(fetchquery);
            

            var output = Connection.Query<Invoice>(query).ToList();

            return output;
        }

	public List<Invoice> GetInvoiceByMv(int invoice_type)
        {
            var fetchquery = string.Empty;
            var query = string.Empty;

           
                fetchquery = @"SELECT distinct customer_group  FROM udf_Invoice({0}, '') WHERE customer_group='TAM'";
                query = string.Format(fetchquery, invoice_type);
           
               
            //var genericDataTableQuery = new GenericDataSourceQuery(Connection, request);
            //var output = genericDataTableQuery.GetData<Invoice>(query);

            var output = Connection.Query<Invoice>(query).ToList();

            return output;
        }
        public List<Invoice> GetInvoiceUserMV()
        {
            var fetchquery = string.Empty;
            var query = string.Empty;


            fetchquery = @"SELECT email from tb_m_customer_id where user_id in ('erfelinda.n','ulfah','arifudin','ANDIPE102088','Ryan.Permadi','Isnaini.Khalifah')";
            query = string.Format(fetchquery);

            fetchquery = @"SELECT email from tb_m_customer_id where user_id in ('erfelinda.n','ulfah','arifudin','ANDIPE102088','Ryan.Permadi','Isnaini.Khalifah')";
            query = string.Format(fetchquery);


            var output = Connection.Query<Invoice>(query).ToList();

            return output;
        }
        public List<Invoice> GetCust(int invoice_type)
        {
            var fetchquery = string.Empty;
            var query = string.Empty;

                fetchquery = @"SELECT distinct customer_group  FROM udf_Invoice({0}, '') WHERE due_date is not null and customer_group is not null";
                query = string.Format(fetchquery, invoice_type);
           
               
            

            //var genericDataTableQuery = new GenericDataSourceQuery(Connection, request);
            //var output = genericDataTableQuery.GetData<Invoice>(query);

            var output = Connection.Query<Invoice>(query).ToList();

            return output;
        }

        public List<Invoice> GetInvoiceByDueDate(string status, int invoice_type)
        {
            var fetchquery = string.Empty;
            var query = string.Empty;

            if (status == null)
            {
                fetchquery = @"SELECT distinct customer_group  FROM udf_Invoice({0}, '') where ar_status_name != 'Settlement' and customer_group is not null";
                query = string.Format(fetchquery, invoice_type);
            }
            else
            {
                fetchquery = @"SELECT distinct customer_group FROM udf_Invoice({0}, '')where ar_status_name != 'Settlement' and customer_group is not null";
                query = string.Format(fetchquery, invoice_type, status);
            }

            //var genericDataTableQuery = new GenericDataSourceQuery(Connection, request);
            //var output = genericDataTableQuery.GetData<Invoice>(query);

            var output = Connection.Query<Invoice>(query).ToList();

            return output;
        }

        public Invoice GetInvoiceByDaNumber(string da_no)
        {
            string query = string.Format(@"SELECT * FROM udf_invoice('', '') WHERE da_no = '{0}'", da_no);
            return Connection.Query<Invoice>(query).FirstOrDefault();
        }
        
        public Invoice GetInvoiceByCaNumberDaNumber(string invoiceType, string caNo, string daNo)
        {
            string query = string.Format(@"SELECT * FROM udf_invoice('{0}', '') WHERE invoice_number = '{1}' and da_no = '{2}'", invoiceType, caNo, daNo);
            return Connection.Query<Invoice>(query).FirstOrDefault();
        }

        public List<Invoice> GetInvoiceByInvoiceNumber(string invoiceType, string invoiceNo)
        {
            string query = string.Format(@"SELECT * FROM udf_invoice('{0}', '') WHERE invoice_number = '{1}' ", invoiceType, invoiceNo);
            return Connection.Query<Invoice>(query).ToList();
        }

        public Invoice GetSingleInvoiceByInvoiceNumber(string invoiceType, string invoiceNo)
        {
            string query = string.Format(@"SELECT * FROM udf_invoice('{0}', '') WHERE invoice_number = '{1}' ", invoiceType, invoiceNo);
            return Connection.Query<Invoice>(query).FirstOrDefault();
        }

        public List<Invoice> GetDueDateData(string customer_group)
        {
            string query = string.Format(@"select ar_status_name, invoice_number, doc_date, due_date, total_amount from udf_Invoice(1, '') 
			                where ar_status_name != 'Settlement' and customer_group = '{0}' ", customer_group);

            var output = Connection.Query<Invoice>(query).ToList();

            return output;
        }

        public List<Invoice> GetSummaryInvoice(string dateFrom, string dateTo,string businessType, string customerGroup, string totalAmount)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("dateFrom", dateFrom);
            parameters.Add("dateTo", dateTo);
            parameters.Add("businessType", businessType);
            parameters.Add("customerId", customerGroup);
            parameters.Add("totAmount", totalAmount);

            var output = Connection.Query<Invoice>("usp_Print_SummaryInvoice", parameters, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);

            return output.ToList();
        }

        public List<Invoice> GetReceipt(string dateFrom, string dateTo, string businessType, string customerGroup , string totalAmount)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("dateFrom", dateFrom);
            parameters.Add("dateTo", dateTo);
            parameters.Add("businessType", businessType);
            parameters.Add("customerId", customerGroup);
            parameters.Add("totAmount", totalAmount);



            // TODO change to date settlement in sp usp_Print_Receipt
            var output = Connection.Query<Invoice>("usp_Print_Receipt", parameters, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);

            return output.ToList();
        }

        public List<Invoice> GetInvoiceDownload(string status, string customer_group)
        {
            var fetchquery = string.Empty;
            var query = string.Empty;
            string customer_gr = "'"+customer_group+"'";

            if (status == "download")
            {
                fetchquery = @"select inv_downloaded, COUNT(*) as invoice_number from udf_Invoice(1, {0}) 
                               WHERE inv_downloaded is not null and customer_group is not null  group by inv_downloaded ";
                query = string.Format(fetchquery, customer_gr);
            }
            else if (status == "notdownload")
            {
                fetchquery = @"select inv_downloaded, COUNT(*) as invoice_number from udf_Invoice(1, {0}) 
                               WHERE inv_downloaded is null and customer_group is not null  group by inv_downloaded ";
                query = string.Format(fetchquery, customer_gr);
            }

            //var genericDataTableQuery = new GenericDataSourceQuery(Connection, request);
            //var output = genericDataTableQuery.GetData<Invoice>(query);

            var output = Connection.Query<Invoice>(query).ToList();

            return output;
        }

        public List<Invoice> GetInvoice(string customer_group, string invoice_number = null, int? business_unit_id = null, int? transaction_type_id = null, string date_from = null, string date_to = null, string ar_status = null, int? document_status = null)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("TYPE", 1);
            parameters.Add("CUSTOMER_GROUP", customer_group);
            parameters.Add("BUSINESS_UNIT_ID", business_unit_id);
            parameters.Add("INVOICE_NUMBER", invoice_number);
            parameters.Add("TRANSACTION_TYPE_ID", transaction_type_id);
            parameters.Add("DATE_FROM", date_from);
            parameters.Add("DATE_TO", date_to);
            parameters.Add("AR_STATUS", ar_status);
            parameters.Add("DOCUMENT_STATUS", document_status);

            var output = Connection.Query<Invoice>("usp_GetInvoice", parameters, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);

            return output.ToList();
        }

        public List<Invoice> GetDataMV(string customer_group)
        {
            var fetchquery = string.Empty;
            var query = string.Empty;

          
                fetchquery = @"SELECT distinct 
	a.business_unit,
	a.business_unit_id,
	a.invoice_number,
	a.transaction_type,
	a.transaction_type_id,
	a.doc_date,
	a.due_date,
	a.currency,
	a.dpp,
	a.tax,
	a.total_amount,
	a.ar_status,
	a.tax_invoice,
	a.pph_no,
	a.da_code,
	a.da_no,
	a.customer_code,
	a.customer_name,
	a.customer_group,
	a.exc_rate,
	sum(a.amount_pph) as amount_pph,
	a.inv_downloaded,
	a.notur_downloaded,
	a.notur_uploaded,
	a.downloaded_date,
	a.downloaded_by
FROM udf_Invoice('{0}','{0}') a
";
                query = string.Format(fetchquery, customer_group);
         
              

            var output = Connection.Query<Invoice>(query).ToList();

            return output;
        }

        public List<Invoice> GetInvoicesAvailableYesterday(string invoiceType)
        {
            string query = string.Format(@"SELECT * FROM udf_invoice('{0}', '') where doc_date = dateadd(day,datediff(day,1,GETDATE()),0) ", invoiceType);
            return Connection.Query<Invoice>(query).ToList();
        }

        public List<Invoice> GetInvoicesDueDate(string invoiceType, string dueDate)
        {
            string query = string.Format(@"
            SELECT
	            *
            FROM udf_invoice('{0}', '')
            WHERE DATEDIFF(DAY, doc_date, GETDATE()) >= {1}
            AND (notur_uploaded IS NULL
            OR notur_uploaded = 0)"
            , invoiceType, dueDate);
            return Connection.Query<Invoice>(query).ToList();
        }

        public List<Invoice> GetInvoicesDueDateVAT(string invoiceType, string dueDate)
        {
            string query = string.Format(@"
            SELECT
	            *
            FROM udf_invoice('{0}', '')
            WHERE DATEDIFF(DAY, doc_date, GETDATE()) >= '{1}'
            AND (notur_uploaded IS NULL
            OR notur_uploaded = 0)
            AND customer_code not in
            (
	            select value1 
	            from tb_m_parameter 
	            where key_param ='exclude_vat'
            )"
            , invoiceType, dueDate);
            return Connection.Query<Invoice>(query).ToList();
        }
    }
}
