﻿using System;
using Kendo.Mvc.UI;
using System.Data;
using Dapper;
using Tam.Ebilling.Domain.Modules.Core.Model;
using System.Collections.Generic;
using System.Linq;

namespace Tam.Ebilling.Domain
{
    public partial interface IAttachmentNotaReturRepository : IDapperRepository<AttachmentNotaRetur>
    {
        List<AttachmentNotaRetur> UploadNotur(string filename, string reference, string typeDoc);
        List<AttachmentNotaRetur> Remove(long attachment_notaretur_id);
        DataSourceResult GetDataSourceResult(DataSourceRequest request, object parameter = null);
        List<AttachmentNotaRetur> UploadSpCaRetur(string filename, string reference);
        List<AttachmentNotaRetur> RemoveSpCaNotaRetur(long attachment_notaretur_id);

        List<AttachmentNotaRetur> Cekdata(string reference);
    }

    public partial class AttachmentNotaReturRepository : DapperRepository<AttachmentNotaRetur>, IAttachmentNotaReturRepository
    {
        public AttachmentNotaReturRepository(IDbConnection connection, IDbTransaction transaction)
            : base(connection, transaction)
        {
        }

        public DataSourceResult GetDataSourceResult(DataSourceRequest request, object parameter = null)
        {
            var genericDataTableQuery = new GenericDataSourceQuery(Connection, request);
            var output = genericDataTableQuery.GetData<AttachmentNotaRetur>();

            return output;
        }
        public DataSourceResult GetDetailByReference(DataSourceRequest request, string reference, object parameter = null)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("reference", reference);
            var genericDataTableQuery = new GenericDataSourceQuery(Connection, request);
            var output = genericDataTableQuery.GetData<AttachmentNotaRetur>(parameters);

            return output;
        }
        public List<AttachmentNotaRetur> UploadNotur(string filename, string reference, string typeDoc)
        {

            var parameters = new Dictionary<string, object>();
            parameters.Add("filename", filename);
            parameters.Add("reference", reference);
            parameters.Add("document_type", typeDoc);
            var output = Connection.Query<AttachmentNotaRetur>("usp_UploadNotaRetur", parameters, null, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);

            return output.ToList();
        }
        public List<AttachmentNotaRetur> UploadSpCaRetur(string filename, string reference)
        {

            var parameters = new Dictionary<string, object>();
            parameters.Add("filename", filename);
            parameters.Add("reference", reference);
            var output = Connection.Query<AttachmentNotaRetur>("usp_UploadSpCaRetur", parameters, null, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);

            return output.ToList();
        }
        //public List<AttachmentNotaRetur> UploadNotur(long attachment_notaretur_id, string filename, string reference)
        //{
        //    var transact = Connection.BeginTransaction();
        //    var output = Connection.Query<AttachmentNotaRetur>("usp_UploadNotaRetur", commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);
        //    try
        //    {

        //        transact.Commit();
        //    }
        //    catch (Exception e)
        //    {
        //        transact.Rollback();
        //    }
        //    return output.ToList();
        //}

        public List<AttachmentNotaRetur> Remove(long attachment_notaretur_id)
        {

            var parameters = new Dictionary<string, object>();
            parameters.Add("attachment_notaretur_id", attachment_notaretur_id);
            var output = Connection.Query<AttachmentNotaRetur>("usp_DeleteUpload", parameters, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);

            return output.ToList();
        }

        public List<AttachmentNotaRetur> RemoveSpCaNotaRetur(long attachment_notaretur_id)
        {

            var parameters = new Dictionary<string, object>();
            parameters.Add("attachment_spcaretur_id", attachment_notaretur_id);
            var output = Connection.Query<AttachmentNotaRetur>("usp_DeleteUploadSpCaRetur", parameters, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);

            return output.ToList();
        }
        public List<AttachmentNotaRetur> Cekdata(string reference)
        {

            var parameters = new Dictionary<string, object>();
            parameters.Add("reference", reference);
            var output = Connection.Query<AttachmentNotaRetur>("usp_GetDataCreditNumber", parameters, null, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);

            return output.ToList();
        }
    }
}
