﻿using Agit.Domain;
using Agit.Mvc;
using Kendo.Mvc.UI;
using System.Data;

namespace Tam.Ebilling.Domain
{
    public partial interface IEmailTemplateRepository : IDapperRepository<EmailTemplate>
    {
        DataSourceResult GetDataSourceResult(DataSourceRequest request, object parameter = null);
    }

    public partial class EmailTemplateRepository : DapperRepository<EmailTemplate>, IEmailTemplateRepository
    {
        public EmailTemplateRepository(IDbConnection connection, IDbTransaction transaction)
            : base(connection, transaction)
        {
        }

        public DataSourceResult GetDataSourceResult(DataSourceRequest request, object parameter = null)
        {
            var genericDataTableQuery = new GenericDataSourceQuery(Connection, request);
            var output = genericDataTableQuery.GetData<EmailTemplate>();

            return output;
        }

    }
}
