﻿using Agit.Domain;
using Agit.Mvc;
using Kendo.Mvc.UI;
using System.Data;
using Dapper;
using System.Collections.Generic;
using System.Linq;

namespace Tam.Ebilling.Domain
{
    public partial interface IMenuRoleRepository : IDapperRepository<MenuRole>
    {
        List<MenuRole> GetAllowedRoleByMenu(int menuId);
        bool GetAllowedByUserTypeUsername(int menuId, string userType, string user);
    }

    public partial class MenuRoleRepository : DapperRepository<MenuRole>, IMenuRoleRepository
    {
        public MenuRoleRepository(IDbConnection connection, IDbTransaction transaction)
            : base(connection, transaction)
        {
        }

        public List<MenuRole> GetAllowedRoleByMenu(int menuId)
        {
            List<MenuRole> items = new List<MenuRole>();

            var query = string.Format(@"
                SELECT        
                    *
                FROM
                    dbo.tb_r_menu_role 
                WHERE 
                    menu_id = {0}", menuId);

            items = Connection.Query<MenuRole>(query).ToList();

            return items;
        }

        //wo.apiyudin permission PV Monitoring
        public bool GetAllowedByUserTypeUsername(int menuId, string userType, string user)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@menuId", menuId);
            parameters.Add("@userType", userType);
            parameters.Add("@user", user);

            bool result = Connection.Query<bool>("usp_GetAllowedMenuBy_UserType", parameters, null, true, null, System.Data.CommandType.StoredProcedure).SingleOrDefault();

            return result;
        }
        //end
    }
}
