﻿using Kendo.Mvc.UI;
using System.Data;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Text;

using Kendo.Mvc.Extensions;

namespace Tam.Ebilling.Domain
{
    public partial interface IPVMonitoringRepository : IDapperRepository<PVMonitoring>
    {
        int Log(string _msgId, string _errMessage, string _errLocation = "", string _userID = "", string _functionID = "",
            int ID = 0, string _remarks = null, string _type = null, string _sts = null);

        List<PVHeader> GetViewDataSourceResult(string user, string roleStr, string userType, string levelCode, string statusFlag);
        
        List<string> FindEmailByLevelCode(string levelCode);
        List<string> FindEmailByUsername(string username);
        List<PVHeader> GetDataPVEmail(string pid, string expired, bool toBudget);
        List<PVHeader> GetDataPVRejected(string pvno);

        int GetPid(IList<PVMonitoring> pVMonitorings);
        int GetPid(int ID = 0, string function = "", string user = "");
        HashSet<string> ValDuplicateTaxNo(IList<PVMonitoring> pVMonitorings);
        IList<Table> GetTableProperty(string tableName);
        string InsertPVH(IDbTransaction transaction, PVHeader pvH, string user, int pid);
        void InsertPVT(IDbTransaction transaction, string user, int pid);
        void InsertPVHD(IDbTransaction transaction, string user, int pid);
        HashSet<string> GetUploadValidation(int pid);
        string InsertPVMonitoring(IDbTransaction transaction, PVMonitoring pvMonitoring, string user, int pid);

        PVHeader GetPVHeader(int pId);
        PVHeader GetPVTHeader(int pId);
        List<PVMonitoring> GetPVMonitoring(int pId);
        List<PVMonitoring> GetPVD(int pId);
        List<PVAmount> GetPVAmount(int pId);
        List<PVAmount> GetPVAmount(int pvno, int year);

        string GetDivId(string user);
        string GetStatusCmb(string val, string keyParam, bool singleString);
        List<ComboEx> GetStatusCmb(string userType, string keyParam, string levelCode);
        void DeleteUnSubmittedPVMonitoring(IDbTransaction transaction, string pvno, int delete, string user);
        void NotProper(IDbTransaction transaction, string pv_no, string reason, string user);
        HashSet<string> CompareViewGeneratorByPV(string[] pv_no);
        void VerifiedPIC(IDbTransaction transaction, string invoice_number, string user);

        HashSet<string> CheckBeforeSubmit(string[] pv_no);
        void Submit(IDbTransaction transaction, string pv_no, string user);

        List<TransactionType> GetTransactionType(string divId);
        List<ComboEx> GetVendorByInvoice(string processId, bool isAll);
        List<WBSStructure> GetBudgetNo(int div, string ficsalYear, string tc);
        void SaveAttachmentInfo(IDbTransaction transaction, PVMonitoringAttachment attachment, string userType);
        void SaveBankAttachmentInfo(IDbTransaction transaction, string vendorCode, string folderFile, string filename, string guidFilename, string userType);
        /*
        List<PVMonitoringBankAccount> GetBankProcessByInvoice(string processId);
        */
        string GetPVByProcessId(string pid);
        List<PVMonitoringAttachment> GetDocumentFileByReference(string processId, string userType);
        List<PVHDistribusionStatus> GetDistributionStatusByReference(string pvno);
        List<PVMonitoringAttachment> GetDocumentFileByPIDReff(string processId, string userType);
        void RequestBankAccount(IDbTransaction transaction, string user, string invoiceNo, string vendor, string bank, string bankAccount, string beneficiaries);
        HashSet<string> UpdatePVMonitoring(IDbTransaction transaction, IEnumerable<PVMonitoring> pvDtl, string pid, string bank_key, string user);
        /*
        string GetBankAccount(string invoice, string userType);
        bool GetBankAccount(string bank_key, string bank_account, string vendor);
        */
        void UpdateBudgetPVMonitoring(IDbTransaction transaction, int pId, string status, string budgetNo);

        string GetFullName(string user);
        List<PVCoverData> getInvoiceValue(string PV_NO, string PV_YEAR);
        List<CodeConstant> GetPVTypes(int exclude);
        List<CodeConstant> GetTransactionTypesSkipApproval();
        List<WBSStructure> WbsNumbers(string bookingID = "");
        int GetNum(string sKeyCode, int ValueDefault = 0);

        List<CodeConstant> GetCostCenter(string divId, int prodFlag);
    }

    public partial class PVMonitoringRepository : DapperRepository<PVMonitoring>, IPVMonitoringRepository
    {
        public PVMonitoringRepository(IDbConnection connection, IDbTransaction transaction)
            : base(connection, transaction)
        {
        }

        public List<PVHeader> GetViewDataSourceResult(string user, string roleStr, string userType, string levelCode, string statusFlag)
        {
            List<PVHeader> x = new List<PVHeader>();

            x = Connection.Query<PVHeader>("usp_GetGridPVHeader", new { user = user, roleStr = roleStr, userType = userType, levelCode = levelCode, statusFlag = statusFlag },
                                null, true, null, System.Data.CommandType.StoredProcedure).ToList();
            return x;
        }

        /*
        public List<PVMonitoringBankAccount> GetBankProcessByInvoice(string processId)
        {
            StringBuilder fetchquery = null;

            fetchquery = new StringBuilder(@"select distinct 
	                                            d.BANK_ACCOUNT,
	                                            upper(d.name_of_bank)[NAME_OF_BANK],
	                                            'Available' [STATUS_REQ]
                                            from TB_T_UPLOAD_H a
                                            join TB_T_UPLOAD_D c
                                                on c.PROCESS_ID = a.PROCESS_ID
                                            join sap_db.dbo.tb_m_vendor_bank d on d.VENDOR_CD = a.VENDOR_CD
                                            where 1=1
	                                            and c.PROCESS_ID = '{0}'
                                            union all
                                            select distinct 
	                                            b.BANK_ACCOUNT,
	                                            upper(b.NAME_OF_BANK)[NAME_OF_BANK],
	                                            cast(e.[description] as varchar)[STATUS_REQ]
                                            from TB_T_UPLOAD_H a
                                            left join tb_m_vendor_bank b
                                                on a.VENDOR_CD = b.VENDOR_CD
                                            left join tb_m_parameter e
	                                            on b.STATUS_REQ = e.value1
		                                            and e.key_param = 'pv_bank_status'
                                            join TB_T_UPLOAD_D c
                                                on c.PROCESS_ID = a.PROCESS_ID
                                            join sap_db.dbo.tb_m_vendor_bank d on d.VENDOR_CD = a.VENDOR_CD
                                            where 1=1
                                                and b.BANK_ACCOUNT is not null
	                                            and c.PROCESS_ID = '{1}'");

            string query = string.Format(fetchquery.ToString(), processId, processId);
            var output = Connection.Query<PVMonitoringBankAccount>(query).ToList();

            return output;
        }
        */

        public List<PVHeader> GetDataPVRejected(string pvno)
        {
            StringBuilder fetchquery = null;

            fetchquery = new StringBuilder(@"select process_id	
	                                            ,transaction_cd	
	                                            ,vendor_cd	
	                                            ,upload_filename	
	                                            ,reff_no	
	                                            ,rows_count	
	                                            ,ok	
	                                            ,created_by	
	                                            ,created_dt	
	                                            ,changed_by	
	                                            ,changed_dt	
	                                            ,bank_key	
	                                            ,bank_account	
	                                            ,pv_no	
	                                            ,status_budget	
	                                            ,bank_type	
	                                            ,rejected_reason	
	                                            ,rejected_by	
	                                            ,rejected_dt	
	                                            ,budget_no 
                                            from tb_t_upload_h 
                                            where pv_no='{0}'");

            string query = string.Format(fetchquery.ToString(), pvno);
            var output = Connection.Query<PVHeader>(query).ToList();

            return output;
        }

        public List<PVHDistribusionStatus> GetDistributionStatusByReference(string pvno)
        {
            StringBuilder fetchquery = null;

            fetchquery = new StringBuilder(@"select reff_no	
	                                              ,seq_no	
	                                              ,case when len(h.status_cd) >= 2
			                                            then (select top 1 status_name from vw_TbMStatusElvis v where v.status_cd=h.status_cd)
			                                            else cast(p.[description] as varchar) 
	                                               end [status_cd]
	                                              ,date	
	                                              ,un.fullname [created_by]
	                                              ,h.created_dt
                                            from tb_h_distribution_status h
                                            left join tb_m_parameter p 
	                                            on p.value1 = h.status_cd and p.key_param = 'pv_status'
                                            outer apply
                                            (
	                                            select top 1 * from (
		                                            select distinct username, fullname
		                                            from (
			                                            select user_id [username], upper(user_name)[fullname] from tb_m_customer_id aa where aa.user_id=h.created_by
			                                            union all
			                                            select username [username], upper(full_name)[fullname] from vw_UserElvis aa where aa.username =h.created_by
		                                            ) a
	                                            ) x
                                            ) un
                                            where reff_no = 
                                            (
	                                            select top 1 cast(pv_no as varchar) + cast(pv_year as varchar) 
	                                            from tb_r_pv_h where pv_no = '{0}'
                                            )");

            string query = string.Format(fetchquery.ToString(), pvno);
            var output = Connection.Query<PVHDistribusionStatus>(query).ToList();

            return output;
        }

        public List<PVMonitoringAttachment> GetDocumentFileByReference(string processId, string userType)
        {
            StringBuilder fetchquery = null;

            fetchquery = new StringBuilder(@"select  distinct
		                                            a.reference_no,
	                                                a.ref_seq_no,
	                                                cast(b.[description] as varchar) attach_cd,
	                                                a.[description],
	                                                a.directory,
	                                                a.[file_name],
	                                                a.created_by,
	                                                a.created_dt,
	                                                a.changed_by,
	                                                a.changed_dt,
	                                                c.pv_no,
                                                    case when '{0}' = 'Internal' then 'Y' else 'N' end [is_usertam],
                                                    a.by_internal
                                            from tb_r_pv_monitoring_attachment a
                                            join tb_t_upload_h c on a.reference_no like cast(c.pv_no as varchar) + '%'
                                            join tb_r_pv_h d on c.pv_no = d.pv_no
                                            join tb_m_parameter b on a.attach_cd = b.value1
					                                            and b.key_param = 'pv_attachment_type'
                                            where c.process_id = '{1}'
                                            and by_internal != 'Y'
                                            and case when '{2}' = 'Internal' then 
			                                            case when len(d.status_cd) = 1 then
				                                             case when d.status_cd >= 4 then 1
					                                            else 0
				                                             end
		                                                    else 1
			                                            end
                                                    else 1
	                                            end = 1
                                            union
                                            select  distinct
		                                            a.reference_no,
	                                                a.ref_seq_no,
	                                                cast(b.[description] as varchar) attach_cd,
	                                                a.[description],
	                                                a.directory,
	                                                a.[file_name],
	                                                a.created_by,
	                                                a.created_dt,
	                                                a.changed_by,
	                                                a.changed_dt,
	                                                c.pv_no,
                                                    case when '{3}' = 'Internal' then 'Y' else 'N' end [is_usertam],
                                                    a.by_internal
                                            from tb_r_pv_monitoring_attachment a
                                            join tb_t_upload_h c on a.reference_no like cast(c.pv_no as varchar) + '%'
                                            join tb_r_pv_h d on c.pv_no = d.pv_no
                                            join tb_m_parameter b on a.attach_cd = b.value1
					                                            and b.key_param = 'pv_attachment_type'
                                            where c.process_id = '{4}'
                                                 and by_internal = 'Y'
                                            ");

            string query = string.Format(fetchquery.ToString(), userType, processId, userType, userType, processId);
            var output = Connection.Query<PVMonitoringAttachment>(query).ToList();

            return output;
        }

        public List<PVMonitoringAttachment> GetDocumentFileByPIDReff(string reffNo, string userType)
        {
            StringBuilder fetchquery = null;

            fetchquery = new StringBuilder(@"select distinct
		                                            a.reference_no,
	                                                a.ref_seq_no,
	                                                cast(b.[description] as varchar) attach_cd,
	                                                a.[description],
	                                                a.directory,
	                                                a.[file_name],
	                                                a.created_by,
	                                                a.created_dt,
	                                                a.changed_by,
	                                                a.changed_dt,
                                                    case when '{0}' = 'Internal' then 'Y' else 'N' end [is_usertam],
                                                    a.by_internal
                                            from tb_r_pv_monitoring_attachment a
                                            join tb_m_parameter b on a.attach_cd = b.value1
					                                            and b.key_param = 'pv_attachment_type'
                                            where a.REFERENCE_NO = '{1}'");

            string query = string.Format(fetchquery.ToString(), userType, reffNo);
            var output = Connection.Query<PVMonitoringAttachment>(query).ToList();

            return output;
        }

        public void UpdateBudgetPVMonitoring(IDbTransaction transaction, int pId, string status, string budgetNo)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("pid", pId);
            parameters.Add("statusBudget", status);
            parameters.Add("budgetNo", budgetNo);

            var output = Connection.Execute("usp_UpdateBudgetPVMonitoring", parameters, transaction, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);
        }

        public List<TransactionType> GetTransactionType(string divId)
        {
            List<TransactionType> result = new List<TransactionType>();

            result = Connection.Query<TransactionType>("usp_GetPVTransactionType",
                    new
                    {
                        divId
                    }
                    , null, true, null, System.Data.CommandType.StoredProcedure).ToList();

            return result;
        }

        public PVHeader GetPVHeader(int pId)
        {
            PVHeader data = new PVHeader();
            
            data = Connection.Query<PVHeader>("usp_GetPVHeader",
                    new
                    {
                        pid = pId
                    }
                    , null, true, null, System.Data.CommandType.StoredProcedure).SingleOrDefault();

            return data;
        }

        public PVHeader GetPVTHeader(int pId)
        {
            PVHeader data = new PVHeader();

            data = Connection.Query<PVHeader>("usp_GetPVTHeader",
                    new
                    {
                        pid = pId
                    }
                    , null, true, null, System.Data.CommandType.StoredProcedure).SingleOrDefault();

            return data;
        }

        public List<PVHeader> GetDataPVEmail(string pid, string expired, bool toBudget)
        {
            int x = toBudget ? 1 : 0;

            List<PVHeader> data = new List<PVHeader>();

            data = Connection.Query<PVHeader>("usp_GetPVEmail",
                    new
                    {
                        pid = pid,
                        expired = expired,
                        tobudget = x
                    }, null, true, null, System.Data.CommandType.StoredProcedure).ToList();

            return data;
        }

        public List<PVMonitoring> GetPVMonitoring(int pId)
        {
            List<PVMonitoring> data = new List<PVMonitoring>();

            data = Connection.Query<PVMonitoring>("usp_GetPVMonitoring",
                    new {
                        pid = pId
                    }, null, true, null, System.Data.CommandType.StoredProcedure).ToList();
            
            return data;
        }

        public List<PVMonitoring> GetPVD(int pId)
        {
            List<PVMonitoring> data = new List<PVMonitoring>();

            data = Connection.Query<PVMonitoring>("usp_GetPVD",
                    new
                    {
                        pid = pId
                    }, null, true, null, System.Data.CommandType.StoredProcedure).ToList();

            return data;
        }

        public List<PVAmount> GetPVAmount(int pId)
        {
            List<PVAmount> data = new List<PVAmount>();

            data = Connection.Query<PVAmount>("usp_GetPVAmount",
                    new
                    {
                        pid = pId
                    }, null, true, null, System.Data.CommandType.StoredProcedure).ToList();

            return data;
        }

        public List<PVAmount> GetPVAmount(int pvno, int year)
        {
            string query = string.Format(@"SELECT 
	                                        DOC_NO,
                                            CURRENCY_CD,
	                                        DOC_YEAR,
	                                        TOTAL_AMOUNT,
	                                        EXCHANGE_RATE
                                        FROM TB_R_PVRV_AMOUNT
                                        WHERE DOC_NO = {0} AND DOC_YEAR = {1} ", pvno, year);

            var output = Connection.Query<PVAmount>(query).ToList();

            return output;
        }

        public List<WBSStructure> GetBudgetNo(int div, string ficsalYear, string tc)
        {
            if (tc == null)
            {
                return new List<WBSStructure>(0);
            }

            List<WBSStructure> wbsNumbers = new List<WBSStructure>();

            int year = DateTime.Now.Year;

            bool isGlAccountProject = Connection.Query<int>("usp_GetIsGLAccountProject",
                    new { div = div }, null, true, null, System.Data.CommandType.StoredProcedure).FirstOrDefault() > 0;

            wbsNumbers = Connection.Query<WBSStructure>("usp_GetWbsNumbers",
                            new
                            {
                                 fiscalYear = year
                                ,isFinanceDirectorate = ((div >= 100) ? 1 : 0)
                                ,division = div
                                ,isProject = (isGlAccountProject ? 1 : 0)
                                ,transactionCode = tc
                            }
                            , null, true, null, System.Data.CommandType.StoredProcedure).ToList();

            return wbsNumbers;
        }

        public List<CodeConstant> GetCostCenter(string divId="", int prodFlag=0)
        {
            StringBuilder fetchquery = null;
    
            fetchquery = new StringBuilder(@"select ltrim(rtrim(cost_center)) as [code], [description] as [description] from dbo.vw_CostCenterElvis
                                                where ((nullif('{0}', '') is null) or division = '{1}')
                                                and (('{2}' is null) or production_flag = '{3}');");

            string query = string.Format(fetchquery.ToString(), divId, divId, prodFlag, prodFlag);
            List<CodeConstant> result = Connection.Query<CodeConstant>(query).ToList();

            return result;
        }

        public List<ComboEx> GetVendorByInvoice(string processId, bool isAll)
        {
            StringBuilder fetchquery = null;

            if (!isAll)
            {
                fetchquery = new StringBuilder(@"select top 1 h.vendor_cd [code], upper(v.vendor_name) [name]
                                                    from tb_t_upload_d d
	                                                    join tb_t_upload_h h on h.PROCESS_ID = d.PROCESS_ID
	                                                    join vw_VendorSAPElvis v on h.vendor_cd = v.vendor_cd
                                                    where 1=1");

                if (!string.IsNullOrEmpty(processId))
                {
                    fetchquery.Append(" and d.process_id = '{0}'");
                }
            }
            else
            {
                fetchquery = new StringBuilder(@"select a.vendor_cd [code], b.vendor_cd, upper(b.vendor_name) [name]
                                                    from tb_t_mapp_vendor a
                                                    left join vw_VendorSAPElvis b
	                                                    on a.vendor_cd = b.vendor_cd
                                                    where 1=1
                                                        and a.is_viewed_pv_monitoring = 1
	                                                    and b.vendor_cd is not null");
            }

            string query = string.Format(fetchquery.ToString(), processId);
            List<ComboEx> result = Connection.Query<ComboEx>(query).ToList();

            return result;
        }

        public string GetFullName(string user)
        {
            StringBuilder fetchquery = null;

            fetchquery = new StringBuilder(@"select top 1 [user_name]
                                                from (
	                                                select [user_name] from tb_m_customer_id where [user_id] = '{0}'
	                                                union all
	                                                select display_name [user_name] from [tb_m_security_users] where username = '{1}'
                                                ) x");

            string query = string.Format(fetchquery.ToString(), user, user);
            var result = Connection.QuerySingleOrDefault<string>(query);

            return result;
        }

        public string GetPVByProcessId(string pid)
        {
            StringBuilder fetchquery = null;

            fetchquery = new StringBuilder(@"select pv_no from tb_t_upload_h where process_id = '{0}'");

            string query = string.Format(fetchquery.ToString(), pid);
            var result = Connection.QuerySingleOrDefault<string>(query);

            return result;
        }

        /*
        public string GetBankAccount(string processId, string userType)
        {
            StringBuilder fetchquery = null;

            fetchquery = new StringBuilder(@"select case when x.bank_type is not null
				                                            and x.bank_key is not null
			                                            then x.bank_type + ':' + x.bank_key
			                                            else ''
	                                                end [bank_key]
                                            from 
                                            (
	                                            select '' [x]
                                            ) z
                                            outer apply
                                            (
	                                            select top 1 cast(a.bank_type as varchar)[bank_type], c.bank_key [bank_key]
	                                            from tb_r_pv_h a
	                                            join tb_t_upload_h b on a.pv_no = b.pv_no
	                                            join sap_db.dbo.tb_m_vendor_bank c on c.bank_key= b.bank_key 
		                                            and c.bank_account = b.bank_account
	                                            where b.process_id='{0}'
		                                            and case when '{1}' = 'Internal' then 
					                                            case when len(a.status_cd) = 1 then
						                                                case when a.status_cd >= 4 then 1
							                                                else 0
						                                                end
				                                                    else 1
					                                            end
				                                                else 1
			                                            end = 1
                                            ) x");

            string query = string.Format(fetchquery.ToString(), processId, userType);
            var result = Connection.QuerySingleOrDefault<string>(query);

            return result;
        }

        public bool GetBankAccount(string bank_key, string bank_account, string vendor)
        {
            bool output = false;
            StringBuilder fetchquery = null;

            fetchquery = new StringBuilder(@"select top 1 BANK_ACCOUNT 
                                            from tb_m_vendor_bank
                                            where vendor_cd = '{0}'
	                                            and STATUS_REQ = '1'");

            string query = string.Format(fetchquery.ToString(), vendor);
            string result = Connection.QuerySingleOrDefault<string>(query);

            if (!string.IsNullOrEmpty(result))
            {
                output = true;
            }

            return output;
        }
        */

        public IList<Table> GetTableProperty(string tableName)
        {
            string query = string.Format(@"SELECT TABLE_SCHEMA,TABLE_NAME,COLUMN_NAME,DATA_TYPE,IS_NULLABLE,CHARACTER_MAXIMUM_LENGTH
                                                FROM INFORMATION_SCHEMA.COLUMNS
                                                WHERE TABLE_NAME = '{0}' AND TABLE_SCHEMA='dbo'", tableName);

            var output = Connection.Query<Table>(query).ToList();

            return output;
        }

        public List<string> FindEmailByLevelCode(string levelCode)
        {
            string query = string.Format(@"select domain from tb_m_security_users where level_code = '{0}'", levelCode);

            var output = Connection.Query<string>(query).ToList();

            return output;
        }

        public List<string> FindEmailByUsername(string username)
        {
            string query = string.Format(@"select domain from tb_m_security_users where username = '{0}'", username);

            var output = Connection.Query<string>(query).ToList();

            return output;
        }

        public string InsertPVH(IDbTransaction transaction, PVHeader pvH, string user, int pid)
        {
            int p_status = 0;
            string err_mesg = "";

            var parameters = new DynamicParameters();
            parameters.Add("@process_status", p_status, DbType.Int32, ParameterDirection.InputOutput);
            parameters.Add("@err_mesg", err_mesg, DbType.String, ParameterDirection.InputOutput);
            parameters.Add("pid", pid);
            parameters.Add("transaction_cd", pvH.TRANSACTION_CD);
            parameters.Add("vendor_cd", pvH.VENDOR_CD);
            parameters.Add("upload_filename", pvH.UPLOAD_FILENAME);
            parameters.Add("user", user);
            parameters.Add("budget_no", pvH.BUDGET);

            var output = Connection.Execute("usp_SavePVH", parameters, transaction, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);

            p_status = parameters.Get<int>("@process_status");
            string mesg = parameters.Get<string>("@err_mesg");

            if (p_status != 0 && mesg != null)
            {
                err_mesg = mesg;
            }

            return err_mesg;
        }

        public void InsertPVHD(IDbTransaction transaction, string user, int pid)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("pid", pid);
            parameters.Add("uid", user);

            var output = Connection.Execute("usp_InsertPVHD", parameters, transaction, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);
        }

        public void InsertPVT(IDbTransaction transaction, string user, int pid)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("pid", pid);
            parameters.Add("uid", user);

            var output = Connection.Execute("usp_PutUpload_TAXNew", parameters, transaction, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);
        }

        public void RequestBankAccount(IDbTransaction transaction, string user, string pid, string vendor, string bank, string bankAccount, string beneficiaries)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("pid", pid);
            parameters.Add("vendor", vendor);
            parameters.Add("bank", bank);
            parameters.Add("bankAccount", bankAccount);
            parameters.Add("username", user);
            parameters.Add("beneficiaries", beneficiaries);

            var output = Connection.Execute("usp_RegisterBankAccount", parameters, transaction, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);
        }

        public string InsertPVMonitoring(IDbTransaction transaction, PVMonitoring pvMonitoring, string user, int pid)
        {
            int p_status=0;
            string err_mesg = "";

            var parameters = new DynamicParameters();
            parameters.Add("@process_status", p_status, DbType.Int32, ParameterDirection.InputOutput);
            parameters.Add("@err_mesg", err_mesg, DbType.String, ParameterDirection.InputOutput);
            parameters.Add("@pidi", pid);
            parameters.Add("@seq_no", pvMonitoring.SEQ_NO);
            parameters.Add("@seq_xcl", pvMonitoring.SEQ_XCL);
            parameters.Add("@invoice_no", pvMonitoring.INVOICE_NO);
            parameters.Add("@invoice_date", pvMonitoring.INVOICE_DATE);
            parameters.Add("@tax_no", pvMonitoring.TAX_NO);
            parameters.Add("@tax_dt", pvMonitoring.TAX_DT);
            parameters.Add("@currency_cd", pvMonitoring.CURRENCY_CD);
            parameters.Add("@amount_turn_over", pvMonitoring.AMOUNT_TURN_OVER);
            parameters.Add("@amount_ppn", pvMonitoring.AMOUNT_PPN);
            parameters.Add("@materai", pvMonitoring.MATERAI);
            parameters.Add("@amount", pvMonitoring.AMOUNT);
            parameters.Add("@description", pvMonitoring.DESCRIPTION);
            parameters.Add("@account_info", pvMonitoring.ACCOUNT_INFO);
            parameters.Add("@cost_center_cd", pvMonitoring.COST_CENTER_CD);
            parameters.Add("@dpp_pph_amount", pvMonitoring.DPP_PPh_AMOUNT);
            parameters.Add("@npwp_available", pvMonitoring.NPWP_Available);
            parameters.Add("@tax_code_pph23", pvMonitoring.TAX_CODE_PPh23);
            parameters.Add("@tax_tariff_pph23", pvMonitoring.TAX_TARIFF_PPh23);
            parameters.Add("@amount_pph23", pvMonitoring.AMOUNT_PPh23);
            parameters.Add("@amount_pph21_intern", pvMonitoring.AMOUNT_PPh21_INTERN);
            parameters.Add("@tax_code_pph21_extern", pvMonitoring.TAX_CODE_PPh21_EXTERN);
            parameters.Add("@tax_dpp_accu_pph21_extern", pvMonitoring.TAX_DPP_ACCUMULATION_PPH21_EXTERN);
            parameters.Add("@tax_tariff_pph21_extern", pvMonitoring.TAX_TARIFF_PPh21_EXTERN);
            parameters.Add("@amount_pph21_extern", pvMonitoring.AMOUNT_PPh21_EXTERN);
            parameters.Add("@nik_pph21_extern", pvMonitoring.NIK_PPh21_EXTERN);
            parameters.Add("@tax_code_pph26", pvMonitoring.TAX_CODE_PPh26);
            parameters.Add("@tax_tariff_pph26", pvMonitoring.TAX_TARIFF_PPh26);
            parameters.Add("@amount_pph26", pvMonitoring.AMOUNT_PPh26);
            parameters.Add("@tax_code_pph_final", pvMonitoring.TAX_CODE_PPh_FINAL);
            parameters.Add("@tax_tariff_pph_final", pvMonitoring.TAX_TARIFF_PPh_FINAL);
            parameters.Add("@amount_pph_final", pvMonitoring.AMOUNT_PPh_FINAL);
            parameters.Add("@info_pph_final", pvMonitoring.INFO_PPh_FINAL);
            parameters.Add("@user", user);

            var output = Connection.Execute("usp_SavePVMonitoring", parameters, transaction, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);

            p_status = parameters.Get<int>("@process_status");
            string mesg = parameters.Get<string>("@err_mesg");

            if (p_status != 0 && mesg != null)
            {
                err_mesg = mesg;
            }

            return err_mesg;
        }

        public HashSet<string> ValDuplicateTaxNo(IList<PVMonitoring> pVMonitorings)
        {
            string x = "";
            HashSet<string> result = new HashSet<string>();

            foreach (PVMonitoring m in pVMonitorings)
            {
                string xz = m.PV_NO == null ? "0" : m.PV_NO;
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@result", x, DbType.String, ParameterDirection.InputOutput);
                parameters.Add("@pv_no", xz);
                parameters.Add("@tax_no", m.TAX_NO);
                    
                var output = Connection.Execute("usp_GetDuplicateTaxNo", parameters, null, 0, commandType: System.Data.CommandType.StoredProcedure);
                x = parameters.Get<string>("@result");
                result.Add(x);
            }

            return result;
        }

        public int Log(
                    string _msgId,
                    string _errMessage,
                    string _errLocation = "",
                    string _userID = "",
                    string _functionID = "",
                    int ID = 0,
                    string _remarks = null,
                    string _type = null,
                    string _sts = null)
        {            

            DynamicParameters dyn = new DynamicParameters();
                    dyn.Add("@what", _errMessage);
                    dyn.Add("@user", _userID);
                    dyn.Add("@where", _errLocation);
                    dyn.Add("@pid", ID, DbType.Int32, ParameterDirection.InputOutput);
                    dyn.Add("@id", _msgId);
                    dyn.Add("@type", _type);
                    dyn.Add("@func", _functionID);
                    dyn.Add("@sts", _sts);
                    dyn.Add("@rem", _remarks);
            
            Connection.Execute("usp_PutLog", dyn, null, null, System.Data.CommandType.StoredProcedure);
            ID = dyn.Get<int>("@pid");
            
            return ID;
        }

        public HashSet<string> GetUploadValidation(int pid)
        {
            string query = string.Format(@"SELECT
	                                            D.ERRMSG
                                            FROM TB_T_UPLOAD_D D 
                                            WHERE D.PROCESS_ID = {0} 
                                            AND NULLIF(D.ERRMSG,'') IS NOT NULL", pid);

            var result = Connection.Query<string>(query).ToList();

            HashSet<string> output = new HashSet<string>(result);

            return output;
        }

        public int GetPid(IList<PVMonitoring> pVMonitorings)
        {
            int ID = 0, result = 0;

            foreach (PVMonitoring m in pVMonitorings)
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@pidi", ID, DbType.Int32, ParameterDirection.InputOutput);
                parameters.Add("@invoice_no", m.INVOICE_NO);
                parameters.Add("@function", "UPLOAD-PV");
                parameters.Add("@username", "");

                var output = Connection.Execute("usp_GetPID", parameters, null, 0, commandType: System.Data.CommandType.StoredProcedure);
                ID = parameters.Get<int>("@pidi");

                if (ID != 0)
                {
                    result = ID;
                    break;
                }
            }

            return result;
        }

        public int GetPid(int ID = 0, string function = "", string user = "")
        {      
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@pidi", ID, DbType.Int32, ParameterDirection.InputOutput);
            parameters.Add("@invoice_no", "");
            parameters.Add("@function", function);
            parameters.Add("@username", user);

            var output = Connection.Execute("usp_GetPID", parameters, null, 0,  commandType: System.Data.CommandType.StoredProcedure);
            ID = parameters.Get<int>("@pidi");

            return ID;
        }

        public void DeleteUnSubmittedPVMonitoring(IDbTransaction transaction, string pvno, int delete, string user)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("pvno", pvno);
            parameters.Add("delete", delete);
            parameters.Add("user", user);
            var output = Connection.Execute("usp_DeleteUnSubmittedPVMonitoring", parameters, transaction, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);
        }

        public void NotProper(IDbTransaction transaction, string pv_no, string reason, string user)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("pv_no", pv_no);
            parameters.Add("reason", reason);
            parameters.Add("user", user);
            var output = Connection.Execute("usp_NotProperPVMonitoring", parameters, transaction, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);
        }

        public HashSet<string> UpdatePVMonitoring(IDbTransaction transaction, IEnumerable<PVMonitoring> pvDtl, string pid, string bank_key, string user)
        {
            int p_status = 0;
            string bankKey="", bankType="", err_mesg = "";
            HashSet<string> result = new HashSet<string>();

            if (!string.IsNullOrEmpty(bank_key))
            {
                string[] x = bank_key.Split(':').ToArray();
                bankType = x[0];
                bankKey = x[1];
            }

            foreach (PVMonitoring x in pvDtl)
            {
                var parameters = new DynamicParameters();
                parameters.Add("@process_status", p_status, DbType.Int32, ParameterDirection.InputOutput);
                parameters.Add("@err_mesg", err_mesg, DbType.String, ParameterDirection.InputOutput);
                parameters.Add("pid", pid);
                parameters.Add("pvNo", x.PV_NO);
                parameters.Add("seqNo", x.SEQ_NO);
                parameters.Add("invoiceNo", x.INVOICE_NO);
                parameters.Add("invoiceDate", x.INVOICE_DATE);
                parameters.Add("taxNo", x.TAX_NO);
                parameters.Add("taxDt", x.TAX_DT);
                parameters.Add("bankType", bankType);
                parameters.Add("bankKey", bankKey);
                parameters.Add("user", user);

                var output = Connection.Execute("usp_UpdatePVMonitoring", parameters, transaction, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);

                p_status = parameters.Get<int>("@process_status");
                string mesg = parameters.Get<string>("@err_mesg");

                if (p_status != 0 && mesg != null)
                {
                    result.Add(mesg);
                }
            }

            return result;
        }

        public string GetDivId(string val)
        {
            StringBuilder fetchquery = null;

            fetchquery = new StringBuilder(@"select distinct isnull(isnull(aa.div_id,bb.div_id),'0')[div_id]
                                                from (
	                                                select b.div_id, a.username
	                                                from tb_m_security_users a
	                                                left join tb_m_mapp_div b on a.departement_id = b.department_id
	                                                where a.username = '{0}'
                                                ) aa
                                                outer apply
                                                (
	                                                select b.div_id, b.department_id, b.[username] 
	                                                from tb_m_mapp_div b
	                                                where b.[username] = aa.[username]
                                                ) bb");

            string query = string.Format(fetchquery.ToString(), val);
            var output = Connection.QuerySingleOrDefault<string>(query);

            return output;
        }

        public string GetStatusCmb(string val, string keyParam, bool singleString)
        {
            string output = "";

            if (singleString)
            {
                StringBuilder fetchquery = null;

                fetchquery = new StringBuilder(@"select [description] [name] from dbo.tb_m_parameter where key_param='{0}' and value1 = '{1}'");

                string query = string.Format(fetchquery.ToString(), keyParam, val);
                output = Connection.QuerySingleOrDefault<string>(query);
            }

            return output;
        }

        public List<ComboEx> GetStatusCmb(string userType, string keyParam, string levelCode)
        {
            StringBuilder fetchquery = null;

            fetchquery = new StringBuilder(@"declare @exDealer table
                                                (
	                                                val varchar(2)
                                                )

                                                declare @exBudget table
                                                (
	                                                val varchar(2)
                                                )

                                                insert @exDealer values
                                                ('3'),('42'),('4')

                                                insert @exBudget values
                                                ('2'),('7')

                                                declare @order table
                                                (
	                                                odr int,
	                                                val varchar(2)
                                                )

                                                insert @order values
                                                ('1','1'),('2','3'),('3','6'),('4','4'),('5','42'),('6','2'),('7','8'),('8','7')

                                                select row_number() over(order by o.odr asc) seq, 
	                                                value1 [code], [description] [name] 
                                                from dbo.tb_m_parameter a
                                                left join @exDealer x on a.value1 = x.val
                                                left join @exBudget z on a.value1 = z.val
                                                join @order o on o.val = a.value1
                                                where key_param='{0}'");

            if (levelCode != "MD")
            {
                if (!string.IsNullOrEmpty(userType) && userType != "Internal")
                {
                    fetchquery.Append(" and x.val is not null");
                }
                else
                {
                    fetchquery.Append(" and z.val is null");
                }
            }

            string query = string.Format(fetchquery.ToString(), keyParam);
            var output = Connection.Query<ComboEx>(query).ToList();

            return output;
        }

        public void VerifiedPIC(IDbTransaction transaction, string pv_no, string user)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("pv_no", pv_no);
            parameters.Add("user", user);
            var output = Connection.Execute("usp_VerifiedPICPVMonitoring", parameters, transaction, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);
        }

        public void Submit(IDbTransaction transaction, string pv_no, string user)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("pv_no", pv_no);
            parameters.Add("user", user);
            var output = Connection.Execute("usp_SubmitByDealer", parameters, transaction, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);
        }

        private HashSet<string> SearchListCompare(string[] pv_no)
        {
            HashSet<string> result = new HashSet<string>();

            foreach (string pvno in pv_no)
            {
                List<string> data = new List<string>();

                data = Connection.Query<string>("efb_sp_vw_compare_faktur_list", new { pv_no = pvno }, null, true, null, System.Data.CommandType.StoredProcedure).ToList();

                if (data.Count() > 0)
                {
                    foreach (string x in data)
                    {
                        if (!x.Contains("Passed."))
                            result.Add(x);
                    }
                }
            }

            return result;
        }

        public HashSet<string> CompareViewGeneratorByPV(string[] pv_no)
        {
            HashSet<string> res = new HashSet<string>();

            foreach (string pvno in pv_no)
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@PV_NO", pvno);

                var output = Connection.Execute("efb_sp_compare_invoice_efaktur", parameters, null, 0, commandType: System.Data.CommandType.StoredProcedure);
            }

            res = SearchListCompare(pv_no);

            return res;
        }

        public HashSet<string> CheckBeforeSubmit(string[] pv_no)
        {
            HashSet<string> result = new HashSet<string>();

            foreach (string pvno in pv_no)
            {
                List<string> data = new List<string>();

                data = Connection.Query<string>("usp_check_before_submit", new { pv_no = pvno }, null, true, null, System.Data.CommandType.StoredProcedure).ToList();

                if (data.Count() > 0)
                {
                    foreach (string x in data)
                    {
                        result.Add(x);
                    }
                }
            }

            return result;
        }

        public void SaveBankAttachmentInfo(IDbTransaction transaction, string vendor, string directory, string filename, string guidFilename, string userType)
        {
            var parameters = new Dictionary<string, object>();

            parameters.Add("vendorCd", vendor);
            parameters.Add("directory", directory);
            parameters.Add("fileName", filename);
            parameters.Add("guidFilename", guidFilename);
            parameters.Add("username", userType);

            var output = Connection.Execute("usp_SaveBankAttachmentInfo", parameters, transaction, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);
        }

        public void SaveAttachmentInfo(IDbTransaction transaction, PVMonitoringAttachment attachment, string userType)
        {
            string x = userType == "Internal" ? "Y" : "N";
            var parameters = new Dictionary<string, object>();

            parameters.Add("referenceNumber", attachment.REFERENCE_NO);
            parameters.Add("fileName", attachment.FILE_NAME);
            parameters.Add("categoryCode", attachment.ATTACH_CD);
            parameters.Add("description", attachment.DESCRIPTION == null ? "" : attachment.DESCRIPTION);
            parameters.Add("path", attachment.DIRECTORY);
            parameters.Add("username", attachment.CREATED_BY);
            parameters.Add("byInternal", x);

            var output = Connection.Execute("usp_SaveAttachmentInfo", parameters, transaction, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);
        }

        public List<PVCoverData> getInvoiceValue(string PV_NO, string PV_YEAR)
        {
            string query = string.Format(@"SELECT b.PV_NO, b.PV_YEAR, a.INVOICE_NO, MAX(DISTINCT a.INVOICE_DATE) AS INVOICE_DATE, MAX(DISTINCT b.TAX_NO) AS TAX_NO, 
                                                                    MAX(DISTINCT b.TAX_CD) AS TAX_CD,
                                                                        (SELECT TAX_VALUE
                                                                        FROM ELVIS_DB.dbo.TB_M_TAX AS c
                                                                        WHERE (TAX_CD = MAX(DISTINCT b.TAX_CD))) AS TAX_AMOUNT, b.CURRENCY_CD, SUM(b.AMOUNT) AS AMOUNT, a.VENDOR_CD
                                            FROM ELVIS_DB.dbo.TB_R_INVOICE_H AS a INNER JOIN
                                                 ELVIS_DB.dbo.TB_R_PV_D AS b ON a.INVOICE_NO = b.INVOICE_NO
                                            WHERE a.PV_NO = '{0}' and a.PV_YEAR = '{1}'
                                            GROUP BY b.PV_NO, b.PV_YEAR, a.INVOICE_NO, b.CURRENCY_CD, a.VENDOR_CD
                                            ", PV_NO, PV_YEAR);

            var output = Connection.Query<PVCoverData>(query).ToList();

            return output;
        }

        public List<CodeConstant> GetPVTypes(int exclude)
        {
            string query = string.Format(@"select cast(value1 as varchar)[code], cast(value2 as varchar)[description] 
                                            from tb_m_parameter 
                                            where key_param='pv_type'
	                                            and value1 <> '{0}'", exclude);

            var output = Connection.Query<CodeConstant>(query).ToList();

            return output;
        }

        public List<CodeConstant> GetTransactionTypesSkipApproval()
        {
            string query = string.Format(@"SELECT [SYSTEM_CD] Code
                                                  ,[SYSTEM_VALUE_TXT] [Description]
                                            FROM vw_TbMSystemElvis
                                             WHERE SYSTEM_TYPE = 'TRANSACTION_TYPE' 
                                                   AND SYSTEM_CD = 'SKIP_USER_APPROVAL'");

            var output = Connection.Query<CodeConstant>(query).ToList();

            return output;
        }

        public List<WBSStructure> WbsNumbers(string bookingID = "")
        {
            string query = string.Format(@"SELECT [WbsNumber], [Description] 
                                            FROM vw_WbsNumberFromAccrBalance
                                            where [booking_no] = '{0}'", bookingID);

            var output = Connection.Query<WBSStructure>(query).ToList();

            return output;
        }

        public int GetNum(string sKeyCode, int ValueDefault = 0)
        {
            if (string.IsNullOrEmpty(sKeyCode)) return ValueDefault;
            string skey = sKeyCode;
            string scode = "";
            if (sKeyCode.Contains("/"))
            {
                string[] skc = sKeyCode.Split(new string[] { "/" }, StringSplitOptions.None);
                skey = skc[0];
                scode = skc[1];
            }
            else
            {
                skey = sKeyCode;
            }

            string query = string.Format(@"SELECT TOP 1 SYSTEM_VALUE_NUM 
                                            FROM vw_TbMSystemElvis 
                                            WHERE SYSTEM_TYPE = '{0}'
                                            AND SYSTEM_CD= '{1}'", skey, scode);

            List<int> li = Connection.Query<int>(query).ToList();

            if (li != null && li.Count > 0)
                return li.FirstOrDefault();
            else
                return ValueDefault;
        }

    }
}
