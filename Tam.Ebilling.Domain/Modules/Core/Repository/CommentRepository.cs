﻿using Kendo.Mvc.UI;
using System.Data;
using Dapper;
using System.Collections.Generic;
using System.Linq;

namespace Tam.Ebilling.Domain
{
    public partial interface ICommentRepository : IDapperRepository<Comment>
    {
        DataSourceResult GetCommentDataSourceResult(DataSourceRequest request, string invoice_number, int business_unit_id);
        int Add(Comment comment, string[] columns);
        List<Comment> GetHistoryComment(string invoice_no, string business_unit_id);
    }

    public partial class CommentRepository : DapperRepository<Comment>, ICommentRepository
    {
        public CommentRepository(IDbConnection connection, IDbTransaction transaction)
            : base(connection, transaction)
        {
        }

        public DataSourceResult GetCommentDataSourceResult(DataSourceRequest request, string invoice_number, int business_unit_id)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("invoice_number", invoice_number);
            parameters.Add("business_unit_id", business_unit_id);
            var genericDataTableQuery = new GenericDataSourceQuery(Connection, request);
            var output = genericDataTableQuery.GetData<Comment>(parameters);

            return output;
        }

        public int Add(Comment comment, string[] columns)
        {
            var parameters = (object)Mapping(comment);
            return Connection.Query<int>(DynamicQuery.GetInsertQuery(TableName, parameters, "comment_id", columns) + ";SELECT CAST(SCOPE_IDENTITY() as int)", parameters, Transaction).FirstOrDefault();
        }

        public List<Comment> GetHistoryComment(string invoice_no, string business_unit_id)
        {
            var output = Connection.Query<Comment>(string.Format("SELECT * FROM tb_r_comment WHERE invoice_number = '{0}' and business_unit_id = '{1}' ", invoice_no, business_unit_id));
            //var output = Connection.Query<Comment>("usp_Print_MvInvoice", parameters, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);

            //    var user = cnn.Query<User>("spGetUser", new { Id = 1 },
            //commandType: CommandType.StoredProcedure).First();

            return output.AsList<Comment>();
        }
    }
}
