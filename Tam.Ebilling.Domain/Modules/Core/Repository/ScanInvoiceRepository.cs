﻿using System;
using Kendo.Mvc.UI;
using System.Data;
using Dapper;
using Tam.Ebilling.Domain.Modules.Core.Model;
using System.Collections.Generic;
using System.Linq;

namespace Tam.Ebilling.Domain
{
    public partial interface IScanInvoiceRepository : IDapperRepository<ScanInvoice>
    {
        List<ScanInvoice> UploadScan(string filename, string invoice_number);
        List<ScanInvoice> Remove(long attachment_invoice_id);
      
        DataSourceResult GetDataSourceResult(DataSourceRequest request, object parameter = null);
    }

    public partial class ScanInvoiceRepository : DapperRepository<ScanInvoice>, IScanInvoiceRepository
    {
        public ScanInvoiceRepository(IDbConnection connection, IDbTransaction transaction)
            : base(connection, transaction)
        {
        }

        public DataSourceResult GetDataSourceResult(DataSourceRequest request, object parameter = null)
        {
            var genericDataTableQuery = new GenericDataSourceQuery(Connection, request);
            var output = genericDataTableQuery.GetData<ScanInvoice>();

            return output;
        }
        public DataSourceResult GetDetailByReference(DataSourceRequest request, string invoice_number, object parameter = null)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("invoice_number", invoice_number);
            var genericDataTableQuery = new GenericDataSourceQuery(Connection, request);
            var output = genericDataTableQuery.GetData<ScanInvoice>(parameters);

            return output;
        }
        public List<ScanInvoice> UploadScan(string filename, string invoice_number)
        {

            var parameters = new Dictionary<string, object>();
            parameters.Add("filename", filename);
            parameters.Add("invoice_number", invoice_number);
            var output = Connection.Query<ScanInvoice>("usp_UploadScanInvoice", parameters, null, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);

            return output.ToList();
        }
      

        //public List<AttachmentNotaRetur> UploadNotur(long attachment_notaretur_id, string filename, string reference)
        //{
        //    var transact = Connection.BeginTransaction();
        //    var output = Connection.Query<AttachmentNotaRetur>("usp_UploadNotaRetur", commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);
        //    try
        //    {

        //        transact.Commit();
        //    }
        //    catch (Exception e)
        //    {
        //        transact.Rollback();
        //    }
        //    return output.ToList();
        //}

        public List<ScanInvoice> Remove(long attachment_invoice_id)
        {

            var parameters = new Dictionary<string, object>();
            parameters.Add("attachment_invoice_id", attachment_invoice_id);
            var output = Connection.Query<ScanInvoice>("usp_DeleteUploadScan", parameters, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);

            return output.ToList();
        }
       
    }
}
