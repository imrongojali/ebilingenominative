using System.Data;
using Agit.Domain;
using System.Collections.Generic;
using Dapper;

namespace Tam.Ebilling.Domain
{
    public partial interface IMenuRepository : IDapperRepository<Menu>
    {
        int Add(Menu item, string[] columns);
        int Update(Menu item, string[] columns);
        int Delete(int id);
    }

    public partial class MenuRepository : DapperRepository<Menu>, IMenuRepository
    {
        public MenuRepository(IDbConnection connection, IDbTransaction transaction)
            : base(connection, transaction)
        {
        }

        public override IEnumerable<Menu> FindAll()
        {
            IEnumerable<Menu> items = null;

            var query = @"
                SELECT        
                    m.menu_id, m.parent_id, m.menu_group_id, m.Title, m.Url, m.Description, m.icon_class, 
                    m.order_index, m.created_date, m.created_by, g.name AS group_name, m.Visible
                FROM
                    dbo.tb_m_menu AS m LEFT OUTER JOIN
                    dbo.tb_m_menu_group AS g ON g.menu_group_id = m.menu_group_id";

            items = Connection.Query<Menu>(query);

            return items;
        }

        public int Add(Menu item, string[] columns)
        {
            var parameters = (object)Mapping(item);
            return Connection.Execute(DynamicQuery.GetInsertQuery(TableName, parameters, "menu_id", columns), parameters, Transaction);
        }

        public int Update(Menu item, string[] columns)
        {
            var parameters = (object)Mapping(item);
            return Connection.Execute(DynamicQuery.GetUpdateQuery(TableName, item, "menu_id", columns), parameters, Transaction);
        }

        public int Delete(int id)
        {
            return Connection.Execute("DELETE FROM " + TableName + " WHERE menu_id = @menu_id", new { menu_id = id }, Transaction);
        }
    }
}

