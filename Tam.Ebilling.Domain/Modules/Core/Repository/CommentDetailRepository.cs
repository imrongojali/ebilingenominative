﻿using Kendo.Mvc.UI;
using System.Data;
using Dapper;
using System.Collections.Generic;

namespace Tam.Ebilling.Domain
{
    public partial interface ICommentDetailRepository : IDapperRepository<CommentDetail>
    {
        int Add(CommentDetail comment, string[] columns);
        List<Notification> Get(string comment_id);
        int Update(string invoice_number, string user_id);
    }

    public partial class CommentDetailRepository : DapperRepository<CommentDetail>, ICommentDetailRepository
    {
        public CommentDetailRepository(IDbConnection connection, IDbTransaction transaction)
            : base(connection, transaction)
        {
        }

        public int Add(CommentDetail comment, string[] columns)
        {
            var parameters = (object)Mapping(comment);
            return Connection.Execute(DynamicQuery.GetInsertQuery(TableName, parameters, "comment_id", columns), parameters, Transaction);
        }

        public List<Notification> Get(string user_id)
        {
            var output = Connection.Query<Notification>(string.Format(@"
                            SELECT c.invoice_number, count(comment_detail_id) as count FROM tb_r_comment c
                            JOIN tb_r_comment_detail d on c.comment_id = d.comment_id
                            and d.is_read = 0 and d.user_id = '{0}'
                            group by c.invoice_number", user_id));
            //var output = Connection.Query<Comment>("usp_Print_MvInvoice", parameters, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure);

            //    var user = cnn.Query<User>("spGetUser", new { Id = 1 },
            //commandType: CommandType.StoredProcedure).First();

            return output.AsList<Notification>();
        }

        public int Update(string invoice_number, string user_id)
        {
            string query = @"UPDATE detail SET detail.is_read = 1
                            FROM tb_r_comment_detail AS detail
                            INNER JOIN tb_r_comment AS header
                            ON header.comment_id = detail.comment_id
                            WHERE header.invoice_number = '{0}' AND detail.is_read = 0 
                            AND detail.user_id = '{1}'";

            return Connection.Execute(string.Format(query, invoice_number, user_id));
        }
    }
}
