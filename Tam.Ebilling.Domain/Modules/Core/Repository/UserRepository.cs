﻿using Agit.Domain;
using Agit.Mvc;
using Kendo.Mvc.UI;
using System.Data;
using Dapper;

namespace Tam.Ebilling.Domain
{
    public partial interface IUserRepository : IDapperRepository<User>
    {
    }

    public partial class UserRepository : DapperRepository<User>, IUserRepository
    {
        public UserRepository(IDbConnection connection, IDbTransaction transaction)
            : base(connection, transaction)
        {
        }

    }
}
