﻿
using Kendo.Mvc.UI;
using System.Data;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using System;

namespace Tam.Ebilling.Domain
{
    public partial interface IDealerCodeRepository : IDapperRepository<DealerCode>
    {
        List<DealerCode> GetDealerCode(string customer_group);
    }
    public partial class DealerCodeRepository : DapperRepository<DealerCode>, IDealerCodeRepository
    {
        public DealerCodeRepository(IDbConnection connection, IDbTransaction transaction)
            : base(connection, transaction)
        {
        }

        public List<DealerCode> GetDealerCode(string customer_group)
        {
            var fetchquery = string.Empty;
            var query = string.Empty;

            if (customer_group == null)
            {
                fetchquery = @"SELECT distinct dealer_code_name  FROM vw_DealerCode";
                query = string.Format(fetchquery);
            }
            else
            {
                fetchquery = @"SELECT distinct dealer_code_name  FROM vw_DealerCode WHERE customer_group = '{0}'";
                query = string.Format(fetchquery, customer_group);
            }
            var output = Connection.Query<DealerCode>(query).ToList();

            return output;
        }
    }
}
