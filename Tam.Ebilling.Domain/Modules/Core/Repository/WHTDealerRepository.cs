﻿
using Kendo.Mvc.UI;
using System.Data;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using System;

namespace Tam.Ebilling.Domain
{
    public partial interface IWHTDealerRepository : IDapperRepository<WHTDealer>
    {
        List<WHTDealer> GetWHTDealerName(string customer_group);
        List<WHTDealer> GetWHTOriDealerName(string customer_group);
        List<WHTDealer> GetWHTDealerNPWP(string customer_group);
    }

    public partial class WHTDealerRepository : DapperRepository<WHTDealer>, IWHTDealerRepository
    {
        public WHTDealerRepository(IDbConnection connection, IDbTransaction transaction)
            : base(connection, transaction)
        {
        }

        public List<WHTDealer> GetWHTDealerName(string customer_group)
        {
            var fetchquery = string.Empty;
            var query = string.Empty;

            if (customer_group == null)
            {
                fetchquery = @"SELECT distinct dealer_name  FROM vw_WHTDealer";
                query = string.Format(fetchquery);
            }
            else
            {
                fetchquery = @"SELECT distinct dealer_name  FROM vw_WHTDealer WHERE customer_group = '{0}'";
                query = string.Format(fetchquery, customer_group);
            }
            var output = Connection.Query<WHTDealer>(query).ToList();

            return output;
        }

        public List<WHTDealer> GetWHTOriDealerName(string customer_group)
        {
            var fetchquery = string.Empty;
            var query = string.Empty;

            if (customer_group == null)
            {
                fetchquery = @"SELECT distinct original_dealer_name  FROM vw_WHTDealer";
                query = string.Format(fetchquery);
            }
            else
            {
                fetchquery = @"SELECT distinct original_dealer_name  FROM vw_WHTDealer WHERE customer_group = '{0}'";
                query = string.Format(fetchquery, customer_group);
            }
            var output = Connection.Query<WHTDealer>(query).ToList();

            return output;
        }

        public List<WHTDealer> GetWHTDealerNPWP(string customer_group)
        {
            var fetchquery = string.Empty;
            var query = string.Empty;

            if (customer_group == null)
            {
                fetchquery = @"SELECT distinct dealer_npwp FROM vw_WHTDealer";
                query = string.Format(fetchquery);
            }
            else
            {
                fetchquery = @"SELECT distinct dealer_npwp FROM vw_WHTDealer WHERE customer_group = '{0}'";
                query = string.Format(fetchquery, customer_group);
            }
            var output = Connection.Query<WHTDealer>(query).ToList();

            return output;
        }
    }
}
