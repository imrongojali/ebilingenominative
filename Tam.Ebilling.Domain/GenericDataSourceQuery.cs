﻿using Kendo.Mvc;
using Kendo.Mvc.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

using Tam.Ebilling.Domain.Common;
using System.Text.RegularExpressions;

namespace Tam.Ebilling.Domain
{
    public class GenericDataSourceQuery
    {
        protected const string TABLE_ALIAS = "gdb";
        private readonly Dictionary<FilterOperator, string> _operators = new Dictionary<FilterOperator, string>();
        private readonly List<SqlParameter> _paraCols = new List<SqlParameter>();
        protected IDbConnection Connection { get; set; }
        protected DataSourceRequest DataTableRequest { get; set; }

        //Add by Hendry, bug row_number engga ber urut, solusinya di order by row_number
        public string FetchQuery = "SELECT TOP {0} * FROM (SELECT *, ROW_NUMBER() OVER (ORDER BY {4}) as row_number FROM (SELECT * FROM ({1}) AS {2}) AS {2} WHERE ({3})) AS {2} WHERE {2}.row_number >= {5} order by row_number";
        public string FetchQueryForTotal = "SELECT * FROM ({0}) as gdb  WHERE 1=1 and {2}";
        public GenericDataSourceQuery(IDbConnection connection, DataSourceRequest dataTableRequest)
        {
            Connection = connection;

            DataTableRequest = dataTableRequest;

            _operators.Add(FilterOperator.Contains, "{0} LIKE '%' + {1} + '%'");
            //_operators.Add(FilterOperator.Contains, "{0} LIKE '%' + {1}");
            _operators.Add(FilterOperator.DoesNotContain, "{0} NOT LIKE '%' + {1} + '%'");
            _operators.Add(FilterOperator.EndsWith, "{0} LIKE '%' + {1}");
            _operators.Add(FilterOperator.StartsWith, "{0} LIKE {1} + '%'");
            _operators.Add(FilterOperator.IsNull, "{0} IS NULL");
            _operators.Add(FilterOperator.IsNotNull, "{0} IS NOT NULL");
            _operators.Add(FilterOperator.IsNotEqualTo, "{0} <> {1}");
            _operators.Add(FilterOperator.IsNotEmpty, "{0} <> ''");
            _operators.Add(FilterOperator.IsLessThanOrEqualTo, "{0} <= {1}");
            _operators.Add(FilterOperator.IsLessThan, "{0} < {1}");
            _operators.Add(FilterOperator.IsGreaterThanOrEqualTo, "{0} >= {1}");
            _operators.Add(FilterOperator.IsGreaterThan, "{0} > {1}");
            _operators.Add(FilterOperator.IsEqualTo, "{0} = {1}");
            _operators.Add(FilterOperator.IsContainedIn, "{0} IN({1})");
        }
        public DataSourceResult GetData<T>(string storedFunction, Dictionary<string, object> parameters, Dictionary<string, object> conditionals = null)
        {
            var whereClause = string.Empty;

            if (parameters != null)
            {
                foreach (var key in parameters.Keys)
                {
                    _paraCols.Add(new SqlParameter(key, parameters[key]));
                }
            }

            var parameterString = parameters != null ? string.Join(", ", _paraCols.Select(x => "@" + x.ParameterName)) : string.Empty;

            if (conditionals != null)
            {
                foreach (var key in conditionals.Keys)
                {
                    _paraCols.Add(new SqlParameter(key, conditionals[key]));
                }

                whereClause = "WHERE " + string.Join(" AND ", conditionals.Select(x => x.Key + " = @" + x.Key));
            }

            return GetData<T>(string.Format("SELECT * FROM {0}({1}) {2}", storedFunction, parameterString, whereClause));
        }
        public DataSourceResult GetData<T>(Dictionary<string, object> parameters)
        {
            var whereClause = string.Empty;

            if (parameters != null)
            {
                foreach (var key in parameters.Keys)
                {
                    _paraCols.Add(new SqlParameter(key, parameters[key]));
                }

                whereClause = "WHERE " + string.Join(" AND ", parameters.Select(x => x.Key + " = @" + x.Key));
            }
            var type = typeof(T);
            var attribute = type.GetCustomAttributes(typeof(TableAttribute), true).FirstOrDefault() as TableAttribute;

            return GetData<T>(string.Format("SELECT * FROM [{0}] {1}", attribute != null ? attribute.Name : type.Name, whereClause));
        }
        public DataSourceResult GetData<T>()
        {
            var whereClause = string.Empty;

            var type = typeof(T);
            var attribute = type.GetCustomAttributes(typeof(TableAttribute), true).FirstOrDefault() as TableAttribute;

            return GetData<T>(string.Format("SELECT * FROM [{0}] {1}", attribute != null ? attribute.Name : type.Name, whereClause));
        }        
        public DataSourceResult GetData<T>(string query)
        {
            int displayLength = DataTableRequest.PageSize == 0 ? int.MaxValue : DataTableRequest.PageSize;
            int displayStart = (DataTableRequest.Page - 1) * displayLength + 1;

            if (query.Contains("cast(id as varchar(50))"))
                displayStart = 1;

            //query = "select distinct  * from(SELECT  a.*,case when b.NomorFakturGabungan is not null and b.PdfUrl is not null then 'Full' else 'NotComp' end as doc_status  from BES.dbo.vw_Invoice_Menu a left join [TAM_EFAKTUR].[dbo].[TB_R_VATOut] b on a.tax_invoice = b.NomorFakturGabungan ) as Temp";

            string filterQuery = DataTableRequest.Filters != null ? BuildFilter(DataTableRequest.Filters.ToList(), "AND") : "1=1";

            bool sortWithNull = false;
            var sortables = DataTableRequest.Sorts;
            foreach (var sort in sortables)
            {
                if (string.IsNullOrEmpty(sort.Member))
                {
                    sortWithNull = true;
                    break;
                }   
            }

            IEnumerable data = null;
            int totalCount = 0;

            if (!sortWithNull)
            {
                filterQuery = filterQuery.Replace("credit_note_status =", "PARSENAME(REPLACE(([TAM_EFAKTUR].dbo.[udf_GetColorFromParam]('EBILL_VATCN',invoice_number,tax_date,NULL,creditnotettd,creditnotedgt,NULL,business_unit,NULL,doc_date,due_date,downloaded_date,downloadedscan_date,pi_downloaded_date,TanggalRetur,TglFaktuPajakYangDiRetur,null,null)),'|', '.'), 1) =");
                //filterQuery = filterQuery.Replace("wht_prepaid_status =", "PARSENAME(REPLACE(([TAM_EFAKTUR].dbo.[udf_GetColorFromParam]('EBILL_DWHTS',df.invoice_number,b.invoice_date,b.flag_potong_pajak,b.wht_pdf_22,b.wht_pdf_23,b.wht_pdf_42,b.business_unit,NULL)),'|', '.'), 1) =");

                filterQuery = filterQuery.Replace("credit_note_status LIKE ", "PARSENAME(REPLACE(([TAM_EFAKTUR].dbo.[udf_GetColorFromParam]('EBILL_VATCN',invoice_number,tax_date,NULL,creditnotettd,creditnotedgt,NULL,business_unit,NULL,doc_date,due_date,downloaded_date,downloadedscan_date,pi_downloaded_date,TanggalRetur,TglFaktuPajakYangDiRetur,null,null)),'|', '.'), 1) LIKE ");
                //filterQuery = filterQuery.Replace("wht_prepaid_status LIKE ", "PARSENAME(REPLACE(([TAM_EFAKTUR].dbo.[udf_GetColorFromParam]('EBILL_DWHTS',df.invoice_number,b.invoice_date,b.flag_potong_pajak,b.wht_pdf_22,b.wht_pdf_23,b.wht_pdf_42,b.business_unit,NULL)),'|', '.'), 1) LIKE ");

                filterQuery = filterQuery.Replace("CountRelatedInvoice =", "(SELECT Count(1) FROM BES.dbo.tb_r_bukti_potong_attachment bt WHERE  bt.invoice_number = invoice_number AND bt.upload_id = (SELECT TOP 1 bt.upload_id FROM   BES.dbo.tb_r_bukti_potong_attachment bt WHERE bt.invoice_number = invoice_number)) =");

                string sortQuery = BuildSort<T>();

                //Ori
                //var filterQueryCount = filterQuery.Replace("CountRelatedInvoice LIKE", "(SELECT Count(1) FROM tb_r_bukti_potong_attachment bt WHERE  bt.invoice_number = df.invoice_number AND bt.upload_id = (SELECT TOP 1 bt.upload_id FROM   tb_r_bukti_potong_attachment bt WHERE bt.invoice_number = df.invoice_number)) LIKE");

                //Change
                var filterQueryCount = filterQuery.Replace("CountRelatedInvoice LIKE", "(SELECT Count(1) FROM BES.dbo.tb_r_bukti_potong_attachment bt WHERE  bt.invoice_number = invoice_number AND bt.upload_id = (SELECT TOP 1 bt.upload_id FROM   BES.dbo.tb_r_bukti_potong_attachment bt WHERE bt.invoice_number = invoice_number)) LIKE");
                 
                string sqlQuery = string.Format(FetchQuery, displayLength, query, TABLE_ALIAS, filterQueryCount, sortQuery, displayStart);
                string sqlQueryforTotal = string.Format(FetchQueryForTotal,query, TABLE_ALIAS, filterQueryCount, "1=1");


                if (filterQuery.Contains("credit_note_status") || filterQuery.Contains("wht_prepaid_status"))
                {
                    sqlQuery = string.Format(FetchQuery, displayLength, query + " and "+ filterQueryCount, TABLE_ALIAS, "1=1", sortQuery, displayStart);
                    sqlQueryforTotal = string.Format(FetchQueryForTotal, query.Replace(", ([TAM_EFAKTUR].dbo.[udf_GetColorFromParam]('EBILL_VATCN',invoice_number,tax_date,NULL,creditnotettd,creditnotedgt,NULL,business_unit,NULL,doc_date,due_date,downloaded_date,downloadedscan_date,pi_downloaded_date,TanggalRetur,TglFaktuPajakYangDiRetur,null,null))  as credit_note_status","") + " and "+ filterQueryCount, TABLE_ALIAS, "1=1");
                }

                if (query.Contains("udf_Invoice(1"))
                {
                    sqlQuery = string.Format("SELECT TOP {0} * FROM (SELECT *, ROW_NUMBER() OVER (ORDER BY {4}) as row_number FROM (select inv.id, business_unit, business_unit_id, invoice_number, transaction_type, transaction_type_id, doc_date, due_date, currency, sum(dpp) as dpp, sum(tax) as tax, sum(total_amount) as total_amount, ar_status, tax_invoice, pph_no, da_code, da_no, customer_code, customer_name, customer_group, exc_rate, amount_pph, inv_downloaded, notur_uploaded, is_notur_upload_done, notur_downloaded, downloaded_date, downloaded_by, is_inv_available, inv_scan_downloaded, downloadedscan_date, downloadedscan_by, is_inv_scan_available, rate_pph, vat_dormitory, dormitory, keyword, pi_downloaded, pi_downloaded_date, pi_downloaded_by, customer_address, is_retur,(select nama_pt from [TAM_EFAKTUR].[dbo].[TB_M_Company_Detail] where Code='J') nameReportCSV, (select npwp_pt from [TAM_EFAKTUR].[dbo].[TB_M_Company_Detail] where Code='J') npwpReportCSV, ar.config_text AS ar_status_name, ar.config_description AS ar_status_img_url   from {1}  group by  inv.id, business_unit, business_unit_id, invoice_number, transaction_type, transaction_type_id, doc_date, due_date, currency,  ar_status, tax_invoice, pph_no, da_code, da_no, customer_code, customer_name, customer_group, exc_rate, amount_pph, inv_downloaded, notur_uploaded, is_notur_upload_done,  notur_downloaded, downloaded_date, downloaded_by, is_inv_available, inv_scan_downloaded, downloadedscan_date, downloadedscan_by, is_inv_scan_available, rate_pph, vat_dormitory, dormitory, keyword, pi_downloaded, pi_downloaded_date, pi_downloaded_by, customer_address, is_retur,ar.config_text , ar.config_description   ) AS {2}) AS {2} WHERE {2}.row_number >= {5} order by row_number", displayLength, query + "inv LEFT JOIN vw_AR_Status ar on inv.ar_status = ar.config_value LEFT JOIN [TAM_EFAKTUR].[dbo].[TB_R_VATOut] ef on inv.NoFakturPajakYangDiRetur = ef.NomorFakturGabungan and ApprovalStatus = 'Approval Sukses' where 1=1 and " + filterQueryCount, TABLE_ALIAS, "1=1", sortQuery, displayStart);
                    sqlQueryforTotal = string.Format("SELECT COUNT(0) FROM (SELECT *, ROW_NUMBER() OVER (ORDER BY {4}) as row_number FROM (select inv.id, business_unit, business_unit_id, invoice_number, transaction_type, transaction_type_id, doc_date, due_date, currency, sum(dpp) as dpp, sum(tax) as tax, sum(total_amount) as total_amount, ar_status, tax_invoice, pph_no, da_code, da_no, customer_code, customer_name, customer_group, exc_rate, amount_pph, inv_downloaded, notur_uploaded, is_notur_upload_done,  notur_downloaded, downloaded_date, downloaded_by, is_inv_available, inv_scan_downloaded, downloadedscan_date, downloadedscan_by, is_inv_scan_available, rate_pph, vat_dormitory, dormitory, keyword, pi_downloaded, pi_downloaded_date, pi_downloaded_by, customer_address, is_retur,(select nama_pt from [TAM_EFAKTUR].[dbo].[TB_M_Company_Detail] where Code='J') nameReportCSV, (select top 1 npwp_pt from [TAM_EFAKTUR].[dbo].[TB_M_Company_Detail] where Code='J') npwpReportCSV, ar.config_text AS ar_status_name, ar.config_description AS ar_status_img_url   from {1}  group by  inv.id, business_unit, business_unit_id, invoice_number, transaction_type, transaction_type_id, doc_date, due_date, currency,  ar_status, tax_invoice, pph_no, da_code, da_no, customer_code, customer_name, customer_group, exc_rate, amount_pph, inv_downloaded, notur_uploaded, is_notur_upload_done, notur_downloaded, downloaded_date, downloaded_by, is_inv_available, inv_scan_downloaded, downloadedscan_date, downloadedscan_by, is_inv_scan_available, rate_pph, vat_dormitory, dormitory, keyword, pi_downloaded, pi_downloaded_date, pi_downloaded_by, customer_address, is_retur,ar.config_text , ar.config_description   ) AS {2}) AS {2}", displayLength, query + "inv LEFT JOIN vw_AR_Status ar on inv.ar_status = ar.config_value LEFT JOIN [TAM_EFAKTUR].[dbo].[TB_R_VATOut] ef on inv.NoFakturPajakYangDiRetur = ef.NomorFakturGabungan and ApprovalStatus = 'Approval Sukses' where 1=1 and " + filterQueryCount, TABLE_ALIAS, "1=1", sortQuery, displayStart);
                }

                //totalCount = GetTotalCount(query, filterQuery); 

                //DynamicParameters parameters = new DynamicParameters();
                //_paraCols.ForEach(x => parameters.Add(x.ParameterName, x.Value));
                //sqlQuery = "SELECT TOP 14 * FROM (SELECT *, ROW_NUMBER() OVER (ORDER BY doc_date DESC) as row_number FROM(SELECT * FROM(SELECT  a.* from BES.dbo.vw_Invoice_Menu a left join [TAM_EFAKTUR].[dbo].[TB_R_VATOut] b on a.tax_invoice = b.NomorFakturGabungan  WHERE b.NomorFakturGabungan is not null and b.PdfUrl is not null) AS gdb) AS gdb WHERE((1 = 1))) AS gdb WHERE gdb.row_number >= 1";
                // string sqlQuery =  string.Format(FetchQuery, displayLength, query, TABLE_ALIAS, filterQuery, sortQuery, displayStart);

                try
                {
                    //totalCount = GetTotalCount(query, filterQuery);

                    DynamicParameters parameters = new DynamicParameters();
                    _paraCols.ForEach(x => parameters.Add(x.ParameterName, x.Value));
                    //sqlQuery = "SELECT TOP 14 * FROM (SELECT *, ROW_NUMBER() OVER (ORDER BY doc_date DESC) as row_number FROM(SELECT * FROM(SELECT  a.* from BES.dbo.vw_Invoice_Menu a left join [TAM_EFAKTUR].[dbo].[TB_R_VATOut] b on a.tax_invoice = b.NomorFakturGabungan  WHERE b.NomorFakturGabungan is not null and b.PdfUrl is not null) AS gdb) AS gdb WHERE((1 = 1))) AS gdb WHERE gdb.row_number >= 1";


                    var dataT = Connection.Query<T>(
                            sqlQuery,
                            param: parameters,
                            commandTimeout: 0,
                            commandType: System.Data.CommandType.Text
                        );
                    data = dataT.ToList();


                    if (query.Contains("udf_Invoice(1"))
                    {
                        var dataTotalInv = Connection.Query<int>(
                              sqlQueryforTotal,
                              param: parameters,
                              commandTimeout: 0,
                              commandType: System.Data.CommandType.Text
                          );
                        totalCount = dataTotalInv.FirstOrDefault();
                    }
                    else
                    {
                        var dataTotal = Connection.Query<T>(
                               sqlQueryforTotal,
                               param: parameters,
                               commandTimeout: 0,
                               commandType: System.Data.CommandType.Text
                           );
                        totalCount = dataTotal.Count();
                    }


                    _paraCols.Clear();
                   
                }
                catch (Exception e)
                {

                    throw new Exception(e.Message);
                }

            }

            return new DataSourceResult { Data = data, Total = totalCount };
        }

        
        public DataSourceResult GetDataFromUDF<T>(string query)
        { 
             
            IEnumerable data = null;
             
            data = Connection.Query<T>(query, null, commandTimeout: 0, commandType: System.Data.CommandType.Text).ToList();
             
            _paraCols.Clear();

            return new DataSourceResult { Data = data };
        }

        protected virtual int GetTotalCount(string query, string filterQuery)
        {

            var sqlString = string.Format("SELECT distinct COUNT(*) FROM({0}) AS {1} WHERE ({2})", query, TABLE_ALIAS, filterQuery);

            sqlString = sqlString.Replace("CountRelatedInvoice =", "(SELECT Count(1) FROM BES.dbo.tb_r_bukti_potong_attachment bt WHERE  bt.invoice_number = invoice_number AND bt.upload_id = (SELECT TOP 1 bt.upload_id FROM   BES.dbo.tb_r_bukti_potong_attachment bt WHERE bt.invoice_number = invoice_number)) =");
            sqlString = sqlString.Replace("PARSENAME(REPLACE(([TAM_EFAKTUR].dbo.[udf_GetColorFromParam]('EBILL_DWHTS',df.invoice_number,b.invoice_date,b.flag_potong_pajak,b.wht_pdf_22,b.wht_pdf_23,b.wht_pdf_42,b.business_unit,NULL,b.tgl_payment,b.submit_date,b.created_date,b.modified_date,b.tanggal_bukti_potong,NULL,NULL,NULL,NULL)),'|', '.'), 1)", "wht_prepaid_status");

            DynamicParameters parameters = new DynamicParameters();
            _paraCols.ForEach(x => parameters.Add(x.ParameterName, x.Value));

            return Connection.Query<int>(sqlString, parameters).FirstOrDefault();
            //return Connection.Query<int>(sqlString).FirstOrDefault();
        }

        public DataSourceResult GetUDFWithSPData<T>(string query)
        {
            int displayLength = DataTableRequest.PageSize == 0 ? int.MaxValue : DataTableRequest.PageSize;
            int displayStart = (DataTableRequest.Page - 1) * displayLength + 1;

            if (query.Contains("cast(id as varchar(50))"))
                displayStart = 1;

            string filterQuery = DataTableRequest.Filters != null ? BuildFilter(DataTableRequest.Filters.ToList(), "AND") : "1=1";

            string sortQuery = BuildSort<T>();

            var sqlQuery =
                string.Format(FetchQuery, displayLength, query, TABLE_ALIAS, filterQuery, sortQuery, displayStart);
            
            IEnumerable data = null;

            DynamicParameters parameters = new DynamicParameters();
            _paraCols.ForEach(x => parameters.Add(x.ParameterName, x.Value));

            foreach (SqlParameter x in _paraCols)
            {
                sqlQuery = Regex.Replace(sqlQuery, x.ParameterName, "'" + x.Value.ToString() + "'");
            }

            sqlQuery = replaceExpression(sqlQuery);
            
            var _params = new Dictionary<string, object>();
            _params.Add("sqlQuery", sqlQuery);

            data = Connection.Query<T>("usp_GetDataUDFWithSP", _params, commandTimeout:0, commandType: System.Data.CommandType.StoredProcedure).ToList();

            int totalCount = GetUDFWithSPTotalCount(query, filterQuery);

            _paraCols.Clear();

            return new DataSourceResult { Data = data, Total = totalCount };
        }

        protected virtual int GetUDFWithSPTotalCount(string query, string filterQuery)
        {
            var sqlString = string.Format(@"SELECT COUNT(*) FROM ({0}) AS {1} WHERE ({2})", query, TABLE_ALIAS, string.IsNullOrEmpty(filterQuery) ? "1=1" : filterQuery);

            DynamicParameters parameters = new DynamicParameters();
            _paraCols.ForEach(x => parameters.Add(x.ParameterName, x.Value));

            foreach (SqlParameter x in _paraCols)
            {
                sqlString = Regex.Replace(sqlString, x.ParameterName, "'" + x.Value.ToString() + "'");
            }

            sqlString = replaceExpression(sqlString);

            var _params = new Dictionary<string, object>();
            _params.Add("sqlQuery", sqlString);

            int count = Connection.Query<int>("usp_GetDataUDFWithSP", _params, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure).SingleOrDefault(); 

            return count;
        }

        private string replaceExpression(string input)
        {
            HashSet<string> expression = new HashSet<string>() { "\r", "\n", "\t", "False", "True" };

            foreach (string x in expression)
            {
                if (x == "False")
                {
                    input = Regex.Replace(input, x, "0");
                }
                else if (x == "True")
                {
                    input = Regex.Replace(input, x, "1");
                }
                else
                {
                    input = Regex.Replace(input, x, "");
                }
            }

            return input;
        }
        /*
        public DataSourceResult GetHeapData<T>(string query)
        {
            int displayLength = DataTableRequest.PageSize == 0 ? int.MaxValue : DataTableRequest.PageSize;
            int displayStart = (DataTableRequest.Page - 1) * displayLength + 1;

            if (query.Contains("cast(id as varchar(50))"))
                displayStart = 1;

            string filterQuery = DataTableRequest.Filters != null ? BuildFilter(DataTableRequest.Filters.ToList(), "AND") : "1=1";

            string sortQuery = BuildSort<T>();
            string sqlQuery =
                string.Format(FetchQuery, displayLength, query, TABLE_ALIAS, filterQuery, sortQuery, displayStart);
            int totalCount = 0;
            IEnumerable data = null;

            DynamicParameters parameters = new DynamicParameters();
            _paraCols.ForEach(x => parameters.Add(x.ParameterName, x.Value));

            HashSet<string> hs = new HashSet<string>();
            _paraCols.ForEach(x => hs.Add(x.ParameterName.ToString() + "=" + x.Value.ToString()));
            string param = "", sqlId = "", sqlIdTCount = "";
            if (hs.Count() > 0)
            {
                foreach (string p in hs)
                {
                    param += (";" + p);
                }

                sqlId = query + ";" + displayLength.ToString() + ";" + displayStart + param;
                sqlIdTCount = sqlId + "dataCount";
                data = Heap.Get<IEnumerable<T>>(sqlId);
                if (data == null)
                {
                    data = Connection.Query<T>(
                    sqlQuery,
                        param: parameters
                    ).ToList();

                    if (data != null)
                    {
                        Heap.Set(sqlId, data);

                        totalCount = GetTotalCount(query, filterQuery);
                        if (totalCount != 0)
                            Heap.Set(sqlIdTCount, totalCount);
                    }
                }
                else
                {
                    int xtot = GetTotalCount(query, filterQuery);
                        totalCount = Heap.Get<int>(sqlIdTCount);

                    if (xtot != totalCount)
                    {
                        IEnumerable outv = null;

                        outv = Connection.Query<T>(
                        sqlQuery,
                            param: parameters
                        ).ToList();

                        if (outv != null)
                        {
                            data = outv;
                            Heap.Set(sqlId, data);

                            totalCount = xtot;
                            if (totalCount != 0)
                                Heap.Set(sqlIdTCount, totalCount);
                        }
                    }
                }
            }
            else
            {
                totalCount = GetTotalCount(query, filterQuery);

                data = Connection.Query<T>(
                    sqlQuery,
                    param: parameters
                ).ToList();
            }

            _paraCols.Clear();
            
            return new DataSourceResult { Data = data, Total = totalCount };
        }
        */
        protected virtual string BuildFilter(List<IFilterDescriptor> searchables, string operand)
        {
            var qb = new List<string>();

            foreach (IFilterDescriptor filterDescriptor in searchables)
            {
                if (filterDescriptor is CompositeFilterDescriptor)
                {
                    var composite = filterDescriptor as CompositeFilterDescriptor;

                    qb.Add(BuildFilter(composite.FilterDescriptors.ToList(), composite.LogicalOperator.ToString()));
                }
                else
                {
                    var filter = filterDescriptor as FilterDescriptor;
                    var index = _paraCols.Count;
                    var paramName = "@" + filter.Member + index;
                    //_paraCols.Add(new SqlParameter(paramName, filter.Value.ToString().Trim() ));
                    _paraCols.Add(new SqlParameter(paramName, filter.Value));

                    qb.Add(string.Format(_operators[filter.Operator], filter.Member, paramName));
                }
            }

            return "(" + (qb.Count > 0 ? string.Join(string.Format(" {0} ", operand), qb) : "1 = 1") + ")";
        }
        protected virtual string BuildSort<T>()
        {
            var sortList = new List<string>();
            var sortables = DataTableRequest.Sorts;

            if (sortables != null && sortables.Count > 0)
            {
                foreach (var sort in sortables)
                {
                    sortList.Add(string.Format("{0} {1}", sort.Member, sort.SortDirection == System.ComponentModel.ListSortDirection.Ascending ? "ASC" : "DESC"));
                }
            }
            else
            {
                var type = typeof(T);
                var propertyName = typeof(T) != typeof(object) ? type.GetProperties().First().Name : "ID";

                sortList.Add(propertyName + " ASC");
            }

            return string.Join(", ", sortList);
        }

        /*public DataSourceResult GetDataType<T>()
        {
            string sqlQuery =
                      string.Format('Select top 10 * from tabel')

            IEnumerable data = null;
            data = Connection.Query<T>(
                    sqlQuery
                ).ToList();

            return new DataSourceResult { Data = data };
        }*/
    }
}
