﻿using Agit.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tam.Ebilling.Domain
{
    public partial interface IDbHelper : IDisposable
    {
        IDbConnection Connection { get; }
        IDbTransaction Transaction { get; }
        IMenuGroupRepository MenuGroupRepository { get; }
        IMenuRepository MenuRepository { get; }
        IConfigRepository ConfigRepository { get; }
        IEmailTemplateRepository EmailTemplateRepository { get; }
        ILogRepository LogRepository { get; }
        IInvoiceRepository InvoiceRepository { get; }
        ICommentRepository CommentRepository { get; }
        ICommentDetailRepository CommentDetailRepository { get; }
        INotaReturRepository NotaReturRepository { get; }
        IAttachmentRepository AttachmentRepository { get; }
        IAttachmentNotaReturRepository AttachmentNotaReturRepository { get; }
        IScanInvoiceRepository ScanInvoiceRepository { get; }
        IScanInvoiceNoturRepository ScanInvoiceNoturRepository { get; }
        IUserRepository UserRepository { get; }
        ITransactionTypePicRepository TransactionTypePicRepository { get; }
        IParameterRepository ParameterRepository { get; }
        ICustomerRepository CustomerRepository { get; }

        IKwitansiRepository KwitansiRepository { get; }
        ICustomerGroupRepository CustomerGroupRepository { get; }

        IWHTRepository WHTRepository { get; }
        IWHTDealerRepository WHTDealerRepository { get; }
        IDealerCodeRepository DealerCodeRepository { get; }

        ISummaryInvoiceRepository SummaryInvoiceRepository { get; }
        IReceiptRepository ReceiptRepository { get; }
        IBuktiPotongRepository BuktiPotongRepository { get; }
        IBuktiPotongAttachmentRepository BuktiPotongAttachmentRepository { get; }

        IMenuFeatureRepository MenuFeatureRepository { get; }
        IMenuLevelFeatureRepository MenuLevelFeatureRepository { get; }
        IMenuRoleRepository MenuRoleRepository { get; }

        IPVMonitoringRepository PVMonitoringRepository { get; }
        IPVMonitoringAttachmentRepository PVMonitoringAttachmentRepository { get; }

        //add by Hendry
        IDRKBRepository DRKBRepository { get; }

        INotifRepository NotifRepository { get; }

    }
    public class DbHelper : DbContext, IDbHelper
    {
        public DbHelper(bool UseTransaction = false) : base(UseTransaction)
        {
        }

        public IDbConnection Connection
        {
            get
            {
                return base._connection;
            }
        }

        public IDbTransaction Transaction
        {
            get
            {
                return base._transaction;
            }
        }
        
        private IMenuGroupRepository _MenuGroupRepository;
        private IMenuRepository _MenuRepository;
        private ILogRepository _LogRepository;
        private IConfigRepository _ConfigRepository;
        private IEmailTemplateRepository _EmailTemplateRepository;
        private IInvoiceRepository _InvoiceRepository;
        private ICommentRepository _CommentRepository;
        private ICommentDetailRepository _CommentDetailRepository;
        private INotaReturRepository _NotaReturRepository;
        private IAttachmentRepository _AttachmentRepository;
        private IAttachmentNotaReturRepository _AttachmentNotaReturRepository;
        private IScanInvoiceRepository _ScanInvoiceRepository;
        private IScanInvoiceNoturRepository _ScanInvoiceNoturRepository;
        private IUserRepository _UserRepository;
        private ITransactionTypePicRepository _TransactionTypePicRepository;
        private IParameterRepository _ParameterRepository;
        private ICustomerRepository _CustomerRepository;
        private IKwitansiRepository _KwitansiRepository;
        private ICustomerGroupRepository _CustomerGrouopRepository;

        private IWHTRepository _WHTRepository;
        private IWHTDealerRepository _WHTDealerRepository;
        private IDealerCodeRepository _DealerCodeRepository;

        private ISummaryInvoiceRepository _SummaryInvoiceRepository;
        private IReceiptRepository _ReceiptRepository;
        private IBuktiPotongRepository _BuktiPotongRepository;
        private IBuktiPotongAttachmentRepository _BuktiPotongAttachmentRepository;
        private IMenuFeatureRepository _MenuFeatureRepository;
        private IMenuLevelFeatureRepository _MenuLevelFeatureRepository;
        private IMenuRoleRepository _MenuRoleRepository;

        private IPVMonitoringRepository _PVMonitoringRepository;
        private IPVMonitoringAttachmentRepository _PVMonitoringAttachmentRepository;

        //Add by hendry
        private IDRKBRepository _DRKBRepository;
        private INotifRepository _NotifRepository;


        public ILogRepository LogRepository
        {
            get { return _LogRepository ?? (_LogRepository = new LogRepository(_connection, _transaction)); }
        }
        public IConfigRepository ConfigRepository
        {
            get { return _ConfigRepository ?? (_ConfigRepository = new ConfigRepository(_connection, _transaction)); }
        }
        public IEmailTemplateRepository EmailTemplateRepository
        {
            get { return _EmailTemplateRepository ?? (_EmailTemplateRepository = new EmailTemplateRepository(_connection, _transaction)); }
        }
        public IMenuGroupRepository MenuGroupRepository
        {
            get { return _MenuGroupRepository ?? (_MenuGroupRepository = new MenuGroupRepository(_connection, _transaction)); }
        }
        public IMenuRepository MenuRepository
        {
            get { return _MenuRepository ?? (_MenuRepository = new MenuRepository(_connection, _transaction)); }
        }

        public IInvoiceRepository InvoiceRepository
        {
            get { return _InvoiceRepository ?? (_InvoiceRepository = new InvoiceRepository(_connection, _transaction)); }
        }

        public ICommentRepository CommentRepository
        {
            get { return _CommentRepository ?? (_CommentRepository = new CommentRepository(_connection, _transaction)); }
        }
        public ICommentDetailRepository CommentDetailRepository
        {
            get { return _CommentDetailRepository ?? (_CommentDetailRepository = new CommentDetailRepository(_connection, _transaction)); }
        }
        public INotaReturRepository NotaReturRepository
        {
            get { return _NotaReturRepository ?? (_NotaReturRepository = new NotaReturRepository(_connection, _transaction)); }
        }
        public IAttachmentRepository AttachmentRepository
        {
            get { return _AttachmentRepository ?? (_AttachmentRepository = new AttachmentRepository(_connection, _transaction)); }
        }
        public IAttachmentNotaReturRepository AttachmentNotaReturRepository
        {
            get { return _AttachmentNotaReturRepository ?? (_AttachmentNotaReturRepository = new AttachmentNotaReturRepository(_connection, _transaction)); }
        }
        public IScanInvoiceRepository ScanInvoiceRepository
        {
            get { return _ScanInvoiceRepository ?? (_ScanInvoiceRepository = new ScanInvoiceRepository(_connection, _transaction)); }
        }
        public IScanInvoiceNoturRepository ScanInvoiceNoturRepository
        {
            get { return _ScanInvoiceNoturRepository ?? (_ScanInvoiceNoturRepository = new ScanInvoiceNoturRepository(_connection, _transaction)); }
        }
        public IUserRepository UserRepository
        {
            get { return _UserRepository ?? (_UserRepository = new UserRepository(_connection, _transaction)); }
        }

        public ITransactionTypePicRepository TransactionTypePicRepository
        {
            get { return _TransactionTypePicRepository ?? (_TransactionTypePicRepository = new TransactionTypePicRepository(_connection, _transaction)); }
        }

        public IParameterRepository ParameterRepository
        {
            get { return _ParameterRepository ?? (_ParameterRepository = new ParameterRepository(_connection, _transaction)); }
        }

        public ICustomerRepository CustomerRepository
        {
            get { return _CustomerRepository ?? (_CustomerRepository = new CustomerRepository(_connection, _transaction)); }
        }

        public IKwitansiRepository KwitansiRepository
        {
            get { return _KwitansiRepository ?? (_KwitansiRepository = new KwitansiRepository(_connection, _transaction)); }
        }

        public ICustomerGroupRepository CustomerGroupRepository
        {
            get { return _CustomerGrouopRepository ?? (_CustomerGrouopRepository = new CustomerGroupRepository(_connection, _transaction)); }
        }

        public IWHTRepository WHTRepository
        {
            get { return _WHTRepository ?? (_WHTRepository = new WHTRepository(_connection, _transaction)); }
        }

        public IWHTDealerRepository WHTDealerRepository
        {
            get { return _WHTDealerRepository ?? (_WHTDealerRepository = new WHTDealerRepository(_connection, _transaction)); }
        }

        public IDealerCodeRepository DealerCodeRepository
        {
            get { return _DealerCodeRepository ?? (_DealerCodeRepository = new DealerCodeRepository(_connection, _transaction)); }
        }

        public ISummaryInvoiceRepository SummaryInvoiceRepository
        {
            get { return _SummaryInvoiceRepository ?? (_SummaryInvoiceRepository = new SummaryInvoiceRepository(_connection, _transaction)); }
        }

        public IReceiptRepository ReceiptRepository
        {
            get { return _ReceiptRepository ?? (_ReceiptRepository = new ReceiptRepository(_connection, _transaction)); }
        }

        public IBuktiPotongRepository BuktiPotongRepository
        {
            get { return _BuktiPotongRepository ?? (_BuktiPotongRepository = new BuktiPotongRepository(_connection, _transaction)); }
        }

        public IBuktiPotongAttachmentRepository BuktiPotongAttachmentRepository
        {
            get { return _BuktiPotongAttachmentRepository ?? (_BuktiPotongAttachmentRepository = new BuktiPotongAttachmentRepository(_connection, _transaction)); }
        }

        public IMenuFeatureRepository MenuFeatureRepository
        {
            get { return _MenuFeatureRepository ?? (_MenuFeatureRepository = new MenuFeatureRepository(_connection, _transaction)); }
        }

        public IMenuLevelFeatureRepository MenuLevelFeatureRepository
        {
            get { return _MenuLevelFeatureRepository ?? (_MenuLevelFeatureRepository = new MenuLevelFeatureRepository(_connection, _transaction)); }
        }

        public IMenuRoleRepository MenuRoleRepository
        {
            get { return _MenuRoleRepository ?? (_MenuRoleRepository = new MenuRoleRepository(_connection, _transaction)); }
        }

        public IPVMonitoringRepository PVMonitoringRepository
        {
            get { return _PVMonitoringRepository ?? (_PVMonitoringRepository = new PVMonitoringRepository(_connection, _transaction)); }
        }

        public IPVMonitoringAttachmentRepository PVMonitoringAttachmentRepository
        {
            get { return _PVMonitoringAttachmentRepository ?? (_PVMonitoringAttachmentRepository = new PVMonitoringAttachmentRepository(_connection, _transaction)); }
        }

        //Add By Hendry
        public IDRKBRepository DRKBRepository
        {
            get { return _DRKBRepository ?? (_DRKBRepository = new DRKBRepository(_connection, _transaction)); }
        }
        public INotifRepository NotifRepository
        {
            get { return _NotifRepository ?? (_NotifRepository = new NotifRepository(_connection, _transaction)); }
        }

        public override void resetRepositories()
        {

            _LogRepository = null;
            _ConfigRepository = null;
            _EmailTemplateRepository = null;
            _MenuRepository = null;
            _InvoiceRepository = null;
            _NotaReturRepository = null;
            _CommentRepository = null;
            _MenuGroupRepository = null;
            _AttachmentRepository = null;
            _AttachmentNotaReturRepository = null;
            _UserRepository = null;
            _TransactionTypePicRepository = null;
            _ParameterRepository = null;
            _CustomerRepository = null;
            _KwitansiRepository = null;

            _WHTRepository = null;
            _WHTDealerRepository = null;

	        _SummaryInvoiceRepository = null;
            _ReceiptRepository = null;
            _BuktiPotongRepository = null;
            _BuktiPotongAttachmentRepository = null;
            _MenuFeatureRepository = null;
            _MenuLevelFeatureRepository = null;
            _MenuRoleRepository = null;

            _PVMonitoringRepository = null;
            _PVMonitoringAttachmentRepository = null;

            //Add By Hendry
            _DRKBRepository = null;
            _NotifRepository = null;

        }

    }
}
