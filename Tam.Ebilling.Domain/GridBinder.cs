﻿using Kendo.Mvc;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tam.Ebilling.Domain
{
    public class GridBinder
    {
        public int PageNumber { get; set; } = 1;

        public int PageSize { get; set; } = 10;

        public int RecordCount { get; set; }

        public SortInfo SortInfo { get; set; } = new SortInfo() { Direction = SortDirection.Asc, Member = string.Empty };

        private readonly DataSourceRequest _command;

        public GridBinder(DataSourceRequest command)
        {
            _command = command;
            PageNumber = command.Page;
            PageSize = command.PageSize;
            GetSortDescriptor();
        }

        private void GetSortDescriptor()
        {

            foreach (SortDescriptor descriptor in _command.Sorts)
            {
                SortInfo.Member = descriptor.Member;
                SortInfo.Direction = descriptor.SortDirection == ListSortDirection.Ascending ? SortDirection.Asc : SortDirection.Desc;
            }
        }

        public string GetSortDescriptorArry()
        {
            var sortList = new List<string>();

            foreach (SortDescriptor descriptor in _command.Sorts)
            {
                SortInfo.Member = descriptor.Member;
                SortInfo.Direction = descriptor.SortDirection == ListSortDirection.Ascending ? SortDirection.Asc : SortDirection.Desc;
                sortList.Add(string.Format("{0} {1}", SortInfo.Member, SortInfo.Direction));
            }
            return string.Join(", ", sortList);
        }

        

        public string GetFilterDescriptor()
        {
            string filters = string.Empty;
            foreach (IFilterDescriptor filter in _command.Filters)
            {
                filters += ApplyFilter(filter);
            }

            return filters;
        }

        private static string ApplyFilter(IFilterDescriptor filter)
        {
            var filters = "";
            if (filter is CompositeFilterDescriptor)
            {
                filters += "(";
                var compositeFilterDescriptor = (CompositeFilterDescriptor)filter;
                foreach (IFilterDescriptor childFilter in compositeFilterDescriptor.FilterDescriptors)
                {
                    filters += ApplyFilter(childFilter);
                    filters += " " + compositeFilterDescriptor.LogicalOperator.ToString() + " ";
                }
            }
            else
            {
               
                string filterDescriptor = "{0} {1} {2}";
                var descriptor = (FilterDescriptor)filter;

                descriptor.Value = (descriptor.Value.GetType() == typeof(DateTime) ? String.Format("{0:yyyy-MM-dd HH:mm:ss}", descriptor.Value) : descriptor.Value);

                if (descriptor.Operator == FilterOperator.StartsWith)
                {
                    filterDescriptor = string.Format(filterDescriptor, descriptor.Member, "LIKE", "'" + descriptor.Value + "%'");
                }
                else if (descriptor.Operator == FilterOperator.EndsWith)
                {
                    filterDescriptor = string.Format(filterDescriptor, descriptor.Member, "LIKE", "'%" + descriptor.Value + "'");
                }
                else if (descriptor.Operator == FilterOperator.Contains)
                {
                    filterDescriptor = string.Format(filterDescriptor, descriptor.Member, "LIKE", "'%" + descriptor.Value + "%'");
                }
                else if (descriptor.Operator == FilterOperator.DoesNotContain)
                {
                    filterDescriptor = string.Format(filterDescriptor, descriptor.Member, "NOT LIKE", "'%" + descriptor.Value + "%'");
                }
                else if (descriptor.Operator == FilterOperator.IsEqualTo)
                {
                    filterDescriptor = string.Format(filterDescriptor, descriptor.Member, "=", "'" + descriptor.Value + "'");
                }
                else if (descriptor.Operator == FilterOperator.IsNotEqualTo)
                {
                    filterDescriptor = string.Format(filterDescriptor, descriptor.Member, "<>", "'" + descriptor.Value + "'");
                }
                else if (descriptor.Operator == FilterOperator.IsGreaterThan)
                {
                    filterDescriptor = string.Format(filterDescriptor, descriptor.Member, ">", "'" + descriptor.Value + "'");
                }
                else if (descriptor.Operator == FilterOperator.IsGreaterThanOrEqualTo)
                {
                    filterDescriptor = string.Format(filterDescriptor, descriptor.Member, ">=", "'" + descriptor.Value + "'");
                }
                else if (descriptor.Operator == FilterOperator.IsLessThan)
                {
                    filterDescriptor = string.Format(filterDescriptor, descriptor.Member, "<", "'" + descriptor.Value + "'");
                }
                else if (descriptor.Operator == FilterOperator.IsLessThanOrEqualTo)
                {
                    filterDescriptor = string.Format(filterDescriptor, descriptor.Member, "<=", "'" + descriptor.Value + "'");
                }

                filters = filterDescriptor;
            }

            filters = filters.EndsWith("And ") == true ? filters.Substring(0, filters.Length - 4) + ")" : filters;
            filters = filters.EndsWith("Or ") == true ? filters.Substring(0, filters.Length - 4) + ")" : filters;

            return filters;
        }
    }
    public class SortInfo
    {
        public string Member { get; set; }
        public SortDirection Direction { get; set; }
    }

    public enum SortDirection
    {
        Asc, Desc
    }
}
