﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace Tam.Ebilling.Notification
{ 
    public class sqlsrv
    {
        SqlConnection conn = new SqlConnection();

        public static void WriteErrorLog(Exception ex)
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", true);
                sw.WriteLine(DateTime.Now.ToString() + ": " + ex.Source.ToString().Trim() + "; " + ex.Message.ToString().Trim());
                sw.Flush();
                sw.Close();
            }
            catch
            {
            }
        }
        // This function write Message to log file.
        public static void WriteErrorLog(string Message)
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", true);
                sw.WriteLine(DateTime.Now.ToString() + ": " + Message);
                sw.Flush();
                sw.Close();
            }
            catch
            {
            }
        }
        public sqlsrv()
        {
            try
            {
                conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString);
                conn.Open();
            }
            catch (Exception err)
            {
                WriteErrorLog("SQL CONNECT (" + DateTime.Now + ") : " + err.Message + "");
                //error.error_log("SQL CONNECT : " + err.Message);
                conn.Close();
            }
        }

        public DataSet sql_select(string cmd)
        {
            DataSet ds = new DataSet();

            try
            {
                SqlDataAdapter data = new SqlDataAdapter(cmd, conn);
                data.Fill(ds);
            }
            catch (Exception err)
            {
                ds.Clear();
                WriteErrorLog("SQL SELECT (" + DateTime.Now + ") : " + err.Message + "");
                WriteErrorLog("SELECT CMD (" + DateTime.Now + ") : " + cmd + "");
                //error.error_log("SQL SELECT : " + err.Message);
                //error.error_log("SELECT CMD : " + cmd);
            }
            return ds;
        }

        public void string_cmd(string cmd)
        {
            try
            {
                SqlCommand data = new SqlCommand();
                data.CommandText = cmd;
                data.CommandType = CommandType.Text;
                data.Connection = conn;

                data.ExecuteScalar();
            }
            catch (Exception err)
            {
                WriteErrorLog("SQL COMMAND (" + DateTime.Now + ") : " + err.Message + "");
                WriteErrorLog("STRING CMD (" + DateTime.Now + ") : " + cmd + "");
                //error.error_log("SQL COMMAND : " + err.Message);
                //error.error_log("STRING CMD : " + cmd);
            }
        }

        public void close_con()
        {
            try
            {
                conn.Close();
            }
            catch (Exception err)
            {
                WriteErrorLog("SQL CLOSE (" + DateTime.Now + ") : " + err.Message + "");
                //error.error_log("SQL CLOSE : " + err.Message);
            }
        }
    }
}
