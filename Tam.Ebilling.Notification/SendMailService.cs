﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.IO;
using System.Net.Mail;
using Tam.Ebilling.Domain;
using System.Net;
using System.Globalization;

namespace Tam.Ebilling.Notification
{
    public class SendMailService
    {
       public static void WriteErrorLog(Exception ex)
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", true);
                sw.WriteLine(DateTime.Now.ToString() + ": " + ex.Source.ToString().Trim() + "; " + ex.Message.ToString().Trim());
                sw.Flush();
                sw.Close();
            }
            catch
            {
            }
        }
        // This function write Message to log file.
        public static void WriteErrorLog(string Message)
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", true);
                sw.WriteLine(DateTime.Now.ToString() + ": " + Message);
                sw.Flush();
                sw.Close();
            }
            catch
            {
            }
        }

        public static void WriteAppLog(string logType, string logDesc)
        {
            using (var db = new DbHelper())
            {
                var log = new ApplicationLog
                {
                    created_by = "Service",
                    created_date = DateTime.Now,
                    username = "Service",
                    message_type = logType,
                    message_location = string.Format("<b>Module:</b> {0}", "Invoice Notification Service"),
                    message_description = logDesc,
                    iP = Dns.GetHostAddresses(Dns.GetHostName())[0].ToString(),
                    browser = string.Format("<b>Browser:</b> {0}<br/><b>Version:</b> {1}", "None", "None")
                };

                db.LogRepository.Add(log, null);
            }
        }

        public static void SendEmailInvoice()
        {
            try
            {
                string username, password, domain, host, port, subject, body, from;
                string jumdown, jumnotdown;
                List<string> to = new List<string>();
                List<string> usertam = new List<string>();
                // Generate email setting from tb_m_parameter
                using (var db = new DbHelper())
                {
                    var invoices = db.InvoiceRepository.GetInvoiceByDlStatus(null, 1);
                    var usernew = db.InvoiceRepository.GetInvoiceUser();
                    WriteAppLog("Info", "Invoice Notification Service is starting.");

                    username = db.ParameterRepository.Find(new { key_param = "email_username_chat_notification" }).FirstOrDefault().value1;
                    password = db.ParameterRepository.Find(new { key_param = "email_password_chat_notification" }).FirstOrDefault().value1;
                    domain = db.ParameterRepository.Find(new { key_param = "email_domain_chat_notification" }).FirstOrDefault().value1;
                    host = db.ParameterRepository.Find(new { key_param = "email_host_chat_notification" }).FirstOrDefault().value1;
                    port = db.ParameterRepository.Find(new { key_param = "email_port_chat_notification" }).FirstOrDefault().value1;
                    subject = db.EmailTemplateRepository.Find(new { module = "Download Invoice Email Reminder", mail_key = "email_key" }).FirstOrDefault().subject;
                    //body = db.EmailTemplateRepository.Find(new { module = "Download Invoice Email Reminder", mail_key = "email_key" }).FirstOrDefault().mail_content;
                    from = db.ParameterRepository.Find(new { key_param = "email_username" }).FirstOrDefault().value1;

                    foreach (var inv in invoices)
                    {
                        
                        body = db.EmailTemplateRepository.Find(new { module = "Download Invoice Email Reminder", mail_key = "email_key" }).FirstOrDefault().mail_content;

                        var users = db.UserRepository.Find(new { customer_group = inv.customer_group });
                          
                            var downloaded = db.InvoiceRepository.GetInvoiceDownload("download", inv.customer_group);
                            var notdownloaded = db.InvoiceRepository.GetInvoiceDownload("notdownload", inv.customer_group);

                            if (downloaded.Count() > 0)
                            {
                                foreach (var invdown in downloaded)
                                {
                                    jumdown = invdown.invoice_number;
                                    body = body.Replace("{JumInvDownload}", jumdown);
                                }
                            }
                            else
                            {
                                body = body.Replace("{JumInvDownload}", "0");
                            }

                            if (notdownloaded.Count() > 0)
                            {
                                foreach (var invnotdown in notdownloaded)
                                {
                                    jumnotdown = invnotdown.invoice_number;
                                    body = body.Replace("{JumInvNotDownload}", jumnotdown);
                                }
                            }
                            else
                            {
                                body = body.Replace("{JumInvNotDownload}", "0");
                            }

                            body = body.Replace("{CustomerGroup}", inv.customer_group);


                            foreach (var usr in users)
                            {
                                to.Clear();
                                if (!string.IsNullOrEmpty(usr.email))
                                {
                                    to.Add(usr.email);
                                }
                            }

                        //EmailService emailSvc = new EmailService();
                        SmtpClient emailSvc = new SmtpClient();
                            emailSvc.Host = host;
                            //emailSvc.User = username;
                            //emailSvc.Password = password;
                            emailSvc.UseDefaultCredentials = false;
                            emailSvc.Credentials = new NetworkCredential(username, password);
                            emailSvc.EnableSsl = false;
                            if (from.Contains("gmail")) { emailSvc.EnableSsl = true; }

                            //EmailMessage emailMessage = new EmailMessage();
                            MailMessage emailMessage = new MailMessage(from, from);

                            emailMessage.From = new MailAddress(from);
                            emailMessage.To.RemoveAt(0);
                            emailMessage.Subject = subject;
                            emailMessage.Body = body;
                            emailMessage.IsBodyHtml = true;
                        foreach (var userss in usernew)
                        {
                            usertam.Clear();
                            if (!string.IsNullOrEmpty(userss.email))
                            {
                                usertam.Add(userss.email);
                                emailMessage.CC.Add(userss.email);
                            }
                        }
                        foreach (var mail in to)
                            {
                                //emailMessage.Recipients.Add(mail);
                                emailMessage.To.Add(new MailAddress(mail));
                            }

                            emailSvc.Send(emailMessage);
                        WriteAppLog("Info Invoice", "From:" + emailMessage.From + "To:" + emailMessage.To);
                    }

                        WriteAppLog("Info", "Invoice Notification Service successfully ended.");
                    
                }
            }
            catch (Exception ex)
            {
                WriteAppLog("Err", "Invoice Notification error: " + ex.Message + ". Stack Trace: " + ex.StackTrace + ".");
                throw;
            }
        }

        public static void SendEmailNotur()
        {
            try
            {
                string username, password, domain, host, port, subject, body, from;
                string jumdown, jumnotdown, jumupload, jumnotupload;
                List<string> to = new List<string>();
                List<string> usertam = new List<string>();
                // Generate email setting from tb_m_parameter
                using (var db = new DbHelper())
                {
                    var invoices = db.NotaReturRepository.GetNoturByUploadStatus(null, 2);
                    var usernew = db.InvoiceRepository.GetInvoiceUser();
                    WriteAppLog("Info", "NotaRetur Notification Service is starting.");

                    username = db.ParameterRepository.Find(new { key_param = "email_username_chat_notification" }).FirstOrDefault().value1;
                    password = db.ParameterRepository.Find(new { key_param = "email_password_chat_notification" }).FirstOrDefault().value1;
                    domain = db.ParameterRepository.Find(new { key_param = "email_domain_chat_notification" }).FirstOrDefault().value1;
                    host = db.ParameterRepository.Find(new { key_param = "email_host_chat_notification" }).FirstOrDefault().value1;
                    port = db.ParameterRepository.Find(new { key_param = "email_port_chat_notification" }).FirstOrDefault().value1;
                    subject = db.EmailTemplateRepository.Find(new { module = "Download Nota Retur Email Reminder", mail_key = "email_key" }).FirstOrDefault().subject;
                    //body = db.EmailTemplateRepository.Find(new { module = "Download Invoice Email Reminder", mail_key = "email_key" }).FirstOrDefault().mail_content;
                    from = db.ParameterRepository.Find(new { key_param = "email_username" }).FirstOrDefault().value1;

                    foreach (var inv in invoices)
                    {
                        body = db.EmailTemplateRepository.Find(new { module = "Download Nota Retur Email Reminder", mail_key = "email_key" }).FirstOrDefault().mail_content;

                        var users = db.UserRepository.Find(new { customer_group = inv.customer_group });
                        var uploaded = db.NotaReturRepository.GetNoturUpload("upload", inv.customer_group);

                        if (uploaded.Count() > 0)
                        {
                            foreach (var invupload in uploaded)
                            {
                                jumupload = invupload.invoice_number;
                                body = body.Replace("{JumInvUpload}", jumupload);
                            }

                            body = body.Replace("{CustomerGroup}", inv.customer_group);

                            foreach (var usr in users)
                            {
                                to.Clear();
                                if (!string.IsNullOrEmpty(usr.email))
                                {
                                    to.Add(usr.email);
                                }
                            }

                            //EmailService emailSvc = new EmailService();
                            SmtpClient emailSvc = new SmtpClient();
                            emailSvc.Host = host;
                            //emailSvc.User = username;
                            //emailSvc.Password = password;
                            emailSvc.UseDefaultCredentials = false;
                            emailSvc.Credentials = new NetworkCredential(username, password);
                            emailSvc.EnableSsl = false;
                            if (from.Contains("gmail")) { emailSvc.EnableSsl = true; }

                            //EmailMessage emailMessage = new EmailMessage();
                            MailMessage emailMessage = new MailMessage(from, from);
                            emailMessage.From = new MailAddress(from);
                            emailMessage.To.RemoveAt(0);
                            emailMessage.Subject = subject;
                            emailMessage.Body = body;
                            emailMessage.IsBodyHtml = true;
                            foreach (var userss in usernew)
                            {
                                usertam.Clear();
                                if (!string.IsNullOrEmpty(userss.email))
                                {
                                    usertam.Add(userss.email);
                                    emailMessage.CC.Add(userss.email);
                                }
                            }
                            foreach (var mail in to)
                            {
                                //emailMessage.Recipients.Add(mail);
                                emailMessage.To.Add(new MailAddress(mail));
                            }

                            emailSvc.Send(emailMessage);
                            WriteAppLog("Info Notur", "From:" + emailMessage.From + "To:" + emailMessage.To);
                        }
                        else
                        {
                            //body = body.Replace("{JumInvUpload}", "0");
                        }
                    }

                    WriteAppLog("Info", "NotaRetur Notification Service successfully ended.");
                }
            }
            catch (Exception ex)
            {
                WriteAppLog("Err", "NotaRetur Notification error: " + ex.Message + ". Stack Trace: " + ex.StackTrace + ".");
                throw;
            }
        }
        //public static void SendEmailDueDate()
        //{
        //    try
        //    {
        //        string username, password, domain, host, port, subject, body, from;
        //        //string jumdown, jumnotdown;
        //        List<string> to = new List<string>();

        //        // Generate email setting from tb_m_parameter
        //        using (var db = new DbHelper())
        //        {
        //            var invoices = db.InvoiceRepository.GetCust(1);

        //            var dueDate2 = invoices.Where(x => x.due_date >= DateTime.Today.AddDays(2) && x.due_date < DateTime.Today.AddDays(4));
        //            var dueDate4 = invoices.Where(x => x.due_date >= DateTime.Today.AddDays(4) && x.due_date < DateTime.Today.AddDays(6));
        //            var dueDate6 = invoices.Where(x => x.due_date >= DateTime.Today.AddDays(6));

        //            string jumdown;
        //            WriteAppLog("Info", "Due Date Notification Service is starting.");

        //            username = db.ParameterRepository.Find(new { key_param = "email_username_chat_notification" }).FirstOrDefault().value1;
        //            password = db.ParameterRepository.Find(new { key_param = "email_password_chat_notification" }).FirstOrDefault().value1;
        //            domain = db.ParameterRepository.Find(new { key_param = "email_domain_chat_notification" }).FirstOrDefault().value1;
        //            host = db.ParameterRepository.Find(new { key_param = "email_host_chat_notification" }).FirstOrDefault().value1;
        //            port = db.ParameterRepository.Find(new { key_param = "email_port_chat_notification" }).FirstOrDefault().value1;
        //            subject = db.EmailTemplateRepository.Find(new { module = "Due Date Invoice Reminder", mail_key = "email_key" }).FirstOrDefault().subject;
        //            body = db.EmailTemplateRepository.Find(new { module = "Due Date Invoice Reminder", mail_key = "email_key" }).FirstOrDefault().mail_content;
        //            from = username;

        //            foreach (var inv in invoices)
        //            {
        //                var users = db.UserRepository.Find(new { customer_group = inv.customer_group });
        //                var date = db.InvoiceRepository.GetDueDateInvoice(inv.customer_group);
        //                // var notdownloaded = db.InvoiceRepository.GetInvoiceDownload("notdownload", inv.customer_group);

        //                DateTime currentDate = inv.due_date;
        //                if (inv.due_date > currentDate.AddDays(2))
        //                {
        //                    foreach (var invdown in date)
        //                    {
        //                        jumdown = invdown.invoice_number;
        //                        body = body.Replace("{JumInvDueDate}", jumdown);
        //                    }
        //                }
        //                else
        //                {
        //                    body = body.Replace("{JumInvDownload}", "0");
        //                }

        //                body = body.Replace("{CustomerGroup}", inv.customer_group);

        //                foreach (var usr in users)
        //                {
        //                    to.Clear();
        //                    if (!string.IsNullOrEmpty(usr.email))
        //                    {
        //                        to.Add(usr.email);
        //                    }
        //                }

        //                //EmailService emailSvc = new EmailService();
        //                SmtpClient emailSvc = new SmtpClient();
        //                emailSvc.Host = host;
        //                //emailSvc.User = username;
        //                //emailSvc.Password = password;
        //                emailSvc.UseDefaultCredentials = false;
        //                emailSvc.Credentials = new NetworkCredential(username, password);
        //                emailSvc.EnableSsl = false;
        //                if (from.Contains("gmail")) { emailSvc.EnableSsl = true; }

        //                //EmailMessage emailMessage = new EmailMessage();
        //                MailMessage emailMessage = new MailMessage(from, from);
        //                emailMessage.From = new MailAddress(from);
        //                emailMessage.To.RemoveAt(0);
        //                emailMessage.Subject = subject;
        //                emailMessage.Body = body;
        //                emailMessage.IsBodyHtml = true;

        //                foreach (var mail in to)
        //                {
        //                    //emailMessage.Recipients.Add(mail);
        //                    emailMessage.To.Add(new MailAddress(mail));
        //                }

        //                emailSvc.Send(emailMessage);
        //            }

        //            WriteAppLog("Info", "Due Date Invoice Notification Service successfully ended.");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        WriteAppLog("Err", "Due Date Invoice Notification error: " + ex.Message + ". Stack Trace: " + ex.StackTrace + ".");
        //        throw;
        //    }
        //}

        public static void SendEmailDueDate(int dueDate)
        {
            try
            {
                string username, password, domain, host, port, subject, body, from, rows;
                string inv_number, doc_date, due_date, tot_amount, amount;
                List<string> to = new List<string>();

                // Generate email setting from tb_m_parameter
                using (var db = new DbHelper())
                {
                    WriteAppLog("Info", "Due Date Notification Service is starting.");

                    username = db.ParameterRepository.Find(new { key_param = "email_username_chat_notification" }).FirstOrDefault().value1;
                    password = db.ParameterRepository.Find(new { key_param = "email_password_chat_notification" }).FirstOrDefault().value1;
                    domain = db.ParameterRepository.Find(new { key_param = "email_domain_chat_notification" }).FirstOrDefault().value1;
                    host = db.ParameterRepository.Find(new { key_param = "email_host_chat_notification" }).FirstOrDefault().value1;
                    port = db.ParameterRepository.Find(new { key_param = "email_port_chat_notification" }).FirstOrDefault().value1;
                    subject = db.EmailTemplateRepository.Find(new { module = "Due Date Invoice Reminder", mail_key = "email_key" }).FirstOrDefault().subject;
                   from = db.ParameterRepository.Find(new { key_param = "email_username" }).FirstOrDefault().value1;

                    var custGroup = db.CustomerGroupRepository.Get();

                    foreach (var grp in custGroup)
                    {
                        var invoices = db.InvoiceRepository.GetDueDateData(grp.customer_group_id);

                        if (dueDate == 6)
                        {
                            invoices = invoices.Where(x => x.due_date.AddDays(6) <= DateTime.Today).ToList();


                            if (!String.IsNullOrEmpty(grp.divhead_email))
                            {
                                to.Add(grp.spv_email);
                                to.Add(grp.depthead_email);
                                to.Add(grp.divhead_email);
                            }
                        }

                        if (dueDate == 4)
                        {
                            invoices = invoices.Where(x => x.due_date.AddDays(4) <= DateTime.Today).ToList();

                            if (!String.IsNullOrEmpty(grp.depthead_email))
                            {
                                to.Add(grp.spv_email);
                                to.Add(grp.depthead_email);
                            }
                        }

                        if (dueDate == 2)
                        {
                            invoices = invoices.Where(x => x.due_date.AddDays(2) <= DateTime.Today).ToList();

                            if (!String.IsNullOrEmpty(grp.spv_email))
                            {
                                to.Add(grp.spv_email);
                            }

                        }

                        if (invoices.Count() > 0)
                        {
                            body = db.EmailTemplateRepository.Find(new { module = "Due Date Invoice Reminder", mail_key = "email_key" }).FirstOrDefault().mail_content;

                            rows = "";

                            int index = 1;
                            foreach (var inv in invoices)
                            {
                                inv_number = inv.invoice_number;
                                doc_date = inv.doc_date.ToString("dd/MM/yyyy");
                                due_date = inv.due_date.ToString("dd/MM/yyyy");
                                //tot_amount = inv.total_amount.ToString();
                                amount = String.Format(CultureInfo.CreateSpecificCulture("id-id"), "Rp. {0:N0}", inv.total_amount);

                                rows += "<tr>";
                                rows += "<td>" + index + "</td>";
                                rows += "<td>" + inv_number + "</td>";
                                rows += "<td>" + doc_date + "</td>";
                                rows += "<td>" + due_date + "</td>";
                                rows += "<td>" + amount + "</td>";
                                rows += "</tr>";
                                index++;
                            }

                            body = body.Replace("{rows}", rows);

                            //EmailService emailSvc = new EmailService();
                            SmtpClient emailSvc = new SmtpClient();
                            emailSvc.Host = host;
                            emailSvc.UseDefaultCredentials = false;
                            emailSvc.Credentials = new NetworkCredential(username, password);
                            emailSvc.EnableSsl = false;
                            if (from.Contains("gmail")) { emailSvc.EnableSsl = true; }

                            //EmailMessage emailMessage = new EmailMessage();
                            MailMessage emailMessage = new MailMessage(from, from);
                            emailMessage.From = new MailAddress(from);
                            emailMessage.To.RemoveAt(0);
                            emailMessage.Subject = subject;
                            emailMessage.Body = body;
                            emailMessage.IsBodyHtml = true;

                            foreach (var mail in to)
                            {
                                //emailMessage.Recipients.Add(mail);
                                emailMessage.To.Add(new MailAddress(mail));
                            }

                            emailSvc.Send(emailMessage);
                        }
                    }
                }

                WriteAppLog("Info", "NotaRetur Notification Service successfully ended.");
            }
            catch (Exception ex)
            {
                WriteAppLog("Err", "NotaRetur Notification error: " + ex.Message + ". Stack Trace: " + ex.StackTrace + ".");
                throw;
            }
        }

        //public static void SendEmail(String ToEmail)
        public static void SendEmail()
        {
            SendEmailInvoice();
            SendEmailNotur();
            SendEmailDueDate(6);
            SendEmailDueDate(4);
            SendEmailDueDate(2);
        }
    }
}
