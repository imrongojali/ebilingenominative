﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Tam.Ebilling.Domain;

namespace Tam.Ebilling.Notification
{
    public class ReadSettlementFile
    {
        static string WatchPathIN = ConfigurationManager.AppSettings["WatchPathIN"];
        static string WatchPathOUT = ConfigurationManager.AppSettings["WatchPathOUT"];

        public static void ProsesData()
        {
            DirectoryInfo d = new DirectoryInfo(WatchPathIN);//Assuming Test is your Folder
            FileInfo[] Files = d.GetFiles("*.txt"); //Getting Text files

            foreach (FileInfo file in Files)
            {
                ReadTXT(file, file.Name);
            }
        }

        private static void ReadTXT(FileInfo file, string FileType)
        {
            if (file.Length > 0)
            {
                try
                {
                    using (StreamReader sr = new StreamReader(file.FullName))
                    {
                        string line;
                        while ((line = sr.ReadLine()) != null)
                        {
                            string[] a = line.Split('|');
                            string invoicenumber = a[0];
                            string[] invnumb = invoicenumber.Split('/');
                            string inv1 = invnumb[0];
                            string inv2 = invnumb[1];
                            string inv3 = invnumb[2];

                            string journalposting = a[1];
                            string journalclearing = a[2];
                            string clearingdate = a[3];

                            UpdateTableMV(inv3, journalposting, journalclearing, clearingdate);
                        }
                    }

                    string destination = System.IO.Path.Combine(@WatchPathOUT + "\\" + file.Name);
                    File.Move(file.FullName, destination); // Try to move
                    Thread.Sleep(1000);
                }
                catch (Exception ex)
                {
                    SendMailService.WriteAppLog("Err", "Error while process read Settlement file. Message: " + ex.Message + ". Stacktrace: + " + ex.StackTrace);
                }
            }
        }

        public static void UpdateTableMV(string inv3, string journalposting, string journalclearing, string clearingdate)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@ClearingDate", clearingdate);
            parameters.Add("@DaNo", inv3);
            parameters.Add("@JournalPosting", journalposting);
            parameters.Add("@JournalClearing", journalclearing);

            using (var db = new DbHelper())
            {
                db.Connection.Execute("usp_UpdateSettlementMV", parameters, commandType: CommandType.StoredProcedure);
            }
        }
    }
}
