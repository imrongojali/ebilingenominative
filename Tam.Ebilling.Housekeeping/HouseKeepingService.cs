﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Tam.Ebilling.Domain;
using Tam.Ebilling.Infrastructure.Helper;

namespace Tam.Ebilling.Housekeeping
{
    public class HouseKeepingService
    {
        public static void WriteErrorLog(Exception ex)
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", true);
                sw.WriteLine(DateTime.Now.ToString() + ": " + ex.Source.ToString().Trim() + "; " + ex.Message.ToString().Trim());
                sw.Flush();
                sw.Close();
            }
            catch
            {
            }
        }
        // This function write Message to log file.
        public static void WriteErrorLog(string Message)
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", true);
                sw.WriteLine(DateTime.Now.ToString() + ": " + Message);
                sw.Flush();
                sw.Close();
            }
            catch
            {
            }
        }

        public static void WriteAppLog(string logType, string logDesc)
        {
            using (var db = new DbHelper())
            {
                var log = new ApplicationLog
                {
                    created_by = "Service",
                    created_date = DateTime.Now,
                    username = "Service",
                    message_type = logType,
                    message_location = string.Format("<b>Module:</b> {0}", "HouseKeeping folder temporary"),
                    message_description = logDesc,
                    iP = Dns.GetHostAddresses(Dns.GetHostName())[0].ToString(),
                    browser = string.Format("<b>Browser:</b> {0}<br/><b>Version:</b> {1}", "None", "None")
                };

                db.LogRepository.Add(log, null);
            }
        }

        public static void CleanFileTemp()
        {
            cleanBuktiPotongTempFolder();
        }

        private static void cleanBuktiPotongTempFolder()
        {
            try
            {
                int day = Convert.ToInt16(ConfigurationManager.AppSettings["FileOlderByDay"]);
                using (var db = new DbHelper())
                {
                    var username = db.ConfigRepository.Find(new { config_key = "FileBuktiPotongUserName" }).FirstOrDefault().config_value; 
                    var password = db.ConfigRepository.Find(new { config_key = "FileBuktiPotongPassword" }).FirstOrDefault().config_value; 
                    var config = db.ConfigRepository.Find(new { config_key = "FileBuktiPotongFolderTemp" }).FirstOrDefault().config_value; 
                    var domain = db.ConfigRepository.Find(new { config_key = "FileBuktiPotongDomain" }).FirstOrDefault().config_value;

                    WriteAppLog("Info", "HouseKeeping Service on " + config + " is starting.");

                    var userdomain = domain + "\\" + username;
                    var pers = new Impersonation();
                    pers.Impersonate(userdomain, password);
                    var credential = new NetworkCredential(userdomain, password);
                    using (new NetworkConnection(config, credential))
                    {
                        string[] files = Directory.GetFiles(config);
                        foreach (string file in files)
                        {
                            FileInfo fi = new FileInfo(file);

                            if (fi.LastWriteTime < DateTime.Now.AddDays(-day))
                            {
                                fi.Delete();
                            }
                        }
                    }
                    WriteAppLog("Info", "HouseKeeping Service on " + config + " successfully ended.");
                }
                
            }
            catch (Exception ex)
            {
                WriteAppLog("Err", "HouseKeeping Service error: " + ex.Message + ". Stack Trace: " + ex.StackTrace + ".");
                throw;
            }
        }
    }
}
