﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace Tam.Ebilling.Housekeeping
{
    public partial class HouseKeeping : ServiceBase
    {
        private System.Timers.Timer timer = new System.Timers.Timer();
        private bool isInProgress;

        public HouseKeeping()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            HouseKeepingService.WriteAppLog("Info", "Starting Housekeeping Service...");

            isInProgress = false;
            timer.Enabled = true;
            timer.Interval = Convert.ToInt32(ConfigurationManager.AppSettings["Interval"].ToString()) * 1000;
            timer.Elapsed += new ElapsedEventHandler(ServiceTimer_Tick);
        }

        protected override void OnStop()
        {
            isInProgress = false;
            HouseKeepingService.WriteAppLog("Info", "Stopping Housekeeping Service...");
        }

        private void ServiceTimer_Tick(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (!isInProgress)
            {
                //DateTime dateschedule = Convert.ToDateTime(ConfigurationManager.AppSettings["ScheduleTime"]);
                //if (DateTime.Now.Hour == dateschedule.Hour && DateTime.Now.Minute == dateschedule.Minute)
                //{
                    try
                    {
                        isInProgress = true;
                        HouseKeepingService.CleanFileTemp();
                        Thread.Sleep(60 * 1000);   // hold 1 menit biar tidak masuk lagi di next interval
                        isInProgress = false;
                    }
                    catch (Exception ex)
                    {
                        HouseKeepingService.WriteAppLog("Err", "Error while looping service timer. Message: " + ex.Message + ". Stacktrace: + " + ex.StackTrace);
                        isInProgress = false;

                    }
                //}
            }
        }
    }
}
