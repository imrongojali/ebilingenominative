﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using Tam.Ebilling.Domain;
using Kendo.Mvc.UI;
using Tam.Ebilling.Infrastructure.Session;
using Tam.Ebilling.Service;
using Tam.Ebilling.Infrastructure.Enum;
using System.Data;

namespace Tam.Ebilling.Reporting
{
    public class Report
    {
        #region Get Debit Advice Report
        public byte[] GetDebitAdviceReport(string da_number, string business_unit_id, string user, out string extension, out string mimeType)
        {
            try
            {
                Stream stream = new MemoryStream();
                Warning[] warnings;
                string encoding, repPath, queryString, apiUrl = string.Empty;
                string[] streamIds;
                LocalReport report = new LocalReport();
                ReportViewer reportViewer = new ReportViewer();
                Infrastructure.Barcode.Barcode barcode = new Infrastructure.Barcode.Barcode();

                repPath = string.Format("{0}/{1}", ConfigurationManager.AppSettings["RDLPath"], ConfigurationManager.AppSettings["DebitAdvicePath"]);

                using (var db = new DbHelper())
                {
                    var inv = db.InvoiceRepository.GetInvoiceByDaNumber(da_number);

                    queryString = string.Format("invoice/getsingleinvoice?invoice_number={0}&customer_group={1}", inv.invoice_number, inv.customer_group);
                    apiUrl = string.Format("{0}/{1}", ConfigurationManager.AppSettings["APIUrl"], queryString);

                    var dataSource = db.NotaReturRepository.GetDebitAdvice(new DataSourceRequest(), da_number, user);

                    report.ReportPath = repPath;
                    report.EnableExternalImages = true;

                    // Edit by Angga 20181025
                    // Note: Stored data not displayed on this reports while using DataSet.
                    // It should by work by passing data to Report Parameters.
                    if (dataSource.Count > 0)
                    {
                        report.SetParameters(new ReportParameter("pInvoiceNumber", dataSource.First().Invoice_Number));
                        report.SetParameters(new ReportParameter("pName", dataSource.First().Name));
                        report.SetParameters(new ReportParameter("pStreet", dataSource.First().Street));
                        report.SetParameters(new ReportParameter("pCity", dataSource.First().City));
                        report.SetParameters(new ReportParameter("pNameTotalIncludeVat", dataSource.First().Name_Total_Include_VAT));
                        report.SetParameters(new ReportParameter("pNameTotalExcludeVat", dataSource.First().Name_Total_Exclude_VAT));
                        report.SetParameters(new ReportParameter("pNameVat", dataSource.First().Name_VAT));
                        report.SetParameters(new ReportParameter("pNameDormitory", dataSource.First().Name_Dormitory));
                        report.SetParameters(new ReportParameter("pNameVatDormitory", dataSource.First().Name_VatDormitory));
                        report.SetParameters(new ReportParameter("pNameMiccelenious", dataSource.First().Name_Micellenious));
                        report.SetParameters(new ReportParameter("pNameStamp", dataSource.First().Name_Stamp));

                        report.SetParameters(new ReportParameter("pTotalNonVat", dataSource.First().Total_Non_VAT == 0 ? string.Empty : String.Format("{0:N}", dataSource.First().Total_Non_VAT)));
                        report.SetParameters(new ReportParameter("pTotal", String.Format("{0:N}", dataSource.First().Total)));
                        report.SetParameters(new ReportParameter("pTotalVat", dataSource.First().Total_VAT == 0 ? string.Empty : String.Format("{0:N}", dataSource.First().Total_VAT)));
                        report.SetParameters(new ReportParameter("pTotalDormitory", dataSource.First().Total_Dormitory == 0 ? string.Empty : String.Format("{0:N}", dataSource.First().Total_Dormitory)));
                        report.SetParameters(new ReportParameter("pTotalVatDormitory", dataSource.First().Total_VatDormitory == 0 ? string.Empty : String.Format("{0:N}", dataSource.First().Total_VatDormitory)));
                        report.SetParameters(new ReportParameter("pTotalMiccelenious", dataSource.First().Total_Micellenious == 0 ? string.Empty : String.Format("{0:N}", dataSource.First().Total_Micellenious)));
                        report.SetParameters(new ReportParameter("pStamp", dataSource.First().Stamp == 0 ? string.Empty : String.Format("{0:N}", dataSource.First().Stamp)));
                        report.SetParameters(new ReportParameter("pDateNow", dataSource.First().Date_Now.ToString("yyyy.MM.dd")));
                        report.SetParameters(new ReportParameter("pCurrencyCode", dataSource.First().Currency_Code));
                        report.SetParameters(new ReportParameter("pTotalAll", String.Format("{0:N}", dataSource.First().TotalAll)));
                        report.SetParameters(new ReportParameter("pWord", dataSource.First().Word));
                        report.SetParameters(new ReportParameter("pCustomerCode", dataSource.First().Customer_Code));
                        report.SetParameters(new ReportParameter("pTaxNumber", dataSource.First().Tax_Number));
                        report.SetParameters(new ReportParameter("pUsers", dataSource.First().Users));
                        report.SetParameters(new ReportParameter("pDaNumber", da_number));
                        report.SetParameters(new ReportParameter("pQrCode", barcode.GenerateQrCode(apiUrl)));
                        report.SetParameters(new ReportParameter("pPostingDate", dataSource.First().Posting_Date.ToString("yyyy.MM.dd")));
                    }
                    // Set empty datasource
                    report.DataSources.Clear();
                    report.DataSources.Add(new ReportDataSource("DataSet2", dataSource));

                    report.Refresh();

                    byte[] bytes = report.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

                    return bytes;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region Get Summary Invoice All Cust. Report
        public byte[] GetSumReceiptInvoiceAllReport(string dateFrom, string dateTo, List<Invoice> dataSource, string repPath, Parameter paramSignBy, out string extension, out string mimeType)
        {
            try
            {
                Stream stream = new MemoryStream();
                Warning[] warnings;
                string encoding;
                string[] streamIds;
                LocalReport report = new LocalReport();            

                using (var db = new DbHelper())
                {                   
                    report.ReportPath = repPath;
                    report.EnableExternalImages = true;

                    report.ReportPath = repPath;
                    report.SetParameters(new ReportParameter("pDateFrom", dateFrom));
                    report.SetParameters(new ReportParameter("pDateTo", dateTo));
                    report.SetParameters(new ReportParameter("pDateTo", dateTo));
                    report.SetParameters(new ReportParameter("pCustomerName", dataSource.FirstOrDefault().customer_name));
                    report.SetParameters(new ReportParameter("pCustomerAddress", dataSource.FirstOrDefault().customer_address));
                    report.SetParameters(new ReportParameter("pPosition", paramSignBy.description));
                    report.SetParameters(new ReportParameter("pSignBy", paramSignBy.value1));
                    report.DataSources.Clear();
                    report.DataSources.Add(new ReportDataSource("DataSetSummaryInvoice", dataSource));
                    report.Refresh();

                    byte[] bytes = report.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

                    return bytes;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region Get Motor Vehicle Invoice Report
        public byte[] GetMvInvoiceReport(string da_number, string business_unit_id, string user, out string extension, out string mimeType)
        {
            Stream stream = new MemoryStream();
            Warning[] warnings;
            string encoding, repPath, apiUrl, queryString = string.Empty;
            string[] streamIds;
            LocalReport report = new LocalReport();
            ReportViewer reportViewer = new ReportViewer();
            Infrastructure.Barcode.Barcode barcode = new Infrastructure.Barcode.Barcode();

            try
            {
                repPath = string.Format("{0}/{1}", ConfigurationManager.AppSettings["RDLPath"], ConfigurationManager.AppSettings["MvInvoicePath"]);

                using (var db = new DbHelper())
                {
                    var inv = db.InvoiceRepository.GetInvoiceByDaNumber(da_number);

                    queryString = string.Format("invoice/getsingleinvoice?invoice_number={0}&customer_group={1}", inv.invoice_number, inv.customer_group);
                    apiUrl = string.Format("{0}/{1}", ConfigurationManager.AppSettings["APIUrl"], queryString);

                    var dataSource = db.NotaReturRepository.GetMvInvoice(da_number, user);
                    
                    report.ReportPath = repPath;
                    report.SetParameters(new ReportParameter("pQrCode", barcode.GenerateQrCode(apiUrl)));
                    report.DataSources.Clear();
                    report.DataSources.Add(new ReportDataSource("MvInvoice", dataSource));

                    report.Refresh();

                    byte[] bytes = report.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

                    return bytes;
                }
            }
            catch(Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region Get Nota Retur Report
        public byte[] GetNotaRetur(string da_number, string user, out string extension, out string mimeType)
        {
            try
            {
                using (var db = new DbHelper())
                {
                    string repPath = string.Format("{0}/{1}", ConfigurationManager.AppSettings["RDLPath"], ConfigurationManager.AppSettings["NotaReturPath"]);

                    var dataSource = db.NotaReturRepository.GetPrintNotaRetur(da_number, user);

                    LocalReport report = new LocalReport();
                    report.ReportPath = repPath;
                    report.DataSources.Clear();
                    report.DataSources.Add(new ReportDataSource("DataSet1", dataSource));
                    report.Refresh();

                    Stream stream = new MemoryStream();
                    Warning[] warnings;
                    string encoding;
                    string[] streamIds;

                    byte[] bytes = report.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

                    return bytes;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region Get Spare Part Debit Advice Report
        public byte[] GetSpDebitAdviceReport(string da_number, string user, out string extension, out string mimeType)
        {
            try
            {
                Stream stream = new MemoryStream();
                Warning[] warnings;
                string encoding, repPath, queryString, apiUrl = string.Empty;
                string[] streamIds;
                LocalReport report = new LocalReport();
                ReportViewer reportViewer = new ReportViewer();
                Infrastructure.Barcode.Barcode barcode = new Infrastructure.Barcode.Barcode();
                
                using (var db = new DbHelper())
                {
                    repPath = string.Format("{0}/{1}", ConfigurationManager.AppSettings["RDLPath"], ConfigurationManager.AppSettings["SpDebitAdvicePath"]);

                    var dataSource = db.NotaReturRepository.GetSpDebitAdvice(da_number, user);
                    //var dataSource = Service<NoturService>().GetSpDebitAdvice(new DataSourceRequest(), da_number, user);

                    var inv = db.InvoiceRepository.GetSingleInvoiceByInvoiceNumber("1", da_number);

                    queryString = string.Format("invoice/getsingleinvoice?invoice_number={0}&customer_group={1}", inv.invoice_number, inv.customer_group);
                    apiUrl = string.Format("{0}/{1}", ConfigurationManager.AppSettings["APIUrl"], queryString);


                    report.ReportPath = repPath;
                    report.SetParameters(new ReportParameter("pDaNo", dataSource.First().DaNo));
                    report.SetParameters(new ReportParameter("pDeliveryDate", dataSource.First().DeliveryDate.ToString("dd/MM/yyyy")));
                    report.SetParameters(new ReportParameter("pCustomerName", dataSource.First().CustomerName));
                    report.SetParameters(new ReportParameter("pCustomerAddress", dataSource.First().CustomerAddress));
                    report.SetParameters(new ReportParameter("pFakturPajakNo", dataSource.First().FakturPajakNo));
                    report.SetParameters(new ReportParameter("pTurnOverAmount", dataSource.First().TurnOverAmount == 0 ? string.Empty : String.Format("{0:N}", dataSource.First().TurnOverAmount)));
                    report.SetParameters(new ReportParameter("p10Vat", dataSource.First().Vat10 == 0 ? string.Empty : String.Format("{0:N}", dataSource.First().Vat10)));
                    report.SetParameters(new ReportParameter("pTotal", dataSource.First().Total == 0 ? string.Empty : String.Format("{0:N}", dataSource.First().Total)));
                    report.SetParameters(new ReportParameter("pDueDateTo", dataSource.First().DueDateTo.ToString("dd/MM/yyyy")));
                    report.SetParameters(new ReportParameter("pDueDateVat", dataSource.First().DueDateVat.ToString("dd/MM/yyyy")));
                    report.SetParameters(new ReportParameter("pCollectionDate", dataSource.First().CollectionDate.ToString("dd/MM/yyyy")));
                    report.SetParameters(new ReportParameter("pQrCode", barcode.GenerateQrCode(apiUrl)));
                    report.DataSources.Clear();
                    report.DataSources.Add(new ReportDataSource("DataSet2", dataSource));

                    report.Refresh();

                    byte[] bytes = report.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

                    return bytes;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region Get Spare Part Credit Advice Report
        public byte[] GetSpCreditAdviceReport(string da_number, string user, out string extension, out string mimeType)
        {
            try
            {
                Stream stream = new MemoryStream();
                Warning[] warnings;
                string encoding, repPath, queryString, apiUrl = string.Empty;
                string[] streamIds;
                LocalReport report = new LocalReport();
                ReportViewer reportViewer = new ReportViewer();
                Infrastructure.Barcode.Barcode barcode = new Infrastructure.Barcode.Barcode();

                using (var db = new DbHelper())
                {
                    repPath = string.Format("{0}/{1}", ConfigurationManager.AppSettings["RDLPath"], ConfigurationManager.AppSettings["SpCreditAdvicePath"]);

                    var dataSource = db.NotaReturRepository.GetSpCreditAdvice(new DataSourceRequest(), da_number, user);
                    //var dataSource = Service<NoturService>().GetSpDebitAdvice(new DataSourceRequest(), da_number, user);

                    var inv = db.InvoiceRepository.GetSingleInvoiceByInvoiceNumber("2", da_number);

                    queryString = string.Format("invoice/getsingleinvoice?invoice_number={0}&customer_group={1}", inv.invoice_number, inv.customer_group);
                    apiUrl = string.Format("{0}/{1}", ConfigurationManager.AppSettings["APIUrl"], queryString);


                    
                    report.ReportPath = repPath;
                    report.SetParameters(new ReportParameter("pDaNo", dataSource.First().CaNo));
                    report.SetParameters(new ReportParameter("pDeliveryDate", dataSource.First().DeliveryDate.ToString("dd/MM/yyyy")));
                    report.SetParameters(new ReportParameter("pCustomerName", dataSource.First().CustomerName));
                    report.SetParameters(new ReportParameter("pCustomerAddress", dataSource.First().CustomerAddress));
                    report.SetParameters(new ReportParameter("pFakturPajakNo", dataSource.First().FakturPajakNo));
                    report.SetParameters(new ReportParameter("pTurnOverAmount", dataSource.First().TurnOverAmount == 0 ? string.Empty : String.Format("{0:N}", dataSource.First().TurnOverAmount)));
                    report.SetParameters(new ReportParameter("p10Vat", dataSource.First().Vat10 == 0 ? string.Empty : String.Format("{0:N}", dataSource.First().Vat10)));
                    report.SetParameters(new ReportParameter("pTotal", dataSource.First().Total == 0 ? string.Empty : String.Format("{0:N}", dataSource.First().Total)));
                    report.SetParameters(new ReportParameter("pDueDateTo", dataSource.First().DueDateTo.ToString("dd/MM/yyyy")));
                    report.SetParameters(new ReportParameter("pDueDateVat", dataSource.First().DueDateVat.ToString("dd/MM/yyyy")));
                    report.SetParameters(new ReportParameter("pCollectionDate", dataSource.First().CollectionDate.ToString("dd/MM/yyyy")));
                    report.SetParameters(new ReportParameter("pQrCode", barcode.GenerateQrCode(apiUrl)));
                    report.DataSources.Clear();
                    report.DataSources.Add(new ReportDataSource("DataSet2", dataSource));

                    report.Refresh();

                    byte[] bytes = report.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

                    return bytes;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region Get Attachment Spare Part Notur
        public byte[] GetAttachmentSparePartNotur(string da_number, string user, out string extension, out string mimeType)
        {
            try
            {
                Stream stream = new MemoryStream();
                Warning[] warnings;
                string encoding, repPath;
                string[] streamIds;
                LocalReport report = new LocalReport();

                using (var db = new DbHelper())
                {
                    repPath = string.Format("{0}/{1}", ConfigurationManager.AppSettings["RDLPath"], ConfigurationManager.AppSettings["SpAttCreditAdvicePath"]);
                    
                    List<AttachmentSpDebitAdvice> dataSource = db.AttachmentRepository.GetSpCreditAdvice(new DataSourceRequest(), da_number, user).ToList();

                    report.ReportPath = repPath;
                    report.SetParameters(new ReportParameter("pDaNo", dataSource.First().CaNo));
                    report.SetParameters(new ReportParameter("pDeliveryDate", dataSource.First().InvoiceDate.ToString("dd/MM/yyyy")));
                    report.SetParameters(new ReportParameter("pCustomerName", dataSource.First().CustomerName));
                    report.SetParameters(new ReportParameter("pCustomerAddress", dataSource.First().CustomerAddress));

                    DataTable table = new DataTable();
                    table.Columns.Add("InvoiceNo", type: typeof(string));
                    table.Columns.Add("InvoiceDate", type: typeof(DateTime));
                    table.Columns.Add("OT", type: typeof(string));
                    table.Columns.Add("OrderNo", type: typeof(string));
                    table.Columns.Add("Items", type: typeof(int));
                    table.Columns.Add("GrossAmount", type: typeof(decimal));
                    table.Columns.Add("NetAmount", type: typeof(decimal));

                    foreach (AttachmentSpDebitAdvice a in dataSource)
                    {
                        DataRow row = table.NewRow();

                        row["InvoiceNo"] = a.InvoiceNo;
                        row["InvoiceDate"] = a.InvoiceDate;
                        row["OT"] = a.OT;
                        row["OrderNo"] = a.OrderNo;
                        row["Items"] = a.Items;
                        row["GrossAmount"] = a.GrossAmount;
                        row["NetAmount"] = a.NetAmount;

                        table.Rows.Add(row);
                    }

                    report.DataSources.Clear();
                    report.DataSources.Add(new ReportDataSource("DataSetSpAttachment", table));

                    report.Refresh();

                    byte[] bytes = report.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

                    return bytes;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region Get Attachment Spare Part
        public byte[] GetAttachmentSparePart(string da_number, string user, out string extension, out string mimeType)
        {
            try
            {
                Stream stream = new MemoryStream();
                Warning[] warnings;
                string encoding, repPath;
                string[] streamIds;
                LocalReport report = new LocalReport();

                using (var db = new DbHelper())
                {
                    repPath = string.Format("{0}/{1}", ConfigurationManager.AppSettings["RDLPath"], ConfigurationManager.AppSettings["SpAttDebitAdvicePath"]);

                    var inv = db.InvoiceRepository.GetSingleInvoiceByInvoiceNumber("1", da_number);
                    List<AttachmentSpDebitAdvice> dataSource = db.AttachmentRepository.GetSpDebitAdvice(new DataSourceRequest(), da_number, user, inv.doc_date.ToString("yyyy-MM-dd")).ToList();

                    report.ReportPath = repPath;
                    report.SetParameters(new ReportParameter("pDaNo", dataSource.First().CaNo));
                    report.SetParameters(new ReportParameter("pDeliveryDate", dataSource.First().InvoiceDate.ToString("dd/MM/yyyy")));
                    report.SetParameters(new ReportParameter("pCustomerName", dataSource.First().CustomerName));
                    report.SetParameters(new ReportParameter("pCustomerAddress", dataSource.First().CustomerAddress));

                    DataTable table = new DataTable();
                    table.Columns.Add("InvoiceNo", type: typeof(string));
                    table.Columns.Add("InvoiceDate", type: typeof(DateTime));
                    table.Columns.Add("OT", type: typeof(string));
                    table.Columns.Add("OrderNo", type: typeof(string));
                    table.Columns.Add("Items", type: typeof(int));
                    table.Columns.Add("GrossAmount", type: typeof(decimal));
                    table.Columns.Add("NetAmount", type: typeof(decimal));

                    foreach (AttachmentSpDebitAdvice a in dataSource)
                    {
                        DataRow row = table.NewRow();

                        row["InvoiceNo"] = a.InvoiceNo;
                        row["InvoiceDate"] = a.InvoiceDate;
                        row["OT"] = a.OT;
                        row["OrderNo"] = a.OrderNo;
                        row["Items"] = a.Items;
                        row["GrossAmount"] = a.GrossAmount;
                        row["NetAmount"] = a.NetAmount;

                        table.Rows.Add(row);
                    }

                    report.DataSources.Clear();
                    report.DataSources.Add(new ReportDataSource("DataSetSpAttachment", table));

                    report.Refresh();

                    byte[] bytes = report.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

                    return bytes;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        public byte[] GetNotaReturSpOrMv(List<SpNotaRetur> dataSource, SpNotaRetur notaRetur, out string extension, out string mimeType)
        {
            Stream stream = new MemoryStream();
            Warning[] warnings;
            string encoding, repPath;
            string[] streamIds;
            LocalReport report = new LocalReport();

            repPath = string.Format("{0}/{1}", ConfigurationManager.AppSettings["RDLPath"], ConfigurationManager.AppSettings["NotaReturSpPath"]);

            
            report.ReportPath = repPath;
            report.SetParameters(new ReportParameter("pNomorRetur", notaRetur.NomorRetur));
            report.SetParameters(new ReportParameter("pFakturPajakNo", notaRetur.FakturPajakNo));
            report.SetParameters(new ReportParameter("pTglRetur", notaRetur.TglRetur.ToString("dd/MM/yyyy")));
            report.SetParameters(new ReportParameter("pTglFakturPajak", notaRetur.TglFakturPajak.ToString("dd/MM/yyyy")));
            report.SetParameters(new ReportParameter("pCustName", notaRetur.CustName));
            report.SetParameters(new ReportParameter("pAddress1", notaRetur.Address1));
            report.SetParameters(new ReportParameter("pAddress2", notaRetur.Address2));
            report.SetParameters(new ReportParameter("pNpwp", notaRetur.Npwp));

            DataTable table = new DataTable();
            table.Columns.Add("NoUrut", type: typeof(string));
            table.Columns.Add("PiNo", type: typeof(string));
            table.Columns.Add("PartNo", type: typeof(string));
            table.Columns.Add("PartName", type: typeof(string));
            table.Columns.Add("Kuantum", type: typeof(int));
            table.Columns.Add("HargaSatuan", type: typeof(decimal));
            table.Columns.Add("HargaBkp", type: typeof(decimal));

            List<SpNotaRetur> distinctData = dataSource.FindAll(s => s.NomorRetur == notaRetur.NomorRetur).ToList();

            int baris = 1;
            foreach (SpNotaRetur a in distinctData)
            {
                var itemNo = baris;
                DataRow row = table.NewRow();

                row["NoUrut"] = itemNo;
                row["PiNo"] = a.PiNo;
                row["PartNo"] = a.PartNo;
                row["PartName"] = a.PartName;
                row["Kuantum"] = a.Kuantum;
                row["HargaSatuan"] = a.HargaSatuan;
                row["HargaBkp"] = a.HargaBkp;

                table.Rows.Add(row);
                baris++;
            }

            report.DataSources.Clear();
            report.DataSources.Add(new ReportDataSource("DataSetSpNotaReturDetail", table));

            report.Refresh();

            byte[] bytes = report.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
            

            return bytes;
        }
    }
}
